

#include <iostream>
#include <fstream>
#include <map>
#include <time.h>

//PETSc
#include <petsc.h>
#include <petscksp.h>

//Spline
#include "msp/Mesh.h"
#include "msp/BasesVertex.h"
#include "msp/Bases.h"
#include "msp/BasesCell.h"
#include "msp/MSplineFunction.h"
#include "msp/PointSet.h"
#include "msp/Coordinate.h"
#include "msp/MSplineFunctions.h"
#include "msp/Functions.h"

//FEM
#include "msp/FEM_Functions.h"

//Parametrization
#include "msp/PMap_DynamicFunctions.h"
#include "msp/PMap_Functions.h"
#include "msp/FEM_Part.h"

using namespace std;
/**
 * \section PDE_PFEM_2
 * In this application, we want to solve the following PDEs:
 * - 1: \f$ g(x,y)\nabla ((1/g(x,y))\nabla F)=f \f$
 * - 2: \f$ g(x,y)\nabla ((1/g(x,y))\nabla F)=f+F \f$
 * - 3: \f$ g(x,y)\nabla ((1/g(x,y))\nabla F)=f+F^k \f$, where \f$k=1,2,3,4 ...\f$
 *   Here, we take \f$ k=2 \f$
 * - 4: \f$ g(x,y)\nabla ((1/g(x,y))\nabla F)=R((F-F(center))/(F(XPoint)-F(center)))\f$
 * over the alpha shape domain (without a hole) [This PDE problem desn't finish until now]
 * where \f$g(x,y) \f$ is given
 * - \c Input: Read the initial parametric mesh and the coeffients
 * - \c Output: The solution of the PDE, the errors
 *
 * \subsection If one wants to solve these PDEs over other physical domains,
 * - \c choose an ExampleIndex for this example. This ExampleIndex must be different from the ExampleIndices
 * which we have used.
* - \c Based on this ExampleIndex, one should modify the functions:
*  - KMatrixValue
*  - ExactFunctionValue
*  - RightSideFunctionValue
*  - FxFunctionValue
*  - FyFunctionValue
* which are used to generate coeff_matrix, load_vect and errors between FEM solutions and exact solutions. And,
* Here, we use some methods to modify the parameterization.
*
* - approximate the boundary of a physical domain: ModifyRefinedParametrization()
*   In order to use ModifyRefinedParametrization(), some preparations should be done before using it, such as
*   reading in the boundary edges file, generating the exact positions the points on the boundary "Qs", given the
*   parameters "Ps" for "Qs".
*
* - modify the parameterization inside the domain with the Spring model
*   which we developed in the application: mspSpringparametrization. Thus, we need to set parameters "Hc",
*   "CoulombCoeff" and so on. This part of modifying the parameterization is implemented in ModifyParameterizationBySpringModelAndBoundaryFixing()
*   which is a function in the MSplineFunctions class.
*   Based on this parameterization, all the image of interior vertices should be localed within the domain covered by the initial parameterization.
*   Thus, when we subdivide the initial parameter mesh, some interior vertices might be in the outside of the domain. we should deal with that case by case.
*   For example, in main() function, we take the example with ExampleIndex=32 for example. From the line "if(ExampleIndex==32)//if ExampleIndex=32, reset the position of vertices of P_RefinedParametricMap",
*   This method can be generalized to treated the physical domain which bounded by implicit equations.
*
* - \c For other new type of PDE, one needs write the functions in order to generate its own coeff_matrix and load_vect.
*
* \subsection usage Usage
* - \c It can be used as follows:
* \code
* ./msplinePDE_PFEM_2
* \endcode
*
* - \c For this applications, modification parameterizations is involved.
 */
//======
void solveEquation1(ofstream &OfTime, Mesh * P_ParametricMesh, vector<vector<double> >& Coeff_Parametric_Map,
                    unsigned int ExampleIndex)
{
    Vec load_vect, m_solut;
    Mat coeff_matrix;
    KSPConvergedReason reason;



    unsigned int EquationIndex=1;
    //Generate the initial Parametric map
    MSplineFunctions * P_ParametricMap = new MSplineFunctions(Coeff_Parametric_Map, *P_ParametricMesh);


    unsigned int k;
    //k=0;
    cout<<"Subdivide the refined mesh: input k for this subdivision"<<endl;
    OfTime<<"Subdivide the refined mesh: input k for this subdivision"<<endl;

    cin>>k;

    OfTime<<"Here, k = "<<k<<endl;


    map<unsigned int, unsigned int> OrigialVertexToCurrentVertex;
    Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(k, OrigialVertexToCurrentVertex);


    /* Define a size of cell*/
    double h=(P_FEMMesh->P_Cells->at(0)).CellSize[1]-(P_FEMMesh->P_Cells->at(0)).CellSize[0];

    cout<<"Generate Gauss points and Weights"<<endl;
    OfTime<<endl;
    OfTime<<"Generate Guass points and Weights"<<endl;
    double t0=(double)clock();

    //Preparation: GuassPoints
    cout<<"Please input the number of Guass Points:"<<endl;
    unsigned int Gp;
    cin>>Gp;
    //unsigned int u_gaussnum=Gp, v_gaussnum=Gp;
    unsigned int u_gaussnum=Gp, v_gaussnum=Gp;
    unsigned int load_gaussnum=Gp;//load_gaussnum=5;
    vector<double> gausspt, m_IntePt, m_loadw;
    //Weights/////////
    double **w;
     w=GaussPoints_Weights(u_gaussnum, load_gaussnum, gausspt, m_IntePt, m_loadw);
    double t1=(double)clock();
    cout<<"\"Generate Guass points and Weights\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<endl;

    OfTime<<"\"Generate Guass points and Weights\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<endl;
    OfTime<<"u_gaussnum="<<u_gaussnum<<"  v_gaussnum="<<v_gaussnum<<endl;
    OfTime<<"The Basis Number = "<<4*(P_FEMMesh->P_Vertices->size())<<endl;

    cout<<"The Basis Number = "<<4*(P_FEMMesh->P_Vertices->size())<<endl;
    PetscInt NumVertex = P_FEMMesh->P_Vertices->size();


    cout<<"Initialized coeffMatrix and Generate_coeffMatrix"<<endl;

    OfTime<<endl;
    OfTime<<"Initialized coeffMatrix and Generate_coeffMatrix"<<endl;

    t0=(double)clock();

    MatCreate(PETSC_COMM_WORLD, &coeff_matrix);

    MatSetSizes(coeff_matrix, PETSC_DECIDE, PETSC_DECIDE, NumVertex*4, NumVertex*4);
    MatSetFromOptions(coeff_matrix);
    MatMPIAIJSetPreallocation(coeff_matrix, 64, NULL, 64, NULL);
    MatSeqAIJSetPreallocation(coeff_matrix, PETSC_DEFAULT, NULL);
    MatSetUp(coeff_matrix);

    cout<<"Generate_CoeffMatrix_Based on Cells"<<endl;
    double k1=1.0;
    double k2=1.0;//In fact, for solving these four equations, the KMatrix is non-business with k1, k2.

    Generate_CoeffMatrix_Cell(u_gaussnum, v_gaussnum, coeff_matrix, m_IntePt, m_IntePt, w, P_FEMMesh,
                              P_ParametricMesh, *P_ParametricMap, k1, k2, ExampleIndex);


    t1=(double)clock();
    cout<<"\"Initialized coeffMatrix and Generate_coeffMatrix\" costs ="<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    OfTime<<"\"Initialized coeffMatrix and Generate_coeffMatrix\" costs ="<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    //load_vect
    //vector<double> load_vect;
    cout<<"Initialized load Vector and Generate load Vector"<<endl;

    OfTime<<endl;
    OfTime<<"Initialized load Vector and Generate load Vector"<<endl;
    t0=(double)clock();


    Initialized_loadVector(load_vect, P_FEMMesh->P_Vertices->size());
//    GenerateLoadVector_RefinedParametrization(load_vect, gausspt, m_loadw, P_FEMMesh,
//                                              *P_RefinedParametricMap, k1, k2, ExampleIndex);
    Generate_LoadVector_PDE_PFEM(load_vect, gausspt, m_loadw, P_FEMMesh, P_ParametricMesh,
                        *P_ParametricMap, k1, k2, ExampleIndex, EquationIndex);

    t1=(double)clock();

    cout<<"Finished and it costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    OfTime<<"Finished and it costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    //Set Bases type
    cout<<"Set Bases type"<<endl;
    t0=(double)clock();
    vector<unsigned int> BasesType;
    BasesType.resize(4*P_FEMMesh->P_Vertices->size(), 0);
    Set_Bases_type(P_FEMMesh, BasesType);

    t1=(double)clock();
    cout<<"\"Set Bases type\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    OfTime<<endl;
    OfTime<<"There are "<< 4*P_FEMMesh->P_Vertices->size()<<" bases"<<endl;
    OfTime<<"\"Set Bases type\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;



    //Modify LoadVector and CoeffMatrix
    double C=0;//The value at the boundary of physical domain is zero!

    MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);



    map<unsigned int, set<unsigned int> > HTable;
    Initialized_HTable_withoutHangingVertices_1(P_FEMMesh, HTable);
    cout<<"Modification_LoadVector_CoeffMatrix"<<endl;

    OfTime<<endl;
    OfTime<<"Modification_LoadVector_CoeffMatrix"<<endl;
    t0=(double)clock();
    //The value at the boundary of physical domain is zero!
    Modification_LoadVector_CoeffMatrix(C, BasesType, coeff_matrix, load_vect, HTable);

    MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);
    MatSetOption(coeff_matrix, MAT_SYMMETRIC, PETSC_TRUE);

    t1=(double)clock();
    cout<<"\"Modification_LoadVector_CoeffMatrix\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    OfTime<<"\"Modification_LoadVector_CoeffMatrix\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    //
    VecCreate(PETSC_COMM_WORLD, &m_solut);
    VecSetSizes(m_solut, PETSC_DECIDE, 4*P_FEMMesh->P_Vertices->size());
    VecSetFromOptions(m_solut);

    //Solving the Linear System Part======================================================
    cout<<"solve the Linear system"<<endl;
    t0=(double)clock();
    KSP ksp;
    //MatNullSpace nullsp;
    PC pc;

    KSPCreate(PETSC_COMM_WORLD, &ksp);
   // KSPSetType(ksp, KSPCG);
    KSPSetType(ksp, KSPGMRES);
    KSPSetOperators(ksp, coeff_matrix, coeff_matrix);

    //MatNullSpaceCreate(PETSC_COMM_WORLD, PETSC_TRUE, 0, NULL, &nullsp);

    //MatNullSpaceRemove(nullsp, load_vect, NULL);

    //KSPSetNullSpace(ksp, nullsp);

    //MatNullSpaceDestroy(&nullsp);



//    PetscReal rtol=1.e-18;
//    PetscReal stol=1.e-16;

    PetscReal rtol=(1.e-4)*pow(h,4.0);
    PetscReal stol=(1.e-3)*pow(h,4.0);

    KSPSetTolerances(ksp, rtol, stol, PETSC_DEFAULT, PETSC_DEFAULT);

    KSPGetPC(ksp, &pc);
    PCSetType(pc, PCGAMG);
    KSPSetFromOptions(ksp);


   // // //====KSPSetComputeEigenvalues()
   //KSPSetComputeEigenvalues(ksp, PETSC_TRUE);
   // // //=============================


    KSPSetUp(ksp);

    KSPSolve(ksp, load_vect, m_solut);

    KSPGetConvergedReason(ksp, &reason);


    //cout<<"Before KSPComputeEignvalues()"<<endl;
    //======KSPComputeEigenvalues()
    // PetscInt numberEigen;
    // numberEigen=4*P_FEMMesh->P_Vertices->size();

    // PetscReal r[numberEigen];
    // PetscReal c[numberEigen];
    // PetscInt neig[numberEigen];




    //KSPComputeEigenvalues(ksp, numberEigen, r, c, neig);
    // //KSPComputeEigenvaluesExplicitly(ksp, numberEigen, r, c);
    //cout<<"After KSPComputeEignvalues()"<<endl;

    //ofstream ofEigenvalues;
    //ofEigenvalues.open("Eigenvalues.txt");

    //cout<<"EigenValues:"<<endl;
    //for(unsigned int j=0; j<numberEigen; j++)
    //{

    //    ofEigenvalues<<r[j]<<" + "<<c[j]<<" i"<<endl;
    //    cout<<r[j]<<" + "<<c[j]<<" i"<<endl;
    //}
    // //========================
    //ofEigenvalues.close();

    //printf
    PetscInt its;
    KSPGetIterationNumber(ksp, &its);
    PetscPrintf(PETSC_COMM_WORLD, "The number of iteration %D\n", its);

    t1=(double)clock();
    cout<<"\"Solving the linear system\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    cout<<"h = "<<h<<endl;
    cout<<"where, rtol=(1.e-4)*pow(h,4.0)="<<rtol<<" and  stol=(1.e-3)*pow(h,4.0)= "<<stol<<endl;

    OfTime<<endl;
    OfTime<<"\"Solving the linear system\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    OfTime<<"h = "<<h<<endl;
    OfTime<<"where, rtol=(1.e-4)*pow(h,4.0)="<<rtol<<" and  stol=(1.e-3)*pow(h,4.0)= "<<stol<<endl;

    OfTime<<"KSP ConvergedReason= "<<reason<<endl;
    OfTime<<"the number of iteration = "<<its<<endl;
    OfTime<<endl;
    //-------
    OfTime.close();


    //Print load_vect
    PrintLoadVector(load_vect);

    PrintSolution(m_solut);

    PrintAxelFile_FEMSolution(Coeff_Parametric_Map, P_ParametricMesh, k, m_solut);

    //Error=============================================================
    //======L2=====
    vector<double> cell_errorsPhysDom;
    double total_ErrorPhysDom;

    FEML2PosteriorError_PDE_FEM(m_IntePt, w, m_solut, P_FEMMesh, P_ParametricMesh,
                        P_ParametricMap, ExampleIndex,EquationIndex, cell_errorsPhysDom, total_ErrorPhysDom);

    //Destroy ksp coeff_matrix load_vec m_solut
    KSPDestroy(&ksp);
    MatDestroy(&coeff_matrix);
    VecDestroy(&load_vect);
    VecDestroy(&m_solut);
}
//=====================================================

void BMatrix_Equation2(Mat &BMatrix, vector<double>& gausspt, vector<double>& gaussw,
                       unsigned int ExampleIndex, unsigned int EquationIndex,
                       const Mesh* P_FEMMesh, const Mesh* P_ParametricMesh,
                       MSplineFunctions * P_MSplineParametricMap)
{
    MatZeroEntries(BMatrix);

    int gaussnum = gausspt.size();

    PetscScalar v;
    PetscInt I, J;
    for(unsigned int i=0;i<P_FEMMesh->P_Cells->size();i++)// Based on each cell
    {
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];
//=============================================================
        vector<double> funval, m_Baseval_k, m_Baseval_l;
        double tmpx;
        vector<vector<double> > m_JPart;
//==============================================================
        vector<Coordinate*> Pcoordinate_Ps;
        for (int l=0; l<gaussnum; l++)
        {
            for (int j=0; j<gaussnum; j++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=(x1+x0+(x1-x0)*gausspt[l]) * 0.5;//u[l];
                CoorP->xy[1]=(y0+y1+(y1-y0)*gausspt[j]) * 0.5;//v[j];
                Pcoordinate_Ps.push_back(CoorP);
            }
        }
//===============================================================
        ComputeFunval_MSplineParametricMap_PDE_PFEM(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent, funval, 4,
                                                    *P_ParametricMesh, P_MSplineParametricMap, ExampleIndex, EquationIndex);
        //The value of phi in the right side
//===============================================================

        BasesCell *P_BC=new BasesCell(i, *P_FEMMesh);//Bases over the i-th cell
        vector<double> DetJ;
        double k1=1.0, k2=1.0;//Here is non-business with k1, k1, in fact.
        ParametricMapSigma_MSplineParametricMap(Pcoordinate_Ps, m_JPart, DetJ, P_FEMMesh->P_Cells->at(i).ItsParent,
                                                *P_MSplineParametricMap, P_ParametricMesh, k1, k2, ExampleIndex);

        for(unsigned int k=0; k< P_BC->P_Bases_Cell.size(); k++)
        {
            m_Baseval_k.clear();
            value_Cell_Hermite(0, 0, Pcoordinate_Ps, P_BC->P_Bases_Cell[k]->P_HermiteData_Cells[0], m_Baseval_k, *P_FEMMesh);

            I=P_BC->P_Bases_Cell[k]->Index_basis;
            for(unsigned int l=k; l< P_BC->P_Bases_Cell.size(); l++)
            {
                m_Baseval_l.clear();
                value_Cell_Hermite(0, 0, Pcoordinate_Ps, P_BC->P_Bases_Cell[l]->P_HermiteData_Cells[0], m_Baseval_l, *P_FEMMesh);

                tmpx=0.0;
                for(unsigned int j=0; j<m_Baseval_k.size(); j++)
                {
                    int nn = j/gaussnum;
                    int mm = j%gaussnum;

                    tmpx += gaussw[nn]*gaussw[mm]*funval[j]*m_Baseval_k[j]*m_Baseval_l[j]*DetJ[j];
                }

                J=P_BC->P_Bases_Cell[l]->Index_basis;

                v=tmpx * (x1-x0) * (y1-y0)/ 4;

                if(I==J)
                {
                    MatSetValues(BMatrix,1, &I, 1, &J, &v, ADD_VALUES);
                }
                else
                {
                    MatSetValues(BMatrix,1, &I, 1, &J, &v, ADD_VALUES);
                    MatSetValues(BMatrix,1, &J, 1, &I, &v, ADD_VALUES);
                }

            }
        }

    }
}
//=====================================================
void solveEquation2(ofstream &OfTime, Mesh * P_ParametricMesh, vector<vector<double> >& Coeff_Parametric_Map,
                    unsigned int ExampleIndex)
{
    Vec load_vect, m_solut;
    Mat coeff_matrix;
    Mat BMatrix;
    KSPConvergedReason reason;

    unsigned int EquationIndex=2;

    //Generate the initial Parametric map
    MSplineFunctions * P_ParametricMap = new MSplineFunctions(Coeff_Parametric_Map, *P_ParametricMesh);

    unsigned int k;
    //k=0;
    cout<<"Subdivide the refined mesh: input k for this subdivision"<<endl;
    OfTime<<"Subdivide the refined mesh: input k for this subdivision"<<endl;

    cin>>k;

    OfTime<<"Here, k = "<<k<<endl;

    map<unsigned int, unsigned int> OrigialVertexToCurrentVertex;
    Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(k, OrigialVertexToCurrentVertex);

    /* Define a size of cell*/
    double h=(P_FEMMesh->P_Cells->at(0)).CellSize[1]-(P_FEMMesh->P_Cells->at(0)).CellSize[0];

    cout<<"Generate Gauss points and Weights"<<endl;
    OfTime<<endl;
    OfTime<<"Generate Guass points and Weights"<<endl;
    double t0=(double)clock();

    //Preparation: GuassPoints
    cout<<"Please input the number of Guass Points:"<<endl;
    unsigned int Gp;
    cin>>Gp;
    unsigned int u_gaussnum=Gp, v_gaussnum=Gp;
    unsigned int load_gaussnum=Gp;//load_gaussnum=5;
    vector<double> gausspt, m_IntePt, m_loadw;
    //Weights/////////
    double **w;
     w=GaussPoints_Weights(u_gaussnum, load_gaussnum, gausspt, m_IntePt, m_loadw);
    double t1=(double)clock();
    cout<<"\"Generate Guass points and Weights\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<endl;

    OfTime<<"\"Generate Guass points and Weights\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<endl;
    OfTime<<"u_gaussnum="<<u_gaussnum<<"  v_gaussnum="<<v_gaussnum<<endl;
    OfTime<<"The Basis Number = "<<4*(P_FEMMesh->P_Vertices->size())<<endl;

    cout<<"The Basis Number = "<<4*(P_FEMMesh->P_Vertices->size())<<endl;
    PetscInt NumVertex = P_FEMMesh->P_Vertices->size();

    cout<<"Initialized coeffMatrix and Generate_coeffMatrix"<<endl;

    OfTime<<endl;
    OfTime<<"Initialized coeffMatrix and Generate_coeffMatrix"<<endl;

    t0=(double)clock();

    MatCreate(PETSC_COMM_WORLD, &coeff_matrix);

    MatSetSizes(coeff_matrix, PETSC_DECIDE, PETSC_DECIDE, NumVertex*4, NumVertex*4);
    MatSetFromOptions(coeff_matrix);
    MatMPIAIJSetPreallocation(coeff_matrix, 64, NULL, 64, NULL);
    MatSeqAIJSetPreallocation(coeff_matrix, PETSC_DEFAULT, NULL);
    MatSetUp(coeff_matrix);



    cout<<"Generate_CoeffMatrix_Based on Cells"<<endl;
    double k1=1.0;
    double k2=1.0;//In fact, for solving these four equations, the KMatrix is non-business with k1, k2.
    Generate_CoeffMatrix_Cell(u_gaussnum, v_gaussnum, coeff_matrix, m_IntePt, m_IntePt, w, P_FEMMesh,
                              P_ParametricMesh, *P_ParametricMap, k1, k2, ExampleIndex);

    MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);
    //**********************************

    MatCreate(PETSC_COMM_WORLD, &BMatrix);

    MatSetSizes(BMatrix, PETSC_DECIDE, PETSC_DECIDE, NumVertex*4, NumVertex*4);
    MatSetFromOptions(BMatrix);
    MatMPIAIJSetPreallocation(BMatrix, 64, NULL, 64, NULL);
    MatSeqAIJSetPreallocation(BMatrix, PETSC_DEFAULT, NULL);
    MatSetUp(BMatrix);

    BMatrix_Equation2(BMatrix, gausspt, m_loadw, ExampleIndex, EquationIndex,
                      P_FEMMesh, P_ParametricMesh, P_ParametricMap);

    MatAssemblyBegin(BMatrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(BMatrix, MAT_FINAL_ASSEMBLY);

    MatAXPY(coeff_matrix, -1.0, BMatrix, DIFFERENT_NONZERO_PATTERN);
    //**********************************

    MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);

    t1=(double)clock();
    cout<<"\"Initialized coeffMatrix and Generate_coeffMatrix\" costs ="<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    OfTime<<"\"Initialized coeffMatrix and Generate_coeffMatrix\" costs ="<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    //load_vect
    //vector<double> load_vect;
    cout<<"Initialized load Vector and Generate load Vector"<<endl;

    OfTime<<endl;
    OfTime<<"Initialized load Vector and Generate load Vector"<<endl;
    t0=(double)clock();


    Initialized_loadVector(load_vect, P_FEMMesh->P_Vertices->size());

    Generate_LoadVector_PDE_PFEM(load_vect, gausspt, m_loadw, P_FEMMesh, P_ParametricMesh,
                        *P_ParametricMap, k1, k2, ExampleIndex, EquationIndex);

    t1=(double)clock();

    cout<<"Finished and it costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    OfTime<<"Finished and it costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    //Set Bases type
    cout<<"Set Bases type"<<endl;
    t0=(double)clock();
    vector<unsigned int> BasesType;
    BasesType.resize(4*P_FEMMesh->P_Vertices->size(), 0);
    Set_Bases_type(P_FEMMesh, BasesType);

    t1=(double)clock();
    cout<<"\"Set Bases type\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    OfTime<<endl;
    OfTime<<"There are "<< 4*P_FEMMesh->P_Vertices->size()<<" bases"<<endl;
    OfTime<<"\"Set Bases type\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;



    //Modify LoadVector and CoeffMatrix
    double C=0;//The value at the boundary of physical domain is zero!

    MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);



    map<unsigned int, set<unsigned int> > HTable;
    Initialized_HTable_withoutHangingVertices_1(P_FEMMesh, HTable);
    cout<<"Modification_LoadVector_CoeffMatrix"<<endl;

    OfTime<<endl;
    OfTime<<"Modification_LoadVector_CoeffMatrix"<<endl;
    t0=(double)clock();
    //The value at the boundary of physical domain is zero!
    Modification_LoadVector_CoeffMatrix(C, BasesType, coeff_matrix, load_vect, HTable);

    MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);



    MatSetOption(coeff_matrix, MAT_SYMMETRIC, PETSC_TRUE);

    t1=(double)clock();
    cout<<"\"Modification_LoadVector_CoeffMatrix\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    OfTime<<"\"Modification_LoadVector_CoeffMatrix\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    //
    VecCreate(PETSC_COMM_WORLD, &m_solut);
    VecSetSizes(m_solut, PETSC_DECIDE, 4*P_FEMMesh->P_Vertices->size());
    VecSetFromOptions(m_solut);

    //Solving the Linear System Part======================================================
    cout<<"solve the Linear system"<<endl;
    t0=(double)clock();
    KSP ksp;
    PC pc;
    KSPCreate(PETSC_COMM_WORLD, &ksp);
   // KSPSetType(ksp, KSPCG);
    KSPSetType(ksp, KSPGMRES);
    KSPSetOperators(ksp, coeff_matrix, coeff_matrix);
    //PetscReal rtol=1.e-18;
    //PetscReal stol=1.e-16;

    PetscReal rtol=(1.e-4)*pow(h,4.0);
    PetscReal stol=(1.e-3)*pow(h,4.0);

    KSPSetTolerances(ksp, rtol, stol, PETSC_DEFAULT, PETSC_DEFAULT);

    KSPGetPC(ksp, &pc);
    PCSetType(pc, PCGAMG);
    KSPSetFromOptions(ksp);

    KSPSolve(ksp, load_vect, m_solut);

    KSPGetConvergedReason(ksp,&reason);
    //printf
    PetscInt its;
    KSPGetIterationNumber(ksp, &its);
    PetscPrintf(PETSC_COMM_WORLD, "The number of iteration %D\n", its);

    t1=(double)clock();
    cout<<"\"Solving the linear system\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    cout<<"KSP ConvergedReason= "<<reason<<endl;

    cout<<"h = "<<h<<endl;
    cout<<"where, rtol=(1.e-4)*pow(h,4.0)="<<rtol<<" and  stol=(1.e-3)*pow(h,4.0)= "<<stol<<endl;

    OfTime<<endl;
    OfTime<<"\"Solving the linear system\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    OfTime<<"k = "<<k<<endl;
    OfTime<<"h = "<<h<<endl;
    OfTime<<"where, rtol=(1.e-4)*pow(h,4.0)="<<rtol<<" and  stol=(1.e-3)*pow(h,4.0)= "<<stol<<endl;

    OfTime<<"KSP ConvergedReason= "<<reason<<endl;
    OfTime<<"the number of iteration = "<<its<<endl;
    OfTime<<endl;
    //-------
    OfTime.close();


    //Print load_vect
    PrintLoadVector(load_vect);

    PrintSolution(m_solut);

    PrintAxelFile_FEMSolution(Coeff_Parametric_Map, P_ParametricMesh, k, m_solut);

//======L2=====
    vector<double> cell_errorsPhysDom;
    double total_ErrorPhysDom;

    FEML2PosteriorError_PDE_FEM(m_IntePt, w, m_solut, P_FEMMesh, P_ParametricMesh,
                       P_ParametricMap, ExampleIndex, EquationIndex, cell_errorsPhysDom, total_ErrorPhysDom);

    //Destroy ksp coeff_matrix load_vec m_solut
    KSPDestroy(&ksp);
    MatDestroy(&coeff_matrix);
    VecDestroy(&load_vect);
    VecDestroy(&m_solut);
}

//=====================================================
void BMatrix_Equation3(Mat &BMatrix, vector<double>& gausspt, vector<double>& gaussw,
                       unsigned int ExampleIndex, unsigned int EquationIndex,
                       const Mesh* P_FEMMesh, const Mesh* P_ParametricMesh,
                       MSplineFunctions *P_MSplineParametricMap, Vec &m_solut)
{
    //cout<<"Before BMatrix_Equation3: MatZeroEntries(BMatrix)"<<endl;
    MatZeroEntries(BMatrix);

    //cout<<"Before BMatrix_Equation3: Vec2Stdvector()"<<endl;
    vector<double> coeff;
    Vec2Stdvector(m_solut, coeff);

    MSplineFunction *P_Phi0= new MSplineFunction(coeff, *P_FEMMesh);

    int gaussnum = gausspt.size();

    PetscScalar v;
    PetscInt I, J;
    for(unsigned int i=0; i<P_FEMMesh->P_Cells->size(); i++)// Based on each cell
    {
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];
//=============================================================
        vector<double> funval, m_Baseval_k, m_Baseval_l;
        double tmpx;
        vector<vector<double> > m_JPart;
//==============================================================
        vector<Coordinate*> Pcoordinate_Ps;
        for (int l=0; l<gaussnum; l++)
        {
            for (int j=0; j<gaussnum; j++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=(x1+x0+(x1-x0)*gausspt[l]) * 0.5;//u[l];
                CoorP->xy[1]=(y0+y1+(y1-y0)*gausspt[j]) * 0.5;//v[j];
                Pcoordinate_Ps.push_back(CoorP);
            }
        }
//===============================================================
        ComputeFunval_MSplineParametricMap_PDE_PFEM(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent, funval, 4,
                                                    *P_ParametricMesh, P_MSplineParametricMap, ExampleIndex, EquationIndex);//The value of phi in the right side
//===============================================================

        BasesCell *P_BC=new BasesCell(i, *P_FEMMesh);//Bases over the i-th cell
        vector<double> DetJ;
        vector<double> BiValues;
        double k1=1.0, k2=1.0;//Here is non-business with k1, k1, in fact.
        ParametricMapSigma_MSplineParametricMap(Pcoordinate_Ps, m_JPart, DetJ, P_FEMMesh->P_Cells->at(i).ItsParent,
                                                *P_MSplineParametricMap, P_ParametricMesh, k1, k2, ExampleIndex);
        P_Phi0->Evaluation(0, 0, Pcoordinate_Ps, BiValues, i, *P_FEMMesh);

        for(unsigned int k=0; k< P_BC->P_Bases_Cell.size(); k++)
        {
            m_Baseval_k.clear();
            value_Cell_Hermite(0, 0, Pcoordinate_Ps, P_BC->P_Bases_Cell[k]->P_HermiteData_Cells[0], m_Baseval_k, *P_FEMMesh);

            I=P_BC->P_Bases_Cell[k]->Index_basis;
            for(unsigned int l=k; l< P_BC->P_Bases_Cell.size(); l++)
            {
                m_Baseval_l.clear();
                value_Cell_Hermite(0, 0, Pcoordinate_Ps, P_BC->P_Bases_Cell[l]->P_HermiteData_Cells[0], m_Baseval_l, *P_FEMMesh);

                tmpx=0.0;
                for(unsigned int j=0; j<m_Baseval_k.size(); j++)
                {
                    int nn = j/gaussnum;
                    int mm = j%gaussnum;

                    tmpx += gaussw[nn]*gaussw[mm]*funval[j]*m_Baseval_k[j]*m_Baseval_l[j]*DetJ[j]*BiValues[j];
                }

                J=P_BC->P_Bases_Cell[l]->Index_basis;

                v=tmpx * (x1-x0) * (y1-y0)/ 4;

                //cout<<"BMatrix_Equation3: Before MatSetValues()"<<endl;
                if(I==J)
                {
                    MatSetValues(BMatrix,1, &I, 1, &J, &v, ADD_VALUES);
                }
                else
                {
                    MatSetValues(BMatrix,1, &I, 1, &J, &v, ADD_VALUES);
                    MatSetValues(BMatrix,1, &J, 1, &I, &v, ADD_VALUES);
                }

            }
        }

    }
}
/* This function is used to solving Equation 3 of case 25.
 * InPut: OfTime -> used to record the solving progress
 *        epsilon-> used to control whether the iteration of solving can be stopped
 *        InitialCoeff-> the initial coefficients always set as zeros (the initial value of iteration)
 *        P_ParametricMesh-> the pointer of initial mesh
 *        Coeff_Parametric_Map->the coefficients of this parametric map based on the initial mesh
 *        ExampleIndex-> the index of our example
 *
 * OutPut: IsConvergence -> if IsConvergence is 1 (true), then this iteration step is convergence.
 *                           if IsConvergence is 0 (false), then this iteration step is not convergence.
 * RMK: How to determine "convergence" or not?
 *     Let b^i and b^{i+1} be the solutions at the i-th and (i+1)-th iteration steps.
 *     if ||b^i-b^{i+1}||<epsilon, IsConvergence=1;
 *     Otherwise, IsConvergence=0.
*/
void solveEquation3(ofstream &OfTime, double epsilon, Mesh* P_ParametricMesh,
                    vector<vector<double> >& Coeff_Parametric_Map, unsigned int ExampleIndex, bool& IsConvergence)
{
    Vec load_vect, m_solut;
    Vec m_soluti;
    Mat coeff_matrix;
    Mat coeff_matrix_copy;
    Mat BMatrix;
    KSP ksp;
    PC pc;
    KSPConvergedReason reason;

    unsigned int EquationIndex=3;

    //Generate the initial Parametric map
    MSplineFunctions * P_ParametricMap = new MSplineFunctions(Coeff_Parametric_Map, *P_ParametricMesh);

    cout<<"Input the times of subdivision without modifiying the parametic map!"<<endl;
    unsigned int k;//The time for subdiving
    cin>>k;

    map<unsigned int, unsigned int> OrigialVertexToCurrentVertex;
    Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(k, OrigialVertexToCurrentVertex);

    /* Define a size of cell*/
    double h=(P_FEMMesh->P_Cells->at(0)).CellSize[1]-(P_FEMMesh->P_Cells->at(0)).CellSize[0];

    //Rechoose epsilon----
    epsilon=0.1*pow(h, 4.0);

    cout<<"Generate Gauss points and Weights"<<endl;
    OfTime<<endl;
    OfTime<<"Generate Guass points and Weights"<<endl;
    double t0=(double)clock();

    //Preparation: GuassPoints
    cout<<"Please input the number of Guass Points:"<<endl;
    unsigned int Gp;
    cin>>Gp;
    unsigned int u_gaussnum=Gp, v_gaussnum=Gp;
    unsigned int load_gaussnum=Gp;
    vector<double> gausspt, m_IntePt, m_loadw;
    //Weights/////////
    double **w;
     w=GaussPoints_Weights(u_gaussnum, load_gaussnum, gausspt, m_IntePt, m_loadw);
    double t1=(double)clock();
    cout<<"\"Generate Guass points and Weights\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<endl;

    OfTime<<"\"Generate Guass points and Weights\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<endl;
    OfTime<<"u_gaussnum="<<u_gaussnum<<"  v_gaussnum="<<v_gaussnum<<endl;
    OfTime<<"The Basis Number = "<<4*(P_FEMMesh->P_Vertices->size())<<endl;

    cout<<"The Basis Number = "<<4*(P_FEMMesh->P_Vertices->size())<<endl;
    PetscInt NumVertex = P_FEMMesh->P_Vertices->size();

    cout<<"Initialized coeffMatrix and Generate_coeffMatrix"<<endl;

    OfTime<<endl;
    OfTime<<"Initialized coeffMatrix and Generate_coeffMatrix"<<endl;

    t0=(double)clock();

    MatCreate(PETSC_COMM_WORLD, &coeff_matrix);

    MatSetSizes(coeff_matrix, PETSC_DECIDE, PETSC_DECIDE, NumVertex*4, NumVertex*4);
    MatSetFromOptions(coeff_matrix);
    MatMPIAIJSetPreallocation(coeff_matrix, 64, NULL, 64, NULL);
    MatSeqAIJSetPreallocation(coeff_matrix, PETSC_DEFAULT, NULL);
    MatSetUp(coeff_matrix);


    cout<<"Generate_CoeffMatrix_Based on Cells"<<endl;
    double k1=1.0;
    double k2=1.0;//In fact, for solving these four equations, the KMatrix is non-business with k1, k2.
    Generate_CoeffMatrix_Cell(u_gaussnum, v_gaussnum, coeff_matrix, m_IntePt, m_IntePt, w, P_FEMMesh,
                              P_ParametricMesh, *P_ParametricMap, k1, k2, ExampleIndex);

    MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);

    t1=(double)clock();
    cout<<"\"Initialized coeffMatrix and Generate_coeffMatrix\" costs ="<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    OfTime<<"\"Initialized coeffMatrix and Generate_coeffMatrix\" costs ="<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    //**********************************

    //load_vect
    //vector<double> load_vect;
    cout<<"Initialized load Vector and Generate load Vector"<<endl;

    OfTime<<endl;
    OfTime<<"Initialized load Vector and Generate load Vector"<<endl;

    t0=(double)clock();
    Initialized_loadVector(load_vect, P_FEMMesh->P_Vertices->size());

    Generate_LoadVector_PDE_PFEM(load_vect, gausspt, m_loadw, P_FEMMesh, P_ParametricMesh,
                        *P_ParametricMap, k1, k2, ExampleIndex, EquationIndex);
    t1=(double)clock();

    cout<<"Finished and it costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    OfTime<<"Finished and it costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;



    //Set Bases type
    cout<<"Set Bases type"<<endl;
    t0=(double)clock();
    vector<unsigned int> BasesType;
    BasesType.resize(4*P_FEMMesh->P_Vertices->size(), 0);
    Set_Bases_type(P_FEMMesh, BasesType);

    t1=(double)clock();
    cout<<"\"Set Bases type\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    OfTime<<endl;
    OfTime<<"There are "<< 4*P_FEMMesh->P_Vertices->size()<<" bases"<<endl;
    OfTime<<"\"Set Bases type\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;


    map<unsigned int, set<unsigned int> > HTable;
    Initialized_HTable_withoutHangingVertices_1(P_FEMMesh, HTable);

    VecCreate(PETSC_COMM_WORLD, &m_solut);
    VecSetSizes(m_solut, PETSC_DECIDE, 4*P_FEMMesh->P_Vertices->size());
    VecSetFromOptions(m_solut);

    VecZeroEntries(m_solut);//The initial values of m_solut are zeros!

    PetscReal rtol=(1.e-4)*pow(h,4.0);
    PetscReal stol=(1.e-3)*pow(h,4.0);

    VecCreate(PETSC_COMM_WORLD, &m_soluti);
    VecSetSizes(m_soluti, PETSC_DECIDE, 4*P_FEMMesh->P_Vertices->size());
    VecSetFromOptions(m_soluti);

    KSPCreate(PETSC_COMM_WORLD, &ksp);

    MatCreate(PETSC_COMM_WORLD, &BMatrix);
    MatSetSizes(BMatrix, PETSC_DECIDE, PETSC_DECIDE, NumVertex*4, NumVertex*4);
    MatSetFromOptions(BMatrix);
    MatMPIAIJSetPreallocation(BMatrix, 64, NULL, 64, NULL);
    MatSeqAIJSetPreallocation(BMatrix, PETSC_DEFAULT, NULL);
    MatSetUp(BMatrix);

    MatAssemblyBegin(BMatrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(BMatrix, MAT_FINAL_ASSEMBLY);


    cout<<"begin to iterate"<<endl;

    PetscReal val=1.0;
    //Iteration:
    unsigned int IterationNum=0;
    while(IterationNum<100 && IsConvergence==false && val>1.0e-15)
    {

        //cout<<"Before MatAXPY: coeff_matrix_copy"<<endl;

        MatDuplicate(coeff_matrix, MAT_COPY_VALUES, &coeff_matrix_copy);

        MatAssemblyBegin(coeff_matrix_copy, MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd(coeff_matrix_copy, MAT_FINAL_ASSEMBLY);

        //cout<<"Before BMatrix_Equation3"<<endl;

        //MatView(BMatrix, PETSC_VIEWER_STDOUT_SELF);
        BMatrix_Equation3(BMatrix, gausspt, m_loadw, ExampleIndex, EquationIndex,
                          P_FEMMesh, P_ParametricMesh, P_ParametricMap, m_solut);

        MatAssemblyBegin(BMatrix, MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd(BMatrix, MAT_FINAL_ASSEMBLY);

        //cout<<"Before MatAXPY: BMatrix"<<endl;
        MatAXPY(coeff_matrix_copy, -1.0, BMatrix, DIFFERENT_NONZERO_PATTERN);
        //    //**********************************

        MatAssemblyBegin(coeff_matrix_copy, MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd(coeff_matrix_copy, MAT_FINAL_ASSEMBLY);

        //Modify LoadVector and CoeffMatrix
        double C=0;//The value at the boundary of physical domain is zero

        //"Modification_LoadVector_CoeffMatrix"
        //The value at the boundary of physical domain is zero!
        Modification_LoadVector_CoeffMatrix(C, BasesType, coeff_matrix_copy, load_vect, HTable);
        MatAssemblyBegin(coeff_matrix_copy, MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd(coeff_matrix_copy, MAT_FINAL_ASSEMBLY);
        MatSetOption(coeff_matrix_copy, MAT_SYMMETRIC, PETSC_TRUE);

       //Solving the Linear System Part======================================================

        // KSPSetType(ksp, KSPCG);
        KSPSetType(ksp, KSPGMRES);
        KSPSetOperators(ksp, coeff_matrix_copy, coeff_matrix_copy);

        KSPSetTolerances(ksp, rtol, stol, PETSC_DEFAULT, PETSC_DEFAULT);
        KSPGetPC(ksp, &pc);
        PCSetType(pc, PCGAMG);
        KSPSetFromOptions(ksp);
        KSPSolve(ksp, load_vect, m_soluti);

        KSPGetConvergedReason(ksp,&reason);



        //m_solut=m_solut-m_soluti
        VecAXPY(m_solut,-1.0, m_soluti);
        VecNorm(m_solut, NORM_2, &val);

        cout<<"the norm of error val = "<<val<<endl;
        OfTime<<"the norm of error val = "<<val<<endl;
        cout<<"KSPConvergeReason= "<<reason<<endl;
        OfTime<<"KSPConvergeReason= "<<reason<<endl;
        if(val<epsilon)
        {
            IsConvergence=true;
        }

        //m_solut=m_soluti
        VecCopy(m_soluti, m_solut);

        VecAssemblyBegin(m_solut);
        VecAssemblyEnd(m_solut);

        IterationNum = IterationNum + 1;
    }

    OfTime<<"h = "<<h<<endl;
    OfTime<<"where, rtol=(1.e-4)*pow(h,4.0)="<<rtol<<" and  stol=(1.e-3)*pow(h,4.0)= "<<stol<<endl;

    OfTime<<"In iteration, epsilon = "<<epsilon<<endl;
    cout<<endl;
    cout<<"In iteration, epsilon = "<<epsilon<<endl;

    OfTime<<"IterationConvergence = "<<IsConvergence<<endl;
    OfTime<<"The number of iteration = "<<IterationNum<<endl;

    cout<<"IterationConvergence = "<<IsConvergence<<endl;
    cout<<"The number of iteration = "<<IterationNum<<endl;
    cout<<"h = "<<h<<endl;
    cout<<"where, rtol=(1.e-4)*pow(h,4.0)="<<rtol<<" and  stol=(1.e-3)*pow(h,4.0)= "<<stol<<endl;
    //-------
    OfTime.close();


    //Print load_vect
    PrintLoadVector(load_vect);

    PrintSolution(m_solut);

    PrintAxelFile_FEMSolution(Coeff_Parametric_Map, P_ParametricMesh, k, m_solut);


//======L2=====
    vector<double> cell_errorsPhysDom;
    double total_ErrorPhysDom;

    FEML2PosteriorError_PDE_FEM(m_IntePt,w, m_solut, P_FEMMesh, P_ParametricMesh,
                        P_ParametricMap, ExampleIndex, EquationIndex, cell_errorsPhysDom, total_ErrorPhysDom);

    //Destroy ksp coeff_matrix load_vec m_solut
    KSPDestroy(&ksp);
    MatDestroy(&coeff_matrix);
    MatDestroy(&coeff_matrix_copy);
    VecDestroy(&load_vect);
    VecDestroy(&m_solut);
    VecDestroy(&m_soluti);
}
/* solveEquation4:
*/
void solveEquation4(ofstream &OfTime, vector<PointSet>& XPoint, vector<PointSet>& CenterPoint , Mesh* P_ParametricMesh,
                    vector<vector<double> >& Coeff_Parametric_Map, unsigned int ExampleIndex, bool& IsConvergence)
{
    Vec load_vect, m_solut;
    Vec load_vect_Iter;
    Vec load_vect_copy;
    Vec m_soluti;

    Mat coeff_matrix;
    Mat coeff_matrix_copy;
    Mat BMatrix2;
    Mat BMatrix3;

    KSP ksp;
    PC pc;
    //KSPConvergedReason reason;

    unsigned int EquationIndex=4;

    cout<<"Input the times of subdivision without modifiying the parametic map!"<<endl;
    unsigned int k;
    cin>>k;


    //Generate the initial Parametric map
    MSplineFunctions * P_ParametricMap = new MSplineFunctions(Coeff_Parametric_Map, *P_ParametricMesh);


    map<unsigned int, unsigned int> OrigialVertexToCurrentVertex;
    Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(k, OrigialVertexToCurrentVertex);

    /* Define a size of cell*/
    double h=(P_FEMMesh->P_Cells->at(0)).CellSize[1]-(P_FEMMesh->P_Cells->at(0)).CellSize[0];
    double epsilon=pow(h,4.0);

    cout<<"Generate Gauss points and Weights"<<endl;
    OfTime<<endl;
    OfTime<<"Generate Guass points and Weights"<<endl;
    double t0=(double)clock();

    //Preparation: GuassPoints
    cout<<"Please input the number of Guass Points:"<<endl;
    unsigned int Gp;
    cin>>Gp;
    unsigned int u_gaussnum=Gp, v_gaussnum=Gp;
    unsigned int load_gaussnum=Gp;
    vector<double> gausspt, m_IntePt, m_loadw;
    //Weights/////////
    double **w;
     w=GaussPoints_Weights(u_gaussnum, load_gaussnum, gausspt, m_IntePt, m_loadw);
    double t1=(double)clock();
    cout<<"\"Generate Guass points and Weights\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<endl;

    OfTime<<"\"Generate Guass points and Weights\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<endl;
    OfTime<<"u_gaussnum="<<u_gaussnum<<"  v_gaussnum="<<v_gaussnum<<endl;
    OfTime<<"The Basis Number = "<<4*(P_FEMMesh->P_Vertices->size())<<endl;

    cout<<"The Basis Number = "<<4*(P_FEMMesh->P_Vertices->size())<<endl;
    PetscInt NumVertex = P_FEMMesh->P_Vertices->size();



    MatCreate(PETSC_COMM_WORLD, &coeff_matrix);

    MatSetSizes(coeff_matrix, PETSC_DECIDE, PETSC_DECIDE, NumVertex*4, NumVertex*4);
    MatSetFromOptions(coeff_matrix);
    MatMPIAIJSetPreallocation(coeff_matrix, 64, NULL, 64, NULL);
    MatSeqAIJSetPreallocation(coeff_matrix, PETSC_DEFAULT, NULL);
    MatSetUp(coeff_matrix);


    cout<<"Generate_CoeffMatrix_Based on Cells"<<endl;
    double k1=1.0;
    double k2=1.0;//In fact, for solving these four equations, the KMatrix is non-business with k1, k2.

    //coeff_matrix
    Generate_CoeffMatrix_Cell(u_gaussnum, v_gaussnum, coeff_matrix, m_IntePt, m_IntePt, w, P_FEMMesh,
                              P_ParametricMesh, *P_ParametricMap, k1, k2, ExampleIndex);

    MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);
    //**********************************

    //load_vect
    cout<<"Initialized load Vector and Generate load Vector"<<endl;
    Initialized_loadVector(load_vect, P_FEMMesh->P_Vertices->size());
    EquationIndex=4;
    Generate_LoadVector_PDE_PFEM(load_vect, gausspt, m_loadw, P_FEMMesh, P_ParametricMesh,
                        *P_ParametricMap, k1, k2, ExampleIndex, EquationIndex);

    //load_vect_Iter
    cout<<"Initialized load_vect_Iter and Generate load_vect_Iter"<<endl;
    Initialized_loadVector(load_vect_Iter,P_FEMMesh->P_Vertices->size());
    unsigned int EquationIndex_2=42;//4_2 for making a difference from EquationIndex
    Generate_LoadVector_PDE_PFEM(load_vect_Iter, gausspt, m_loadw, P_FEMMesh, P_ParametricMesh,
                                 *P_ParametricMap, k1, k2, ExampleIndex, EquationIndex_2);
    //load_vect_copy
    cout<<"Initialized load_vect_copy"<<endl;
    Initialized_loadVector(load_vect_copy,P_FEMMesh->P_Vertices->size());

    //Set Bases type
    cout<<"Set Bases type"<<endl;
    t0=(double)clock();
    vector<unsigned int> BasesType;
    BasesType.resize(4*P_FEMMesh->P_Vertices->size(), 0);
    Set_Bases_type(P_FEMMesh, BasesType);

    t1=(double)clock();
    cout<<"\"Set Bases type\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    OfTime<<endl;
    OfTime<<"There are "<< 4*P_FEMMesh->P_Vertices->size()<<" bases"<<endl;
    OfTime<<"\"Set Bases type\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;


    map<unsigned int, set<unsigned int> > HTable;
    Initialized_HTable_withoutHangingVertices_1(P_FEMMesh, HTable);

    VecCreate(PETSC_COMM_WORLD, &m_solut);
    VecSetSizes(m_solut, PETSC_DECIDE, 4*P_FEMMesh->P_Vertices->size());
    VecSetFromOptions(m_solut);

    //The initial values of m_solut are zeros!
    cout<<"The initial values of m_solut are zeros!"<<endl;
    OfTime<<"The initial values of m_solut are zeros!"<<endl;
    VecZeroEntries(m_solut);

    PetscReal rtol=(1.e-4)*pow(h, 4.0);
    PetscReal stol=(1.e-3)*pow(h, 4.0);


    VecCreate(PETSC_COMM_WORLD, &m_soluti);
    VecSetSizes(m_soluti, PETSC_DECIDE, 4*P_FEMMesh->P_Vertices->size());
    VecSetFromOptions(m_soluti);

    KSPCreate(PETSC_COMM_WORLD, &ksp);


    cout<<"BMatrix2: the fix part"<<endl;
    MatCreate(PETSC_COMM_WORLD, &BMatrix2);
    MatSetSizes(BMatrix2, PETSC_DECIDE, PETSC_DECIDE, NumVertex*4, NumVertex*4);
    MatSetFromOptions(BMatrix2);
    MatMPIAIJSetPreallocation(BMatrix2, 64, NULL, 64, NULL);
    MatSeqAIJSetPreallocation(BMatrix2, PETSC_DEFAULT, NULL);
    MatSetUp(BMatrix2);
    MatAssemblyBegin(BMatrix2, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(BMatrix2, MAT_FINAL_ASSEMBLY);

    unsigned int EquationIndex_3=421;
    BMatrix_Equation2(BMatrix2, gausspt, m_loadw, ExampleIndex, EquationIndex_3,P_FEMMesh, P_ParametricMesh,P_ParametricMap);

    MatAssemblyBegin(BMatrix2, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(BMatrix2, MAT_FINAL_ASSEMBLY);


    cout<<"BMatrix3: the fix part"<<endl;
    MatCreate(PETSC_COMM_WORLD, &BMatrix3);
    MatSetSizes(BMatrix3, PETSC_DECIDE, PETSC_DECIDE, NumVertex*4, NumVertex*4);
    MatSetFromOptions(BMatrix3);
    MatMPIAIJSetPreallocation(BMatrix3, 64, NULL, 64, NULL);
    MatSeqAIJSetPreallocation(BMatrix3, PETSC_DEFAULT, NULL);
    MatSetUp(BMatrix3);

    MatAssemblyBegin(BMatrix3, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(BMatrix3, MAT_FINAL_ASSEMBLY);

    //==========================
    double Para_u, Para_v;
    Para_u=XPoint[0].P_coordinates_Ps[0]->xy[0];
    Para_v=XPoint[0].P_coordinates_Ps[0]->xy[1];
    unsigned int OriginalCellIndex=XPoint[0].Index_ItsCell;
    P_FEMMesh->NewIndexOfCellOnRefinedMesh(P_ParametricMesh, Para_u, Para_v, OriginalCellIndex, XPoint[0].Index_ItsCell);

    Para_u=CenterPoint[0].P_coordinates_Ps[0]->xy[0];
    Para_v=CenterPoint[0].P_coordinates_Ps[0]->xy[1];
    OriginalCellIndex=CenterPoint[0].Index_ItsCell;
    P_FEMMesh->NewIndexOfCellOnRefinedMesh(P_ParametricMesh, Para_u, Para_v, OriginalCellIndex, CenterPoint[0].Index_ItsCell);
    //==========================

    cout<<"begin to iterate"<<endl;

    PetscReal val=1.0;
    //Iteration:
    unsigned int IterationNum=0;
    double ValueXP, ValueCP;

    while(IterationNum<100 && IsConvergence==false && val>1.0e-15)
    {

        vector<double> coeff;
        Vec2Stdvector(m_solut, coeff);
        MSplineFunction *P_Phi0 = new MSplineFunction(coeff, *P_FEMMesh);

        vector<vector<double> > Values;
        P_Phi0->Evaluation(0, 0, XPoint, Values, *P_FEMMesh);
        ValueXP=Values[0][0];

        Values.clear();
        P_Phi0->Evaluation(0, 0, CenterPoint, Values, *P_FEMMesh);
        ValueCP=Values[0][0];

        //In order to avoid ValueCP=ValueXP
        if(ValueCP-ValueXP<1.0e-5 && ValueCP-ValueXP>-1.0e-5)
        {
            ValueXP=ValueCP+1.0;// such that: |ValueXP-ValueCP|=1
        }

        //coeff_matrix_copy=coeff_matrix
        MatDuplicate(coeff_matrix, MAT_COPY_VALUES, &coeff_matrix_copy);

        MatAssemblyBegin(coeff_matrix_copy, MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd(coeff_matrix_copy, MAT_FINAL_ASSEMBLY);


        //coeff_matrix_copy=coeff_matrix_copy - BMatrix2*coeffient
        MatAXPY(coeff_matrix_copy, -ValueCP/((ValueXP-ValueCP)*(ValueXP-ValueCP)), BMatrix2, DIFFERENT_NONZERO_PATTERN);


        //MatView(BMatrix, PETSC_VIEWER_STDOUT_SELF);
        //The initial of m_solut is a zero vector
        unsigned int EquationIndex_4=422;
        BMatrix_Equation3(BMatrix3, gausspt, m_loadw, ExampleIndex, EquationIndex_4,
                          P_FEMMesh, P_ParametricMesh, P_ParametricMap, m_solut);

        MatAssemblyBegin(BMatrix3, MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd(BMatrix3, MAT_FINAL_ASSEMBLY);

        //cout<<"Before MatAXPY: BMatrix"<<endl;
        //coeff_matrix_copy=coeff_matrix_copy - BMatrix3*coeffient
        MatAXPY(coeff_matrix_copy, -1.0/((ValueXP-ValueCP)*(ValueXP-ValueCP)), BMatrix3, DIFFERENT_NONZERO_PATTERN);

        MatAssemblyBegin(coeff_matrix_copy, MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd(coeff_matrix_copy, MAT_FINAL_ASSEMBLY);

        //load_vect_copy
        VecCopy(load_vect, load_vect_copy);//load_vect_copy=load_vect

        //load_vect_copy=load_vect_copy+coeffient*load_vect_Iter
        VecAXPY(load_vect_copy, ValueCP*ValueCP/((ValueXP-ValueCP)*(ValueXP-ValueCP)), load_vect_Iter);

        VecAssemblyBegin(load_vect_copy);
        VecAssemblyEnd(load_vect_copy);

        //Modify LoadVector and CoeffMatrix
        double C=0;//The value at the boundary of physical domain is zero

        //"Modification_LoadVector_CoeffMatrix"
        //The value at the boundary of physical domain is zero!
        Modification_LoadVector_CoeffMatrix(C, BasesType, coeff_matrix_copy, load_vect_copy, HTable);
        MatAssemblyBegin(coeff_matrix_copy, MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd(coeff_matrix_copy, MAT_FINAL_ASSEMBLY);
        MatSetOption(coeff_matrix_copy, MAT_SYMMETRIC, PETSC_TRUE);

       //Solving the Linear System Part======================================================

        // KSPSetType(ksp, KSPCG);
        KSPSetType(ksp, KSPGMRES);
        KSPSetOperators(ksp, coeff_matrix_copy, coeff_matrix_copy);

        KSPSetTolerances(ksp, rtol, stol, PETSC_DEFAULT, PETSC_DEFAULT);
        KSPGetPC(ksp, &pc);
        PCSetType(pc, PCGAMG);
        KSPSetFromOptions(ksp);
        KSPSolve(ksp, load_vect_copy, m_soluti);

//        KSPGetConvergedReason(ksp,&reason);

        //m_solut=m_solut-m_soluti
        VecAXPY(m_solut,-1.0, m_soluti);
        VecNorm(m_solut, NORM_2, &val);

        cout<<"the norm of error val = "<<val<<endl;
        OfTime<<"the norm of error val = "<<val<<endl;
        if(val<epsilon)
        {
            IsConvergence=true;
        }

        //m_solut=m_soluti
        VecCopy(m_soluti, m_solut);

        VecAssemblyBegin(m_solut);
        VecAssemblyEnd(m_solut);

        IterationNum = IterationNum + 1;
    }

    OfTime<<"h = "<<h<<endl;
    OfTime<<"where, rtol=(1.e-4)*pow("<<h<<" ,4.0)="<<rtol<<" and  stol=(1.e-3)*pow("<< h <<",4.0)= "<<stol<<endl;

    OfTime<<"In iteration, epsilon = "<<epsilon<<endl;
    cout<<"In iteration, epsilon = "<<epsilon<<endl;

    OfTime<<"IsConvergence = "<<IsConvergence<<endl;
    OfTime<<"The number of iteration = "<<IterationNum<<endl;

    cout<<"IsConvergence = "<<IsConvergence<<endl;
    cout<<"The number of iteration = "<<IterationNum<<endl;
    //-------
    OfTime.close();


    //Print load_vect
    PrintLoadVector(load_vect);

    PrintSolution(m_solut);

    PrintAxelFile_FEMSolution(Coeff_Parametric_Map, P_ParametricMesh, k, m_solut);


//======L2=====
    vector<double> cell_errorsPhysDom;
    double total_ErrorPhysDom;

    FEML2PosteriorError_PDE_FEM(m_IntePt,w, m_solut, P_FEMMesh, P_ParametricMesh,
                        P_ParametricMap, ExampleIndex, EquationIndex, cell_errorsPhysDom, total_ErrorPhysDom);

    //Destroy ksp coeff_matrix load_vec m_solut
    KSPDestroy(&ksp);
    MatDestroy(&coeff_matrix);
    MatDestroy(&coeff_matrix_copy);
    MatDestroy(&BMatrix2);
    MatDestroy(&BMatrix3);

    VecDestroy(&load_vect);
    VecDestroy(&m_solut);
    VecDestroy(&m_soluti);
    VecDestroy(&load_vect_copy);
    VecDestroy(&load_vect_Iter);

}


void  GetXPointCenterPoint(unsigned int ExampleIndex, Mesh * P_FEMMesh, Mesh *P_ParametricMesh, vector<PointSet>& XPoint, vector<PointSet>& CenterPoint)
{
    XPoint.clear();
    CenterPoint.clear();

    switch(ExampleIndex)
    {
    case 33:
    {
        Coordinate* P_XPoint=new Coordinate();
        P_XPoint->xy[0]=1.0;
        P_XPoint->xy[1]=1.0;//CellIndex=1

        Coordinate* P_Center=new Coordinate();
        P_Center->xy[0]=0.5;
        P_Center->xy[1]=0.5;//CellIndex=12


        unsigned int Index_ItsCell_Initial;
        double para_u, para_v;
        PointSet XP, CP;
        Index_ItsCell_Initial=1;
        para_u=1.0;
        para_v=1.0;
        //-------------------
        P_FEMMesh->NewIndexOfCellOnRefinedMesh(P_ParametricMesh, para_u, para_v, Index_ItsCell_Initial, XP.Index_ItsCell);
        //-------------------
        XP.P_coordinates_Ps.push_back(P_XPoint);
        XPoint.push_back(XP);

        Index_ItsCell_Initial=12;
        para_u=0.5;
        para_v=0.5;
        //--------------------
        P_FEMMesh->NewIndexOfCellOnRefinedMesh(P_ParametricMesh, para_u, para_v, Index_ItsCell_Initial, CP.Index_ItsCell);
        //--------------------
        CP.P_coordinates_Ps.push_back(P_Center);
        CenterPoint.push_back(CP);
    }
        break;

    case 34:
    {
        Coordinate* P_XPoint=new Coordinate();
        P_XPoint->xy[0]=1.0;
        P_XPoint->xy[1]=0.0;

        Coordinate* P_Center=new Coordinate();
        P_Center->xy[0]=0.0;
        P_Center->xy[1]=0.0;

        unsigned int Index_ItsCell_Initial;
        double para_u, para_v;
        PointSet XP, CP;
        Index_ItsCell_Initial=0;
        para_u=1.0;
        para_v=0.0;
        //-------------------
        P_FEMMesh->NewIndexOfCellOnRefinedMesh(P_ParametricMesh, para_u, para_v, Index_ItsCell_Initial, XP.Index_ItsCell);
        //-------------------
        XP.P_coordinates_Ps.push_back(P_XPoint);
        XPoint.push_back(XP);

        Index_ItsCell_Initial=0;
        para_u=0.0;
        para_v=0.0;
        //--------------------
        P_FEMMesh->NewIndexOfCellOnRefinedMesh(P_ParametricMesh, para_u, para_v, Index_ItsCell_Initial, CP.Index_ItsCell);
        //--------------------
        CP.P_coordinates_Ps.push_back(P_Center);
        CenterPoint.push_back(CP);
    }
        break;
    }
}

/*---------------------------------------------*/
int main(int argc,char **args)
{
    /*Initalize PETSc*/
    PetscInitialize(&argc, &args, (char*)0, NULL);

    ofstream OfTime;
    OfTime.open("Time.txt");
    //================================================
    //Read the ParametricMap
    //Mesh
    cout<<"Read the mesh, subdivide it and parametric map"<<endl;

    //----------
    OfTime<<endl;
    OfTime<<"Read the mesh, subdivide it and parametric map"<<endl;

    Mesh * P_ParametricMesh=new Mesh;
    //Coeff
    vector<vector<double> > Coeff_Parametric_Map;

    cout<<"The following examples can be chosen:"<<endl;

    cout<<"ExampleIndex 32: PDE_PFEM_2: Example7_1:alpha shape with the boundary determined by y^2-x(x-1)^2=2"<<endl;
    cout<<"ExampleIndex 33: Example7_0:alpha shape with the boundary determined by y^2-x(x-1)^2=2 and x=2y^2+1.5"<<endl;
    cout<<"ExampleIndex 34: The same as the case with ExampleIndex as 4:"<<endl;
    cout<<"ExampleIndex 35: Example7_0:alpha shape with the boundary determined by y^2-x(x-1)^2=2 and x=2y^2+1.5"<<endl;

    OfTime<<"The following examples can be chosen:"<<endl;

    OfTime<<"ExampleIndex 32: PDE_PFEM_2: Example7_1:alpha shape with the boundary determined by y^2-x(x-1)^2=2"<<endl;
    OfTime<<"ExampleIndex 33: Example7_0:alpha shape with the boundary determined by y^2-x(x-1)^2=2 and x=2y^2+1.5"<<endl;
    OfTime<<"ExampleIndex 34: The same as the case with ExampleIndex as 4:"<<endl;
    OfTime<<"ExampleIndex 35: Example7_0:alpha shape with the boundary determined by y^2-x(x-1)^2=2 and x=2y^2+1.5"<<endl;

    unsigned int ExampleIndex;
    cout<<"Please put in the index of this example"<<endl;
    cin>>ExampleIndex;

    cout<<"Remark:----------------------------------"<<endl;
    cout<<"In this application (PDE_PFEM_2), ExampleIndex="<<ExampleIndex<<endl;

    OfTime<<endl;
    OfTime<<"In this application (PDE_PFEM_2), ExampleIndex="<<ExampleIndex<<endl;

    unsigned int dimension=2;
    unsigned int VerNum=0;

    switch(ExampleIndex)
    {

    case 32://PDE_PFEM_2: Example7_1:alpha shape with the boundary determined by y^2-x(x-1)^2=2

        //RMK: For this cases, we need to modify the boundary of this alpha shape domain, when subdiving the mesh.
        //Read the initial mesh form /msplinetools/data/Examples/Example7_1/
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/Example7_1/2/ParametricMesh.txt");

        //Read the coefficients
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/Example7_1/2/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;


    case 33://PDE_PFEM_2: Example7_0:alpha shape with the boundary determined by y^2-x(x-1)^2=2 and x=2y^2+1.5

        //RMK: For this cases, we need to modify the boundary of this alpha shape domain, when subdiving the mesh.
        //Read the initial mesh form /msplinetools/data/Examples/Example7_0/
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/Example7_0/ParametricMesh.txt");

        //Read the coefficients
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/Example7_0/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 34://The same as case 4:
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/Example4/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/Example4/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 35://PDE_PFEM_2: Example7_0:alpha shape with the boundary determined by y^2-x(x-1)^2=2 and x=2y^2+1.5
        //We solve an elliptic equation on this alpha shape physical domain in order to check that why the solover doesn't converge
        //Just solve Equation 1 with K=IdentityMatrix

        //RMK: For this cases, we need to modify the boundary of this alpha shape domain, when subdiving the mesh.
        //Read the initial mesh form /msplinetools/data/Examples/Example7_0/
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/Example7_0/4/ParametricMesh.txt");

        //Read the coefficients
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/Example7_0/4/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

//    The max case num is 45 in mspSpringParameterization:

    default:
        cout<<"Read Meshs and Coeffs: Impossibile!"<<endl;
        cin.get();
        break;
    }


    //P_ParametricMesh->Cout_Mesh_File();


    //

    //PrintParametricMap(Coeff_Parametric_Map, P_ParametricMesh, "InitialParametricMap.axl");
    //Generate the initial Parametric map
    MSplineFunctions * P_ParametricMap = new MSplineFunctions(Coeff_Parametric_Map, *P_ParametricMesh);

//    MSplineFunctions * P_ParametricMap2=P_ParametricMap->SetInitialVerticesPositions(P_ParametricMesh, Coeff_Parametric_Map);
//    delete P_ParametricMap;
//    P_ParametricMap=P_ParametricMap2;
//    P_ParametricMap2=NULL;

    vector<vector<double> >().swap(Coeff_Parametric_Map);//Coeff_Parametric_Map.clear();
   // P_ParametricMap->CoutThisMSplineFunctionsAsBSplineSurfaceAxelFile(P_ParametricMesh);

    //Read the set of edges of *P_ParametricMesh
    vector<pair<unsigned int, unsigned int> > InitialEdges;
    vector<unsigned int> EdgesIndexofCells;
    ReadTheEdges(ExampleIndex, InitialEdges, EdgesIndexofCells);



    //Input k for subdividing:
    cout<<"Input the times of subdivision"<<endl;
    unsigned int k;
    cin>>k;

    //Generate Ps and Qs-----------------------
    unsigned int pho;
    cout<<"Put in the density of points between two adjacent vertices"<<endl;
    cin>>pho;
    //---
    OfTime<<"Put in the density of points between two adjacent vertices"<<endl;
    OfTime<<"pho="<<pho<<endl;

    //==========================================
    vector<PointSet> Ps;
    vector<vector<Coordinate *> > Qs;
    unsigned int NumPEdge=(k+1)*pho+(k+2);

    for(unsigned int IndexEdge=0; IndexEdge < InitialEdges.size(); IndexEdge++)
    {
        PointSet Psi;
        vector<Coordinate *> Qsi;
        GetPsQsOverPhysicalDomain(ExampleIndex, IndexEdge, NumPEdge, P_ParametricMap,P_ParametricMesh,
                                  InitialEdges, EdgesIndexofCells, Psi, Qsi);

        Ps.push_back(Psi);
        Qs.push_back(Qsi);
    }


    map<unsigned int, unsigned int> OrigialVertexToCurrentVertex;

    map<unsigned int, pair<unsigned int ,Coordinate*> > CurrentVerticesIndexToCoordinate;

    vector<vector<unsigned int> > CurrentVerticesEdges;
    //CurrentVertices[i] is the set of vertex indices which are on InitialEdges[i]
    Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally_Vertex(k,InitialEdges,EdgesIndexofCells,
                                      OrigialVertexToCurrentVertex, CurrentVerticesEdges, CurrentVerticesIndexToCoordinate);

    //cout<<"P_FEMMesh:"<<endl;
    //P_FEMMesh->Cout_Mesh_File();

    //InitialEdges.clear();
    vector<pair<unsigned int, unsigned int> >().swap(InitialEdges);
    //EdgesIndexofCells.clear();
    vector<unsigned int>().swap(EdgesIndexofCells);
    //OrigialVertexToCurrentVertex.clear();
    map<unsigned int, unsigned int>().swap(OrigialVertexToCurrentVertex);


    MSplineFunctions * P_RefinedParametricMap;
    vector<vector<double> > CoeffRefinedParametricMap;

    //cout<<"Before ReExpressionParametrization()"<<endl;
    P_RefinedParametricMap=ReExpressionParametrization(k, pho, P_ParametricMap,
                           CurrentVerticesIndexToCoordinate, CurrentVerticesEdges, Qs,
                           P_FEMMesh, P_ParametricMesh, CoeffRefinedParametricMap);


    delete P_ParametricMap;
    P_ParametricMap=NULL;



    //map<unsigned int, pair<unsigned int ,Coordinate*> > CurrentVerticesIndexToCoordinate.clear();
    for(map<unsigned int, pair<unsigned int ,Coordinate*> >::iterator itClear=CurrentVerticesIndexToCoordinate.begin();
        itClear!=CurrentVerticesIndexToCoordinate.end(); itClear++)
    {
        delete (itClear->second).second;
        (itClear->second).second=NULL;
    }
    map<unsigned int, pair<unsigned int, Coordinate*> >().swap(CurrentVerticesIndexToCoordinate);

    vector<PointSet> ModifiedPs;
    vector< vector<Coordinate *> > ModifiedQs;
   // cout<<"Test:ModifyRefinedParametrization"<<endl;

    //PetscInitialize(&argc, &args, (char*)0, NULL);

    ModifyRefinedParametrization(argc, args, k, pho, CurrentVerticesEdges,
                                 P_FEMMesh, CoeffRefinedParametricMap, Ps, Qs,
                                 ModifiedPs, ModifiedQs, P_RefinedParametricMap);

//    cout<<"ModifiedPs.clear()"<<endl;
//    for(unsigned int iClear=0; iClear<ModifiedPs.size(); iClear++)
//    {
//        ModifiedPs[iClear].DeleteClear();
//    }
//    vector<PointSet>().swap(ModifiedPs);

    cout<<"ModifiedQs.clear()"<<endl;
    for(unsigned int iClear=0; iClear<ModifiedQs.size(); iClear++)
    {
        for(unsigned int jClear=0; jClear<ModifiedQs[iClear].size(); jClear++)
        {
            delete ModifiedQs[iClear][jClear];
            ModifiedQs[iClear][jClear]=NULL;
        }
    }
    vector<vector<Coordinate *> >().swap(ModifiedQs);

    cout<<"Ps.clear()"<<endl;
    for(unsigned int iClear=0; iClear<Ps.size(); iClear++)
    {
        Ps[iClear].DeleteClear();
    }
    vector<PointSet>().swap(Ps);

    cout<<"Qs.clear()"<<endl;
    for(unsigned int iClear=0; iClear<Qs.size(); iClear++)
    {
        for(unsigned int jClear=0; jClear<Qs[iClear].size(); jClear++)
        {
            delete Qs[iClear][jClear];
            Qs[iClear][jClear]=NULL;
        }
    }
    vector<vector<Coordinate *> >().swap(Qs);

    cout<<"CurrentVerticesEdges.clear()"<<endl;
    vector<vector<unsigned int> > ().swap(CurrentVerticesEdges);


    if(ExampleIndex==32)//if ExampleIndex=32, reset the position of vertices of P_RefinedParametricMap
    {
        //The area is bounded by the following equations
        /* 1. y^2-x(x-1)^2=2
         * 2. x=2
         * 3. (x-2)^2+y^2=(0.5)^2
         *************************
         * i.e.,
         * F1=(x-2)^2+y^2-1/4>0
         * F2=x-2<0
         * F3=y^2-x(x-1)^2-2<0
        */

        /*------Set dl-----------------------*/
        double dl;//=0.001;//0.0001;//0.7/(1.0*k);
        cout<<"inPut dl"<<endl;
        cin>>dl;

        OfTime<<"dl = "<<dl<<endl;

        double mdl;
        cout<<"Input mdl==>dl1=mdl*dl"<<endl;
        cin>>mdl;
        OfTime<<"mdl = "<<mdl<<" such that dl1=mdl*dl"<<endl;

        for(unsigned int i=0; i<P_FEMMesh->P_Vertices->size(); i++)
        {
            if(P_FEMMesh->P_Vertices->at(i).Is_interiorVertex)
            {
                unsigned int IndexV=4*i;
                double x = CoeffRefinedParametricMap[0][IndexV];
                double y = CoeffRefinedParametricMap[1][IndexV];

//                /*------Set dl-----------------------*/
//                double dl;//=0.001;//0.0001;//0.7/(1.0*k);
//                cout<<"inPut dl"<<endl;
//                cin>>dl;

//                OfTime<<"dl = "<<dl<<endl;
                /*------------------------------------*/

                double F1=(x-2.0)*(x-2.0)+y*y-0.25-dl;//(x-2.0)*(x-2.0)+y*y-0.25;
                double F2=x-2.0+dl;//1.95;//x-2.0
                double F3=y*y-x*(x-1.0)*(x-1.0)-2.0+dl;//1.95;//y*y-x*(x-1.0)*(x-1.0)-2.0;

                if(F1<0||F2>0||F3>0)//this interior vertex isn't in the physical domain
                {
                    //Set as (1.5, 1), (1.5, -1), (0, 0.5), (0, -0.5)
                    //
                    vector<vector<double> > InsidePoints;

                    double dl1;
                    dl1=mdl*dl;//500.0*dl;

                    InitialExample32InsidePoints(dl1, k, InsidePoints);

                    vector<double> Distances;
                    for(unsigned int ip=0; ip<InsidePoints.size(); ip++)
                    {
                        double d=(x-InsidePoints[ip][0])*(x-InsidePoints[ip][0])+(y-InsidePoints[ip][1])*(y-InsidePoints[ip][1]);
                        Distances.push_back(d);
                    }


                    unsigned int indexp=0;
                    double min_d=Distances[0];
                    for(unsigned ip=1; ip<Distances.size(); ip++)
                    {
                        if(Distances[ip]<min_d)
                        {
                            min_d=Distances[ip];
                            indexp=ip;
                        }
                    }

                    CoeffRefinedParametricMap[0][IndexV]=InsidePoints[indexp][0];
                    CoeffRefinedParametricMap[1][IndexV]=InsidePoints[indexp][1];

                }
            }

        }
    }

    //nd*CoeffRefinedParametricMap
    double nd=4.0;

    cout<<"Input the time of Enlarging Coeffs (default is 4.0)"<<endl;

    cin>>nd;

    for(unsigned int i=0; i<CoeffRefinedParametricMap.size(); i++)
    {
        for(unsigned int j=0; j<CoeffRefinedParametricMap[i].size(); j++)
        {
            CoeffRefinedParametricMap[i][j]=nd*CoeffRefinedParametricMap[i][j];
        }
    }

    MSplineFunctions * P_Fun=new MSplineFunctions(CoeffRefinedParametricMap, *P_FEMMesh);


    delete P_RefinedParametricMap;
    P_RefinedParametricMap=P_Fun;
    P_Fun = NULL;

    cout<<"Before Modifying Parameterization"<<endl;

    //====================================================================================
    //Here, Modify the inside control points
    //Modify CoeffRefinedParametricMap
    vector<vector<double> > NewCoeffRefinedParametricMap;
    double TimeSize, CoulombCoeff, epsilon0, Hc;
    cout<<"Put in TimeSize"<<endl;
    cin>>TimeSize;
    cout<<"Put in Hc"<<endl;
    cin>>Hc;
    cout<<"Put in CoulombCoeff"<<endl;
    cin>>CoulombCoeff;
    cout<<"Put in epsilon0"<<endl;
    cin>>epsilon0;

    MSplineFunctions * P_Fun2= P_RefinedParametricMap->ModifyParameterizationBySpringModelAndBoundaryFixing
            (TimeSize, Hc, CoulombCoeff, epsilon0, 10, 10, 10000, P_FEMMesh,
             CoeffRefinedParametricMap, NewCoeffRefinedParametricMap,"NULL.txt","NULL.txt","NULL.txt","NULL.txt","NULL.txt","NULL.txt","NULL.txt","NULL.txt","NULL.txt");


    cin.get();

    delete P_RefinedParametricMap;
    P_RefinedParametricMap=NULL;
    delete P_Fun2;
    P_Fun2=NULL;

    //CoeffRefinedParametricMap.clear();
    vector<vector<double> >().swap(CoeffRefinedParametricMap);

    //=====================================================================
    // NewCoeffRefinedParametricMap/nd
    for(unsigned int i=0; i<NewCoeffRefinedParametricMap.size(); i++)
    {
        for(unsigned int j=0; j<NewCoeffRefinedParametricMap[i].size(); j++)
        {
            NewCoeffRefinedParametricMap[i][j]=NewCoeffRefinedParametricMap[i][j]/nd;
        }
    }
    //====================================================================================
    cout<<"After Modifying Parameterization"<<endl;

   // cout<<"Print the Parametric Map:"<<endl;
   // PrintParametricMap(CoeffRefinedParametricMap, P_FEMMesh, "Parameterization_Axel.axl");

   // cin.get();

    //============FEM solver============
    //the Parametric Map is *P_RefinedParametricMap
    //the Parametric Mesh is *P_FEM

    cout<<"solving x^2 grad( grad phi/x^2)=f(x,y)---> input 1"<<endl;
    cout<<"solving x^2 grad( grad phi/x^2)=f(x,y)+phi---> input 2"<<endl;
    cout<<"solving x^2 grad( grad phi/x^2)=f(x,y)+phi^2---> input 3"<<endl;
    cout<<"solving x^2 grad( grad phi/x^2)=R(phi????) ---->input 4"<<endl;
    unsigned int EquationIndex;
    cin>>EquationIndex;


    OfTime<<"-------------------------------"<<endl;
    OfTime<<"solving x^2 grad( grad phi/x^2)=f(x,y)---> input 1"<<endl;
    OfTime<<"solving x^2 grad( grad phi/x^2)=f(x,y)+phi---> input 2"<<endl;
    OfTime<<"solving x^2 grad( grad phi/x^2)=f(x,y)+phi^2---> input 3"<<endl;
    OfTime<<"solving x^2 grad( grad phi/x^2)=R(phi????) ---->input 4"<<endl;
    OfTime<<endl;
    OfTime<<"the "<<EquationIndex<<"-th Equation has been choosen."<<endl;



    if(ExampleIndex == 32 || ExampleIndex == 33 || ExampleIndex == 34 ||ExampleIndex==35)
    {
        if(ExampleIndex==35)
        {
            EquationIndex=1;//For the 35-th example, we just test the Equation 1 with K=IdentityMatrix,
                         //i.e. an elliptic equation
        }

        switch (EquationIndex)
        {
        case 1:
        {
            solveEquation1(OfTime, P_FEMMesh, NewCoeffRefinedParametricMap, ExampleIndex);
            break;
        }
        case 2:
        {
            solveEquation2(OfTime, P_FEMMesh, NewCoeffRefinedParametricMap, ExampleIndex);
            break;
        }
        case 3:
        {
            double epsilon = 0.1;// Reset in solveEquation3() as (1/(k+1))^4
            //double InitialCoeff=0.0;
            bool IsConvergence=false;

            solveEquation3(OfTime, epsilon, P_FEMMesh, NewCoeffRefinedParametricMap,
                           ExampleIndex, IsConvergence);
            break;
        }
        case 4:
        {
            bool IsConvergence=false;
            //Initialize the values of XPoint and CenterPoint
            //In the initial mesh:
            vector<PointSet> XPoint;
            vector<PointSet> CenterPoint;

            GetXPointCenterPoint(ExampleIndex, P_FEMMesh,P_ParametricMesh, XPoint, CenterPoint);

//            vector<Coordinate*> Pcoordinate_PsXPoint;
//            Pcoordinate_PsXPoint.push_back(P_XPoint);

//            vector<Coordinate*> Pcoordinate_PsFCenter;
//            Pcoordinate_PsFCenter.push_back(P_Center);

//            cout<<"XP.Index_ItsCell = "<<XP.Index_ItsCell<<endl;
//            cout<<"CP.Index_ItsCell = "<<CP.Index_ItsCell<<endl;

//            //Compute XPoints, CenterPoint
//           vector<vector< vector<double> > > XPxy;
//           vector<vector< vector<double> > > CPxy;
//           P_RefinedParametricMap->Evaluation(0,0,XPoint,XPxy,*P_FEMMesh);
//           cout<<"XPoint: ("<<XPxy[0][0][0]<<", "<<XPxy[0][1][0]<<") "<<endl;
//           P_RefinedParametricMap->Evaluation(0,0,CenterPoint,CPxy,*P_FEMMesh);
//           cout<<"Center Point: ("<<CPxy[0][0][0]<<", "<<CPxy[0][1][0]<<")"<<endl;
//           cin.get();

            //Solve PDE
            solveEquation4(OfTime, XPoint, CenterPoint, P_FEMMesh, NewCoeffRefinedParametricMap,
                           ExampleIndex, IsConvergence);
            break;
        }
        default:
            cout<<"The EquationIndex is out of the range"<<endl;
            cin.get();
            break;
        }

    }
    else
    {
        cout<<"check EquationIndex!!!"<<endl;
        cin.get();
    }

    //OfTime.close();

        /*Finish PETSc program*/
        PetscFinalize();

}


///*---------------------------------------------*/
//int main(int argc,char **args)
//{//Read Modified parameterization map from
//    /*Initalize PETSc*/
//    PetscInitialize(&argc, &args, (char*)0, NULL);


//    ofstream OfTime;
//    OfTime.open("Time.txt");
//    //================================================
//    //Read the ParametricMap
//    //Mesh
//    cout<<"Read the mesh, subdivide it and parametric map"<<endl;

//    //----------
//    OfTime<<endl;
//    OfTime<<"Read the mesh, subdivide it and parametric map"<<endl;

//    Mesh * P_FEMMesh=new Mesh;
//    //Coeff
//    vector<vector<double> > NewCoeffRefinedParametricMap;

//    cout<<"The following examples can be chosen:"<<endl;

//    cout<<"ExampleIndex 32: PDE_PFEM_2: Example7_1:alpha shape with the boundary determined by y^2-x(x-1)^2=2"<<endl;

//    OfTime<<"The following examples can be chosen:"<<endl;

//    OfTime<<"ExampleIndex 32: PDE_PFEM_2: Example7_1:alpha shape with the boundary determined by y^2-x(x-1)^2=2"<<endl;
//    OfTime<<"ExampleIndex 33: Example7_0:alpha shape with the boundary determined by y^2-x(x-1)^2=2 and x=2y^2+1.5"<<endl;
//    OfTime<<"ExampleIndex 34: The same as the case with ExampleIndex as 4:"<<endl;
//    OfTime<<"ExampleIndex 35: Example7_0:alpha shape with the boundary determined by y^2-x(x-1)^2=2 and x=2y^2+1.5"<<endl;

//    unsigned int ExampleIndex;
//    cout<<"Please put in the index of this example"<<endl;
//    cin>>ExampleIndex;

//    cout<<"Remark:----------------------------------"<<endl;
//    cout<<"In this application (PDE_PFEM_2), ExampleIndex="<<ExampleIndex<<endl;

//    OfTime<<endl;
//    OfTime<<"In this application (PDE_PFEM_2), ExampleIndex="<<ExampleIndex<<endl;

//    unsigned int dimension=2;
//    unsigned int VerNum=0;

//    switch(ExampleIndex)
//    {

//    case 32://PDE_PFEM_2: Example7_1:alpha shape with the boundary determined by y^2-x(x-1)^2=2

//        P_FEMMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Results/Example32PDE_PFEM_2/Equation2/k=31/ParametricMesh.txt");

//        //Read the coefficients
//        VerNum=P_FEMMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
//        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Results/Example32PDE_PFEM_2/Equation2/k=31/ParametricCoeff.txt", dimension, VerNum, NewCoeffRefinedParametricMap);
//        break;

//    default:
//        cout<<"Read Meshs and Coeffs: Impossibile!"<<endl;
//        cin.get();
//        break;
//    }



//    double nd;
//    cout<<"Input the enlarging time nd"<<endl;
//    cin>>nd;

//    OfTime<<"nd = "<<nd<<endl;

//    //=====================================================================
//    // NewCoeffRefinedParametricMap/nd
//    for(unsigned int i=0; i<NewCoeffRefinedParametricMap.size(); i++)
//    {
//        for(unsigned int j=0; j<NewCoeffRefinedParametricMap[i].size(); j++)
//        {
//            NewCoeffRefinedParametricMap[i][j]=NewCoeffRefinedParametricMap[i][j]/nd;
//        }
//    }
//    //====================================================================================
//    cout<<"After Modifying Parameterization"<<endl;

//   // cout<<"Print the Parametric Map:"<<endl;
//   // PrintParametricMap(CoeffRefinedParametricMap, P_FEMMesh, "Parameterization_Axel.axl");

//   // cin.get();

//    //============FEM solver============
//    //the Parametric Map is *P_RefinedParametricMap
//    //the Parametric Mesh is *P_FEM

//    cout<<"solving x^2 grad( grad phi/x^2)=f(x,y)---> input 1"<<endl;
//    cout<<"solving x^2 grad( grad phi/x^2)=f(x,y)+phi---> input 2"<<endl;
//    cout<<"solving x^2 grad( grad phi/x^2)=f(x,y)+phi^2---> input 3"<<endl;
//    cout<<"solving x^2 grad( grad phi/x^2)=R(phi????) ---->input 4"<<endl;
//    unsigned int EquationIndex;
//    cin>>EquationIndex;


//    OfTime<<"-------------------------------"<<endl;
//    OfTime<<"solving x^2 grad( grad phi/x^2)=f(x,y)---> input 1"<<endl;
//    OfTime<<"solving x^2 grad( grad phi/x^2)=f(x,y)+phi---> input 2"<<endl;
//    OfTime<<"solving x^2 grad( grad phi/x^2)=f(x,y)+phi^2---> input 3"<<endl;
//    OfTime<<"solving x^2 grad( grad phi/x^2)=R(phi????) ---->input 4"<<endl;
//    OfTime<<endl;
//    OfTime<<"the "<<EquationIndex<<"-th Equation has been choosen."<<endl;



//    if(ExampleIndex == 32 || ExampleIndex == 33 || ExampleIndex == 34 ||ExampleIndex==35)
//    {
//        if(ExampleIndex==35)
//        {
//            EquationIndex=1;//For the 35-th example, we just test the Equation 1 with K=IdentityMatrix,
//                         //i.e. an elliptic equation
//        }

//        switch (EquationIndex)
//        {
//        case 1:
//        {
//            solveEquation1(OfTime, P_FEMMesh, NewCoeffRefinedParametricMap, ExampleIndex);
//            break;
//        }
//        case 2:
//        {
//            solveEquation2(OfTime, P_FEMMesh, NewCoeffRefinedParametricMap, ExampleIndex);
//            break;
//        }
//        case 3:
//        {
//            double epsilon = 0.1;// Reset in solveEquation3() as (1/(k+1))^4
//            //double InitialCoeff=0.0;
//            bool IsConvergence=false;

//            solveEquation3(OfTime, epsilon, P_FEMMesh, NewCoeffRefinedParametricMap,
//                           ExampleIndex, IsConvergence);
//            break;
//        }

//        default:
//            cout<<"The EquationIndex is out of the range"<<endl;
//            cin.get();
//            break;
//        }

//    }
//    else
//    {
//        cout<<"check EquationIndex!!!"<<endl;
//        cin.get();
//    }

//    //OfTime.close();

//        /*Finish PETSc program*/
//        PetscFinalize();

//}
