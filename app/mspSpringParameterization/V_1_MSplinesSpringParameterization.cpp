// V_1_MSplinesSpringParameterization.cpp


#include <iostream>
#include <fstream>
#include <map>
#include <time.h>

// //PETSc
#include <petsc.h>
#include <petscksp.h>

//Data structure
#include "msp/Node.h"

//Spline
#include "msp/Mesh.h"
#include "msp/MSplineFunction.h"
#include "msp/PointSet.h"
#include "msp/Coordinate.h"
#include "msp/MSplineFunctions.h"
#include "msp/Functions.h"


#include "msp/FEM_CoeffMatrixLoadVector.h"
#include "msp/FEM_ErrorFunctions.h"

using namespace std;

/**
 * \section mspSpringParameterization
 * In this application, a method for modifing the quality of an initial parameterization by fixing
 * the boundary of this initial parameterization.
 *
 * *************************
 * - \c Input: the initial parameterization
 * - \c Outout: the modified parameterization
 * *************************
 * Here, we suppose that all the vertices of the initial parameterization are inside of the area covered
 * by the initial parameterization.
 *
 * \subsection method The algorithm is based on two models.
 * - In order to distribute the vertices inside, the spring model is introduced,
 * where "Hc" is used to set the coefficient of this type of force.
 * - In order to restrict all the vertices are within the area covered by the initial parameterization,
 *	 we suppose each interior vertex is forced by Coulomb forces (treated the vertex and the boundary of this area have the same charges.).
 * where "CoulombCoeff" is used to set the coefficient of this type of force.
 * - This algorithm is implemented in a discrete way. Thus, we should set "TimeSize" for each step.
 *\subsection otherParameterization If one wants to modify other parameterizations,
 * the initial parameterization should be given with reading in a parametric mesh data and coefficient data.
 *
 * \subsection usage Usage
 * - It can be used as follows:
 * \code
 * ./msplineSpringParameterization
 * \endcode
 * - We use the KSPSolver in PETSc process of solving classical Possion Equation
 *   to test if the modifed parameterization is good or not.
*/


void DeltaPositionBasedOnSpringModel(double Hc, double TimeSize, const vector<double>& Mass, const vector<vector<double> >& VerticesPosition,
                                     Mesh * P_ParametricMesh, vector<vector<double> > &DeltaPosition)
{
    /** InPut: Hook's coeff (Hc); TimeSize; the mass vector vector<double> Mass, the positions of vertices vector<vector<double> >& VerticesPosition
     *	  And the mesh P_ParmetricMesh
     * OutPut: the difference between the original vertices and the new vertices vector<vector<double> > &DeltaPosition
    */

    DeltaPosition.clear();

    //For each vertex, compute DeltaPosition[i_vertex]
    for(unsigned int i_vertex = 0; i_vertex < P_ParametricMesh->P_Vertices->size(); i_vertex++)
    {
        //===============================
        // For the i_vertex-th vertex, computer the force at this vertex (set<unsigned int> Index_ItsVertices;)

        //The force
        vector<double> Fi;
        Fi.push_back(0);
        Fi.push_back(0);//Fi=[0, 0]
        if(P_ParametricMesh->P_Vertices->at(i_vertex).Is_interiorVertex)
        {
            //The length:
            vector<double> L;
            double MeanL=0;

            for(set<unsigned int>::const_iterator itV=P_ParametricMesh->P_Vertices->at(i_vertex).Index_ItsVertices.begin();
                itV != P_ParametricMesh->P_Vertices->at(i_vertex).Index_ItsVertices.end(); itV++)
            {
                //L=[L0, L1, L2, ... ...]
                //the length from VerticesPosition[*itV] to VerticesPosition[i_vertex]
                double s=sqrt((VerticesPosition[*itV][0]-VerticesPosition[i_vertex][0])*(VerticesPosition[*itV][0]-VerticesPosition[i_vertex][0])+(VerticesPosition[*itV][1]-VerticesPosition[i_vertex][1])*(VerticesPosition[*itV][1]-VerticesPosition[i_vertex][1]));
                MeanL=MeanL+s;
                L.push_back(s);
            }
            MeanL=MeanL/(L.size()*1.0);// the average of the length

            unsigned int index=0;
            for(set<unsigned int>::const_iterator itV=P_ParametricMesh->P_Vertices->at(i_vertex).Index_ItsVertices.begin();
                itV != P_ParametricMesh->P_Vertices->at(i_vertex).Index_ItsVertices.end(); itV++)
            {

                //Fi=Fi+(VerticesPosition[*itV]-VerticesPosition[i_vertex])//the sum of vectors
                //directionCoeff=1.0-(MeanL/L[index])
                double directionCoeff=1.0-MeanL/L[index];
                //
                directionCoeff=1.0;
                //
                //directionCoeff=1.0-0.3*MeanL/L[index];

                Fi[0]=Fi[0]+directionCoeff*(VerticesPosition[*itV][0]-VerticesPosition[i_vertex][0]);
                Fi[1]=Fi[1]+directionCoeff*(VerticesPosition[*itV][1]-VerticesPosition[i_vertex][1]);

                index=index+1;

            }
            //Fi=Hc*Fi
            Fi[0]=Hc*Fi[0];
            Fi[1]=Hc*Fi[1];
        }

        //============================================
        //The delta displacement
        vector<double> Si;
        Si.push_back(0.5*Fi[0]*TimeSize*TimeSize/Mass[i_vertex]);
        Si.push_back(0.5*Fi[1]*TimeSize*TimeSize/Mass[i_vertex]);

        //============================================
        //Push_back the i_vertex-th delta displacement
        DeltaPosition.push_back(Si);

    }

}

/*
 * Compute the delta positions owning to the coulomb Force from boundary edges
*/
double CoulombForceIntegration(const double k, const double l, const double A, const double B, const double C)
{
    double F=0;
    //------------
    F=k*(-(2.0*B+4.0*C)/((4*A*C-B*B)*sqrt(A+B+C))+4.0*C/((4.0*A*C-B*B)*sqrt(C)));
    F=F+l*((4.0*A+2.0*B)/((4.0*A*C-B*B)*sqrt(A+B+C))-(2.0*B)/((4.0*A*C-B*B)*sqrt(C)));
    //------------
    return F;
}
void CoulombForce(vector<double>& dF, const unsigned int i_vertex, set<pair<unsigned int, unsigned int> >::const_iterator et, const vector<vector<double> >& VerticesPosition)
{
    dF.clear();
    //---------------------
    unsigned int VEindex1=(*et).first;
    unsigned int VEindex2=(*et).second;
    double x0, y0, x1, y1, x2, y2;
    x0=VerticesPosition[i_vertex][0];
    y0=VerticesPosition[i_vertex][1];

    x1=VerticesPosition[VEindex1][0];
    y1=VerticesPosition[VEindex1][1];

    x2=VerticesPosition[VEindex2][0];
    y2=VerticesPosition[VEindex2][1];
    //-----------------------
    double A, B, C;
    A=(x2-x1)*(x2-x1)+(y2-y1)*(y2-y1);
    B=-2.0*((x0-x1)*(x2-x1)+(y0-y1)*(y2-y1));
    C=(x0-x1)*(x0-x1)+(y0-y1)*(y0-y1);

    double kx, lx;
    kx=x1-x2;
    lx=x0-x1;

    double ky, ly;
    ky=y1-y2;
    ly=y0-y1;
    //
    double Fx, Fy;
    Fx=CoulombForceIntegration(kx, lx, A, B, C);//Do not implement
    Fy=CoulombForceIntegration(ky, ly, A, B, C);
    //
    dF.push_back(Fx);//dF[0]

    dF.push_back(Fy);//dF[1]


}
void DeltaPositionBasedOnBoundaryCoulombForce(const double &CoulombCoeff, const double &TimeSize, Mesh* P_ParametricMesh,
                                              const vector<vector<double> > &VerticesPosition,
                                              const vector<double> &Mass,
                                              vector<vector<double> > &DeltaPositionCoulomb)
{
    DeltaPositionCoulomb.clear();

    //Compute the boundary edges
    set<pair<unsigned int, unsigned int> > BoundaryEdges;
    P_ParametricMesh->GetBoundaryEdgesFromParametricMesh(BoundaryEdges);

    //Compute its delta position based on the force
    for(unsigned int i_vertex = 0; i_vertex < P_ParametricMesh->P_Vertices->size(); i_vertex++)
    {

        //----------------
        vector<double> dP;//the delta position for the i_vertex-th vertex
        //----------------

        vector<double> Fi;
        Fi.push_back(0.0);
        Fi.push_back(0.0);

        if(P_ParametricMesh->P_Vertices->at(i_vertex).Is_interiorVertex)//the i_vertex-th vertex is an interior vertex
        {
            //-----------For each boundary------
            for(set<pair<unsigned int, unsigned int> >::const_iterator et=BoundaryEdges.begin();
                et!=BoundaryEdges.end(); et++)
            {


                //-----
                vector<double> dF;
                //For the i_vertex (interior) vertex,
                //compute the force from the boundary edge *et
                CoulombForce(dF, i_vertex, et, VerticesPosition);//Do not implement
                //-----

                Fi[0]=Fi[0]+dF[0];
                Fi[1]=Fi[1]+dF[1];

            }


            //-----------------------------
        }

        Fi[0]=CoulombCoeff*Fi[0];
        Fi[1]=CoulombCoeff*Fi[1];

        //Compute the delta position for the i_vertex-th (interior) vertex
        dP.push_back(0.5*Fi[0]*TimeSize*TimeSize/Mass[i_vertex]);
        dP.push_back(0.5*Fi[1]*TimeSize*TimeSize/Mass[i_vertex]);

        //
        DeltaPositionCoulomb.push_back(dP);
    }
}

/*
 *VerticesPosition = VerticesPosition + DeltaPosition
*/
void UpDataVerticesPosition(vector< vector<double> > &VerticesPosition,
                            const vector< vector<double> > &DeltaPosition)
{
    for(unsigned int i=0; i < VerticesPosition.size(); i++)
    {
        VerticesPosition[i][0]=VerticesPosition[i][0]+DeltaPosition[i][0];
        VerticesPosition[i][1]=VerticesPosition[i][1]+DeltaPosition[i][1];
    }
    //Return VerticesPosition
}


double TotalDistance(const vector<vector<double> > &DeltaPositions)
{
    double epsilon=0.0;

    for(unsigned int i=0; i<DeltaPositions.size(); i++)
    {
        for(unsigned int j=0; j<DeltaPositions[i].size(); j++)
        {
            epsilon=epsilon+DeltaPositions[i][j]*DeltaPositions[i][j];
        }
    }

    epsilon=sqrt(epsilon)/(1.0*DeltaPositions.size());

    return epsilon;
}
// ////////////////////////////////////////////////////////////////////////////////////////
int main(int argc,char **args)
{
    Mat coeff_matrix;
    Vec load_vect;
    Vec m_solut;

    unsigned int n_u=20;
    unsigned int n_v=20;

    /*Initalize PETSc*/
    PetscInitialize(&argc, &args, (char*)0, NULL);

    ofstream ofProgress;
    ofProgress.open("ComputationProgress.txt");

    //============Initial Parametric Map=========
    unsigned int VerNum;
    unsigned int dimension=2;
    //Mesh
    Mesh * P_ParametricMesh=new Mesh;
    //Coeff
    vector<vector<double> > Coeff_Parametric_Map;
    unsigned int ExampleIndex=40;//default
    cout<<"Please put in the index of this example"<<endl;
    ofProgress<<"Please put in the index of this example"<<endl;

    cout<<"Remark:----------------------------------"<<endl;
    cout<<"38-->ModifySpringParameterization-->For the example with index 35 k=7"<<endl;
    cout<<"39-->ModifySpringParameterization-->For the example with index 35 k=3"<<endl;
    cout<<"40-->ModifySpringParameterization-->For the example with index 35 k=1"<<endl;
    cout<<"41-->ModifySpringParameterization-->For the example9: in this case when k=7, KSP doesn't convergent"<<endl;
    cout<<"42-->ModifySpringParameterization-->For the example2_0:"<<endl;
    cout<<"43-->ModifySpringParameterization-->DrawWingsFittingData: WingFittingResult"<<endl;
    cout<<"44-->ModifySpringParameterization-->FourLeafClover"<<endl;
    cout<<"45-->ModifySpringParameterization-->MultiHoleStructure"<<endl;

    cout<<"10000--->For test"<<endl;


    //ofProgress
    ofProgress<<"38-->ModifySpringParameterization-->For the example with index 35 k=7"<<endl;
    ofProgress<<"39-->ModifySpringParameterization-->For the example with index 35 k=3"<<endl;
    ofProgress<<"40-->ModifySpringParameterization-->For the example with index 35 k=1"<<endl;

    ofProgress<<"41-->ModifySpringParameterization-->For the example9: in this case when k=7, KSP doesn't convergent"<<endl;
    ofProgress<<"42-->ModifySpringParameterization-->For the example2_0:"<<endl;
    ofProgress<<"43-->ModifySpringParameterization-->DrawWingsFittingData: WingFittingResult"<<endl;
    ofProgress<<"44-->ModifySpringParameterization-->FourLeafClover"<<endl;
    ofProgress<<"45-->ModifySpringParameterization-->MultiHoleStructure"<<endl;

    ofProgress<<"10000--->For test"<<endl;

    cin>>ExampleIndex;

    ofProgress<<endl;
    ofProgress<<"The "<<ExampleIndex<<"-th example has been chosen."<<endl;

    //Read the mesh and coeff
    switch(ExampleIndex)
    {
    case 38:
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/Mywork/msplinetools/data/Examples/ModifyParameterization/1/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/Mywork/msplinetools/data/Examples/ModifyParameterization/1/ParametricCoeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;
    case 39:
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/Mywork/msplinetools/data/Examples/ModifyParameterization/2/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/Mywork/msplinetools/data/Examples/ModifyParameterization/2/ParametricCoeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;
    case 40:
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/Mywork/msplinetools/data/Examples/ModifyParameterization/3/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/Mywork/msplinetools/data/Examples/ModifyParameterization/3/ParametricCoeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 41:
//        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/Example9/ParametricMesh.txt");
//        //P_ParametricMesh->Cout_Mesh_File();
//        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
//        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/Example9/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
//        break;
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/Example9/TestData/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/Example9/TestData/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 42:
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/Example2_0/BasedOnParametricMap_1/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/Example2_0/BasedOnParametricMap_1/ParametricCoeffs1.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 43:
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/ExampleWings/WingFitting/WingFittingResult/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/ExampleWings/WingFitting/WingFittingResult/ParametricCoeffTestWing2.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 44://FourLeafClover
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/ModifyParameterization/FourLeafClover/ParametricMesh.txt");
        //P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Results/Example44/Example44FourLeafClover3/2_data/ParametricMesh.txt");
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/ModifyParameterization/FourLeafClover/ParametricCoeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        //Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Results/Example44/Example44FourLeafClover3/2_data/ParametricCoeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 45://MultiHoles
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/ModifyParameterization/MultiholeStructure/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/ModifyParameterization/MultiholeStructure/ParametricCoeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 10000://test
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Results/stones/modifyParameterizationResult/InitialParameterizationdata/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Results/stones/modifyParameterizationResult/InitialParameterizationdata/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    default:
        cout<<"It is Default~"<<endl;
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/Documents/MyWorkDocument/TheoreyMSplines/OurWorks/PlanarDomainParametrization/Data/FourLeafClover/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/Documents/MyWorkDocument/TheoreyMSplines/OurWorks/PlanarDomainParametrization/Data/FourLeafClover/ParametricCoeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;
        cin.get();
        break;
    }

//    //Generate the initial Parametric map
    MSplineFunctions* P_ParametricMap=new MSplineFunctions(Coeff_Parametric_Map, *P_ParametricMesh);

    P_ParametricMap->CoutThisMSplineFunctionsJacobianMeshAxelFile(P_ParametricMesh, n_u, n_v, "InitialParameterization_JacobianAxelMesh.axl", "InitialParameterization_Areas.txt");

   // P_ParametricMap->ModifyParameterizationBySpringModelAndBoundaryFixing(0.1, 1.0, 0.001, 0.001, 20, 20, 1000, 1, P_ParametricMesh, Coeff_Parametric_Map);

    cout<<"Do you need to modify this parametric map with Spring model with fixing boundary? (0=NO; 1=Yes)"<<endl;
    ofProgress<<"Do you need to modify this parametric map with Spring model with fixing boundary?"<<endl;
    bool Is_modify;
    cin>>Is_modify;
    ofProgress<<"---"<<Is_modify<<"(0=NO; 1=Yes)"<<endl;
    if(Is_modify)
    {
        ofProgress<<"====Modify the parametric map with Spring model with fixing boundary===="<<endl;
        //InPut the mass vector
        vector<double> Mass(VerNum, 1.0);//the mass of each is the same and taken as 1.0
        ofProgress<<"Default: all the mass are set as 1"<<endl;

        /*-----------------------------------------------------------------------------*/
        //Input the max iteration number
        unsigned int MaxIterNum;
        cout<<"Input the max iteration number"<<endl;
        cin>>MaxIterNum;
        ofProgress<<"MaxIterNum = "<<MaxIterNum<<endl;

        //InPut the parameters of our model
        //InPut the size of time:
        double TimeSize;
        cout<<"input the time size"<<endl;
        cin>>TimeSize;
        ofProgress<<"the size of time step = "<<TimeSize<<endl;
        //InPut Hook's coeffient
        double Hc;
        cout<<"input the Hook's coeffient"<<endl;
        cin>>Hc;
        ofProgress<<"the Hook's coeffient = "<<Hc<<endl;
        //InPut Coulomb Coeff
        double CoulombCoeff;
        cout<<"Input the positive coeffecient (>0) and if it is larger, the interior vertices will be more far from the boundary edges"<<endl;
        cin>>CoulombCoeff;
        ofProgress<<"The coeffient of Coulomb force is "<<CoulombCoeff<<endl;

        //InPut the tolerance of the modification parameterization's convergence
        double epsilon0;
        cout<<"Input the tolerance of the modification parameterization's convergence"<<endl;
        cin>>epsilon0;
        ofProgress<<"The tolerance of the modification parameterization's convergence = "<< epsilon0<<endl;

        // The loop for modifying
        double epsilon=epsilon0+1.0;
        unsigned int iternum =0;

        //Compute the initial position of vertices
        vector<vector<double> >VerticesPosition;
        P_ParametricMap->ComputeVerticesPositions(VerticesPosition, P_ParametricMesh);

        cout<<"VerticesPosition.size()="<<VerticesPosition.size()<<endl;

        unsigned int N=1;
        ofstream ofEpsilon;
        ofEpsilon.open("SpringMethodVerticesDistanceConvergenceEpsilons.txt");
        while(epsilon>epsilon0 && iternum<MaxIterNum+1)
        {
            //Modify updated P_ParametricMap
            //===Begin to iterate
            vector<vector<double> > DeltaPositionSpring;
            vector<vector<double> > DeltaPositionCoulomb;
            for(unsigned int iterIndex=0; iterIndex<N; iterIndex++)
            {
                //vector<vector<double> > DeltaPositionSpring;
                DeltaPositionBasedOnSpringModel(Hc, TimeSize, Mass, VerticesPosition, P_ParametricMesh, DeltaPositionSpring);



                //////vector<vector<double> > DeltaPositionCoulomb;
                DeltaPositionBasedOnBoundaryCoulombForce(CoulombCoeff, TimeSize, P_ParametricMesh, VerticesPosition, Mass, DeltaPositionCoulomb);

//                /////////////////////////////////////////
//                for(unsigned i_test=0; i_test<DeltaPositionCoulomb.size(); i_test++)
//                {
//                    DeltaPositionCoulomb[i_test][0]=0;
//                    DeltaPositionCoulomb[i_test][1]=0;
//                }
//                ///////////////////////////////////////////



                //VerticesPosition=VerticesPosition+DeltaPosition
                UpDataVerticesPosition(VerticesPosition, DeltaPositionSpring);


                //////VerticesPosition=VerticesPosition+DeltaPosition
                //UpDataVerticesPosition(VerticesPosition, DeltaPositionCoulomb);
            }

            //Update P_ParametricMap
            //Generate other Hermite Data Based On Positions Of Vertices
            vector<vector<double> > NewCoeff_ParametricMap;
            MSplineFunctions* P_ParametricMap2=new MSplineFunctions(VerticesPosition, P_ParametricMesh, NewCoeff_ParametricMap);


            delete P_ParametricMap2;
            P_ParametricMap2=NULL;
            //"NewCoeff_ParametricMap" is treated as a feedback for Print the new Parametric Map
            //For NewCoeff_ParametricMap, all the coeffecients are gotten with the help of the distances between different vertices.
            //Now we modify "NewCoeff_ParametricMap" with the original coeff "Coeff_Parametric_Map"
            // such that following the boundary edge direction, the coeff in NewCoeff_ParametricMap is the same as the coeff in Coeff_ParametricMap
            //===================================================================================
            for(unsigned int i_v = 0; i_v < P_ParametricMesh->P_Vertices->size(); i_v ++)
            {
                if(!P_ParametricMesh->P_Vertices->at(i_v).Is_interiorVertex)//the i_v-th vertex is a boundary vertex
                {
                    if(P_ParametricMesh->P_Vertices->at(i_v).Deg()<=3)//deg(the i_v-th vertex)=3
                    {
                        unsigned int TheFirstCellIndex=P_ParametricMesh->P_Vertices->at(i_v).ItsCellsToCoordinatesMap.begin()->first;
                        //Which edge with an endpoint as the i-th vertex is an boundary edge?
                        for(set<unsigned int>::const_iterator jt_v=P_ParametricMesh->P_Vertices->at(i_v).Index_ItsVertices.begin();
                            jt_v!=P_ParametricMesh->P_Vertices->at(i_v).Index_ItsVertices.end(); jt_v++)
                        {
                            unsigned int j_v=*jt_v;
                            //Check the edge i_v-j_v is a boundary?
                            bool is_boundary=P_ParametricMesh->Is_ABoundaryEdge(i_v, j_v);
                            //in the view of the TheFirstCellIndex-th Cell,
                            //the edge i_vj_v is along s-direction or t-direction
                            if(is_boundary)
                            {
                                int Flag=P_ParametricMesh->P_Cells->at(TheFirstCellIndex).TheLocalDirection(i_v, j_v);
                                //s-direction (1), -s -direction (-1); t-direction (2), -t -direction (-2)
                                if(Flag==1||Flag==-1)//s-direction
                                {
                                    unsigned int coeffindex=4*i_v+1;
                                    for(unsigned int i=0; i<NewCoeff_ParametricMap.size(); i++)
                                    {
                                        NewCoeff_ParametricMap[i][coeffindex]=Coeff_Parametric_Map[i][coeffindex];
                                    }
                                }
                                if(Flag==2||Flag==-2)//t-direction
                                {
                                    unsigned int coeffindex=4*i_v+2;
                                    for(unsigned int i=0; i<NewCoeff_ParametricMap.size(); i++)
                                    {
                                        NewCoeff_ParametricMap[i][coeffindex]=Coeff_Parametric_Map[i][coeffindex];
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //////DeltaPositionSpring=DeltaPositionSpring+DeltaPositionCoulomb
            //UpDataVerticesPosition(DeltaPositionSpring, DeltaPositionCoulomb);

            //Update iternum
            iternum=iternum+N;
            //Compute epsilon again
            epsilon=TotalDistance(DeltaPositionSpring);
            //TimeStep--->scaler to 1s
            epsilon=epsilon/(TimeSize*TimeSize);

            ofEpsilon<<"iteration num = "<<iternum<<"  epsilon = "<<epsilon<<endl;

            delete P_ParametricMap;
            P_ParametricMap = new MSplineFunctions(NewCoeff_ParametricMap, *P_ParametricMesh);

            //Print the new parametric map
            if(epsilon<epsilon0 || iternum > MaxIterNum)//Print the result of iteration!
            {
                PrintParametricMap(NewCoeff_ParametricMap, P_ParametricMesh, "ModifiedParameterization.axl");
                PrintParametricMapPositionInfo(NewCoeff_ParametricMap, P_ParametricMesh, "ModifiedVerticesPosition_Axel.axl");
                P_ParametricMap->CoutThisMSplineFunctionsAsBSplineSurfaceAxelFile(P_ParametricMesh);
                P_ParametricMap->CoutThisMSplineFunctionsJacobianMeshAxelFile(P_ParametricMesh, n_u, n_v, "ModifiedParameterizationWithSpringModel_JacobianAxelMesh.axl", "ModifiedParameterizationWithSpringModel_PhysicalAreas.txt");

                cout<<"Have printed modified documents!"<<endl;
                cin.get();
            }

            //===================================================================================


        }

        ofEpsilon.close();

        if(iternum>MaxIterNum)
        {
            cout<<"The iteration of modifying parameterization doesn't converge with the tolerance = "<<epsilon0<<endl;
            ofProgress<<"The iteration of modifying parameterization doesn't converge with the tolerance = "<<epsilon0<<endl;
        }


    }



    //===========================================================================================================================================
    //===========================================================================================================================================

    cout<<"Generate the stiffness matrix based on possion's Equation with Homogeneous boundary condition"<<endl;
    ofProgress<<"=====Generate the stiffness matrix based on possion's Equation with Homogeneous boundary condition====="<<endl;

    //======================================================================
    //Preparation: GuassPoint
    unsigned int Gp=0;
    cout<<"Input the number of Guass points:"<<endl;
    cin>>Gp;
    ofProgress<<"the number of Guass Points ="<<Gp<<endl;


    unsigned int u_gaussnum=Gp, v_gaussnum=Gp;
    unsigned int load_gaussnum=5;
    vector<double> gausspt, m_IntePt, m_loadw;

    //Weights/////////
    double **w;
    w=GaussPoints_Weights(u_gaussnum, load_gaussnum, gausspt, m_IntePt, m_loadw);
    //==========================================================================================
    //Set the FileNames:
    //----ModifiedParameterizationJacobinOverFinerMesh.axl
    //----ModifiedParameterizationAreasOverFinerMesh.txt
    //----Coeff_matrix.m (Matlab file)
    //----monitor.txt (ksp)
    //----singularvalues.txt
    vector<char *> JacobianFileNames;
    vector<char *> AreasFileNames;
    vector<char *> CoeffMatrixFileNames;
    vector<char *> MonitorFileNames;
    vector<char *> SingularValuesFileNames;


    //k=0;
    JacobianFileNames.push_back("ModifiedPMapJacobian_k=0.axl");
    AreasFileNames.push_back("ModifedPMapAreas_k=0.txt");
    CoeffMatrixFileNames.push_back("coeffmatrix_k0.m");

    MonitorFileNames.push_back("Monitor_k=0.txt");
    SingularValuesFileNames.push_back("singularvalues_k=0.txt");
    //k=1;
    JacobianFileNames.push_back("ModifiedPMapJacobian_k=1.axl");
    AreasFileNames.push_back("ModifedPMapAreas_k=1.txt");
    CoeffMatrixFileNames.push_back("coeffmatrix_k1.m");

    MonitorFileNames.push_back("Monitor_k=1.txt");
    SingularValuesFileNames.push_back("singularvalues_k=1.txt");
    //k=3;
    JacobianFileNames.push_back("ModifiedPMapJacobian_k=3.axl");
    AreasFileNames.push_back("ModifedPMapAreas_k=3.txt");
    CoeffMatrixFileNames.push_back("coeffmatrix_k3.m");

    MonitorFileNames.push_back("Monitor_k=3.txt");
    SingularValuesFileNames.push_back("singularvalues_k=3.txt");
    //k=7;
    JacobianFileNames.push_back("ModifiedPMapJacobian_k=7.axl");
    AreasFileNames.push_back("ModifedPMapAreas_k=7.txt");
    CoeffMatrixFileNames.push_back("coeffmatrix_k7.m");

    MonitorFileNames.push_back("Monitor_k=7.txt");
    SingularValuesFileNames.push_back("singularvalues_k=7.txt");
    //k=15;
    JacobianFileNames.push_back("ModifiedPMapJacobian_k=15.axl");
    AreasFileNames.push_back("ModifedPMapAreas_k=15.txt");
    CoeffMatrixFileNames.push_back("coeffmatrix_k15.m");

    MonitorFileNames.push_back("Monitor_k=15.txt");
    SingularValuesFileNames.push_back("singularvalues_k=15.txt");
    //===========================================================================================
    //for(unsigned int i=0; i<5; i++)
        for(unsigned int i=0; i<1; i++)
    {
        PetscOptionsSetValue("-ksp_monitor_true_residual", MonitorFileNames[i]);
        PetscOptionsSetValue("-ksp_monitor_singular_value", SingularValuesFileNames[i]);
        //The FEM Mesh
        map<unsigned int, unsigned int> OrigialVertexToCurrentVertex;
        //subdivide the initial parametric mesh
        unsigned int k=pow(2, i)-1;
        cout<<"the times of subdivision = "<<k<<endl;
        Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(k, OrigialVertexToCurrentVertex);

        ofProgress<<endl;
        ofProgress<<"the times of subdivision = "<<k<<endl;
        ofProgress<<"The Basis Number = "<<4*(P_FEMMesh->P_Vertices->size())<<endl;

//        if(P_FEMMesh->P_Vertices->size() > 40000)//The size of coeffmatrix is too large
//        {
//            break;
//        }

        //Initial Mat coeff_matrix
        MatCreate(PETSC_COMM_WORLD, &coeff_matrix);
        MatSetSizes(coeff_matrix, PETSC_DECIDE, PETSC_DECIDE, P_FEMMesh->P_Vertices->size()*4, P_FEMMesh->P_Vertices->size()*4);
        MatSetFromOptions(coeff_matrix);
        MatMPIAIJSetPreallocation(coeff_matrix, 64, NULL, 64, NULL);
        MatSeqAIJSetPreallocation(coeff_matrix, PETSC_DEFAULT, NULL);
        MatSetUp(coeff_matrix);

        double k1=0;
        double k2=0;
        cout<<"Begin to generate the coeff_matrix:"<<endl;

        Generate_CoeffMatrix_Cell(u_gaussnum, v_gaussnum, coeff_matrix, m_IntePt, m_IntePt, w,
                                      P_FEMMesh, P_ParametricMesh, *P_ParametricMap, k1, k2, ExampleIndex);

        cout<<"Begin to initialize the load vector:"<<endl;
        Initialized_loadVector(load_vect, P_FEMMesh->P_Vertices->size());

        Generate_LoadVector(load_vect, gausspt, m_loadw, P_FEMMesh, P_ParametricMesh, *P_ParametricMap,
                            k1, k2, ExampleIndex);

        //Set Bases type
        cout<<"Set Bases type"<<endl;
        vector<unsigned int> BasesType;
        BasesType.resize(4*P_FEMMesh->P_Vertices->size(), 0);
        Set_Bases_type(P_FEMMesh, BasesType);

        //Modify LoadVector and CoeffMatrix
        MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);

//        //Save coeff_matrix as a matlab file
//        PetscViewer viewer;
//        PetscViewerASCIIOpen(PETSC_COMM_WORLD, CoeffMatrixFileNames[i], &viewer);
//        PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_MATLAB);
//        MatView(coeff_matrix, viewer);
//        PetscViewerDestroy(&viewer);

        map<unsigned int, set<unsigned int> > HTable;
        Initialized_HTable_withoutHangingVertices_1(P_FEMMesh, HTable);

        cout<<"Modify the load vector and coeff matrix:"<<endl;
        double C=0.0;
        Modification_LoadVector_CoeffMatrix(C, BasesType, coeff_matrix, load_vect, HTable);
        MatSetOption(coeff_matrix, MAT_SYMMETRIC, PETSC_TRUE);
        MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);
        //===================================================
        //Save coeff_matrix as a matlab file
        PetscViewer viewer;
        PetscViewerASCIIOpen(PETSC_COMM_WORLD, CoeffMatrixFileNames[i], &viewer);
        PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_MATLAB);
        MatView(coeff_matrix, viewer);
        PetscViewerDestroy(&viewer);
        //=======================================================
        VecCreate(PETSC_COMM_WORLD, &m_solut);
        VecSetSizes(m_solut, PETSC_DECIDE, 4*P_FEMMesh->P_Vertices->size());
        VecSetFromOptions(m_solut);
        //===================================================
        KSP ksp;
        PC pc;
        KSPCreate(PETSC_COMM_WORLD, &ksp);
        KSPSetType(ksp, KSPGMRES);
        KSPSetOperators(ksp, coeff_matrix, coeff_matrix);


        PetscReal rtol=1.0e-16;
        PetscReal stol=1.0e-16;

        PetscInt maxits=10000;
        KSPSetTolerances(ksp, rtol, stol, PETSC_DEFAULT, maxits);

        KSPGetPC(ksp, &pc);
        PCSetType(pc, PCGAMG);

        KSPSetComputeEigenvalues(ksp, PETSC_TRUE);

        KSPSetFromOptions(ksp);

       KSPSolve(ksp, load_vect, m_solut);

       PetscInt its;
       KSPGetIterationNumber(ksp, &its);

       KSPConvergedReason reason;
       KSPGetConvergedReason(ksp, &reason);

       cout<<"KSPIterationNumber = "<<its<<endl;
       ofProgress<<"KSPIterationNumber = "<<its<<endl;

       cout<<"KSPConvergedReason = "<<reason<<endl;
       ofProgress<<"KSPConvergedReason = "<<reason<<endl;


       ofProgress<<"KSPSetTolerances(ksp, rtol, stol, PETSC_DEFAULT, maxits);"<<endl;
       ofProgress<<"rtol = "<<rtol<<endl;
       ofProgress<<"stol = "<<stol<<endl;
       ofProgress<<"maxits = "<<maxits<<endl;

       //Jacobian And Areas
       vector< vector<double> > CoeffMap;
       P_ParametricMap->CoutThisMSplineFunctionsOverAFinerMesh(k, P_ParametricMesh, CoeffMap);
       MSplineFunctions * P_ParametricFinerMap=new MSplineFunctions(CoeffMap, *P_FEMMesh);
       P_ParametricFinerMap->CoutThisMSplineFunctionsJacobianMeshAxelFile(P_FEMMesh, n_u, n_v, JacobianFileNames[i], AreasFileNames[i]);
       delete P_ParametricFinerMap;
       P_ParametricFinerMap=NULL;

       //DestoryKSP PC
       KSPDestroy(&ksp);
    }



//   cout<<"The size of coeff_matrix = "<<4*P_FEMMesh->P_Vertices->size()<<endl;
//   cout<<"Do you want to compute the exact Eigenvalues of this matrix? (Suggestion: choose \"yes!\" if the size of this matrix is less than 500)"<<endl;
//   cout<<"RMK:Yes=1, No=0"<<endl;
//   bool Is_yes;
//   cin>>Is_yes;
//   if(Is_yes)
//   {
//       //======KSPComputeEigenvalues()
//        PetscReal r[4*P_FEMMesh->P_Vertices->size()];
//        PetscReal c[4*P_FEMMesh->P_Vertices->size()];
//       cout<<"KSP ComputeEigenvalueExplicitly:"<<endl;
//       KSPComputeEigenvaluesExplicitly(ksp, 4*P_FEMMesh->P_Vertices->size(), r, c);

//       double maxNorm, minNorm;
//       maxNorm=0;
//       minNorm=0;
//       if(P_FEMMesh->P_Vertices->size()>0)
//       {
//           minNorm=sqrt(r[0]*r[0]+c[0]*c[0]);
//       }
//       ofProgress<<"EigenValues of this stiff matrix:"<<endl;
//       for(unsigned int j=0; j<P_FEMMesh->P_Vertices->size(); j++)
//       {
//           double l=sqrt(r[j]*r[j]+c[j]*c[j]);
//           if(l>maxNorm)
//           {
//               maxNorm=l;
//           }
//           if(l<minNorm)
//           {
//               minNorm=l;
//           }
//           ofProgress<<r[j]<<" + "<<c[j]<<" i"<<endl;
//       }
//       ofProgress<<endl;
//       ofProgress<<"MaxNorm = "<<maxNorm<<endl;
//       ofProgress<<"MinNorm = "<<minNorm<<endl;
//       ofProgress<<"ConditionNumber = "<<maxNorm/minNorm<<endl;
//   }

//     //========================
//   PrintLoadVector(load_vect);

//   PrintSolution(m_solut);

//    PrintAxelFile_FEMSolution(Coeff_Parametric_Map, P_ParametricMesh, k, m_solut);
//    //============================
//    vector<double> cell_errorsResidualPhsicalDom;
//    double total_Error_ResidualPhsicalDom;

//    ResidualError_PhysicalDomain_RefinedParametricMap(gausspt, gausspt, w, m_solut,
//                                                      cell_errorsResidualPhsicalDom, total_Error_ResidualPhsicalDom,
//                                                       P_FEMMesh,  P_ParametricMesh,
//                                                      P_ParametricMap, ExampleIndex);
//    ofstream ofErrors;
//    ofErrors.open("L2ResidualErrorsOverCellsOnPhyDom.txt");
//    for(unsigned int i_error=0; i_error<cell_errorsResidualPhsicalDom.size(); i_error++)
//    {
//        ofErrors<<cell_errorsResidualPhsicalDom[i_error]<<" "<<endl;
//    }
//    ofErrors<<endl;
//    ofErrors.close();

//    ofErrors.open("L2TotalResidualErrorOverCellsOnPhyDom.txt");
//    ofErrors<<total_Error_ResidualPhsicalDom<<endl;
//    ofErrors.close();

//=================================================================================================
  //Destroy ksp coeff_matrix load_vec m_solut
    //KSPDestroy(&ksp);
    MatDestroy(&coeff_matrix);
    VecDestroy(&load_vect);
    VecDestroy(&m_solut);

    ofProgress.close();

     /*Finish PETSc program*/
     PetscFinalize();
//=================================================================================================
     return 0;
 }

