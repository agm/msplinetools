// MSplinesSpringParameterization.cpp


#include <iostream>
#include <fstream>
#include <map>
#include <time.h>
#include <math.h>

// //PETSc
#include <petsc.h>
#include <petscksp.h>

//Data structure
#include "msp/Node.h"

//Spline
#include "msp/Mesh.h"
#include "msp/MSplineFunction.h"
#include "msp/PointSet.h"
#include "msp/Coordinate.h"
#include "msp/MSplineFunctions.h"
#include "msp/Functions.h"


#include "msp/FEM_CoeffMatrixLoadVector.h"
#include "msp/FEM_ErrorFunctions.h"

using namespace std;

/**
 * \section mspSpringParameterization
 * In this application, a method for modifing the quality of an initial parameterization by fixing
 * the boundary of this initial parameterization.
 *
 * *************************
 * - \c Input: the initial parameterization
 * - \c Outout: the modified parameterization
 * *************************
 * Here, we suppose that all the vertices of the initial parameterization are inside of the area covered
 * by the initial parameterization.
 *
 * \subsection method The algorithm is based on two models.
 * - In order to distribute the vertices inside, the spring model is introduced,
 * where "Hc" is used to set the coefficient of this type of force.
 * - In order to restrict all the vertices are within the area covered by the initial parameterization,
 *	 we suppose each interior vertex is forced by Coulomb forces (treated the vertex and the boundary of this area have the same charges.).
 * where "CoulombCoeff" is used to set the coefficient of this type of force.
 * - This algorithm is implemented in a discrete way. Thus, we should set "TimeSize" for each step.
 *\subsection otherParameterization If one wants to modify other parameterizations,
 * the initial parameterization should be given with reading in a parametric mesh data and coefficient data.
 *
 * \subsection usage Usage
 * - It can be used as follows:
 * \code
 * ./msplineSpringParameterization
 * \endcode
 * - We use the KSPSolver in PETSc process of solving classical Possion Equation
 *   to test if the modifed parameterization is good or not.
*/



// ////////////////////////////////////////////////////////////////////////////////////////
int main(int argc,char **args)
{

    unsigned int n_u=20;
    unsigned int n_v=20;


    ofstream ofProgress;
    ofProgress.open("ComputationProgress.txt");

    //============Initial Parametric Map=========
    unsigned int VerNum;
    unsigned int dimension=2;
    //Mesh
    Mesh * P_ParametricMesh=new Mesh;
    //Coeff
    vector<vector<double> > Coeff_Parametric_Map;
    unsigned int ExampleIndex=40;//default
    cout<<"Please put in the index of this example"<<endl;
    ofProgress<<"Please put in the index of this example"<<endl;

    cout<<"Remark:----------------------------------"<<endl;

    cout<<"44-->ModifySpringParameterization-->FourLeafClover"<<endl;
    cout<<"45-->ModifySpringParameterization-->MultiHoleStructure"<<endl;
    cout<<"46-->ModifySpringParameterization-->Circle"<<endl;
    cout<<"47-->ModifySpringParameterization-->Rectangle"<<endl;

    cout<<"10000--->For test"<<endl;


    //ofProgress
    ofProgress<<"44-->ModifySpringParameterization-->FourLeafClover"<<endl;
    ofProgress<<"45-->ModifySpringParameterization-->MultiHoleStructure"<<endl;
    ofProgress<<"46-->ModifySpringParameterization-->Circle"<<endl;
    ofProgress<<"47-->ModifySpringParameterization-->Rectangle"<<endl;


    ofProgress<<"10000--->For test"<<endl;

    cin>>ExampleIndex;

    ofProgress<<endl;
    ofProgress<<"The "<<ExampleIndex<<"-th example has been chosen."<<endl;

    //Read the mesh and coeff
    switch(ExampleIndex)
    {
    case 44://FourLeafClover
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/ModifyParameterization/FourLeafClover/ParametricMesh.txt");
        //P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Results/Example44/Example44FourLeafClover3/1_data/ParametricMesh.txt");
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/ModifyParameterization/FourLeafClover/ParametricCoeff4.txt", dimension, VerNum, Coeff_Parametric_Map);
        //Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Results/Example44/Example44FourLeafClover3/1_data/ParametricCoeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 45://MultiHoles
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/ModifyParameterization/MultiholeStructure/ParametricMesh.txt");
        //P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Results/Example45MultiStructure/3_data/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/ModifyParameterization/MultiholeStructure/ParametricCoeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        //Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Results/Example45MultiStructure/3_data/ParametricCoeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 46://Circle
        //P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/ModifyParameterization/Circle/ParametricMesh.txt");
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Results/Example46Circle/5_data/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        //Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/ModifyParameterization/Circle/ParametricCoeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Results/Example46Circle/5_data/ParametricCoeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 47://Rectangle

        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/ModifyParameterization/Squares/CompareSquare_Singularities/RegularSquare2/UniformPara/k=10/ParametricMesh.txt");
        //P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/ModifyParameterization/Squares/CompareSquare_Singularities/SingularitySquare/RefinedPara/k=15/ParametricMesh.txt");
        //P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/ModifyParameterization/Squares/CompareSquare_Singularities/RegularSquare/RefinedPara/k=18/ParametricMesh.txt");
        //P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/ModifyParameterization/Squares/CompareCond_Ex3_2Squares/ModifiedPara_P_1/ParametricMesh.txt");
        //P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/ModifyParameterization/Squares/CompareCond_Ex3_2Squares/InitialPara_P_0/ParametricMesh.txt");
        //P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/ModifyParameterization/Squares/CompareCond_Ex3_2Squares/IdParametric/ParametricMesh.txt");
        //P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/ModifyParameterization/Rectangle/ParametricMesh.txt");
        //P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Results/Example47Rectangle/5_data/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis

        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/ModifyParameterization/Squares/CompareSquare_Singularities/RegularSquare2/UniformPara/k=10/ParametricCoeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        //Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/ModifyParameterization/Squares/CompareSquare_Singularities/SingularitySquare/RefinedPara/k=15/ParametricCoeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        //Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/ModifyParameterization/Squares/CompareSquare_Singularities/RegularSquare/RefinedPara/k=18/ParametricCoeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        //Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/ModifyParameterization/Squares/CompareCond_Ex3_2Squares/ModifiedPara_P_1/ParametricCoeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        //Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/ModifyParameterization/Squares/CompareCond_Ex3_2Squares/InitialPara_P_0/ParametricCoeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        //Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/ModifyParameterization/Squares/CompareCond_Ex3_2Squares/IdParametric/ParametricCoeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        //Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/ModifyParameterization/Rectangle/ParametricCoeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        //Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Results/Example47Rectangle/5_data/ParametricCoeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 10000://test
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Results/stones/modifyParameterizationResult/InitialParameterizationdata/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Results/stones/modifyParameterizationResult/InitialParameterizationdata/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    default:
        cout<<"It is Default~"<<endl;
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/Documents/MyWorkDocument/TheoreyMSplines/OurWorks/PlanarDomainParametrization/Data/FourLeafClover/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/Documents/MyWorkDocument/TheoreyMSplines/OurWorks/PlanarDomainParametrization/Data/FourLeafClover/ParametricCoeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;
        cin.get();
        break;
    }

//    //Generate the initial Parametric map
    MSplineFunctions* P_ParametricMap=new MSplineFunctions(Coeff_Parametric_Map, *P_ParametricMesh);

//////===================================================================================
//    map<unsigned int, unsigned int> OrigialVertexToCurrentVertex222;

//    Mesh * P_FEMMesh2=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(9, OrigialVertexToCurrentVertex222);

//    map<unsigned int, unsigned int> ().swap(OrigialVertexToCurrentVertex222);

//    vector< vector<double> > CoeffMap222;
//    P_ParametricMap->CoutThisMSplineFunctionsOverAFinerMesh(9, P_ParametricMesh, CoeffMap222);
//    PrintParametricMap(CoeffMap222, P_FEMMesh2, "TwistRectangle_Axel.axl");
//    PrintParametricMapPositionInfo(CoeffMap222, P_FEMMesh2, "TwistRectangle_VerticesPosition_Axel.axl");
//    cin.get();
//    cin.get();
//    cin.get();
////////=================================================================================


//    P_ParametricMap->CoutThisMSplineFunctionsJacobianMeshAxelFile(P_ParametricMesh, 2, 2, "InitialParameterization_JacobianAxelMesh.axl", "InitialParameterization_Areas.txt");

    unsigned MaxIteNum;
    cout<<"Put in MaxIteNum"<<endl;
    cin>>MaxIteNum;

    ofProgress<<"MaxIteNum ="<<MaxIteNum<<endl;


    vector<vector<double> > NewCoeff_ParametricMap;
    MSplineFunctions* P_ParametricMap2=P_ParametricMap->ModifyParameterizationBySpringModelAndBoundaryFixing(0.1, 0.4, 0.01, 0.001, 2, 2, MaxIteNum, P_ParametricMesh, Coeff_Parametric_Map,NewCoeff_ParametricMap,
                                                                                                            "0_5_ModifyTheParameterizationComputationalProgress.txt",
                                                                                                            "0_5_SpringMethodVerticesDistanceConvergenceEpsilons.txt",
                                                                                                            "0_5_Initial_ModifiedPara.axl",
                                                                                                            "0_5_Initial_ModifiedPara_JacobianAxelMesh.axl",
                                                                                                            "0_5_Initial_ModifiedVerticesPosition.axl",
                                                                                                            "0_5_ModifiedPara_PhysicalAreas.txt",
                                                                                                            "0_5_H_CellbyCell.txt",
                                                                                                            "0_5_Theta_CellbyCell.txt",
                                                                                                            "0_5_Is_Concave_CellbyCell.txt");



    vector<vector<double> > ().swap(NewCoeff_ParametricMap);
    delete P_ParametricMap;
    P_ParametricMap = P_ParametricMap2;
    P_ParametricMap2=NULL;




 vector<vector<double> >().swap(Coeff_Parametric_Map);



    //===========================================================================================================================================
    //======================================================================
    Mat coeff_matrix;
    Vec load_vect;
    Vec m_solut;
    /*Initalize PETSc*/
    PetscInitialize(&argc, &args, (char*)0, NULL);
    //=======================================================================
    //Preparation: GuassPoint
    unsigned int Gp=0;
    cout<<"Input the number of Guass points:"<<endl;
    cin>>Gp;
    ofProgress<<"the number of Guass Points ="<<Gp<<endl;


    unsigned int u_gaussnum=Gp, v_gaussnum=Gp;
    unsigned int load_gaussnum=5;
    vector<double> gausspt, m_IntePt, m_loadw;

    //Weights/////////
    double **w;
    w=GaussPoints_Weights(u_gaussnum, load_gaussnum, gausspt, m_IntePt, m_loadw);

    vector<char *> JacobianFileNames;
    vector<char *> AreasFileNames;
    vector<char *> CoeffMatrixFileNames;
 //   vector<char *> MonitorFileNames;
 //   vector<char *> SingularValuesFileNames;


    //k=0;
    JacobianFileNames.push_back("ModifiedPMapJacobian_k=0.axl");
    AreasFileNames.push_back("ModifedPMapAreas_k=0.txt");
    CoeffMatrixFileNames.push_back("coeffmatrix_k0.m");

//    MonitorFileNames.push_back("Monitor_k=0.txt");
//    SingularValuesFileNames.push_back("singularvalues_k=0.txt");

    for(unsigned int i=0; i<1; i++)
{
//    PetscOptionsSetValue("-ksp_monitor_true_residual", MonitorFileNames[i]);
//    PetscOptionsSetValue("-ksp_monitor_singular_value", SingularValuesFileNames[i]);
    //The FEM Mesh
    map<unsigned int, unsigned int> OrigialVertexToCurrentVertex;
    //subdivide the initial parametric mesh
    unsigned int k=pow(2, i)-1;
    cout<<"the times of subdivision = "<<k<<endl;
    Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(k, OrigialVertexToCurrentVertex);

    ofProgress<<endl;
    ofProgress<<"the times of subdivision = "<<k<<endl;
    ofProgress<<"The Basis Number = "<<4*(P_FEMMesh->P_Vertices->size())<<endl;


    //Initial Mat coeff_matrix
    MatCreate(PETSC_COMM_WORLD, &coeff_matrix);
    MatSetSizes(coeff_matrix, PETSC_DECIDE, PETSC_DECIDE, P_FEMMesh->P_Vertices->size()*4, P_FEMMesh->P_Vertices->size()*4);
    MatSetFromOptions(coeff_matrix);
    MatMPIAIJSetPreallocation(coeff_matrix, 64, NULL, 64, NULL);
    MatSeqAIJSetPreallocation(coeff_matrix, PETSC_DEFAULT, NULL);
    MatSetUp(coeff_matrix);

    double k1=0;
    double k2=0;
    cout<<"Begin to generate the coeff_matrix:"<<endl;

    Generate_CoeffMatrix_Cell(u_gaussnum, v_gaussnum, coeff_matrix, m_IntePt, m_IntePt, w,
                                  P_FEMMesh, P_ParametricMesh, *P_ParametricMap, k1, k2, ExampleIndex);

    cout<<"Begin to initialize the load vector:"<<endl;
    Initialized_loadVector(load_vect, P_FEMMesh->P_Vertices->size());

    Generate_LoadVector(load_vect, gausspt, m_loadw, P_FEMMesh, P_ParametricMesh, *P_ParametricMap,
                        k1, k2, ExampleIndex);

    //Set Bases type
    cout<<"Set Bases type"<<endl;
    vector<unsigned int> BasesType;
    BasesType.resize(4*P_FEMMesh->P_Vertices->size(), 0);
    Set_Bases_type(P_FEMMesh, BasesType);

    //Modify LoadVector and CoeffMatrix
    MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);


    map<unsigned int, set<unsigned int> > HTable;
    Initialized_HTable_withoutHangingVertices_1(P_FEMMesh, HTable);

    cout<<"Modify the load vector and coeff matrix:"<<endl;
    double C=0.0;
    Modification_LoadVector_CoeffMatrix(C, BasesType, coeff_matrix, load_vect, HTable);
    MatSetOption(coeff_matrix, MAT_SYMMETRIC, PETSC_TRUE);
    MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);
    //===================================================
    //Save coeff_matrix as a matlab file
    PetscViewer viewer;
    PetscViewerASCIIOpen(PETSC_COMM_WORLD, CoeffMatrixFileNames[i], &viewer);
    PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_MATLAB);
    MatView(coeff_matrix, viewer);
    PetscViewerDestroy(&viewer);
    //=======================================================
    VecCreate(PETSC_COMM_WORLD, &m_solut);
    VecSetSizes(m_solut, PETSC_DECIDE, 4*P_FEMMesh->P_Vertices->size());
    VecSetFromOptions(m_solut);
    //===================================================
    KSP ksp;
    PC pc;
    KSPCreate(PETSC_COMM_WORLD, &ksp);
    KSPSetType(ksp, KSPGMRES);
    KSPSetOperators(ksp, coeff_matrix, coeff_matrix);


    PetscReal rtol=1.0e-16;
    PetscReal stol=1.0e-16;

    PetscInt maxits=10000;
    KSPSetTolerances(ksp, rtol, stol, PETSC_DEFAULT, maxits);

    KSPGetPC(ksp, &pc);
    PCSetType(pc, PCGAMG);

    KSPSetComputeEigenvalues(ksp, PETSC_TRUE);

    KSPSetFromOptions(ksp);

   KSPSolve(ksp, load_vect, m_solut);

   PetscInt its;
   KSPGetIterationNumber(ksp, &its);

   KSPConvergedReason reason;
   KSPGetConvergedReason(ksp, &reason);

   cout<<"KSPIterationNumber = "<<its<<endl;
   ofProgress<<"KSPIterationNumber = "<<its<<endl;

   cout<<"KSPConvergedReason = "<<reason<<endl;
   ofProgress<<"KSPConvergedReason = "<<reason<<endl;


   ofProgress<<"KSPSetTolerances(ksp, rtol, stol, PETSC_DEFAULT, maxits);"<<endl;
   ofProgress<<"rtol = "<<rtol<<endl;
   ofProgress<<"stol = "<<stol<<endl;
   ofProgress<<"maxits = "<<maxits<<endl;

   //Jacobian And Areas
   vector< vector<double> > CoeffMap;
   P_ParametricMap->CoutThisMSplineFunctionsOverAFinerMesh(k, P_ParametricMesh, CoeffMap);
   MSplineFunctions * P_ParametricFinerMap=new MSplineFunctions(CoeffMap, *P_FEMMesh);
   P_ParametricFinerMap->CoutThisMSplineFunctionsJacobianMeshAxelFile(P_FEMMesh, n_u, n_v, JacobianFileNames[i], AreasFileNames[i]);
   delete P_ParametricFinerMap;
   P_ParametricFinerMap=NULL;

   //DestoryKSP PC
   KSPDestroy(&ksp);

}
    MatDestroy(&coeff_matrix);
    VecDestroy(&load_vect);
    VecDestroy(&m_solut);
    /*Finish PETSc program*/
    PetscFinalize();
    //===========================================================================================================================================

    //==========================================================================================
    //Set the FileNames:
    //----at k: cellindex h_physical_max h_physical_min--->H_phy_k=*.txt
    //----at k: cellindex theta_max theta_min--->Theta_k=*.txt
    //----at k: cellindex Is_concave--->Is_concave_k=*.txt
    //
    //
    vector<char *> H_physical_FileNames;
    vector<char *> Theta_physical_FileNames;
    vector<char *> Is_Concave_FileNames;

    vector<char *> ModifiedComputationalProgress;
    vector<char *> Epsilon;
    vector<char *> ModifiedVerticesPosition_Axel_axl;
    vector<char *> ModifiedParameterizationWithSpringModel_PhysicalAreas;

    vector<char *> ModifiedParameterization_axl;
    vector<char *> ModifiedParameterization_JacobianAxelMesh_axl;


    //k=0;
    H_physical_FileNames.push_back("k=0_H_phy.txt");
    Theta_physical_FileNames.push_back("k=0_MaxCosTheta.txt");
    Is_Concave_FileNames.push_back("k=0_Is_concave.txt");

    ModifiedComputationalProgress.push_back("k=0_ModifiedComputationalProgress.txt");
    Epsilon.push_back("k=0_Epsilon.txt");
    ModifiedVerticesPosition_Axel_axl.push_back("k=0_ModifiedVerticesPosition.axl");
    ModifiedParameterizationWithSpringModel_PhysicalAreas.push_back("k=0_PhysicalAreas.txt");

    ModifiedParameterization_axl.push_back("k=0_ModifiedParameterization.axl");
    ModifiedParameterization_JacobianAxelMesh_axl.push_back("k=0_ModifiedParameterizationJacobian.axl");


    //k=1;
    H_physical_FileNames.push_back("k=1_H_phy.txt");
    Theta_physical_FileNames.push_back("k=1_MaxCosTheta.txt");
    Is_Concave_FileNames.push_back("k=1_Is_concave.txt");

    ModifiedComputationalProgress.push_back("k=1_ModifiedComputationalProgress.txt");
    Epsilon.push_back("k=1_Epsilon.txt");
    ModifiedVerticesPosition_Axel_axl.push_back("k=1_ModifiedVerticesPosition.axl");
    ModifiedParameterizationWithSpringModel_PhysicalAreas.push_back("k=1_PhysicalAreas.txt");

    ModifiedParameterization_axl.push_back("k=1_ModifiedParameterization.axl");
    ModifiedParameterization_JacobianAxelMesh_axl.push_back("k=1_ModifiedParameterizationJacobian.axl");

    //k=2;
    H_physical_FileNames.push_back("k=2_H_phy.txt");
    Theta_physical_FileNames.push_back("k=2_MaxCosTheta.txt");
    Is_Concave_FileNames.push_back("k=2_Is_concave.txt");

    ModifiedComputationalProgress.push_back("k=2_ModifiedComputationalProgress.txt");
    Epsilon.push_back("k=2_Epsilon.txt");
    ModifiedVerticesPosition_Axel_axl.push_back("k=2_ModifiedVerticesPosition.axl");
    ModifiedParameterizationWithSpringModel_PhysicalAreas.push_back("k=2_PhysicalAreas.txt");

    ModifiedParameterization_axl.push_back("k=2_ModifiedParameterization.axl");
    ModifiedParameterization_JacobianAxelMesh_axl.push_back("k=2_ModifiedParameterizationJacobian.axl");

    //k=3;
    H_physical_FileNames.push_back("k=3_H_phy.txt");
    Theta_physical_FileNames.push_back("k=3_MaxCosTheta.txt");
    Is_Concave_FileNames.push_back("k=3_Is_concave.txt");

    ModifiedComputationalProgress.push_back("k=3_ModifiedComputationalProgress.txt");
    Epsilon.push_back("k=3_Epsilon.txt");
    ModifiedVerticesPosition_Axel_axl.push_back("k=3_ModifiedVerticesPosition.axl");
    ModifiedParameterizationWithSpringModel_PhysicalAreas.push_back("k=3_PhysicalAreas.txt");

    ModifiedParameterization_axl.push_back("k=3_ModifiedParameterization.axl");
    ModifiedParameterization_JacobianAxelMesh_axl.push_back("k=3_ModifiedParameterizationJacobian.axl");

    //k=4;
    H_physical_FileNames.push_back("k=4_H_phy.txt");
    Theta_physical_FileNames.push_back("k=4_MaxCosTheta.txt");
    Is_Concave_FileNames.push_back("k=4_Is_concave.txt");

    ModifiedComputationalProgress.push_back("k=4_ModifiedComputationalProgress.txt");
    Epsilon.push_back("k=4_Epsilon.txt");
    ModifiedVerticesPosition_Axel_axl.push_back("k=4_ModifiedVerticesPosition.axl");
    ModifiedParameterizationWithSpringModel_PhysicalAreas.push_back("k=4_PhysicalAreas.txt");

    ModifiedParameterization_axl.push_back("k=4_ModifiedParameterization.axl");
    ModifiedParameterization_JacobianAxelMesh_axl.push_back("k=4_ModifiedParameterizationJacobian.axl");

    //k=5;
    H_physical_FileNames.push_back("k=5_H_phy.txt");
    Theta_physical_FileNames.push_back("k=5_MaxCosTheta.txt");
    Is_Concave_FileNames.push_back("k=5_Is_concave.txt");

    ModifiedComputationalProgress.push_back("k=5_ModifiedComputationalProgress.txt");
    Epsilon.push_back("k=5_Epsilon.txt");
    ModifiedVerticesPosition_Axel_axl.push_back("k=5_ModifiedVerticesPosition.axl");
    ModifiedParameterizationWithSpringModel_PhysicalAreas.push_back("k=5_PhysicalAreas.txt");

    ModifiedParameterization_axl.push_back("k=5_ModifiedParameterization.axl");
    ModifiedParameterization_JacobianAxelMesh_axl.push_back("k=5_ModifiedParameterizationJacobian.axl");

    //k=6;
    H_physical_FileNames.push_back("k=6_H_phy.txt");
    Theta_physical_FileNames.push_back("k=6_MaxCosTheta.txt");
    Is_Concave_FileNames.push_back("k=6_Is_concave.txt");

    ModifiedComputationalProgress.push_back("k=6_ModifiedComputationalProgress.txt");
    Epsilon.push_back("k=6_Epsilon.txt");
    ModifiedVerticesPosition_Axel_axl.push_back("k=6_ModifiedVerticesPosition.axl");
    ModifiedParameterizationWithSpringModel_PhysicalAreas.push_back("k=6_PhysicalAreas.txt");

    ModifiedParameterization_axl.push_back("k=6_ModifiedParameterization.axl");
    ModifiedParameterization_JacobianAxelMesh_axl.push_back("k=6_ModifiedParameterizationJacobian.axl");

//    //k=3;
//    H_physical_FileNames.push_back("k=3_H_phy.txt");
//    Theta_physical_FileNames.push_back("k=3_MaxCosTheta.txt");
//    Is_Concave_FileNames.push_back("k=3_Is_concave.txt");

//    ModifiedComputationalProgress.push_back("k=3_ModifiedComputationalProgress.txt");
//    Epsilon.push_back("k=3_Epsilon.txt");
//    ModifiedVerticesPosition_Axel_axl.push_back("k=3_ModifiedVerticesPosition.axl");
//    ModifiedParameterizationWithSpringModel_PhysicalAreas.push_back("k=3_PhysicalAreas.txt");

//    ModifiedParameterization_axl.push_back("k=3_ModifiedParameterization.axl");
//    ModifiedParameterization_JacobianAxelMesh_axl.push_back("k=3_ModifiedParameterizationJacobian.axl");

//    //k=7;
//    H_physical_FileNames.push_back("k=7_H_phy.txt");
//    Theta_physical_FileNames.push_back("k=7_MaxCosTheta.txt");
//    Is_Concave_FileNames.push_back("k=7_Is_concave.txt");

//    ModifiedComputationalProgress.push_back("k=7_ModifiedComputationalProgress.txt");
//    Epsilon.push_back("k=7_Epsilon.txt");
//    ModifiedVerticesPosition_Axel_axl.push_back("k=7_ModifiedVerticesPosition.axl");
//    ModifiedParameterizationWithSpringModel_PhysicalAreas.push_back("k=7_PhysicalAreas.txt");

//    ModifiedParameterization_axl.push_back("k=7_ModifiedParameterization.axl");
//    ModifiedParameterization_JacobianAxelMesh_axl.push_back("k=7_ModifiedParameterizationJacobian.axl");

//    //k=15;
//    H_physical_FileNames.push_back("k=15_H_phy.txt");
//    Theta_physical_FileNames.push_back("k=15_MaxCosTheta.txt");
//    Is_Concave_FileNames.push_back("k=15_Is_concave.txt");

//    ModifiedComputationalProgress.push_back("k=15_ModifiedComputationalProgress.txt");
//    Epsilon.push_back("k=15_Epsilon.txt");
//    ModifiedVerticesPosition_Axel_axl.push_back("k=15_ModifiedVerticesPosition.axl");
//    ModifiedParameterizationWithSpringModel_PhysicalAreas.push_back("k=15_PhysicalAreas.txt");

//    ModifiedParameterization_axl.push_back("k=15_ModifiedParameterization.axl");
//    ModifiedParameterization_JacobianAxelMesh_axl.push_back("k=15_ModifiedParameterizationJacobian.axl");
    //===========================================================================================


    Mesh *P_FEMMesh=new Mesh;
    P_FEMMesh=NULL;

    MSplineFunctions* P_ParametricMap_1;//(NULL);//=new MSplineFunctions;
    P_ParametricMap_1=NULL;

    MSplineFunctions* P_ParametricMap_Modified;//(NULL);
    P_ParametricMap_Modified=NULL;


    unsigned int kk;
    cout<<"Put in subdivision time (1--6)"<<endl;
    cin>>kk;

    for(unsigned int i=kk; i<kk+1; i++)
    {
        //The FEM Mesh
        map<unsigned int, unsigned int> OrigialVertexToCurrentVertex;
        //subdivide the parametric mesh
        P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(1, OrigialVertexToCurrentVertex);

        map<unsigned int, unsigned int> ().swap(OrigialVertexToCurrentVertex);

        vector< vector<double> > CoeffMap;
        P_ParametricMap->CoutThisMSplineFunctionsOverAFinerMesh(1, P_ParametricMesh, CoeffMap);


        P_ParametricMap_1=new MSplineFunctions(CoeffMap, *P_FEMMesh);



        double k=1;//=pow(2, i)-1;
        //cout<<"the times of subdivision = "<<k<<endl;

        vector<vector<double> > NewCoeff_ParametricMap2;



        MSplineFunctions* P_ParametricMap_Modified =P_ParametricMap_1->ModifyParameterizationBySpringModelAndBoundaryFixing(0.1, 0.4, 0.01, 0.00001, 2, 2, 100,
                                                                                                                            P_FEMMesh, CoeffMap, NewCoeff_ParametricMap2,
                                                                                                                            ModifiedComputationalProgress[i],
                                                                                                                            Epsilon[i],
                                                                                                                            ModifiedParameterization_axl[i],
                                                                                                                            ModifiedParameterization_JacobianAxelMesh_axl[i],
                                                                                                                            ModifiedVerticesPosition_Axel_axl[i],
                                                                                                                            ModifiedParameterizationWithSpringModel_PhysicalAreas[i],
                                                                                                                            H_physical_FileNames[i],
                                                                                                                            Theta_physical_FileNames[i],
                                                                                                                            Is_Concave_FileNames[i]);

        PrintParametricMeshCoeff(NewCoeff_ParametricMap2, P_FEMMesh);

        vector< vector<double> > ().swap(CoeffMap);
        vector<vector<double> > ().swap(NewCoeff_ParametricMap2);
        delete P_FEMMesh;
        P_FEMMesh=NULL;

        delete P_ParametricMap_1;
        P_ParametricMap_1=NULL;

        delete P_ParametricMap_Modified;
        P_ParametricMap_Modified=NULL;


        //P_ParametricMap->CoutThisMSplineFunctionsJacobianMeshAxelFile(P_ParametricMesh,2,2,ModifiedParameterization_JacobianAxelMesh_axl[i],ModifiedParameterizationWithSpringModel_PhysicalAreas[i]);



        ofProgress<<endl;




    }


    ofProgress.close();

//=================================================================================================
     return 0;
 }


