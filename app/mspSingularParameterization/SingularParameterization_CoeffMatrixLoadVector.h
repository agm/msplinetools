#ifndef SINGULARPARAMETERIZATION_COEFFMATRIXLOADVECTOR_H
#define SINGULARPARAMETERIZATION_COEFFMATRIXLOADVECTOR_H


#include "msp/Mesh.h"
#include "msp/MSplineFunctions.h"
#include "msp/BasesCell.h"
#include "msp/Bases.h"
#include "msp/FEM_Functions.h"

#include "SingularParameterization_Functions.h"
#include "msp/Functions.h"

#include "SingularParameterization_DynamicFunctionValues.h"



#include "petsc.h"


void Classical_Generate_CoeffMatrix_Cell(unsigned int u_gaussnum, unsigned int v_gaussnum, Mat &coeff_matrix,
                               vector<double> u_gausspt, vector<double> v_gausspt, double **w,
                               const Mesh*  P_FEMMesh, const Mesh * P_ParametricMesh,
                               const MSplineFunctions &ParametricMap)
{

    MatZeroEntries(coeff_matrix);

    //----
    //-----
    //vector<double> u,v;//store Gauss points in a cell
    /*--Compute the stiff matrix(the upper triangular matrix)--*/
    vector<double> m_kDervalu;
    vector<double> m_kDervalv;
    vector<double> m_lDervalu;
    vector<double> m_lDervalv;
    vector<vector<double> > m_JPart;
    vector<double> detJ;
    double m_temp;

    PetscInt I;
    PetscInt J;

    for(unsigned int i=0; i<P_FEMMesh->P_Cells->size(); i++)//fill in the coeff matrix based on cells
    {
        //===========Guass Points============================
        double u0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double u1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double v0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double v1=P_FEMMesh->P_Cells->at(i).CellSize[3];

        vector<Coordinate*> Pcoordinate_Ps;
        for (unsigned int j=0; j<u_gaussnum; j++)
        {
            for (unsigned int l=0; l<v_gaussnum; l++)
            {
                Coordinate * CoorP=new Coordinate;
                //CoorP->xy[0]=u[j];
                CoorP->xy[0]=((u1-u0) * u_gausspt[j] + u0 + u1) * 0.5;
                //CoorP->xy[1]=v[l];
                CoorP->xy[1]=((v1-v0) * v_gausspt[l] + v1 + v0) * 0.5;
                Pcoordinate_Ps.push_back(CoorP);
                CoorP=NULL;
            }
        }

        //============Bases over this cell===============================================
        BasesCell * P_BC=new BasesCell(i, *P_FEMMesh);//BasesCell(unsigned int index_Cell, const Mesh & CurrentMesh);

        for (unsigned int k=0; k<P_BC->P_Bases_Cell.size(); k++)//the FEM-bases over the i-th cells
        {
            m_kDervalu.clear();
            m_kDervalv.clear();//m_val.clear();
            //==========================
            value_Cell_Hermite(1, 0, Pcoordinate_Ps, P_BC->P_Bases_Cell[k]->P_HermiteData_Cells[0], m_kDervalu, *P_FEMMesh);
            value_Cell_Hermite(0, 1, Pcoordinate_Ps, P_BC->P_Bases_Cell[k]->P_HermiteData_Cells[0], m_kDervalv, *P_FEMMesh);

            SingularParameterization_MSplineParametricMap(Pcoordinate_Ps, m_JPart, detJ,
                                                          P_FEMMesh->P_Cells->at(i).ItsParent, ParametricMap,
                                                                       P_ParametricMesh);

            //==========================
            for(unsigned int kk = 0; kk < P_BC->P_Bases_Cell.size(); kk++)
            {
                if(P_BC->P_Bases_Cell[kk]->Index_basis >= P_BC->P_Bases_Cell[k]->Index_basis)
                {
                    m_lDervalu.clear();
                    m_lDervalv.clear();
                    value_Cell_Hermite(1, 0, Pcoordinate_Ps, P_BC->P_Bases_Cell[kk]->P_HermiteData_Cells[0], m_lDervalu, *P_FEMMesh);
                    value_Cell_Hermite(0, 1, Pcoordinate_Ps, P_BC->P_Bases_Cell[kk]->P_HermiteData_Cells[0], m_lDervalv, *P_FEMMesh);

                    //================================================================================
                    m_temp = 0;
                    for(unsigned int l = 0; l < u_gaussnum*v_gaussnum; l++)
                    {
                        int nn = l / v_gaussnum;
                        int mm = l % v_gaussnum;

                        double xx1=m_kDervalu[l];
                        double yy1=m_kDervalv[l];
                        double xx2=m_lDervalu[l];
                        double yy2=m_lDervalv[l];
                        double J11=m_JPart[l][0];
                        double J12=m_JPart[l][1];
                        double J21=m_JPart[l][2];
                        double J22=m_JPart[l][3];

                        m_temp += w[nn][mm]*(xx1*J11*xx2+yy1*J21*xx2+xx1*J12*yy2+yy1*J22*yy2);
                    }
                    I=P_BC->P_Bases_Cell[k]->Index_basis;
                    J=P_BC->P_Bases_Cell[kk]->Index_basis;
                    PetscScalar v=m_temp * (u1-u0) * (v1-v0) / 4.0;



                    if(I!=J)
                    {
                        MatSetValues(coeff_matrix, 1, &I, 1, &J, &v, ADD_VALUES);
                        MatSetValues(coeff_matrix, 1, &J, 1, &I, &v, ADD_VALUES);
                    }
                    else
                    {
                        MatSetValues(coeff_matrix, 1, &I, 1, &J, &v, ADD_VALUES);
                    }
                }
            }
        }
        delete P_BC;
        P_BC=NULL;

    }

}

void Classical_Generate_LoadVector(Vec &load_vect, vector<double>& gausspt, vector<double>& gaussw, const Mesh * P_FEMMesh,
                         const Mesh * P_ParametricMesh, MSplineFunctions & MSplineParametricMap,
                         unsigned int ExampleIndex)
{
    //---------------------------------------------
    int gaussnum = gausspt.size();

    VecZeroEntries(load_vect);

    PetscScalar v;
    PetscInt I;

    for(unsigned int i=0;i<P_FEMMesh->P_Cells->size();i++)// Based on each cell
    {
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];
        //=============================================================
        vector<double> funval, m_Baseval;
        double tmpx;
        vector<vector<double> > m_JPart;
        //==============================================================
        vector<Coordinate*> Pcoordinate_Ps;
        for (int l=0; l<gaussnum; l++)
        {
            for (int j=0; j<gaussnum; j++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=(x1+x0+(x1-x0)*gausspt[l]) * 0.5;//u[l];
                CoorP->xy[1]=(y0+y1+(y1-y0)*gausspt[j]) * 0.5;//v[j];
                Pcoordinate_Ps.push_back(CoorP);
            }
        }
        //===============================================================
        MSplineFunctions * P_MSplineParametricMap= & MSplineParametricMap;


        SingularParameterization_ComputeFunval_MSplineParametricMap(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent,
                                           funval, 1, *P_ParametricMesh, P_MSplineParametricMap,
                                           ExampleIndex);//The value of right side
        //===============================================================

        BasesCell *P_BC=new BasesCell(i, *P_FEMMesh);//Bases over the i-th cell

        for(unsigned int k = 0; k < P_BC->P_Bases_Cell.size(); k++)
        {
            tmpx = 0;

            vector<double> DetJ;

            SingularParameterization_MSplineParametricMap(Pcoordinate_Ps, m_JPart, DetJ,
                                                    P_FEMMesh->P_Cells->at(i).ItsParent, MSplineParametricMap,
                                                    P_ParametricMesh);

// 			//==============
            m_Baseval.clear();

            value_Cell_Hermite(0, 0, Pcoordinate_Ps, P_BC->P_Bases_Cell[k]->P_HermiteData_Cells[0], m_Baseval, *P_FEMMesh);

            for(unsigned int j = 0; j < m_Baseval.size(); j++)
            {
                int nn = j / gaussnum;
                int mm = j % gaussnum;
                tmpx += gaussw[nn] * gaussw[mm] * funval[j] * m_Baseval[j] * DetJ[j];
            }
            //load_vect[P_BC->P_Bases_Cell[k]->Index_basis] += tmpx * (x1-x0) * (y1-y0)/ 4;
            I=P_BC->P_Bases_Cell[k]->Index_basis;
            v=tmpx * (x1-x0) * (y1-y0)/ 4;

            VecSetValues(load_vect, 1, &I, &v, ADD_VALUES);
        }
    }
}


void SingularParameterization_Generate_CoeffMatrix_Cell(unsigned int u_gaussnum, unsigned int v_gaussnum, Mat &coeff_matrix,
                               vector<double> u_gausspt, vector<double> v_gausspt, double **w,
                               const Mesh*  P_FEMMesh, const Mesh * P_ParametricMesh,
                               const MSplineFunctions &ParametricMap,  const set<unsigned> &SingularCellIndice,
                               const set<unsigned>& SingularVertexIndiceFEM)
{

    MatZeroEntries(coeff_matrix);
    //-----
    //vector<double> u,v;//store Gauss points in a cell
    /*--Compute the stiff matrix(the upper triangular matrix)--*/
    vector<double> m_kDervalu;
    vector<double> m_kDervalv;
    vector<double> m_lDervalu;
    vector<double> m_lDervalv;
    vector<vector<double> > m_JPart;
    vector<double> detJ;
    double m_temp;

    PetscInt I;
    PetscInt J;

    for(unsigned int i=0; i<P_FEMMesh->P_Cells->size(); i++)//fill in the coeff matrix based on cells
    {
        //=======Flag======================================
        bool Flag=false; //if Flag=false, the basis values on this cell are 1.
        if(SingularCellIndice.find(i)==SingularCellIndice.end())
        {
            Flag=true;
        }
        //cout<<"Flag="<<Flag<<endl;
        //===========Guass Points============================
        double u0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double u1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double v0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double v1=P_FEMMesh->P_Cells->at(i).CellSize[3];

        vector<Coordinate*> Pcoordinate_Ps;
        for (unsigned int j=0; j<u_gaussnum; j++)
        {
            for (unsigned int l=0; l<v_gaussnum; l++)
            {
                Coordinate * CoorP=new Coordinate;
                //CoorP->xy[0]=u[j];
                CoorP->xy[0]=((u1-u0) * u_gausspt[j] + u0 + u1) * 0.5;
                //CoorP->xy[1]=v[l];
                CoorP->xy[1]=((v1-v0) * v_gausspt[l] + v1 + v0) * 0.5;
                Pcoordinate_Ps.push_back(CoorP);
                CoorP=NULL;
            }
        }

        //============Bases over this cell===============================================
        BasesCell * P_BC=new BasesCell(i, *P_FEMMesh);//BasesCell(unsigned int index_Cell, const Mesh & CurrentMesh);

        for (unsigned int k=0; k<P_BC->P_Bases_Cell.size(); k++)//the FEM-bases over the i-th cells
        {
            m_kDervalu.clear();
            m_kDervalv.clear();//m_val.clear();
            //==========================

            //============================
            unsigned int vindex=P_BC->P_Bases_Cell[k]->Index_basis/4;
            //cout<<"vindex="<<vindex<<endl;
            bool Flag2=true;
            if(SingularVertexIndiceFEM.find(vindex)!=SingularVertexIndiceFEM.end()&&P_BC->P_Bases_Cell[k]->Index_basis%4==0)
            {
                Flag2=false;
            }
            //============================

            if(Flag||Flag2)
            {
                value_Cell_Hermite(1, 0, Pcoordinate_Ps, P_BC->P_Bases_Cell[k]->P_HermiteData_Cells[0], m_kDervalu, *P_FEMMesh);
                value_Cell_Hermite(0, 1, Pcoordinate_Ps, P_BC->P_Bases_Cell[k]->P_HermiteData_Cells[0], m_kDervalv, *P_FEMMesh);
            }
            else//(!Flage && !Flag2)// the basis at an extraordinary vertex on a singular cell
            {
                cout<<"P_BC->P_Bases_Cell[k]="<<P_BC->P_Bases_Cell[k]->Index_basis<<endl;
                //Derivates are zeros
                m_kDervalu.resize(Pcoordinate_Ps.size(),0.0);
                m_kDervalv.resize(Pcoordinate_Ps.size(),0.0);
            }
            SingularParameterization_MSplineParametricMap(Pcoordinate_Ps, m_JPart, detJ,
                                P_FEMMesh->P_Cells->at(i).ItsParent, ParametricMap,
                                             P_ParametricMesh);
            //==========================
            for(unsigned int kk = 0; kk < P_BC->P_Bases_Cell.size(); kk++)
            {
                //============================
                unsigned int vindex=P_BC->P_Bases_Cell[kk]->Index_basis/4;
                //cout<<"vindex="<<vindex<<endl;
                bool Flag2=true;
                if(SingularVertexIndiceFEM.find(vindex)!=SingularVertexIndiceFEM.end()&&P_BC->P_Bases_Cell[kk]->Index_basis%4==0)
                {
                    Flag2=false;
                }
                //============================

                if(P_BC->P_Bases_Cell[kk]->Index_basis >= P_BC->P_Bases_Cell[k]->Index_basis)
                {
                    m_lDervalu.clear();
                    m_lDervalv.clear();

                    if(Flag||Flag2)
                    {
                        value_Cell_Hermite(1, 0, Pcoordinate_Ps, P_BC->P_Bases_Cell[kk]->P_HermiteData_Cells[0], m_lDervalu, *P_FEMMesh);
                        value_Cell_Hermite(0, 1, Pcoordinate_Ps, P_BC->P_Bases_Cell[kk]->P_HermiteData_Cells[0], m_lDervalv, *P_FEMMesh);
                    }
                    else
                    {
                        cout<<"P_BC->P_Bases_Cell[kk] = "<<P_BC->P_Bases_Cell[kk]->Index_basis<<endl;
                        //Derivates are zeros
                        m_lDervalu.resize(Pcoordinate_Ps.size(),0.0);
                        m_lDervalv.resize(Pcoordinate_Ps.size(),0.0);
                    }

                    //================================================================================
                    m_temp = 0;
                    for(unsigned int l = 0; l < u_gaussnum*v_gaussnum; l++)
                    {
                        int nn = l / v_gaussnum;
                        int mm = l % v_gaussnum;

                        double xx1=m_kDervalu[l];
                        double yy1=m_kDervalv[l];
                        double xx2=m_lDervalu[l];
                        double yy2=m_lDervalv[l];
                        double J11=m_JPart[l][0];
                        double J12=m_JPart[l][1];
                        double J21=m_JPart[l][2];
                        double J22=m_JPart[l][3];

                        m_temp += w[nn][mm]*(xx1*J11*xx2+yy1*J21*xx2+xx1*J12*yy2+yy1*J22*yy2);
                    }
                    I=P_BC->P_Bases_Cell[k]->Index_basis;
                    J=P_BC->P_Bases_Cell[kk]->Index_basis;
                    PetscScalar v=m_temp * (u1-u0) * (v1-v0) / 4.0;



                    if(I!=J)
                    {
                        MatSetValues(coeff_matrix, 1, &I, 1, &J, &v, ADD_VALUES);
                        MatSetValues(coeff_matrix, 1, &J, 1, &I, &v, ADD_VALUES);
                    }
                    else
                    {
                        MatSetValues(coeff_matrix, 1, &I, 1, &J, &v, ADD_VALUES);
                    }
                }
            }
        }



        delete P_BC;
        P_BC=NULL;

    }

}





//LoadVector
void SingularParameterization_Generate_LoadVector(Vec &load_vect, vector<double>& gausspt, vector<double>& gaussw,
                                            Mesh * P_FEMMesh, const Mesh * P_ParametricMesh,
                                            MSplineFunctions & MSplineParametricMap,
                                            const set<unsigned int> SingularCellIndice,
                                            const set<unsigned int> SingularVertexIndiceFEM,
                                            unsigned int ExampleIndex)
{
    int gaussnum = gausspt.size();


    VecZeroEntries(load_vect);

    PetscScalar v;
    PetscInt I;

    for(unsigned int i=0;i<P_FEMMesh->P_Cells->size();i++)// Based on each cell
    {
         bool Flag=false;
         if(SingularCellIndice.find(i)==SingularCellIndice.end())
         {
             Flag=true;
         }

        // cout<<"Flag="<<Flag<<endl;
        //===========================================================
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];
        //=============================================================
        vector<double> funval, m_Baseval;
        double tmpx;
        vector<vector<double> > m_JPart;
        //==============================================================
        vector<Coordinate*> Pcoordinate_Ps;
        for (int l=0; l<gaussnum; l++)
        {
            for (int j=0; j<gaussnum; j++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=(x1+x0+(x1-x0)*gausspt[l]) * 0.5;//u[l];
                CoorP->xy[1]=(y0+y1+(y1-y0)*gausspt[j]) * 0.5;//v[j];
                Pcoordinate_Ps.push_back(CoorP);
            }
        }
        //===============================================================
        MSplineFunctions * P_MSplineParametricMap= & MSplineParametricMap;


        //cout<<"SingularParameterization_Generate_LoadVector:SingularParameterization_ComputeFunval_MSplineParametricMap"<<endl;
        SingularParameterization_ComputeFunval_MSplineParametricMap(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent,
                                           funval, 1, *P_ParametricMesh, P_MSplineParametricMap,
                                           ExampleIndex);
        //===============================================================

        BasesCell *P_BC=new BasesCell(i, *P_FEMMesh);//Bases over the i-th cell

        for(unsigned int k = 0; k < P_BC->P_Bases_Cell.size(); k++)
        {
            tmpx = 0;

            vector<double> DetJ;

            SingularParameterization_MSplineParametricMap(Pcoordinate_Ps, m_JPart, DetJ,
                                                    P_FEMMesh->P_Cells->at(i).ItsParent, MSplineParametricMap,
                                                    P_ParametricMesh);

// 			//==============
            m_Baseval.clear();

            unsigned int vindex=P_BC->P_Bases_Cell[k]->Index_basis/4;
            //cout<<"vindex="<<vindex<<endl;
            bool Flag2=true;
            if(SingularVertexIndiceFEM.find(vindex)!=SingularVertexIndiceFEM.end()&&P_BC->P_Bases_Cell[k]->Index_basis%4==0)
            {
                Flag2=false;
            }

            if(Flag||Flag2)
            {
                 value_Cell_Hermite(0, 0, Pcoordinate_Ps, P_BC->P_Bases_Cell[k]->P_HermiteData_Cells[0], m_Baseval, *P_FEMMesh);
            }
            else
            {
                //basis function values are 1 at Gauss points
                m_Baseval.resize(Pcoordinate_Ps.size(),1.0);
            }

            for(unsigned int j = 0; j < m_Baseval.size(); j++)
            {
                int nn = j / gaussnum;
                int mm = j % gaussnum;

                tmpx += gaussw[nn] * gaussw[mm] * funval[j] * m_Baseval[j] * DetJ[j];

            }
            //load_vect[P_BC->P_Bases_Cell[k]->Index_basis] += tmpx * (x1-x0) * (y1-y0)/ 4;
            I=P_BC->P_Bases_Cell[k]->Index_basis;
            v=tmpx * (x1-x0) * (y1-y0)/ 4;

            VecSetValues(load_vect, 1, &I, &v, ADD_VALUES);
        }
    }
}



#endif
