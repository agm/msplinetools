// MSPSingularParameterization.cpp
//===========================================

//===========================================

#include <iostream>
#include <fstream>
#include <map>
#include <time.h>


#include<stdio.h>
#include<dirent.h>

//PETSc
#include <petsc.h>
#include <petscksp.h>

//Spline
#include "msp/Mesh.h"
#include "msp/BasesVertex.h"
#include "msp/Bases.h"
#include "msp/BasesCell.h"
#include "msp/MSplineFunction.h"
#include "msp/PointSet.h"
#include "msp/Coordinate.h"
#include "msp/MSplineFunctions.h"
#include "msp/Functions.h"

//FEM
//#include "msp/FEM_ErrorFunctions.h"
//#include "msp/FEM_Functions.h"
//#include "msp/FEM_DynamicFunctions.h"
//#include "msp/FEM_CoeffMatrixLoadVector.h"
//#include "msp/FEM_Print.h"
#include "SingularParameterization_CoeffMatrixLoadVector.h"
#include "SingularParameterization_Functions.h"
#include "SingularParameterization_ErrorFunctions.h"

using namespace std;


int main(int argc,char **args)
{
    PetscInt m;
    PetscInt n = 4;
    Vec load_vect, m_solut;
    Mat coeff_matrix;// the stiff matrix

    KSP ksp;
    PC pc;

    KSPConvergedReason reason;

    ofstream OfTime;
    OfTime.open("Time.txt");

        //-------------FEM Solver----------------------
        cout<<"Generate Guass points and Weights"<<endl;

        OfTime<<endl;
        OfTime<<"Generate Guass points and Weights"<<endl;
        double t0=(double)clock();

        //Preparation: GuassPoint
        unsigned int Gp=0;
        cout<<"Input the number of Guass points:"<<endl;
        cin>>Gp;

        unsigned int u_gaussnum=Gp, v_gaussnum=Gp;
        unsigned int load_gaussnum=Gp;
        vector<double> gausspt, m_IntePt, m_loadw;
        //Weights/////////
        double **w;
         w=GaussPoints_Weights(u_gaussnum, load_gaussnum, gausspt, m_IntePt, m_loadw);

        double t1=(double)clock();
        cout<<"\"Generate Guass points and Weights\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<endl;

        OfTime<<"\"Generate Guass points and Weights\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<endl;
        //================================================
        //Read the ParametricMap
        //Mesh
        cout<<"Read the mesh, subdivide it and parametric map"<<endl;

        //----------
        OfTime<<endl;
        OfTime<<"Read the mesh, subdivide it and parametric map"<<endl;

        Mesh * P_ParametricMesh=new Mesh;
        //Coeff
        vector<vector<double> > Coeff_Parametric_Map;
        unsigned int ExampleIndex=4;//default

       // cout<<"Please put in the index of this example"<<endl;

        cout<<"Remark:----------------------------------"<<endl;

        cout<<"1-->Crack Rectangule (known solution)"<<endl;
        cout<<"2-->L-shape exact solution"<<endl;
        cout<<"3--->test:triangle with a center singular vertex"<<endl;
        cout<<"4--->test:square with a center singular vertex by different parameterizations"<<endl;

        cout<<"Put in the ExampleIndex:"<<endl;
        cin>>ExampleIndex;




        OfTime<<"mspSingularParameterization"<<endl;
        OfTime<<"Please put in the index of this example"<<endl;

        OfTime<<"1-->Crack Rectangular (known solution)"<<endl;
        OfTime<<"2-->L-shape exact solution"<<endl;
        OfTime<<"3--->test:triangle with a center singular vertex"<<endl;
        OfTime<<"4--->test:square with a center singular vertex by different parameterizations"<<endl;

        OfTime<<"The mesh of this example that has been chosen: "<< ExampleIndex<<endl;


        unsigned int dimension=2;
        unsigned int VerNum;

        /*
         * Check the current work dictionary
    */

        switch(ExampleIndex)
        {


        case 1://CrackRectangular
            P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/SingularityTest_CrackRectangular/4/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/SingularityTest_CrackRectangular/4/ParametricCoeffs.txt",
                                       dimension, VerNum, Coeff_Parametric_Map);
            break;

        case 2://L-shape exact solution
            P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/SingularityTest_LShape/1/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/SingularityTest_LShape/1/ParametricCoeffs.txt",
                                       dimension, VerNum, Coeff_Parametric_Map);
            break;

        case 3://test-triangle with a center singular vertex
            P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/Example0_0/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/Example0_0/ParametricCoeffs.txt",
                                       dimension, VerNum, Coeff_Parametric_Map);
            break;
        case 4://squares with a center singular vertex
            P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/SquareDifferentSingularPara/3/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/SquareDifferentSingularPara/3/ParametricCoeffs.txt",
                                       dimension, VerNum, Coeff_Parametric_Map);
            break;

        default:
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example4/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/Example4/ParametricCoeffs.txt",
                                       dimension, VerNum, Coeff_Parametric_Map);
            break;
        }

        /* OutPut the parametricMesh for checking*/
        //P_ParametricMesh->Cout_Mesh_File();

        double InitialCellSize=1.0;
        if(P_ParametricMesh->P_Cells->size())
        {
            InitialCellSize=P_ParametricMesh->P_Cells->at(0).CellSize[1]-P_ParametricMesh->P_Cells->at(0).CellSize[0];
        }

        /*Generate a Parametric map*/
        MSplineFunctions ParametricMap(Coeff_Parametric_Map, *P_ParametricMesh);

        //==========SingularVertice of P_ParametricMesh============================
        set<unsigned int> SingularVertexIndice;

        SingularVertexIndice_ParametricMesh(Coeff_Parametric_Map, SingularVertexIndice);

        //=========================================================================
        cout<<"SingularVertexIndice.size()="<<SingularVertexIndice.size()<<endl;

        for (set<unsigned>::iterator ii=SingularVertexIndice.begin(); ii!=SingularVertexIndice.end(); ii++)
        {
            cout<<*ii<<" ";
        }

        cin.get();
        //=========================================================================
        /*OutPut this Parametric map*/
        //ParametricMap.Cout_MSplineFunctions();
        //========================================
        unsigned int k;
        cout<<"Put in the value k for determining the times of subdivision:"<<endl;
        cin>>k;
        //---------------------
        OfTime<<endl;
        OfTime<<"Put in the value k for determining the times of subdivision:"<<endl;
        OfTime<<"k = "<<k<<endl;
        //---------------------

        //The FEM Mesh
        map<unsigned int, unsigned int> OriginalVertexToCurrentVertex;

        //subdivide the initial parametric mesh
        Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(k, OriginalVertexToCurrentVertex);

        //----------------
        //SingularVertexIndice on a P_ParametricMesh -> SingularVertexIndice on the FEMMesh
        set<unsigned> SingularVertexIndiceFEM;
        SingularVertexIndice_OnFEMMesh(SingularVertexIndiceFEM, SingularVertexIndice, OriginalVertexToCurrentVertex);
        //----------------///

        //----------
        set<unsigned> ().swap(SingularVertexIndice);
        map<unsigned int, unsigned int> ().swap(OriginalVertexToCurrentVertex);
        //----------
        set<unsigned> SingularCellIndice;//!
        P_FEMMesh->SingularCellIndice_Finder(SingularCellIndice, SingularVertexIndiceFEM);
        //
        //
	
        cout<<"The Basis Number = "<<4*(P_FEMMesh->P_Vertices->size())<<endl;
        cout<<"SingularCellIndice.size()="<<SingularCellIndice.size()<<endl;
	
        m = P_FEMMesh->P_Vertices->size();

        //==============================PETSC===================================

        PetscInitialize(&argc, &args, (char*) 0, NULL);
        PetscOptionsGetInt(NULL, "-m", &m, NULL);
        PetscOptionsGetInt(NULL, "-n", &n, NULL);

        cout<<"Initialized coeffMatrix and Generate_coeffMatrix"<<endl;

        OfTime<<endl;
        OfTime<<"Initialized coeffMatrix and Generate_coeffMatrix"<<endl;

        t0=(double)clock();

        MatCreate(PETSC_COMM_WORLD, &coeff_matrix);
       // PetscInt matrix_size=m*n;
        MatSetSizes(coeff_matrix, PETSC_DECIDE, PETSC_DECIDE, m*n, m*n);
        MatSetFromOptions(coeff_matrix);
        MatMPIAIJSetPreallocation(coeff_matrix, 64, NULL, 64, NULL);
        MatSeqAIJSetPreallocation(coeff_matrix, PETSC_DEFAULT, NULL);
        MatSetUp(coeff_matrix);

        map<unsigned int, set<unsigned int> > HTable;
        //Initialized_HTable_withoutHangingVertices(P_FEMMesh, HTable);
        cout<<"Generate_CoeffMatrix_Based on Cells"<<endl;
        /*SingularParameterization_Generate_CoeffMatrix_Cell(u_gaussnum, v_gaussnum, coeff_matrix, m_IntePt, m_IntePt, w,
                                  P_FEMMesh, P_ParametricMesh, ParametricMap, SingularCellIndice,SingularVertexIndiceFEM);*/
        Classical_Generate_CoeffMatrix_Cell(u_gaussnum, v_gaussnum, coeff_matrix, m_IntePt, m_IntePt, w,
                                  P_FEMMesh, P_ParametricMesh, ParametricMap);


        t1=(double)clock();
        cout<<"\"Initialized coeffMatrix and Generate_coeffMatrix\" costs ="<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
        OfTime<<"\"Initialized coeffMatrix and Generate_coeffMatrix\" costs ="<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

        //load_vect
        //vector<double> load_vect;
        cout<<"Initialized the load Vector and Generate the load Vector"<<endl;

        OfTime<<endl;
        OfTime<<"Initialized the load Vector and Generate the load Vector"<<endl;
        t0=(double)clock();


        Initialized_loadVector(load_vect, P_FEMMesh->P_Vertices->size());

        /*SingularParameterization_Generate_LoadVector(load_vect, gausspt, m_loadw, P_FEMMesh, P_ParametricMesh, ParametricMap, SingularCellIndice, SingularVertexIndiceFEM, ExampleIndex);
         */
        Classical_Generate_LoadVector(load_vect, gausspt, m_loadw, P_FEMMesh, P_ParametricMesh, ParametricMap, ExampleIndex);

        //
        t1=(double)clock();
        cout<<"Finished and it costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
        OfTime<<"Finished and it costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
        //
        set<unsigned> ().swap(SingularVertexIndiceFEM);
        //Set Bases type
        cout<<"Set Bases type"<<endl;
        t0=(double)clock();
        vector<unsigned int> BasesType;
        BasesType.resize(4*P_FEMMesh->P_Vertices->size(), 0);
        SingularParameterization_Set_Bases_type(P_FEMMesh, BasesType);


        cin.get();

        t1=(double)clock();
        cout<<"\"Set Bases type\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

        OfTime<<endl;
        OfTime<<"There are "<< 4*P_FEMMesh->P_Vertices->size()<<" bases"<<endl;
        OfTime<<"\"Set Bases type\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

        //Modify LoadVector and CoeffMatrix
        double C=0.0;

        MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);

        VecAssemblyBegin(load_vect);
        VecAssemblyEnd(load_vect);


        Initialized_HTable_withoutHangingVertices_1(P_FEMMesh, HTable);
        cout<<"Modification_LoadVector_CoeffMatrix"<<endl;

        OfTime<<endl;
        OfTime<<"Modification_LoadVector_CoeffMatrix"<<endl;
        t0=(double)clock();
        Modification_LoadVector_CoeffMatrix(C, BasesType, coeff_matrix, load_vect, HTable);

   /*     //Save CoeffMatrix
        PetscViewer viewer;
        PetscViewerASCIIOpen(PETSC_COMM_WORLD, "coeffmatrix.m", &viewer);
        PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_MATLAB);
        MatView(coeff_matrix, viewer);
        PetscViewerDestroy(&viewer);
        */

        //================================================================================
//        cout<<"Coeff_matrix"<<endl;
//        PetscInt m_size, n_size;
//        MatGetSize(coeff_matrix, &m_size, &n_size);
//        for (PetscInt i1=0; i1<m_size; i1++)
//        {
//            for(PetscInt i2=0; i2<n_size; i2++)
//            {
//                PetscScalar v;
//                MatGetValue(coeff_matrix, i1, i2, &v);
//                cout<<v<<" ";
//            }
//            cout<<endl;
//        }
        //=================================================================================
        VecAssemblyBegin(load_vect);
        VecAssemblyEnd(load_vect);

       // PetscLogStagePop();

        MatSetOption(coeff_matrix, MAT_SYMMETRIC, PETSC_TRUE);

        t1=(double)clock();
        cout<<"\"Modification_LoadVector_CoeffMatrix\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
        OfTime<<"\"Modification_LoadVector_CoeffMatrix\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
        //
        VecCreate(PETSC_COMM_WORLD, &m_solut);
        VecSetSizes(m_solut, PETSC_DECIDE, 4*P_FEMMesh->P_Vertices->size());
        VecSetFromOptions(m_solut);

        VecAssemblyBegin(m_solut);
        VecAssemblyEnd(m_solut);
        MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);

        //Solving the Linear System Part====================
        cout<<"solve the Linear system"<<endl;
        t0=(double)clock();
        //KSP ksp;
        //PC pc;
        KSPCreate(PETSC_COMM_WORLD, &ksp);
       // KSPSetType(ksp, KSPCG);
        KSPSetType(ksp, KSPGMRES);
        KSPSetOperators(ksp, coeff_matrix, coeff_matrix);


        cout<<"InitialCellSize = "<<InitialCellSize<<endl;

        /*Set the parameters of KSP*/
        PetscReal rtol=(1.e-4)*pow(InitialCellSize/(1.0*k+1.0),4.0);
        PetscReal stol=(1.e-3)*pow(InitialCellSize/(1.0*k+1.0),4.0);
//        PetscReal rtol=1.0e-18;
//        PetscReal stol=1.0e-18;

        KSPSetTolerances(ksp, rtol, stol, PETSC_DEFAULT, PETSC_DEFAULT);

        KSPGetPC(ksp, &pc);
        PCSetType(pc, PCGAMG);
        KSPSetFromOptions(ksp);

        KSPSolve(ksp, load_vect, m_solut);

        KSPGetConvergedReason(ksp,&reason);
        //printf
        PetscInt its;
        KSPGetIterationNumber(ksp, &its);
        PetscPrintf(PETSC_COMM_WORLD, "The number of iteration %D\n", its);
        cout<<"KSP ConvergedReason= "<<reason<<endl;

        t1=(double)clock();
        cout<<"\"Solving the linear system\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

        OfTime<<endl;
        OfTime<<"\"Solving the linear system\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
        OfTime<<"k = "<<k<<endl;
        OfTime<<"where, rtol=(1.e-4)*pow(InitialCellSize/(k+1),4.0)="<<rtol<<" and  stol=(1.e-3)*pow(InitialCellSize/(k+1),4.0)= "<<stol<<endl;

        OfTime<<"KSP ConvergedReason= "<<reason<<endl;
        OfTime<<"the number of iteration = "<<its<<endl;
        OfTime<<endl;
        //-------
        OfTime.close();


        //Print load_vect
        PrintLoadVector(load_vect);

        PrintSolution(m_solut);

        SingularParameterization_PrintAxelFile_FEMSolution(Coeff_Parametric_Map, P_ParametricMesh, k, m_solut);

        //=========Detroy
        VecDestroy(&load_vect);
        MatDestroy(&coeff_matrix);
        KSPDestroy(&ksp);
//        PCDestroy(&pc);

        //Error========================================================================
        vector<double> cell_errorsPhysDom;
        double total_ErrorPhysDom;
        //---

        vector<double> cell_errorsResidualPhysicalDom;
        double total_Error_ResidualPhysicalDom;

//        //---
        vector<double> CellErrEnergyNormPhysDom;
        double total_ErrEnergyNormPhysDom;
        //---


        double Regular_EnergeError_PhysicalDom;
        double Regular_ResidualError_PhysicalDom;


//        //Compute all kinds of errors
        SingularParameterization_Errors(gausspt, w, m_solut, P_FEMMesh, P_ParametricMesh,
                  &ParametricMap, SingularCellIndice,SingularVertexIndiceFEM, ExampleIndex,
                  cell_errorsPhysDom, total_ErrorPhysDom,
                  CellErrEnergyNormPhysDom, total_ErrEnergyNormPhysDom,
                  cell_errorsResidualPhysicalDom, total_Error_ResidualPhysicalDom,
                 Regular_EnergeError_PhysicalDom, Regular_ResidualError_PhysicalDom);



//        //-----------------------------------


        VecDestroy(&m_solut);

        PetscFinalize();
}

