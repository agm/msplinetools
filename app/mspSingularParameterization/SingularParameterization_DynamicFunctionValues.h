#ifndef BIHARMONICEQUATION_DYNAMICFUNCTIONVALUES_H
#define BIHARMONICEQUATION_DYNAMICFUNCTIONVALUES_H


#include <vector>

#include <cmath>

using namespace std;

static double SingularParameterization_ExactFunctionValue(double x,double y, unsigned int ExampleIndex)
{
    double exactVal;
    switch (ExampleIndex)
    {
    case 1://-------------------------Crack rectangle-------------------------
        //Smooth solutions
        ////1:
        //exactVal=0.0001*exp(pow(x,2)+pow(y,2))*x*y*(4.0-x*x)*(4.0-y*y)*(pow(x,3.0)+pow(y,2.0)+1.0);
        ////2:
        //exactVal=sin(x)*sin(y)*(4.0-x*x)*(4.0-y*y);
        ////======================================================
        //Non-smooth solutions
        exactVal=(1.0/8.0)*pow(x*x+y*y,-5.0/4.0)*(-0.5+pow(x*x+y*y,0.25)+pow(x*x+y*y,0.5)+2.0*pow(x*x+y*y,3.0/4.0)+3.0*(x*x+y*y))*(4.0-x*x)*(4.0-y*y)*x*y;
        break;
    case 2://-------------------------L-shape-------------------------
        //Smooth solutions
        ////1:
        //exactVal=100.0*(1-x*x)*(1-y*y)*x*y*(y-x*x)*(x*x*x+y*y);
        ////======================================================
        ////2:
        //exactVal=(1-x*x)*(1-y*y)*sin(x)*y;
        ////======================================================
        //Non-smooth solutions
        exactVal=pow((x*x+y*y),-7.0/6.0)*(1.0+pow((x*x+y*y),1.0/3.0)-0.5*pow(x*x+y*y,2.0/3.0))*(1.0-x*x)*(1.0-y*y)*x*y;
        break;

    case 3://Triangule
        exactVal=y*(2.0*x+y-2.0)*(y-2.0*x-2.0);
        break;

    case 4://[0,2]X[0,2]
        exactVal=(2.0-x)*sin(2.0+x)*(2.0+y)*sin(2.0-y);
        break;

    default:
        exactVal=0.0;
        break;
    }

    return exactVal;
}
//-----------
static double SingularParameterization_FxFunctionValue(double x,double y,unsigned int ExampleIndex)
{
    double fx;

    switch (ExampleIndex)
    {
    case 1://-------------------------Crack rectangle-------------------------
        //Smooth solutions
        ////1_Reslut
        //fx=0.0001*exp(pow(x,2.0) + pow(y,2.0))*y*(y-2.0)*(y+2.0)*(2.0*pow(x,7.0) - 2.0*pow(x,5.0) + 2.0*pow(x,4.0)*pow(y,2.0) + 2.0*pow(x,4.0) - 16.0*pow(x,3.0) - 5.0*pow(x,2.0)*pow(y,2.0)-5.0*pow(x,2.0) - 4.0*pow(y,2.0)-4.0);
        ////2_Reslut
        //fx=cos(x)*sin(y)*(x*x - 4.0)*(y*y - 4.0) + 2.0*x*sin(x)*sin(y)*(y*y - 4.0);
        ////======================================================
        //Non-smooth solutions
        fx=(y*(y - 2.0)*(y + 2.0)*(36.0*x*x*pow(y,4.0)-78.0*x*x*y*y+66.0*pow(x,4.0)*pow(y,2.0)-16.0*pow(x*x+y*y,3.0/2.0) - 16.0*pow(x*x + y*y,5.0/4.0) - 32.0*pow(x*x + y*y,7.0/4.0) + 24.0*x*x*pow(x*x + y*y, 1.0/2.0) + 32.0*x*x*pow(x*x + y*y,1.0/4.0) + 12.0*x*x*pow(x*x+y*y, 3.0/2.0) - 6.0*pow(x,4.0)*pow(x*x + y*y,0.5) + 32.0*x*x*pow(x*x + y*y, 3.0/4.0) - 8.0*pow(x,4.0)*pow((x*x + y*y), 1.0/4.0) + 12.0*x*x*pow(x*x+y*y, 5.0/4.0) - 8.0*pow(x,4.0)*pow(x*x + y*y, 3.0/4.0) + 24.0*x*x*pow(x*x + y*y, 7.0/4.0) - 12.0*x*x-25.0*pow(x,4.0)+30.0*pow(x,6.0)+8.0*y*y-48.0*pow(y,4.0)))/(32.0*pow((x*x + y*y), 9.0/4.0));
        break;

    case 2://-------------------------L-shape-------------------------
        //Smooth solutions
        ////1:
        //fx=100.0*(y*(pow(y,2)-1)*(-8*pow(x,7)+6*pow(x,5)*y+6.0*pow(x,5)-5.0*pow(x,4.0)*pow(y,2.0)-4.0*pow(x,3.0)*y+3.0*pow(x,2.0)*pow(y,3.0)+3.0*pow(x,2.0)*pow(y,2.0)-pow(y,3)));
        ////======================================================
        ////2:
        //fx=y*cos(x)*(x*x - 1.0)*(y*y - 1) + 2.0*x*y*sin(x)*(y*y - 1.0);
        ////======================================================
        //Non-smooth solutions
        fx=-(y*(y - 1.0)*(y + 1.0)*(6.0*pow(x*x + y*y,4.0/3.0) - 18.0*x*x*y*y - 3.0*pow((x*x+y*y),5.0/3.0)-10.0*pow(x,2.0)*pow((x*x + y*y),(1.0/3.0))+3.0*x*x*pow((x*x+y*y), 2.0/3.0)+10.0*pow(x,4.0)*pow((x*x+y*y),1.0/3.0)-18.0*x*x*pow((x*x+y*y),4.0/3.0)-3.0*pow(x,4.0)*pow(x*x+y*y,2.0/3.0)+9.0*x*x*pow((x*x+y*y), 5.0/3.0)-8.0*x*x-4.0*pow(x,4.0)+6.0*y*y))/(6.0*pow((x*x+y*y),13.0/6.0));
        break;

    case 3:
        fx=- 2.0*y*(2.0*x + y - 2.0) - 2.0*y*(2.0*x - y + 2.0);
        break;

    case 4://[0,2]X[0,2]
        fx=sin(x + 2.0)*sin(y - 2.0)*(y + 2.0) + cos(x + 2.0)*sin(y - 2.0)*(x - 2.0)*(y + 2.0);
        break;

    default:
        fx=0.0;
        break;
    }

    return fx;
}

static double SingularParameterization_FyFunctionValue(double x,double y,unsigned int ExampleIndex)
{
    double fy;

    switch (ExampleIndex)
    {
    case 1://-------------------------Crack rectangle-------------------------
        //Smooth solutions
        ////1_Reslut
        //fy=0.0001*exp(x*x+y*y)*x*(x - 2.0)*(x + 2.0)*(2.0*pow(x,3.0)*pow(y,4.0) - 5.0*pow(x,3.0)*pow(y,2.0) - 4.0*pow(x,3.0) + 2.0*pow(y,6.0) - pow(y,4.0) - 17.0*pow(y,2.0)-4.0);
        ////2_Reslut
        //fy=cos(y)*sin(x)*(x*x - 4.0)*(y*y - 4.0) + 2.0*y*sin(x)*sin(y)*(x*x - 4.0);
        ////======================================================
        //Non-smooth solutions
        fy=(x*(x - 2.0)*(x + 2.0)*(66.0*x*x*pow(y,4.0)-78.0*x*x*y*y+36.0*pow(x,4.0)*y*y-16.0*pow(x*x+y*y, 3.0/2.0) - 16.0*pow(x*x+y*y, 5.0/4.0) - 32.0*pow(x*x + y*y,7.0/4.0) + 24.0*y*y*pow(x*x + y*y,0.5)+ 32.0*y*y*pow(x*x+y*y, 1.0/4.0) + 12.0*y*y*pow(x*x + y*y, 3.0/2.0) - 6.0*pow(y,4.0)*pow(x*x + y*y, 1.0/2.0) + 32.0*y*y*pow(x*x+y*y, 3.0/4.0) - 8.0*pow(y,4.0)*pow(x*x+y*y, 1.0/4.0) + 12.0*y*y*pow(x*x+y*y, 5.0/4.0) - 8.0*pow(y,4.0)*pow(x*x + y*y,3.0/4.0) + 24.0*pow(y,2.0)*pow(x*x + y*y,7.0/4.0) + 8.0*x*x-48.0*pow(x,4.0)-12.0*pow(y,2.0)-25.0*pow(y,4.0)+30.0*pow(y,6.0)))/(32.0*pow(x*x+y*y, 9.0/4.0));
        break;
    case 2://-------------------------L-shape-------------------------
        //Smooth solutions
        ////1:
        //fy=100.0*(x*(pow(x,2)-1.0)*(-3.0*pow(x,5.0)*pow(y,2) + pow(x,5) + 4.0*pow(x,3.0)*pow(y,3.0)-2.0*pow(x,3.0)*y-5.0*pow(x,2.0)*pow(y,4.0)+3.0*pow(x,2.0)*pow(y,2.0)+6.0*pow(y,5.0)-4.0*pow(y,3.0)));
        ////======================================================
        ////2:
        //fy=2.0*y*y*sin(x)*(x*x - 1.0) + sin(x)*(x*x - 1.0)*(y*y - 1.0);
        ////======================================================
        //Non-smooth solutions
        fy=(x*(x - 1.0)*(x + 1.0)*(18.0*x*x*y*y-6.0*pow((x*x+y*y),4.0/3.0)+3.0*pow((x*x+y*y),5.0/3.0)+10.0*y*y*pow(x*x + y*y,1.0/3.0)-3.0*y*y*pow(x*x+y*y,2.0/3.0)-10.0*pow(y,4.0)*pow(x*x+y*y,1.0/3.0)+18.0*y*y*pow((x*x+y*y),4.0/3.0)+3.0*pow(y,4.0)*pow(x*x+y*y,2.0/3.0) - 9.0*y*y*pow((x*x+ y*y),5.0/3.0)-6.0*x*x+8.0*y*y+4.0*pow(y,4.0)))/(6.0*pow(x*x+y*y,13.0/6.0));
        break;
    case 3:
        fy=y*(2.0*x + y - 2.0) - (2.0*x - y + 2.0)*(2.0*x + y - 2.0) - y*(2.0*x - y + 2.0);
        break;
    case 4://[0,2]X[0,2]
        fy=sin(x + 2.0)*sin(y - 2.0)*(x - 2.0) + cos(y - 2.0)*sin(x + 2.0)*(x - 2.0)*(y + 2.0);
        break;
    default:
        fy=0.0;
        break;
    }

    return fy;
}
static double SingularParameterization_RightSideFunctionValue(double x, double y, unsigned int ExampleIndex)
{
    double d_temp;

    switch(ExampleIndex)
    {
    case 1://-------------------------Crack rectangle-------------------------
        //Smooth solutions
        ////1_Reslut
        //d_temp=0.0001*(-2.0)*exp(x*x + y*y)*x*y*(2.0*pow(x,7.0)*pow(y,2.0)-8.0*pow(x,7.0)+2.0*pow(x,5.0)*pow(y,4.0) + 4.0*pow(x,5.0)*pow(y,2.0)-29.0*pow(x,5.0)+2.0*pow(x,4.0)*pow(y,4.0)-6.0*pow(x,4.0)*pow(y,2.0)-8.0*pow(x,4.0)-8.0*pow(x,3.0)*pow(y,4.0)-17.0*pow(x,3.0)*pow(y,2.0)+120.0*pow(x,3.0)+2.0*x*x*pow(y,6.0)+4.0*pow(x,2.0)*pow(y,4)-16.0*pow(x,2)*pow(y,2)-17.0*pow(x,2)-24.0*x*pow(y,2)+96.0*x-8.0*pow(y,6.0)-29.0*pow(y,4.0)+103.0*y*y +120.0);
        ////2_Reslut
        //d_temp=2.0*sin(x)*sin(y)*(x*x - 4.0)*(y*y - 4.0) - 2.0*sin(x)*sin(y)*(y*y - 4.0) - 2.0*sin(x)*sin(y)*(x*x - 4.0) - 4.0*x*cos(x)*sin(y)*(y*y - 4.0) - 4.0*y*cos(y)*sin(x)*(x*x - 4.0);
        ////======================================================
        //Non-smooth solutions
        d_temp=-(x*y*(438.0*x*x*pow(y,6.0)-2353.0*x*x*pow(y,4.0)-2353.0*pow(x,4.0)*pow(y,2.0)-1400.0*pow(x,2.0)*pow(y,2.0)+588.0*pow(x,4)*pow(y,4)+438.0*pow(x,6)*y*y-1152.0*pow(x*x+y*y,3.0/2.0)-384.0*pow(x*x+y*y,5/2)-1536.0*pow(x*x + y*y,5.0/4.0) - 1536.0*pow(x*x + y*y,7.0/4.0)-384.0*pow(x*x+y*y,9.0/4.0) - 768.0*pow((x*x + y*y),(11.0/4.0)) + 672.0*x*x*pow(x*x+y*y, 1.0/2.0) + 1024*x*x*pow((x*x + y*y),1.0/4.0) + 480.0*x*x*pow(x*x + y*y,3.0/2.0)-168.0*pow(x,4.0)*pow(x*x + y*y,1.0/2.0) + 768.0*x*x*pow(x*x+y*y,3.0/4.0) + 48.0*x*x*pow(x*x + y*y,5.0/2.0) - 256.0*pow(x,4.0)*pow(x*x + y*y,1.0/4.0) + 640.0*x*x*pow(x*x + y*y,5.0/4.0) - 192.0*pow(x,4.0)*pow(x*x+ y*y,3/4.0) + 640.0*x*x*pow(x*x + y*y,7.0/4.0) + 48*x*x*pow(x*x + y*y,9/4.0) + 96.0*x*x*pow(x*x + y*y,11.0/4.0) + 672.0*y*y*pow(x*x + y*y,1.0/2.0) + 1024.0*y*y*pow(x*x + y*y,1.0/4.0)+480.0*y*y*pow(x*x + y*y,3.0/2.0) - 168.0*pow(y,4.0)*pow(x*x + y*y,1.0/2.0) + 768.0*y*y*pow(x*x + y*y,3.0/4.0) + 48.0*y*y*pow(x*x + y*y,5.0/2.0) - 256.0*pow(y,4.0)*pow(x*x+y*y,1.0/4.0) + 640.0*y*y*pow((x*x+y*y),5.0/4.0)-192.0*pow(y,4.0)*pow(x*x + y*y,3.0/4.0) + 640.0*y*y*pow(x*x + y*y,7.0/4.0) + 48.0*y*y*pow(x*x + y*y,9.0/4.0) + 96.0*y*y*pow(x*x + y*y,11.0/4.0) + 240.0*x*x-700.0*pow(x,4.0)-816.0*pow(x,6.0) + 144.0*pow(x,8.0) + 240.0*y*y-700.0*pow(y,4.0)-816.0*pow(y,6.0)+144.0*pow(y,8.0) - 336.0*x*x*y*y*pow(x*x + y*y,1.0/2.0) - 512.0*x*x*y*y*pow(x*x + y*y,1.0/4.0)-168.0*x*x*y*y*pow(x*x + y*y,3.0/2.0) + 42.0*x*x*pow(y,4.0)*pow(x*x+y*y,1.0/2.0) + 42.0*pow(x,4.0)*y*y*pow(x*x + y*y,1.0/2.0) - 384.0*x*x*y*y*pow(x*x + y*y,3.0/4.0) + 64.0*x*x*pow(y,4.0)*pow(x*x+y*y,1.0/4.0) + 64.0*pow(x,4.0)*pow(y,2.0)*pow(x*x + y*y,1.0/4.0)-224.0*x*x*y*y*pow(x*x+y*y,5.0/4.0) + 48.0*x*x*pow(y,4.0)*pow(x*x+y*y,3.0/4.0) + 48.0*pow(x,4.0)*y*y*pow(x*x + y*y,3.0/4.0) - 224.0*x*x*y*y*pow(x*x + y*y,7.0/4.0)))/(64.0*pow(x*x + y*y,13.0/4.0));
        break;
    case 2://-------------------------L-shape-------------------------
        //Smooth solutions
        //1:
         //d_temp=100.0*(2.0*x*(3.0*pow(x,7.0)*y + 28.0*pow(x,5.0)*pow(y,3.0) - 6.0*pow(x,5.0)*pow(y,2.0) - 31.0*pow(x,5.0)*y + pow(x,5.0) + 10.0*pow(x,4.0)*pow(y,3.0) - 3.0*pow(x,4.0)*y - 15.0*pow(x,3.0)*pow(y,4.0) - 15.0*pow(x,3)*pow(y,3) + 21.0*pow(x,3)*pow(y,2) + 15.0*pow(x,3)*y - pow(x,3) + 10.0*pow(x,2)*pow(y,5)-15.0*pow(x,2)*pow(y,4)-20.0*pow(x,2)*pow(y,3) + 6.0*pow(x,2)*pow(y,2) + 3.0*pow(x,2)*y + 6.0*x*pow(y,4) - 6.0*x*pow(y,2) - 3.0*pow(y,6.0) - 3.0*pow(y,5.0) + 18.0*pow(y,4) + 3.0*pow(y,3) - 6.0*pow(y,2)));
        //2:
        //d_temp=y*sin(x)*(x*x - 1.0)*(y*y - 1.0) - 2.0*y*sin(x)*(y*y - 1.0) - 6.0*y*sin(x)*(x*x - 1.0) - 4.0*x*y*cos(x)*(y*y - 1.0);
        ////======================================================
        //Non-smooth solutions
        ///
        d_temp=-(x*y*(44.0*x*x*y*y-82.0*x*x*pow(y,4.0)-82.0*pow(x,4.0)*y*y-180.0*pow(x*x+y*y,4.0/3.0)+54.0*pow((x*x+y*y),5.0/3.0)-216.0*pow(x*x+y*y,7.0/3.0) + 108.0*pow(x*x+y*y,8.0/3.0)+110.0*x*x*pow((x*x+y*y),1.0/3.0)-27.0*x*x*pow((x*x+y*y),2.0/3.0)-110.0*pow(x,4.0)*pow((x*x+y*y),1.0/3.0)+300.0*x*x*pow(x*x+y*y,4.0/3.0)+27.0*pow(x,4.0)*pow((x*x+y*y),2.0/3.0)-90.0*x*x*pow((x*x+y*y),5.0/3.0)+108.0*pow(x,2.0)*pow((x*x+y*y),7.0/3.0)-54.0*x*x*pow(x*x+y*y,8.0/3.0) + 110.0*y*y*pow(x*x+y*y,1.0/3.0) - 27.0*y*y*pow((x*x + y*y),2.0/3.0)-110.0*pow(y,4.0)*pow(x*x+y*y, 1.0/3.0) + 300.0*y*y*pow(x*x + y*y,4.0/3.0)+27.0*pow(y,4.0)*pow(x*x+y*y, 2.0/3.0)-90.0*y*y*pow(x*x+y*y, 5.0/3.0)+108.0*y*y*pow(x*x+y*y, 7.0/3.0)-54.0*y*y*pow(x*x+y*y, 8.0/3.0) - 70.0*x*x+22.0*pow(x,4.0)+108.0*pow(x,6.0)-70.0*y*y+22.0*pow(y,4.0)+108.0*pow(y,6.0)-220.0*x*x*y*y*pow(x*x+y*y,1.0/3.0)+54.0*x*x*y*y*pow(x*x+y*y,2.0/3.0)+110.0*x*x*pow(y,4.0)*pow(x*x+y*y, 1.0/3.0) + 110.0*pow(x,4.0)*y*y*pow(x*x+y*y,1.0/3.0) - 420.0*x*x*y*y*pow(x*x+y*y,4.0/3.0)-27.0*x*x*pow(y,4.0)*pow(x*x+y*y, 2.0/3.0)-27.0*pow(x,4.0)*pow(y,2.0)*pow(x*x + y*y,2.0/3.0) + 126.0*x*x*y*y*pow(x*x+y*y,5.0/3.0)))/(18.0*pow(x*x + y*y, 19.0/6.0));
        break;

    case 3:
        d_temp=2.0*y + 8.0;
        break;

    case 4://[0,2]X[0,2]
        d_temp=2.0*sin(x + 2.0)*sin(y - 2.0)*(x - 2.0)*(y + 2.0) - 2.0*cos(x + 2.0)*sin(y - 2.0)*(y + 2.0) - 2.0*cos(y - 2.0)*sin(x + 2.0)*(x - 2.0);
        break;
    default:
        d_temp=0.0;
        break;
    }
    return d_temp;
//==================================
}


//========================================================================

#endif
