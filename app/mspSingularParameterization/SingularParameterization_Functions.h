#ifndef SINGULARPARAMETERIZATION_FUNCTIONS_H
#define SINGULARPARAMETERIZATION_FUNCTIONS_H

#include <math.h>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

#include "msp/Basis.h"
#include "msp/MSplineEvaluation.h"
#include "msp/MSplineFunctionCell.h"
#include "msp/HermiteData.h"
#include "msp/HermiteCell.h"
#include "msp/Functions.h"
#include "msp/BasesVertex.h"
#include "msp/MSplineFunctions.h"

#include "SingularParameterization_DynamicFunctionValues.h"

//#include "msp/FEM_DynamicFunctions.h"
//#include "msp/FEM_Print.h"



#include <petsc.h>
#include <petscksp.h>

void SingularVertexIndice_ParametricMesh(const vector<vector<double> > &Coeff_Parametric_Map, set<unsigned> &SingularVertexIndice)
{
    SingularVertexIndice.clear();

    for(unsigned int i=0; i<Coeff_Parametric_Map[0].size(); i=i+4)
    {
        bool key=false;

        for(unsigned j=0; j<2;j++)
        {
            //Coeff_Parametric_Map[j]
            for(unsigned ii=1; ii<3; ii++)
            {
                if(abs(Coeff_Parametric_Map[j][i+ii])>10e-8)
                {
                    key=true;
                    break;
                }
            }
            if(key)
            {
                break;
            }
        }

        if(key==false)
        {
            SingularVertexIndice.insert(i/4);
        }

    }
}


void SingularVertexIndice_OnFEMMesh(set<unsigned> &SingularVertexIndiceFEM, const set<unsigned>& SingularVertexIndice, const map<unsigned int, unsigned int>& OrigialVertexToCurrentVertex)
{
    SingularVertexIndiceFEM.clear();
    for(set<unsigned>::iterator it=SingularVertexIndice.begin(); it!=SingularVertexIndice.end(); it++)
    {
        map<unsigned, unsigned>::const_iterator jt=OrigialVertexToCurrentVertex.find(*it);
        if(jt!=OrigialVertexToCurrentVertex.end())
        {
            SingularVertexIndiceFEM.insert((*jt).second);
        }
        else
        {
            cout<<"There is an error in SingularVertexIndice_OnFEM()"<<endl;
        }
    }
}
////================ParametricMap Part===========================================================

void SingularParameterization_ComputeFunval_MSplineParametricMap(const vector<Coordinate*> &Pcoordinate_Ps, unsigned int Index_Cell, vector<double> &funval, int flag,
                                        const Mesh & CurrentParametricMesh, MSplineFunctions * P_MSplineParametricMap, unsigned int ExampleIndex)
{

    double funcval;
    funval.clear();

    vector<vector<double> > XYZ;
    P_MSplineParametricMap->Evaluation(0,0,Pcoordinate_Ps, Index_Cell, XYZ, CurrentParametricMesh);


    //-----------------------------------
    if(flag == 0)//the value of function F
    {
        for (unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
        {
            double x=XYZ[0][i];
            double y=XYZ[1][i];
            funcval=SingularParameterization_ExactFunctionValue(x, y, ExampleIndex);
            funval.push_back(funcval);
        }

    }
    else if(flag == 1)//f=-Delta(F)
    {
        for (unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
        {

            double x=XYZ[0][i];
            double y=XYZ[1][i];

            funcval=SingularParameterization_RightSideFunctionValue(x, y, ExampleIndex);

            funval.push_back(funcval);
        }
    }
    else if(flag == 2)// Fx
    {
        for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
        {
            double x=XYZ[0][i];
            double y=XYZ[1][i];
            funcval=SingularParameterization_FxFunctionValue(x,y,ExampleIndex);

            funval.push_back(funcval);
        }
    }
    else if(flag == 3)// Fy
    {
        for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
        {
            double x=XYZ[0][i];
            double y=XYZ[1][i];
            funcval=SingularParameterization_FyFunctionValue(x,y,ExampleIndex);

            funval.push_back(funcval);
        }
    }
    else
    {
        cout<<"SingularParameterization_Functions.h:SingularParameterization_ComputeFunval_MSplineParametricMap():: the flag should be 0(F) , 1(-Delta F), 2 (Fx), 3 (Fy)!"<<endl;
        system("PAUSE");
    }
    vector<double>(funval).swap(funval);
}

//============================================================================================

void SingularParameterization_MSplineParametricMap(const vector<Coordinate*> &Pcoordinate_Ps, vector<vector<double> > &m_JPart,
                                             vector<double> &DetJ, unsigned int Index_ParametricMeshCell,
                                             const MSplineFunctions &MSplineParametricMap, const Mesh * P_ParametricMesh)
{//dimParametricMap=2
    //ParametricMapSigma_MSplineParametricMap(coordinate_Ps, m_JPart, detJ , P_FEMMesh->P_Cells[i]->ItsParent, ParametricMap, P_ParametricMesh);
    m_JPart.clear();
    DetJ.clear();

    vector <double> X, Y;
    MSplineParametricMap.P_MSplineFunctions[0]->Evaluation(0,0,Pcoordinate_Ps, X, Index_ParametricMeshCell, *P_ParametricMesh);
    MSplineParametricMap.P_MSplineFunctions[1]->Evaluation(0,0,Pcoordinate_Ps, Y, Index_ParametricMeshCell, *P_ParametricMesh);

    //X(s,t): MSplineParametricMap.P_MSplineFunctions[0]
    vector<double> dXds, dXdt;
    MSplineParametricMap.P_MSplineFunctions[0]->Evaluation(1,0,Pcoordinate_Ps, dXds, Index_ParametricMeshCell, *P_ParametricMesh);
    MSplineParametricMap.P_MSplineFunctions[0]->Evaluation(0,1,Pcoordinate_Ps, dXdt, Index_ParametricMeshCell, *P_ParametricMesh);

    //Y(s,t): MSplineParametricMap.P_MSplineFunctions[1]
    vector<double> dYds, dYdt;
    MSplineParametricMap.P_MSplineFunctions[1]->Evaluation(1,0,Pcoordinate_Ps, dYds, Index_ParametricMeshCell, *P_ParametricMesh);
    MSplineParametricMap.P_MSplineFunctions[1]->Evaluation(0,1,Pcoordinate_Ps, dYdt, Index_ParametricMeshCell, *P_ParametricMesh);


    //----det(Jacobian)
    for (unsigned int ii=0; ii<Pcoordinate_Ps.size(); ii++)
    {
        DetJ.push_back(dXds[ii]*dYdt[ii]-dXdt[ii]*dYds[ii]);

        //cout<<"dXds["<<ii<<"]= "<<dXds[ii]<<endl;
        //cout<<"dYdt["<<ii<<"]= "<<dYdt[ii]<<endl;
        //cout<<"dXdt["<<ii<<"]= "<<dXdt[ii]<<endl;
        //cout<<"dYds["<<ii<<"]= "<<dYds[ii]<<endl;

        //cout<<"FEM_Functions.h::DetJ= "<<dXds[ii]*dYdt[ii]-dXdt[ii]*dYds[ii]<<endl;
    }

    vector<double> m_JParti;
    for (unsigned int ii=0; ii<Pcoordinate_Ps.size(); ii++)
    {
        m_JParti.clear();

        //J^(-1)====================================
        double Jinv11(0.0);
        double Jinv12(0.0);
        double Jinv21(0.0);
        double Jinv22(0.0);

        Jinv11=dYdt[ii]/DetJ[ii];
        Jinv12=-dXdt[ii]/DetJ[ii];
        Jinv21=-dYds[ii]/DetJ[ii];
        Jinv22=dXds[ii]/DetJ[ii];


        vector<double> Kxy;
        Kxy.push_back(1.0);
        Kxy.push_back(0.0);
        Kxy.push_back(0.0);
        Kxy.push_back(1.0);

        double a11=Jinv11*(Jinv11*Kxy[0]+Jinv12*Kxy[1])+Jinv12*(Jinv11*Kxy[2]+Jinv12*Kxy[3]);
        double a12=Jinv21*(Jinv11*Kxy[0]+Jinv12*Kxy[1])+Jinv22*(Jinv11*Kxy[2]+Jinv12*Kxy[3]);
        double a21=Jinv11*(Jinv21*Kxy[0]+Jinv22*Kxy[1])+Jinv12*(Jinv21*Kxy[2]+Jinv22*Kxy[3]);
        double a22=Jinv21*(Jinv21*Kxy[0]+Jinv22*Kxy[1])+Jinv22*(Jinv21*Kxy[2]+Jinv22*Kxy[3]);

        m_JParti.push_back(DetJ[ii]*a11);
        m_JParti.push_back(DetJ[ii]*a12);
        m_JParti.push_back(DetJ[ii]*a21);
        m_JParti.push_back(DetJ[ii]*a22);
        m_JPart.push_back(m_JParti);
    }
}

//====

void SingularParameterization_PrintAxelFile_FEMSolution(const vector<vector<double> > &Coeff_Parametric_Map,
                                                               Mesh * P_ParametricMesh, const unsigned int k, Vec &m_solut)
{
    vector<vector<double> > CoeffsOverFinedMesh;
    
    MSplineFunctions * P_ParametricMap = new MSplineFunctions(Coeff_Parametric_Map, *P_ParametricMesh);
    
    Mesh* P_FEMMesh = P_ParametricMap->GenerateThisMSplineFunctionsOverAFinerMesh(k, P_ParametricMesh, CoeffsOverFinedMesh);


    
    //Modified the third-component
    for(PetscInt i = 0; i < CoeffsOverFinedMesh[2].size(); i ++)
    {
        PetscScalar v;
        VecGetValues(m_solut, 1, &i, &v);
        
        CoeffsOverFinedMesh[2][i]=v;
        
        // cout<<"v = "<<v<<endl;
    }
    //Now the FEMSolution is defined over P_FEMMesh and the coeffients is CoeffsOverFinedMesh
    
    
    ofstream ofAxel;
    ofAxel.open("FEMSolution_Axel.axl");
    
    ofAxel<<"<axl>"<<endl;
    ofAxel<<"<mspline type=\"InitialMesh\">"<<endl;
    ofAxel<<"<vertexnumber> "<<P_FEMMesh->P_Vertices->size()<<"</vertexnumber>"<<endl;
    
    for (unsigned int i=0; i<P_FEMMesh->P_Cells->size(); i++)
    {
        ofAxel<<"<cell>"<<endl;
        ofAxel<<P_FEMMesh->P_Cells->at(i).CellSize[0]<<" "<<P_FEMMesh->P_Cells->at(i).CellSize[1]<<" "<<P_FEMMesh->P_Cells->at(i).CellSize[2]<<" "<<P_FEMMesh->P_Cells->at(i).CellSize[3]<<endl;
        ofAxel<<P_FEMMesh->P_Cells->at(i).Index_ItsCornerVertices[0]<<" "<<P_FEMMesh->P_Cells->at(i).Index_ItsCornerVertices[1]<<" "<<P_FEMMesh->P_Cells->at(i).Index_ItsCornerVertices[2]<<" "<<P_FEMMesh->P_Cells->at(i).Index_ItsCornerVertices[3]<<endl;
        ofAxel<<"</cell>"<<endl;
    }
    for (unsigned int i=0; i<P_FEMMesh->P_Cells->size(); i++)
    {
        for (unsigned int j=0; j<i; j++)
        {
            unsigned int J=P_FEMMesh->P_Cells->at(j).Index_InitialCell;
            unsigned int I=P_FEMMesh->P_Cells->at(i).Index_InitialCell;
            double O11, O12, O21, O22;
            O11=(P_FEMMesh->P_FrameTranMatrix->at(I))[J].O11;
            O12=(P_FEMMesh->P_FrameTranMatrix->at(I))[J].O12;
            O21=(P_FEMMesh->P_FrameTranMatrix->at(I))[J].O21;
            O22=(P_FEMMesh->P_FrameTranMatrix->at(I))[J].O22;
            
            double IdError=(O11-1.0)*(O11-1.0)+O12*O12+O21*O21+(O22-1.0)*(O22-1.0);
            if(IdError>1.0e-2)
            {
                ofAxel<<"<transition cells=\""<<i<<" "<<j<<"\">"<<endl;
                ofAxel<<O11<<" "<<O12<<" "<<O21<<" "<<O22<<endl;
                ofAxel<<"</transition>"<<endl;
            }
            
        }
    }
    
    ofAxel<<"<points>"<<endl;
    switch(CoeffsOverFinedMesh.size())
    {
        case 0:
            cout<<"There is no point!!"<<endl;
            break;
        case 1:
            for (unsigned int i=0; i<CoeffsOverFinedMesh[0].size(); i++)
            {
                ofAxel<<CoeffsOverFinedMesh[0][i]<<" "<<0.0<<" "<<0.0<<endl;
            }
            break;
        case 2:
            for (unsigned int i=0; i<CoeffsOverFinedMesh[0].size(); i++)
            {
                ofAxel<<CoeffsOverFinedMesh[0][i]<<" "<<CoeffsOverFinedMesh[1][i]<<" "<<0.0<<endl;
            }
            break;
        case 3:
            for (unsigned int i=0; i<CoeffsOverFinedMesh[0].size(); i++)
            {
                ofAxel<<CoeffsOverFinedMesh[0][i]<<" "<<CoeffsOverFinedMesh[1][i]<<" "<<CoeffsOverFinedMesh[2][i]<<endl;
            }
            break;
        default:
            for (unsigned int i=0; i<CoeffsOverFinedMesh[0].size(); i++)
            {
                ofAxel<<CoeffsOverFinedMesh[0][i]<<" "<<CoeffsOverFinedMesh[1][i]<<" "<<CoeffsOverFinedMesh[2][i]<<endl;
            }
            cout<<"the dimesion of parametricMap > 3"<<endl;
            break;
            
    }
    ofAxel<<"</points>"<<endl;
    ofAxel<<"</mspline>"<<endl;
    ofAxel<<"</axl>"<<endl;
    
    ofAxel.close();
}

//
void SingularParameterization_Set_Bases_type(const Mesh * P_FEMMesh, vector<unsigned int> &BasesType)
{
    //Here BasesType.resize(0.0, 4*P_FEMMesh->P_Vertices->size());

    //elements in BasesType have three possible values: 0, 1, 2;
    //0: a true basis and the value of this basis is zero on the boundary of the domain
    //1: a true basis with non-zero values  (ds or dt) along the boundary of the domain.
    //2: at the interior extraordinary vertex, the basis functions refer to [0,1,0,0], [0,0,1,0] and [0,0,0,1]
    //3: a true basis with non-zero values on the boundary of the domain: [1,0,0,0]
    for (unsigned int i_vertex=0; i_vertex < P_FEMMesh->P_Vertices->size(); i_vertex++)
    {
        //classify the type of P_FEMMesh->P_Vertices[i_vertex]
        if (P_FEMMesh->P_Vertices->at(i_vertex).Is_interiorVertex)//It is an interior vertex
        {
            unsigned int flag=(P_FEMMesh->P_Vertices->at(i_vertex).Index_ItsVertices.size())%4;
            switch(flag)
            {
            case 1:
                BasesType[4*i_vertex+1]=2;
                BasesType[4*i_vertex+2]=2;
                BasesType[4*i_vertex+3]=2;
                break;
            case 2:
                BasesType[4*i_vertex+1]=2;
                BasesType[4*i_vertex+2]=2;
                //Add
                BasesType[4*i_vertex+3]=2;
                break;
            case 3:
                BasesType[4*i_vertex+1]=2;
                BasesType[4*i_vertex+2]=2;
                BasesType[4*i_vertex+3]=2;
                break;
            }
        }
        else//is a boundary vertex
        {
            Bases_type_Cell(i_vertex, P_FEMMesh , BasesType);//(BasesType);
        }
    }
}

#endif
