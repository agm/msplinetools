#ifndef BIHARMONICEQUATION_ERRORFUNCTIONS_H
#define BIHARMONICEQUATION_ERRORFUNCTIONS_H

#include "SingularParameterization_Functions.h"
#include "msp/MSplineFunctions.h"



//===========================================================================
//Energy norm Error
static void SingularParameterization_EnergyNormError_PhyscialDomain_RefinedParametricMap(const vector<double>& u_gausspt, const vector<double> & v_gausspt,
                   double **w, const Vec & m_solut,
                   vector<double> &cell_errors, double &totol_Error, double& Regular_EnergyError_PhysicalDom,
                   Mesh*  P_FEMMesh, Mesh* P_ParametricMesh,
                   MSplineFunctions * P_MSplineParametricMap, const set<unsigned> &SingularCellIndice, const set<unsigned> &SingularVertexIndiceFEM, unsigned int ExampleIndex)
{
    unsigned int u_gaussnum=u_gausspt.size();
    unsigned int v_gaussnum=v_gausspt.size();

    vector<double> ExacSou_x;
    vector<double> ExacSou_y;

    vector<double> Jval;
    vector<double> FEMSou_x;
    vector<double> FEMSou_y;

    cell_errors.clear();
    cell_errors.resize(P_FEMMesh->P_Cells->size());
    totol_Error=0.0;
    Regular_EnergyError_PhysicalDom=0.0;

    vector<double> solution;
    PetscInt SolutSize;
    VecGetSize(m_solut, &SolutSize);
    for(PetscInt Ipetsc = 0; Ipetsc < SolutSize; Ipetsc++)
    {
        PetscScalar v;
        VecGetValues(m_solut,1,&Ipetsc,&v);
        solution.push_back(v);
    }

    MSplineFunction * P_FEMSolution=new MSplineFunction(solution, *P_FEMMesh);//The Solution of FEM

    for (int i=0;i<P_FEMMesh->P_Cells->size();i++)
    {
        bool Flag=false;
        if(SingularCellIndice.find(i)==SingularCellIndice.end())
        {
            Flag=true;
        }
        //
/*       //The set of indices of irregular cells
       set<unsigned> IrrCells;
       P_FEMMesh->IrrCellIndices(IrrCells);
       */
        //
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];

        vector<Coordinate*> Pcoordinate_Ps;
        for (int k=0; k<u_gaussnum; k++)
        {
            for (int l=0; l<v_gaussnum; l++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=0.5*(x1+x0)+0.5*(x1-x0)*u_gausspt[k];//u[k];
                CoorP->xy[1]=0.5*(y1+y0)+0.5*(y1-y0)*v_gausspt[l];//v[l];(y0+y1+(y1-y0)*gausspt[k]) * 0.5;
                Pcoordinate_Ps.push_back(CoorP);
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        //Fx
        SingularParameterization_ComputeFunval_MSplineParametricMap(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent,
                                           ExacSou_x, 2, *P_ParametricMesh, P_MSplineParametricMap, ExampleIndex);
        //Fy
        SingularParameterization_ComputeFunval_MSplineParametricMap(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent,
                                           ExacSou_y, 3, *P_ParametricMesh, P_MSplineParametricMap, ExampleIndex);
        //J(x,y)
        JacobianValue(P_MSplineParametricMap->P_MSplineFunctions[0],P_MSplineParametricMap->P_MSplineFunctions[1],
                      Pcoordinate_Ps, Jval, P_FEMMesh->P_Cells->at(i).ItsParent,
                      P_FEMMesh->P_Cells->at(i).ItsParent, P_ParametricMesh, P_ParametricMesh);

        //FEMSou_x//FEMSou_y//
        /*Flag is set as true*/
        Flag=true;//For testing what happening when we take different numbers of Gauss points
        if(Flag)//the cell is a regular cell
        {
            DUx_AND_DUy(P_MSplineParametricMap->P_MSplineFunctions[0],P_MSplineParametricMap->P_MSplineFunctions[1],
                        P_FEMSolution, Pcoordinate_Ps, FEMSou_x, FEMSou_y, i, P_FEMMesh, P_ParametricMesh);
        }
        else// the cell is a singular cell
        {
            vector<double> Modified_m_solution;
            PetscInt msolutionSize;
            VecGetSize(m_solut,&msolutionSize);
            for(PetscInt i=0; i<msolutionSize; i++)
            {
                PetscScalar d;
                VecGetValues(m_solut,1,&i,&d);
                Modified_m_solution.push_back(d);
            }
            //Modfied m_solut:
            //Modified the solutions
             for(set<unsigned>::iterator it=SingularVertexIndiceFEM.begin(); it!=SingularVertexIndiceFEM.end(); it++)
             {
                 unsigned ibasis=4*(*it);
                 double c=Modified_m_solution[ibasis];
                 //Find the other vertices which share the same cell with this extraordinary vertex
                 set<unsigned> IvertexIndice;
                 //---
                 set<unsigned> I_singularV;
                 I_singularV.insert(*it);
                 set<unsigned> I_singularCells;
                 P_FEMMesh->SingularCellIndice_Finder(I_singularCells,I_singularV);
                 for(set<unsigned>::iterator itc=I_singularCells.begin(); itc!=I_singularCells.end(); itc++)
                 {
                     //P_FEMMesh->P_Cells->at(*itc)->Index_ItsCornerVertices
                     for(unsigned i=0; i<P_FEMMesh->P_Cells->at(*itc).Index_ItsCornerVertices.size(); i++)
                     {
                         if(P_FEMMesh->P_Cells->at(*itc).Index_ItsCornerVertices[i]!=*it)
                         {
                             IvertexIndice.insert(P_FEMMesh->P_Cells->at(*itc).Index_ItsCornerVertices[i]);
                         }
                     }
                 }
                 //-------------------
                 for(set<unsigned>::iterator itv=IvertexIndice.begin(); itv!=IvertexIndice.end(); itv++)
                 {
                     //m_solut[4*(*itv)]=m_solut[4*(*itv)]+c;
                     unsigned ibasis2=4*(*itv);
                     Modified_m_solution[ibasis2]=Modified_m_solution[ibasis2]+c;
                 }
             }

             MSplineFunction * P_FEMSolution2=new MSplineFunction(Modified_m_solution, *P_FEMMesh);

             DUx_AND_DUy(P_MSplineParametricMap->P_MSplineFunctions[0], P_MSplineParametricMap->P_MSplineFunctions[1],
                         P_FEMSolution2, Pcoordinate_Ps, FEMSou_x, FEMSou_y, i, P_FEMMesh, P_ParametricMesh);

             vector<double>().swap(Modified_m_solution);
             delete P_FEMSolution2;
             P_FEMSolution2=NULL;

        }


        cell_errors[i] = 0;
        for(int j=0;j<u_gaussnum*v_gaussnum;j++)
        {
            int nn = j / v_gaussnum;
            int mm = j % v_gaussnum;

            if(Jval[j]<0)
            {
                Jval[j]=-Jval[j];
            }


            cell_errors[i] += w[nn][mm]*((ExacSou_x[j]-FEMSou_x[j])*(ExacSou_x[j]-FEMSou_x[j])+(ExacSou_y[j]-FEMSou_y[j])*((ExacSou_y[j]-FEMSou_y[j])))*Jval[j];
        }
        //=======================================================
        totol_Error = totol_Error+cell_errors[i]*(y1-y0)*(x1-x0)/4;
        if(Flag)
        {
            Regular_EnergyError_PhysicalDom=Regular_EnergyError_PhysicalDom+cell_errors[i]*(y1-y0)*(x1-x0)/4;
        }
        //=======================================================
        cell_errors[i] = sqrt(cell_errors[i] *(y1-y0) * (x1-x0)/4);
        //cout<<"Errors at "<<i<<"-th cell = "<<cell_errors[i]<<endl;
    }
    totol_Error = sqrt(totol_Error);
    Regular_EnergyError_PhysicalDom=sqrt(Regular_EnergyError_PhysicalDom);
}


//=====================================================
//L2 error on the physical Domain
static void SingularParameterization_ComputError_MSplineParametricMap_OnPhysicalDomain(const vector<double>& u_gausspt,
                                                                                       const vector<double> & v_gausspt,
                                                              double **w, const Vec & m_solut,
                                                       vector<double> &cell_errors, double &totol_Error,
                                                        const Mesh*  P_FEMMesh, const Mesh * P_ParametricMesh,
                                                       MSplineFunctions * P_MSplineParametricMap, unsigned int ExampleIndex)
{
   // ofstream ofJacobian;
    //ofJacobian.open("Jacobian.txt");
    //double MaxMaxJ=-100.0;
    //double MinMinJ=100.0;

    unsigned int u_gaussnum=u_gausspt.size();
    unsigned int v_gaussnum=v_gausspt.size();

    vector<double> temp_Error;
    vector<double> exactval;
    vector<double> Jval;

    cell_errors.clear();
    cell_errors.resize(P_FEMMesh->P_Cells->size());
    totol_Error=0.0;

    vector<double> solution;
    PetscInt SolutSize;
    VecGetSize(m_solut, &SolutSize);
    for(PetscInt Ipetsc = 0; Ipetsc < SolutSize; Ipetsc++)
    {
        PetscScalar v;
        VecGetValues(m_solut,1,&Ipetsc,&v);
        solution.push_back(v);
    }

    MSplineFunction * P_FEMSolution=new MSplineFunction(solution, *P_FEMMesh);

    for (int i=0;i<P_FEMMesh->P_Cells->size();i++)
    {
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];

        vector<Coordinate*> Pcoordinate_Ps;
        for (int k=0; k<u_gaussnum; k++)
        {
            for (int l=0; l<v_gaussnum; l++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=0.5*(x1+x0)+0.5*(x1-x0)*u_gausspt[k];//u[k];
                CoorP->xy[1]=0.5*(y1+y0)+0.5*(y1-y0)*v_gausspt[l];//v[l];(y0+y1+(y1-y0)*gausspt[k]) * 0.5;
                Pcoordinate_Ps.push_back(CoorP);
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        SingularParameterization_ComputeFunval_MSplineParametricMap(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent, exactval,
                                           0, *P_ParametricMesh, P_MSplineParametricMap, ExampleIndex);


        P_FEMSolution->Evaluation(0, 0, Pcoordinate_Ps, temp_Error, i, *P_FEMMesh);

      //P_MSplineParametricMap->Cout_MSplineFunctions();
        //cin.get();

        //Jacobian
        JacobianValue(P_MSplineParametricMap->P_MSplineFunctions[0], P_MSplineParametricMap->P_MSplineFunctions[1],
                      Pcoordinate_Ps,Jval,P_FEMMesh->P_Cells->at(i).ItsParent,P_FEMMesh->P_Cells->at(i).ItsParent,
                      P_ParametricMesh,P_ParametricMesh);


        cell_errors[i] = 0.0;

        for(int j=0;j<u_gaussnum*v_gaussnum;j++)
        {
            //cout<<"L2 error on the physical Domain::Jacobian["<<j<<"] = "<<Jval[j]<<endl;


            if(Jval[j]<0)
            {
                Jval[j]=-Jval[j];
            }

            int nn = j / v_gaussnum;
            int mm = j % v_gaussnum;
            cell_errors[i] += w[nn][mm]*pow(exactval[j]-temp_Error[j],2)*Jval[j];
        }
        //ofJacobian<<"the"<<i<<"-th cell:"<<endl;
        //ofJacobian<<"maxJ = "<<maxJ<<endl;
        //ofJacobian<<"minJ = "<<minJ<<endl;

        //=======================================================
        totol_Error = totol_Error+cell_errors[i]*(y1-y0)*(x1-x0)/4;
        //=======================================================
        cell_errors[i] = sqrt(cell_errors[i] *(y1-y0) * (x1-x0)/4);
        //cout<<"Errors at "<<i<<"-th cell = "<<cell_errors[i]<<endl;
    }
    totol_Error = sqrt(totol_Error);



//    ofJacobian<<endl;
//    ofJacobian<<endl;

//    ofJacobian<<"MaxMaxJ= "<<MaxMaxJ<<endl;
//    ofJacobian<<"MinMinJ= "<<MinMinJ<<endl;

//    ofJacobian.close();
}

//Residual Error on the physical domain
static void SingularParameterization_ResidualError_PhysicalDomain_RefinedParametricMap
                   (const vector<double>& u_gausspt, const vector<double> & v_gausspt,
                   double **w, const Vec & m_solut,
                   vector<double> &cell_errors, double &totol_Error,
                    double &Regular_ResidualError_PhysicalDom,
                   Mesh*  P_FEMMesh, Mesh* P_ParametricMesh,
                   MSplineFunctions * P_MSplineParametricMap,
                   const set<unsigned> &SingularCellIndices,
                    const set<unsigned> &SingularVertexIndiceFEM, unsigned int ExampleIndex)
{
    unsigned int u_gaussnum=u_gausspt.size();
    unsigned int v_gaussnum=v_gausspt.size();

    vector<double> fval;
    vector<double> Jval;
    vector<double> UxxPUyy;


    //--------

    cell_errors.clear();
    cell_errors.resize(P_FEMMesh->P_Cells->size());
    totol_Error=0.0;
    Regular_ResidualError_PhysicalDom=0.0;

    vector<double> solution;
    PetscInt SolutSize;
    VecGetSize(m_solut, &SolutSize);
    for(PetscInt Ipetsc = 0; Ipetsc < SolutSize; Ipetsc++)
    {
        PetscScalar v;
        VecGetValues(m_solut,1,&Ipetsc,&v);
        solution.push_back(v);
    }

    MSplineFunction * P_FEMSolution=new MSplineFunction(solution, *P_FEMMesh);//The Solution of FEM

    for (int i=0;i<P_FEMMesh->P_Cells->size();i++)
    {
        //============================================
        bool Flag=false;
        if(SingularCellIndices.find(i)==SingularCellIndices.end())
        {
            Flag=true;
        }
        //============================================
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];

        vector<Coordinate*> Pcoordinate_Ps;
        for (int k=0; k<u_gaussnum; k++)
        {
            for (int l=0; l<v_gaussnum; l++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=0.5*(x1+x0)+0.5*(x1-x0)*u_gausspt[k];//u[k];
                CoorP->xy[1]=0.5*(y1+y0)+0.5*(y1-y0)*v_gausspt[l];//v[l];(y0+y1+(y1-y0)*gausspt[k]) * 0.5;
                Pcoordinate_Ps.push_back(CoorP);
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        //f(x,y)
        SingularParameterization_ComputeFunval_MSplineParametricMap(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent,
                                           fval, 1, *P_ParametricMesh, P_MSplineParametricMap, ExampleIndex);
        //J(x,y)
        JacobianValue(P_MSplineParametricMap->P_MSplineFunctions[0],P_MSplineParametricMap->P_MSplineFunctions[1],
                      Pcoordinate_Ps, Jval, P_FEMMesh->P_Cells->at(i).ItsParent,
                      P_FEMMesh->P_Cells->at(i).ItsParent, P_ParametricMesh, P_ParametricMesh);
        //Uxx+Uyy
        /*Flag is set as True: the classical method*/
        Flag=true;//For testing what happening when we take different Gauss points
        if(Flag)
        {
            DUxxValue_PLUS_DUyyValue(P_MSplineParametricMap->P_MSplineFunctions[0], P_MSplineParametricMap->P_MSplineFunctions[1],
                                     P_FEMSolution, Pcoordinate_Ps, UxxPUyy, i, P_FEMMesh, P_ParametricMesh);

        }
        else
        {
            vector<double> Modified_m_solution;
            PetscInt msolutionSize;
            VecGetSize(m_solut,&msolutionSize);
            for(PetscInt i=0; i<msolutionSize; i++)
            {
                PetscScalar d;
                VecGetValues(m_solut,1,&i,&d);
                Modified_m_solution.push_back(d);
            }
            //Modfied m_solut:
            //Modified the solutions
             for(set<unsigned>::iterator it=SingularVertexIndiceFEM.begin(); it!=SingularVertexIndiceFEM.end(); it++)
             {
                 unsigned ibasis=4*(*it);
                 double c=Modified_m_solution[ibasis];
                 //Find the other vertices which share the same cell with this extraordinary vertex
                 set<unsigned> IvertexIndice;
                 //---
                 set<unsigned> I_singularV;
                 I_singularV.insert(*it);
                 set<unsigned> I_singularCells;
                 P_FEMMesh->SingularCellIndice_Finder(I_singularCells,I_singularV);
                 for(set<unsigned>::iterator itc=I_singularCells.begin(); itc!=I_singularCells.end(); itc++)
                 {
                     //P_FEMMesh->P_Cells->at(*itc)->Index_ItsCornerVertices
                     for(unsigned i=0; i<P_FEMMesh->P_Cells->at(*itc).Index_ItsCornerVertices.size(); i++)
                     {
                         if(P_FEMMesh->P_Cells->at(*itc).Index_ItsCornerVertices[i]!=*it)
                         {
                             IvertexIndice.insert(P_FEMMesh->P_Cells->at(*itc).Index_ItsCornerVertices[i]);
                         }
                     }
                 }
                 //-------------------
                 for(set<unsigned>::iterator itv=IvertexIndice.begin(); itv!=IvertexIndice.end(); itv++)
                 {
                     //m_solut[4*(*itv)]=m_solut[4*(*itv)]+c;
                     unsigned ibasis2=4*(*itv);
                     Modified_m_solution[ibasis2]=Modified_m_solution[ibasis2]+c;
                 }
             }

             MSplineFunction * P_FEMSolution2=new MSplineFunction(Modified_m_solution, *P_FEMMesh);
             vector<double>().swap(Modified_m_solution);

             DUxxValue_PLUS_DUyyValue(P_MSplineParametricMap->P_MSplineFunctions[0], P_MSplineParametricMap->P_MSplineFunctions[1],
                                      P_FEMSolution2, Pcoordinate_Ps, UxxPUyy, i, P_FEMMesh, P_ParametricMesh);
             delete P_FEMSolution2;
             P_FEMSolution2=NULL;
        }

        //============================
        //vector<vector<double> > ValuesXYZ;
        //P_MSplineParametricMap->Evaluation(0, 0, Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent, ValuesXYZ, *P_ParametricMesh);

        cell_errors[i] = 0;

        for(int j=0;j<u_gaussnum*v_gaussnum;j++)
        {
            if(Jval[j]<0)
            {
                Jval[j]=-Jval[j];
            }

            int nn = j / v_gaussnum;
            int mm = j % v_gaussnum;

            cell_errors[i] += w[nn][mm]*pow(fval[j]+UxxPUyy[j],2)*Jval[j];
        }

        //=======================================================
        totol_Error = totol_Error+cell_errors[i]*(y1-y0)*(x1-x0)/4.0;
        if(Flag)
        {
            Regular_ResidualError_PhysicalDom=Regular_ResidualError_PhysicalDom+cell_errors[i]*(y1-y0)*(x1-x0)/4.0;
        }
        //=======================================================
        cell_errors[i] = sqrt(cell_errors[i]*(y1-y0)*(x1-x0)/4.0);
    }
    totol_Error = sqrt(totol_Error);
    Regular_ResidualError_PhysicalDom=sqrt(Regular_ResidualError_PhysicalDom);
    //
}

//=====================================================
static void SingularParameterization_PrintErrors(const vector<double> &cell_errorsPhysDom, const double total_ErrorPhysDom,
                        const vector<double> & CellErrEnergyNormPhysDom, const double total_ErrEnergyNormPhysDom,
                        const vector<double> &Cell_errorsResidualPhsicalDom, const double total_Error_ResidualPhsicalDom,
                        const double Regular_EnergyError_PhysicalDom, const double Regular_ResidualError_PhysicalDom)
{
       /* ofstream ofErrors;
        //=====
        ofErrors.open("L2ErrorsOverCellsOnPhysicalDomain.txt");
        for(unsigned int i_error=0; i_error<cell_errorsPhysDom.size(); i_error++)
        {
            ofErrors<<cell_errorsPhysDom[i_error]<<" "<<endl;
        }
        ofErrors<<endl;
        ofErrors.close();
        //=====
        ofErrors.open("EnergyNormErrorsOverCellsOnPhysicalDomain.txt");
        for(unsigned int i_error=0; i_error<CellErrEnergyNormPhysDom.size(); i_error++)
        {
            ofErrors<<CellErrEnergyNormPhysDom[i_error]<<" "<<endl;
        }
        ofErrors<<endl;
        ofErrors.close();
        //======
        ofErrors.open("ResidualErrorsOverCellsOnPhysicalDomain.txt");
        for(unsigned int i_error=0; i_error<Cell_errorsResidualPhsicalDom.size(); i_error++)
        {
            ofErrors<<Cell_errorsResidualPhsicalDom[i_error]<<" "<<endl;
        }
        ofErrors<<endl;
        ofErrors.close();*/
        //======
        ofstream ofTot_Error;
        //======

        ofTot_Error.open("L2Total_ErrorOnPhysicalDomain.txt");
        ofTot_Error<<total_ErrorPhysDom<<endl;
        ofTot_Error.close();
        //======
        ofTot_Error.open("EnergyNormTotalErrorOnPhysicalDomain.txt");
        ofTot_Error<<total_ErrEnergyNormPhysDom<<endl;
        ofTot_Error.close();
        //======
        ofTot_Error.open("ResidualTotalErrorOnPhysicalDomain.txt");
        ofTot_Error<<total_Error_ResidualPhsicalDom<<endl;
        ofTot_Error.close();
       /*
        * //======
        ofTot_Error.open("RegularDomain_EnergyNormTotal.txt");
        ofTot_Error<<Regular_EnergyError_PhysicalDom<<endl;
        ofTot_Error.close();
        //======
        ofTot_Error.open("RegularDomain_ResidualErrorTotal.txt");
        ofTot_Error<<Regular_ResidualError_PhysicalDom<<endl;
        ofTot_Error.close();
        */
}

//=============================================
/*SingularParameterization_Errors(gausspt, w, m_solut, P_FEMMesh, P_ParametricMesh,
          &ParametricMap, SingularCellIndice, ExampleIndex,
          cell_errors, totol_Error,
          cell_errorsPhysDom, total_ErrorPhysDom,
          CellErrEnergyNormPhysDom, total_ErrEnergyNormPhysDom,
          cell_errorsResidualPhsicalDom, total_Error_ResidualPhsicalDom);*/

void SingularParameterization_Errors(const vector<double> & gausspt, double ** w,
               Vec &m_solut,
               Mesh* P_FEMMesh, Mesh* P_ParametricMesh,
               MSplineFunctions* P_ParametricMap,const set<unsigned int>& SingularCellsIndices,
               const set<unsigned> & SingularVertexIndiceFEM,unsigned int ExampleIndex,
               vector<double> &cell_L2errorsPhysDom, double total_L2ErrorPhysDom,
               vector<double> &CellErrEnergyNormPhysDom, double total_ErrEnergyNormPhysDom,
               vector<double> &cell_errorsResidualPhsicalDom, double total_Error_ResidualPhsicalDom,
               double &Regular_EnergyError_PhysicalDom, double &Regular_ResidualError_PhysicalDom)
{

    //L2NormError
    SingularParameterization_ComputError_MSplineParametricMap_OnPhysicalDomain(gausspt, gausspt, w, m_solut,
                                                      cell_L2errorsPhysDom, total_L2ErrorPhysDom,
                                                       P_FEMMesh,  P_ParametricMesh,
                                                      P_ParametricMap, ExampleIndex);
    //EnergyNormError
    SingularParameterization_EnergyNormError_PhyscialDomain_RefinedParametricMap(gausspt, gausspt,
                       w, m_solut, CellErrEnergyNormPhysDom, total_ErrEnergyNormPhysDom,Regular_EnergyError_PhysicalDom,
                       P_FEMMesh, P_ParametricMesh, P_ParametricMap, SingularCellsIndices, SingularVertexIndiceFEM, ExampleIndex);

    //Residual Error
    SingularParameterization_ResidualError_PhysicalDomain_RefinedParametricMap(gausspt, gausspt, w, m_solut,
                                                         cell_errorsResidualPhsicalDom, total_Error_ResidualPhsicalDom,
                                                         Regular_ResidualError_PhysicalDom,
                                                         P_FEMMesh, P_ParametricMesh, P_ParametricMap,
                                                         SingularCellsIndices, SingularVertexIndiceFEM, ExampleIndex);


    //=========PutOut Errors=============
    SingularParameterization_PrintErrors(cell_L2errorsPhysDom, total_L2ErrorPhysDom,
                CellErrEnergyNormPhysDom, total_ErrEnergyNormPhysDom,
                 cell_errorsResidualPhsicalDom, total_Error_ResidualPhsicalDom, Regular_EnergyError_PhysicalDom,
                                         Regular_ResidualError_PhysicalDom);
    //===================================
}


#endif
