#include <iostream>
#include <fstream>
#include <map>
#include <time.h>

//PETSc
#include <petsc.h>
#include <petscksp.h>

//Spline
#include "msp/Mesh.h"
#include "msp/BasesVertex.h"
#include "msp/Bases.h"
#include "msp/BasesCell.h"
#include "msp/MSplineFunction.h"
#include "msp/PointSet.h"
#include "msp/Coordinate.h"
#include "msp/MSplineFunctions.h"
#include "msp/Functions.h"
//FEM
#include "msp/FEM_Functions.h"

//Parametrization
#include "msp/PMap_DynamicFunctions.h"
#include "msp/PMap_Functions.h"
#include "msp/FEM_Part.h"

using namespace std;

//int main()
//{
//    //main(): solve non homogenous boundary condition over fan shapes with polar coordinates transformation
//    //intergral mspfem and mspfem_NonhomoBoundaryCondition
//    //

//    //
//}

//=*=*=*=**+=8=*=*=88=8+8=8==88+=8=*=8=*=*=8=*+*=*=*=8=8=*+*=*=8=8=8=8=8++*=*+*+8=8==*=*=8=8==*+8==8=8=*++*=8==*+8
//static void ReadWingData(string file, vector<vector<double> > &QVertices)
//{
//    ifstream infile(file.c_str());
//    if(infile == NULL)//Do not find this file
//    {
//        cout<< "There is no elements in Wing's data File:" << file <<endl;
//    }
//    else
//    {
//        vector<double> QVi;
//        unsigned int Size1, Size2;
//        infile>> Size1;
//        infile>> Size2;
//        Size1=Size1+Size2;

//        double v;
//        for(unsigned int i=0; i<Size1; i++)
//        {
//            QVi.clear();
//            infile>>v;
//            QVi.push_back(v+2.0);//x-direction:0->1 => 2->3
//            infile>>v;
//            QVi.push_back(v);
//            QVertices.push_back(QVi);

//            //cout<<"QVi=("<<QVi[0]<<" , "<<QVi[1]<<")"<<endl;
//        }
//    }

//}


//static void GeneratePsQsFromWingQ(unsigned int NumPEdge, const vector<vector<double> > & QVertices,
//                                  const vector<pair<unsigned int, unsigned int> > &InitialEdges,
//                                  const vector<unsigned int> &EdgesIndexofCells, Mesh * P_ParametricMesh,
//                                  vector<PointSet> &Ps, vector<vector<Coordinate *> > & Qs)
//{
//    //Qs===========================
//    Qs.clear();
//    vector<Coordinate *> Qsi;
//    //UpBoundary
//    for(unsigned int i=0; i<4; i++)
//    {
//        Qsi.clear();
//        for(unsigned int j=0; j<NumPEdge; j++)
//        {
//            Coordinate * PCoor=new Coordinate(QVertices[(NumPEdge-1)*i+j][0], QVertices[(NumPEdge-1)*i+j][1]);
//            Qsi.push_back(PCoor);
//        }
//        Qs.push_back(Qsi);
//    }
//    //DownBoundary
//    unsigned int n=65;
//    for(unsigned int i=0; i<4; i++)
//    {
//        Qsi.clear();

//        for(unsigned int j=0; j<NumPEdge; j++)
//        {
//            Coordinate * PCoor=new Coordinate(QVertices[n+(NumPEdge-1)*i+j][0], QVertices[n+(NumPEdge-1)*i+j][1]);
//            Qsi.push_back(PCoor);
//        }
//        Qs.push_back(Qsi);
//    }
//    //
//    //Ps===============================
//    Ps.clear();
//    PointSet Psi;
//    for(unsigned int i=0; i<InitialEdges.size(); i++)
//    {
//        Psi.Index_ItsCell=EdgesIndexofCells[i];
//        Psi.P_coordinates_Ps.clear();
//        unsigned int IndexV1=InitialEdges[i].first;
//        unsigned int IndexV2=InitialEdges[i].second;
//        int Flag=P_ParametricMesh->P_Cells->at(EdgesIndexofCells[i]).TheLocalDirection(IndexV1, IndexV2);
//        //s-direction (1), -s -direction (-1); t-direction (2), -t -direction (-2)
//        double dst=1.0/(NumPEdge-1.0);
//        for(unsigned int j=0; j<NumPEdge; j++)
//        {
//            if(Flag==1||Flag==-1)//s-direction
//            {
//                if(Flag==1)
//                {
//                    Coordinate * PCoor=new Coordinate(j*dst, 1.0);
//                    Psi.P_coordinates_Ps.push_back(PCoor);
//                }
//                else//Flag==-1
//                {
//                    Coordinate *PCoor=new Coordinate(1.0-j*dst, 1.0);
//                    Psi.P_coordinates_Ps.push_back(PCoor);
//                }
//            }
//            else//t-direction Flag==2||Flag==-2: RMK:In this wings example, there is no this part
//            {}
//        }
//        Ps.push_back(Psi);
//    }
//    //
//    //
//}
///*---------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------*/
////int main(int argc,char **args)
////{
////    //This is for the example that fitting a part of boundary with a set of point data
////    //================================================
////    //Read the ParametricMap
////    //Mesh
////    cout<<"Read the mesh, subdivide it and parametric map"<<endl;


////    Mesh * P_ParametricMesh=new Mesh;
////    //Coeff
////    vector<vector<double> > Coeff_Parametric_Map;

////    unsigned int dimension=2;
////    unsigned int VerNum=0;

////    P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/ExampleWings/DrawedWing/ParametricMesh.txt");

////    //Read the coefficients
////    VerNum=P_ParametricMesh->P_Vertices->size();
////    Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/ExampleWings/DrawedWing/ParametricCoeffTestWing2.txt", dimension, VerNum, Coeff_Parametric_Map);


//////    cout<<"P_ParametricMesh"<<endl;
//////    P_ParametricMesh->Cout_Mesh_File();
//////    cin.get();


////    //Generate the initial Parametric map
////    MSplineFunctions * P_ParametricMap = new MSplineFunctions(Coeff_Parametric_Map, *P_ParametricMesh);

//////    cout<<"Input the subdivision time:"<<endl;
////    unsigned int k;
//////    cin>>k;
////    //P_ParametricMap->CoutThisMSplineFunctionsOverAFinerMesh(k, P_ParametricMesh);

//////================================================================
////    //Read the set of edges of *P_ParametricMesh
////    vector<pair<unsigned int, unsigned int> > InitialEdges;
////    vector<unsigned int> EdgesIndexofCells;
////    //ReadTheEdges(ExampleIndex, InitialEdges, EdgesIndexofCells);
////    REdges("/Users/wumeng/MyWork/msplinetools/data/Examples/EdgesForExamples/ExampleWingFitting/Edges.txt",InitialEdges, EdgesIndexofCells);
//////=================================================================

////    //Generate Ps and Qs
////    unsigned int pho;
//////    cout<<"Put in the density of points between two adjacent vertices"<<endl;
//////    cin>>pho;
//////    //---

////    pho=3;
////    k=3;
////    //==========================================
////    vector<PointSet> Ps;
////    vector<vector<Coordinate *> > Qs;
////    unsigned int NumPEdge=(k+1)*pho+(k+2);

////    //*************************
////    //1. Read The data on the image of these edges
////    vector<vector<double> > QVertices;
////    ReadWingData("/Users/wumeng/MyWork/msplinetools/data/Examples/EdgesForExamples/ExampleWingFitting/rae2822.txt", QVertices);

////    //2. Generate Ps, Qs //Do not implement
////    GeneratePsQsFromWingQ(NumPEdge,QVertices,InitialEdges,EdgesIndexofCells, P_ParametricMesh, Ps, Qs);
////    //*************************

//////    //===============================================================
//////    cout<<"Ps.size = "<<Ps.size()<<endl;
//////    cout<<"Qs.size = "<<Qs.size()<<endl;
//////    cout<<"Ps:"<<endl;
//////    for(unsigned int i=0; i<Ps.size(); i++)
//////    {
//////        cout<<"Ps["<<i<<"]:"<<endl;
//////        cout<<"Index_ItsCell = "<<Ps[i].Index_ItsCell<<endl;
//////        for(unsigned int j=0; j<Ps[i].P_coordinates_Ps.size(); j++)
//////        {
//////            Coordinate * P_coor=Ps[i].P_coordinates_Ps[j];
//////            cout<<"("<<P_coor->xy[0]<<", "<<P_coor->xy[1]<<")"<<endl;
//////            cout<<"Qs: ("<<Qs[i][j]->xy[0]<<", "<<Qs[i][j]->xy[1]<<")"<<endl;
//////        }
//////        cin.get();

//////    }
//////    //================================================================


////    map<unsigned int, unsigned int> OrigialVertexToCurrentVertex;

////    map<unsigned int, pair<unsigned int ,Coordinate*> > CurrentVerticesIndexToCoordinate;

////    vector<vector<unsigned int> > CurrentVerticesEdges;
////    //CurrentVertices[i] is the set of vertex indices which are on InitialEdges[i]
////    Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally_Vertex(k,InitialEdges,EdgesIndexofCells,
////                                      OrigialVertexToCurrentVertex, CurrentVerticesEdges, CurrentVerticesIndexToCoordinate);

////    //cout<<"P_FEMMesh:"<<endl;
////    //P_FEMMesh->Cout_Mesh_File();


////    MSplineFunctions * P_RefinedParametricMap;
////    vector<vector<double> > CoeffRefinedParametricMap;


////    P_RefinedParametricMap=ReExpressionParametrization(k, pho, P_ParametricMap,
////                           CurrentVerticesIndexToCoordinate, CurrentVerticesEdges, Qs,
////                           P_FEMMesh, P_ParametricMesh, CoeffRefinedParametricMap);

//////PETSc Initialize
////PetscInitialize(&argc, &args, (char*) 0, NULL);

////    vector<PointSet> ModifiedPs;
////    vector< vector<Coordinate *> > ModifiedQs;
////    cout<<"Test:ModifyRefinedParametrization"<<endl;

////    ModifyRefinedParametrization(argc, args, k, pho, CurrentVerticesEdges,
////                                 P_FEMMesh, CoeffRefinedParametricMap, Ps, Qs,
////                                 ModifiedPs, ModifiedQs, P_RefinedParametricMap);
//////PETSc Finalize
////PetscFinalize();

////    cout<<"Print the Parametric Map:"<<endl;
////    PrintParametricMap(CoeffRefinedParametricMap, P_FEMMesh);
////}

////====================================================================================
////====================================================================================
////====================================================================================
////====================================================================================

int main()
{
    //Read the ParametricMap
    //Mesh
    cout<<"Read the mesh, subdivide it and parametric map"<<endl;

    Mesh * P_ParametricMesh=new Mesh;
    //Coeff
    vector<vector<double> > Coeff_Parametric_Map;

    unsigned int dimension=3;
    unsigned int VerNum=0;

    //P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/ModifyParameterization/MultiholeStructure/InitialMap/ParametricMesh.txt");

    P_ParametricMesh->ReadFile_withoutHangingVertices("../ParametricMesh.txt");
    //Read the coefficients
    VerNum=P_ParametricMesh->P_Vertices->size();
    //Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/ModifyParameterization/MultiholeStructure/InitialMap/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);

    Reader_Coeff_ParametricMap("../ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);

    cout<<"Coeff_Parametric_Map[0].size()="<<Coeff_Parametric_Map[0].size()<<endl;
    //Generate the initial Parametric map
    MSplineFunctions * P_ParametricMap = new MSplineFunctions(Coeff_Parametric_Map, *P_ParametricMesh);

    //
    cout<<"Input the subdivision time:"<<endl;
    unsigned int k;
    cin>>k;
    vector<vector<double> > CoeffMap;
    P_ParametricMap->CoutThisMSplineFunctionsOverAFinerMesh(k, P_ParametricMesh,CoeffMap);
    //

    cin.get();

    //Cout Jacobian of the initial parameterization:
    unsigned int n_u=20;
    unsigned int n_v=20;
    P_ParametricMap->CoutThisMSplineFunctionsJacobianMeshAxelFile(P_ParametricMesh, n_u, n_v, "JacobianAxelMesh.axl" , "filenameAreas.txt");


    P_ParametricMap->CoutThisMSplineFunctionsAsBSplineSurfaceAxelFile(P_ParametricMesh);
    /*
    cout<<"Input the subdivision time:"<<endl;
    unsigned int k;
    cin>>k;
    vector<vector<double> > CoeffMap;
    P_ParametricMap->CoutThisMSplineFunctionsOverAFinerMesh(k, P_ParametricMesh, CoeffMap);
    */


    return 0;
}

