#include <iostream>
#include <fstream>
#include <map>
#include <utility>
#include <time.h>

////PETSc
#include <petsc.h>
#include <petscksp.h>

//Data structure
#include "msp/Node.h"

//Spline
#include "msp/Mesh.h"
#include "msp/MSplineFunction.h"
#include "msp/PointSet.h"
#include "msp/Coordinate.h"
#include "msp/MSplineFunctions.h"
#include "msp/Functions.h"

//Parametric Map
#include "msp/PMap_Functions.h"
using namespace std;


int main(int argc,char **args)
{
    /**                 Arguments of this application
     * In this application, this code is for testing the parameterization part
     * in msp_P_FEM,
     * Thus, you can ignore this application.
    */

//    map<unsigned int, unsigned int> IVQ;//from the indices of Vset to the indices of Qset
//
//    double Pi=3.1415926;
//    //============Initial Parametric Map=========
//    unsigned int VerNum;
//    unsigned int dimension=2;
//    //Mesh
//    Mesh * P_ParametricMesh=new Mesh;
//    //Coeff
//    vector<vector<double> > Coeff_Parametric_Map;
//    unsigned int ExampleIndex=4;//default
//    cout<<"Please put in the index of this example"<<endl;
//
//    cout<<"Remark:----------------------------------"<<endl;
//    cout<<"1-->Example 1_1;"<<endl;
//    cout<<"2-->Example 2_0;"<<endl;
//    cout<<"3-->Example 2_1;"<<endl;
//    cout<<"4-->Example 4;"<<endl;
//    cout<<"5-->TestErrorOrderExample1_BiCubicSolution"<<endl;
//    cout<<"6-->TestErrorOrderExample2_GeneralSolution"<<endl;
//    cout<<"7-->-div(K divF)=f: the same mesh with Example 4"<<endl;
//    cout<<"8-->-div(K divF)=f with mesh [-1,1]X[-1,1]"<<endl;
//    cout<<"9-->-div(divF)=f with mesh [-1,1]X[-1,1]"<<endl;
//    cout<<"10-->test1: a parametric map's error"<<endl;
//    cout<<"11--> test2: a parametric map's error"<<endl;
//    cout<<"12-->Example4_1: for fitting"<<endl;
//    cin>>ExampleIndex;
//
//    //Read the mesh and coeff
//    switch(ExampleIndex)
//    {
//    case 1://Example 1_1
//        P_ParametricMesh->ReadFile_withoutHangingVertices("/user/mewu/home/axel-plugins/msplinetools/data/Examples/Example1_1/ParametricMesh.txt");
//        //P_ParametricMesh->Cout_Mesh_File();
//        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
//        Reader_Coeff_ParametricMap("/user/mewu/home/axel-plugins/msplinetools/data/Examples/Example1_1/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
//        break;
//    case 2://Example 2_0;
//        P_ParametricMesh->ReadFile_withoutHangingVertices("/user/mewu/home/axel-plugins/msplinetools/data/Examples/Example2_0/BasedOnParametricMap_2/ParametricMesh.txt");
//        //P_ParametricMesh->Cout_Mesh_File();
//        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
//        Reader_Coeff_ParametricMap("/user/mewu/home/axel-plugins/msplinetools/data/Examples/Example2_0/BasedOnParametricMap_2/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
//        break;
//    case 3://Example 2_1
//        P_ParametricMesh->ReadFile_withoutHangingVertices("/user/mewu/home/axel-plugins/msplinetools/data/Examples/Example2_1/BasedOnParametricMap_2/ParametricMesh.txt");
//        //P_ParametricMesh->Cout_Mesh_File();
//        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
//        Reader_Coeff_ParametricMap("/user/mewu/home/axel-plugins/msplinetools/data/Examples/Example2_1/BasedOnParametricMap_2/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
//        break;
//    case 4://Example 4
//        P_ParametricMesh->ReadFile_withoutHangingVertices("/user/mewu/home/axel-plugins/msplinetools/data/Examples/Example4/ParametricMesh.txt");
//        //P_ParametricMesh->Cout_Mesh_File();
//        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
//        Reader_Coeff_ParametricMap("/user/mewu/home/axel-plugins/msplinetools/data/Examples/Example4/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
//        break;
//    case 5://TestErrorOrderExample1_BiCubicSolution
//        P_ParametricMesh->ReadFile_withoutHangingVertices("/user/mewu/home/axel-plugins/msplinetools/data/Examples/TestErrorOrderExamples/ParametricMesh.txt");
//        //P_ParametricMesh->Cout_Mesh_File();
//        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
//        Reader_Coeff_ParametricMap("/user/mewu/home/axel-plugins/msplinetools/data/Examples/TestErrorOrderExamples/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
//        break;
//    case 6://TestErrorOrderExample2_GeneralSolution
//        P_ParametricMesh->ReadFile_withoutHangingVertices("/user/mewu/home/axel-plugins/msplinetools/data/Examples/TestErrorOrderExamples/ParametricMesh.txt");
//        //P_ParametricMesh->Cout_Mesh_File();
//        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
//        Reader_Coeff_ParametricMap("/user/mewu/home/axel-plugins/msplinetools/data/Examples/TestErrorOrderExamples/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
//        break;
//    case 7://-div(K divF)=f: with the same mesh with Example 4
//        P_ParametricMesh->ReadFile_withoutHangingVertices("/user/mewu/home/axel-plugins/msplinetools/data/Examples/Example4/ParametricMesh.txt");
//        //P_ParametricMesh->Cout_Mesh_File();
//        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
//        Reader_Coeff_ParametricMap("/user/mewu/home/axel-plugins/msplinetools/data/Examples/Example4/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
//        break;
//    case 8://-div(K divF)=f: with mesh [-1,1]X[-1,1]
//        P_ParametricMesh->ReadFile_withoutHangingVertices("/user/mewu/home/axel-plugins/msplinetools/data/Examples/Example8/ParametricMesh.txt");
//        //P_ParametricMesh->Cout_Mesh_File();
//        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
//        Reader_Coeff_ParametricMap("/user/mewu/home/axel-plugins/msplinetools/data/Examples/Example8/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
//        break;
//    case 9://-div(divF)=f: with mesh [-1,1]X[-1,1]
//        P_ParametricMesh->ReadFile_withoutHangingVertices("/user/mewu/home/axel-plugins/msplinetools/data/Examples/Example8/ParametricMesh.txt");
//        //P_ParametricMesh->Cout_Mesh_File();
//        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
//        Reader_Coeff_ParametricMap("/user/mewu/home/axel-plugins/msplinetools/data/Examples/Example8/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
//        break;
//    case 10:// test1: a parametric map's error
//        P_ParametricMesh->ReadFile_withoutHangingVertices("/user/mewu/home/axel-plugins/msplinetools/data/Examples/Example4/ParametricMesh.txt");
//        //P_ParametricMesh->Cout_Mesh_File();
//        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
//        Reader_Coeff_ParametricMap("/user/mewu/home/axel-plugins/msplinetools/data/Examples/Example4/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
//        break;
//    case 11:// test2: a parametric map's error
//        P_ParametricMesh->ReadFile_withoutHangingVertices("/user/mewu/home/axel-plugins/msplinetools/data/Examples/Example4/ParametricMesh.txt");
//        //P_ParametricMesh->Cout_Mesh_File();
//        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
//        Reader_Coeff_ParametricMap("/user/mewu/home/axel-plugins/msplinetools/data/Examples/Example4/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
//        break;
//    case 12:
//        P_ParametricMesh->ReadFile_withoutHangingVertices("/user/mewu/home/axel-plugins/msplinetools/data/Examples/Example4_1-circle/ParametricMesh.txt");
//        //P_ParametricMesh->Cout_Mesh_File();
//        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
//        Reader_Coeff_ParametricMap("/user/mewu/home/axel-plugins/msplinetools/data/Examples/Example4_1-circle/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
//        break;
//
//    default:
//        cout<<"It is Default~"<<endl;
//
//        P_ParametricMesh->ReadFile_withoutHangingVertices("/user/mewu/home/axel-plugins/msplinetools/data/Examples/ExampleBox/ParametricMesh.txt");
//        //P_ParametricMesh->Cout_Mesh_File();
//        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
//        dimension=3;
//        Reader_Coeff_ParametricMap("/user/mewu/home/axel-plugins/msplinetools/data/Examples/ExampleBox/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
//        break;
//    }
//
//    //Generate the initial Parametric map
//    MSplineFunctions* P_ParametricMap=new MSplineFunctions(Coeff_Parametric_Map, *P_ParametricMesh);
//    //---------------------------------------------------
//    map<unsigned int, unsigned int> WWW;
//    Mesh P_FEMM=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(2,WWW);
//
//
//
//    //===================================================
//
//
//
//    //--------------------------
//    //matrix_size=4*VerNum;
//
//
// //======================================================
// //One Loop:
// //======================================================
//     //the Data structure of curves (the connection relationship of points)
//     //Indices of Vertices of the parametric Mesh
//     vector<unsigned int> Vset;
//     //For this example, initialize the points set.
//     //1. on the parametric mesh
//     for(unsigned int i=1; i<17; i++)
//     {
//         Vset.push_back(i);
//     }
//     ////////////////////////////
//
//     vector<Node<unsigned int>* > Tv;
//     for(unsigned int i=0; i<Vset.size(); i++)
//     {
//         Node<unsigned int> * Nv=new Node<unsigned int>;
//         if(i<Vset.size()-1)
//         {
//             Nv->ItsChildren.push_back(i+1);
//         }
//         else
//         {
//             Nv->ItsChildren.push_back(0);
//         }
//
//         Tv.push_back(Nv);
//     }
//
//     for(unsigned int i=0; i<Tv.size(); i++)
//     {
//         for(unsigned int j=0; j<Tv[i]->ItsChildren.size(); j++)
//         {
//             unsigned int I_temp=Tv[i]->ItsChildren[j];
//             Tv[I_temp]->ItsParents.push_back(i);
//         }
//     }
//
//     //2. On the physical domain
//     vector<Coordinate *> Qset;
//     for(unsigned int i=0; i<64; i++)
//     {
//         Coordinate * XY=new Coordinate;
//         XY->xy[0]=cos(Pi*i/32.0);
//         XY->xy[1]=sin(Pi*i/32.0);
//         Qset.push_back(XY);
//
// //         Coordinate * XY1=new Coordinate;
// //         XY1->xy[0]=cos(Pi*(i+1)/32.0);
// //         XY1->xy[1]=sin(Pi*(i+1)/32.0);
// //         Qset.push_back(XY1);
//
//     }
//
//     vector<Node<unsigned int>* > Tq;
//     for(unsigned int i=0; i<Qset.size(); i++)
//     {
//         Node<unsigned int> *Nq=new Node<unsigned int>;
//         if(i<Qset.size()-1)
//         {
//             Nq->ItsChildren.push_back(i+1);
//         }
//         else
//         {
//             Nq->ItsChildren.push_back(0);
//         }
//         Tq.push_back(Nq);
//     }
//
//     for(unsigned int i=0; i<Qset.size(); i++)
//     {
//         for(unsigned int j=0; j<Tq[i]->ItsChildren.size(); j++)
//         {
//             unsigned int I_temp=Tq[i]->ItsChildren[j];
//             Tq[I_temp]->ItsParents.push_back(i);
//         }
//     }
//
//
//     //3.The correspondence between Vset and Qset
//     //map<unsigned int, unsigned int> IVQ;
//     //from the indices of Vset to the indices of Qset
//     for(unsigned int i=0; i<Vset.size(); i++)
//     {
//         IVQ.insert(pair<unsigned int, unsigned int>(i,4*i));
//
//     }
//
//
//     //========================================================================================
//     UpDataParametricMap(argc, args, Vset, Tv, Qset, Tq, P_ParametricMesh, IVQ, P_ParametricMap, Coeff_Parametric_Map);
//     //========================================================================================
//     PrintParametricMap(Coeff_Parametric_Map, P_ParametricMesh);
//     //===========================================

     return 0;
 }

