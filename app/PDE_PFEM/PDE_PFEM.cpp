// MSplinesP_FEM.cpp
/*        Arguments of this application
 *In this application, we want to solve the following PDEs:
 *1: x^2 grad ((1/x^2) grad F)=f
 *2: x^2 grad ((1/x^2) grad F)=f+F
 *3: x^2 grad ((1/x^2) grad F)=f+F^k, where k=1,2,3,4 ...
 *   Here, we take k=2
 *4: x^2 grad ((1/x^2) grad F)=R((F-F(center))/(F(XPoint)-F(center)))
 *
 * Now this application is being completed.
 *
 */

#include <iostream>
#include <fstream>
#include <map>
#include <time.h>

//PETSc
#include <petsc.h>
#include <petscksp.h>

//Spline
#include "msp/Mesh.h"
#include "msp/BasesVertex.h"
#include "msp/Bases.h"
#include "msp/BasesCell.h"
#include "msp/MSplineFunction.h"
#include "msp/PointSet.h"
#include "msp/Coordinate.h"
#include "msp/MSplineFunctions.h"
#include "msp/Functions.h"
//FEM
#include "msp/FEM_Functions.h"

//Parametrization
#include "msp/PMap_DynamicFunctions.h"
#include "msp/PMap_Functions.h"
#include "msp/FEM_Part.h"

using namespace std;

//======
void solveEquation1(ofstream &OfTime, Mesh * P_ParametricMesh, vector<vector<double> >& Coeff_Parametric_Map,
                    unsigned int ExampleIndex)
{
    Vec load_vect, m_solut;
    Mat coeff_matrix;
    KSPConvergedReason reason;

    unsigned int EquationIndex=1;
    //Generate the initial Parametric map
    MSplineFunctions * P_ParametricMap = new MSplineFunctions(Coeff_Parametric_Map, *P_ParametricMesh);


    cout<<"Input the times of subdivision"<<endl;
    unsigned int k;
    cin>>k;

    map<unsigned int, unsigned int> OrigialVertexToCurrentVertex;
    Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(k, OrigialVertexToCurrentVertex);
    cout<<"OrigialVertexToCurrentVertex"<<endl;
    for(map<unsigned int, unsigned int>::const_iterator jt=OrigialVertexToCurrentVertex.begin();
        jt!=OrigialVertexToCurrentVertex.end(); jt++)
    {
        cout<<jt->first<<" --> "<<jt->second<<endl;
    }
    cin.get();

    cout<<"P_FEMMesh:"<<endl;
    P_FEMMesh->Cout_Mesh_File();


    cout<<"Generate Gauss points and Weights"<<endl;
    OfTime<<endl;
    OfTime<<"Generate Guass points and Weights"<<endl;
    double t0=(double)clock();

    //Preparation: GuassPoints
    cout<<"Please input the number of Guass Points:"<<endl;
    unsigned int Gp;
    cin>>Gp;
    unsigned int u_gaussnum=Gp, v_gaussnum=Gp;
    unsigned int load_gaussnum=5;
    vector<double> gausspt, m_IntePt, m_loadw;
    //Weights/////////
    double **w;
     w=GaussPoints_Weights(u_gaussnum, load_gaussnum, gausspt, m_IntePt, m_loadw);
    double t1=(double)clock();
    cout<<"\"Generate Guass points and Weights\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<endl;

    OfTime<<"\"Generate Guass points and Weights\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<endl;
    OfTime<<"u_gaussnum="<<u_gaussnum<<"  v_gaussnum="<<v_gaussnum<<endl;
    OfTime<<"The Basis Number = "<<4*(P_FEMMesh->P_Vertices->size())<<endl;

    cout<<"The Basis Number = "<<4*(P_FEMMesh->P_Vertices->size())<<endl;
    PetscInt NumVertex = P_FEMMesh->P_Vertices->size();



    cout<<"Initialized coeffMatrix and Generate_coeffMatrix"<<endl;

    OfTime<<endl;
    OfTime<<"Initialized coeffMatrix and Generate_coeffMatrix"<<endl;

    t0=(double)clock();

    MatCreate(PETSC_COMM_WORLD, &coeff_matrix);

    MatSetSizes(coeff_matrix, PETSC_DECIDE, PETSC_DECIDE, NumVertex*4, NumVertex*4);
    MatSetFromOptions(coeff_matrix);
    MatMPIAIJSetPreallocation(coeff_matrix, 64, NULL, 64, NULL);
    MatSeqAIJSetPreallocation(coeff_matrix, PETSC_DEFAULT, NULL);
    MatSetUp(coeff_matrix);

    cout<<"Generate_CoeffMatrix_Based on Cells"<<endl;
    double k1=1.0;
    double k2=1.0;//In fact, for solving these four equations, the KMatrix is non-business with k1, k2.
    Generate_CoeffMatrix_Cell(u_gaussnum, v_gaussnum, coeff_matrix, m_IntePt, m_IntePt, w, P_FEMMesh,
                              P_ParametricMesh, *P_ParametricMap, k1, k2, ExampleIndex);


    t1=(double)clock();
    cout<<"\"Initialized coeffMatrix and Generate_coeffMatrix\" costs ="<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    OfTime<<"\"Initialized coeffMatrix and Generate_coeffMatrix\" costs ="<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    //load_vect
    //vector<double> load_vect;
    cout<<"Initialized load Vector and Generate load Vector"<<endl;

    OfTime<<endl;
    OfTime<<"Initialized load Vector and Generate load Vector"<<endl;
    t0=(double)clock();


    Initialized_loadVector(load_vect, P_FEMMesh->P_Vertices->size());
//    GenerateLoadVector_RefinedParametrization(load_vect, gausspt, m_loadw, P_FEMMesh,
//                                              *P_RefinedParametricMap, k1, k2, ExampleIndex);
    Generate_LoadVector_PDE_PFEM(load_vect, gausspt, m_loadw, P_FEMMesh, P_ParametricMesh,
                        *P_ParametricMap, k1, k2, ExampleIndex, EquationIndex);

    t1=(double)clock();

    cout<<"Finished and it costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    OfTime<<"Finished and it costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    //Set Bases type
    cout<<"Set Bases type"<<endl;
    t0=(double)clock();
    vector<unsigned int> BasesType;
    BasesType.resize(4*P_FEMMesh->P_Vertices->size(), 0);
    Set_Bases_type(P_FEMMesh, BasesType);

    t1=(double)clock();
    cout<<"\"Set Bases type\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    OfTime<<endl;
    OfTime<<"There are "<< 4*P_FEMMesh->P_Vertices->size()<<" bases"<<endl;
    OfTime<<"\"Set Bases type\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;



    //Modify LoadVector and CoeffMatrix
    double C=0;//The value at the boundary of physical domain is zero!

    MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);



    map<unsigned int, set<unsigned int> > HTable;
    Initialized_HTable_withoutHangingVertices_1(P_FEMMesh, HTable);
    cout<<"Modification_LoadVector_CoeffMatrix"<<endl;

    OfTime<<endl;
    OfTime<<"Modification_LoadVector_CoeffMatrix"<<endl;
    t0=(double)clock();
    //The value at the boundary of physical domain is zero!
    Modification_LoadVector_CoeffMatrix(C, BasesType, coeff_matrix, load_vect, HTable);

    MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);
    MatSetOption(coeff_matrix, MAT_SYMMETRIC, PETSC_TRUE);

    t1=(double)clock();
    cout<<"\"Modification_LoadVector_CoeffMatrix\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    OfTime<<"\"Modification_LoadVector_CoeffMatrix\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    //
    VecCreate(PETSC_COMM_WORLD, &m_solut);
    VecSetSizes(m_solut, PETSC_DECIDE, 4*P_FEMMesh->P_Vertices->size());
    VecSetFromOptions(m_solut);

    //Solving the Linear System Part======================================================
    cout<<"solve the Linear system"<<endl;
    t0=(double)clock();
    KSP ksp;
    PC pc;
    KSPCreate(PETSC_COMM_WORLD, &ksp);
   // KSPSetType(ksp, KSPCG);
    KSPSetType(ksp, KSPGMRES);
    KSPSetOperators(ksp, coeff_matrix, coeff_matrix, DIFFERENT_NONZERO_PATTERN);
    //PetscReal rtol=1.e-18;
    //PetscReal stol=1.e-16;

    PetscReal rtol=(1.e-4)*pow(1.0/(1.0*k+1.0),4.0);
    PetscReal stol=(1.e-3)*pow(1.0/(1.0*k+1.0),4.0);

    KSPSetTolerances(ksp, rtol, stol, PETSC_DEFAULT, PETSC_DEFAULT);

    KSPGetPC(ksp, &pc);
    PCSetType(pc, PCGAMG);
    KSPSetFromOptions(ksp);

    KSPSolve(ksp, load_vect, m_solut);

    KSPGetConvergedReason(ksp,&reason);
    //printf
    PetscInt its;
    KSPGetIterationNumber(ksp, &its);
    PetscPrintf(PETSC_COMM_WORLD, "The number of iteration %D\n", its);

    t1=(double)clock();
    cout<<"\"Solving the linear system\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    OfTime<<endl;
    OfTime<<"\"Solving the linear system\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    OfTime<<"k = "<<k<<endl;
    OfTime<<"where, rtol=(1.e-4)*pow(1/(k+2),4.0)="<<rtol<<" and  stol=(1.e-3)*pow(1/(k+2),4.0)= "<<stol<<endl;

    OfTime<<"KSP ConvergedReason= "<<reason<<endl;
    OfTime<<"the number of iteration = "<<its<<endl;
    OfTime<<endl;
    //-------
    OfTime.close();


    //Print load_vect
    PrintLoadVector(load_vect);

    PrintSolution(m_solut);

    PrintAxelFile_FEMSolution(Coeff_Parametric_Map, P_ParametricMesh, k, m_solut);

    //Error========================================================================
    vector<double> cell_errors;//Store the errors over all the cells
    double totol_Error=1.0;
    //----
    vector<double> cell_errorsPhysDom;
    double total_ErrorPhysDom;
    //---
    vector<double> cell_errorsResidual;
    double total_Error_Residual;
    //---
    vector<double> cell_errorsResidualPhsicalDom;
    double total_Error_ResidualPhsicalDom;
    //---
    vector<double> CellErrEnergyNormPhysDom;
    double total_ErrEnergyNormPhysDom;
    //---
    vector<double> CellErrEnergyNormMesh;
    double total_ErrEnergyNormMesh;

    FEMErrors_PDE_PFEM(gausspt, w, m_solut, P_FEMMesh, P_ParametricMesh,
              P_ParametricMap, ExampleIndex, EquationIndex,
              cell_errors, totol_Error,
              cell_errorsPhysDom, total_ErrorPhysDom,
              cell_errorsResidual, total_Error_Residual,
              cell_errorsResidualPhsicalDom,  total_Error_ResidualPhsicalDom,
              CellErrEnergyNormMesh, total_ErrEnergyNormMesh,
              CellErrEnergyNormPhysDom, total_ErrEnergyNormPhysDom);

    //Destroy ksp coeff_matrix load_vec m_solut
    KSPDestroy(&ksp);
    MatDestroy(&coeff_matrix);
    VecDestroy(&load_vect);
    VecDestroy(&m_solut);
}
//=====================================================

void BMatrix_Equation2(Mat &BMatrix, vector<double>& gausspt, vector<double>& gaussw,
                       unsigned int ExampleIndex, unsigned int EquationIndex,
                       const Mesh* P_FEMMesh, const Mesh* P_ParametricMesh,
                       MSplineFunctions * P_MSplineParametricMap)
{
    MatZeroEntries(BMatrix);

    int gaussnum = gausspt.size();

    PetscScalar v;
    PetscInt I, J;
    for(unsigned int i=0;i<P_FEMMesh->P_Cells->size();i++)// Based on each cell
    {
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];
//=============================================================
        vector<double> funval, m_Baseval_k, m_Baseval_l;
        double tmpx;
        vector<vector<double> > m_JPart;
//==============================================================
        vector<Coordinate*> Pcoordinate_Ps;
        for (int l=0; l<gaussnum; l++)
        {
            for (int j=0; j<gaussnum; j++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=(x1+x0+(x1-x0)*gausspt[l]) * 0.5;//u[l];
                CoorP->xy[1]=(y0+y1+(y1-y0)*gausspt[j]) * 0.5;//v[j];
                Pcoordinate_Ps.push_back(CoorP);
            }
        }
//===============================================================
        ComputeFunval_MSplineParametricMap_PDE_PFEM(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent, funval, 4,
                                                    *P_ParametricMesh, P_MSplineParametricMap,ExampleIndex,EquationIndex);//The value of phi in the right side
//===============================================================

        BasesCell *P_BC=new BasesCell(i, *P_FEMMesh);//Bases over the i-th cell
        vector<double> DetJ;
        double k1=1.0, k2=1.0;//Here is non-business with k1, k1, in fact.
        ParametricMapSigma_MSplineParametricMap(Pcoordinate_Ps, m_JPart, DetJ, P_FEMMesh->P_Cells->at(i).ItsParent,
                                                *P_MSplineParametricMap, P_ParametricMesh, k1, k2, ExampleIndex);

        for(unsigned int k=0; k< P_BC->P_Bases_Cell.size(); k++)
        {
            m_Baseval_k.clear();
            value_Cell_Hermite(0, 0, Pcoordinate_Ps, P_BC->P_Bases_Cell[k]->P_HermiteData_Cells[0], m_Baseval_k, *P_FEMMesh);

            I=P_BC->P_Bases_Cell[k]->Index_basis;
            for(unsigned int l=k; l< P_BC->P_Bases_Cell.size(); l++)
            {
                m_Baseval_l.clear();
                value_Cell_Hermite(0, 0, Pcoordinate_Ps, P_BC->P_Bases_Cell[l]->P_HermiteData_Cells[0], m_Baseval_l, *P_FEMMesh);

                tmpx=0.0;
                for(unsigned int j=0; j<m_Baseval_k.size(); j++)
                {
                    int nn = j/gaussnum;
                    int mm = j%gaussnum;

                    tmpx += gaussw[nn]*gaussw[mm]*funval[j]*m_Baseval_k[j]*m_Baseval_l[j]*DetJ[j];
                }

                J=P_BC->P_Bases_Cell[l]->Index_basis;

                v=tmpx * (x1-x0) * (y1-y0)/ 4;

                if(I==J)
                {
                    MatSetValues(BMatrix,1, &I, 1, &J, &v, ADD_VALUES);
                }
                else
                {
                    MatSetValues(BMatrix,1, &I, 1, &J, &v, ADD_VALUES);
                    MatSetValues(BMatrix,1, &J, 1, &I, &v, ADD_VALUES);
                }

            }
        }

    }
}
//=====================================================
void solveEquation2(ofstream &OfTime, Mesh * P_ParametricMesh, vector<vector<double> >& Coeff_Parametric_Map,
                    unsigned int ExampleIndex)
{
    Vec load_vect, m_solut;
    Mat coeff_matrix;
    Mat BMatrix;
    KSPConvergedReason reason;

    unsigned int EquationIndex=2;

    //Generate the initial Parametric map
    MSplineFunctions * P_ParametricMap = new MSplineFunctions(Coeff_Parametric_Map, *P_ParametricMesh);


    cout<<"Input the times of subdivision"<<endl;
    unsigned int k;
    cin>>k;

    map<unsigned int, unsigned int> OrigialVertexToCurrentVertex;
    Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(k, OrigialVertexToCurrentVertex);

    cout<<"Generate Gauss points and Weights"<<endl;
    OfTime<<endl;
    OfTime<<"Generate Guass points and Weights"<<endl;
    double t0=(double)clock();

    //Preparation: GuassPoints
    cout<<"Please input the number of Guass Points:"<<endl;
    unsigned int Gp;
    cin>>Gp;
    unsigned int u_gaussnum=Gp, v_gaussnum=Gp;
    unsigned int load_gaussnum=5;
    vector<double> gausspt, m_IntePt, m_loadw;
    //Weights/////////
    double **w;
     w=GaussPoints_Weights(u_gaussnum, load_gaussnum, gausspt, m_IntePt, m_loadw);
    double t1=(double)clock();
    cout<<"\"Generate Guass points and Weights\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<endl;

    OfTime<<"\"Generate Guass points and Weights\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<endl;
    OfTime<<"u_gaussnum="<<u_gaussnum<<"  v_gaussnum="<<v_gaussnum<<endl;
    OfTime<<"The Basis Number = "<<4*(P_FEMMesh->P_Vertices->size())<<endl;

    cout<<"The Basis Number = "<<4*(P_FEMMesh->P_Vertices->size())<<endl;
    PetscInt NumVertex = P_FEMMesh->P_Vertices->size();

    cout<<"Initialized coeffMatrix and Generate_coeffMatrix"<<endl;

    OfTime<<endl;
    OfTime<<"Initialized coeffMatrix and Generate_coeffMatrix"<<endl;

    t0=(double)clock();

    MatCreate(PETSC_COMM_WORLD, &coeff_matrix);

    MatSetSizes(coeff_matrix, PETSC_DECIDE, PETSC_DECIDE, NumVertex*4, NumVertex*4);
    MatSetFromOptions(coeff_matrix);
    MatMPIAIJSetPreallocation(coeff_matrix, 64, NULL, 64, NULL);
    MatSeqAIJSetPreallocation(coeff_matrix, PETSC_DEFAULT, NULL);
    MatSetUp(coeff_matrix);



    cout<<"Generate_CoeffMatrix_Based on Cells"<<endl;
    double k1=1.0;
    double k2=1.0;//In fact, for solving these four equations, the KMatrix is non-business with k1, k2.
    Generate_CoeffMatrix_Cell(u_gaussnum, v_gaussnum, coeff_matrix, m_IntePt, m_IntePt, w, P_FEMMesh,
                              P_ParametricMesh, *P_ParametricMap, k1, k2, ExampleIndex);

    MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);
    //**********************************

    MatCreate(PETSC_COMM_WORLD, &BMatrix);

    MatSetSizes(BMatrix, PETSC_DECIDE, PETSC_DECIDE, NumVertex*4, NumVertex*4);
    MatSetFromOptions(BMatrix);
    MatMPIAIJSetPreallocation(BMatrix, 64, NULL, 64, NULL);
    MatSeqAIJSetPreallocation(BMatrix, PETSC_DEFAULT, NULL);
    MatSetUp(BMatrix);

    BMatrix_Equation2(BMatrix, gausspt, m_loadw, ExampleIndex, EquationIndex,
                      P_FEMMesh, P_ParametricMesh, P_ParametricMap);
    MatAssemblyBegin(BMatrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(BMatrix, MAT_FINAL_ASSEMBLY);

    MatAXPY(coeff_matrix, -1.0, BMatrix, DIFFERENT_NONZERO_PATTERN);
    //**********************************

    MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);

    t1=(double)clock();
    cout<<"\"Initialized coeffMatrix and Generate_coeffMatrix\" costs ="<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    OfTime<<"\"Initialized coeffMatrix and Generate_coeffMatrix\" costs ="<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    //load_vect
    //vector<double> load_vect;
    cout<<"Initialized load Vector and Generate load Vector"<<endl;

    OfTime<<endl;
    OfTime<<"Initialized load Vector and Generate load Vector"<<endl;
    t0=(double)clock();


    Initialized_loadVector(load_vect, P_FEMMesh->P_Vertices->size());

    Generate_LoadVector_PDE_PFEM(load_vect, gausspt, m_loadw, P_FEMMesh, P_ParametricMesh,
                        *P_ParametricMap, k1, k2, ExampleIndex, EquationIndex);

    t1=(double)clock();

    cout<<"Finished and it costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    OfTime<<"Finished and it costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    //Set Bases type
    cout<<"Set Bases type"<<endl;
    t0=(double)clock();
    vector<unsigned int> BasesType;
    BasesType.resize(4*P_FEMMesh->P_Vertices->size(), 0);
    Set_Bases_type(P_FEMMesh, BasesType);

    t1=(double)clock();
    cout<<"\"Set Bases type\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    OfTime<<endl;
    OfTime<<"There are "<< 4*P_FEMMesh->P_Vertices->size()<<" bases"<<endl;
    OfTime<<"\"Set Bases type\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;



    //Modify LoadVector and CoeffMatrix
    double C=0;//The value at the boundary of physical domain is zero!

    MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);



    map<unsigned int, set<unsigned int> > HTable;
    Initialized_HTable_withoutHangingVertices_1(P_FEMMesh, HTable);
    cout<<"Modification_LoadVector_CoeffMatrix"<<endl;

    OfTime<<endl;
    OfTime<<"Modification_LoadVector_CoeffMatrix"<<endl;
    t0=(double)clock();
    //The value at the boundary of physical domain is zero!
    Modification_LoadVector_CoeffMatrix(C, BasesType, coeff_matrix, load_vect, HTable);

    MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);



    MatSetOption(coeff_matrix, MAT_SYMMETRIC, PETSC_TRUE);

    t1=(double)clock();
    cout<<"\"Modification_LoadVector_CoeffMatrix\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    OfTime<<"\"Modification_LoadVector_CoeffMatrix\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    //
    VecCreate(PETSC_COMM_WORLD, &m_solut);
    VecSetSizes(m_solut, PETSC_DECIDE, 4*P_FEMMesh->P_Vertices->size());
    VecSetFromOptions(m_solut);

    //Solving the Linear System Part======================================================
    cout<<"solve the Linear system"<<endl;
    t0=(double)clock();
    KSP ksp;
    PC pc;
    KSPCreate(PETSC_COMM_WORLD, &ksp);
   // KSPSetType(ksp, KSPCG);
    KSPSetType(ksp, KSPGMRES);
    KSPSetOperators(ksp, coeff_matrix, coeff_matrix, DIFFERENT_NONZERO_PATTERN);
    //PetscReal rtol=1.e-18;
    //PetscReal stol=1.e-16;

    PetscReal rtol=(1.e-4)*pow(1.0/(1.0*k+1.0),4.0);
    PetscReal stol=(1.e-3)*pow(1.0/(1.0*k+1.0),4.0);

    KSPSetTolerances(ksp, rtol, stol, PETSC_DEFAULT, PETSC_DEFAULT);

    KSPGetPC(ksp, &pc);
    PCSetType(pc, PCGAMG);
    KSPSetFromOptions(ksp);

    KSPSolve(ksp, load_vect, m_solut);

    KSPGetConvergedReason(ksp,&reason);
    //printf
    PetscInt its;
    KSPGetIterationNumber(ksp, &its);
    PetscPrintf(PETSC_COMM_WORLD, "The number of iteration %D\n", its);

    t1=(double)clock();
    cout<<"\"Solving the linear system\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    OfTime<<endl;
    OfTime<<"\"Solving the linear system\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    OfTime<<"k = "<<k<<endl;
    OfTime<<"where, rtol=(1.e-4)*pow(1/(k+2),4.0)="<<rtol<<" and  stol=(1.e-3)*pow(1/(k+2),4.0)= "<<stol<<endl;

    OfTime<<"KSP ConvergedReason= "<<reason<<endl;
    OfTime<<"the number of iteration = "<<its<<endl;
    OfTime<<endl;
    //-------
    OfTime.close();


    //Print load_vect
    PrintLoadVector(load_vect);

    PrintSolution(m_solut);

    PrintAxelFile_FEMSolution(Coeff_Parametric_Map, P_ParametricMesh, k, m_solut);

    //Error========================================================================
    vector<double> cell_errors;//Store the errors over all the cells
    double totol_Error=1.0;
    //----
    vector<double> cell_errorsPhysDom;
    double total_ErrorPhysDom;
    //---
    vector<double> cell_errorsResidual;
    double total_Error_Residual;
    //---
    vector<double> cell_errorsResidualPhsicalDom;
    double total_Error_ResidualPhsicalDom;
    //---
    vector<double> CellErrEnergyNormPhysDom;
    double total_ErrEnergyNormPhysDom;
    //---
    vector<double> CellErrEnergyNormMesh;
    double total_ErrEnergyNormMesh;

    FEMErrors_PDE_PFEM(gausspt, w, m_solut, P_FEMMesh, P_ParametricMesh,
              P_ParametricMap, ExampleIndex, EquationIndex,
              cell_errors, totol_Error,
              cell_errorsPhysDom, total_ErrorPhysDom,
              cell_errorsResidual, total_Error_Residual,
              cell_errorsResidualPhsicalDom,  total_Error_ResidualPhsicalDom,
              CellErrEnergyNormMesh, total_ErrEnergyNormMesh,
              CellErrEnergyNormPhysDom, total_ErrEnergyNormPhysDom);

    //Destroy ksp coeff_matrix load_vec m_solut
    KSPDestroy(&ksp);
    MatDestroy(&coeff_matrix);
    VecDestroy(&load_vect);
    VecDestroy(&m_solut);
}

//=====================================================
void BMatrix_Equation3(Mat &BMatrix, vector<double>& gausspt, vector<double>& gaussw,
                       unsigned int ExampleIndex, unsigned int EquationIndex,
                       const Mesh* P_FEMMesh, const Mesh* P_ParametricMesh,
                       MSplineFunctions *P_MSplineParametricMap, Vec &m_solut)
{
    //cout<<"Before BMatrix_Equation3: MatZeroEntries(BMatrix)"<<endl;
    MatZeroEntries(BMatrix);

    //cout<<"Before BMatrix_Equation3: Vec2Stdvector()"<<endl;
    vector<double> coeff;
    Vec2Stdvector(m_solut, coeff);

    MSplineFunction *P_Phi0= new MSplineFunction(coeff, *P_FEMMesh);

    int gaussnum = gausspt.size();

    PetscScalar v;
    PetscInt I, J;
    for(unsigned int i=0; i<P_FEMMesh->P_Cells->size(); i++)// Based on each cell
    {
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];
//=============================================================
        vector<double> funval, m_Baseval_k, m_Baseval_l;
        double tmpx;
        vector<vector<double> > m_JPart;
//==============================================================
        vector<Coordinate*> Pcoordinate_Ps;
        for (int l=0; l<gaussnum; l++)
        {
            for (int j=0; j<gaussnum; j++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=(x1+x0+(x1-x0)*gausspt[l]) * 0.5;//u[l];
                CoorP->xy[1]=(y0+y1+(y1-y0)*gausspt[j]) * 0.5;//v[j];
                Pcoordinate_Ps.push_back(CoorP);
            }
        }
//===============================================================
        ComputeFunval_MSplineParametricMap_PDE_PFEM(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent, funval, 4,
                                                    *P_ParametricMesh, P_MSplineParametricMap, ExampleIndex, EquationIndex);//The value of phi in the right side
//===============================================================

        BasesCell *P_BC=new BasesCell(i, *P_FEMMesh);//Bases over the i-th cell
        vector<double> DetJ;
        vector<double> BiValues;
        double k1=1.0, k2=1.0;//Here is non-business with k1, k1, in fact.
        ParametricMapSigma_MSplineParametricMap(Pcoordinate_Ps, m_JPart, DetJ, P_FEMMesh->P_Cells->at(i).ItsParent,
                                                *P_MSplineParametricMap, P_ParametricMesh, k1, k2, ExampleIndex);
        P_Phi0->Evaluation(0, 0, Pcoordinate_Ps, BiValues, i, *P_FEMMesh);

        for(unsigned int k=0; k< P_BC->P_Bases_Cell.size(); k++)
        {
            m_Baseval_k.clear();
            value_Cell_Hermite(0, 0, Pcoordinate_Ps, P_BC->P_Bases_Cell[k]->P_HermiteData_Cells[0], m_Baseval_k, *P_FEMMesh);

            I=P_BC->P_Bases_Cell[k]->Index_basis;
            for(unsigned int l=k; l< P_BC->P_Bases_Cell.size(); l++)
            {
                m_Baseval_l.clear();
                value_Cell_Hermite(0, 0, Pcoordinate_Ps, P_BC->P_Bases_Cell[l]->P_HermiteData_Cells[0], m_Baseval_l, *P_FEMMesh);

                tmpx=0.0;
                for(unsigned int j=0; j<m_Baseval_k.size(); j++)
                {
                    int nn = j/gaussnum;
                    int mm = j%gaussnum;

                    tmpx += gaussw[nn]*gaussw[mm]*funval[j]*m_Baseval_k[j]*m_Baseval_l[j]*DetJ[j]*BiValues[j];
                }

                J=P_BC->P_Bases_Cell[l]->Index_basis;

                v=tmpx * (x1-x0) * (y1-y0)/ 4;

                //cout<<"BMatrix_Equation3: Before MatSetValues()"<<endl;
                if(I==J)
                {
                    MatSetValues(BMatrix,1, &I, 1, &J, &v, ADD_VALUES);
                }
                else
                {
                    MatSetValues(BMatrix,1, &I, 1, &J, &v, ADD_VALUES);
                    MatSetValues(BMatrix,1, &J, 1, &I, &v, ADD_VALUES);
                }

            }
        }

    }
}
/* This function is used to solving Equation 3 of case 25.
 * InPut: OfTime -> used to record the solving progress
 *        epsilon-> used to control whether the iteration of solving can be stopped
 *        InitialCoeff-> the initial coefficients always set as zeros (the initial value of iteration)
 *        P_ParametricMesh-> the pointer of initial mesh
 *        Coeff_Parametric_Map->the coefficients of this parametric map based on the initial mesh
 *        ExampleIndex-> the index of our example
 *
 * OutPut: IsConvergence -> if IsConvergence is 1 (true), then this iteration step is convergence.
 *                           if IsConvergence is 0 (false), then this iteration step is not convergence.
 * RMK: How to determine "convergence" or not?
 *     Let b^i and b^{i+1} be the solutions at the i-th and (i+1)-th iteration steps.
 *     if ||b^i-b^{i+1}||<epsilon, IsConvergence=1;
 *     Otherwise, IsConvergence=0.
*/
void solveEquation3(ofstream &OfTime, double epsilon, Mesh* P_ParametricMesh,
                    vector<vector<double> >& Coeff_Parametric_Map, unsigned int ExampleIndex, bool& IsConvergence)
{
    Vec load_vect, m_solut;
    Vec m_soluti;
    Mat coeff_matrix;
    Mat coeff_matrix_copy;
    Mat BMatrix;
    KSP ksp;
    PC pc;
    //KSPConvergedReason reason;

    unsigned int EquationIndex=3;

    //Generate the initial Parametric map
    MSplineFunctions * P_ParametricMap = new MSplineFunctions(Coeff_Parametric_Map, *P_ParametricMesh);


    cout<<"Input the times of subdivision"<<endl;
    unsigned int k;
    cin>>k;

    //Rechoose epsilon----
    epsilon=(1.0/(k+1.0))*(1.0/(k+1.0))*(1.0/(k+1.0))*(1.0/(k+1.0));

    map<unsigned int, unsigned int> OrigialVertexToCurrentVertex;
    Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(k, OrigialVertexToCurrentVertex);

    cout<<"Generate Gauss points and Weights"<<endl;
    OfTime<<endl;
    OfTime<<"Generate Guass points and Weights"<<endl;
    double t0=(double)clock();

    //Preparation: GuassPoints
    cout<<"Please input the number of Guass Points:"<<endl;
    unsigned int Gp;
    cin>>Gp;
    unsigned int u_gaussnum=Gp, v_gaussnum=Gp;
    unsigned int load_gaussnum=5;
    vector<double> gausspt, m_IntePt, m_loadw;
    //Weights/////////
    double **w;
     w=GaussPoints_Weights(u_gaussnum, load_gaussnum, gausspt, m_IntePt, m_loadw);
    double t1=(double)clock();
    cout<<"\"Generate Guass points and Weights\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<endl;

    OfTime<<"\"Generate Guass points and Weights\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<endl;
    OfTime<<"u_gaussnum="<<u_gaussnum<<"  v_gaussnum="<<v_gaussnum<<endl;
    OfTime<<"The Basis Number = "<<4*(P_FEMMesh->P_Vertices->size())<<endl;

    cout<<"The Basis Number = "<<4*(P_FEMMesh->P_Vertices->size())<<endl;
    PetscInt NumVertex = P_FEMMesh->P_Vertices->size();

    cout<<"Initialized coeffMatrix and Generate_coeffMatrix"<<endl;

    OfTime<<endl;
    OfTime<<"Initialized coeffMatrix and Generate_coeffMatrix"<<endl;

    t0=(double)clock();

    MatCreate(PETSC_COMM_WORLD, &coeff_matrix);

    MatSetSizes(coeff_matrix, PETSC_DECIDE, PETSC_DECIDE, NumVertex*4, NumVertex*4);
    MatSetFromOptions(coeff_matrix);
    MatMPIAIJSetPreallocation(coeff_matrix, 64, NULL, 64, NULL);
    MatSeqAIJSetPreallocation(coeff_matrix, PETSC_DEFAULT, NULL);
    MatSetUp(coeff_matrix);


    cout<<"Generate_CoeffMatrix_Based on Cells"<<endl;
    double k1=1.0;
    double k2=1.0;//In fact, for solving these four equations, the KMatrix is non-business with k1, k2.
    Generate_CoeffMatrix_Cell(u_gaussnum, v_gaussnum, coeff_matrix, m_IntePt, m_IntePt, w, P_FEMMesh,
                              P_ParametricMesh, *P_ParametricMap, k1, k2, ExampleIndex);

    MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);

    t1=(double)clock();
    cout<<"\"Initialized coeffMatrix and Generate_coeffMatrix\" costs ="<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    OfTime<<"\"Initialized coeffMatrix and Generate_coeffMatrix\" costs ="<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    //**********************************

    //load_vect
    //vector<double> load_vect;
    cout<<"Initialized load Vector and Generate load Vector"<<endl;

    OfTime<<endl;
    OfTime<<"Initialized load Vector and Generate load Vector"<<endl;

    t0=(double)clock();
    Initialized_loadVector(load_vect, P_FEMMesh->P_Vertices->size());

    Generate_LoadVector_PDE_PFEM(load_vect, gausspt, m_loadw, P_FEMMesh, P_ParametricMesh,
                        *P_ParametricMap, k1, k2, ExampleIndex, EquationIndex);
    t1=(double)clock();

    cout<<"Finished and it costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    OfTime<<"Finished and it costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;



    //Set Bases type
    cout<<"Set Bases type"<<endl;
    t0=(double)clock();
    vector<unsigned int> BasesType;
    BasesType.resize(4*P_FEMMesh->P_Vertices->size(), 0);
    Set_Bases_type(P_FEMMesh, BasesType);

    t1=(double)clock();
    cout<<"\"Set Bases type\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    OfTime<<endl;
    OfTime<<"There are "<< 4*P_FEMMesh->P_Vertices->size()<<" bases"<<endl;
    OfTime<<"\"Set Bases type\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;


    map<unsigned int, set<unsigned int> > HTable;
    Initialized_HTable_withoutHangingVertices_1(P_FEMMesh, HTable);

    VecCreate(PETSC_COMM_WORLD, &m_solut);
    VecSetSizes(m_solut, PETSC_DECIDE, 4*P_FEMMesh->P_Vertices->size());
    VecSetFromOptions(m_solut);

    VecZeroEntries(m_solut);

    PetscReal rtol=(1.e-4)*pow(1.0/(1.0*k+1.0),4.0);
    PetscReal stol=(1.e-3)*pow(1.0/(1.0*k+1.0),4.0);

    VecCreate(PETSC_COMM_WORLD, &m_soluti);
    VecSetSizes(m_soluti, PETSC_DECIDE, 4*P_FEMMesh->P_Vertices->size());
    VecSetFromOptions(m_soluti);

    KSPCreate(PETSC_COMM_WORLD, &ksp);

    MatCreate(PETSC_COMM_WORLD, &BMatrix);
    MatSetSizes(BMatrix, PETSC_DECIDE, PETSC_DECIDE, NumVertex*4, NumVertex*4);
    MatSetFromOptions(BMatrix);
    MatMPIAIJSetPreallocation(BMatrix, 64, NULL, 64, NULL);
    MatSeqAIJSetPreallocation(BMatrix, PETSC_DEFAULT, NULL);
    MatSetUp(BMatrix);

    MatAssemblyBegin(BMatrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(BMatrix, MAT_FINAL_ASSEMBLY);


    cout<<"begin to iterate"<<endl;

    PetscReal val=1.0;
    //Iteration:
    unsigned int IterationNum=0;
    while(IterationNum<1000 && IsConvergence==false && val>1.0e-15)
    {

        //cout<<"Before MatAXPY: coeff_matrix_copy"<<endl;

        MatDuplicate(coeff_matrix, MAT_COPY_VALUES, &coeff_matrix_copy);

        MatAssemblyBegin(coeff_matrix_copy, MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd(coeff_matrix_copy, MAT_FINAL_ASSEMBLY);

        //cout<<"Before BMatrix_Equation3"<<endl;

        //MatView(BMatrix, PETSC_VIEWER_STDOUT_SELF);
        BMatrix_Equation3(BMatrix, gausspt, m_loadw, ExampleIndex, EquationIndex,
                          P_FEMMesh, P_ParametricMesh, P_ParametricMap, m_solut);

        MatAssemblyBegin(BMatrix, MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd(BMatrix, MAT_FINAL_ASSEMBLY);

        //cout<<"Before MatAXPY: BMatrix"<<endl;
        MatAXPY(coeff_matrix_copy, -1.0, BMatrix, DIFFERENT_NONZERO_PATTERN);
        //    //**********************************

        MatAssemblyBegin(coeff_matrix_copy, MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd(coeff_matrix_copy, MAT_FINAL_ASSEMBLY);

        //Modify LoadVector and CoeffMatrix
        double C=0;//The value at the boundary of physical domain is zero

        //"Modification_LoadVector_CoeffMatrix"
        //The value at the boundary of physical domain is zero!
        Modification_LoadVector_CoeffMatrix(C, BasesType, coeff_matrix_copy, load_vect, HTable);
        MatAssemblyBegin(coeff_matrix_copy, MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd(coeff_matrix_copy, MAT_FINAL_ASSEMBLY);
        MatSetOption(coeff_matrix_copy, MAT_SYMMETRIC, PETSC_TRUE);

       //Solving the Linear System Part======================================================

        // KSPSetType(ksp, KSPCG);
        KSPSetType(ksp, KSPGMRES);
        KSPSetOperators(ksp, coeff_matrix_copy, coeff_matrix_copy, DIFFERENT_NONZERO_PATTERN);

        KSPSetTolerances(ksp, rtol, stol, PETSC_DEFAULT, PETSC_DEFAULT);
        KSPGetPC(ksp, &pc);
        PCSetType(pc, PCGAMG);
        KSPSetFromOptions(ksp);
        KSPSolve(ksp, load_vect, m_soluti);

//        KSPGetConvergedReason(ksp,&reason);

        //m_solut=m_solut-m_soluti
        VecAXPY(m_solut,-1.0, m_soluti);
        VecNorm(m_solut, NORM_2, &val);

        cout<<"the norm of error val = "<<val<<endl;
        if(val<epsilon)
        {
            IsConvergence=true;
        }

        //m_solut=m_soluti
        VecCopy(m_soluti, m_solut);

        VecAssemblyBegin(m_solut);
        VecAssemblyEnd(m_solut);

        IterationNum = IterationNum + 1;
    }

    OfTime<<"k = "<<k<<endl;
    OfTime<<"where, rtol=(1.e-4)*pow(1/(k+2),4.0)="<<rtol<<" and  stol=(1.e-3)*pow(1/(k+2),4.0)= "<<stol<<endl;

    OfTime<<"IsConvergence = "<<IsConvergence<<endl;
    OfTime<<"The number of iteration = "<<IterationNum<<endl;

    cout<<"IsConvergence = "<<IsConvergence<<endl;
    cout<<"The number of iteration = "<<IterationNum<<endl;
    //-------
    OfTime.close();


    //Print load_vect
    PrintLoadVector(load_vect);

    PrintSolution(m_solut);

    PrintAxelFile_FEMSolution(Coeff_Parametric_Map, P_ParametricMesh, k, m_solut);

    //Error========================================================================
    vector<double> cell_errors;//Store the errors over all the cells
    double totol_Error=1.0;
    //----
    vector<double> cell_errorsPhysDom;
    double total_ErrorPhysDom;
    //---
    vector<double> cell_errorsResidual;
    double total_Error_Residual;
    //---
    vector<double> cell_errorsResidualPhsicalDom;
    double total_Error_ResidualPhsicalDom;
    //---
    vector<double> CellErrEnergyNormPhysDom;
    double total_ErrEnergyNormPhysDom;
    //---
    vector<double> CellErrEnergyNormMesh;
    double total_ErrEnergyNormMesh;

    FEMErrors_PDE_PFEM(gausspt, w, m_solut, P_FEMMesh, P_ParametricMesh,
              P_ParametricMap, ExampleIndex, EquationIndex,
              cell_errors, totol_Error,
              cell_errorsPhysDom, total_ErrorPhysDom,
              cell_errorsResidual, total_Error_Residual,
              cell_errorsResidualPhsicalDom,  total_Error_ResidualPhsicalDom,
              CellErrEnergyNormMesh, total_ErrEnergyNormMesh,
              CellErrEnergyNormPhysDom, total_ErrEnergyNormPhysDom);

    //Destroy ksp coeff_matrix load_vec m_solut
    KSPDestroy(&ksp);
    MatDestroy(&coeff_matrix);
    MatDestroy(&coeff_matrix_copy);
    VecDestroy(&load_vect);
    VecDestroy(&m_solut);
    VecDestroy(&m_soluti);
}

//int main()
int main(int argc,char **args)
{
    /*Initalize PETSc*/
    PetscInitialize(&argc, &args, (char*)0, NULL);

    ofstream OfTime;
    OfTime.open("Time.txt");


    //================================================
    //Read the ParametricMap
    //Mesh
    cout<<"Read the mesh, subdivide it and parametric map"<<endl;

    //----------
    OfTime<<endl;
    OfTime<<"Read the mesh, subdivide it and parametric map"<<endl;

    Mesh * P_ParametricMesh=new Mesh;
    //Coeff
    vector<vector<double> > Coeff_Parametric_Map;
    unsigned int ExampleIndex=4;//default
    cout<<"Please put in the index of this example"<<endl;

    cout<<"Remark:----------------------------------"<<endl;
    cout<<"25--> PDE_PFEM_5_0_1;"<<endl;
    cout<<"29--> PDE_PFEM_Example7_2:alpha shape with exact boundaries"<<endl;
    cout<<"31--> PDE_PFEM_Example0_0: Triangular with a singularity vertex"<<endl;
    cin>>ExampleIndex;

    OfTime<<endl;
    OfTime<<"25--> PDE_PFEM_5_0_1;"<<endl;
    OfTime<<"29--> PDE_PFEM_Example7_2:alpha shape with exact boundaries"<<endl;
    OfTime<<"31--> PDE_PFEM_Example0_0: Triangular with a singularity vertex"<<endl;


    OfTime<<"The mesh of this example that has been chosen: "<< ExampleIndex<<endl;

    unsigned int dimension=2;
    unsigned int VerNum=0;

    switch(ExampleIndex)
    {
    case 25://PDE_PFEM: Example4_1 Square===>the same with case 15 in App "mspP_FEM"
        //RMK: Case 25 is an example with exact boundaries (Thus, it doesn't need to modify its boundaries)
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/TestErrorOrderExamples/IGATest/exactBoundary/Example4_1-square/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/TestErrorOrderExamples/IGATest/exactBoundary/Example4_1-square/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 29://PDE_PFEM: Example7_2:alpha shape with exact boundaries
        //RMK: Case 295 is an example with exact boundaries (Thus, it doesn't need to modify its boundaries)
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/Example7_2/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/Example7_2/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 31://PDE_PFEM: Example0_0: Triangular with a singularity vertex
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/Example0_0/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/Example0_0/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

//    The max case num is 31:

    default:
        P_ParametricMesh->ReadFile_withoutHangingVertices("/user/mewu/home/axel-plugins/msplinetools/data/Examples/Example4/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/user/mewu/home/axel-plugins/msplinetools/data/Examples/Example4/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;
    }

   // P_ParametricMesh->Cout_Mesh_File();


    cout<<"solving x^2 grad( grad phi/x^2)=f(x,y)---> input 1"<<endl;
    cout<<"solving x^2 grad( grad phi/x^2)=f(x,y)+ phi---> input 2"<<endl;
    cout<<"solving x^2 grad( grad phi/x^2)=f(x,y)+ phi^2---> input 3"<<endl;
    cout<<"solving x^2 grad( grad phi/x^2)=R( phi????) ---->input 4"<<endl;
    unsigned int EquationIndex;
    cin>>EquationIndex;


    OfTime<<"-------------------------------"<<endl;
    OfTime<<"solving x^2 grad( grad phi/x^2)=f(x,y)---> input 1"<<endl;
    OfTime<<"solving x^2 grad( grad phi/x^2)=f(x,y)+ phi---> input 2"<<endl;
    OfTime<<"solving x^2 grad( grad phi/x^2)=f(x,y)+ phi^2---> input 3"<<endl;
    OfTime<<"solving x^2 grad( grad phi/x^2)=R( phi????) ---->input 4"<<endl;
    OfTime<<endl;
    OfTime<<"the "<<EquationIndex<<"-th Equation has been choosen."<<endl;

    if(ExampleIndex == 25|| ExampleIndex == 29 || ExampleIndex == 31)
        //Case 25 is an example with exact boundaries (Thus, it doesn't need to modify its boundaries)
    {
        switch (EquationIndex)
        {
        case 1:
            solveEquation1(OfTime, P_ParametricMesh, Coeff_Parametric_Map, ExampleIndex);
            break;
        case 2:
            solveEquation2(OfTime, P_ParametricMesh, Coeff_Parametric_Map, ExampleIndex);
            break;
        case 3:
            double epsilon = 0.1;// Reset in solveEquation3() as (1/(k+1))^4
            //double InitialCoeff=0.0;
            bool IsConvergence=false;
            solveEquation3(OfTime, epsilon, P_ParametricMesh, Coeff_Parametric_Map,
                           ExampleIndex, IsConvergence);
            break;
        //case 4:
            //solveEquation4();
         //   break;
//        default:
//            cout<<"The EquationIndex is out of the range"<<endl;
//            cin.get();
//            break;
        }

    }
    else
    {
    }

        /*Finish PETSc program*/
        PetscFinalize();

}
