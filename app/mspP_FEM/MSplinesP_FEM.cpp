// MSplinesP_FEM.cpp
//==================================================================
//
#include <iostream>
#include <fstream>
#include <map>
#include <time.h>

//PETSc
#include <petsc.h>
#include <petscksp.h>

//Spline
#include "msp/Mesh.h"
#include "msp/BasesVertex.h"
#include "msp/Bases.h"
#include "msp/BasesCell.h"
#include "msp/MSplineFunction.h"
#include "msp/PointSet.h"
#include "msp/Coordinate.h"
#include "msp/MSplineFunctions.h"
#include "msp/Functions.h"
//FEM
#include "msp/FEM_Functions.h"

//Parametrization
#include "msp/PMap_DynamicFunctions.h"
#include "msp/PMap_Functions.h"
#include "msp/FEM_Part.h"

using namespace std;

/**
 * \section MSplinesP_FEM
 *
 * In this application, we solve the PDE \f$-\nabla(K\nabla F)=f \f$ with homogeneous boundary condition
 * over a physcial domain which is bounded by an implict equation.
 * In our algorithm, we modify the parametric map in the solving process.
 **************************
 * -\c Input: Read the initial parametric mesh, the coeffients and the boundary edges of this mesh
 * -\c Output:The solution of the PDE, the errors of PDE
 **************************
 * It uses the MSplineFunctions class to describe the PDE solutions.
 *
 * \subsection other_pde Other Partial Differential Equations
 * If one wants to add their own PDE, the first thing that you should do is given
 * an ExampleIndex for this example (this ExampleIndex should be different from
 * the ExampleIndices of the given examples), then
 *
 * -# if your PDE is \f$-\nabla(K\nabla F)=f \f$, you should modify
 *  - KMatrixValue in FEM_DynamicFunctions.h
 *  - ExactFunctionValue in FEM_DynamicFunctions.h
 *  - RightSideFunctionValue in FEM_DynamicFunctions.h
 *  - FxFunctionValue in FEM_DynamicFunctions.h
 *  - FyFunctionValue in FEM_DynamicFunctions.h
 *
 *  - ReadTheEdges in PMap_Dynamicfunctions.h
 *  - GetPsQsOverPhysicalDomain in PMap_Dynamicfunctions.h
 * where, in GetPsQsOverPhysicalDomain(), the other function (simlar to Example4PhysicalCurve())
 * should be added.
 *
 *
 * -# if your PDE in other form,
 * Please re-write functions to generate coeff_matrix and load_vect
 * Moreover, ReadTheEdges() GetPsQsOverPhysicalDomain() in PMap_Dynamicfunctions.h should
 * be modified, where, in GetPsQsOverPhysicalDomain(), the other function (be simlar to Example4PhysicalCurve())
 * should be added.
 *
 **************************
 * About The paths of examples:
 * Suppose that the current work directionary is XXX/msplinetools/XX
 * where, "XX" is a folder given by ourself for saving the result of this application
 *
 * \subsection usage Usage
 * It can be used as follows:
 * \code
 * ./msplinePFEM
 * \endcode
*/


//int main()
int main(int argc,char **args)
{


    /*Initalize PETSc*/
    PetscInitialize(&argc, &args, (char*)0, NULL);

    ofstream OfTime;
    OfTime.open("Time.txt");


    //================================================
    //Read the ParametricMap
    //Mesh
    cout<<"Read the mesh, subdivide it and parametric map"<<endl;

    //----------
    OfTime<<endl;
    OfTime<<"Read the mesh, subdivide it and parametric map"<<endl;

    Mesh * P_ParametricMesh=new Mesh;
    //Coeff
    vector<vector<double> > Coeff_Parametric_Map;
    unsigned int ExampleIndex=4;//default
    cout<<"Please put in the index of this example"<<endl;

    cout<<"Remark:----------------------------------"<<endl;
    cout<<"1-->Example 1_1;"<<endl;
    cout<<"2-->Example 2_0;"<<endl;
    cout<<"3-->Example 2_1;"<<endl;
    cout<<"4-->Example 4;"<<endl;
    cout<<"5-->TestErrorOrderExample1_BiCubicSolution"<<endl;
    cout<<"6-->TestErrorOrderExample2_GeneralSolution"<<endl;
    cout<<"7-->-div(K divF)=f: the same mesh with Example 4"<<endl;
    cout<<"8-->-div(K divF)=f with mesh [-1,1]X[-1,1]"<<endl;
    cout<<"9-->-div(divF)=f with mesh [-1,1]X[-1,1]"<<endl;
    cout<<"10--> test1: a parametric map's error"<<endl;
    cout<<"11--> test2: a parametric map's error"<<endl;
    cout<<"12--> TestErrorOrderExample: Modified Example 4"<<endl;
    cout<<"13--> TestErrorOrderExample: 16-RegularPolygon"<<endl;
    cout<<"14--> TestErrorOrderExample: Modified 16-RegularPolygon"<<endl;
    cout<<"15--> TestErrorOrderExample: Example4_1 Square"<<endl;

    cout<<"16--> The Polygon with N=8"<<endl;
    cout<<"17--> Modified the Polygon with N=8"<<endl;
    cout<<"18--> The Polygon with N=16"<<endl;
    cout<<"19--> Modified the Polygon with N=16"<<endl;

    cout<<"20--> The Polygon with N=32"<<endl;
    cout<<"21--> Modified the Polygon with N=32"<<endl;
    cout<<"22--> The Polygon with N=64"<<endl;
    cout<<"23--> Modified the Polygon with N=64"<<endl;

    cout<<"24--> Example 2_2;"<<endl;

    cout<<"25--> PDE_PFEM_5_0_1;"<<endl;

    cout<<"27--> Example2_2 [0,1]X[0,1]"<<endl;

    cin>>ExampleIndex;

    OfTime<<endl;
    OfTime<<"Please put in the index of this example"<<endl;
    OfTime<<"1-->Example 1_1;"<<endl;
    OfTime<<"2-->Example 2_0;"<<endl;
    OfTime<<"3-->Example 2_1;"<<endl;
    OfTime<<"4-->Example 4;"<<endl;
    OfTime<<"5-->TestErrorOrderExample1_BiCubicSolution"<<endl;
    OfTime<<"6-->TestErrorOrderExample2_GeneralSolution"<<endl;

    OfTime<<"7-->-grad(K gradF)=f: the same mesh with Example 4"<<endl;
    OfTime<<"8-->-grad(K gradF)=f with mesh [-1,1]X[-1,1]"<<endl;
    OfTime<<"9-->-grad(gradF)=f with mesh [-1,1]X[-1,1]"<<endl;

    OfTime<<"10-->test1: a parametric map's error"<<endl;
    OfTime<<"11--> test2: a parametric map's error"<<endl;
    OfTime<<"12--> TestErrorOrderExample:Modified Example 4"<<endl;
    OfTime<<"13--> TestErrorOrderExample: 16-RegularPolygon"<<endl;
    OfTime<<"14--> TestErrorOrderExample: Modified 16-RegularPolygon"<<endl;
    OfTime<<"15--> TestErrorOrderExample: Example4_1 Square"<<endl;

    OfTime<<"16--> The Polygon with N=8"<<endl;
    OfTime<<"17--> Modified the Polygon with N=8"<<endl;
    OfTime<<"18--> The Polygon with N=16"<<endl;
    OfTime<<"19--> Modified the Polygon with N=16"<<endl;

    OfTime<<"20--> The Polygon with N=32"<<endl;
    OfTime<<"21--> Modified the Polygon with N=32"<<endl;
    OfTime<<"22--> The Polygon with N=64"<<endl;
    OfTime<<"23--> Modified the Polygon with N=64"<<endl;

    OfTime<<"24--> Example 2_2;"<<endl;
    OfTime<<"25--> PDE_PFEM_5_0_1;"<<endl;

    OfTime<<"27--> Example2_2 [0,1]X[0,1]"<<endl;

    OfTime<<"The mesh of this example that has been chosen: "<< ExampleIndex<<endl;

    unsigned int dimension=2;
    unsigned int VerNum;

    /*Set k1 ,k2 for KMatrix in -div(K divF)=f*/
    double k1=0.0, k2=1.0;

    if(ExampleIndex>=7&& ExampleIndex<10)
    {
        cout<<"Put in the parameter k1, k2 for this Example:"<<endl;
        cin>>k1;
        cin>>k2;
        OfTime<<"The parameters of this Example:"<<endl;
        OfTime<<"k1 = "<<k1<<endl;
        OfTime<<"k2 = "<<k2<<endl;
    }

    switch(ExampleIndex)
    {
    case 1://Example 1_1
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example1_1/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/Example1_1/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;
    case 2://Example 2_0;
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example2_0/BasedOnParametricMap_2/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/Example2_0/BasedOnParametricMap_2/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;
    case 3://Example 2_1
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example2_1/BasedOnParametricMap_2/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/Example2_1/BasedOnParametricMap_2/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;
    case 4://Example 4
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/Example4/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/Example4/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);

        break;
    case 5://TestErrorOrderExample1_BiCubicSolution
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/TestErrorOrderExamples/ClassicalTest/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/TestErrorOrderExamples/ClassicalTest/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;
    case 6://TestErrorOrderExample2_GeneralSolution
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/TestErrorOrderExamples/ClassicalTest/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/TestErrorOrderExamples/ClassicalTest/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;
    case 7://-div(K divF)=f: with the same mesh with Example 4
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example4/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/Example4/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;
    case 8://-div(K divF)=f: with mesh [-1,1]X[-1,1]
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example8/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/Example8/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;
    case 9://-div(divF)=f: with mesh [-1,1]X[-1,1]
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example8/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/Example8/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;
    case 10:// test1: a parametric map's error
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example4/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/Example4/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;
    case 11:// test2: a parametric map's error
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example4/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/Example4/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;
    case 12:// Modify the parametric Map of Example 4
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/TestErrorOrderExamples/IGATest/non-exactBoundary/Example4Modified/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/TestErrorOrderExamples/IGATest/non-exactBoundary/Example4Modified/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;
    case 13:// Example4_1: the parametric map: 16-RegularPolygon
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/TestErrorOrderExamples/IGATest/non-exactBoundary/Example4_1-circle/16-RegularPolygon/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/TestErrorOrderExamples/IGATest/non-exactBoundary/Example4_1-circle/16-RegularPolygon/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;
    case 14:// Example4_1: the parametric map: Modified 16-RegularPolygon
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/TestErrorOrderExamples/IGATest/non-exactBoundary/Example4_1-circle/modifed16RegularPolygon/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/TestErrorOrderExamples/IGATest/non-exactBoundary/Example4_1-circle/modifed16RegularPolygon/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;
    case 15:// TestErrorOrderExample: Example4_1 Square
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/TestErrorOrderExamples/IGATest/exactBoundary/Example4_1-square/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/TestErrorOrderExamples/IGATest/exactBoundary/Example4_1-square/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 16://The Polygon with N=8
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/NPolygon/n=8/Mesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/NPolygon/n=8/Coeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 17://The Modified Polygon with N=8
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/NPolygon/n=8/Mesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/NPolygon/n=8/ModifiedCoeff/Coeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 18://The Polygon with N=16
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/NPolygon/n=16/Mesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/NPolygon/n=16/Coeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 19://The Modified Polygon with N=16
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/NPolygon/n=16/Mesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/NPolygon/n=16/ModifiedCoeff/Coeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 20://The Polygon with N=32
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/NPolygon/n=32/Mesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/NPolygon/n=32/Coeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 21://The Modified Polygon with N=32
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/NPolygon/n=32/Mesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/NPolygon/n=32/ModifiedCoeff/Coeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 22://The Polygon with N=64
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/NPolygon/n=64/Mesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/NPolygon/n=64/Coeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 23://The Modified Polygon with N=64
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/NPolygon/n=64/Mesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/NPolygon/n=64/ModifiedCoeff/Coeff.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 24://24--> Example 2_2
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example2_2/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/Example2_2/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 25://PDE_PFEM: Example4_1 Square===>the same with case 15
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/TestErrorOrderExamples/IGATest/exactBoundary/Example4_1-square/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/TestErrorOrderExamples/IGATest/exactBoundary/Example4_1-square/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 27://27--> Example 2_2
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example2_2/Improved_Parametrization/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/Example2_2/Improved_Parametrization/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;

    default:
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example4/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/Example4/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
        break;
    }
    //Generate the initial Parametric map
    MSplineFunctions ParametricMap(Coeff_Parametric_Map, *P_ParametricMesh);

//        cout<<"The initial Coeff"<<endl;
//        for(unsigned int i_test=0; i_test<Coeff_Parametric_Map[0].size(); i_test++)
//        {
//            cout<<"("<<Coeff_Parametric_Map[0][i_test]<<", "<<Coeff_Parametric_Map[1][i_test]<<")"<<endl;
//        }


    //Read the set of edges of *P_ParametricMesh
    vector<pair<unsigned int, unsigned int> > InitialEdges;
    vector<unsigned int> EdgesIndexofCells;
    ReadTheEdges(ExampleIndex, InitialEdges, EdgesIndexofCells);

//    cout<<"InitialEdges"<<endl;
//    for(unsigned int i_test=0; i_test<InitialEdges.size(); i_test++)
//    {
//        cout<<"Edge:<"<<InitialEdges[i_test].first<<", "<<InitialEdges[i_test].second<<">"<<endl;
//    }

//    cin.get();
//    cout<<"EdgesIndexofCells:"<<endl;
//    for(unsigned int i_test=0; i_test<EdgesIndexofCells.size(); i_test++)
//    {
//        cout<<"the "<<i_test<<"-th EdgesIndexofCells= "<< EdgesIndexofCells[i_test]<<endl;
//    }
//    cin.get();

        //========================================
        unsigned int k;
        cout<<"Put in the value k for determining the times of subdivision:"<<endl;
        cin>>k;
        //---------------------
        OfTime<<endl;
        OfTime<<"Put in the value k for determining the times of subdivision:"<<endl;
        OfTime<<"k = "<<k<<endl;
        //---------------------
        unsigned int pho;
        cout<<"Put in the density of points between two adjacent vertices"<<endl;
        cin>>pho;
        //---
        OfTime<<"Put in the density of points between two adjacent vertices"<<endl;
        OfTime<<"pho="<<pho<<endl;

        //==========================================
        vector<PointSet> Ps;
        vector<vector<Coordinate *> > Qs;
        unsigned int NumPEdge=(k+1)*pho+(k+2);

        for(unsigned int IndexEdge=0; IndexEdge < InitialEdges.size(); IndexEdge++)
        {
            PointSet Psi;
            vector<Coordinate *> Qsi;
            GetPsQsOverPhysicalDomain(ExampleIndex, IndexEdge, NumPEdge, &ParametricMap,P_ParametricMesh,
                                      InitialEdges, EdgesIndexofCells, Psi, Qsi);

            Ps.push_back(Psi);
            Qs.push_back(Qsi);
        }


        //===========================================
        //The subdivide the initial Mesh
        map<unsigned int, unsigned int> OrigialVertexToCurrentVertex;

        //Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(k, OrigialVertexToCurrentVertex);
         map<unsigned int, pair<unsigned int ,Coordinate*> > CurrentVerticesIndexToCoordinate;

        vector<vector<unsigned int> > CurrentVerticesEdges;
        //CurrentVertices[i] is the set of vertex indices which are on InitialEdges[i]

        cout<<"Test: P_ParametricMesh->Subdivide_initialParametricMesh_Globally_Vertex!"<<endl;
        
        Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally_Vertex(k,InitialEdges,
               EdgesIndexofCells, OrigialVertexToCurrentVertex, CurrentVerticesEdges, CurrentVerticesIndexToCoordinate);

//        cout<<"CurrentVerticesIndexToCoordinate"<<endl;
//        for(map<unsigned int, pair<unsigned int, Coordinate*> >::const_iterator
//            ItMap=CurrentVerticesIndexToCoordinate.begin();
//            ItMap!=CurrentVerticesIndexToCoordinate.end(); ItMap++)
//        {
//            cout<<"IndexVertex = "<<ItMap->first<<endl;
//            cout<<"cellindex="<< (ItMap->second).first<<" ("<<(ItMap->second).second->xy[0]
//               <<", "<<(ItMap->second).second->xy[1]<<") "<<endl;
//        }


        MSplineFunctions * P_RefinedParametricMap;
        vector<vector<double> > CoeffRefinedParametricMap;


        P_RefinedParametricMap=ReExpressionParametrization(k, pho, &ParametricMap,
                               CurrentVerticesIndexToCoordinate, CurrentVerticesEdges, Qs,
                               P_FEMMesh, P_ParametricMesh, CoeffRefinedParametricMap);
       //PrintParametricMap(CoeffRefinedParametricMap, P_FEMMesh);


        vector<PointSet> ModifiedPs;
        vector< vector<Coordinate *> > ModifiedQs;
        cout<<"Test:ModifyRefinedParametrization"<<endl;

        ModifyRefinedParametrization(argc, args, k, pho, CurrentVerticesEdges,
                                     P_FEMMesh, CoeffRefinedParametricMap, Ps, Qs,
                                     ModifiedPs, ModifiedQs, P_RefinedParametricMap);



        //Modify the Coeff of RefinedParameterlization
        cout<<"Test:ParametricMapError"<<endl;
        vector<double> ErrorMap;
        double maxErrorMap;
        ParametricMapError(P_RefinedParametricMap, P_FEMMesh, ModifiedPs,  ModifiedQs,
                           maxErrorMap, ErrorMap);

        cout<<"Test: PrintMapError"<<endl;
        PrintMapError(maxErrorMap, ErrorMap);

        cout<<"Test: PrintParametricMap"<<endl;
        PrintParametricMap(CoeffRefinedParametricMap, P_FEMMesh, "Parameterization_Axel.axl");

        cout<<"The following is the FEM part"<<endl;

        //-------------FEM Solver----------------------

        //=============================================

        FEMPart_RefinedParametricMap(OfTime, P_FEMMesh, P_RefinedParametricMap,
                                     k1, k2, ExampleIndex,k,CoeffRefinedParametricMap);
        /*Finish PETSc program*/
        PetscFinalize();

}
