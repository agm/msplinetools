#ifndef SPECIALPARAMETERIZATION_FUNCTIONS_H
#define SPECIALPARAMETERIZATION_FUNCTIONS_H

#include <math.h>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

#include "msp/Basis.h"
#include "msp/MSplineEvaluation.h"
#include "msp/MSplineFunctionCell.h"
#include "msp/HermiteData.h"
#include "msp/HermiteCell.h"
#include "msp/Functions.h"
#include "msp/BasesVertex.h"
#include "msp/MSplineFunctions.h"

#include "SpecialParameterization_DynamicFunctionValues.h"
#include "msp/FEM_Print.h"



#include <petsc.h>
#include <petscksp.h>

static void PrintAxelMeshFile_FEMSolution_SpecialParameterization(const Vec & m_solut, const Mesh*  P_FEMMesh,
                                                                  unsigned int ExampleIndex, unsigned int k,
                                                                  unsigned int SubExampleIndex, double delta)
{
    unsigned int n_v=1+20/(k+1);
    unsigned int n_u=1+20/(k+1);

    unsigned int nV=0;
    unsigned int nF=0;
    unsigned int nE=0;

    vector<vector<vector<double> > > VerticesForCells;
    //vector<vector<double> > VerticesSeq;
    vector<vector<vector<unsigned int> > > FacesForCells;
    vector<vector<vector<unsigned int> > > EdgesForCells;

    //---------
    vector<double> solution;
    PetscInt SolutSize;
    VecGetSize(m_solut, &SolutSize);
    for(PetscInt Ipetsc = 0; Ipetsc < SolutSize; Ipetsc++)
    {
        PetscScalar v;
        VecGetValues(m_solut,1,&Ipetsc,&v);
        solution.push_back(v);
    }

    MSplineFunction * P_FEMSolution=new MSplineFunction(solution, *P_FEMMesh);//The Solution of FEM

    for (int i=0;i<P_FEMMesh->P_Cells->size();i++)
    {
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];

        double dx=(x1-x0)/(n_u-1.0);
        double dy=(y1-y0)/(n_v-1.0);


        vector<Coordinate*> Pcoordinate_Ps;
        for (int u_index=0; u_index < n_u; u_index++)
        {
            for (int v_index=0; v_index < n_v; v_index++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=u_index*dx+x0;//
                CoorP->xy[1]=v_index*dy+y0;//
                Pcoordinate_Ps.push_back(CoorP);
            }
        }

        vector<double> FEMSolutionVal;

        P_FEMSolution->Evaluation(0,0,Pcoordinate_Ps,FEMSolutionVal,i,*P_FEMMesh);

        //=============================
        //vector<vector<double> > ValuesXYZ;
        //P_MSplineParametricMap->Evaluation(0, 0, Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent, ValuesXYZ, *P_ParametricMesh);
        vector<double> X, Y;
        Special_ParametricMapSigmaVal(0,0,Pcoordinate_Ps,X,Y,ExampleIndex,SubExampleIndex, delta);
        //
        vector<vector<double> > VerticesForCelli;
        for(unsigned int iv=0; iv<X.size(); iv++)
        {
            vector<double> Vi;
            Vi.push_back(X[iv]);
            Vi.push_back(Y[iv]);
            Vi.push_back(FEMSolutionVal[iv]);

            VerticesForCelli.push_back(Vi);
        }
        nV=nV+VerticesForCelli.size();
        VerticesForCells.push_back(VerticesForCelli);

        //====Faces==
        vector<vector<unsigned int> > FacesForCelli;
        unsigned cell_index=i;
        //For Faces(triangules)
        for(int u_index=0; u_index < n_u-1; u_index++)
        {
            for(int v_index=0; v_index < n_v-1; v_index++)
            {
                vector<unsigned int> Fi;
                Fi.push_back(u_index*n_v+v_index+n_u*n_v*cell_index);
                Fi.push_back(u_index*n_v+v_index+1+n_u*n_v*cell_index);
                Fi.push_back((u_index+1)*n_v+v_index+n_u*n_v*cell_index);
                FacesForCelli.push_back(Fi);

                Fi.clear();
                Fi.push_back(u_index*n_v+v_index+1+n_u*n_v*cell_index);
                Fi.push_back((u_index+1)*n_v+v_index+n_u*n_v*cell_index);
                Fi.push_back((u_index+1)*n_v+v_index+1+n_u*n_v*cell_index);
                FacesForCelli.push_back(Fi);
            };
        };
        nF=nF+FacesForCelli.size();
        FacesForCells.push_back(FacesForCelli);
        //Edges
        vector<vector<unsigned int> > EdgesForCelli;
        vector<unsigned int> Ei;
        //For Edges
        for(unsigned int ie=0; ie<n_v-1; ie++)
        {
            Ei.clear();
            Ei.push_back(ie+n_u*n_v*cell_index);
            Ei.push_back(ie+1+n_u*n_v*cell_index);
            EdgesForCelli.push_back(Ei);
        }
        for(int je = 0; je < n_u-1 ; je++)
        {
            Ei.clear();
            Ei.push_back(n_u-1+je*n_v+n_u*n_v*cell_index);
            Ei.push_back(n_u-1+(je+1)*n_v+n_u*n_v*cell_index);
            EdgesForCelli.push_back(Ei);
        }

        for(int ie = 0; ie < n_v-1 ; ie++)
        {
            Ei.clear();
            Ei.push_back((n_u-1)*n_v + (n_v-1-ie)+n_u*n_v*cell_index);
            Ei.push_back((n_u-1)*n_v + (n_v-1-(ie+1))+n_u*n_v*cell_index);
            EdgesForCelli.push_back(Ei);
        }

        for(int je = 0; je < n_u-1 ; je++)
        {
            Ei.clear();
            Ei.push_back((n_u-1-je)*n_v+n_u*n_v*cell_index);
            Ei.push_back((n_u-1-(je+1))*n_v+n_u*n_v*cell_index);
            EdgesForCelli.push_back(Ei);
        }
        nE=nE+EdgesForCelli.size();
        EdgesForCells.push_back(EdgesForCelli);
    }


    //--------------
    ofstream ofValueDistribution;
    ofValueDistribution.open("SolutionOverFanShape_SpecialParameterization_AxelMesh.axl");
    ofValueDistribution<<"<axl>"<<endl;
    ofValueDistribution<<"<mesh>"<<endl;

    ofValueDistribution<<"<count>"<<nV<<" "<<nE<<" "<<nF<<"</count>"<<endl;
    //Points
    ofValueDistribution<<"<points>"<<endl;
    for(unsigned int iv1=0; iv1<VerticesForCells.size(); iv1++)
    {
        for(unsigned int iv2=0; iv2<VerticesForCells[iv1].size(); iv2++)
        {
            //The first Choice
            for(unsigned int iv3=0; iv3<VerticesForCells[iv1][iv2].size(); iv3++)
            {
                ofValueDistribution<<VerticesForCells[iv1][iv2][iv3]<<" ";
            }
            ofValueDistribution<<endl;
        }
    }
    ofValueDistribution<<"</points>"<<endl;

    //===Faces
    ofValueDistribution<<"<faces>"<<endl;
    for(unsigned int iF1=0; iF1<FacesForCells.size(); iF1++)
    {
        for(unsigned int iF2=0; iF2<FacesForCells[iF1].size(); iF2++)
        {

            ofValueDistribution<<3<<" ";
            for(unsigned int iF3=0; iF3<FacesForCells[iF1][iF2].size();iF3++)
            {
                ofValueDistribution<<FacesForCells[iF1][iF2][iF3]<<" ";
            }
            ofValueDistribution<<endl;
            //
        }
    }
    ofValueDistribution<<"</faces>"<<endl;

    ofValueDistribution<<"<edges>"<<endl;
    for(unsigned int iE1=0; iE1<EdgesForCells.size(); iE1++)
    {
        for(unsigned int iE2=0; iE2<EdgesForCells[iE1].size(); iE2++)
        {
            ofValueDistribution<<2<<" ";
            for(unsigned int iE3=0; iE3<EdgesForCells[iE1][iE2].size(); iE3++)
            {
                ofValueDistribution<<EdgesForCells[iE1][iE2][iE3]<<" ";
            }
            ofValueDistribution<<endl;
        }
    }
    ofValueDistribution<<"</edges>"<<endl;

    ofValueDistribution<<"</mesh>"<<endl;
    ofValueDistribution<<"</axl>"<<endl;
    ofValueDistribution.close();

}


////================ParametricMap Part===========================================================
//==================================
void ComputeFunval_SpecialParametricMap(const vector<Coordinate*> &Pcoordinate_Ps,
                                        vector<double> &funval, int flag,
                                        unsigned int ExampleIndex, unsigned int SubExampleIndex,
                                        double delta)
{

    double funcval;
    funval.clear();

    vector<double> X, Y;
    Special_ParametricMapSigmaVal(0,0,Pcoordinate_Ps,X,Y, ExampleIndex, SubExampleIndex, delta);

    //-----------------------------------
    if(flag == 0)//the value of function F
    {
        for (unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
        {
            double x=X[i];
            double y=Y[i];
            funcval=SpecialParameterization_ExactValues(x, y, ExampleIndex,SubExampleIndex);
            funval.push_back(funcval);
        }

    }
    else if(flag == 1)//f=-Delta(F)
    {
        for (unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
        {

            double x=X[i];
            double y=Y[i];

            funcval=SpecialParameterization_RightSideFunctionValue(x, y, ExampleIndex, SubExampleIndex);

            funval.push_back(funcval);
        }
    }
    else if(flag == 2)// Fx
    {
        for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
        {
            double x=X[i];
            double y=Y[i];

            vector<double> fxfy;
            SpecialParameterization_FxFy(fxfy, x, y, ExampleIndex, SubExampleIndex);

            funval.push_back(fxfy[0]);
        }
    }
    else if(flag == 3)// Fy
    {
        for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
        {
            double x=X[i];
            double y=Y[i];
            vector<double> fxfy;
            SpecialParameterization_FxFy(fxfy, x, y, ExampleIndex, SubExampleIndex);

            funval.push_back(fxfy[1]);
        }
    }
    else
    {
        cout<<"SpecialParameterization_Functions.h:ComputeFunval_MSplineParametricMap():: the flag should be 0(F) , 1(-Delta F), 2 (Fx), 3 (Fy)!"<<endl;
        system("PAUSE");
    }
    vector<double>(funval).swap(funval);
}
//==========================

//void ParametricMapSigma_MSplineParametricMap(const vector<Coordinate*> &Pcoordinate_Ps, vector<vector<double> > &m_JPart,
//                                             vector<double> &DetJ, unsigned int Index_ParametricMeshCell,
//                                             const MSplineFunctions &MSplineParametricMap, const Mesh * P_ParametricMesh,
//                                             double k1, double k2, unsigned int ExampleIndex)
//{//dimParametricMap=2
//    //ParametricMapSigma_MSplineParametricMap(coordinate_Ps, m_JPart, detJ , P_FEMMesh->P_Cells[i]->ItsParent, ParametricMap, P_ParametricMesh);
//    m_JPart.clear();
//    DetJ.clear();

//    vector <double> X, Y;
//    MSplineParametricMap.P_MSplineFunctions[0]->Evaluation(0,0,Pcoordinate_Ps, X, Index_ParametricMeshCell, *P_ParametricMesh);
//    MSplineParametricMap.P_MSplineFunctions[1]->Evaluation(0,0,Pcoordinate_Ps, Y, Index_ParametricMeshCell, *P_ParametricMesh);

//    //X(s,t): MSplineParametricMap.P_MSplineFunctions[0]
//    vector<double> dXds, dXdt;

//    MSplineParametricMap.P_MSplineFunctions[0]->Evaluation(1,0,Pcoordinate_Ps, dXds, Index_ParametricMeshCell, *P_ParametricMesh);
//    MSplineParametricMap.P_MSplineFunctions[0]->Evaluation(0,1,Pcoordinate_Ps, dXdt, Index_ParametricMeshCell, *P_ParametricMesh);

//    //Y(s,t): MSplineParametricMap.P_MSplineFunctions[1]
//    vector<double> dYds, dYdt;
//    MSplineParametricMap.P_MSplineFunctions[1]->Evaluation(1,0,Pcoordinate_Ps, dYds, Index_ParametricMeshCell, *P_ParametricMesh);
//    MSplineParametricMap.P_MSplineFunctions[1]->Evaluation(0,1,Pcoordinate_Ps, dYdt, Index_ParametricMeshCell, *P_ParametricMesh);




//    //----det(Jacobian)
//    for (unsigned int ii=0; ii<Pcoordinate_Ps.size(); ii++)
//    {
//        DetJ.push_back(dXds[ii]*dYdt[ii]-dXdt[ii]*dYds[ii]);
//    }

//    vector<double> m_JParti;
//    for (unsigned int ii=0; ii<Pcoordinate_Ps.size(); ii++)
//    {
//        m_JParti.clear();

//        //J^(-1)====================================
//        double Jinv11(0.0);
//        double Jinv12(0.0);
//        double Jinv21(0.0);
//        double Jinv22(0.0);

//        if (abs_double(DetJ[ii])<1.0e-10)
//        {

//            cout<<"CellIndex = "<<Index_ParametricMeshCell<<endl;
//            //P_ParametricMesh->P_Cells->at(Index_ParametricMeshCell)
//            cout<<"MSplineParametricMap["<<Index_ParametricMeshCell<<"]"<<endl;
//            MSplineParametricMap.P_MSplineFunctions[0]->P_MSpline[Index_ParametricMeshCell]->Cout_MSplineFunctionCell();
//            MSplineParametricMap.P_MSplineFunctions[1]->P_MSpline[Index_ParametricMeshCell]->Cout_MSplineFunctionCell();

//            cout<<"dXds[ii]="<<dXds[ii]<<", "<<"dXdt[ii]="<<dXdt[ii]<<endl;
//            cout<<"dYds[ii]="<<dYds[ii]<<", "<<"dYdt[ii]="<<dYdt[ii]<<endl;

//            cout<<"Jocabian Matrix is nearly singular!"<<endl;
//            cin.get();
//        }
//        else
//        {
//            Jinv11=dYdt[ii]/DetJ[ii];
//            Jinv12=-dXdt[ii]/DetJ[ii];
//            Jinv21=-dYds[ii]/DetJ[ii];
//            Jinv22=dXds[ii]/DetJ[ii];
//        }

//        vector<double> Kxy;
//        double x=X[ii];
//        double y=Y[ii];
//        KMatrixValue(x,y,k1,k2,Kxy,ExampleIndex);

//        double a11=Jinv11*(Jinv11*Kxy[0]+Jinv12*Kxy[1])+Jinv12*(Jinv11*Kxy[2]+Jinv12*Kxy[3]);
//        double a12=Jinv21*(Jinv11*Kxy[0]+Jinv12*Kxy[1])+Jinv22*(Jinv11*Kxy[2]+Jinv12*Kxy[3]);
//        double a21=Jinv11*(Jinv21*Kxy[0]+Jinv22*Kxy[1])+Jinv12*(Jinv21*Kxy[2]+Jinv22*Kxy[3]);
//        double a22=Jinv21*(Jinv21*Kxy[0]+Jinv22*Kxy[1])+Jinv22*(Jinv21*Kxy[2]+Jinv22*Kxy[3]);

//        m_JParti.push_back(DetJ[ii]*a11);
//        m_JParti.push_back(DetJ[ii]*a12);
//        m_JParti.push_back(DetJ[ii]*a21);
//        m_JParti.push_back(DetJ[ii]*a22);
//        m_JPart.push_back(m_JParti);
//    }
//}

//====
void Special_ParametricMapSigma(const vector<Coordinate*> &Pcoordinate_Ps, vector<vector<double> > &m_JPart,
                        vector<double> &DetJ, unsigned int ExampleIndex, unsigned SubExampleIndex, double delta)
{
    m_JPart.clear();
    DetJ.clear();

    vector <double> X, Y;
    Special_ParametricMapSigmaVal(0, 0, Pcoordinate_Ps,X,Y, ExampleIndex, SubExampleIndex, delta);

    //X(s,t):
    vector<double> dXds, dXdt;
    //Y(s,t):
    vector<double> dYds, dYdt;
    Special_ParametricMapSigmaVal(1, 0, Pcoordinate_Ps,dXds,dYds, ExampleIndex, SubExampleIndex,delta);
    Special_ParametricMapSigmaVal(0, 1, Pcoordinate_Ps,dXdt,dYdt, ExampleIndex, SubExampleIndex,delta);

    //----det(Jacobian)
    for (unsigned int ii=0; ii<Pcoordinate_Ps.size(); ii++)
    {
        DetJ.push_back(dXds[ii]*dYdt[ii]-dXdt[ii]*dYds[ii]);
    }

    vector<double> m_JParti;
    for (unsigned int ii=0; ii<Pcoordinate_Ps.size(); ii++)
    {
        m_JParti.clear();

        //J^(-1)====================================
        double Jinv11(0.0);
        double Jinv12(0.0);
        double Jinv21(0.0);
        double Jinv22(0.0);

        if (abs_double(DetJ[ii])<1.0e-18)
        {
            cout<<"Jocabian Matrix is nearly singular!"<<endl;
            //cin.get();
            Jinv11=dYdt[ii]/DetJ[ii];
            Jinv12=-dXdt[ii]/DetJ[ii];
            Jinv21=-dYds[ii]/DetJ[ii];
            Jinv22=dXds[ii]/DetJ[ii];
        }
        else
        {
            Jinv11=dYdt[ii]/DetJ[ii];
            Jinv12=-dXdt[ii]/DetJ[ii];
            Jinv21=-dYds[ii]/DetJ[ii];
            Jinv22=dXds[ii]/DetJ[ii];
        }

        vector<double> Kxy;

        Kxy.push_back(1.0);
        Kxy.push_back(0.0);
        Kxy.push_back(0.0);
        Kxy.push_back(1.0);


        double b11=(dYdt[ii]*dYdt[ii]*Kxy[0]-dYdt[ii]*dXdt[ii]*Kxy[1]-dXdt[ii]*dYdt[ii]*Kxy[2]+dXdt[ii]*dXdt[ii]*Kxy[3])/DetJ[ii];
        double b12=(-dYds[ii]*dYdt[ii]*Kxy[0]+dYds[ii]*dXdt[ii]*Kxy[1]+dXds[ii]*dYdt[ii]*Kxy[2]-dXds[ii]*dXdt[ii]*Kxy[3])/DetJ[ii];
        double b21=(-dYds[ii]*dYdt[ii]*Kxy[0]+dYdt[ii]*dXds[ii]*Kxy[1]+dXdt[ii]*dYds[ii]*Kxy[2]-dXds[ii]*dXdt[ii]*Kxy[3])/DetJ[ii];
        double b22=(dYds[ii]*dYds[ii]*Kxy[0]-dYds[ii]*dXds[ii]*Kxy[1]-dXds[ii]*dYds[ii]*Kxy[2]+dXds[ii]*dXds[ii]*Kxy[3])/DetJ[ii];
        m_JParti.push_back(b11);
        m_JParti.push_back(b12);
        m_JParti.push_back(b21);
        m_JParti.push_back(b22);

        m_JPart.push_back(m_JParti);

    }
}




static void SpecialParameterization_JacobianValue(const vector<Coordinate *> & P_Coordinates,
                          vector<double> &Jacobian, unsigned int ExampleIndex, unsigned int SubExampleIndex, double delta)//J(x,y)
{
    //X(s,t):
    vector<double> dXds, dXdt;
    //Y(s,t):
    vector<double> dYds, dYdt;
    Special_ParametricMapSigmaVal(1, 0, P_Coordinates, dXds, dYds, ExampleIndex, SubExampleIndex, delta);
    Special_ParametricMapSigmaVal(0, 1, P_Coordinates, dXdt, dYdt, ExampleIndex, SubExampleIndex, delta);


    Jacobian.clear();
    for(unsigned int i=0; i<P_Coordinates.size(); i++)
    {
        double Jv=dXds[i]*dYdt[i]-dXdt[i]*dYds[i];
        Jacobian.push_back(Jv);
    }

}


#endif
