#ifndef SPECIALPARAMETERIZATION_ERRORFUNCTIONS_H
#define SPECIALPARAMETERIZATION_ERRORFUNCTIONS_H

#include "msp/FEM_Functions.h"
#include "msp/MSplineFunctions.h"


#include "SpecialParameterization_CoeffMatrixLoadVector.h"

//=====================================================
//L2 error on the physical Domain
static void SpecialParameterization_ComputError_MSplineParametricMap_OnPhysicalDomain(const vector<double>& u_gausspt, const vector<double> & v_gausspt,
                                                              double **w, const Vec & m_solut,
                                                       vector<double> &cell_errors, double &totol_Error,
                                                      /*Add*/ vector<double> & cell_Energe_errorsPhysDom, double &total_Energe_ErrorPhysDom,
                                                       const Mesh*  P_FEMMesh, unsigned int ExampleIndex,
                                                        unsigned int SubExampleIndex, double delta)
{

    ofstream ofJacobian;
    ofJacobian.open("Jacobian.txt");
    double MaxMaxJ=-100.0;
    double MinMinJ=100.0;

    unsigned int u_gaussnum=u_gausspt.size();
    unsigned int v_gaussnum=v_gausspt.size();

    vector<double> temp_Error;
    vector<double> exactval;
    /*Add*/vector<double> ExacSou_x, ExacSou_y;
    /*Add*/vector<double> Solu_s, Solu_t;
    vector<double> Jval;

    cell_errors.clear();
    /*Add*/cell_Energe_errorsPhysDom.clear();

    cell_errors.resize(P_FEMMesh->P_Cells->size());
    /*Add*/cell_Energe_errorsPhysDom.resize(P_FEMMesh->P_Cells->size());

    totol_Error=0.0;
    /*Add*/total_Energe_ErrorPhysDom=0.0;

    vector<double> solution;
    PetscInt SolutSize;
    VecGetSize(m_solut, &SolutSize);
    for(PetscInt Ipetsc = 0; Ipetsc < SolutSize; Ipetsc++)
    {
        PetscScalar v;
        VecGetValues(m_solut,1,&Ipetsc,&v);
        solution.push_back(v);
    }

    MSplineFunction * P_FEMSolution=new MSplineFunction(solution, *P_FEMMesh);//The Solution of FEM

    for (int i=0;i<P_FEMMesh->P_Cells->size();i++)
    {
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];

        vector<Coordinate*> Pcoordinate_Ps;
        for (int k=0; k<u_gaussnum; k++)
        {
            for (int l=0; l<v_gaussnum; l++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=0.5*(x1+x0)+0.5*(x1-x0)*u_gausspt[k];//u[k];
                CoorP->xy[1]=0.5*(y1+y0)+0.5*(y1-y0)*v_gausspt[l];//v[l];(y0+y1+(y1-y0)*gausspt[k]) * 0.5;
                Pcoordinate_Ps.push_back(CoorP);
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
       //L2 Error
        ComputeFunval_SpecialParametricMap(Pcoordinate_Ps,  exactval,
                                           0, ExampleIndex, SubExampleIndex, delta);
        //This function should be modified,
        //different SubExampleIndex corresponding to different alpha
        P_FEMSolution->Evaluation(0, 0, Pcoordinate_Ps, temp_Error, i, *P_FEMMesh);

        //Add: Energe Error
        //Fx
        ComputeFunval_SpecialParametricMap(Pcoordinate_Ps,  ExacSou_x,
                                           2, ExampleIndex, SubExampleIndex, delta);
        //Fy
        ComputeFunval_SpecialParametricMap(Pcoordinate_Ps,  ExacSou_y,
                                           3, ExampleIndex, SubExampleIndex, delta);
        //Solu_s
        P_FEMSolution->Evaluation(1, 0, Pcoordinate_Ps, Solu_s, i, *P_FEMMesh);
        //Solu_t
        P_FEMSolution->Evaluation(0, 1, Pcoordinate_Ps, Solu_t, i, *P_FEMMesh);
        //==
        //Jacobian
        SpecialParameterization_JacobianValue(Pcoordinate_Ps,Jval,ExampleIndex, SubExampleIndex, delta);
        //
        vector<double> dXds, dXdt, dYds, dYdt;
        Special_ParametricMapSigmaVal(1, 0, Pcoordinate_Ps, dXds, dYds, ExampleIndex, SubExampleIndex, delta);
        Special_ParametricMapSigmaVal(0, 1, Pcoordinate_Ps, dXdt, dYdt, ExampleIndex, SubExampleIndex, delta);


        cell_errors[i] = 0.0;

        double maxJ=-100.0;
        double minJ=100.0;

        for(int j=0;j<u_gaussnum*v_gaussnum;j++)
        {
            //cout<<"L2 error on the physical Domain::Jacobian["<<j<<"] = "<<Jval[j]<<endl;
            if(Jval[j]>maxJ)
            {
                maxJ=Jval[j];
            }
            if(Jval[j]<minJ)
            {
                minJ=Jval[j];
            }
            if(Jval[j]>MaxMaxJ)
            {
                MaxMaxJ=Jval[j];
            }
            if(Jval[j]<MinMinJ)
            {
                MinMinJ=Jval[j];
            }


            if(Jval[j]<0)
            {
                Jval[j]=-Jval[j];
            }

            int nn = j / v_gaussnum;
            int mm = j % v_gaussnum;
            cell_errors[i] += w[nn][mm]*pow(exactval[j]-temp_Error[j],2)*Jval[j];

            /*Add*/
            //double DuhDx=(Solu_s[j]*dYdt[j]-Solu_t[j]*dYds[j])/Jval[j];
            //double DuhDy=(Solu_t[j]*dXds[j]-Solu_s[j]*dXdt[j])/Jval[j];
            //cell_Energe_errorsPhysDom[i] +=w[nn][mm]*(pow(ExacSou_x[j]-DuhDx,2)+pow(ExacSou_y[j]-DuhDy,2))*Jval[j];
            double duds=ExacSou_x[j]*dXds[j]+ExacSou_y[j]*dYds[j];
            double dudt=ExacSou_x[j]*dXdt[j]+ExacSou_y[j]*dYdt[j];
            double e_cell=(pow((duds-Solu_s[j])*dYdt[j]-(dudt-Solu_t[j])*dYds[j],2.0)+pow(dXds[j]*(dudt-Solu_t[j])-dXdt[j]*(duds-Solu_s[j]),2.0))/Jval[j];
            cell_Energe_errorsPhysDom[i] +=w[nn][mm]*e_cell;


            //cout<<"exact solution="<<exactval[j]<<endl;
            //cout<<"temp_Error[j]="<<temp_Error[j]<<endl;
            //cout<<"Jval[j]="<<Jval[j]<<endl;
        }
        ofJacobian<<"the"<<i<<"-th cell:"<<endl;
        ofJacobian<<"maxJ = "<<maxJ<<endl;
        ofJacobian<<"minJ = "<<minJ<<endl;


        //=======================================================
        totol_Error = totol_Error+cell_errors[i]*(y1-y0)*(x1-x0)/4;
        /*Add*/total_Energe_ErrorPhysDom=total_Energe_ErrorPhysDom+cell_Energe_errorsPhysDom[i]*(y1-y0)*(x1-x0)/4;
        //=======================================================
        cell_errors[i] = sqrt(cell_errors[i] *(y1-y0) * (x1-x0)/4);
        /*Add*/cell_Energe_errorsPhysDom[i]=sqrt(cell_Energe_errorsPhysDom[i]*(y1-y0)*(x1-x0)/4);
        //cout<<"Errors at "<<i<<"-th cell = "<<cell_errors[i]<<endl;
    }
    totol_Error = sqrt(totol_Error);
    /*Add*/total_Energe_ErrorPhysDom=sqrt(total_Energe_ErrorPhysDom);
    ofJacobian<<endl;
    ofJacobian<<endl;

    ofJacobian<<"MaxMaxJ= "<<MaxMaxJ<<endl;
    ofJacobian<<"MinMinJ= "<<MinMinJ<<endl;

    ofJacobian.close();
}

//=====================================================


#endif
