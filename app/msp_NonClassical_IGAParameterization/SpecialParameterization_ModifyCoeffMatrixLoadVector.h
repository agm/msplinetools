#ifndef SPECIALPARAMETERIZATION_MODIFYCOEFFMATRIXLOADVECTOR_H//SpecialParameterization_ModifyCoeffMatrixLoadVector.h
#define SPECIALPARAMETERIZATION_MODIFYCOEFFMATRIXLOADVECTOR_H

#include "time.h"
#include <map>
#include <set>

#include "msp/BasesVertex.h"
#include "msp/Bases.h"
#include "msp/BasesCell.h"
#include "msp/MSplineFunction.h"
#include "msp/PointSet.h"
#include "msp/Coordinate.h"
#include "msp/MSplineFunctions.h"
#include "msp/Mesh.h"
#include "msp/Vertex.h"
#include "msp/Coordinate.h"

#include "SpecialParameterization_DynamicFunctionValues.h"

#include "petsc.h"
#include "petscksp.h"

using namespace std;
/**\file */


static  void SpecialParameterization_Modification_LoadVector_CoeffMatrix(const vector<double> &C, const vector<unsigned int>& BasesType, Mat& coeff_matrix, Vec& load_vect, const map<unsigned int, set<unsigned int> >& HTable)
{
    double t0, t1;
    double ModLoadVecTime=0.0, ModCoeffVecTime=0.0;

    unsigned int matrix_size=BasesType.size();// Should be the same as the size of the vector C

    unsigned int NumZeroBases=0;

    for (unsigned int i=0; i<matrix_size; i++)
    {
        if (BasesType[i]&& C[i]==C[i])
        {
            NumZeroBases=NumZeroBases+1;

            t0=(double)clock();

            //modify load_vect's elements
            PetscInt LoadSize;
            VecGetSize(load_vect, &LoadSize);
            for (PetscInt ii = 0; ii < LoadSize; ii ++)
            {
                PetscScalar v;
                PetscInt i_temp=i;
                MatGetValues(coeff_matrix,1,&ii,1,&i_temp,&v);
                v=-C[i]*v;
                VecSetValues(load_vect, 1, &ii, &v, ADD_VALUES);
            }

            //load_vect[i]=C;
            PetscScalar Cpetsc = C[i];
            PetscInt ipetsc = i;
            VecSetValues(load_vect, 1, &ipetsc, &Cpetsc, INSERT_VALUES);


            VecAssemblyBegin(load_vect);
            VecAssemblyEnd(load_vect);

            t1=(double)clock();
            //cout<<"The time costed by modification the load vector = "<<t1-t0<<endl;
            ModLoadVecTime=ModLoadVecTime+(t1-t0)/CLOCKS_PER_SEC;


            //cout<<"Modify the coeff_matrix:"<<endl;

            t0=(double)clock();
            //Modify Coeff_Matrix==============

            //Based on H_Table,Jp will be decided
            unsigned int IndexV = i/4;//the index of vertex such that the i-th basis is associated with it

            map<unsigned int, set<unsigned int> >::const_iterator Imap=HTable.find(IndexV);

            PetscInt Jpsize = 4*((*Imap).second).size();
            PetscInt Jp[Jpsize];
            PetscScalar Vp[Jpsize];


            set<unsigned int>::iterator itHTable;//= HTable[IndexV].begin();

            for(PetscInt jindex=0; jindex<((*Imap).second).size(); jindex++)
            {//4*jindex, 4*jindex+1, 4*jindex+2, 4*jindex+3
                if(jindex==0)
                {
                    itHTable= ((*Imap).second).begin();
                }
                else
                {
                    itHTable++;
                }

                PetscInt VHTable=*itHTable;
                Jp[4*jindex] = 4*VHTable;
                Jp[4*jindex+1] = 4*VHTable+1;
                Jp[4*jindex+2] = 4*VHTable+2;
                Jp[4*jindex+3] = 4*VHTable+3;
                //-------------
                Vp[4*jindex] = 0.0;
                Vp[4*jindex+1] = 0.0;
                Vp[4*jindex+2] = 0.0;
                Vp[4*jindex+3] = 0.0;
            }

            PetscInt Ip = i;
            MatSetValues(coeff_matrix, 1, &Ip, Jpsize, Jp, Vp, INSERT_VALUES);
            MatSetValues(coeff_matrix, Jpsize, Jp, 1, &Ip, Vp,INSERT_VALUES);

            //coeff_matrix[i][i]=1.0;
            PetscScalar vp=1.0;
            MatSetValues(coeff_matrix, 1, &Ip, 1, &Ip, &vp, INSERT_VALUES);

            MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);
            //==================================

            t1=(double)clock();
            //cout<<"The time costed by modification the coeff_matrix = "<<t1-t0<<endl;
            ModCoeffVecTime=ModCoeffVecTime+(t1-t0)/CLOCKS_PER_SEC;

        }
    }

}


void SpecialParameterization_NonHomoDiricheletBoundaryCondition(vector<double> &C, const vector<unsigned int> &BasesType,
                                        Mesh *& P_FEMMesh, unsigned int ExampleIndex, unsigned int SubExampleIndex, double delta)
{
    //For approximate the boundary, we still use all Hermite basis functions
    C.clear();
    C.resize(BasesType.size(),0.0);


    for(unsigned i_basis=0; i_basis<BasesType.size(); i_basis++)
    {
        if(BasesType[i_basis]==3)
        {
            //[1, 0, 0, 0]
            unsigned int Vindex=i_basis/4;//The index of the vertex
          map<unsigned int, Coordinate>::iterator it=P_FEMMesh->P_Vertices->at(Vindex).ItsCellsToCoordinatesMap.begin();
          vector<Coordinate*> Pcoordinate_Ps;
          Pcoordinate_Ps.push_back(&(*it).second);

          vector<double> X, Y;
          Special_ParametricMapSigmaVal(0,0,Pcoordinate_Ps,X,Y, ExampleIndex, SubExampleIndex, delta);

          double x=X[0];
          double y=Y[0];
          //(x,y)
          C[i_basis]=SpecialParameterization_ExactValues(x,y,ExampleIndex,SubExampleIndex);
        }
        else
        {
            if(BasesType[i_basis]==1)//Tangent directions
            {
                unsigned int Vindex=i_basis/4;
                map<unsigned int, Coordinate>::iterator it=P_FEMMesh->P_Vertices->at(Vindex).ItsCellsToCoordinatesMap.begin();
                vector<Coordinate*> Pcoordinate_Ps;
                Pcoordinate_Ps.push_back(&(*it).second);
                //(x,y)
                vector<double> X, Y;
                Special_ParametricMapSigmaVal(0, 0, Pcoordinate_Ps, X, Y, ExampleIndex, SubExampleIndex, delta);
                double x=X[0];
                double y=Y[0];

                vector<double> fxfy;
                SpecialParameterization_FxFy(fxfy, x, y, ExampleIndex,SubExampleIndex);//fx=fxfy[0], fy=fxfy[1]

                //s-direction
                if(i_basis%4==1)
                {
                    vector<double> dXds, dYds;
                    Special_ParametricMapSigmaVal(1, 0, Pcoordinate_Ps, dXds, dYds, ExampleIndex, SubExampleIndex, delta);

                    C[i_basis]=fxfy[0]*dXds[0]+fxfy[1]*dYds[0];

                }

                //t-direction
                if(i_basis%4==2)
                {
                    vector<double> dXdt, dYdt;
                    Special_ParametricMapSigmaVal(0, 1, Pcoordinate_Ps, dXdt, dYdt, ExampleIndex, SubExampleIndex, delta);

                    C[i_basis]=fxfy[0]*dXdt[0]+fxfy[1]*dYdt[0];
                }
            }
        }
    }
}


//========================================================================
#endif
