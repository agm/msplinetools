// WithNonClassical_IGAParameterization.cpp
//===========================================

//The non-classical IGA parameterization related here is a similar polar-mapping
//the singularity of this type of parameterization is controllable.
//===========================================

#include <iostream>
#include <fstream>
#include <map>
#include <time.h>


#include<stdio.h>
#include<dirent.h>

//PETSc
#include <petsc.h>
#include <petscksp.h>

//Spline
#include "msp/Mesh.h"
#include "msp/BasesVertex.h"
#include "msp/Bases.h"
#include "msp/BasesCell.h"
#include "msp/MSplineFunction.h"
#include "msp/PointSet.h"
#include "msp/Coordinate.h"
#include "msp/MSplineFunctions.h"
#include "msp/Functions.h"

//FEM
#include "SpecialParameterization_ErrorFunctions.h"
#include "SpecialParameterization_Functions.h"
#include "SpecialParameterization_DynamicFunctionValues.h"
#include "SpecialParameterization_CoeffMatrixLoadVector.h"
#include "SpecialParameterization_ModifyCoeffMatrixLoadVector.h"
#include "msp/FEM_Print.h"


using namespace std;


int main(int argc,char **args)
{
    PetscInt m;
    PetscInt n = 4;
    Vec load_vect, m_solut;
    Mat coeff_matrix;// the stiff matrix

    KSP ksp;
    PC pc;

    KSPConvergedReason reason;

    ofstream OfTime;
    OfTime.open("Time.txt");


    //-------------FEM Solver----------------------
    cout<<"Generate Guass points and Weights"<<endl;

    OfTime<<endl;
    OfTime<<"Generate Guass points and Weights"<<endl;

    //Preparation: GuassPoint
    unsigned int Gp=0;
    cout<<"Input the number of Guass points:"<<endl;
    cin>>Gp;

    unsigned int u_gaussnum=Gp, v_gaussnum=Gp;
    //unsigned int load_gaussnum=5;
    unsigned int load_gaussnum=Gp;
    vector<double> gausspt, m_IntePt, m_loadw;
    //Weights/////////
    double **w;
     w=GaussPoints_Weights(u_gaussnum, load_gaussnum, gausspt, m_IntePt, m_loadw);

    //================================================
    //Read the ParametricMap
    //Mesh
    cout<<"Read the mesh, subdivide it and parametric map"<<endl;

    //----------
    OfTime<<endl;
    OfTime<<"Read the mesh, subdivide it and parametric map"<<endl;

    Mesh * P_ParametricMesh=new Mesh;
    //Coeff
    vector<vector<double> > Coeff_Parametric_Map;
    unsigned int ExampleIndex=1;//default

    unsigned int SubExampleIndex=1;//default

    double delta;

    cout<<"1-->fan shape with polar coordinates transformation"<<endl;


    //cout<<"51-->Fan Shape (Unknown solution, SubExampleIndex=1,2,3,4,5)"<<endl;


    cout<<"Put in the ExampleIndex:"<<endl;
    cin>>ExampleIndex;

    cout<<"SubExampleIndex: Put in The index of this example of this experiment with ExampleIndex: "<<endl;
    cin>>SubExampleIndex;

    cout<<"Put in delat for the special parameterization"<<endl;
    cin>>delta;


    OfTime<<"WithNonClassical_IGAParameterization"<<endl;


    OfTime<<"1(ExampleIndex)-->fan shape with polar coordinates transformation"<<endl;

    OfTime<<"The mesh of this example that has been chosen: "<< ExampleIndex<<endl;

    OfTime<<"The SubExampleIndex of this example of this experiment with ExampleIndex: "<<SubExampleIndex<<endl;

    OfTime<<"The power of this specail parameterization: "<<delta<<endl;


    unsigned int dimension=2;
    unsigned int VerNum;


    switch(ExampleIndex)
    {
    case 1://Fan-Shape: the parameter domain is rectangular. parameterization--> polar coordinates transformation
        //alpha=2
        //
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/UpdatedAxel/msplinetools/data/Examples/SingularityTest_fanShape/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/UpdatedAxel/msplinetools/data/Examples/SingularityTest_fanShape/ParametricCoeffs.txt",
                                   dimension, VerNum, Coeff_Parametric_Map);
        break;
    case 2://Fan-Shape: the parameter domain is rectangular. parameterization--> polar coordinates transformation
        //alpha=3/2
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/UpdatedAxel/msplinetools/data/Examples/SingularityTest_fanShape/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/UpdatedAxel/msplinetools/data/Examples/SingularityTest_fanShape/ParametricCoeffs.txt",
                                   dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 3://Homogeous boundary condition
        //Fan-shape:
        //alpha=2: 0->2*pi
        //
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/UpdatedAxel/msplinetools/data/Examples/SingularityTest_fanShape/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/UpdatedAxel/msplinetools/data/Examples/SingularityTest_fanShape/ParametricCoeffs.txt",
                                   dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 4://
        //Fan-shape:
        //alpha=2: 0->2*pi
        //the second example
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/UpdatedAxel/msplinetools/data/Examples/SingularityTest_fanShape/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/UpdatedAxel/msplinetools/data/Examples/SingularityTest_fanShape/ParametricCoeffs.txt",
                                   dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 5://
        //Fan-shape:
        //alpha=3/2: 0->2*pi
        //the second example
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/UpdatedAxel/msplinetools/data/Examples/SingularityTest_fanShape/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/UpdatedAxel/msplinetools/data/Examples/SingularityTest_fanShape/ParametricCoeffs.txt",
                                   dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 6://
        //Fan-shape:
        //alpha=2: 0->2*pi
        //a smooth exact solution
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/UpdatedAxel/msplinetools/data/Examples/SingularityTest_fanShape/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/UpdatedAxel/msplinetools/data/Examples/SingularityTest_fanShape/ParametricCoeffs.txt",
                                   dimension, VerNum, Coeff_Parametric_Map);
        break;

    case 7://
        //Fan-shape:
        //alpha=3/2: 0->2*pi
        //a smooth exact solution
        P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/UpdatedAxel/msplinetools/data/Examples/SingularityTest_fanShape/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/UpdatedAxel/msplinetools/data/Examples/SingularityTest_fanShape/ParametricCoeffs.txt",
                                   dimension, VerNum, Coeff_Parametric_Map);
        break;


    default:
        P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example4/ParametricMesh.txt");
        //P_ParametricMesh->Cout_Mesh_File();
        VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
        Reader_Coeff_ParametricMap("../data/Examples/Example4/ParametricCoeffs.txt",
                                   dimension, VerNum, Coeff_Parametric_Map);
        break;
    }

    /* OutPut the parametricMesh for checking*/
    //P_ParametricMesh->Cout_Mesh_File();

    double InitialCellSize=1.0;
    if(P_ParametricMesh->P_Cells->size())
    {
        InitialCellSize=P_ParametricMesh->P_Cells->at(0).CellSize[1]-P_ParametricMesh->P_Cells->at(0).CellSize[0];
    }


    /*OutPut this Parametric map*/
    //ParametricMap.Cout_MSplineFunctions();
    //========================================
    unsigned int k;
    cout<<"Put in the value k for determining the times of subdivision:"<<endl;
    cin>>k;
    //---------------------
    OfTime<<endl;
    OfTime<<"Put in the value k for determining the times of subdivision:"<<endl;
    OfTime<<"k = "<<k<<endl;
    //---------------------

    //========================================
    //The FEM Mesh
    map<unsigned int, unsigned int> OrigialVertexToCurrentVertex;

    //subdivide the initial parametric mesh
    Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(k, OrigialVertexToCurrentVertex);

    cout<<"The Basis Number = "<<4*(P_FEMMesh->P_Vertices->size())<<endl;

    m = P_FEMMesh->P_Vertices->size();

    //==============================PETSC===================================

    PetscInitialize(&argc, &args, (char*) 0, NULL);
    PetscOptionsGetInt(NULL, "-m", &m, NULL);
    PetscOptionsGetInt(NULL, "-n", &n, NULL);

    cout<<"Initialized coeffMatrix and Generate_coeffMatrix"<<endl;

    OfTime<<endl;
    OfTime<<"Initialized coeffMatrix and Generate_coeffMatrix"<<endl;

    MatCreate(PETSC_COMM_WORLD, &coeff_matrix);
   // PetscInt matrix_size=m*n;
    MatSetSizes(coeff_matrix, PETSC_DECIDE, PETSC_DECIDE, m*n, m*n);
    MatSetFromOptions(coeff_matrix);
    MatMPIAIJSetPreallocation(coeff_matrix, 64, NULL, 64, NULL);
    MatSeqAIJSetPreallocation(coeff_matrix, PETSC_DEFAULT, NULL);
    MatSetUp(coeff_matrix);


    /*Generate coeff_CoeffMatrix*/
    cout<<"Generate_CoeffMatrix_Based on Cells"<<endl;
    SpecialParameterization_Generate_CoeffMatrix_Cell(u_gaussnum, v_gaussnum, coeff_matrix, m_IntePt, m_IntePt, w,
                              P_FEMMesh, ExampleIndex, SubExampleIndex, delta);


    //load_vect
    //vector<double> load_vect;
    cout<<"Initialized the load Vector and Generate the load Vector"<<endl;

    OfTime<<endl;
    OfTime<<"Initialized the load Vector and Generate the load Vector"<<endl;


    Initialized_loadVector(load_vect, P_FEMMesh->P_Vertices->size());
    SpecialParameterization_Generate_LoadVector(load_vect, gausspt, m_loadw, P_FEMMesh,ExampleIndex, SubExampleIndex, delta);
    //

    //Set Bases type
    cout<<"Set Bases type"<<endl;
    vector<unsigned int> BasesType;
    BasesType.resize(4*P_FEMMesh->P_Vertices->size(), 0);
    Set_Bases_type(P_FEMMesh, BasesType);

    cin.get();

    OfTime<<endl;
    OfTime<<"There are "<< 4*P_FEMMesh->P_Vertices->size()<<" bases"<<endl;

    //Modify LoadVector and CoeffMatrix
    /*Need to generate C : for determining the coefficents of the basis functions at the bounadry of the parameter domain */
    vector<double> C;
    SpecialParameterization_NonHomoDiricheletBoundaryCondition(C, BasesType, P_FEMMesh, ExampleIndex, SubExampleIndex, delta);

    if(ExampleIndex==3||ExampleIndex==4||ExampleIndex==5||ExampleIndex==6||ExampleIndex==7)
    {
        for(unsigned int i=0; i<C.size(); i++)
        {
            C[i]=0.0;
        }
    }
     //-----------------------------------------------------------------
     MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
     MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);

     VecAssemblyBegin(load_vect);
     VecAssemblyEnd(load_vect);

     map<unsigned int, set<unsigned int> > HTable;
     Initialized_HTable_withoutHangingVertices_1(P_FEMMesh, HTable);


     cout<<"SpecialParameterization_Modification_LoadVector_CoeffMatrix"<<endl;

     OfTime<<endl;
     OfTime<<"SpecialParameterization_Modification_LoadVector_CoeffMatrix"<<endl;

     SpecialParameterization_Modification_LoadVector_CoeffMatrix(C, BasesType, coeff_matrix, load_vect, HTable);

     VecAssemblyBegin(load_vect);
     VecAssemblyEnd(load_vect);
    //


    MatSetOption(coeff_matrix, MAT_SYMMETRIC, PETSC_TRUE);

    VecCreate(PETSC_COMM_WORLD, &m_solut);
    VecSetSizes(m_solut, PETSC_DECIDE, 4*P_FEMMesh->P_Vertices->size());
    VecSetFromOptions(m_solut);

    VecAssemblyBegin(m_solut);
    VecAssemblyEnd(m_solut);
    MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);

    //Solving the Linear System Part====================
    cout<<"solve the Linear system"<<endl;

    KSPCreate(PETSC_COMM_WORLD, &ksp);
    KSPSetType(ksp, KSPGMRES);
    KSPSetOperators(ksp, coeff_matrix, coeff_matrix);


    cout<<"InitialCellSize = "<<InitialCellSize<<endl;

    /*Set the parameters of KSP*/
    PetscReal rtol=(1.e-4)*pow(InitialCellSize/(1.0*k+1.0),4.0);
    PetscReal stol=(1.e-3)*pow(InitialCellSize/(1.0*k+1.0),4.0);
//        PetscReal rtol=1.0e-18;
//        PetscReal stol=1.0e-18;

    KSPSetTolerances(ksp, rtol, stol, PETSC_DEFAULT, PETSC_DEFAULT);

    KSPGetPC(ksp, &pc);
    PCSetType(pc, PCGAMG);
    KSPSetFromOptions(ksp);

    KSPSolve(ksp, load_vect, m_solut);

    KSPGetConvergedReason(ksp,&reason);
    //printf
    PetscInt its;
    KSPGetIterationNumber(ksp, &its);
    PetscPrintf(PETSC_COMM_WORLD, "The number of iteration %D\n", its);
    cout<<"KSP ConvergedReason= "<<reason<<endl;

    OfTime<<endl;
    OfTime<<"k = "<<k<<endl;
    OfTime<<"where, rtol=(1.e-4)*pow(InitialCellSize/(k+1),4.0)="<<rtol<<" and  stol=(1.e-3)*pow(InitialCellSize/(k+1),4.0)= "<<stol<<endl;

    OfTime<<"KSP ConvergedReason= "<<reason<<endl;
    OfTime<<"the number of iteration = "<<its<<endl;
    OfTime<<endl;
    //-------
    OfTime.close();


    //Print load_vect
    PrintLoadVector(load_vect);

    PrintSolution(m_solut);

    PrintAxelMeshFile_FEMSolution_SpecialParameterization(m_solut,P_FEMMesh, ExampleIndex, k, SubExampleIndex, delta);

    //=========Detroy
    VecDestroy(&load_vect);
    MatDestroy(&coeff_matrix);
    KSPDestroy(&ksp);

    //Error========================================================================
    //----
    //Error with L2 norm
    vector<double> cell_errorsPhysDom;
    double total_ErrorPhysDom;
    //Error with Energe norm
    vector<double> cell_Energe_errorsPhysDom;
    double total_Energe_ErrorPhysDom;

    SpecialParameterization_ComputError_MSplineParametricMap_OnPhysicalDomain(gausspt, gausspt, w, m_solut,
                                                      cell_errorsPhysDom, total_ErrorPhysDom,cell_Energe_errorsPhysDom,
                                                       total_Energe_ErrorPhysDom,P_FEMMesh, ExampleIndex, SubExampleIndex, delta);


    ofstream ofErrors;
    ofErrors.open("L2ErrorsOverCells.txt");
    for (unsigned int i_error=0; i_error<cell_errorsPhysDom.size(); i_error++)
    {
        ofErrors<<cell_errorsPhysDom[i_error]<<" "<<endl;
    }
    ofErrors<<endl;
    ofErrors.close();

    ofErrors.open("L2TotalError.txt");
    ofErrors<<total_ErrorPhysDom<<" "<<endl;
    ofErrors<<endl;
    ofErrors.close();

    ofErrors.open("Energe_ErrorsOverCells.txt");
    for (unsigned int i_error=0; i_error<cell_Energe_errorsPhysDom.size(); i_error++)
    {
        ofErrors<<cell_Energe_errorsPhysDom[i_error]<<" "<<endl;
    }
    ofErrors<<endl;
    ofErrors.close();

    ofErrors.open("Energe_TotalError.txt");
    ofErrors<<total_Energe_ErrorPhysDom<<" "<<endl;
    ofErrors<<endl;
    ofErrors.close();

    VecDestroy(&m_solut);

    PetscFinalize();
}

