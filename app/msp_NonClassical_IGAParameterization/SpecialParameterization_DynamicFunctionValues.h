#ifndef SPECIALPARAMETERIZATION_DYNAMICFUNCTIONVALUES_H//SpecialParameterization_DynamicFunctionValues.h
#define SPECIALPARAMETERIZATION_DYNAMICFUNCTIONVALUES_H


#include <vector>

using namespace std;

static double SpecialParameterization_ExactValues(double x,double y, unsigned int ExampleIndex, unsigned int SubExampleIndex)
{
    double exactVal;
    switch (ExampleIndex)
    {
    case 1://ExampleIndex=1, SubExampleIndex=2
        switch (SubExampleIndex)
        {
        case 1:
            exactVal=pow(x*x+y*y,0.25);
            break;
        case 2:
            exactVal=pow(x*x+y*y,0.25);
            break;

        default:
            break;
        }
        break;

    case 2:
        switch(SubExampleIndex)
        {
        case 1:
            exactVal=pow(x*x+y*y,1.0/3.0);
            break;
        case 2:
            exactVal=pow(x*x+y*y,1.0/3.0);
            break;
        }
        break;

    case 3://delta=4.1
    {
        double r=sqrt(x*x+y*y);//
        double sin_tpi;
        if(r==0)
        {
            sin_tpi=0.0;
        }
        else//r!=0
        {
            sin_tpi=y/r;
        }
        double pi=3.141592653589793238;
        exactVal=pow(r,0.5)*cos(r*pi/pow(2.0,4.1+1.0))*sin_tpi;

    }
        break;

    case 4://delta=4.1
    {
        double r=sqrt(x*x+y*y);
        double sint;
        if(r==0)
        {
            sint=0.0;
        }
        else
        {
            sint=y/r;
        }
        exactVal=pow(r,0.5)*(1.0-r)*sint;
    }
        break;

    case 5://delta=4.1
    {
        double r=sqrt(x*x+y*y);
        double sint,cost,sin2t;
        if(r)
        {
            sint=y/r;
            cost=x/r;
        }
        else
        {
            sint=0;
            cost=0;
        }
        sin2t=2*sint*cost;

        exactVal=pow(r,2.0/3.0)*(1.0-r)*sin2t;
    }
        break;

    case 6:
    {
        double pi=3.141592653589793238;
        exactVal=y*cos((x*x+y*y)*pi/2);// a smooth exact solution
    }
        break;
    case 7:
    {
        double pi=3.141592653589793238;
        exactVal=x*y*cos((x*x+y*y)*pi/2);//a smooth exact solution
    }
        break;

    default:
        exactVal=0.0;
        break;
    }


    return exactVal;
}
//-----------
static void SpecialParameterization_FxFy(vector<double> &fxfy, double x, double y, unsigned int ExampleIndex, unsigned int SubExampleIndex)
{
    fxfy.clear();
    fxfy.resize(2,0.0);

    switch (ExampleIndex)
    {
    case 1:
        switch (SubExampleIndex)
        {
        case 1:
            fxfy[0]=0.5*pow(x*x+y*y,-3.0/4.0)*x; //Fx
            fxfy[1]=0.5*pow(x*x+y*y,-3.0/4.0)*y; //Fy
            break;
        case 2:
            fxfy[0]=0.5*pow(x*x+y*y,-3.0/4.0)*x; //Fx
            fxfy[1]=0.5*pow(x*x+y*y,-3.0/4.0)*y; //Fy
            break;
        default:
            break;            
        }
        break;
    case 2:
        switch (SubExampleIndex)
        {
        case 1:
            fxfy[0]=(2.0/3.0)*pow(x*x+y*y,-2.0/3.0)*x;
            fxfy[1]=(2.0/3.0)*pow(x*x+y*y,-2.0/3.0)*y;
            break;
        case 2:
            fxfy[0]=(2.0/3.0)*pow(x*x+y*y,-2.0/3.0)*x;
            fxfy[1]=(2.0/3.0)*pow(x*x+y*y,-2.0/3.0)*y;
            break;
        default:
            break;
        }
        break;

    case 3:
    {
        double r=sqrt(x*x+y*y);//
        double sin_tpi, cos_tpi;
        if(r==0)
        {
            sin_tpi=0.0;
            cos_tpi=0.0;
            //cout<<"x="<<x<<endl;
            //cout<<"y="<<y<<endl;
        }
        else//r!=0
        {
            sin_tpi=y/r;
            cos_tpi=x/r;
        }
        double pi=3.141592653589793238;
        fxfy[0]=0.5*pow(r,-1.5)*x*cos(r*pi/pow(2.0,4.1+1.0))*sin_tpi;
        fxfy[0]=fxfy[0]-pow(r,-0.5)*sin(r*pi/pow(2.0,4.1+1.0))*sin_tpi*x*pi/pow(2.0,4.1+1.0);
        fxfy[0]=fxfy[0]+(pow(r,0.5)*cos(r*pi/pow(2.0,4.1+1.0))*pi*cos_tpi)*(-y*cos_tpi*cos_tpi/(pi*x*x));

        fxfy[1]=(0.5*pow(r,-0.5)*cos(r*pi/pow(2.0,4.1+1.0))*sin_tpi-pow(r,0.5)*sin(r*pi/pow(2.0,4.1+1.0))*sin_tpi*pi/pow(2.0,4.1+1.0))*(y/r);
        fxfy[1]=fxfy[1]+(pow(r,0.5)*cos(r*pi/pow(2.0,4.1+1.0))*pi*cos_tpi)*(cos_tpi*cos_tpi/(x*pi));

        //cout<<"fxfy="<<fxfy[0]<<", "<<fxfy[1]<<endl;

    }
        break;

    case 4:
    {
        double r=sqrt(x*x+y*y);
        double sint, cost;
        if(r)
        {
            sint=y/r;
            cost=x/r;
        }
        else
        {
            sint=0.0;
            cost=0.0;
        }

        fxfy[0]=-0.5*(pow(r,-0.5)+pow(r,0.5))*sint*cost;

        fxfy[1]=0.5*pow(r,-0.5)*(1.0+cost*cost)-0.5*pow(r,0.5)*(2.0+sint*sint);

    }
        break;

    case 5:
    {
        double r=sqrt(x*x+y*y);
        double sint, cost, sin2t, cos2t;
        if(r)
        {
            sint=y/r;
            cost=x/r;
        }
        else
        {
            sint=0.0;
            cost=0.0;
        }
        sin2t=2*sint*cost;
        cos2t=2*cost*cost-1;

        fxfy[0]=(2.0/3.0)*pow(r,-1.0/3.0)*(1.0-r)*sin2t*cost-pow(r,2.0/3.0)*sin2t*cost-2.0*pow(r,-1.0/3.0)*(1.0-r)*sint*cos2t;

        fxfy[1]=(2.0/3.0)*pow(r,-1.0/3.0)*(1.0-r)*sin2t*sint-pow(r,2.0/3.0)*sin2t*sint+2.0*pow(r,-1.0/3.0)*(1.0-r)*cost*cos2t;

    }
        break;

    case 6://smooth exact solution alpha=2
    {
        double pi=3.141592653589793238;
        fxfy[0]=-pi*x*y*sin((pi*(x*x + y*y))/2.0);
        fxfy[1]=cos((pi*(x*x + y*y))/2.0) - pi*y*y*sin((pi*(x*x + y*y))/2.0);
    }
        break;

    case 7://smooth exact solution alpha=3/2
    {
        double pi=3.141592653589793238;
        fxfy[0]=y*cos((pi*(pow(x,2.0) + pow(y,2.0)))/2.0) - pi*x*x*y*sin((pi*(x*x + y*y))/2.0);
        fxfy[1]=x*cos((pi*(x*x + y*y))/2.0) - pi*x*y*y*sin((pi*(x*x + y*y))/2.0);
    }
        break;

    default:
        fxfy[0]=0.0;
        fxfy[1]=0.0;
        break;
    }
}


static double SpecialParameterization_RightSideFunctionValue(double x, double y, unsigned int ExampleIndex, unsigned int SubExampleIndex)
{
    double d_temp;

    switch(ExampleIndex)//delta
    {
    case 1:
        switch (SubExampleIndex)
        {
        case 1://ExampleIndex=1; SubExampleIndex=1
        d_temp=-0.25*pow(x*x+y*y,-3.0/4.0);
        break;
        case 2://ExampleIndex=1; SubExampleIndex=2
        d_temp=-0.25*pow(x*x+y*y,-3.0/4.0);
        break;
        //-----------
        default:
            break;
        }
        break;

    case 2:
        switch (SubExampleIndex)
        {
        case 1:
            d_temp=-(4.0/9.0)*pow(x*x+y*y,-2.0/3.0);
            break;
        case 2:
            d_temp=-(4.0/9.0)*pow(x*x+y*y,-2.0/3.0);
            break;
        default:
            break;
        }
        break;

    case 3:
    {
        double r=sqrt(x*x+y*y);//
        double sin_tpi;
        if(r==0)
        {
            sin_tpi=0.0;
        }
        else//r!=0
        {
            sin_tpi=y/r;
        }
        double pi=3.141592653589793238;
        //d^2u/dr^2
        d_temp=-0.25*pow(r,-1.5)*cos(r*pi/pow(2.0,4.1+1.0))*sin_tpi;
        d_temp=d_temp-pow(r,-0.5)*sin(r*pi/pow(2.0,4.1+1.0))*sin_tpi*(pi/pow(2.0,4.1+1.0));
        d_temp=d_temp-pow(r,0.5)*cos(r*pi/pow(2.0,4.1+1.0))*sin_tpi*pow(pi/pow(2.0,4.1+1.0),2.0);
        //1/r*du/dr
        d_temp=d_temp+(1.0/r)*(0.5*pow(r,-0.5)*cos(r*pi/pow(2.0,4.1+1.0))*sin_tpi-pow(r,0.5)*sin(r*pi/pow(2.0,4.1+1.0))*sin_tpi*pi/pow(2.0,4.1+1.0));
        //1/r^2*d^2u/d^2theta
        d_temp=d_temp+(1.0/pow(r,2.0))*(-pow(r,0.5)*cos(r*pi/pow(2.0,4.1+1.0))*sin_tpi);

        //d_temp=-d_temp
       d_temp=-d_temp;
    }
        break;

    case 4:
    {
        double r=sqrt(x*x+y*y);
        double sint;
        if(r)
        {
            sint=y/r;
        }
        else
        {
            sint=0.0;
        }
        d_temp=pow(r,-1.5)*(0.75+1.25*r)*sint;
    }
        break;

    case 5:
       {
        double r=sqrt(x*x+y*y);
        double sint, cost;
        if(r)
        {
            sint=y/r;
            cost=x/r;
        }
        else
        {
            sint=0.0;
            cost=0.0;
        }
        d_temp=((32.0/9.0)*pow(r,-4.0/3.0)-(11.0/9.0)*pow(r,-1.0/3.0))*2.0*sint*cost;
    }
        break;

    case 6:
    {
        double pi=3.141592653589793238;
        d_temp=pi*pi*pow(y,3.0)*cos((pi*(x*x + y*y))/2.0) + 4*pi*y*sin((pi*(x*x + y*y))/2.0) + pi*pi*x*x*y*cos((pi*(x*x + y*y))/2.0);
    }
        break;

    case 7:
    {
        double pi=3.141592653589793238;
        d_temp=pow(pi,2.0)*x*pow(y,3.0)*cos((pi*(x*x + y*y))/2.0) + pow(pi,2.0)*pow(x,3.0)*y*cos((pi*(x*x + y*y))/2.0) + 6.0*pi*x*y*sin((pi*(x*x + y*y))/2.0);
    }
        break;

    default:
        d_temp=0.0;
        break;
    }

    return d_temp;
//==================================
}



void Special_ParametricMapSigmaVal(unsigned int s_der_order,unsigned int t_der_order, const vector<Coordinate*> &Pcoordinate_Ps,
                           vector<double> &X,vector<double> &Y, unsigned int ExampleIndex, unsigned int SubExampleIndex, double delta)
{
    X.clear();
    Y.clear();
    //
    if(s_der_order==0 && t_der_order==0)
    {
        switch(ExampleIndex)
        {
        case 1://alpha=2.0;
            switch (SubExampleIndex)
            {
            case 1://r=1
                for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                {
                    //
                    double s=Pcoordinate_Ps[i]->xy[0];
                    double t=Pcoordinate_Ps[i]->xy[1];

                    double pi_double=3.141592653589793238;
                    double alpha=2.0;
                    //double delta=2.1;
                    //--------------------------
                    double Vs=pow(s/2.0,delta)*cos(alpha*pi_double*t/2.0);
                    double Vt=pow(s/2.0,delta)*sin(alpha*pi_double*t/2.0);
                    //--------------------------

                    X.push_back(Vs);
                    Y.push_back(Vt);
                }
                break;
            case 2: //r=2^{delta}
                for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                {
                    //
                    double s=Pcoordinate_Ps[i]->xy[0];
                    double t=Pcoordinate_Ps[i]->xy[1];

                    double pi_double=3.141592653589793238;
                    double alpha=2.0;
                    //double delta=4.1;
                    //--------------------------
                    double Vs=pow(s,delta)*cos(alpha*pi_double*t/2.0);
                    double Vt=pow(s,delta)*sin(alpha*pi_double*t/2.0);
                    //--------------------------
                    X.push_back(Vs);
                    Y.push_back(Vt);
                }
                break;
            }
            break;
//--------------------------------------------------------------------------------
        case 2://alpha=3/2
            switch (SubExampleIndex)
            {
            case 1://r=1
                for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                {
                    //
                    double s=Pcoordinate_Ps[i]->xy[0];
                    double t=Pcoordinate_Ps[i]->xy[1];

                    double pi_double=3.141592653589793238;
                    double alpha=3.0/2.0;
                    //double delta=2.1;
                    //--------------------------
                    double Vs=pow(s/2.0,delta)*cos(alpha*pi_double*t/2.0);
                    double Vt=pow(s/2.0,delta)*sin(alpha*pi_double*t/2.0);
                    //--------------------------

                    X.push_back(Vs);
                    Y.push_back(Vt);
                }
                break;
            case 2: //r=2^{delta}
                for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                {
                    //
                    double s=Pcoordinate_Ps[i]->xy[0];
                    double t=Pcoordinate_Ps[i]->xy[1];

                    double pi_double=3.141592653589793238;
                    double alpha=3.0/2.0;
                    //double delta=4.1;
                    //--------------------------
                    double Vs=pow(s,delta)*cos(alpha*pi_double*t/2.0);
                    double Vt=pow(s,delta)*sin(alpha*pi_double*t/2.0);
                    //--------------------------
                    X.push_back(Vs);
                    Y.push_back(Vt);
                }
                break;
            }
            break;
//--------------------------------------------------------------------------------
        case 3://alpha=2.0;
            switch (SubExampleIndex)
            {
            case 1://r=1
                for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                {
                    //
                    double s=Pcoordinate_Ps[i]->xy[0];
                    double t=Pcoordinate_Ps[i]->xy[1];

                    double pi_double=3.141592653589793238;
                    double alpha=2.0;
                    //double delta=2.1;
                    //--------------------------
                    double Vs=pow(s/2.0,delta)*cos(alpha*pi_double*t/2.0);
                    double Vt=pow(s/2.0,delta)*sin(alpha*pi_double*t/2.0);
                    //--------------------------

                    X.push_back(Vs);
                    Y.push_back(Vt);
                }
                break;
            case 2: //r=2^{delta}
                for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                {
                    //
                    double s=Pcoordinate_Ps[i]->xy[0];
                    double t=Pcoordinate_Ps[i]->xy[1];

                    double pi_double=3.141592653589793238;
                    double alpha=2.0;
                    //double delta=4.1;
                    //--------------------------
                    double Vs=pow(s,delta)*cos(alpha*pi_double*t/2.0);
                    double Vt=pow(s,delta)*sin(alpha*pi_double*t/2.0);
                    //--------------------------
                    X.push_back(Vs);
                    Y.push_back(Vt);
                }
                break;
            }
            break;
//--------------------------------------------------------------------------------
        case 4://alpha=2.0; r=1
            for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
            {
                //
                double s=Pcoordinate_Ps[i]->xy[0];
                double t=Pcoordinate_Ps[i]->xy[1];

                double pi_double=3.141592653589793238;
                double alpha=2.0;
                //double delta=2.1;
                //--------------------------
                double Vs=pow(s/2.0,delta)*cos(alpha*pi_double*t/2.0);
                double Vt=pow(s/2.0,delta)*sin(alpha*pi_double*t/2.0);
                //--------------------------

                X.push_back(Vs);
                Y.push_back(Vt);
            }
            break;
//--------------------------------------------------------------------------------//--------------------------------------------------------------------------------
        case 5://alpha=3/2//r=1
            for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
            {
                //
                double s=Pcoordinate_Ps[i]->xy[0];
                double t=Pcoordinate_Ps[i]->xy[1];

                double pi_double=3.141592653589793238;
                double alpha=3.0/2.0;
                //double delta=2.1;
                //--------------------------
                double Vs=pow(s/2.0,delta)*cos(alpha*pi_double*t/2.0);
                double Vt=pow(s/2.0,delta)*sin(alpha*pi_double*t/2.0);
                //--------------------------

                X.push_back(Vs);
                Y.push_back(Vt);
            }
            break;
//-----------------------------------------------

//--------------------------------------------------------------------------------
        case 6://alpha=2.0; r=1; smooth exact solutions
            for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
            {
                //
                double s=Pcoordinate_Ps[i]->xy[0];
                double t=Pcoordinate_Ps[i]->xy[1];

                double pi_double=3.141592653589793238;
                double alpha=2.0;
                //double delta=2.1;
                //--------------------------
                double Vs=pow(s/2.0,delta)*cos(alpha*pi_double*t/2.0);
                double Vt=pow(s/2.0,delta)*sin(alpha*pi_double*t/2.0);
                //--------------------------

                X.push_back(Vs);
                Y.push_back(Vt);
            }
            break;
//--------------------------------------------------------------------------------//--------------------------------------------------------------------------------
        case 7://alpha=3/2//r=1// smooth exact solutions
            for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
            {
                //
                double s=Pcoordinate_Ps[i]->xy[0];
                double t=Pcoordinate_Ps[i]->xy[1];

                double pi_double=3.141592653589793238;
                double alpha=3.0/2.0;
                //double delta=2.1;
                //--------------------------
                double Vs=pow(s/2.0,delta)*cos(alpha*pi_double*t/2.0);
                double Vt=pow(s/2.0,delta)*sin(alpha*pi_double*t/2.0);
                //--------------------------

                X.push_back(Vs);
                Y.push_back(Vt);
            }
            break;
        }
    }
    else
    {
        if(s_der_order==1 && t_der_order==0)
        {
            switch(ExampleIndex)
            {
            case 1://alpha=2.0
                switch (SubExampleIndex)
                {
                case 1://The unit circle  r=1
                    for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                    {
                        //
                        double s=Pcoordinate_Ps[i]->xy[0];
                        double t=Pcoordinate_Ps[i]->xy[1];

                        double pi_double=3.141592653589793238;
                        double alpha=2.0;
                        //double delta=2.1;
                        //--------------------------
                        double Vs=0.5*delta*pow(s/2.0,delta-1.0)*cos(alpha*pi_double*t/2.0);
                        double Vt=0.5*delta*pow(s/2.0,delta-1.0)*sin(alpha*pi_double*t/2.0);
                        //--------------------------

                        X.push_back(Vs);
                        Y.push_back(Vt);
                    }
                    break;

                case 2:// r=2^delta
                    for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                    {
                        //
                        double s=Pcoordinate_Ps[i]->xy[0];
                        double t=Pcoordinate_Ps[i]->xy[1];

                        double pi_double=3.141592653589793238;
                        double alpha=2.0;
                        //double delta=4.1;
                        //--------------------------
                        double Vs=delta*pow(s,delta-1.0)*cos(alpha*pi_double*t/2.0);
                        double Vt=delta*pow(s,delta-1.0)*sin(alpha*pi_double*t/2.0);
                        //--------------------------

                        X.push_back(Vs);
                        Y.push_back(Vt);
                    }
                    break;
                }
                break;
//------------------------------------------------------------------------------------
            case 2://alpha=3/2
                switch (SubExampleIndex)
                {
                case 1://The unit circle  r=1
                    for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                    {
                        //
                        double s=Pcoordinate_Ps[i]->xy[0];
                        double t=Pcoordinate_Ps[i]->xy[1];

                        double pi_double=3.141592653589793238;
                        double alpha=3.0/2.0;
                        //double delta=2.1;
                        //--------------------------
                        double Vs=0.5*delta*pow(s/2.0,delta-1.0)*cos(alpha*pi_double*t/2.0);
                        double Vt=0.5*delta*pow(s/2.0,delta-1.0)*sin(alpha*pi_double*t/2.0);
                        //--------------------------

                        X.push_back(Vs);
                        Y.push_back(Vt);
                    }
                    break;

                case 2:// r=2^delta
                    for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                    {
                        //
                        double s=Pcoordinate_Ps[i]->xy[0];
                        double t=Pcoordinate_Ps[i]->xy[1];

                        double pi_double=3.141592653589793238;
                        double alpha=3.0/2.0;
                        //double delta=4.1;
                        //--------------------------
                        double Vs=delta*pow(s,delta-1.0)*cos(alpha*pi_double*t/2.0);
                        double Vt=delta*pow(s,delta-1.0)*sin(alpha*pi_double*t/2.0);
                        //--------------------------

                        X.push_back(Vs);
                        Y.push_back(Vt);
                    }
                    break;
                }

                break;
//------------------------------------------------------------------------------------
            case 3://alpha=2.0
                switch (SubExampleIndex)
                {
                case 1://The unit circle  r=1
                    for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                    {
                        //
                        double s=Pcoordinate_Ps[i]->xy[0];
                        double t=Pcoordinate_Ps[i]->xy[1];

                        double pi_double=3.141592653589793238;
                        double alpha=2.0;
                        //double delta=2.1;
                        //--------------------------
                        double Vs=0.5*delta*pow(s/2.0,delta-1.0)*cos(alpha*pi_double*t/2.0);
                        double Vt=0.5*delta*pow(s/2.0,delta-1.0)*sin(alpha*pi_double*t/2.0);
                        //--------------------------

                        X.push_back(Vs);
                        Y.push_back(Vt);
                    }
                    break;

                case 2:// r=2^delta
                    for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                    {
                        //
                        double s=Pcoordinate_Ps[i]->xy[0];
                        double t=Pcoordinate_Ps[i]->xy[1];

                        double pi_double=3.141592653589793238;
                        double alpha=2.0;
                        //double delta=4.1;
                        //--------------------------
                        double Vs=delta*pow(s,delta-1.0)*cos(alpha*pi_double*t/2.0);
                        double Vt=delta*pow(s,delta-1.0)*sin(alpha*pi_double*t/2.0);
                        //--------------------------

                        X.push_back(Vs);
                        Y.push_back(Vt);
                    }
                    break;
                }
                 break;
//------------------------------------------------------------------------------------

            case 4://alpha=2.0, r=1
                for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                {
                    //
                    double s=Pcoordinate_Ps[i]->xy[0];
                    double t=Pcoordinate_Ps[i]->xy[1];

                    double pi_double=3.141592653589793238;
                    double alpha=2.0;
                    //double delta=2.1;
                    //--------------------------
                    double Vs=0.5*delta*pow(s/2.0,delta-1.0)*cos(alpha*pi_double*t/2.0);
                    double Vt=0.5*delta*pow(s/2.0,delta-1.0)*sin(alpha*pi_double*t/2.0);
                    //--------------------------

                    X.push_back(Vs);
                    Y.push_back(Vt);
                }
                 break;
//------------------------------------------------------------------------------------
            case 5:
                for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                {
                    //
                    double s=Pcoordinate_Ps[i]->xy[0];
                    double t=Pcoordinate_Ps[i]->xy[1];

                    double pi_double=3.141592653589793238;
                    double alpha=3.0/2.0;
                    //double delta=2.1;
                    //--------------------------
                    double Vs=0.5*delta*pow(s/2.0,delta-1.0)*cos(alpha*pi_double*t/2.0);
                    double Vt=0.5*delta*pow(s/2.0,delta-1.0)*sin(alpha*pi_double*t/2.0);
                    //--------------------------

                    X.push_back(Vs);
                    Y.push_back(Vt);
                }
                break;
//-------------------------------------------------------------------------------------
            case 6://alpha=2.0, r=1
                for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                {
                    //
                    double s=Pcoordinate_Ps[i]->xy[0];
                    double t=Pcoordinate_Ps[i]->xy[1];

                    double pi_double=3.141592653589793238;
                    double alpha=2.0;
                    //double delta=2.1;
                    //--------------------------
                    double Vs=0.5*delta*pow(s/2.0,delta-1.0)*cos(alpha*pi_double*t/2.0);
                    double Vt=0.5*delta*pow(s/2.0,delta-1.0)*sin(alpha*pi_double*t/2.0);
                    //--------------------------

                    X.push_back(Vs);
                    Y.push_back(Vt);
                }
                 break;
//------------------------------------------------------------------------------------
            case 7:
                for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                {
                    //
                    double s=Pcoordinate_Ps[i]->xy[0];
                    double t=Pcoordinate_Ps[i]->xy[1];

                    double pi_double=3.141592653589793238;
                    double alpha=3.0/2.0;
                    //double delta=2.1;
                    //--------------------------
                    double Vs=0.5*delta*pow(s/2.0,delta-1.0)*cos(alpha*pi_double*t/2.0);
                    double Vt=0.5*delta*pow(s/2.0,delta-1.0)*sin(alpha*pi_double*t/2.0);
                    //--------------------------

                    X.push_back(Vs);
                    Y.push_back(Vt);
                }
                break;


            }
        }
        else
        {
            if(s_der_order==0 && t_der_order==1)
            {

                switch(ExampleIndex)
                {
                case 1://alpha=2.0
                    switch (SubExampleIndex)
                    {
                    case 1:// r=1
                        for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                        {
                            //
                            double s=Pcoordinate_Ps[i]->xy[0];
                            double t=Pcoordinate_Ps[i]->xy[1];

                            double pi_double=3.141592653589793238;
                            double alpha=2.0;
                            //double delta=2.1;
                            //--------------------------
                            double Vs=-pow(s/2.0,delta)*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            double Vt=pow(s/2.0,delta)*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            //--------------------------

                            X.push_back(Vs);
                            Y.push_back(Vt);
                        }
                        break;
                    case 2:// r=2^delta
                        for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                        {
                            //
                            double s=Pcoordinate_Ps[i]->xy[0];
                            double t=Pcoordinate_Ps[i]->xy[1];

                            double pi_double=3.141592653589793238;
                            double alpha=2.0;
                            //double delta=4.1;
                            //--------------------------
                            double Vs=-pow(s,delta)*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            double Vt=pow(s,delta)*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            //--------------------------

                            X.push_back(Vs);
                            Y.push_back(Vt);
                        }
                        break;
                    }
                    break;
//---------------------------------------------------------------------------------------------------
                case 2://alpha=3/2
                    switch (SubExampleIndex)
                    {
                    case 1:// r=1
                        for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                        {
                            //
                            double s=Pcoordinate_Ps[i]->xy[0];
                            double t=Pcoordinate_Ps[i]->xy[1];

                            double pi_double=3.141592653589793238;
                            double alpha=3.0/2.0;
                            //double delta=2.1;
                            //--------------------------
                            double Vs=-pow(s/2.0,delta)*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            double Vt=pow(s/2.0,delta)*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            //--------------------------

                            X.push_back(Vs);
                            Y.push_back(Vt);
                        }
                        break;
                    case 2:// r=2^delta
                        for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                        {
                            //
                            double s=Pcoordinate_Ps[i]->xy[0];
                            double t=Pcoordinate_Ps[i]->xy[1];

                            double pi_double=3.141592653589793238;
                            double alpha=3.0/2.0;
                            //double delta=4.1;
                            //--------------------------
                            double Vs=-pow(s,delta)*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            double Vt=pow(s,delta)*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            //--------------------------

                            X.push_back(Vs);
                            Y.push_back(Vt);
                        }
                        break;
                    }
                    break;
//---------------------------------------------------------------------------------------------------
                case 3://alpha=2.0
                    switch (SubExampleIndex)
                    {
                    case 1:// r=1
                        for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                        {
                            //
                            double s=Pcoordinate_Ps[i]->xy[0];
                            double t=Pcoordinate_Ps[i]->xy[1];

                            double pi_double=3.141592653589793238;
                            double alpha=2.0;
                            //double delta=2.1;
                            //--------------------------
                            double Vs=-pow(s/2.0,delta)*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            double Vt=pow(s/2.0,delta)*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            //--------------------------

                            X.push_back(Vs);
                            Y.push_back(Vt);
                        }
                        break;
                    case 2:// r=2^delta
                        for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                        {
                            //
                            double s=Pcoordinate_Ps[i]->xy[0];
                            double t=Pcoordinate_Ps[i]->xy[1];

                            double pi_double=3.141592653589793238;
                            double alpha=2.0;
                            //double delta=4.1;
                            //--------------------------
                            double Vs=-pow(s,delta)*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            double Vt=pow(s,delta)*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            //--------------------------

                            X.push_back(Vs);
                            Y.push_back(Vt);
                        }
                        break;
                    }
                    break;
//---------------------------------------------------------------------------------------------------
                case 4://alpha=2.0// r=1
                    for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                    {
                        //
                        double s=Pcoordinate_Ps[i]->xy[0];
                        double t=Pcoordinate_Ps[i]->xy[1];

                        double pi_double=3.141592653589793238;
                        double alpha=2.0;
                        //double delta=2.1;
                        //--------------------------
                        double Vs=-pow(s/2.0,delta)*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                        double Vt=pow(s/2.0,delta)*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                        //--------------------------

                        X.push_back(Vs);
                        Y.push_back(Vt);
                    }
                    break;
//---------------------------------------------------------------------------------------------------
                case 5://alpha=3/2//r=1
                    for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                    {
                        //
                        double s=Pcoordinate_Ps[i]->xy[0];
                        double t=Pcoordinate_Ps[i]->xy[1];

                        double pi_double=3.141592653589793238;
                        double alpha=3.0/2.0;
                        //double delta=2.1;
                        //--------------------------
                        double Vs=-pow(s/2.0,delta)*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                        double Vt=pow(s/2.0,delta)*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                        //--------------------------

                        X.push_back(Vs);
                        Y.push_back(Vt);
                    }
                    break;
//---------------------------------------------------------------------------------------------------
                case 6://alpha=2.0// r=1
                    for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                    {
                        //
                        double s=Pcoordinate_Ps[i]->xy[0];
                        double t=Pcoordinate_Ps[i]->xy[1];

                        double pi_double=3.141592653589793238;
                        double alpha=2.0;
                        //double delta=2.1;
                        //--------------------------
                        double Vs=-pow(s/2.0,delta)*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                        double Vt=pow(s/2.0,delta)*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                        //--------------------------

                        X.push_back(Vs);
                        Y.push_back(Vt);
                    }
                    break;
//---------------------------------------------------------------------------------------------------
                case 7://alpha=3/2//r=1
                    for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                    {
                        //
                        double s=Pcoordinate_Ps[i]->xy[0];
                        double t=Pcoordinate_Ps[i]->xy[1];

                        double pi_double=3.141592653589793238;
                        double alpha=3.0/2.0;
                        //double delta=2.1;
                        //--------------------------
                        double Vs=-pow(s/2.0,delta)*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                        double Vt=pow(s/2.0,delta)*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                        //--------------------------

                        X.push_back(Vs);
                        Y.push_back(Vt);
                    }
                    break;


                }

            }
            else
            {
                if(s_der_order==1 && t_der_order==1)
                {
                    switch(ExampleIndex)
                    {
                    case 1://alpha=2.0
                        switch(SubExampleIndex)
                        {
                        case 1://r=1
                            for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                            {
                                //
                                double s=Pcoordinate_Ps[i]->xy[0];
                                double t=Pcoordinate_Ps[i]->xy[1];

                                double pi_double=3.141592653589793238;
                                double alpha=2.0;
                                //double delta=2.1;
                                //--------------------------
                                double Vs=-0.5*delta*pow(s/2.0,delta-1.0)*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                                double Vt=0.5*delta*pow(s/2.0,delta-1.0)*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                                //--------------------------

                                X.push_back(Vs);
                                Y.push_back(Vt);
                            }
                            break;

                        case 2://r=2^delta
                            for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                            {
                                //
                                double s=Pcoordinate_Ps[i]->xy[0];
                                double t=Pcoordinate_Ps[i]->xy[1];

                                double pi_double=3.141592653589793238;
                                double alpha=2.0;
                                //double delta=4.1;
                                //--------------------------
                                double Vs=-delta*pow(s,delta-1.0)*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                                double Vt=delta*pow(s,delta-1.0)*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                                //--------------------------

                                X.push_back(Vs);
                                Y.push_back(Vt);
                            }
                            break;

                        }
                        break;
                    case 2://alpha=3/2
                        switch(SubExampleIndex)
                        {
                        case 1://r=1
                            for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                            {
                                //
                                double s=Pcoordinate_Ps[i]->xy[0];
                                double t=Pcoordinate_Ps[i]->xy[1];

                                double pi_double=3.141592653589793238;
                                double alpha=3.0/2.0;
                                //double delta=2.1;
                                //--------------------------
                                double Vs=-0.5*delta*pow(s/2.0,delta-1.0)*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                                double Vt=0.5*delta*pow(s/2.0,delta-1.0)*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                                //--------------------------

                                X.push_back(Vs);
                                Y.push_back(Vt);
                            }
                            break;

                        case 2://r=2^delta
                            for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                            {
                                //
                                double s=Pcoordinate_Ps[i]->xy[0];
                                double t=Pcoordinate_Ps[i]->xy[1];

                                double pi_double=3.141592653589793238;
                                double alpha=3.0/2.0;
                                //double delta=4.1;
                                //--------------------------
                                double Vs=-delta*pow(s,delta-1.0)*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                                double Vt=delta*pow(s,delta-1.0)*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                                //--------------------------

                                X.push_back(Vs);
                                Y.push_back(Vt);
                            }
                            break;

                        }

                        break;

                    case 3://alpha=2.0
                        switch(SubExampleIndex)
                        {
                        case 1://r=1
                            for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                            {
                                //
                                double s=Pcoordinate_Ps[i]->xy[0];
                                double t=Pcoordinate_Ps[i]->xy[1];

                                double pi_double=3.141592653589793238;
                                double alpha=2.0;
                                //double delta=2.1;
                                //--------------------------
                                double Vs=-0.5*delta*pow(s/2.0,delta-1.0)*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                                double Vt=0.5*delta*pow(s/2.0,delta-1.0)*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                                //--------------------------

                                X.push_back(Vs);
                                Y.push_back(Vt);
                            }
                            break;

                        case 2://r=2^delta
                            for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                            {
                                //
                                double s=Pcoordinate_Ps[i]->xy[0];
                                double t=Pcoordinate_Ps[i]->xy[1];

                                double pi_double=3.141592653589793238;
                                double alpha=2.0;
                                //double delta=4.1;
                                //--------------------------
                                double Vs=-delta*pow(s,delta-1.0)*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                                double Vt=delta*pow(s,delta-1.0)*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                                //--------------------------

                                X.push_back(Vs);
                                Y.push_back(Vt);
                            }
                            break;

                        }
                        break;
//---------------------------------------------------------------------------------------------------
                    case 4://alpha=2.0//r=1
                        for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                        {
                            //
                            double s=Pcoordinate_Ps[i]->xy[0];
                            double t=Pcoordinate_Ps[i]->xy[1];

                            double pi_double=3.141592653589793238;
                            double alpha=2.0;
                            //double delta=2.1;
                            //--------------------------
                            double Vs=-0.5*delta*pow(s/2.0,delta-1.0)*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            double Vt=0.5*delta*pow(s/2.0,delta-1.0)*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            //--------------------------

                            X.push_back(Vs);
                            Y.push_back(Vt);
                        }
                        break;
//---------------------------------------------------------------------------------------------------
                    case 5://alpha=3/2//r=1
                        for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                        {
                            //
                            double s=Pcoordinate_Ps[i]->xy[0];
                            double t=Pcoordinate_Ps[i]->xy[1];

                            double pi_double=3.141592653589793238;
                            double alpha=3.0/2.0;
                            //double delta=2.1;
                            //--------------------------
                            double Vs=-0.5*delta*pow(s/2.0,delta-1.0)*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            double Vt=0.5*delta*pow(s/2.0,delta-1.0)*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            //--------------------------

                            X.push_back(Vs);
                            Y.push_back(Vt);
                        }
                        break;
//---------------------------------------------------------------------------------------------------
                    case 6://alpha=2.0//r=1
                        for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                        {
                            //
                            double s=Pcoordinate_Ps[i]->xy[0];
                            double t=Pcoordinate_Ps[i]->xy[1];

                            double pi_double=3.141592653589793238;
                            double alpha=2.0;
                            //double delta=2.1;
                            //--------------------------
                            double Vs=-0.5*delta*pow(s/2.0,delta-1.0)*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            double Vt=0.5*delta*pow(s/2.0,delta-1.0)*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            //--------------------------

                            X.push_back(Vs);
                            Y.push_back(Vt);
                        }
                        break;
//---------------------------------------------------------------------------------------------------
                    case 7://alpha=3/2//r=1
                        for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                        {
                            //
                            double s=Pcoordinate_Ps[i]->xy[0];
                            double t=Pcoordinate_Ps[i]->xy[1];

                            double pi_double=3.141592653589793238;
                            double alpha=3.0/2.0;
                            //double delta=2.1;
                            //--------------------------
                            double Vs=-0.5*delta*pow(s/2.0,delta-1.0)*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            double Vt=0.5*delta*pow(s/2.0,delta-1.0)*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            //--------------------------

                            X.push_back(Vs);
                            Y.push_back(Vt);
                        }
                        break;
                    }

                }
                else
                {
                    cout<<"ParametricMapSigmaVal::s_der_order or t_der_order is out of range!"<<endl;
                    cin.get();
                }
            }
        }
    }

}
//========================================================================

#endif
