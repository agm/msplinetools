// MSplinesWithoutHangingVertices.cpp
//===========================================

//===========================================

#include <iostream>
#include <fstream>
#include <map>
#include <time.h>


#include<stdio.h>
#include<dirent.h>

//PETSc
#include <petsc.h>
#include <petscksp.h>

//Spline
#include "msp/Mesh.h"
#include "msp/BasesVertex.h"
#include "msp/Bases.h"
#include "msp/BasesCell.h"
#include "msp/MSplineFunction.h"
#include "msp/PointSet.h"
#include "msp/Coordinate.h"
#include "msp/MSplineFunctions.h"
#include "msp/Functions.h"

//FEM
#include "msp/FEM_ErrorFunctions.h"
#include "msp/FEM_Functions.h"
#include "msp/FEM_DynamicFunctions.h"
#include "msp/FEM_CoeffMatrixLoadVector.h"
#include "msp/FEM_Print.h"


using namespace std;

/**
 * \section mspfem
 *
 * In this application, we will solve the PDE \f$-\nabla (K\nabla F)=f\f$ with \f$ F|_{\partial\Omega}=0\f$
 * over the domain of \f$\Omega\f$ which boundaries can be expressed in an exact way.
 *************************
 * - \c Input: Read the initial parametric mesh and the coeffients
 * - \c Output: The solution of the PDE, the errors of PDE
 **************************
 * It uses the MSplineFunctions class.
 *
 *\subsection other_pde Other Partial Differential Equations
 * If one wants to add their own PDE, the first thing that you should do is given
 * an ExampleIndex for this example (this ExampleIndex should be different from
 * the ExampleIndices of the given examples), then
 *
 * -# if your PDE is not of the form \f$-\nabla(K\nabla F)=f\f$, you should modify
 *   - KMatrixValue
 *   - ExactFunctionValue
 *   - RightSideFunctionValue
 *   - FxFunctionValue
 *   - FyFunctionValue
 *   See the file FEM_DynamicFunctions.h.
 * -# if your PDE has another form,
 * please re-write functions to generate coeff_matrix and load_vect
 *
 * About The paths of examples:
 * Suppose that the current work directionary is XXX/msplinetools/XX
 * where, "XX" is a folder given by ourself for saving the result of this application
 *
 *\subsection usage Usage
 * It can be used as follows:
 * \code
  ./msplinefem
 * \endcode
*/


int main(int argc,char **args)
{
    PetscInt m;
    PetscInt n = 4;
    Vec load_vect, m_solut;
    Mat coeff_matrix;// the stiff matrix

    KSP ksp;
    PC pc;
//    Mat M;
//    Vec D;
    KSPConvergedReason reason;

    ofstream OfTime;
    OfTime.open("Time.txt");

        //-------------FEM Solver----------------------
        cout<<"Generate Guass points and Weights"<<endl;

        OfTime<<endl;
        OfTime<<"Generate Guass points and Weights"<<endl;
        double t0=(double)clock();

        //Preparation: GuassPoint
        unsigned int Gp=0;
        cout<<"Input the number of Guass points:"<<endl;
        cin>>Gp;

        unsigned int u_gaussnum=Gp, v_gaussnum=Gp;
        //unsigned int load_gaussnum=5;
        unsigned int load_gaussnum=Gp;
        vector<double> gausspt, m_IntePt, m_loadw;
        //Weights/////////
        double **w;
         w=GaussPoints_Weights(u_gaussnum, load_gaussnum, gausspt, m_IntePt, m_loadw);


//         double isum=0.0;
//         for(unsigned int itest=0; itest<Gp; itest++)
//         {
//             for(unsigned int jtest=0; jtest<Gp; jtest++)
//             {
//                 cout<<"W["<<itest<<"]["<<jtest<<"]="<<w[itest][jtest]<<endl;
//                 isum=isum+w[itest][jtest];
//             }
//         }
//         cout<<"weight_sum = "<<isum<<endl;
//         cin.get();
//         cin.get();
//         cin.get();

        double t1=(double)clock();
        cout<<"\"Generate Guass points and Weights\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<endl;

        OfTime<<"\"Generate Guass points and Weights\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<endl;
        //================================================
        //Read the ParametricMap
        //Mesh
        cout<<"Read the mesh, subdivide it and parametric map"<<endl;

        //----------
        OfTime<<endl;
        OfTime<<"Read the mesh, subdivide it and parametric map"<<endl;

        Mesh * P_ParametricMesh=new Mesh;
        //Coeff
        vector<vector<double> > Coeff_Parametric_Map;
        unsigned int ExampleIndex=4;//default

        unsigned int SubExampleIndex=1;//default


       // cout<<"Please put in the index of this example"<<endl;

        cout<<"Remark:----------------------------------"<<endl;
        cout<<"1-->Example 1_1;"<<endl;
        cout<<"2-->Example 2_0;"<<endl;
        cout<<"3-->Example 2_1;"<<endl;
        cout<<"4-->Example 4;"<<endl;
        cout<<"5-->TestErrorOrderExample1_BiCubicSolution"<<endl;
        cout<<"6-->TestErrorOrderExample2_GeneralSolution"<<endl;
        cout<<"7-->-div(K divF)=f: the same mesh with Example 4"<<endl;
        cout<<"8-->-div(K divF)=f with mesh [-1,1]X[-1,1]"<<endl;
        cout<<"9-->-div(divF)=f with mesh [-1,1]X[-1,1]"<<endl;
        cout<<"10--> test1: a parametric map's error"<<endl;
        cout<<"11--> test2: a parametric map's error"<<endl;
        cout<<"12--> TestErrorOrderExample: Modified Example 4"<<endl;
        cout<<"13--> TestErrorOrderExample: 16-RegularPolygon"<<endl;
        cout<<"14--> TestErrorOrderExample: Modified 16-RegularPolygon"<<endl;
        cout<<"15--> TestErrorOrderExample: Example4_1 Square"<<endl;

        cout<<"16--> The Polygon with N=8"<<endl;
        cout<<"17--> Modified the Polygon with N=8"<<endl;
        cout<<"18--> The Polygon with N=16"<<endl;
        cout<<"19--> Modified the Polygon with N=16"<<endl;

        cout<<"20--> The Polygon with N=32"<<endl;
        cout<<"21--> Modified the Polygon with N=32"<<endl;
        cout<<"22--> The Polygon with N=64"<<endl;
        cout<<"23--> Modified the Polygon with N=64"<<endl;

        cout<<"24--> Example 2_2;"<<endl;

        cout<<"27--> Example 2_2 Modified Parametrization"<<endl;

        cout<<"28(a trivial example)-->Example 6: Test: is there some relationships between the error order and C^1 continuity "<<endl;

        cout<<"30(a non-trivial example)-->Example 6: Test: is there some relationships between the error order and C^1 continuity "<<endl;


        cout<<"36-->Example 9: For the \"real\" data?? Residual Error to test "<<endl;

        cout<<"37-->Example 7_3: alpha shape with exact boundary"<<endl;

        cout<<"46(ExampleIndex)-->The examples(1,2,...,7(SubExampleIndex) ) of Singularity test (L-Shape)"<<endl;

        cout<<"47(ExampleIndex)-->The examples (1, 2, 3(SubExampleIndex) ) of Singularity test (Rectangular)(with Residual error)(unKnown solution)"<<endl;

        cout<<"48-->The third example of Singularity test (Rectangular)(with L2 prioir error_1)(Known solution)"<<endl;

        cout<<"49-->The third example of Singularity test (Rectangular)(with L2 prioir error_2)(Known solution)"<<endl;

        cout<<"50-->The third example of Singularity test (Rectangular)(with L2 prioir error_3)(Known solution)"<<endl;

        cout<<"51-->Fan Shape (Unknown solution, SubExampleIndex=1,2,3,4,5)"<<endl;

        cout<<"52-->Crack Rectangular (Unknown solution, SubExampleIndex=1,2,3)"<<endl;
        cout<<"53-->Crack Rectangular (known solution)"<<endl;
        cout<<"54-->L-shape exact solution"<<endl;
        cout<<"55-->squares with a center singular vertex"<<endl;

        cout<<"56-->Test Rectangule"<<endl;

        cout<<"Put in the ExampleIndex:"<<endl;
        cin>>ExampleIndex;

        cout<<"SubExampleIndex: Put in The index of this example of this experiment with ExampleIndex: "<<endl;
        cin>>SubExampleIndex;


        OfTime<<"msplineFEM"<<endl;
        OfTime<<"Please put in the index of this example"<<endl;
        OfTime<<"1-->Example 1_1;"<<endl;
        OfTime<<"2-->Example 2_0;"<<endl;
        OfTime<<"3-->Example 2_1;"<<endl;
        OfTime<<"4-->Example 4;"<<endl;
        OfTime<<"5-->TestErrorOrderExample1_BiCubicSolution"<<endl;
        OfTime<<"6-->TestErrorOrderExample2_GeneralSolution"<<endl;
        OfTime<<"7-->-div(K divF)=f: the same mesh with Example 4"<<endl;
        OfTime<<"8-->-div(K divF)=f with mesh [-1,1]X[-1,1]"<<endl;
        OfTime<<"9-->-div(divF)=f with mesh [-1,1]X[-1,1]"<<endl;
        OfTime<<"10-->test1: a parametric map's error"<<endl;
        OfTime<<"11--> test2: a parametric map's error"<<endl;
        OfTime<<"12--> TestErrorOrderExample:Modified Example 4"<<endl;
        OfTime<<"13--> TestErrorOrderExample: 16-RegularPolygon"<<endl;
        OfTime<<"14--> TestErrorOrderExample: Modified 16-RegularPolygon"<<endl;
        OfTime<<"15--> TestErrorOrderExample: Example4_1 Square"<<endl;

        OfTime<<"16--> The Polygon with N=8"<<endl;
        OfTime<<"17--> Modified the Polygon with N=8"<<endl;
        OfTime<<"18--> The Polygon with N=16"<<endl;
        OfTime<<"19--> Modified the Polygon with N=16"<<endl;

        OfTime<<"20--> The Polygon with N=32"<<endl;
        OfTime<<"21--> Modified the Polygon with N=32"<<endl;
        OfTime<<"22--> The Polygon with N=64"<<endl;
        OfTime<<"23--> Modified the Polygon with N=64"<<endl;

        OfTime<<"24--> Example 2_2;"<<endl;

        OfTime<<"27--> Example 2_2 Modified Parametrization"<<endl;

        OfTime<<"28(a trivial example)-->Example 6: Test: is there some relationships between the error order and C^1 continuity "<<endl;

        OfTime<<"30(a non-trivial example)-->Example 6: Test: is there some relationships between the error order and C^1 continuity "<<endl;

        OfTime<<"36-->Example 9: For the \"real\" data?? Residual Error to test "<<endl;

        OfTime<<"37-->Example 7_3: alpha shape with exact boundary"<<endl;

        OfTime<<"46(ExampleIndex)-->The example (SubExampleIndex=1,2,..,7) of Singularity test (L-Shape)"<<endl;

        OfTime<<"47(ExampleIndex)-->The examples (SubExampleIndex=1, 2, 3) of Singularity test (Rectangular)(Unknown solution)"<<endl;

        OfTime<<"48-->The third second of Singularity test (Rectangular)(with L2 prioir error_1)(Known solution)"<<endl;

        OfTime<<"49-->The third example of Singularity test (Rectangular)(with L2 prioir error_2)(Known solution)"<<endl;

        OfTime<<"50-->The third example of Singularity test (Rectangular)(with L2 prioir error_3)(Known solution)"<<endl;

        OfTime<<"51-->Fan Shape (Unknown solution, SubExampleIndex=1,2,3,4,5)"<<endl;

        OfTime<<"52-->Crack Rectangular (Unknown solution, SubExampleIndex=1,2,3)"<<endl;

        OfTime<<"53-->Crack Rectangular (known solution)"<<endl;

        OfTime<<"54-->L-shape exact solution"<<endl;

        OfTime<<"55-->squares with a center singular vertex"<<endl;

        OfTime<<"56-->Test"<<endl;

        OfTime<<"The mesh of this example that has been chosen: "<< ExampleIndex<<endl;

        OfTime<<"The SubExampleIndex of this example of this experiment with ExampleIndex: "<<SubExampleIndex<<endl;


        unsigned int dimension=2;
        unsigned int VerNum;

        /* InPut k1 and k2 */
        double k1=1.0, k2=1.0;

        if((ExampleIndex>=7&& ExampleIndex<10)||(ExampleIndex==56))
        {
            cout<<"Put in the parameter k1, k2 for this Example:"<<endl;
            cin>>k1;
            cin>>k2;
            OfTime<<"The parameters of this Example:"<<endl;
            OfTime<<"k1 = "<<k1<<endl;
            OfTime<<"k2 = "<<k2<<endl;
        }

        /*
         * Check the current work dictionary

    */

//        cout<<"the current work dictionary (check if it is XXX/msplinetools/X)"<<endl;
//        char *buffer;
//        if((buffer = getcwd(NULL,0))==NULL)
//        {
//            perror("getcwd error");
//        }
//        else
//        {
//            printf("%s\n", buffer);
//            free(buffer);
//        }

//        /*
//        * Read the initial Parametric Mesh (ParametricMesh.txt) and Coefficients (ParametricCoeffs.txt)
//        */

        switch(ExampleIndex)
        {
        case 1://Example 1_1
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example1_1/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/Example1_1/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;
        case 2://Example 2_0;
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example2_0/BasedOnParametricMap_2/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/Example2_0/BasedOnParametricMap_2/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;
        case 3://Example 2_1
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example2_1/BasedOnParametricMap_2/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/Example2_1/BasedOnParametricMap_2/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;
        case 4://Example 4
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example4/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/Example4/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;
        case 5://TestErrorOrderExample1_BiCubicSolution
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/TestErrorOrderExamples/ClassicalTest/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/TestErrorOrderExamples/ClassicalTest/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;
        case 6://TestErrorOrderExample2_GeneralSolution
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/TestErrorOrderExamples/ClassicalTest/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/TestErrorOrderExamples/ClassicalTest/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;
        case 7://-div(K divF)=f: with the same mesh with Example 4
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example4/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/Example4/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;
        case 8://-div(K divF)=f: with mesh [-1,1]X[-1,1]
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example8/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/Example8/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;
        case 9://-div(divF)=f: with mesh [-1,1]X[-1,1]
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example8/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/Example8/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;
        case 10:// test1: a parametric map's error
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example4/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/Example4/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;
        case 11:// test2: a parametric map's error
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example4/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/Example4/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;
        case 12:// Modify the parametric Map of Example 4
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/TestErrorOrderExamples/IGATest/non-exactBoundary/Example4Modified/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/TestErrorOrderExamples/IGATest/non-exactBoundary/Example4Modified/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;
        case 13:// Example4_1: the parametric map: 16-RegularPolygon
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/TestErrorOrderExamples/IGATest/non-exactBoundary/Example4_1-circle/16-RegularPolygon/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/TestErrorOrderExamples/IGATest/non-exactBoundary/Example4_1-circle/16-RegularPolygon/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;
        case 14:// Example4_1: the parametric map: Modified 16-RegularPolygon
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/TestErrorOrderExamples/IGATest/non-exactBoundary/Example4_1-circle/modifed16RegularPolygon/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/TestErrorOrderExamples/IGATest/non-exactBoundary/Example4_1-circle/modifed16RegularPolygon/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;
        case 15:// TestErrorOrderExample: Example4_1 Square
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/TestErrorOrderExamples/IGATest/exactBoundary/Example4_1-square/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/TestErrorOrderExamples/IGATest/exactBoundary/Example4_1-square/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;

        case 16://The Polygon with N=8
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/NPolygon/n=8/Mesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/NPolygon/n=8/Coeff.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;

        case 17://The Modified Polygon with N=8
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/NPolygon/n=8/Mesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/NPolygon/n=8/ModifiedCoeff/Coeff.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;

        case 18://The Polygon with N=16
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/NPolygon/n=16/Mesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/NPolygon/n=16/Coeff.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;

        case 19://The Modified Polygon with N=16
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/NPolygon/n=16/Mesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/NPolygon/n=16/ModifiedCoeff/Coeff.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;

        case 20://The Polygon with N=32
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/NPolygon/n=32/Mesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/NPolygon/n=32/Coeff.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;

        case 21://The Modified Polygon with N=32
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/NPolygon/n=32/Mesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/NPolygon/n=32/ModifiedCoeff/Coeff.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;

        case 22://The Polygon with N=64
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/NPolygon/n=64/Mesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/NPolygon/n=64/Coeff.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;

        case 23://The Modified Polygon with N=64
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/NPolygon/n=64/Mesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/NPolygon/n=64/ModifiedCoeff/Coeff.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;

        case 24://24--> Example 2_2
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example2_2/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/Example2_2/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;

        case 27://27--> Example 2_2
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example2_2/Improved_Parametrization/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/Example2_2/Improved_Parametrization/ParametricCoeffs.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;

        case 28://28-->Example 6
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example6/ParametricMesh_82.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/Example6/ParametricCoeffs_82.txt",
                                       dimension, VerNum, Coeff_Parametric_Map);
            break;

        case 30://28-->Example 6
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example6/ParametricMesh_1.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/Example6/ParametricCoeffs_1.txt",
                                       dimension, VerNum, Coeff_Parametric_Map);
            break;

        case 36://-->Example 9
            P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/Example9/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/Example9/ParametricCoeffs.txt",
                                       dimension, VerNum, Coeff_Parametric_Map);
            break;

        case 37://-->Example 7_3
            P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/Example7_3/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/Example7_3/ParametricCoeffs.txt",
                                       dimension, VerNum, Coeff_Parametric_Map);
            break;

        case 46://-->The  Singularity test
            switch(SubExampleIndex)
            {
            case 1:
                P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_LShape/1/ParametricMesh.txt");
                //P_ParametricMesh->Cout_Mesh_File();
                VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
                Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_LShape/1/ParametricCoeffs.txt",
                                           dimension, VerNum, Coeff_Parametric_Map);
                break;
            case 2:
                P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_LShape/2/ParametricMesh.txt");
                //P_ParametricMesh->Cout_Mesh_File();
                VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
                Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_LShape/2/ParametricCoeffs.txt",
                                           dimension, VerNum, Coeff_Parametric_Map);
                break;
            case 3:
                P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_LShape/3/ParametricMesh.txt");
                //P_ParametricMesh->Cout_Mesh_File();
                VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
                Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_LShape/3/ParametricCoeffs.txt",
                                           dimension, VerNum, Coeff_Parametric_Map);
                break;
            case 4:
                P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_LShape/4/ParametricMesh.txt");
                //P_ParametricMesh->Cout_Mesh_File();
                VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
                Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_LShape/4/ParametricCoeffs.txt",
                                           dimension, VerNum, Coeff_Parametric_Map);
                break;
            case 5:
                P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_LShape/5/ParametricMesh.txt");
                //P_ParametricMesh->Cout_Mesh_File();
                VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
                Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_LShape/5/ParametricCoeffs.txt",
                                           dimension, VerNum, Coeff_Parametric_Map);
                break;
            case 6:
                P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_LShape/6/ParametricMesh.txt");
                //P_ParametricMesh->Cout_Mesh_File();
                VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
                Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_LShape/6/ParametricCoeffs.txt",
                                           dimension, VerNum, Coeff_Parametric_Map);
                break;
            case 7:
                P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_LShape/7/ParametricMesh.txt");
                //P_ParametricMesh->Cout_Mesh_File();
                VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
                Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_LShape/7/ParametricCoeffs.txt",
                                           dimension, VerNum, Coeff_Parametric_Map);
                break;
            case 8:
                P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_LShape/8/ParametricMesh.txt");
                //P_ParametricMesh->Cout_Mesh_File();
                VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
                Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_LShape/8/ParametricCoeffs.txt",
                                           dimension, VerNum, Coeff_Parametric_Map);
                break;
            }

            break;

        case 47://"47(ExampleIndex)-->The examples(1,2,3 (SubExampleIndex) ) of Singularity test (Rectangular)(Unknown Solution)(Residual error)"<<endl;
            switch(SubExampleIndex)
            {
            case 1:
                P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_Rectangular/1/ParametricMesh.txt");
                //P_ParametricMesh->Cout_Mesh_File();
                VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
                Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_Rectangular/1/ParametricCoeffs.txt",
                                           dimension, VerNum, Coeff_Parametric_Map);
                break;

            case 2:
                P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_Rectangular/2/ParametricMesh.txt");
                //P_ParametricMesh->Cout_Mesh_File();
                VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
                Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_Rectangular/2/ParametricCoeffs.txt",
                                           dimension, VerNum, Coeff_Parametric_Map);
                break;

            case 3:
                P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_Rectangular/3/ParametricMesh.txt");
                //P_ParametricMesh->Cout_Mesh_File();
                VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
                Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_Rectangular/3/ParametricCoeffs.txt",
                                           dimension, VerNum, Coeff_Parametric_Map);
                break;
            }

            break;

        case 48://"48-->The second example of Singularity test (Rectangular)(Known Solution) (L2 Priori error)"<<endl;
            P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_Rectangular/3/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_Rectangular/3/ParametricCoeffs.txt",
                                       dimension, VerNum, Coeff_Parametric_Map);
            break;

        case 49://"49-->The third example of Singularity test (Rectangular)(with L2 prioir error_2)(Known solution)"<<endl;
            P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_Rectangular/3/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_Rectangular/3/ParametricCoeffs.txt",
                                       dimension, VerNum, Coeff_Parametric_Map);
            break;

        case 50://"50-->The third example of Singularity test (Rectangular)(with L2 prioir error_3)(Known solution)"<<endl;
            P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_Rectangular/3/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_Rectangular/3/ParametricCoeffs.txt",
                                       dimension, VerNum, Coeff_Parametric_Map);
            break;

        case 51://Fan-Shape (Unknown solution, SubExampleIndex=1,2,3,4)
            //
            P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_fanShape/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_fanShape/ParametricCoeffs.txt",
                                       dimension, VerNum, Coeff_Parametric_Map);
            break;

        case 52://cout<<"52-->Crack Rectangular (Unknown solution, SubExampleIndex=1)"<<endl;

            switch (SubExampleIndex)
            {
            case 1:
                //
                P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_CrackRectangular/1/ParametricMesh.txt");
                //P_ParametricMesh->Cout_Mesh_File();
                VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
                Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_CrackRectangular/1/ParametricCoeffs.txt",
                                           dimension, VerNum, Coeff_Parametric_Map);
                break;
            case 2:
                //
                P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_CrackRectangular/2/ParametricMesh.txt");
                //P_ParametricMesh->Cout_Mesh_File();
                VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
                Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_CrackRectangular/2/ParametricCoeffs.txt",
                                           dimension, VerNum, Coeff_Parametric_Map);
                break;
            case 3:
                //
                P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_CrackRectangular/3/ParametricMesh.txt");
                //P_ParametricMesh->Cout_Mesh_File();
                VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
                Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_CrackRectangular/3/ParametricCoeffs.txt",
                                           dimension, VerNum, Coeff_Parametric_Map);
                break;

            }
            break;

        case 53://CrackRectangular
            P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/SingularityTest_CrackRectangular/2/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/SingularityTest_CrackRectangular/2/ParametricCoeffs.txt",
                                       dimension, VerNum, Coeff_Parametric_Map);
            break;

        case 54://L-shape exact solution
            P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/SingularityTest_LShape/5/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/SingularityTest_LShape/5/ParametricCoeffs.txt",
                                       dimension, VerNum, Coeff_Parametric_Map);
            break;

        case 55://squares with a center singular vertex
            P_ParametricMesh->ReadFile_withoutHangingVertices("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/SquareDifferentSingularPara/2/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/Q4AxelMsplineTools/msplinetools_Qt4/data/Examples/SquareDifferentSingularPara/2/ParametricCoeffs.txt",
                                       dimension, VerNum, Coeff_Parametric_Map);
            break;

        case 56:
            cout<<"read mesh and coeff exampleindex=56"<<endl;
            P_ParametricMesh->ReadFile_withoutHangingVertices("../../txtData/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../../txtData/ParametricCoeffs_copy.txt", dimension, VerNum, Coeff_Parametric_Map);
            break;

        default:
            P_ParametricMesh->ReadFile_withoutHangingVertices("../data/Examples/Example4/ParametricMesh.txt");
            //P_ParametricMesh->Cout_Mesh_File();
            VerNum=P_ParametricMesh->P_Vertices->size();//BasisNum=VerNum*4//Maybe, there is a "zero" basis
            Reader_Coeff_ParametricMap("../data/Examples/Example4/ParametricCoeffs.txt",
                                       dimension, VerNum, Coeff_Parametric_Map);
            break;
        }

        /* OutPut the parametricMesh for checking*/
        //P_ParametricMesh->Cout_Mesh_File();

        double InitialCellSize=1.0;
        if(P_ParametricMesh->P_Cells->size())
        {
            InitialCellSize=P_ParametricMesh->P_Cells->at(0).CellSize[1]-P_ParametricMesh->P_Cells->at(0).CellSize[0];
        }


        /*OutPut the parametricCoeff for checking*/
//        cout<<"Coeff_Parametric_Map:"<<endl;
//        for(unsigned int i_test=0; i_test<Coeff_Parametric_Map[0].size(); i_test++)
//        {
//            cout<<"("<< Coeff_Parametric_Map[0][i_test]<<", "<<Coeff_Parametric_Map[1][i_test]<<")"<<endl;
//        }


        /*Generate a Parametric map, if ExampleIndex!=51 */
        MSplineFunctions ParametricMap(Coeff_Parametric_Map, *P_ParametricMesh);

        /*OutPut this Parametric map*/
        //ParametricMap.Cout_MSplineFunctions();
        //========================================
        unsigned int k;
        cout<<"Put in the value k for determining the times of subdivision:"<<endl;
        cin>>k;
        //---------------------
        OfTime<<endl;
        OfTime<<"Put in the value k for determining the times of subdivision:"<<endl;
        OfTime<<"k = "<<k<<endl;
        //---------------------
        /*unsigned int methodNum;
        cout<<"Choose the methods for generating coeff_matrix: "<<endl;
        cout<<"1: Based on Cells;"<<endl;
        cout<<"2: Based on Vertices;"<<endl;
        cout<<"3: Generated all the bases at the begining;"<<endl;
        cout<<"4: Generated Bases in HVertices form."<<endl;
        cin>>methodNum;*/
        //----------------------
        OfTime<<endl;
        OfTime<<"Choose the methods for generating coeff_matrix: "<<endl;
        OfTime<<"1: Based on Cells;"<<endl;
        OfTime<<"2: Based on Vertices;"<<endl;
        OfTime<<"3: Generated all the bases at the begining;"<<endl;
        OfTime<<"4: Generated Bases in HVertices form."<<endl;
        //OfTime<<"the method which we chosen: "<<methodNum<<endl;
        //========================================
        //The FEM Mesh
        map<unsigned int, unsigned int> OrigialVertexToCurrentVertex;

        //subdivide the initial parametric mesh
        Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(k, OrigialVertexToCurrentVertex);
	
        cout<<"The Basis Number = "<<4*(P_FEMMesh->P_Vertices->size())<<endl;
	
        m = P_FEMMesh->P_Vertices->size();

        //==============================PETSC===================================

        PetscInitialize(&argc, &args, (char*) 0, NULL);
        PetscOptionsGetInt(NULL, "-m", &m, NULL);
        PetscOptionsGetInt(NULL, "-n", &n, NULL);

        cout<<"Initialized coeffMatrix and Generate_coeffMatrix"<<endl;

        OfTime<<endl;
        OfTime<<"Initialized coeffMatrix and Generate_coeffMatrix"<<endl;

        t0=(double)clock();

        MatCreate(PETSC_COMM_WORLD, &coeff_matrix);
       // PetscInt matrix_size=m*n;
        MatSetSizes(coeff_matrix, PETSC_DECIDE, PETSC_DECIDE, m*n, m*n);
        MatSetFromOptions(coeff_matrix);
        MatMPIAIJSetPreallocation(coeff_matrix, 64, NULL, 64, NULL);
        MatSeqAIJSetPreallocation(coeff_matrix, PETSC_DEFAULT, NULL);
        MatSetUp(coeff_matrix);

        map<unsigned int, set<unsigned int> > HTable;
        //Initialized_HTable_withoutHangingVertices(P_FEMMesh, HTable);
        //switch(methodNum)
        //{
        //case 1:
                //1.__Generate_CoeffMatrix_Based on Cells
                cout<<"Generate_CoeffMatrix_Based on Cells"<<endl;
                Generate_CoeffMatrix_Cell(u_gaussnum, v_gaussnum, coeff_matrix, m_IntePt, m_IntePt, w,
                                          P_FEMMesh, P_ParametricMesh, ParametricMap, k1, k2, ExampleIndex);
        //        break;
        //case 2:
//                //2.__Generate_CoeffMatrix_Based on Vertices
//                cout<<"Generate_CoeffMatrix_Based on Vertices"<<endl;
//                Initialized_HTable_withoutHangingVertices(P_FEMMesh, HTable);//Initialized_HTable_withoutHangingVertices(const Mesh *P_FEMMesh, map<unsigned int, set<unsigned int> > &HTable)
//                Generate_CoeffMatrix_Vertex_HTable(u_gaussnum, v_gaussnum, coeff_matrix, m_IntePt, m_IntePt, w,
//                                         P_FEMMesh, P_ParametricMesh, ParametricMap, HTable, k1, k2, ExampleIndex);
//                break;
//        case 3:
//                ////3._Generate_CoeffMatrix_Based on All the bases
//                cout<<"Generate CoeffMatrix Based on All the Bases"<<endl;
//                Initialized_HTable_withoutHangingVertices(P_FEMMesh, HTable);
//                Generate_CoeffMatrix_All_Bases(u_gaussnum, v_gaussnum, coeff_matrix, m_IntePt, m_IntePt, w,
//                                        P_FEMMesh, P_ParametricMesh, ParametricMap, HTable, k1, k2, ExampleIndex);
//                break;
//        case 4:
//                ////4._Generate_CoeffMatrix_Based on HVertices
//                cout<<"Generate_CoeffMatrix_Based on HVertices"<<endl;
//                Initialized_HTable_withoutHangingVertices(P_FEMMesh, HTable);
//                Generate_CoeffMatrix_HVertices(u_gaussnum, v_gaussnum, coeff_matrix, m_IntePt, m_IntePt, w,
//                                        P_FEMMesh, P_ParametricMesh, ParametricMap, HTable, k1, k2, ExampleIndex);
//                break;
//        default:
//                cout<<"You have to choose the index of method between 1 to 4"<<endl;
//                break;
//        }




        t1=(double)clock();
        cout<<"\"Initialized coeffMatrix and Generate_coeffMatrix\" costs ="<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
        OfTime<<"\"Initialized coeffMatrix and Generate_coeffMatrix\" costs ="<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

        //load_vect
        //vector<double> load_vect;
        cout<<"Initialized the load Vector and Generate the load Vector"<<endl;

        OfTime<<endl;
        OfTime<<"Initialized the load Vector and Generate the load Vector"<<endl;
        t0=(double)clock();


        Initialized_loadVector(load_vect, P_FEMMesh->P_Vertices->size());
        Generate_LoadVector(load_vect, gausspt, m_loadw, P_FEMMesh, P_ParametricMesh, ParametricMap, k1, k2, ExampleIndex);
        //

        t1=(double)clock();

        cout<<"Finished and it costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
        OfTime<<"Finished and it costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

        //Set Bases type
        cout<<"Set Bases type"<<endl;
        t0=(double)clock();
        vector<unsigned int> BasesType;
        BasesType.resize(4*P_FEMMesh->P_Vertices->size(), 0);
        Set_Bases_type(P_FEMMesh, BasesType);

//        cout<<"BasesType:"<<endl;
//        for(unsigned int i=0; i<BasesType.size(); i++)
//        {
//            //
//            cout<<i<<"---->"<<BasesType[i]<<endl;
//        }

        cin.get();

        t1=(double)clock();
        cout<<"\"Set Bases type\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

        OfTime<<endl;
        OfTime<<"There are "<< 4*P_FEMMesh->P_Vertices->size()<<" bases"<<endl;
        OfTime<<"\"Set Bases type\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;



        //Modify LoadVector and CoeffMatrix
        double C=0.0;

        MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);

        VecAssemblyBegin(load_vect);
        VecAssemblyEnd(load_vect);

//        MatView(coeff_matrix, PETSC_VIEWER_STDOUT_SELF);
//        cin.get();

        Initialized_HTable_withoutHangingVertices_1(P_FEMMesh, HTable);
        cout<<"Modification_LoadVector_CoeffMatrix"<<endl;

        OfTime<<endl;
        OfTime<<"Modification_LoadVector_CoeffMatrix"<<endl;
        t0=(double)clock();
        Modification_LoadVector_CoeffMatrix(C, BasesType, coeff_matrix, load_vect, HTable);


        VecAssemblyBegin(load_vect);
        VecAssemblyEnd(load_vect);

       // PetscLogStagePop();

        MatSetOption(coeff_matrix, MAT_SYMMETRIC, PETSC_TRUE);

        t1=(double)clock();
        cout<<"\"Modification_LoadVector_CoeffMatrix\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
        OfTime<<"\"Modification_LoadVector_CoeffMatrix\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
        //
        VecCreate(PETSC_COMM_WORLD, &m_solut);
        VecSetSizes(m_solut, PETSC_DECIDE, 4*P_FEMMesh->P_Vertices->size());
        VecSetFromOptions(m_solut);

        VecAssemblyBegin(m_solut);
        VecAssemblyEnd(m_solut);
        MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);


        //Save coeff_matrix as a matlab file
        PetscViewer viewer;
        PetscViewerASCIIOpen(PETSC_COMM_WORLD, "CoeffMatrix_mspfem.m", &viewer);
        PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_MATLAB);
        MatView(coeff_matrix, viewer);
        PetscViewerDestroy(&viewer);

        //Solving the Linear System Part====================
        cout<<"solve the Linear system"<<endl;
        t0=(double)clock();
        //KSP ksp;
        //PC pc;
        KSPCreate(PETSC_COMM_WORLD, &ksp);
       // KSPSetType(ksp, KSPCG);
        KSPSetType(ksp, KSPGMRES);
        KSPSetOperators(ksp, coeff_matrix, coeff_matrix);


        cout<<"InitialCellSize = "<<InitialCellSize<<endl;

        /*Set the parameters of KSP*/
        PetscReal rtol=(1.e-4)*pow(InitialCellSize/(1.0*k+1.0),4.0);
        PetscReal stol=(1.e-3)*pow(InitialCellSize/(1.0*k+1.0),4.0);
//        PetscReal rtol=1.0e-18;
//        PetscReal stol=1.0e-18;

        KSPSetTolerances(ksp, rtol, stol, PETSC_DEFAULT, PETSC_DEFAULT);

        KSPGetPC(ksp, &pc);
        PCSetType(pc, PCGAMG);
        KSPSetFromOptions(ksp);

        KSPSolve(ksp, load_vect, m_solut);

        KSPGetConvergedReason(ksp,&reason);
        //printf
        PetscInt its;
        KSPGetIterationNumber(ksp, &its);
        PetscPrintf(PETSC_COMM_WORLD, "The number of iteration %D\n", its);
        cout<<"KSP ConvergedReason= "<<reason<<endl;

        t1=(double)clock();
        cout<<"\"Solving the linear system\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

        OfTime<<endl;
        OfTime<<"\"Solving the linear system\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
        OfTime<<"k = "<<k<<endl;
        OfTime<<"where, rtol=(1.e-4)*pow(InitialCellSize/(k+1),4.0)="<<rtol<<" and  stol=(1.e-3)*pow(InitialCellSize/(k+1),4.0)= "<<stol<<endl;

        OfTime<<"KSP ConvergedReason= "<<reason<<endl;
        OfTime<<"the number of iteration = "<<its<<endl;
        OfTime<<endl;
        //-------
        OfTime.close();


        //Print load_vect
        PrintLoadVector(load_vect);

        PrintSolution(m_solut);

        if(ExampleIndex!=51)
        {
            PrintAxelFile_FEMSolution(Coeff_Parametric_Map, P_ParametricMesh, k, m_solut);
        }


        //=========Detroy
        VecDestroy(&load_vect);
        MatDestroy(&coeff_matrix);
        KSPDestroy(&ksp);
//        PCDestroy(&pc);

        //Error========================================================================
        vector<double> cell_errors;//Store the errors over all the cells
        double totol_Error=1.0;
        //----
        vector<double> cell_errorsPhysDom;
        double total_ErrorPhysDom;
        //---
        vector<double> cell_errorsResidual;
        double total_Error_Residual;
        //---

        vector<double> cell_errorsResidualPhsicalDom;
        double total_Error_ResidualPhsicalDom;

//        //---
        vector<double> CellErrEnergyNormPhysDom;
        double total_ErrEnergyNormPhysDom;
        //---
        vector<double> CellErrEnergyNormMesh;
        double total_ErrEnergyNormMesh;

//        //Compute all kinds of errors
        FEMErrors(gausspt, w, m_solut, P_FEMMesh, P_ParametricMesh,
                  &ParametricMap, ExampleIndex,
                  cell_errors, totol_Error,
                  cell_errorsPhysDom, total_ErrorPhysDom,
                  cell_errorsResidual, total_Error_Residual,
                  cell_errorsResidualPhsicalDom,  total_Error_ResidualPhsicalDom,
                  CellErrEnergyNormMesh, total_ErrEnergyNormMesh,
                  CellErrEnergyNormPhysDom, total_ErrEnergyNormPhysDom);


  /*      if(ExampleIndex != 36&& ExampleIndex !=46 && ExampleIndex !=47 && ExampleIndex!=51 && ExampleIndex!=52)
        {
            FEML2PosteriorError(gausspt, w, m_solut, P_FEMMesh, P_ParametricMesh,
                                &ParametricMap, ExampleIndex, cell_errorsPhysDom,
                                total_ErrorPhysDom);
        }
        else
        {

////-------------L2 Posterior error (compare with a approximation solution)-----------------
            vector<double> femsolution;
            PetscInt SolutSize;
            VecGetSize(m_solut, &SolutSize);
            for(PetscInt Ipetsc = 0; Ipetsc < SolutSize; Ipetsc++)
            {
                PetscScalar v;
                VecGetValues(m_solut,1,&Ipetsc,&v);
                femsolution.push_back(v);
            }

            Predict_FEML2PosteriorError(gausspt, w,femsolution,
                                        SubExampleIndex, P_ParametricMesh, P_FEMMesh,
                                         ExampleIndex,  &ParametricMap,
                                        cell_errorsPhysDom, total_ErrorPhysDom);
/////----------------------------------------------------------------------------------------------

//            switch (ExampleIndex)
//            {
//            case 51:
//                PrintAxelMeshFile_FEMSolution_SpecialParameterization(m_solut,P_FEMMesh, ExampleIndex, k,SubExampleIndex);
//                break;

//            default:
//                ResidualError_PhysicalDomain_RefinedParametricMap(gausspt, gausspt, w, m_solut,
//                                                                  cell_errorsResidualPhsicalDom, total_Error_ResidualPhsicalDom,
//                                                                   P_FEMMesh,  P_ParametricMesh,
//                                                                  &ParametricMap, ExampleIndex);
//                ResidualError_PhysicalDomain_AxelMeshSurface(m_solut, P_FEMMesh, P_ParametricMesh, &ParametricMap, ExampleIndex, k);

//                break;
//            }
//            //


        }*/


//        //-----------------------------------



        VecDestroy(&m_solut);

        PetscFinalize();
}

