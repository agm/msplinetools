#include <iostream>
using namespace std;

#include "MSplineFunctions.h"
#include "MSplineFunction.h"
#include "Mesh.h"
#include "Cell.h"
#include "BasesVertex.h"
#include "Basis.h"

#include "Functions.h"
#include "MSplineEvaluation.h"
#include "PointSet.h"

MSplineFunctions::MSplineFunctions(void)
{
}

MSplineFunctions::~MSplineFunctions(void)
{
}

void MSplineFunctions::GaussianCurvature(vector<double> &GaussianCurvatures, const vector<PointSet> &PSet, Mesh & CurrentMesh)
{
    //Return GaussianCurvatures at each point
    GaussianCurvatures.clear();

    for(unsigned int i=0; i<PSet.size(); i++)
    {
        unsigned int i_cell=PSet[i].Index_ItsCell;
        vector< vector<double> > Ps, Pt , Pss, Ptt, Pst;
        //
        Evaluation(1,0,PSet[i].P_coordinates_Ps, i_cell, Ps, CurrentMesh);
        Evaluation(0,1,PSet[i].P_coordinates_Ps, i_cell, Pt, CurrentMesh);
        Evaluation(2,0,PSet[i].P_coordinates_Ps, i_cell, Pss, CurrentMesh);
        Evaluation(0,2,PSet[i].P_coordinates_Ps, i_cell, Ptt, CurrentMesh);
        Evaluation(1,1,PSet[i].P_coordinates_Ps, i_cell, Pst, CurrentMesh);
        //
        for(unsigned int j=0; j<PSet[i].P_coordinates_Ps.size(); j++)
        {
            double nx, ny, nz;
            nx=Ps[1][j]*Pt[2][j]-Ps[2][j]*Pt[1][j];
            ny=-(Ps[0][j]*Pt[2][j]-Ps[2][j]*Pt[0][j]);
            nz=Ps[0][j]*Pt[1][j]-Pt[0][j]*Ps[1][j];
            //
            double g=(Ps[0][j]*Ps[0][j]+Ps[1][j]*Ps[1][j]+Ps[2][j]*Ps[2][j])*(Pt[0][j]*Pt[0][j]+Pt[1][j]*Pt[1][j]+Pt[2][j]*Pt[2][j])-pow(Ps[0][j]*Pt[0][j]+Ps[1][j]*Pt[1][j]+Ps[2][j]*Pt[2][j],2.0);
            //
            double K=(nx*Pss[0][j]+ny*Pss[1][j]+nz*Pss[2][j])*(nx*Ptt[0][j]+ny*Ptt[1][j]+nz*Ptt[2][j]);
            K=K-(nx*Pst[0][j]+ny*Pst[1][j]+nz*Pst[2][j])*(nx*Pst[0][j]+ny*Pst[1][j]+nz*Pst[2][j]);
            if(g)
            {
                K=K/g;
            }
            else
            {
                K=0.0;
            }

            cout<<"K="<<K<<endl;
            //
            if(K>0)
            {
                GaussianCurvatures.push_back(K);
            }
            else
            {
                GaussianCurvatures.push_back(-K);
            }

        }
    }

}

void MSplineFunctions::ComputeVerticesPositions(vector<vector<double> >& VerticesPosition,  Mesh* P_ParametricMesh)
{
    VerticesPosition.clear();

    unsigned int VerNum=P_ParametricMesh->P_Vertices->size();

    for(unsigned int i=0; i<VerNum; i++)
    {
        vector< vector<double> > ValueXY;
        vector<double> Value;

        vector<Coordinate*> VertexCoordinate;
        map<unsigned int, Coordinate>::iterator It=P_ParametricMesh->P_Vertices->at(i).ItsCellsToCoordinatesMap.begin();
        VertexCoordinate.push_back(&(It->second));

        //cout<<"VertexCoordinate="<<(It->second).xy[0]<<" "<<(It->second).xy[1]<<endl;

        unsigned int CellIndex= (*It).first;

        this->Evaluation(0, 0, VertexCoordinate, CellIndex, ValueXY, *P_ParametricMesh);


        Value.push_back(ValueXY[0][0]);
        Value.push_back(ValueXY[1][0]);
        VerticesPosition.push_back(Value);
    }

}

void MSplineFunctions::Evaluation(int DerOrderu, int DerOrderv, const vector<PointSet> & PsetS, vector<vector<vector<double> > > &ValuesXYZ, const Mesh & CurrentMesh)
{
    //cout<<"MSplineFunctions::Evaluation()"<<endl;

	ValuesXYZ.clear();

	for (unsigned int i=0; i<P_MSplineFunctions.size(); i++)
	{
		vector<vector<double> > Values;
		P_MSplineFunctions[i]->Evaluation(DerOrderu, DerOrderv, PsetS, Values, CurrentMesh);
		ValuesXYZ.push_back(Values);
	}
}

void MSplineFunctions::Evaluation(int DerOrderu, int DerOrderv, const vector<Coordinate*> & Pcoordinate_Ps, unsigned int index_cell, vector<vector<double> >  &ValuesXYZ, const Mesh & CurrentMesh)
{
	ValuesXYZ.clear();
	for (unsigned int i=0; i<P_MSplineFunctions.size(); i++)
	{
		vector<double> Values;
		P_MSplineFunctions[i]->Evaluation(DerOrderu, DerOrderv, Pcoordinate_Ps, Values, index_cell, CurrentMesh);
		ValuesXYZ.push_back(Values);
	}
}

MSplineFunctions::MSplineFunctions(const vector<vector<double> > &Coeff, const Bases &BasisSet, const Mesh & CurrentMesh)
{
	P_MSplineFunctions.clear();
	for (unsigned int i = 0; i < Coeff.size(); i ++)
	{
        MSplineFunction * P_MSplineFunc = new MSplineFunction(Coeff[i], BasisSet, CurrentMesh);
		P_MSplineFunctions.push_back(P_MSplineFunc);
	}
}

//void MSplineFunctions::Reset(const vector<vector<double> > &Coeff, const Bases &BasisSet, const Mesh & CurrentMesh)
//{
//}

//void MSplineFunctions::Reset(const vector<vector<double> > &Coeff, const Mesh & CurrentMesh);

MSplineFunctions::MSplineFunctions(const vector<vector<double> > &Coeff, const Mesh & CurrentMesh)
{
	P_MSplineFunctions.clear();
	for (unsigned int i = 0; i < Coeff.size(); i++)
	{
		MSplineFunction * P_MSplineFunc = new MSplineFunction(Coeff[i], CurrentMesh);
		P_MSplineFunctions.push_back(P_MSplineFunc);
		P_MSplineFunc = NULL;
	}

}

void MSplineFunctions::Cout_MSplineFunctions()
{
    for(unsigned int i=0; i<P_MSplineFunctions.size(); i++)
    {
        cout<<"P_MSplineFunctions["<<i<<"]"<<endl;
        P_MSplineFunctions[i]->Cout_MSplineFunction();
    }
}

MSplineFunctions::MSplineFunctions(const vector<vector<double> > & VerticesPosition,
                                        Mesh * P_ParametricMesh,
                                        vector<vector <double> >& Coeff)
{

    Coeff.clear();
    vector<double> CoeffX, CoeffY;
    //=====================
    //For each vertex, compute 4 coeffients for s or t directions
    for(unsigned int i_vertex = 0; i_vertex < P_ParametricMesh->P_Vertices->size(); i_vertex++)
    {
        //Position, the first component
        CoeffX.push_back(VerticesPosition[i_vertex][0]);
        CoeffY.push_back(VerticesPosition[i_vertex][1]);

        unsigned int degreeV=P_ParametricMesh->P_Vertices->at(i_vertex).Index_ItsVertices.size();
        if(P_ParametricMesh->P_Vertices->at(i_vertex).Is_interiorVertex)
        {

            if(degreeV!=4)
            {
                //the second TO the forth component
                CoeffX.push_back(0.0);
                CoeffX.push_back(0.0);
                CoeffX.push_back(0.0);

                CoeffY.push_back(0.0);
                CoeffY.push_back(0.0);
                CoeffY.push_back(0.0);
            }
            else//degreeV=4, S1 T1, S2 T2, the interior vertex
            {
                //An interior vertex // map<unsigned int, Coordinate> ItsCellsToCoordinatesMap
                map<unsigned int, Coordinate>::iterator itFristCell=P_ParametricMesh->P_Vertices->at(i_vertex).ItsCellsToCoordinatesMap.begin();
                unsigned int IndexFristCell= (*itFristCell).first;
                //Determine the s-direction and t-direction associated with the given vertex (the i_vertex-th vertex)
                vector<unsigned int> S1, T1;
                double Ssize1, Tsize1;// Suppose that the sizes of cells are the same
                //s-direction and t-direction in the first Cell (store the indices of vertices and the i_vertex vertex's position)
                STDirectionWithGivenVertex(S1, T1, Ssize1, Tsize1, i_vertex, P_ParametricMesh->P_Cells->at(IndexFristCell));

                //Determine the other s-direction vertex's index
                vector<unsigned int> S2, T2;
                //the common cells of the vertices in S1
                set<unsigned int> CommonCellsS1;
                CommonCells(P_ParametricMesh, CommonCellsS1, S1);

//                cout<<"CommonCellsS1.size() = "<<CommonCellsS1.size()<<" (Should be 2)"<<endl;
//                cin.get();

                //check if the i_vertex-th vertex's vertices in these common cells.
                //If there is a vertex that does not belong to these commom cells,
                //then it belongs to the other s-direction.  //set<unsigned int> Index_ItsVertices
                bool key;
                set<unsigned int>::const_iterator itv;
                set<unsigned int> WhatWeHaveVertexIndex;
                for(itv = P_ParametricMesh->P_Vertices->at(i_vertex).Index_ItsVertices.begin();
                    itv != P_ParametricMesh->P_Vertices->at(i_vertex).Index_ItsVertices.end(); itv++)
                {
                    key=Is_InCommonCells(*itv, CommonCellsS1, P_ParametricMesh);

                    if(key==false)//*itv is not in CommonCellsS1
                    {
                        break;
                    }
                }

                if(key==true)// Not find: it is impossible
                {
                    cout<<"MSplineFunctions::MSplineFunctions:: there is no the other s-direction??? it is impossible for an interior vertex!!"<<endl;
                    cin.get();
                }
                else//*itv is the other s-direction
                {
                    S2.clear();
                    //S2
                    if(S1[2]==0)//S1[0]=i_vertex
                    {
                        S2.push_back(*itv);
                        S2.push_back(i_vertex);
                        S2.push_back(1);

                        WhatWeHaveVertexIndex.insert(S1[1]);
                        WhatWeHaveVertexIndex.insert(*itv);
                    }
                    else//S1[2]=1 => S2[1]=i_vertex
                    {
                        S2.push_back(i_vertex);
                        S2.push_back(*itv);
                        S2.push_back(0);

                        WhatWeHaveVertexIndex.insert(S1[0]);
                        WhatWeHaveVertexIndex.insert(*itv);
                    }

                    //T2
                    if(T1[2]==0)//T1[0]=i_vertex
                    {
                        WhatWeHaveVertexIndex.insert(T1[1]);
                    }
                    else//T1[2]=1//T1[1]=i_vertex
                    {
                        WhatWeHaveVertexIndex.insert(T1[0]);
                    }
                    //--------------
                    unsigned int T2Vindex=
                            VertexDoNotInVerticesSet(WhatWeHaveVertexIndex, P_ParametricMesh->P_Vertices->at(i_vertex).Index_ItsVertices);
                    T2.clear();

//                    if(i_vertex>10&&i_vertex<21)
//                    {
//                        cout<<"i_vertex = "<<i_vertex<<endl;
//                        cout<<"T2Vindex = "<<T2Vindex<<endl;
//                        cout<<"WhatWeHaveVertexIndex:"<<endl;
//                            for(set<unsigned int>::const_iterator itc=WhatWeHaveVertexIndex.begin(); itc!=WhatWeHaveVertexIndex.end();
//                                itc++)
//                            {
//                                cout<<*itc<<endl;
//                            }
//                            cout<<endl;

//                            cin.get();

//                            cout<<"Index_ItsVertices:"<<endl;
//                            for(set<unsigned int>::const_iterator itc=P_ParametricMesh->P_Vertices->at(i_vertex).Index_ItsVertices.begin(); itc!=P_ParametricMesh->P_Vertices->at(i_vertex).Index_ItsVertices.end();
//                                itc++)
//                            {
//                                cout<<*itc<<endl;
//                            }
//                            cout<<endl;
//                            cin.get();
//                    }


                    if(T1[2]==0)//T1[0]=i_vertex
                    {
                        T2.push_back(T2Vindex);
                        T2.push_back(i_vertex);
                        T2.push_back(1);//T2[1]=i_vertex
                    }
                    else//T1[1]=i_vertex
                    {
                        T2.push_back(i_vertex);
                        T2.push_back(T2Vindex);
                        T2.push_back(0);//T2[0]=i_vertex
                    }
                }

//                cout<<"Ssize1 = "<<Ssize1<<endl;
//                cout<<"Tsize1 = "<<Tsize1<<endl;
//                cin.get();
//                cin.get();






                //Compute the Hermite data based on S1 T1 S2 T2 Ssize1 Tsize1
                ComputeTheCoeffdata(S1, S2, T1, T2, Ssize1, Tsize1, VerticesPosition, CoeffX, CoeffY);

            }
        }
        else//it is a boundary vertex
            //Remake: for the boundary vertex, this part can be modified. It dependes on different applications
        {
            if(degreeV>3||degreeV<3)
            {
                //the second TO the forth component
                CoeffX.push_back(0.0);
                CoeffX.push_back(0.0);
                CoeffX.push_back(0.0);

                CoeffY.push_back(0.0);
                CoeffY.push_back(0.0);
                CoeffY.push_back(0.0);
            }
            else//degreeV=3
            {
                //a boundary vertex
                map<unsigned int, Coordinate>::iterator itFristCell=P_ParametricMesh->P_Vertices->at(i_vertex).ItsCellsToCoordinatesMap.begin();
                unsigned int IndexFristCell= (*itFristCell).first;
                //Determine the s-direction and t-direction associated with the given vertex (the i_vertex-th vertex)
                vector<unsigned int> S1, T1;
                double Ssize1, Tsize1;// Suppose that the sizes of cells are the same
                //s-direction and t-direction in the first Cell (store the indices of vertices and the i_vertex vertex's position)
                STDirectionWithGivenVertex(S1, T1, Ssize1, Tsize1, i_vertex, P_ParametricMesh->P_Cells->at(IndexFristCell));
                //-------------------------------------------------

                //Check that S1 is an interior edge or a boundary edge
                vector<unsigned int> S2, T2;
                //the common cells of the vertices in S1
                set<unsigned int> CommonCellsS1;
                CommonCells(P_ParametricMesh, CommonCellsS1, S1);

                set<unsigned int> WhatWeHaveVertexIndex;
                WhatWeHaveVertexIndex.insert(S1[0]);
                WhatWeHaveVertexIndex.insert(S1[1]);
                WhatWeHaveVertexIndex.insert(T1[0]);
                WhatWeHaveVertexIndex.insert(T1[1]);
                unsigned int TheOtherVindex=
                        VertexDoNotInVerticesSet(WhatWeHaveVertexIndex, P_ParametricMesh->P_Vertices->at(i_vertex).Index_ItsVertices);

                if(CommonCellsS1.size()==2)//S1 is an interior edge
                {
                    S2.clear();//No S2 direction
                    //T2==Deside
                    if(T1[2]==0)//T1[0]=i_vertex
                    {
                        T2.clear();
                        T2.push_back(TheOtherVindex);
                        T2.push_back(i_vertex);
                        T2.push_back(1);
                    }
                    else//T1[2]==1// T1[1]=i_vertex
                    {
                        T2.clear();
                        T2.push_back(i_vertex);
                        T2.push_back(TheOtherVindex);
                        T2.push_back(0);
                    }
                }
                else//CommonCellsS1.size()==1=>S1 is a boundary edge, otherwise, there is a mistake
                {
                    if(CommonCellsS1.size()==1)//S1 is a boundary edge
                    {
                        T2.clear();// No T2 direction (T1 is an interior edge)
                        //S2==Deside
                        if(S1[2]==0)//S1[0]=i_vertex
                        {
                            S2.clear();
                            S2.push_back(TheOtherVindex);
                            S2.push_back(i_vertex);
                            S2.push_back(1);
                        }
                        else//S1[2]==1//S1[1]=i_vertex
                        {
                            S2.clear();
                            S2.push_back(i_vertex);
                            S2.push_back(TheOtherVindex);
                            S2.push_back(0);
                        }
                    }
                    else
                    {
                        cout<<"MSpineFunctions.cpp::MSplineFunctions:: the i_vertex vertex is a boundary vertex, S1 edge's CommonCellsS1's size is not in the right way"<<endl;
                        cin.get();
                    }
                }
                //Compute the Hermite data based on S1 T1 S2 T2 Ssize1 Tsize1

                ComputeTheCoeffdata(S1, S2, T1, T2, Ssize1, Tsize1, VerticesPosition, CoeffX, CoeffY);
            }
        }
    }
    Coeff.push_back(CoeffX);
    Coeff.push_back(CoeffY);

    MSplineFunctions(Coeff, *P_ParametricMesh);
}
//    Coeff.clear();
//    vector<double> CoeffX, CoeffY;
//    //=====================
//    //For each vertex, compute 4 coeffients for s or t directions
//    for(unsigned int i_vertex = 0; i_vertex < P_ParametricMesh->P_Vertices->size(); i_vertex++)
//    {
//        //Position, the first component
//        CoeffX.push_back(VerticesPosition[i_vertex][0]);
//        CoeffY.push_back(VerticesPosition[i_vertex][1]);

//        unsigned int degreeV=P_ParametricMesh->P_Vertices->at(i_vertex).Index_ItsVertices.size();
//        if(P_ParametricMesh->P_Vertices->at(i_vertex).Is_interiorVertex)
//        {

//            if(degreeV!=4)
//            {
//                //the second TO the forth component
//                CoeffX.push_back(0.0);
//                CoeffX.push_back(0.0);
//                CoeffX.push_back(0.0);

//                CoeffY.push_back(0.0);
//                CoeffY.push_back(0.0);
//                CoeffY.push_back(0.0);
//            }
//            else//degreeV=4, S1 T1, S2 T2, the interior vertex
//            {
//                //1----The vertices around the i_vertex-th vertex in some an order
//                vector<unsigned int> VertexAroundThisVertex;
//                P_ParametricMesh->OneNeighbourhoodSequenceAroundCenterVertex(VertexAroundThisVertex ,i_vertex);
//                //2----In the first cell, decide which direction is s-direction
//                       //and which one is t-direction
//                //VertexAroundThisVertex.size()=4
//                unsigned int TheFirstCellIndex=P_ParametricMesh->P_Vertices->at(i_vertex).ItsCellsToCoordinatesMap.begin()->first;
//                unsigned int is=0;
//                unsigned int it=0;
//                for(is=0; is<VertexAroundThisVertex.size(); is++)
//                {
//                    int Flag = P_ParametricMesh->P_Cells->at(TheFirstCellIndex).TheLocalDirection(VertexAroundThisVertex[is], i_vertex);
//                    if(Flag==1||Flag==-1)//s-direction (1), -s -direction (-1); t-direction (2), -t -direction (-2)
//                    {//is ==>it
//                        int Flag2=P_ParametricMesh->P_Cells->at(TheFirstCellIndex).TheLocalDirection(VertexAroundThisVertex[(is+1)%VertexAroundThisVertex.size()],i_vertex);
//                        if(Flag2==2||Flag2==-2)
//                        {
//                            it=is+1;
//                            break;
//                        }
//                        else
//                        {
//                            int Flag3 = P_ParametricMesh->P_Cells->at(TheFirstCellIndex).TheLocalDirection(VertexAroundThisVertex[(is-1)%VertexAroundThisVertex.size()], i_vertex);
//                            if(Flag3==2||Flag3==-2)
//                            {
//                                it=is-1;
//                                break;
//                            }
//                            else
//                            {
//                                cout<<"Error---1::MSplineFunctions::MSplineFunctions(const vector<vector<double> > & VerticesPosition, const Mesh * P_ParametricMesh, vector<vector <double> >& Coeff)"<<endl;
//                                cin.get();
//                            }
//                        }
//                    }
//                }
//                //====
//                //is and it have been found out
//                unsigned int S1, S2;
//                unsigned int T1, T2;
//                S1=VertexAroundThisVertex[is];
//                T1=VertexAroundThisVertex[it];
//                S2=VertexAroundThisVertex[(is+2)%VertexAroundThisVertex.size()];
//                T2=VertexAroundThisVertex[(it+2)%VertexAroundThisVertex.size()];
//                double S_size=P_ParametricMesh->P_Cells->at(TheFirstCellIndex).CellSize[1];
//                S_size=S_size-P_ParametricMesh->P_Cells->at(TheFirstCellIndex).CellSize[0];
//                double T_size=P_ParametricMesh->P_Cells->at(TheFirstCellIndex).CellSize[3];
//                T_size=T_size-P_ParametricMesh->P_Cells->at(TheFirstCellIndex).CellSize[2];
//                //
//                //3----compute the coeff with the distance between the vertices
//                double CoeffXs_i, CoeffYs_i, CoeffXt_i, CoeffYt_i;
//                CoeffXs_i=0.5*(VerticesPosition[S1][0]-VerticesPosition[S2][0])/S_size;
//                CoeffYs_i=0.5*(VerticesPosition[S1][1]-VerticesPosition[S2][1])/S_size;
//                CoeffXt_i=0.5*(VerticesPosition[T1][0]-VerticesPosition[T2][0])/T_size;
//                CoeffYt_i=0.5*(VerticesPosition[T1][1]-VerticesPosition[T2][1])/T_size;

//                //CoeffX
//                CoeffX.push_back(CoeffXs_i);
//                CoeffX.push_back(CoeffXt_i);
//                CoeffX.push_back(0.0);

//                //CoeffY
//                CoeffY.push_back(CoeffYs_i);
//                CoeffY.push_back(CoeffYt_i);
//                CoeffY.push_back(0.0);
//            }
//        }
//        else//it is a boundary vertex
//            //Remake: for the boundary vertex, this part can be modified. It dependes on different applications
//        {
//            if(degreeV>3||degreeV<3)
//            {
//                //the second TO the forth component
//                CoeffX.push_back(0.0);
//                CoeffX.push_back(0.0);
//                CoeffX.push_back(0.0);

//                CoeffY.push_back(0.0);
//                CoeffY.push_back(0.0);
//                CoeffY.push_back(0.0);
//            }
//            else//degreeV=3
//            {
//                //1----The vertices around the i_vertex-th vertex in some an order
//                vector<unsigned int> VertexAroundThisVertex;
//                P_ParametricMesh->OneNeighbourhoodSequenceAroundCenterVertex(VertexAroundThisVertex ,i_vertex);
//                //2----In the first cell, decide which direction is s-direction
//                       //and which one is t-direction
//                //VertexAroundThisVertex.size()=4
//                unsigned int TheFirstCellIndex=P_ParametricMesh->P_Vertices->at(i_vertex).ItsCellsToCoordinatesMap.begin()->first;
//                unsigned int is=0;
//                unsigned int it=0;
//                double DsX, DsY, DtX, DtY;
//                double S_size, T_size;
//                S_size=P_ParametricMesh->P_Cells->at(TheFirstCellIndex).CellSize[1];
//                S_size=S_size-P_ParametricMesh->P_Cells->at(TheFirstCellIndex).CellSize[0];
//                T_size=P_ParametricMesh->P_Cells->at(TheFirstCellIndex).CellSize[3];
//                T_size=T_size-P_ParametricMesh->P_Cells->at(TheFirstCellIndex).CellSize[2];
//                //VertexAroundThisVertex[0] is a boundary vertex
//                //Decide is it
//                int Flag=P_ParametricMesh->P_Cells->at(TheFirstCellIndex).TheLocalDirection(VertexAroundThisVertex[0], i_vertex);


//                if(Flag==1||Flag==-1)
//                {
//                    is=0;
//                    unsigned int S1=VertexAroundThisVertex[is];
//                    unsigned int S2=VertexAroundThisVertex[(is+2)];
//                    unsigned int T1=VertexAroundThisVertex[(is+1)];
//                    DsX=0.5*(VerticesPosition[S1][0]-VerticesPosition[S2][0])/S_size;
//                    DtX=0.5*(VerticesPosition[S1][1]-VerticesPosition[S2][1])/T_size;
//                    //
//                    DsY=(VerticesPosition[T1][0]-VerticesPosition[i_vertex][0])/S_size;
//                    DtY=(VerticesPosition[T1][1]-VerticesPosition[i_vertex][1])/T_size;
//                    //
//                    //break;
//                }
//                if(Flag==2||Flag==-2)
//                {
//                    it=0;
//                    unsigned int T1=VertexAroundThisVertex[it];
//                    unsigned int T2=VertexAroundThisVertex[it+2];
//                    unsigned int S1=VertexAroundThisVertex[it+1];
//                    DsX=(VerticesPosition[S1][0]-VerticesPosition[i_vertex][0])/S_size;
//                    DtX=(VerticesPosition[S1][1]-VerticesPosition[i_vertex][1])/T_size;
//                    //
//                    DsY=0.5*(VerticesPosition[T1][0]-VerticesPosition[T2][0])/S_size;
//                    DtY=0.5*(VerticesPosition[T1][1]-VerticesPosition[T2][1])/T_size;
//                    //
//                    //break;
//                }
//                //CoeffX
//                CoeffX.push_back(DsX);
//                CoeffX.push_back(DtX);
//                CoeffX.push_back(0.0);

//                //CoeffY
//                CoeffY.push_back(DsY);
//                CoeffY.push_back(DtY);
//                CoeffY.push_back(0.0);

//            }
//        }
//    }

//    Coeff.push_back(CoeffX);
//    Coeff.push_back(CoeffY);

//    MSplineFunctions(Coeff, *P_ParametricMesh);
//}

void MSplineFunctions::ModifyMSplineFunctions(const vector<vector<double> > &Coeff, unsigned int Index_Cell, const Mesh & CurrentMesh)
{
    if(Coeff.size()==P_MSplineFunctions.size())
    {
        for(unsigned int i=0; i<Coeff.size(); i++)
        {
            P_MSplineFunctions[i]->ModifyMSplineFunction(Coeff[i], Index_Cell, CurrentMesh);;
        }
    }
    else
    {
        cout<<"MSplineFunctions::ModifyMSplineFunctions::Coeff.size()!=P_MSplineFunctions.size()"<<endl;
        cin.get();
    }
}

bool MSplineFunctions::NormalVectorAtAVertexOverOneCell(const unsigned int &indexCell, const Coordinate &Coor,
                                                        vector<double> &Ni, Mesh * P_CMesh)
{
    Ni.clear();

    vector<vector<double> >S, T;
    Coordinate * PC=new Coordinate(Coor.xy[0], Coor.xy[1]);
    vector<Coordinate*> Coors;
    Coors.push_back(PC);
    this->Evaluation(1, 0, Coors, indexCell, S, *P_CMesh);
    this->Evaluation(0, 1, Coors, indexCell, T, *P_CMesh);

    vector<double> N1, N2;
    for(unsigned int i=0; i<S.size(); i++)
    {
        N1.push_back(S[i][0]);
        N2.push_back(T[i][0]);
    }

    delete PC;
    PC=NULL;
    //Ni = N1 X N2
   bool Key=VectorCrossProduct(Ni, N1, N2);

   return Key;

}

bool MSplineFunctions::NormalVectorAtAIrregularVertexOverOneCell(const unsigned int &indexCell, const Coordinate &Coor,
                                                                      vector<double> &Ni, Mesh * P_CMesh)
{

    Ni.clear();

    vector<vector<double> > S, T;
    Coordinate *PC= new Coordinate(Coor.xy[0], Coor.xy[1]);
    vector<Coordinate*> Coors;
    Coors.push_back(PC);

    //At an irregular vertex, dsP=0, dtP=0.
    this->Evaluation(2, 0, Coors, indexCell, S, *P_CMesh);
    this->Evaluation(0, 2, Coors, indexCell, T, *P_CMesh);

    vector<double> N1, N2;
    for(unsigned int i=0; i<S.size(); i++)
    {
        N1.push_back(S[i][0]);
        N2.push_back(T[i][0]);
    }

    delete PC;
    PC=NULL;
    //Ni = N1 X N2
   bool Key=VectorCrossProduct(Ni, N1, N2);

   double length=VectorDotProduct(Ni, Ni);
   VectorScalarProduct(1.0/length, Ni);

   return Key;

}


void MSplineFunctions::NormalVectorAtAVertex(const unsigned int & IrregularCenterIndex, Mesh * P_CMesh, vector<double> &initialNormalVector)
{
    initialNormalVector.clear();

    unsigned int dim = this->P_MSplineFunctions.size();
    if(dim==2)
    {
        initialNormalVector.push_back(0.0);
        initialNormalVector.push_back(0.0);
        initialNormalVector.push_back(1.0);
    }
    if(dim>=3)
    {
        initialNormalVector.push_back(0.0);
        initialNormalVector.push_back(0.0);
        initialNormalVector.push_back(0.0);

        unsigned int countNum=0;
        for(map<unsigned int, Coordinate>::const_iterator
            itmap=P_CMesh->P_Vertices->at(IrregularCenterIndex).ItsCellsToCoordinatesMap.begin();
            itmap!=P_CMesh->P_Vertices->at(IrregularCenterIndex).ItsCellsToCoordinatesMap.end();
            itmap++)
        {
            //

            vector<double> Ni;
            bool Key=this->NormalVectorAtAVertexOverOneCell(itmap->first, itmap->second, Ni, P_CMesh);

            if(Key)
            {
                VectorSum(initialNormalVector, Ni);//initialNormalVector=initialNormalVector+Ni
            }

            countNum=countNum+1;
        }

        if(countNum)
        {
            vector<double> initialNormalVector;
            VectorScalarProduct(1.0/(1.0*countNum),initialNormalVector);
        }
        else
        {
            initialNormalVector[2]=1.0;
        }

    }
}


void MSplineFunctions::NormalVectorAtAIrregularVertex(const unsigned int & IrregularCenterIndex, Mesh * P_CMesh, vector<double> &initialNormalVector)
{
    initialNormalVector.clear();

    unsigned int dim = this->P_MSplineFunctions.size();
    if(dim==2)
    {
        initialNormalVector.push_back(0.0);
        initialNormalVector.push_back(0.0);
        initialNormalVector.push_back(1.0);
    }
    if(dim>=3)
    {
        initialNormalVector.push_back(0.0);
        initialNormalVector.push_back(0.0);
        initialNormalVector.push_back(0.0);

        unsigned int countNum=0;
        for(map<unsigned int, Coordinate>::const_iterator
            itmap=P_CMesh->P_Vertices->at(IrregularCenterIndex).ItsCellsToCoordinatesMap.begin();
            itmap!=P_CMesh->P_Vertices->at(IrregularCenterIndex).ItsCellsToCoordinatesMap.end();
            itmap++)
        {
            //

            vector<double> Ni;
            bool Key=this->NormalVectorAtAIrregularVertexOverOneCell(itmap->first, itmap->second, Ni, P_CMesh);

            if(Key)
            {
                VectorSum(initialNormalVector, Ni);//initialNormalVector=initialNormalVector+Ni
            }

            countNum=countNum+1;
        }

        if(countNum)
        {
            vector<double> initialNormalVector;
            VectorScalarProduct(1.0/(1.0*countNum),initialNormalVector);
        }
        else
        {
            initialNormalVector[2]=1.0;
        }

    }
}

/*From V(Index1) to V(Index2)*/
void MSplineFunctions::TheVectorFromOneVertexToTheOther(const unsigned int &Index1, const unsigned int &Index2, Mesh *P_CMesh, vector<double>& TDirection)
{

    TDirection.clear();
     unsigned int CommonCellIndex;
     set<unsigned int> CommonCellIndexSet;
     P_CMesh->CommonCellsIndexOfAnEdges(CommonCellIndexSet, Index1, Index2);
     CommonCellIndex=*CommonCellIndexSet.begin();

     //
     vector<double> CoorVertex1;//The coordinates of the Index1-th vertex
     P_CMesh->P_Cells->at(CommonCellIndex).CoordinateOfACornerVertex(Index1, CoorVertex1);
     Coordinate * CoorXY1=new Coordinate(CoorVertex1[0], CoorVertex1[1]);
     vector<Coordinate*> CoorXY1Vector;
     CoorXY1Vector.push_back(CoorXY1);

     vector<double> CoorVertex2;//The coordinates of the Index2-th vertex
     P_CMesh->P_Cells->at(CommonCellIndex).CoordinateOfACornerVertex(Index2, CoorVertex2);
     Coordinate * CoorXY2=new Coordinate(CoorVertex2[0], CoorVertex2[1]);
     vector<Coordinate*> CoorXY2Vector;
     CoorXY2Vector.push_back(CoorXY2);

     vector<vector<double> > Values1, Values2;
     this->Evaluation(0, 0, CoorXY1Vector, CommonCellIndex, Values1, *P_CMesh);
     this->Evaluation(0, 0, CoorXY2Vector, CommonCellIndex, Values2, *P_CMesh);

     //TDirection=Values2-Values1
     switch (this->P_MSplineFunctions.size())
     {
     case 3:
         TDirection.push_back(Values2[0][0]-Values1[0][0]);
         TDirection.push_back(Values2[1][0]-Values1[1][0]);
         TDirection.push_back(Values2[2][0]-Values1[2][0]);

         TDirection.push_back(0);//Order=0
         break;
     case 2:
         TDirection.push_back(Values2[0][0]-Values1[0][0]);
         TDirection.push_back(Values2[1][0]-Values1[1][0]);
         TDirection.push_back(0.0);

         TDirection.push_back(0);//Order=0
         break;
     case 1:
         TDirection.push_back(Values2[0][0]-Values1[0][0]);
         TDirection.push_back(0.0);
         TDirection.push_back(0.0);
         TDirection.push_back(0);//Order=0
         break;
     default:
         TDirection.push_back(Values2[0][0]-Values1[0][0]);
         TDirection.push_back(Values2[1][0]-Values1[1][0]);
         TDirection.push_back(Values2[2][0]-Values1[2][0]);
         TDirection.push_back(0);//Order=0
         break;
     }


     delete CoorXY1;
     delete CoorXY2;
     CoorXY1=NULL;
     CoorXY2=NULL;


}

/*initialT=[x0, y0, z0, order]*/
bool MSplineFunctions::TangentDirectionFromTheCenterVertexToTheAdjacentVertex
(unsigned int IndexV, unsigned int CenterIndexV, Mesh * P_CMesh, vector<double> &initialT, unsigned int &CommonCellIndex)
{
    bool Has_TangentVector=false;

    initialT.clear();
    //1.Find the common cell of the IndexV and the CenterIndexV-th vertices,the coordinate of the CenterIndexV over this common cell
    //and return this edge is along s-direction or t-direction
    set<unsigned int> CommonCellIndexSet;
    P_CMesh->CommonCellsIndexOfAnEdges(CommonCellIndexSet, IndexV, CenterIndexV);

    CommonCellIndex=*CommonCellIndexSet.begin();// Return

   //Compute the initial value for initialT
   this->TheVectorFromOneVertexToTheOther(CenterIndexV, IndexV, P_CMesh, initialT);


   if(this->P_MSplineFunctions.size()>2)
   {
       //int TheLocalDirection(const unsigned int &IndexV1, const unsigned int &IndexV2);
      int Direction_Flag=P_CMesh->P_Cells->at(CommonCellIndex).TheLocalDirection(CenterIndexV, IndexV);
      //Direction_Flag=  s-direction (1), -s -direction (-1); t-direction (2), -t -direction (-2)

      vector<double> CoorCenterVertex;//The coordinates of the CenterIndexV-th vertex
      P_CMesh->P_Cells->at(CommonCellIndex).CoordinateOfACornerVertex(CenterIndexV, CoorCenterVertex);
      Coordinate * CoorXY=new Coordinate(CoorCenterVertex[0], CoorCenterVertex[1]);

      vector<Coordinate*> CoorXYVector;
      CoorXYVector.push_back(CoorXY);

      vector<vector<double> > ValuesXYZ;//ValuesXYZ[0][0],ValuesXYZ[1][0], ValuesXYZ[2][0]

      if(Direction_Flag==1||Direction_Flag==-1)//Derivative along the s direction
      {
          this->Evaluation(1,0, CoorXYVector, CommonCellIndex, ValuesXYZ, *P_CMesh);
          double length=(ValuesXYZ[0][0])*(ValuesXYZ[0][0])+(ValuesXYZ[1][0])*(ValuesXYZ[1][0])+(ValuesXYZ[2][0])*(ValuesXYZ[2][0]);

          if(length>1.0e-4)
          {
              initialT.clear();

              initialT.push_back(ValuesXYZ[0][0]);
              initialT.push_back(ValuesXYZ[1][0]);
              initialT.push_back(ValuesXYZ[2][0]);

              initialT.push_back(1);//Order=1

              Has_TangentVector=true;

          }
          else
          {
              ValuesXYZ.clear();

              this->Evaluation(2,0, CoorXYVector, CommonCellIndex, ValuesXYZ, *P_CMesh);
              length=(ValuesXYZ[0][0])*(ValuesXYZ[0][0])+(ValuesXYZ[1][0])*(ValuesXYZ[1][0])+(ValuesXYZ[2][0])*(ValuesXYZ[2][0]);
              //
              if(length>1.0e-4)
              {
                  initialT.clear();

                  initialT.push_back(ValuesXYZ[0][0]);
                  initialT.push_back(ValuesXYZ[1][0]);
                  initialT.push_back(ValuesXYZ[2][0]);

                  initialT.push_back(2);//Order=2

                  Has_TangentVector=true;
              }

          }

          if(Direction_Flag==-1)
          {
              for(unsigned int i=0; i<initialT.size()-1; i++)
              {
                  initialT[i]=-1.0*initialT[i];
              }
          }
      }
      else//Derivative along the t direction (Direction_Flag=2 || Direction_Flag=-2)
      {
          this->Evaluation(0,1, CoorXYVector, CommonCellIndex, ValuesXYZ, *P_CMesh);
          double length=(ValuesXYZ[0][0])*(ValuesXYZ[0][0])+(ValuesXYZ[1][0])*(ValuesXYZ[1][0])+(ValuesXYZ[2][0])*(ValuesXYZ[2][0]);

          if(length>1.0e-4)
          {
              initialT.clear();

              initialT.push_back(ValuesXYZ[0][0]);
              initialT.push_back(ValuesXYZ[1][0]);
              initialT.push_back(ValuesXYZ[2][0]);

              initialT.push_back(1);//Order=1

              Has_TangentVector=true;

          }
          else
          {
              ValuesXYZ.clear();

              this->Evaluation(0,2, CoorXYVector, CommonCellIndex, ValuesXYZ, *P_CMesh);
              length=(ValuesXYZ[0][0])*(ValuesXYZ[0][0])+(ValuesXYZ[1][0])*(ValuesXYZ[1][0])+(ValuesXYZ[2][0])*(ValuesXYZ[2][0]);
              //
              if(length>1.0e-4)
              {
                  initialT.clear();

                  initialT.push_back(ValuesXYZ[0][0]);
                  initialT.push_back(ValuesXYZ[1][0]);
                  initialT.push_back(ValuesXYZ[2][0]);

                  initialT.push_back(2);//Order=2

                  Has_TangentVector=true;
              }

          }

          if(Direction_Flag==-2)
          {
              for(unsigned int i=0; i<initialT.size()-1; i++)
              {
                  initialT[i]=-1.0*initialT[i];
              }
          }
      }

      delete CoorXY;
      CoorXY=NULL;
   }



   return Has_TangentVector;

}



/* At first, we arrange the vertices around the center vertex with the order (here, the type of center vertex is classified into two)
 * return: TangentVectors[i] is the i-th tangent vector from the center vertex to the i-th vertex;
      IndexTangentVectors[i] is the vector of indices of coeffs at the i-th vertex connected to this center vertex
      and these indices with the coeffs at the center vertex decide TangentVectors[i]
*/
void MSplineFunctions::TangentVectorsFromAVertex(vector<vector<double> >& TangentVectors, vector<vector<unsigned int> > & IndexTangentVectors, const unsigned int CenterVIndex, Mesh* P_CMesh)
{
    cout<<"Begin: MSplineFunctions::TangentVectorsFromAVertex()"<<endl;
    TangentVectors.clear();
    IndexTangentVectors.clear();

    //cout<<"Compute the one neighbourhood of the center vertex"<<endl;
    /*the one neighbourhood of the center vertex*/
    vector<unsigned int> VertexIndicesSeqAroundCenterVertex;
    P_CMesh->OneNeighbourhoodSequenceAroundCenterVertex(VertexIndicesSeqAroundCenterVertex, CenterVIndex);
//    cout<<"VertexIndicesSeqAroundCenterVertex.size() = "<<VertexIndicesSeqAroundCenterVertex.size()<<endl;

//    cout<<"VertexIndicesSeqAroundCenterVertex=["<<endl;
//    for(unsigned i_test=0; i_test<VertexIndicesSeqAroundCenterVertex.size(); i_test++)
//    {
//        cout<<VertexIndicesSeqAroundCenterVertex[i_test]<<" ";
//    }
//    cout<<"]"<<endl;


//    cout<<"Compute the tangent vectors one by one"<<endl;
    /*compute the Tangent Vectors one by one*/
    for(unsigned int i=0; i<VertexIndicesSeqAroundCenterVertex.size(); i++)
    {
//        cout<<"begin to compute the tangent vector based on the "<<i<<"-th vertex which around the center vertex"<<endl;

        vector<double> Ti;
        unsigned int IndexV=VertexIndicesSeqAroundCenterVertex[i];
        unsigned int CommonCellIndex;

        /*Push_back the elements of TangentVectors*/
        this->TangentDirectionFromTheCenterVertexToTheAdjacentVertex(IndexV, CenterVIndex, P_CMesh, Ti, CommonCellIndex);

//        cin.get();
//        cout<<"The tangent vector=[ ";
//        for(unsigned int i_test=0; i_test<Ti.size(); i_test++)
//        {
//            cout<<Ti[i_test]<<" ";
//        }
//        cout<<"]"<<endl;
//        cin.get();

        TangentVectors.push_back(Ti);

        /*Push_back the elements of IndexTangentVectors*/
        vector<unsigned int> IndexTi;
        IndexTi.push_back(CenterVIndex);
        IndexTi.push_back(IndexV);
        IndexTi.push_back(CommonCellIndex);
        IndexTangentVectors.push_back(IndexTi);
    }
    //=======
}




void MSplineFunctions::ModifyTangentVectorsAtAVertexBasedonGivenNormalVector(const vector<double> &NormalVector,
                                  vector<vector<double> >& TangentVectors)
{
    cout<<"Begin::MSplineFunctions::ModifyTangentVectorsAtAVertexBasedonGivenNormalVector()"<<endl;
    //Project TangentVectors based on the NormalVectors
//    cout<<"Normalize the Normal Vector: NormalizedNormalVector=["<<endl;
    vector<double> NormalizedNormalVector;
    double length=VectorDotProduct(NormalVector, NormalVector);
    length=sqrt(length);

    for(unsigned int i=0; i<NormalVector.size(); i++)
    {
        NormalizedNormalVector.push_back(NormalVector[i]/length);
        //cout<<NormalizedNormalVector[i]<<" ";
    }
//    cout<<"]"<<endl;
    //Projection

//    cout<<"Projection:"<<endl;
    for(unsigned int i=0; i<TangentVectors.size(); i++)
    {

        vector<double> TangentVectors_i;
        TangentVectors_i.push_back(TangentVectors[i][0]);//TangentVectors.size()=4: TangentVectors[3] describes the order of derivative.
        TangentVectors_i.push_back(TangentVectors[i][1]);
        TangentVectors_i.push_back(TangentVectors[i][2]);
        double dotproduct=VectorDotProduct(TangentVectors_i, NormalizedNormalVector);

        //TangentVectors[i]-dotproduct*NormalizedNormalVector
        vector<double> LambdaN;//LambdaN=-1.0*dotproduct NormalizedNormalvector
        //VectorScalarProduct(LambdaN, -1.0*dotproduct, NormalizedNormalVector);
        for(unsigned int iv=0; iv<NormalizedNormalVector.size(); iv++)
        {
            LambdaN.push_back(-1.0*dotproduct*NormalizedNormalVector[iv]);
        }

        VectorSum(TangentVectors_i, LambdaN);//Now TangentVectors[i] has been modified
        TangentVectors[i][0]=TangentVectors_i[0];
        TangentVectors[i][1]=TangentVectors_i[1];
        TangentVectors[i][2]=TangentVectors_i[2];
        //----
        TangentVectors[i].resize(3);
    }

//   cin.get();

    cout<<"Choose the sign:"<<endl;
    //Choose the sign for comparing
    int ChosenSign(0);
    unsigned int NumPositiveSign(0), NumNegativeSign(0);
    double mixPro;
    for(unsigned int i=0; i<TangentVectors.size()-1; i++)
    {
        mixPro=VectorMixProduct(TangentVectors[i], TangentVectors[i+1], NormalizedNormalVector);
        cout<<"mixproduct["<<i<<"]="<<mixPro<<endl;

        if(mixPro<0)
        {
            NumNegativeSign=NumNegativeSign+1;
        }
        else
        {
            NumPositiveSign=NumPositiveSign+1;
        }
    }
    mixPro=VectorMixProduct(TangentVectors[TangentVectors.size()-1], TangentVectors[0], NormalizedNormalVector);
    cout<<"mixproduct["<<TangentVectors.size()<<"]="<<mixPro<<endl;
    if(mixPro<0)
    {
        NumNegativeSign=NumNegativeSign+1;
    }
    else
    {
        NumPositiveSign=NumPositiveSign+1;
    }
    if(NumNegativeSign>NumPositiveSign)
    {
        ChosenSign=-1;
    }
    else
    {
        ChosenSign=1;
    }

    cout<<"ChosenSign = "<<ChosenSign<<endl;

//    cin.get();

//    //Switch the vectors in order to get the same sign
//    unsigned int i=0;
//    unsigned int IterationNum=0;
//    cout<<"Swap the tangent vectors:"<<endl;
//    while(i<TangentVectors.size())
//    {
//        mixPro=VectorMixProduct(TangentVectors[i%TangentVectors.size()], TangentVectors[(i+1)%TangentVectors.size()], NormalizedNormalVector);
//        if(mixPro*ChosenSign<0)
//        {
//            swap(TangentVectors[i%TangentVectors.size()], TangentVectors[(i+1)%TangentVectors.size()]);
//            i=0;
//        }
//        else
//        {
//            i=i+1;
//        }

//        //======Cout the iteration number===========
//        unsigned K=0;
//        IterationNum=IterationNum+1;
//        if(IterationNum%(2*TangentVectors.size())==0)
//        {
//            cout<<"The IterationNum="<<IterationNum<<endl;
//            K=K+1;
//            if(K>=4)
//            {
//                cout<<"Quit this loop with \"break\", where, "<<endl;
//                cout<<"K="<<K<<endl;
//                break;
//            }
//        }
//        //======--------------------------==========
//    }

    cout<<"End::MSplineFunctions::ModifyTangentVectorsAtAVertexBasedonGivenNormalVector()"<<endl;
}


/*
 * Here based on the TangentVectors and IndexTangentVectors, update the Coeff and this MSplineFunctions
 *
 * The structure of TangentVectors: TangentVectors[i] is the i-th tangent vector from the CenterVIndex-th vertex to the vertex around this center vertex
 *
 * The structure of IndexTangentVectors: IndexTangentVectors[i]=[CenterVIndex, IndexVertexAroundCenterVertex, CommomCellIndex]
 * and this information is accroding to TangentVectors[i]
 * Here, the positions of vertices in Coeff are new information (after moving the index-th control point).
 * For the Coeff: we should modify the derivaters coeff of this mspline surface
*/
static void CoeffSetForKeepTangentAtIrregularVertex(unsigned int &index,unsigned int IndexV,
                       unsigned int IrregularCenterIndex,  vector<double> T0, vector<double> & Coeffindex,
                       const Mesh *P_CMesh, unsigned CommonCellIndex, const vector<vector<double> > & Coeff)
{

    BasesVertex *P_BIndexV=new BasesVertex(IndexV, CommonCellIndex, *P_CMesh);
    BasesVertex *P_BIrregularV=new BasesVertex(IrregularCenterIndex, CommonCellIndex, *P_CMesh);

    vector<unsigned int> BasisIndices;
    vector<double> D2Values;

    int DirectionFlag=P_CMesh->P_Cells->at(CommonCellIndex).TheLocalDirection(IrregularCenterIndex, IndexV);

//    //unsigned int VIPIndex;
//    if(DirectionFlag<0)
//    {
//        VectorScalarProduct(-1.0, T0);
//    }

    vector<double> CoordinateIrregularV;
    P_CMesh->P_Cells->at(CommonCellIndex).CoordinateOfACornerVertex(IrregularCenterIndex, CoordinateIrregularV);
    //s-direction (1), -s -direction (-1); t-direction (2), -t -direction (-2)
    if(DirectionFlag==1||DirectionFlag==-1)//s-Direction
    {
        for(unsigned int i=0; i<P_BIndexV->P_Base_Vertex.size(); i++)
        {
            //BasisIndices
            BasisIndices.push_back(P_BIndexV->BasisIndex(i));
            //D2Values
            double v;
            P_BIndexV->P_Base_Vertex.at(i)->Evaluation(0, 2, 0, CoordinateIrregularV, v, P_CMesh->P_Cells->at(CommonCellIndex).CellSize);
            D2Values.push_back(v);
            if(i&&v)
            {
                index=P_BIndexV->BasisIndex(i);
            }
        }
        for(unsigned int i=0; i<P_BIrregularV->P_Base_Vertex.size(); i++)
        {
            //BasisIndices
            BasisIndices.push_back(P_BIrregularV->BasisIndex(i));
            //D2Values
            double v;
            P_BIrregularV->P_Base_Vertex.at(i)->Evaluation(0, 2, 0, CoordinateIrregularV, v, P_CMesh->P_Cells->at(CommonCellIndex).CellSize);
            D2Values.push_back(v);
        }
    }
    else//t-Direction
    {
        for(unsigned int i=0; i<P_BIndexV->P_Base_Vertex.size(); i++)
        {
            //BasisIndices
            BasisIndices.push_back(P_BIndexV->BasisIndex(i));
            //D2Values
            double v;
            P_BIndexV->P_Base_Vertex.at(i)->Evaluation(0, 0, 2, CoordinateIrregularV, v, P_CMesh->P_Cells->at(CommonCellIndex).CellSize);
            D2Values.push_back(v);
            if(i&&v)
            {
                index=P_BIndexV->BasisIndex(i);
            }
        }
        for(unsigned int i=0; i<P_BIrregularV->P_Base_Vertex.size(); i++)
        {
            //BasisIndices
            BasisIndices.push_back(P_BIrregularV->BasisIndex(i));
            //D2Values
            double v;
            P_BIrregularV->P_Base_Vertex.at(i)->Evaluation(0, 0, 2, CoordinateIrregularV, v, P_CMesh->P_Cells->at(CommonCellIndex).CellSize);
            D2Values.push_back(v);
        }
    }
    delete P_BIndexV; P_BIndexV=NULL;
    delete P_BIrregularV; P_BIrregularV=NULL;

    //====Compute the ceff[index]
    Coeffindex.clear();
    Coeffindex.push_back(T0[0]);
    Coeffindex.push_back(T0[1]);
    Coeffindex.push_back(T0[2]);

    unsigned int MarkIndex;
    for(unsigned int i=0; i<BasisIndices.size(); i++)
    {
        if(BasisIndices[i]!=index)
        {
            //Coeffindex=Coeffindex-Coeff[BasisIndices[i]]*D2Values[i]
            Coeffindex[0]=Coeffindex[0]-Coeff[0][BasisIndices[i]]*D2Values[i];
            Coeffindex[1]=Coeffindex[1]-Coeff[1][BasisIndices[i]]*D2Values[i];
            Coeffindex[2]=Coeffindex[2]-Coeff[2][BasisIndices[i]]*D2Values[i];
        }
        else
        {
            MarkIndex=i;
        }
    }
    VectorScalarProduct(1.0/(D2Values[MarkIndex]), Coeffindex);
}
void MSplineFunctions::UpdataMSplineFunctionsAndCoeffAroundAnIrregularVertexByItsTangentVectors(vector<vector<double> > &Coeff,
                     const vector<vector<double> > &TangentVectors, const vector<vector<unsigned int> > &IndexTangentVectors,
                     const unsigned int &CenterVIndex, const Mesh *P_CMesh)
{
    cout<<"Begin: MSplineFunctions::UpdataMSplineFunctionsAndCoeffAroundAnIrregularVertexByItsTangentVectors()"<<endl;
    /*For the i-th tangent vector*/
    for(unsigned int i=0; i<TangentVectors.size(); i++)//
    {

        unsigned int IndexV=IndexTangentVectors[i][1];


        unsigned int CommonCellIndex=IndexTangentVectors[i][2];
        vector<double> TangentVectors_i;
        TangentVectors_i.push_back(TangentVectors[i][0]);
        TangentVectors_i.push_back(TangentVectors[i][1]);
        TangentVectors_i.push_back(TangentVectors[i][2]);
        /*-Modify the coeff at the IndexV-th vertex-*/
        vector<double> Coeffindex;
        unsigned VIPindex;
        CoeffSetForKeepTangentAtIrregularVertex(VIPindex, IndexV,
                               CenterVIndex, TangentVectors_i, Coeffindex,
                               P_CMesh, CommonCellIndex, Coeff);

//        cout<<"IndexV = "<<IndexV<<endl;
//        cout<<"VIPindex= "<<VIPindex<<endl;

        Coeff[0][VIPindex]=Coeffindex[0];
        Coeff[1][VIPindex]=Coeffindex[1];
        Coeff[2][VIPindex]=Coeffindex[2];

    }

    //Modify MSplineFunctions:

    for(unsigned int i=0; i<IndexTangentVectors.size(); i++)
    {
        unsigned int IndexV=IndexTangentVectors[i][1];
        for(map<unsigned int, Coordinate>::const_iterator itCell=P_CMesh->P_Vertices->at(IndexV).ItsCellsToCoordinatesMap.begin();
            itCell!=P_CMesh->P_Vertices->at(IndexV).ItsCellsToCoordinatesMap.end(); itCell++)
        {
            unsigned int IndexCell=itCell->first;
            this->ModifyMSplineFunctions(Coeff, IndexCell, *P_CMesh);
        }
    }

    cout<<"End: MSplineFunctions::UpdataMSplineFunctionsAndCoeffAroundAnIrregularVertexByItsTangentVectors()"<<endl;

}



void MSplineFunctions::UnitNormalVectorsByGivenTangentVectorsSeq(vector<double> &NormalVector, const vector<vector<double> > & TangentVectors)
{
    NormalVector.clear();
    NormalVector.push_back(0.0);
    NormalVector.push_back(0.0);
    NormalVector.push_back(0.0);
    //dim=3
    vector<double> Ni;

    //cout<<"TangentVectors.size()="<<TangentVectors.size()<<endl;

    //cout<<"TangentVectors[0].size()="<<TangentVectors[0].size()<<endl;

    for (unsigned int i = 0; i < TangentVectors.size()-1; i++)
    {
        VectorCrossProduct(Ni,TangentVectors[i], TangentVectors[i+1]);
        VectorSum(NormalVector,Ni);
    }
    VectorCrossProduct(Ni, TangentVectors[TangentVectors.size()-1], TangentVectors[0]);
    VectorSum(NormalVector,Ni);

    //Normalize the vector
    double length=VectorDotProduct(NormalVector, NormalVector);
    VectorScalarProduct(1.0/sqrt(length), NormalVector);
}

void MSplineFunctions::TangentVectorsFromAVertex_BasedOnPositions(vector<vector<double> >& TangentVectors, vector<vector<unsigned int> > & IndexTangentVectors, const unsigned int CenterVIndex, Mesh* P_CMesh)
{
    //TangentVectors=[T0; T1; ....], Ti=[tx, ty, tz]

   {
        cout<<"Begin: MSplineFunctions::TangentVectorsFromAVertex_BasedOnPositions()"<<endl;
        TangentVectors.clear();
        IndexTangentVectors.clear();

        //cout<<"Compute the one neighbourhood of the center vertex"<<endl;
        /*the one neighbourhood of the center vertex*/
        vector<unsigned int> VertexIndicesSeqAroundCenterVertex;
        P_CMesh->OneNeighbourhoodSequenceAroundCenterVertex(VertexIndicesSeqAroundCenterVertex, CenterVIndex);

        /*compute the Tangent Vectors one by one*/
        for(unsigned int i=0; i<VertexIndicesSeqAroundCenterVertex.size(); i++)
        {
            //cout<<"begin to compute the tangent vector based on the "<<i<<"-th vertex which around the center vertex"<<endl;

            vector<double> Ti, Ti3;
            unsigned int IndexV=VertexIndicesSeqAroundCenterVertex[i];
            unsigned int CommonCellIndex;

            set<unsigned int> CommonCellIndexSet;
            P_CMesh->CommonCellsIndexOfAnEdges(CommonCellIndexSet, IndexV, CenterVIndex);

            CommonCellIndex=*CommonCellIndexSet.begin();// Return

           //Compute the initial value for initialT
            this->TheVectorFromOneVertexToTheOther(CenterVIndex, IndexV, P_CMesh, Ti);
            Ti3.push_back(Ti[0]);
            Ti3.push_back(Ti[1]);
            Ti3.push_back(Ti[2]);

            TangentVectors.push_back(Ti3);

            /*Push_back the elements of IndexTangentVectors*/
            vector<unsigned int> IndexTi;
            IndexTi.push_back(CenterVIndex);
            IndexTi.push_back(IndexV);
            IndexTi.push_back(CommonCellIndex);
            IndexTangentVectors.push_back(IndexTi);
        }
        //=======
    }
}

/* InPut: the index of the given irregular vertex (IrVindex) and the initial coeffients (Coeff).
 * OutPut: the modified Coeff.
 */

void MSplineFunctions::InitialSetCG1ConditionAtAIrregularVertexBasedOnPositions(unsigned int IrVindex, Mesh * P_CMesh, vector<vector<double> > & Coeff)
{
    //dim=3

    vector<double> UnitNormalVector;
    vector<vector<double> > TangentVectors;
    vector<vector<unsigned int> > IndexTangentVectors;
    //****************Compute the normal vector (NormalVector) at the IrVindex-th vertex, based on the positions of 1-neighbourhood vertices**************
    //Here TangentVectors are determined by the positions of vertices around the irregular vertex (TangentDirectionFromTheCenterVertexToTheAdjacentVertex)
    TangentVectorsFromAVertex_BasedOnPositions(TangentVectors, IndexTangentVectors, IrVindex, P_CMesh);
    //Return the unit normal vector based on TangentVectors
    UnitNormalVectorsByGivenTangentVectorsSeq(UnitNormalVector, TangentVectors);

    //****************Compute the tangent directions (\partial_ss, \partial_tt) around the irregular vertex****************

    //BasesVertex * PB_IrV=new BasesVertex(IrVindex, * P_Mesh);

    vector<double> C00;
    C00.push_back(Coeff[0][IrVindex*4]);
    C00.push_back(Coeff[1][IrVindex*4]);
    C00.push_back(Coeff[2][IrVindex*4]);

    for(unsigned int i=0; i<IndexTangentVectors.size(); i++)
    {
        vector<double> InitialTangentVec;
        InitialTangentVec.push_back(TangentVectors[i][0]);
        InitialTangentVec.push_back(TangentVectors[i][1]);
        InitialTangentVec.push_back(TangentVectors[i][2]);
        //the i-th tangent vector: modify the coefficents associated to the IndexTangentVectors[i][1]-th vertex
        unsigned int cellindex=IndexTangentVectors[i][2];
        unsigned int RVIndex=IndexTangentVectors[i][1];
        BasesVertex *PB_RV=new BasesVertex(RVIndex, * P_CMesh);

        double s0=P_CMesh->P_Cells->at(cellindex).CellSize[0];
        double s1=P_CMesh->P_Cells->at(cellindex).CellSize[1];
        double t0=P_CMesh->P_Cells->at(cellindex).CellSize[2];
        double t1=P_CMesh->P_Cells->at(cellindex).CellSize[3];

        vector<double> IrV_Coordinates;
        P_CMesh->P_Cells->at(cellindex).CoordinateOfACornerVertex(IrVindex, IrV_Coordinates);
        vector<double> RVCoordinates;
        P_CMesh->P_Cells->at(cellindex).CoordinateOfACornerVertex(RVIndex, RVCoordinates);

        //compute the initial Pss, Ptt at Irregular vertex
        vector<vector<double> > Pss0,Ptt0, Psst0, Pstt0;
        Coordinate * PC_IrV=new Coordinate();
        PC_IrV->xy[0]=IrV_Coordinates[0];
        PC_IrV->xy[1]=IrV_Coordinates[1];
        vector<Coordinate *> PCIr;
        PCIr.push_back(PC_IrV);
        Coordinate * PC_RV=new Coordinate();
        PC_RV->xy[0]=RVCoordinates[0];
        PC_RV->xy[1]=RVCoordinates[1];
        vector<Coordinate *> PCRV;
        PCRV.push_back(PC_RV);
        //
        this->Evaluation(2,0,PCIr,cellindex,Pss0,*P_CMesh);
        this->Evaluation(0,2,PCIr,cellindex,Ptt0, *P_CMesh);
        this->Evaluation(2,1,PCIr,cellindex,Psst0, *P_CMesh);
        this->Evaluation(1,2,PCIr,cellindex,Pstt0, *P_CMesh);


        vector<double> Pss, Ptt, Psst, Pstt;
        Pss.push_back(Pss0[0][0]); Pss.push_back(Pss0[1][0]); Pss.push_back(Pss0[2][0]);
        Ptt.push_back(Ptt0[0][0]); Ptt.push_back(Ptt0[1][0]); Ptt.push_back(Ptt0[2][0]);
        Psst.push_back(Psst0[0][0]); Psst.push_back(Psst0[1][0]); Psst.push_back(Psst0[2][0]);
        Pstt.push_back(Pstt0[0][0]); Pstt.push_back(Pstt0[1][0]); Pstt.push_back(Pstt0[2][0]);

        vector<vector<double> > PsVr0, PtVr0;
        this->Evaluation(1, 0, PCRV, cellindex, PsVr0, *P_CMesh);
        this->Evaluation(0, 1, PCRV, cellindex, PtVr0, *P_CMesh);
        vector<double> PsVr, PtVr;
        PsVr.push_back(PsVr0[0][0]);PsVr.push_back(PsVr0[1][0]);PsVr.push_back(PsVr0[2][0]);
        PtVr.push_back(PtVr0[0][0]);PsVr.push_back(PtVr0[1][0]);PsVr.push_back(PtVr0[2][0]);

        //Project Pss to the tangent plane: Pss <= Pss-(Pss*UnitNormalVector)UnitNormalVector
        double lambda=VectorDotProduct(Pss,UnitNormalVector);
        Pss[0]=Pss[0]-lambda*UnitNormalVector[0];
        Pss[1]=Pss[1]-lambda*UnitNormalVector[1];
        Pss[2]=Pss[2]-lambda*UnitNormalVector[2];
        //Project Psst to the tangent plane: Psst <= Psst-(Psst*UnitNormalVector)UnitNormalVector
        lambda=VectorDotProduct(Psst,UnitNormalVector);
        Psst[0]=Psst[0]-lambda*UnitNormalVector[0];
        Psst[1]=Psst[1]-lambda*UnitNormalVector[1];
        Psst[2]=Psst[2]-lambda*UnitNormalVector[2];
        //
        //Project Ptt to the tangent plane: Ptt <= Ptt-(Ptt*UnitNormalVector)UnitNormalVector
        lambda=VectorDotProduct(Ptt,UnitNormalVector);
        Ptt[0]=Ptt[0]-lambda*UnitNormalVector[0];
        Ptt[1]=Ptt[1]-lambda*UnitNormalVector[1];
        Ptt[2]=Ptt[2]-lambda*UnitNormalVector[2];
        //Project Pstt to the tangent plane: Pstt <= Pstt-(Pstt*UnitNormalVector)UnitNormalVector
        lambda=VectorDotProduct(Pstt,UnitNormalVector);
        Pstt[0]=Pstt[0]-lambda*UnitNormalVector[0];
        Pstt[1]=Pstt[1]-lambda*UnitNormalVector[1];
        Pstt[2]=Pstt[2]-lambda*UnitNormalVector[2];

        //=================================================
        int DirectionFlag=P_CMesh->P_Cells->at(cellindex).TheLocalDirection(IrVindex, RVIndex);
        if(DirectionFlag==1||DirectionFlag==-1)//s direction or -s direction
        {
            //---Check \beta
            double k=VectorDotProduct(Pss,Pss)*VectorDotProduct(Psst,Ptt)-VectorDotProduct(Psst,Pss)*VectorDotProduct(Pss,Ptt);

            cout<<"\beta="<< VectorDotProduct(Pss,Pss)<<"*"<<VectorDotProduct(Psst,Ptt)<<"-"<<VectorDotProduct(Psst,Pss)<<"*"<<VectorDotProduct(Pss,Ptt)<<"="<<k<<endl;
            cin.get();

            if(k<0)
            {
                //Psst <== -Psst
                Psst[0]=-Psst[0];
                Psst[1]=-Psst[1];
                Psst[2]=-Psst[2];
            }

            vector<double> C30;//The position
            C30.push_back(Coeff[0][RVIndex*4]);C30.push_back(Coeff[1][RVIndex*4]);C30.push_back(Coeff[2][RVIndex*4]);

            //Modify C20
            vector<double> C20;
            C20.push_back(Pss[0]);C20.push_back(Pss[1]);C20.push_back(Pss[2]);

            VectorScalarProduct(-(s1-s0)/2.0, C20);
            C20[0]=C20[0]-3.0*C00[0]/(s1-s0)+3.0*C30[0]/(s1-s0);
            C20[1]=C20[1]-3.0*C00[1]/(s1-s0)+3.0*C30[1]/(s1-s0);
            C20[2]=C20[2]-3.0*C00[2]/(s1-s0)+3.0*C30[2]/(s1-s0);

            vector<double> Values;
            unsigned i_basis;
            int i_Coeff=-1;
            for(i_basis=1; i_basis<3; i_basis++)
            {
                PB_RV->P_Base_Vertex.at(i_basis)->Evaluation(1, 0, PCRV, cellindex, Values, * P_CMesh);
                if(Values[0])
                {
//                    cout<<"Values[0]="<<Values[0]<<endl;
//                    cin.get();
//                    cin.get();
//                    cin.get();
                    //
                    i_Coeff=PB_RV->P_Base_Vertex.at(i_basis)->Index_basis;//the index of C20 in Coeff
                    break;
                }
            }
            //Values[0]*i_Coeff=C20
            Coeff[0][i_Coeff]=C20[0]/Values[0];
            Coeff[1][i_Coeff]=C20[1]/Values[0];
            Coeff[2][i_Coeff]=C20[2]/Values[0];

            if(DirectionFlag==-1)
            {
                Coeff[0][i_Coeff]=-Coeff[0][i_Coeff];
                Coeff[1][i_Coeff]=-Coeff[1][i_Coeff];
                Coeff[2][i_Coeff]=-Coeff[2][i_Coeff];
            }


            //Modify C21
            if(i_basis==1)
            {
                i_basis=2;
            }
            else
            {
                i_basis=1;
            }
            //
//            PB_RV->P_Base_Vertex.at(i_basis)->Evaluation(0, 1, PCRV, cellindex, Values, * P_CMesh);
//            i_Coeff=PB_RV->P_Base_Vertex.at(i_basis)->Index_basis;//the index of C31 in Coeff
//            vector<double> C31;
//            C31.push_back(Values[0]*Coeff[0][i_Coeff]);
//            C31.push_back(Values[0]*Coeff[1][i_Coeff]);
//            C31.push_back(Values[0]*Coeff[2][i_Coeff]);

            i_Coeff=i_Coeff=PB_RV->P_Base_Vertex.at(3)->Index_basis;//the index of C21 in Coeff
            PB_RV->P_Base_Vertex.at(3)->Evaluation(1, 1, PCRV, cellindex, Values, * P_CMesh);
            //cout<<"Values"
//            Coeff[0][i_Coeff]=((3.0/(s1-s0))*C31[0]-((s1-s0)/2.0)*Psst[0])/Values[0];
//            Coeff[1][i_Coeff]=((3.0/(s1-s0))*C31[1]-((s1-s0)/2.0)*Psst[1])/Values[0];
//            Coeff[2][i_Coeff]=((3.0/(s1-s0))*C31[2]-((s1-s0)/2.0)*Psst[2])/Values[0];
            Coeff[0][i_Coeff]=0.0;
            Coeff[1][i_Coeff]=0.0;
            Coeff[2][i_Coeff]=0.0;

        }
        else//If it is along the t-direction
        {
            //--------

            //---Check  \gammar
            double k;
            k=VectorDotProduct(Pstt,Pss)*VectorDotProduct(Ptt,Ptt)-VectorDotProduct(Pss,Ptt)*VectorDotProduct(Pstt,Ptt);
            cin.get();

            cout<<"\gammar = "<<VectorDotProduct(Pstt,Pss)<<"*"<< VectorDotProduct(Ptt,Ptt)<<"-"<<VectorDotProduct(Pss,Ptt) <<"*"<< VectorDotProduct(Pstt,Ptt)<<"="<<k<<endl;

            if(k<0)
            {
                //Pstt <== -Pstt
                Pstt[0]=-Pstt[0];
                Pstt[1]=-Pstt[1];
                Pstt[2]=-Pstt[2];
            }

            //C03
            vector<double> C03;
            C03.push_back(Coeff[0][4*RVIndex]); C03.push_back(Coeff[1][4*RVIndex]); C03.push_back(Coeff[2][4*RVIndex]);

            //Modify C02
            vector<double> C02;
            C02.push_back(0.0);C02.push_back(0.0);C02.push_back(0.0);
            C02[0]=-((t1-t0)/2.0)*Ptt[0]-3.0*C00[0]/(t1-t0)+3.0*C03[0]/(t1-t0);
            C02[1]=-((t1-t0)/2.0)*Ptt[1]-3.0*C00[1]/(t1-t0)+3.0*C03[1]/(t1-t0);
            C02[2]=-((t1-t0)/2.0)*Ptt[2]-3.0*C00[2]/(t1-t0)+3.0*C03[2]/(t1-t0);
            vector<double> Values;
            unsigned i_basis;
            int i_Coeff=-1;
            for(i_basis=1; i_basis<3; i_basis++)
            {
                PB_RV->P_Base_Vertex.at(i_basis)->Evaluation(0, 1, PCRV, cellindex, Values, * P_CMesh);
                if(Values[0])
                {
                    i_Coeff=PB_RV->P_Base_Vertex.at(i_basis)->Index_basis;//the index of C20 in Coeff
                    break;
                }
            }
            //Values[0]*i_Coeff=C02
            Coeff[0][i_Coeff]=C02[0]/Values[0];
            Coeff[1][i_Coeff]=C02[1]/Values[0];
            Coeff[2][i_Coeff]=C02[2]/Values[0];

            if(DirectionFlag==-2)
            {
                Coeff[0][i_Coeff]=-Coeff[0][i_Coeff];
                Coeff[1][i_Coeff]=-Coeff[1][i_Coeff];
                Coeff[2][i_Coeff]=-Coeff[2][i_Coeff];
            }

            //Modify C13
            if(i_basis==1)
            {
                i_basis=2;
            }
            else
            {
                i_basis=1;
            }
            //
//            PB_RV->P_Base_Vertex.at(i_basis)->Evaluation(1, 0, PCRV, cellindex, Values, * P_CMesh);
//            i_Coeff=PB_RV->P_Base_Vertex.at(i_basis)->Index_basis;//the index of C13 in Coeff
//            vector<double> C13;
//            C13.push_back(Values[0]*Coeff[0][i_Coeff]);
//            C13.push_back(Values[0]*Coeff[1][i_Coeff]);
//            C13.push_back(Values[0]*Coeff[2][i_Coeff]);

            i_Coeff=i_Coeff=PB_RV->P_Base_Vertex.at(3)->Index_basis;//the index of C12 in Coeff
            PB_RV->P_Base_Vertex.at(3)->Evaluation(1, 1, PCRV, cellindex, Values, * P_CMesh);
//            Coeff[0][i_Coeff]=((3.0/(t1-t0))*C13[0]-((t1-t0)/2.0)*Pstt[0])/Values[0];
//            Coeff[1][i_Coeff]=((3.0/(t1-t0))*C13[1]-((t1-t0)/2.0)*Pstt[1])/Values[0];
//            Coeff[2][i_Coeff]=((3.0/(t1-t0))*C13[2]-((t1-t0)/2.0)*Pstt[2])/Values[0];
            Coeff[0][i_Coeff]=0.0;
            Coeff[1][i_Coeff]=0.0;
            Coeff[2][i_Coeff]=0.0;
        }


//        if(RVIndex==3)
//        {
//            cout<<"RVIndex="<<RVIndex<<endl;
//            cout<<Coeff[0][4*RVIndex]<<" "<<Coeff[1][4*RVIndex]<<" "<<Coeff[2][4*RVIndex]<<endl;
//            cout<<Coeff[0][4*RVIndex+1]<<" "<<Coeff[1][4*RVIndex+1]<<" "<<Coeff[2][4*RVIndex+1]<<endl;
//            cout<<Coeff[0][4*RVIndex+2]<<" "<<Coeff[1][4*RVIndex+2]<<" "<<Coeff[2][4*RVIndex+2]<<endl;
//            cout<<Coeff[0][4*RVIndex+3]<<" "<<Coeff[1][4*RVIndex+3]<<" "<<Coeff[2][4*RVIndex+3]<<endl;
//            cin.get();
//        }

//        if(IrVindex==3)
//        {
//            cout<<"RVIndex="<<RVIndex<<" (IrVindex="<< IrVindex<<" )"<<endl;
//            cout<<Coeff[0][4*RVIndex]<<" "<<Coeff[1][4*RVIndex]<<" "<<Coeff[2][4*RVIndex]<<endl;
//            cout<<Coeff[0][4*RVIndex+1]<<" "<<Coeff[1][4*RVIndex+1]<<" "<<Coeff[2][4*RVIndex+1]<<endl;
//            cout<<Coeff[0][4*RVIndex+2]<<" "<<Coeff[1][4*RVIndex+2]<<" "<<Coeff[2][4*RVIndex+2]<<endl;
//            cout<<Coeff[0][4*RVIndex+3]<<" "<<Coeff[1][4*RVIndex+3]<<" "<<Coeff[2][4*RVIndex+3]<<endl;
//            cin.get();
//        }


    }




}

//===================================================================================================================
Mesh * MSplineFunctions::GenerateThisMSplineFunctionsOverAFinerMesh(const unsigned int &subdivision_k, Mesh *P_CMesh, vector<vector<double> > &CoeffMap)
{
    CoeffMap.clear();

    vector<double> V1;
    CoeffMap.push_back(V1);
    CoeffMap.push_back(V1);
    CoeffMap.push_back(V1);
    //
    map<unsigned int, unsigned int> OriginalVerticesToCurrentVertices;
    Mesh * P_FEMMesh = P_CMesh->Subdivide_initialParametricMesh_Globally(subdivision_k, OriginalVerticesToCurrentVertices);

    for(unsigned int iv = 0; iv < P_FEMMesh->P_Vertices->size(); iv ++)
    {
        //This Vertex's information
        unsigned int ItsCellIndex=P_FEMMesh->P_Vertices->at(iv).ItsCellsToCoordinatesMap.begin()->first;
        Coordinate *ItsCoordinate=new Coordinate(P_FEMMesh->P_Vertices->at(iv).ItsCellsToCoordinatesMap.begin()->second);

        unsigned int OriginalCellIndex=P_FEMMesh->P_Cells->at(ItsCellIndex).Index_InitialCell;
        //The basis coeffients are decided on the ItsCellIndex-th cell
        //Evaluation at this iv-th vertex
        vector<vector<double> > ValuesXYZ;
        vector<Coordinate*> PCoordinates;
        PCoordinates.push_back(ItsCoordinate);

        this->Evaluation(0, 0, PCoordinates, OriginalCellIndex, ValuesXYZ, *P_CMesh);

        CoeffMap[0].push_back(ValuesXYZ[0][0]);
        CoeffMap[1].push_back(ValuesXYZ[1][0]);
        CoeffMap[2].push_back(0.0);
        this->Evaluation(1, 0, PCoordinates, OriginalCellIndex, ValuesXYZ, *P_CMesh);

        CoeffMap[0].push_back(ValuesXYZ[0][0]);
        CoeffMap[1].push_back(ValuesXYZ[1][0]);
        CoeffMap[2].push_back(0.0);
        this->Evaluation(0, 1, PCoordinates, OriginalCellIndex, ValuesXYZ, *P_CMesh);

        CoeffMap[0].push_back(ValuesXYZ[0][0]);
        CoeffMap[1].push_back(ValuesXYZ[1][0]);
        CoeffMap[2].push_back(0.0);
        this->Evaluation(1, 1, PCoordinates, OriginalCellIndex, ValuesXYZ, *P_CMesh);

        CoeffMap[0].push_back(ValuesXYZ[0][0]);
        CoeffMap[1].push_back(ValuesXYZ[1][0]);
        CoeffMap[2].push_back(0.0);

    }

    return P_FEMMesh;
}

void MSplineFunctions::CoutThisMSplineFunctionsOverAFinerMesh(const unsigned int &subdivision_k, Mesh *P_CMesh, vector<vector<double> > &CoeffMap)
{
    CoeffMap.clear();

    vector<double> V1;
    CoeffMap.push_back(V1);
    CoeffMap.push_back(V1);
    CoeffMap.push_back(V1);
    //
    map<unsigned int, unsigned int> OriginalVerticesToCurrentVertices;
    Mesh * P_FEMMesh = P_CMesh->Subdivide_initialParametricMesh_Globally(subdivision_k, OriginalVerticesToCurrentVertices);

    cout<<"P_FEMMesh->P_Vertices->size()="<<P_FEMMesh->P_Vertices->size()<<endl;

    for(unsigned int iv = 0; iv < P_FEMMesh->P_Vertices->size(); iv ++)
    {
        //This Vertex's information
        unsigned int ItsCellIndex=P_FEMMesh->P_Vertices->at(iv).ItsCellsToCoordinatesMap.begin()->first;
        Coordinate *ItsCoordinate=new Coordinate(P_FEMMesh->P_Vertices->at(iv).ItsCellsToCoordinatesMap.begin()->second);

        unsigned int OriginalCellIndex=P_FEMMesh->P_Cells->at(ItsCellIndex).Index_InitialCell;

        //cout<<"OriginalCellIndex = "<<OriginalCellIndex<<endl;
        //The basis coeffients are decided on the ItsCellIndex-th cell
        //Evaluation at this iv-th vertex
        vector<vector<double> > ValuesXYZ;
        vector<Coordinate*> PCoordinates;
        PCoordinates.push_back(ItsCoordinate);

        this->Evaluation(0, 0, PCoordinates, OriginalCellIndex, ValuesXYZ, *P_CMesh);

        CoeffMap[0].push_back(ValuesXYZ[0][0]);
        CoeffMap[1].push_back(ValuesXYZ[1][0]);
        CoeffMap[2].push_back(ValuesXYZ[2][0]);
        this->Evaluation(1, 0, PCoordinates, OriginalCellIndex, ValuesXYZ, *P_CMesh);

        CoeffMap[0].push_back(ValuesXYZ[0][0]);
        CoeffMap[1].push_back(ValuesXYZ[1][0]);
        CoeffMap[2].push_back(ValuesXYZ[2][0]);
        this->Evaluation(0,1,PCoordinates,OriginalCellIndex,ValuesXYZ, *P_CMesh);

        CoeffMap[0].push_back(ValuesXYZ[0][0]);
        CoeffMap[1].push_back(ValuesXYZ[1][0]);
        CoeffMap[2].push_back(ValuesXYZ[2][0]);
        this->Evaluation(1, 1, PCoordinates, OriginalCellIndex, ValuesXYZ, *P_CMesh);

        CoeffMap[0].push_back(ValuesXYZ[0][0]);
        CoeffMap[1].push_back(ValuesXYZ[1][0]);
        CoeffMap[2].push_back(ValuesXYZ[2][0]);

    }

    //Generate this parametrization over P_FEMMesh
    //Cout the parametrization over a finer mesh

    PrintParametricMap(CoeffMap, P_FEMMesh, "Parameterization_Axel.axl");

    delete P_FEMMesh;
    P_FEMMesh = NULL;
}
//======Print the BSpline surface Axel files
void MSplineFunctions::CoutThisMSplineFunctionsAsBSplineSurfaceAxelFile(Mesh * P_CMesh)
{
    vector<vector<double> > CoeffXYZ;
    vector<double> Coeffi;
    ofstream ofAxel;
    ofAxel.open("ParametricMap_BSplineSurfaceAxel.axl");
    ofAxel<<"<axl>"<<endl;


    for(unsigned int i_cell=0; i_cell<P_CMesh->P_Cells->size(); i_cell++)//For each cell
    {
        CoeffXYZ.clear();
        for(unsigned int i=0; i<this->P_MSplineFunctions.size(); i++)//CoeffXYZ
        {
            //this->P_MSplineFunctions[i]
            Coeffi.clear();
            this->P_MSplineFunctions[i]->BSplineCoeffAxelOrder(i_cell, P_CMesh->P_Cells->at(i_cell).CellSize, Coeffi);
            CoeffXYZ.push_back(Coeffi);
        }

        if(CoeffXYZ.size()<3)
        {
            vector<double> CoeffZero(16,0.0);
            unsigned int DiffSize=3-CoeffXYZ.size();
            for(unsigned int i=0; i<DiffSize; i++)
            {
                CoeffXYZ.push_back(CoeffZero);
            }

        }
//        cout<<"Here: CoeffXYZ.size()= "<<CoeffXYZ.size()<<endl;
//        cout<<"CoeffXYZ[0].size() = "<<CoeffXYZ[0].size()<<endl;
//        cout<<"CoeffXYZ[0].size() = "<<CoeffXYZ[0].size()<<endl;
//        cout<<"CoeffXYZ[0].size() = "<<CoeffXYZ[0].size()<<endl;

        ofAxel<<endl;
        double x0=P_CMesh->P_Cells->at(i_cell).CellSize[0];
        double x1=P_CMesh->P_Cells->at(i_cell).CellSize[1];
        double y0=P_CMesh->P_Cells->at(i_cell).CellSize[2];
        double y1=P_CMesh->P_Cells->at(i_cell).CellSize[3];

        ofAxel<<"<surface type=\"bspline\" color=\" "<<100<<" "<<100<<" "<<100<<"\">"<<endl;
        ofAxel<<"<dimension> "<< 3<<" </dimension>"<<endl;
        ofAxel<<"<number> "<<4<<" "<<4 <<"</number>"<<endl;
        ofAxel<<"<order> "<<4<<" "<<4<<"</order>"<<endl;

        ofAxel<<"<knots>"<<x0<<" "<<x0<<" "<<x0<<" "<<x0<<" "<<x1<<" "<<x1<<" "<<x1<<" "<<x1<<"</knots>"<<endl;
        ofAxel<<"<knots>"<<y0<<" "<<y0<<" "<<y0<<" "<<y0<<" "<<y1<<" "<<y1<<" "<<y1<<" "<<y1<<"</knots>"<<endl;

        ofAxel<<"<points>"<<endl;
        for(unsigned int i=0; i<16; i++)
        {
            ofAxel<<CoeffXYZ[0][i]<<" "<<CoeffXYZ[1][i]<<" "<<CoeffXYZ[2][i]<<endl;
        }
        ofAxel<<"</points>"<<endl;
        ofAxel<<"</surface>"<<endl;

        ofAxel<<endl;
    }


    ofAxel<<"</axl>"<<endl;
    //--------------
    ofAxel.close();
}


//====
static double TriangularArea(const vector<unsigned int> &Vindices, const vector<vector<double> > &VerticeSeq)
{
    double A=0.0;

    if(Vindices.size()==3)
    {
        //Triangular
        unsigned int Vindex1=Vindices[0];
        unsigned int Vindex2=Vindices[1];
        unsigned int Vindex3=Vindices[2];
        //
        double a, b, c;
        a=0.0;
        b=0.0;
        c=0.0;
        for(unsigned int i=0; i<VerticeSeq[Vindex1].size(); i++)
        {
            //Vindex1-Vindex2
            a=a+(VerticeSeq[Vindex1][i]-VerticeSeq[Vindex2][i])*(VerticeSeq[Vindex1][i]-VerticeSeq[Vindex2][i]);
            //Vindex1-Vindex3
            b=b+(VerticeSeq[Vindex1][i]-VerticeSeq[Vindex3][i])*(VerticeSeq[Vindex1][i]-VerticeSeq[Vindex3][i]);
            //Vindex2-Vindex3
            c=c+(VerticeSeq[Vindex2][i]-VerticeSeq[Vindex3][i])*(VerticeSeq[Vindex2][i]-VerticeSeq[Vindex3][i]);

        }
        a=sqrt(a);
        b=sqrt(b);
        c=sqrt(c);
        //
        double p=0.5*(a+b+c);
        A=sqrt(p*(p-a)*(p-b)*(p-c));

    }

    return A;

}
void MSplineFunctions::CoutThisMSplineFunctionsJacobianMeshAxelFile(Mesh * P_CMesh, const unsigned int n_u, const unsigned int n_v, const char* filename , const char* filenameAreas)
{
    unsigned int nV=0;
    unsigned int nF=0;
    unsigned int nE=0;

    vector<double> Areas;
    //For each cell, compute the vertices, faces and edges
    //VerticesForCells[i]->the vertices in the i-th cell
    vector<vector<vector<double> > > VerticesForCells;
    vector<vector<double> > VerticeSeq;
    //FacesForCells[i]-> the faces over the i-th cell
    vector<vector<vector<unsigned int> > > FacesForCells;
    //EdgesForCells[i]-> the edges in the i-th cell
    vector<vector<vector<unsigned int> > > EdgesForCells;
    //
    vector<PointSet> PSet;
    for(unsigned int i_cell=0; i_cell<P_CMesh->P_Cells->size(); i_cell++)
    {
        double x0=P_CMesh->P_Cells->at(i_cell).CellSize[0];
        double x1=P_CMesh->P_Cells->at(i_cell).CellSize[1];
        double y0=P_CMesh->P_Cells->at(i_cell).CellSize[2];
        double y1=P_CMesh->P_Cells->at(i_cell).CellSize[3];

        double dx=(x1-x0)/(n_u-1.0);
        double dy=(y1-y0)/(n_v-1.0);
        //====
        PointSet Ps;
        Ps.Index_ItsCell=i_cell;
        Ps.P_coordinates_Ps.clear();
        for(int u_index=0; u_index < n_u; u_index++)
        {
            for(int v_index=0; v_index < n_v; v_index++)
            {
                Coordinate *P_coordinate_P=new Coordinate;
                P_coordinate_P->xy[0]=u_index*dx+x0;
                P_coordinate_P->xy[1]=v_index*dy+y0;
                Ps.P_coordinates_Ps.push_back(P_coordinate_P);
                P_coordinate_P=NULL;
            };

        };
        PSet.push_back(Ps);

    }
    //======
    for(unsigned int iPs=0; iPs < PSet.size(); iPs++)
    {
        vector<vector<double> >  Values;
        this->Evaluation(0,0,PSet[iPs].P_coordinates_Ps, PSet[iPs].Index_ItsCell, Values, *P_CMesh);
        vector<vector<double> > ValuesDs;
        this->Evaluation(1,0,PSet[iPs].P_coordinates_Ps, PSet[iPs].Index_ItsCell, ValuesDs, *P_CMesh);
        vector<vector<double> > ValuesDt;
        this->Evaluation(0,1,PSet[iPs].P_coordinates_Ps, PSet[iPs].Index_ItsCell, ValuesDt, *P_CMesh);

        //=Vertices======//the positions of these vertices are On Planar
        vector<vector<double> > VerticesForCelli;
        //====
        for(unsigned int i=0; i< Values[0].size(); i++)
        {
            vector<double> Vi;
            Vi.push_back(Values[0][i]);
            Vi.push_back(Values[1][i]);

            double JacobianV=ValuesDs[0][i]*ValuesDt[1][i]-ValuesDs[1][i]*ValuesDt[0][i];
            Vi.push_back(JacobianV);

            VerticesForCelli.push_back(Vi);
            VerticeSeq.push_back(Vi);//The position (VerticesSeq[i][0], VerticesSeq[i][1])

        }

        //
        nV=nV+VerticesForCelli.size();
        VerticesForCells.push_back(VerticesForCelli);

        //===Faces===
        vector<vector<unsigned int> > FacesForCelli;
        //==== iPs=i_cell

        unsigned int cell_index=PSet[iPs].Index_ItsCell;
        //For Faces (triangules)
        for(int u_index=0; u_index < n_u-1; u_index++)
        {
            for(int v_index=0; v_index < n_v-1; v_index++)
            {
                vector<unsigned int> Fi;
                Fi.push_back(u_index*n_v+v_index+n_u*n_v*cell_index);
                Fi.push_back(u_index*n_v+v_index+1+n_u*n_v*cell_index);
                Fi.push_back((u_index+1)*n_v+v_index+n_u*n_v*cell_index);
                FacesForCelli.push_back(Fi);

                Fi.clear();
                Fi.push_back(u_index*n_v+v_index+1+n_u*n_v*cell_index);
                Fi.push_back((u_index+1)*n_v+v_index+n_u*n_v*cell_index);
                Fi.push_back((u_index+1)*n_v+v_index+1+n_u*n_v*cell_index);
                FacesForCelli.push_back(Fi);
                //===============================================================================
//                mesh->push_back_face(u_index*n_v+v_index+n_u*n_v*cell_index,u_index*n_v+v_index+1+n_u*n_v*cell_index,(u_index+1)*n_v+v_index+n_u*n_v*cell_index);
//                mesh->push_back_face(u_index*n_v+v_index+1+n_u*n_v*cell_index,(u_index+1)*n_v+v_index+n_u*n_v*cell_index,(u_index+1)*n_v+v_index+1+n_u*n_v*cell_index);
            };
        };

        //====
        nF=nF+FacesForCelli.size();
        FacesForCells.push_back(FacesForCelli);

        //====
        //=Edges=====
        vector<vector<unsigned int> > EdgesForCelli;
        vector<unsigned int> Ei;
        //====
        //For Edges
        // Add the loop of boundary edges
        for(int i = 0; i < n_v-1 ; i++)
        {
            Ei.clear();
            Ei.push_back(i+n_u*n_v*cell_index);
            Ei.push_back(i+1+n_u*n_v*cell_index);
            EdgesForCelli.push_back(Ei);
            //mesh->push_back_edge(i+n_u*n_v*cell_index, i+1+n_u*n_v*cell_index);
        }

        for(int j = 0; j < n_u-1 ; j++)
        {
            Ei.clear();
            Ei.push_back(n_u-1+j*n_v+n_u*n_v*cell_index);
            Ei.push_back(n_u-1+(j+1)*n_v+n_u*n_v*cell_index);
            EdgesForCelli.push_back(Ei);
            //mesh->push_back_edge(n_u-1+j*n_v+n_u*n_v*cell_index, n_u-1+(j+1)*n_v+n_u*n_v*cell_index);
        }

        for(int i = 0; i < n_v-1 ; i++)
        {
            Ei.clear();
            Ei.push_back((n_u-1)*n_v + (n_v-1-i)+n_u*n_v*cell_index);
            Ei.push_back((n_u-1)*n_v + (n_v-1-(i+1))+n_u*n_v*cell_index);
            EdgesForCelli.push_back(Ei);
            //mesh->push_back_edge((n_u-1)*n_v + (n_v-1-i)+n_u*n_v*cell_index,(n_u-1)*n_v + (n_v-1-(i+1))+n_u*n_v*cell_index);
        }

        for(int j = 0; j < n_u-1 ; j++)
        {
            Ei.clear();
            Ei.push_back((n_u-1-j)*n_v+n_u*n_v*cell_index);
            Ei.push_back((n_u-1-(j+1))*n_v+n_u*n_v*cell_index);
            EdgesForCelli.push_back(Ei);
            //mesh->push_back_edge((n_u-1-j)*n_v+n_u*n_v*cell_index,(n_u-1-(j+1))*n_v+n_u*n_v*cell_index);
        }

        nE=nE+EdgesForCelli.size();
        EdgesForCells.push_back(EdgesForCelli);
    }

    //---------------
    ofstream ofAxelMesh;
    ofAxelMesh.open(filename);

    ofAxelMesh<<"<axl>"<<endl;
    ofAxelMesh<<"<mesh>"<<endl;
    ofAxelMesh<<"<count> "<< nV<<" "<<nE<<" "<<nF<<" </count>"<<endl;
    //--Points
    //ofAxelMesh<<"<points color= \"rgb\" >"<<endl;
    ofAxelMesh<<"<points>"<<endl;
    for(unsigned int iv1=0; iv1<VerticesForCells.size(); iv1++)
    {
        for(unsigned int iv2=0; iv2<VerticesForCells[iv1].size(); iv2++)
        {
            for(unsigned int iv3=0; iv3<VerticesForCells[iv1][iv2].size(); iv3++)
            {
                //
                ofAxelMesh<<VerticesForCells[iv1][iv2][iv3]<<" ";
            }
            //ofAxelMesh<<100<<" "<<100<<" "<<100<<endl;
            ofAxelMesh<<endl;
        }
    }
    ofAxelMesh<<"</points>"<<endl;

    //--Faces
    Areas.clear();
    //vector<vector<vector<unsigned int> > > FacesForCells;
    ofAxelMesh<<"<faces>"<<endl;
    for(unsigned int iF1=0; iF1<FacesForCells.size(); iF1++)
    {
        double area=0;
        vector<unsigned int> Vindices;

        for(unsigned int iF2=0; iF2<FacesForCells[iF1].size(); iF2++)
        {
            ofAxelMesh<<3<<" ";
            for(unsigned int iF3=0; iF3<FacesForCells[iF1][iF2].size(); iF3++)
            {
                ofAxelMesh<<FacesForCells[iF1][iF2][iF3]<<" ";
                Vindices.push_back(FacesForCells[iF1][iF2][iF3]);
            }
            ofAxelMesh<<endl;
            //compute the element area
            double areai=TriangularArea(Vindices, VerticeSeq);
            area=area+areai;

        }
        //===
        Areas.push_back(area);
    }

    ofAxelMesh<<"</faces>"<<endl;

    //--Edges
    ofAxelMesh<<"<edges>"<<endl;
    for(unsigned int iE1=0; iE1<EdgesForCells.size(); iE1++)
    {
        for(unsigned int iE2=0; iE2<EdgesForCells[iE1].size(); iE2++)
        {
            ofAxelMesh<<2<<" ";
            for(unsigned int iE3=0; iE3<EdgesForCells[iE1][iE2].size(); iE3++)
            {
                ofAxelMesh<<EdgesForCells[iE1][iE2][iE3]<<" ";
            }
            ofAxelMesh<<endl;
        }
    }

    ofAxelMesh<<"</edges>"<<endl;


    ofAxelMesh<<"</mesh>"<<endl;
    ofAxelMesh<<"</axl>"<<endl;

    ofAxelMesh.close();

    ofstream ofArea;
    ofArea.open(filenameAreas);
    for(unsigned int i=0; i<Areas.size(); i++)
    {
        ofArea<<Areas[i]<<endl;
    }
    ofArea.close();
}
//---------------------------------------------------------------------
/* InPut: Hook's coeff (Hc); TimeSize; the mass vector vector<double> Mass, the positions of vertices vector<vector<double> >& VerticesPosition
 *	  And the mesh P_ParmetricMesh
 * OutPut: the difference between the original vertices and the new vertices vector<vector<double> > &DeltaPosition
*/

static void DeltaPositionBasedOnSpringModel(double Hc, double TimeSize, const vector<double>& Mass, const vector<vector<double> >& VerticesPosition,
                                     Mesh * P_ParametricMesh, vector<vector<double> > &DeltaPosition)
{
    DeltaPosition.clear();

    //For each vertex, compute DeltaPosition[i_vertex]
    for(unsigned int i_vertex = 0; i_vertex < P_ParametricMesh->P_Vertices->size(); i_vertex++)
    {
        //===============================
        // For the i_vertex-th vertex, computer the force at this vertex (set<unsigned int> Index_ItsVertices;)

        //The force
        vector<double> Fi;
        Fi.push_back(0);
        Fi.push_back(0);//Fi=[0, 0]
        if(P_ParametricMesh->P_Vertices->at(i_vertex).Is_interiorVertex)
        {
//            //The length:
//            vector<double> L;
//            double MeanL=0;

//            for(set<unsigned int>::const_iterator itV=P_ParametricMesh->P_Vertices->at(i_vertex).Index_ItsVertices.begin();
//                itV != P_ParametricMesh->P_Vertices->at(i_vertex).Index_ItsVertices.end(); itV++)
//            {
//                //L=[L0, L1, L2, ... ...]
//                //the length from VerticesPosition[*itV] to VerticesPosition[i_vertex]
//                double s=sqrt((VerticesPosition[*itV][0]-VerticesPosition[i_vertex][0])*(VerticesPosition[*itV][0]-VerticesPosition[i_vertex][0])+(VerticesPosition[*itV][1]-VerticesPosition[i_vertex][1])*(VerticesPosition[*itV][1]-VerticesPosition[i_vertex][1]));
//                MeanL=MeanL+s;
//                L.push_back(s);
//            }
//            MeanL=MeanL/(L.size()*1.0);// the average of the length

            unsigned int index=0;
            for(set<unsigned int>::const_iterator itV=P_ParametricMesh->P_Vertices->at(i_vertex).Index_ItsVertices.begin();
                itV != P_ParametricMesh->P_Vertices->at(i_vertex).Index_ItsVertices.end(); itV++)
            {

                //Fi=Fi+(VerticesPosition[*itV]-VerticesPosition[i_vertex])//the sum of vectors
                double directionCoeff=1.0;
                //

                //
                //directionCoeff=1.0-0.3*MeanL/L[index];

                Fi[0]=Fi[0]+directionCoeff*(VerticesPosition[*itV][0]-VerticesPosition[i_vertex][0]);
                Fi[1]=Fi[1]+directionCoeff*(VerticesPosition[*itV][1]-VerticesPosition[i_vertex][1]);

                index=index+1;

            }
            //Fi=Hc*Fi
            Fi[0]=Hc*Fi[0];
            Fi[1]=Hc*Fi[1];
        }

        //============================================
        //The delta displacement
        vector<double> Si;
        Si.push_back(0.5*Fi[0]*TimeSize*TimeSize/Mass[i_vertex]);
        Si.push_back(0.5*Fi[1]*TimeSize*TimeSize/Mass[i_vertex]);

        //============================================
        //Push_back the i_vertex-th delta displacement
        DeltaPosition.push_back(Si);

    }

}

/*
 * Compute the delta positions owning to the coulomb Force from boundary edges
*/
static double CoulombForceIntegration(const double k, const double l, const double A, const double B, const double C)
{
    double F=0;
    //------------
    if(A+B+C<0||C<0)
    {
        cout<<"CoulmbForceInteration:A+B+C<0||C<0"<<endl;
        cin.get();
    }

    F=k*(-(2.0*B+4.0*C)/((4.0*A*C-B*B)*sqrt(A+B+C))+4.0*C/((4.0*A*C-B*B)*sqrt(C)));
    if(F!=F)
    {
        cout<<"F1="<<F<<endl;
        cout<<"k="<<k<<endl;
        cout<<"-(2.0*B+4.0*C)="<<-(2.0*B+4.0*C)<<endl;
        cout<<"(4.0*A*C-B*B)="<<(4.0*A*C-B*B)<<endl;
        cout<<"sqrt(A+B+C))= "<<sqrt(A+B+C)<<endl;
        cout<<"sqrt(C)= "<<sqrt(C)<<endl;
        cin.get();
        cin.get();
        cin.get();
    }
    F=F+l*((4.0*A+2.0*B)/((4.0*A*C-B*B)*sqrt(A+B+C))-(2.0*B)/((4.0*A*C-B*B)*sqrt(C)));
    if(F!=F)
    {
        cout<<"F2="<<F<<endl;
        cout<<"k="<<k<<endl;
        cout<<"(4.0*A+2.0*B)="<<-(2.0*B+4.0*C)<<endl;
        cout<<"(4.0*A*C-B*B)="<<(4.0*A*C-B*B)<<endl;
        cout<<"sqrt(A+B+C))= "<<sqrt(A+B+C)<<endl;
        cout<<"sqrt(C)= "<<sqrt(C)<<endl;
        cin.get();
        cin.get();
        cin.get();
    }
    //------------
    return F;
}
static void CoulombForce(vector<double>& dF, const unsigned int i_vertex, set<pair<unsigned int, unsigned int> >::const_iterator et, const vector<vector<double> >& VerticesPosition)
{
    dF.clear();
    //---------------------
    unsigned int VEindex1=(*et).first;
    unsigned int VEindex2=(*et).second;
    double x0, y0, x1, y1, x2, y2;
    x0=VerticesPosition[i_vertex][0];
    y0=VerticesPosition[i_vertex][1];

    x1=VerticesPosition[VEindex1][0];
    y1=VerticesPosition[VEindex1][1];

    x2=VerticesPosition[VEindex2][0];
    y2=VerticesPosition[VEindex2][1];
    //-----------------------
    double A, B, C;
    A=(x2-x1)*(x2-x1)+(y2-y1)*(y2-y1);
    B=-2.0*((x0-x1)*(x2-x1)+(y0-y1)*(y2-y1));
    C=(x0-x1)*(x0-x1)+(y0-y1)*(y0-y1);

    unsigned int iv=i_vertex;
    unsigned int i=1;

    double delta=B*B-4.0*A*C;
    while( delta==0)
    {
        //Modify x0, y0;
        x0=VerticesPosition[(i_vertex+i)%VerticesPosition.size()][0];
        y0=VerticesPosition[(i_vertex+i)%VerticesPosition.size()][1];
        //====
        //
        //Modify A B C
        A=(x2-x1)*(x2-x1)+(y2-y1)*(y2-y1);
        B=-2.0*((x0-x1)*(x2-x1)+(y0-y1)*(y2-y1));
        C=(x0-x1)*(x0-x1)+(y0-y1)*(y0-y1);

        delta=B*B-4.0*A*C;

        i=i+1;

        if(i>=VerticesPosition.size())// i<VerticesPosition.size()
        {
            cout<<"Delta=B^2-4*A*C always equals to zero! there must be an nan"<<endl;

            cin.get();
            cin.get();
            cin.get();
        }
        //
    }

    double kx, lx;
    kx=x1-x2;
    lx=x0-x1;

    double ky, ly;
    ky=y1-y2;
    ly=y0-y1;
    //
    double Fx, Fy;
    Fx=CoulombForceIntegration(kx, lx, A, B, C);
    Fy=CoulombForceIntegration(ky, ly, A, B, C);
    //
    if(Fx!=Fx)//Fx=nan
    {
        cout<<"Fx=nan"<<endl;
        cout<<"A="<<A<<endl;
        cout<<"B="<<B<<endl;
        cout<<"C="<<C<<endl;
        cout<<"kx="<<kx<<endl;
        cout<<"ky="<<ky<<endl;
        cout<<"lx="<<lx<<endl;
        cout<<"ly="<<ly<<endl;
        cin.get();
        cin.get();
        cin.get();
    }

    if(Fy!=Fy)//Fy=nan
    {
        cout<<"Fy=nan"<<endl;
        cout<<"A="<<A<<endl;
        cout<<"B="<<B<<endl;
        cout<<"C="<<C<<endl;
        cout<<"kx="<<kx<<endl;
        cout<<"ky="<<ky<<endl;
        cout<<"lx="<<lx<<endl;
        cout<<"ly="<<ly<<endl;
        cin.get();
        cin.get();
        cin.get();
    }

    dF.push_back(Fx);//dF[0]

    dF.push_back(Fy);//dF[1]


}
static void CoulombForce2(vector<double>& dF, const unsigned int i_vertex, set<pair<unsigned int, unsigned int> >::const_iterator et, const vector<vector<double> >& VerticesPosition)
{
    dF.clear();
    //---------------------
    unsigned int VEindex1=(*et).first;
    unsigned int VEindex2=(*et).second;
    double x0, y0, x1, y1, x2, y2;
    x0=VerticesPosition[i_vertex][0];
    y0=VerticesPosition[i_vertex][1];

    x1=VerticesPosition[VEindex1][0];
    y1=VerticesPosition[VEindex1][1];

    x2=VerticesPosition[VEindex2][0];
    y2=VerticesPosition[VEindex2][1];
    //-----------------------
    double d1_2, d2_2, ds;
    d1_2=(x1-x0)*(x1-x0)+(y1-y0)*(y1-y0);
    d2_2=(x2-x0)*(x2-x0)+(y2-y0)*(y2-y0);
    ds=sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
    double Fx, Fy;

    if(d1_2<10e-12 && d2_2>10e-12)
    {
        Fx=ds*(x0-x2)/d2_2;
        Fy=ds*(y0-y2)/d2_2;

    }
    if(d1_2>10e-12 && d2_2>10e-12)
    {
        Fx=0.5*((x0-x1)/d1_2+(x0-x2)/d2_2)*ds;
        Fy=0.5*((y0-y1)/d1_2+(y0-y2)/d2_2)*ds;
    }

    if(d1_2<10e-12 && d2_2<10e-12)
    {
        Fx=0;
        Fy=0;
    }
    if(d1_2>10e-12 && d2_2<10e-12)
    {
        Fx=ds*(x0-x1)/d1_2;
        Fy=ds*(y0-y1)/d1_2;
    }

    dF.push_back(Fx);//dF[0]

    dF.push_back(Fy);//dF[1]


}
static void DeltaPositionBasedOnBoundaryCoulombForce(const double &CoulombCoeff, const double &TimeSize, Mesh* P_ParametricMesh,
                                              const vector<vector<double> > &VerticesPosition,
                                              const vector<double> &Mass,
                                              vector<vector<double> > &DeltaPositionCoulomb)
{
    DeltaPositionCoulomb.clear();

    //Compute the boundary edges
    set<pair<unsigned int, unsigned int> > BoundaryEdges;
    P_ParametricMesh->GetBoundaryEdgesFromParametricMesh(BoundaryEdges);

    //Compute its delta position based on the force
    for(unsigned int i_vertex = 0; i_vertex < P_ParametricMesh->P_Vertices->size(); i_vertex++)
    {

        //----------------
        vector<double> dP;//the delta position for the i_vertex-th vertex
        //----------------

        vector<double> Fi;
        Fi.push_back(0.0);
        Fi.push_back(0.0);

        if(P_ParametricMesh->P_Vertices->at(i_vertex).Is_interiorVertex)//the i_vertex-th vertex is an interior vertex
        {
            //-----------For each boundary------
            for(set<pair<unsigned int, unsigned int> >::const_iterator et=BoundaryEdges.begin();
                et!=BoundaryEdges.end(); et++)
            {


                //-----
                vector<double> dF;
                //For the i_vertex (interior) vertex,
                //compute the force from the boundary edge *et
                //CoulombForce(dF, i_vertex, et, VerticesPosition);
                CoulombForce2(dF, i_vertex, et, VerticesPosition);
                //-----

                Fi[0]=Fi[0]+dF[0];
                Fi[1]=Fi[1]+dF[1];

            }


            //-----------------------------
        }

        Fi[0]=CoulombCoeff*Fi[0];
        Fi[1]=CoulombCoeff*Fi[1];

        //Compute the delta position for the i_vertex-th (interior) vertex
        dP.push_back(0.5*Fi[0]*TimeSize*TimeSize/Mass[i_vertex]);
        dP.push_back(0.5*Fi[1]*TimeSize*TimeSize/Mass[i_vertex]);

        //
        DeltaPositionCoulomb.push_back(dP);
    }
}

/*
 *VerticesPosition = VerticesPosition + DeltaPosition
*/
static void UpDataVerticesPosition(vector< vector<double> > &VerticesPosition,
                            const vector< vector<double> > &DeltaPosition)
{
    for(unsigned int i=0; i < VerticesPosition.size(); i++)
    {
        VerticesPosition[i][0]=VerticesPosition[i][0]+DeltaPosition[i][0];
        VerticesPosition[i][1]=VerticesPosition[i][1]+DeltaPosition[i][1];
    }
    //Return VerticesPosition
}


static double TotalDistance(const vector<vector<double> > &DeltaPositions)
{
    double epsilon=0.0;

    for(unsigned int i=0; i<DeltaPositions.size(); i++)
    {
        for(unsigned int j=0; j<DeltaPositions[i].size(); j++)
        {
            epsilon=epsilon+DeltaPositions[i][j]*DeltaPositions[i][j];
        }
    }

    epsilon=sqrt(epsilon)/(1.0*DeltaPositions.size());

    return epsilon;
}

/*------------------------------------------------------------------------------
--------------------------------------------------------------------------------*/
//MSplineFunctions * MSplineFunctions::ModifyParameterizationBySpringModelAndBoundaryFixing_Double(const double TimeSize, const double Hc, const double CoulombCoeff, const double epsilon0,
//                                                       const unsigned n_u, const unsigned n_v, const unsigned MaxIterNum, const unsigned int N, Mesh * P_ParametricMesh,
//                                                                          const  vector<vector<double> > & Coeff_Parametric_Map, vector<vector<double> > &NewCoeff_ParametricMap)
//{
//   MSplineFunctions * ReturnMap=this->ModifyParameterizationBySpringModelAndBoundaryFixing(TimeSize, Hc, 0.0, epsilon0,
//                                                               n_u, n_v, MaxIterNum, N, P_ParametricMesh,Coeff_Parametric_Map, NewCoeff_ParametricMap);
//   MSplineFunctions *ReturnMap2=ReturnMap->ModifyParameterizationBySpringModelAndBoundaryFixing(TimeSize, Hc, CoulombCoeff, epsilon0,
//                                               n_u, n_v, MaxIterNum, N, P_ParametricMesh,Coeff_Parametric_Map, NewCoeff_ParametricMap);
//   delete ReturnMap;
//   ReturnMap=NULL;

//   return ReturnMap2;
//}

void MSplineFunctions::PrintElementInformation(const vector<vector<double> >& VerticesPosition, Mesh *P_ParametricMesh, char * H_CellbyCell, char * CosTheta_CellbyCell, char * Is_Concave_CellbyCell)
{
    ofstream ofHCellByCell;
    ofHCellByCell.open(H_CellbyCell);
    ofstream ofCosTheta_CellbyCell;
    ofCosTheta_CellbyCell.open(CosTheta_CellbyCell);
    ofstream ofIs_Concave_CellbyCell;
    ofIs_Concave_CellbyCell.open(Is_Concave_CellbyCell);

    //VerticesPosition[i]: the i-th vertex and its coordinates (VerticesPosition[i][0], VerticesPosition[i][1])
    //
    for(unsigned int i_cell=0; i_cell<P_ParametricMesh->P_Cells->size(); i_cell++)
    {
        //The indices of vertices of the i_cell-th cell
        //P_ParametricMesh->P_Cells->at(i_cell).Index_ItsCornerVertices;
       //
        double Hmax=0, Hmin=100;
        double CosThetaMax=0;

        //double Concave_Label1=0, Concave_Label2=0, Concave_Label3=0, Concave_Lable4=0;
        vector<double> ConcaveLables;
        //
       for(unsigned i_v=0; i_v<P_ParametricMesh->P_Cells->at(i_cell).Index_ItsCornerVertices.size(); i_v++)
       {
           unsigned prev_Vindex=P_ParametricMesh->P_Cells->at(i_cell).Index_ItsCornerVertices[(i_v+3)%4];
           unsigned curr_Vindex=P_ParametricMesh->P_Cells->at(i_cell).Index_ItsCornerVertices[i_v];
           unsigned next_Vindex=P_ParametricMesh->P_Cells->at(i_cell).Index_ItsCornerVertices[(i_v+1)%4];
           //
           //Hsize//curr_Vindex->prev_Vindex
           double Hsize=(VerticesPosition[curr_Vindex][0]-VerticesPosition[prev_Vindex][0])*(VerticesPosition[curr_Vindex][0]-VerticesPosition[prev_Vindex][0]);
           Hsize=Hsize+(VerticesPosition[curr_Vindex][1]-VerticesPosition[prev_Vindex][1])*(VerticesPosition[curr_Vindex][1]-VerticesPosition[prev_Vindex][1]);
           Hsize=sqrt(Hsize);
           //
           if(Hsize>Hmax)
           {
               Hmax=Hsize;
           };
           if(Hsize<Hmin)
           {
               Hmin=Hsize;
           };
           //
           //CosTheta (x1, y1) (x2, y2)
           //
           double x1=VerticesPosition[curr_Vindex][0]-VerticesPosition[prev_Vindex][0];
           double y1=VerticesPosition[curr_Vindex][1]-VerticesPosition[prev_Vindex][1];
           double x2=VerticesPosition[next_Vindex][0]-VerticesPosition[curr_Vindex][0];
           double y2=VerticesPosition[next_Vindex][1]-VerticesPosition[curr_Vindex][1];
           //
           double CosTheta=(x1*x2+y1*y2)/sqrt((x1*x1+y1*y1)*(x2*x2+y2*y2));
           if(CosTheta>CosThetaMax)
           {
               CosThetaMax=CosTheta;
           }
           //Is_Concave (x1, y1, 0)X(x2, y2, 0)
           ConcaveLables.push_back(x1*y2-x2*y1);
       }

       //------------------------
       //
       ofHCellByCell<<i_cell<<" "<<Hmax<<" "<<Hmin<<endl;
       //
       ofCosTheta_CellbyCell<<i_cell<<" "<<CosThetaMax<<endl;
       //
       if((ConcaveLables[0]*ConcaveLables[1]>0)&&(ConcaveLables[0]*ConcaveLables[2]>0)&&(ConcaveLables[0]*ConcaveLables[3]>0))
       {
           ofIs_Concave_CellbyCell<<i_cell<<" "<<0<<endl;// it is convex
       }
       else
       {
           ofIs_Concave_CellbyCell<<i_cell<<" "<<1<<endl;// it is concave
       }
       //------------------------
    }

    ofHCellByCell.close();
    ofCosTheta_CellbyCell.close();
    ofIs_Concave_CellbyCell.close();
}

MSplineFunctions * MSplineFunctions::ModifyParameterizationBySpringModelAndBoundaryFixing(const double TimeSize, const double Hc, const double CoulombCoeff, const double epsilon0,
                                                       const unsigned n_u, const unsigned n_v, const unsigned MaxIterNum, Mesh * P_ParametricMesh,
                                                                          const  vector<vector<double> > & Coeff_Parametric_Map, vector<vector<double> > &NewCoeff_ParametricMap,
                                                                                          char * ModifyTheParameterizationComputationalProgress,
                                                                                          char * SpringMethodVerticesDistanceConvergenceEpsilons,
                                                                                          char * ModifiedParameterization_axl,
                                                                                          char * ModifiedParameterizationWithSpringModel_JacobianAxelMesh_axl,
                                                                                          char * ModifiedVerticesPosition_Axel_axl,
                                                                                          char * ModifiedParameterizationWithSpringModel_PhysicalAreas,
                                                                                          char * H_CellbyCell,
                                                                                          char * CosTheta_CellbyCell,
                                                                                          char * Is_Concave_CellbyCell)
{
    NewCoeff_ParametricMap.clear();

    ofstream ofProgress;

    ofProgress.open(ModifyTheParameterizationComputationalProgress);

    ofProgress<<"====Modify the parametric map with Spring model with fixing boundary===="<<endl;
    //InPut the mass vector
    unsigned VerNum=P_ParametricMesh->P_Vertices->size();
    vector<double> Mass(VerNum, 1.0);//the mass of each is the same and taken as 1.0
    ofProgress<<"Default: all the mass are set as 1"<<endl;

    /*-----------------------------------------------------------------------------*/
    ofProgress<<"MaxIterNum = "<<MaxIterNum<<endl;

    cout<<"MaxIterNum = "<<MaxIterNum<<endl;

    //the parameters of our model
    //TimeSize
    ofProgress<<"the size of time step = "<<TimeSize<<endl;
    cout<<"the size of time step = "<<TimeSize<<endl;

    //Hook's coeffient
    ofProgress<<"the Hook's coeffient = "<<Hc<<endl;
    cout<<"the Hook's coeffient = "<<Hc<<endl;

    //Coulomb Coeff
    ofProgress<<"The coeffient of Coulomb force is "<<CoulombCoeff<<endl;
    cout<<"The coeffient of Coulomb force is "<<CoulombCoeff<<endl;

    //Tolerance
    ofProgress<<"The tolerance of the modification parameterization's convergence = "<< epsilon0<<endl;
    cout<<"The tolerance of the modification parameterization's convergence = "<< epsilon0<<endl;

    // The loop for modifying
    double epsilon=epsilon0+1.0;
    unsigned int iternum = 0;



    //Compute the initial position of vertices
    vector<vector<double> >VerticesPosition;
    this->ComputeVerticesPositions(VerticesPosition, P_ParametricMesh);


    ofstream ofEpsilon;
    ofEpsilon.open(SpringMethodVerticesDistanceConvergenceEpsilons);


   // cout<<"Here here here 3332222"<<endl;

   // while(epsilon>epsilon0 && iternum<MaxIterNum+1)
   while(epsilon>epsilon0 && iternum<MaxIterNum)
    {   
        vector<vector<double> > DeltaPositionSpring;
        vector<vector<double> > DeltaPositionCoulomb;

        //Modify updated P_ParametricMap
        //vector<vector<double> > DeltaPositionSpring;
        DeltaPositionBasedOnSpringModel(Hc, TimeSize, Mass, VerticesPosition, P_ParametricMesh, DeltaPositionSpring);
        //vector<vector<double> > DeltaPositionCoulomb;
        DeltaPositionBasedOnBoundaryCoulombForce(CoulombCoeff, TimeSize, P_ParametricMesh, VerticesPosition, Mass, DeltaPositionCoulomb);
        //VerticesPosition=VerticesPosition+DeltaPosition
        UpDataVerticesPosition(VerticesPosition, DeltaPositionSpring);
        //VerticesPosition=VerticesPosition+DeltaPosition
        UpDataVerticesPosition(VerticesPosition, DeltaPositionCoulomb);
        //For epsilon
        //DeltaPositionSpring=DeltaPositionSpring+DeltaPositionCoulomb
        UpDataVerticesPosition(DeltaPositionSpring, DeltaPositionCoulomb);
        //Update iternum
        iternum=iternum+1;
        //Compute epsilon again
        epsilon=TotalDistance(DeltaPositionSpring);//Have divided by P_ParametricMesh->P_Vertices->size();
        //TimeStep--->scaler to 1s
        epsilon=epsilon/(TimeSize*TimeSize);
        ofEpsilon<<"iteration num = "<<iternum<<"  epsilon = "<<epsilon<<endl;

        //=========================================================================
        vector<vector<double> >().swap(DeltaPositionSpring);
        vector<vector<double> >().swap(DeltaPositionCoulomb);
    }
    ofEpsilon.close();


    MSplineFunctions* P_ParametricMap2=new MSplineFunctions(VerticesPosition, P_ParametricMesh, NewCoeff_ParametricMap);
    delete P_ParametricMap2;
    P_ParametricMap2=NULL;


    for(unsigned int i_v = 0; i_v < P_ParametricMesh->P_Vertices->size(); i_v ++)
    {
        if(!P_ParametricMesh->P_Vertices->at(i_v).Is_interiorVertex)//the i_v-th vertex is a boundary vertex
        {
            if(P_ParametricMesh->P_Vertices->at(i_v).Deg()<=3)//deg(the i_v-th vertex)=3
            {
                unsigned int TheFirstCellIndex=P_ParametricMesh->P_Vertices->at(i_v).ItsCellsToCoordinatesMap.begin()->first;
                //Which edge with an endpoint as the i-th vertex is an boundary edge?
                for(set<unsigned int>::const_iterator jt_v=P_ParametricMesh->P_Vertices->at(i_v).Index_ItsVertices.begin();
                    jt_v!=P_ParametricMesh->P_Vertices->at(i_v).Index_ItsVertices.end(); jt_v++)
                {
                    unsigned int j_v=*jt_v;
                    //Check the edge i_v-j_v is a boundary?
                    bool is_boundary=P_ParametricMesh->Is_ABoundaryEdge(i_v, j_v);
                    //in the view of the TheFirstCellIndex-th Cell,
                    //the edge i_vj_v is along s-direction or t-direction
                    if(is_boundary)
                    {
                        int Flag=P_ParametricMesh->P_Cells->at(TheFirstCellIndex).TheLocalDirection(i_v, j_v);
                        //s-direction (1), -s -direction (-1); t-direction (2), -t -direction (-2)
                        if(Flag==1||Flag==-1)//s-direction
                        {
                            unsigned int coeffindex=4*i_v+1;
                            for(unsigned int i=0; i<NewCoeff_ParametricMap.size(); i++)
                            {
                                NewCoeff_ParametricMap[i][coeffindex]=Coeff_Parametric_Map[i][coeffindex];
                            }
                        }
                        if(Flag==2||Flag==-2)//t-direction
                        {
                            unsigned int coeffindex=4*i_v+2;
                            for(unsigned int i=0; i<NewCoeff_ParametricMap.size(); i++)
                            {
                                NewCoeff_ParametricMap[i][coeffindex]=Coeff_Parametric_Map[i][coeffindex];
                            }
                        }
                    }
                }
            }
        }
    }



   MSplineFunctions* ReturnMap =new MSplineFunctions(NewCoeff_ParametricMap, *P_ParametricMesh);

    ////Print the new parametric map and its vertices
   PrintParametricMap(NewCoeff_ParametricMap, P_ParametricMesh, ModifiedParameterization_axl);
   PrintParametricMapPositionInfo(NewCoeff_ParametricMap, P_ParametricMesh, ModifiedVerticesPosition_Axel_axl);

   //ReturnMap->CoutThisMSplineFunctionsAsBSplineSurfaceAxelFile(P_ParametricMesh);

   ReturnMap->CoutThisMSplineFunctionsJacobianMeshAxelFile(P_ParametricMesh, n_u, n_v,
          ModifiedParameterizationWithSpringModel_JacobianAxelMesh_axl, ModifiedParameterizationWithSpringModel_PhysicalAreas);


   ReturnMap->PrintElementInformation(VerticesPosition,P_ParametricMesh, H_CellbyCell, CosTheta_CellbyCell, Is_Concave_CellbyCell);

    if(iternum>MaxIterNum)
    {
        cout<<"The iteration of modifying parameterization doesn't converge with the tolerance = "<<epsilon0<<endl;
        ofProgress<<"The iteration of modifying parameterization doesn't converge with the tolerance = "<<epsilon0<<endl;
    }

    ofProgress.close();

    return ReturnMap;
}


//==========================================================================
//MSplineFunctions * MSplineFunctions::SetInitialVerticesPositions(const vector<double> &V0, const Mesh *P_FEMMesh,
//                                               vector<vector<double> >& CoeffRefinedParametricMap)
MSplineFunctions * MSplineFunctions::SetInitialVerticesPositions(Mesh *P_FEMMesh,
                                               vector<vector<double> >& CoeffRefinedParametricMap)
{
//    for(unsigned int i_v = 0; i_v < P_FEMMesh->P_Vertices->size(); i_v ++)
//    {
//        if(P_FEMMesh->P_Vertices->at(i_v).Is_interiorVertex)//Is a interior vertex
//        {
//            //CoeffRefinedParametricMap[0][4*i_v]
//            for(unsigned int i=0; i<CoeffRefinedParametricMap.size(); i++)
//            {
//                CoeffRefinedParametricMap[i][4*i_v] = V0[i];
//            }
//            for(unsigned j=1; j<4; j++)
//            {
//                for(unsigned i=0; i<CoeffRefinedParametricMap.size(); i++)
//                {
//                    CoeffRefinedParametricMap[i][4*i_v+j]=0;
//                }
//            }
//        }
//    }
    for(unsigned int i_v = 0; i_v < P_FEMMesh->P_Vertices->size(); i_v ++)
    {
        if(!P_FEMMesh->P_Vertices->at(i_v).Is_interiorVertex)//the i_v-th vertex is a boundary vertex
        {
            if(P_FEMMesh->P_Vertices->at(i_v).Deg()<=3)//deg(the i_v-th vertex)=3
            {
                unsigned int TheFirstCellIndex=P_FEMMesh->P_Vertices->at(i_v).ItsCellsToCoordinatesMap.begin()->first;
                //Which edge with an endpoint as the i-th vertex is an boundary edge?
                for(set<unsigned int>::const_iterator jt_v=P_FEMMesh->P_Vertices->at(i_v).Index_ItsVertices.begin();
                    jt_v!=P_FEMMesh->P_Vertices->at(i_v).Index_ItsVertices.end(); jt_v++)
                {
                    unsigned int j_v=*jt_v;
                    //Check the edge i_v-j_v is a boundary?
                    bool is_boundary=P_FEMMesh->Is_ABoundaryEdge(i_v, j_v);
                    //in the view of the TheFirstCellIndex-th Cell,
                    //the edge i_vj_v is along s-direction or t-direction
                    if(!is_boundary)//If it is not a boundary edge
                    {
                        int Flag=P_FEMMesh->P_Cells->at(TheFirstCellIndex).TheLocalDirection(i_v, j_v);
                        //s-direction (1), -s -direction (-1); t-direction (2), -t -direction (-2)
                        if(Flag==1||Flag==-1)//s-direction
                        {
                            unsigned int coeffindex=4*i_v+1;
                            for(unsigned int i=0; i<CoeffRefinedParametricMap.size(); i++)
                            {
                                //NewCoeff_ParametricMap[i][coeffindex]=Coeff_Parametric_Map[i][coeffindex];
                                CoeffRefinedParametricMap[i][coeffindex]=0;
                            }
                        }
                        if(Flag==2||Flag==-2)//t-direction
                        {
                            unsigned int coeffindex=4*i_v+2;
                            for(unsigned int i=0; i<CoeffRefinedParametricMap.size(); i++)
                            {
                                //NewCoeff_ParametricMap[i][coeffindex]=Coeff_Parametric_Map[i][coeffindex];
                                CoeffRefinedParametricMap[i][coeffindex]=0;
                            }
                        }
                    }
                }
            }
        }
        else//The i_v-th vertex is an interior vertex
        {
            unsigned int coeffindex=4*i_v;
            for(unsigned int i=0; i<CoeffRefinedParametricMap.size(); i++)
            {
                CoeffRefinedParametricMap[i][coeffindex+1]=0;
                CoeffRefinedParametricMap[i][coeffindex+2]=0;
                CoeffRefinedParametricMap[i][coeffindex+3]=0;
            }
        }
    }

    //
    MSplineFunctions* P_ReturnMap=new MSplineFunctions(CoeffRefinedParametricMap, *P_FEMMesh);

    return P_ReturnMap;
}
