//#include "StdAfx.h"
#include <iostream>
using namespace std;

#include "Bases.h"
#include "Basis.h"

#include "Mesh.h"
#include "BasesVertex.h"
#include "Functions.h"
#include "HermiteVertex.h"
#include "Coordinate.h"
#include "Vertex.h"
#include "HermiteCell.h"

Bases::Bases(void)
{
}

Bases::~Bases(void)
{
}

Bases::Bases(const Mesh & CurrentMesh)
{
	P_BasesSet.clear();

    for(unsigned int i=0; i<CurrentMesh.P_Vertices->size(); i++)
	{//For each vertex
		BasesVertex * P_BasesV =new BasesVertex(i,CurrentMesh);///Implement in BasesVertex
		for (unsigned int j=0; j<P_BasesV->P_Base_Vertex.size(); j++)
		{
			P_BasesSet.push_back(P_BasesV->P_Base_Vertex[j]);
		}
		P_BasesV=NULL;
	}
}

void Bases::Cout_Bases(const Mesh & CurrentMesh)
{
	for(unsigned int i=0; i<P_BasesSet.size(); i++)
	{
		(*P_BasesSet[i]).Cout_basis(CurrentMesh);
        //system("PAUSE");
        cin.get();
	}
};

//----------------------------------------------------------
void Bases::HVerticesData_All_bases(vector<vector<HermiteVertex *> > &P_HVertices, const Mesh & CurrentMesh)
{
	P_HVertices.clear();
    for (unsigned int i=0; i<CurrentMesh.P_Vertices->size(); i++)
	{
		vector<HermiteVertex*> P_Vi;
		vector<vector<double> > HermiteDatas;
		Set_HermiteData_Vertex(i, CurrentMesh, HermiteDatas);
		//HermiteVertex(unsigned int index_vertex, const Mesh & CurrentMesh, const vector<double> & Hermitedata);
		for (unsigned int j=0; j<HermiteDatas.size(); j++)
		{
			HermiteVertex * P_V=new HermiteVertex(i, CurrentMesh, HermiteDatas[j]);
			P_Vi.push_back(P_V);
			P_V=NULL;
		}
		P_HVertices.push_back(P_Vi);
	}
}
//--------------------
basis * Bases::Return_Basis(unsigned int Index_Vertex, unsigned int i_H, const vector<vector<HermiteVertex *> > &P_HVertices, const Mesh & CurrentMesh)
{
	basis * P_Basis=new basis;
	//Index_basis
	P_Basis->Index_basis=Index_Vertex*4+i_H;//--Here, the "basis" functions at each basis vertex is always 4, 
	//although there are some zero "bases" at irregular vertices.

	//--For each cell of this vertex, Gernerate a HermiteVertex for this vertex
	HermiteVertex * P_HV=P_HVertices[Index_Vertex][i_H];

	//P_HV->Cout_HermiteVertex();

	//Generate basis functions at this vertex
	P_Basis->P_HermiteData_Cells.clear();
	//==========================
	unsigned int index=0;
    for (map<unsigned int, Coordinate>::const_iterator it=CurrentMesh.P_Vertices->at(Index_Vertex).ItsCellsToCoordinatesMap.begin(); it!=CurrentMesh.P_Vertices->at(Index_Vertex).ItsCellsToCoordinatesMap.end(); it++)
	{//=P_HV->P_ItsHermiteData.size()
		unsigned int index_cell=(*it).first;

		HermiteCell *P_HC = new HermiteCell(Index_Vertex, index_cell, P_HV->P_ItsHermiteData[index], CurrentMesh);
		P_Basis->P_HermiteData_Cells.push_back(P_HC);

		index=index+1;
		P_HC=NULL;
	}

	P_HV=NULL;

	return P_Basis;
}

basis * Bases::Return_Basis_Cell(unsigned int Index_Vertex, unsigned int i_H, unsigned int Index_Cell, const vector<vector<HermiteVertex *> > &P_HVertices, const Mesh & CurrentMesh)
{
	int Index=-1;
	unsigned int p=0;

    for (map<unsigned int, Coordinate>::const_iterator it=CurrentMesh.P_Vertices->at(Index_Vertex).ItsCellsToCoordinatesMap.begin(); it!=CurrentMesh.P_Vertices->at(Index_Vertex).ItsCellsToCoordinatesMap.end(); it++)
	{
		if (Index_Cell==(*it).first)
		{
			Index=p;
			break;
		}
		p=p+1;
	}
	//===========================================
	if (Index==-1)
	{
		return NULL;// the Index_Vertex*4+i_H-th basis does not cover the Index_Cell-th cell
	}
	else
	{
		basis * P_B=new basis;

		//Index_basis
		P_B->Index_basis=Index_Vertex*4+i_H;//--Here, the "basis" functions at each basis vertex is always 4, 
		//although there are some zero "bases" at irregular vertices.

		//--For each cell of this vertex, Gernerate a HermiteVertex for this vertex
		HermiteVertex * P_HV=P_HVertices[Index_Vertex][i_H];

		//P_HV->Cout_HermiteVertex();

		//Generate basis functions at this vertex
		P_B->P_HermiteData_Cells.clear();
		//==========================

		HermiteCell *P_HC = new HermiteCell(Index_Vertex, Index_Cell, P_HV->P_ItsHermiteData[Index], CurrentMesh);
		P_B->P_HermiteData_Cells.push_back(P_HC);
		P_HC=NULL;

		P_HV=NULL;

		return P_B;

	}

}
