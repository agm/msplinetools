#include "Vertex.h"
#include "Coordinate.h"
Vertex::Vertex(void)
{
}

Vertex::~Vertex(void)
{
}
Vertex::Vertex(const Vertex & V)
{
// 	bool Is_interiorVertex;//Is this vertex an interior vertex?
	Is_interiorVertex=V.Is_interiorVertex;

	//map<unsigned int, Coordinate> ItsCellsToCoordinatesMap;
	ItsCellsToCoordinatesMap.clear();
	for (map<unsigned int, Coordinate>::const_iterator it=V.ItsCellsToCoordinatesMap.begin(); it!=V.ItsCellsToCoordinatesMap.end(); it++)
	{
		ItsCellsToCoordinatesMap.insert(pair<unsigned int, Coordinate> ((*it).first, (*it).second));
	}

// 	set<unsigned int> Index_ItsVertices;
	Index_ItsVertices.clear();
	for (set<unsigned int>::const_iterator it=V.Index_ItsVertices.begin(); it!=V.Index_ItsVertices.end(); it++)
	{
		Index_ItsVertices.insert(*it);
	}
}

unsigned int Vertex::Deg()
{
    return Index_ItsVertices.size();
}
