#include <iostream>
using namespace std;

#include "HermiteCell.h"
#include "HermiteData.h"
#include "Mesh.h"
#include "Cell.h"

#include "MSplineEvaluation.h"

HermiteCell::HermiteCell(void)
{
}

HermiteCell::~HermiteCell(void)
{
}

HermiteCell::HermiteCell(const HermiteCell & HC) 
{
// 	unsigned int Index_InitialCell;
	Index_InitialCell=HC.Index_InitialCell;

// 	unsigned int Index_Cell;
	Index_Cell=HC.Index_Cell;

// 	vector<HermiteData *> P_HermiteAtCornerVertex;
    for(unsigned int i=0; i<P_HermiteAtCornerVertex.size(); i++)
    {
        delete P_HermiteAtCornerVertex[i];
    }
	P_HermiteAtCornerVertex.clear();

	for (unsigned int i=0; i<HC.P_HermiteAtCornerVertex.size(); i++)
	{
        HermiteData * P_HD=new HermiteData(*(HC.P_HermiteAtCornerVertex[i]));
        P_HermiteAtCornerVertex.push_back(P_HD);
        P_HD=NULL;
	};
}

//==========================================
void HermiteCell::ResetHermiteCell(const HermiteCell & HC)
{
    // 	unsigned int Index_InitialCell;
        Index_InitialCell=HC.Index_InitialCell;

    // 	unsigned int Index_Cell;
        Index_Cell=HC.Index_Cell;

    // 	vector<HermiteData *> P_HermiteAtCornerVertex;
        P_HermiteAtCornerVertex.clear();
        for (unsigned int i=0; i<HC.P_HermiteAtCornerVertex.size(); i++)
        {
            P_HermiteAtCornerVertex.push_back(HC.P_HermiteAtCornerVertex[i]);
        };
}
//==========================================

//Generate a HermiteCell such that it correspondents to the index_cell-th cell 
//with the index_vertex-th vertex's Hermitedata as *P_HD_V and other vertices' Hermitedata=[0,0,0,0]
HermiteCell::HermiteCell(unsigned int index_vertex, unsigned int index_cell, HermiteData * P_HD_V, const Mesh & CurrentMesh)
{
	//HermiteCell *P_HC = new HermiteCell(P_HV, CurrentMesh);
    Index_InitialCell=CurrentMesh.P_Cells->at(index_cell).Index_InitialCell;
	Index_Cell=index_cell;
	//------------------------------------

	HermiteData * P_HD=new HermiteData(0.0, Index_InitialCell);
	//------------------------------------

	P_HermiteAtCornerVertex.clear();
	//------------------------------------
    for (unsigned int i=0; i<CurrentMesh.P_Cells->at(index_cell).Index_ItsCornerVertices.size(); i++)
	{
        if (CurrentMesh.P_Cells->at(index_cell).Index_ItsCornerVertices[i]==index_vertex)
		{
			P_HermiteAtCornerVertex.push_back(P_HD_V);
		}
		else
		{
			P_HermiteAtCornerVertex.push_back(P_HD);
		}
	}
	P_HD=NULL;
}

//----overload
//multiply scalar
HermiteCell HermiteCell::Lambda(const double coeff)
{
    HermiteCell HC(*this);

    for (unsigned int i=0; i<P_HermiteAtCornerVertex.size(); i++)
    {
        HC.P_HermiteAtCornerVertex[i]->ItsHermiteData[0]=coeff*P_HermiteAtCornerVertex[i]->ItsHermiteData[0];
        HC.P_HermiteAtCornerVertex[i]->ItsHermiteData[1]=coeff*P_HermiteAtCornerVertex[i]->ItsHermiteData[1];
        HC.P_HermiteAtCornerVertex[i]->ItsHermiteData[2]=coeff*P_HermiteAtCornerVertex[i]->ItsHermiteData[2];
        HC.P_HermiteAtCornerVertex[i]->ItsHermiteData[3]=coeff*P_HermiteAtCornerVertex[i]->ItsHermiteData[3];
    }
    return HC;

};
//+
HermiteCell HermiteCell::operator+(const HermiteCell &right)
{
    HermiteCell HC(*this);

//    //For testing---the Cout Function
//    HC.Cout_HermiteCell();
//    cout<<"*this------>"<<endl;
//    this->Cout_HermiteCell();
//    cout<<"the right side  "<<endl;
//    cout<<right.P_HermiteAtCornerVertex[0]->ItsHermiteData[0]<<endl;
//    cout<<right.P_HermiteAtCornerVertex[0]->ItsHermiteData[1]<<endl;
//    cout<<right.P_HermiteAtCornerVertex[0]->ItsHermiteData[2]<<endl;
//    cout<<right.P_HermiteAtCornerVertex[0]->ItsHermiteData[3]<<endl;

//    cout<<right.P_HermiteAtCornerVertex[1]->ItsHermiteData[0]<<endl;
//    cout<<right.P_HermiteAtCornerVertex[1]->ItsHermiteData[1]<<endl;
//    cout<<right.P_HermiteAtCornerVertex[1]->ItsHermiteData[2]<<endl;
//    cout<<right.P_HermiteAtCornerVertex[1]->ItsHermiteData[3]<<endl;

//    cout<<right.P_HermiteAtCornerVertex[2]->ItsHermiteData[0]<<endl;
//    cout<<right.P_HermiteAtCornerVertex[2]->ItsHermiteData[1]<<endl;
//    cout<<right.P_HermiteAtCornerVertex[2]->ItsHermiteData[2]<<endl;
//    cout<<right.P_HermiteAtCornerVertex[2]->ItsHermiteData[3]<<endl;

//    cout<<right.P_HermiteAtCornerVertex[3]->ItsHermiteData[0]<<endl;
//    cout<<right.P_HermiteAtCornerVertex[3]->ItsHermiteData[1]<<endl;
//    cout<<right.P_HermiteAtCornerVertex[3]->ItsHermiteData[2]<<endl;
//    cout<<right.P_HermiteAtCornerVertex[3]->ItsHermiteData[3]<<endl;

//    cout<<"P_HermiteAtCornerVertex.size()=  "<<P_HermiteAtCornerVertex.size()<<endl;

	for (unsigned int i=0; i<P_HermiteAtCornerVertex.size(); i++)
	{
        HC.P_HermiteAtCornerVertex[i]->ItsHermiteData[0]=P_HermiteAtCornerVertex[i]->ItsHermiteData[0]+right.P_HermiteAtCornerVertex[i]->ItsHermiteData[0];
        HC.P_HermiteAtCornerVertex[i]->ItsHermiteData[1]=P_HermiteAtCornerVertex[i]->ItsHermiteData[1]+right.P_HermiteAtCornerVertex[i]->ItsHermiteData[1];
        HC.P_HermiteAtCornerVertex[i]->ItsHermiteData[2]=P_HermiteAtCornerVertex[i]->ItsHermiteData[2]+right.P_HermiteAtCornerVertex[i]->ItsHermiteData[2];
        HC.P_HermiteAtCornerVertex[i]->ItsHermiteData[3]=P_HermiteAtCornerVertex[i]->ItsHermiteData[3]+right.P_HermiteAtCornerVertex[i]->ItsHermiteData[3];
	}

//    //---For testing--the Cout Function
//    HC.Cout_HermiteCell();

    //--------------------
    return HC;

}
//-
HermiteCell HermiteCell::operator-(const HermiteCell &right)
{
    HermiteCell HC(*this);
	for (unsigned int i=0; i<P_HermiteAtCornerVertex.size(); i++)
	{
        HC.P_HermiteAtCornerVertex[i]->ItsHermiteData[0]=P_HermiteAtCornerVertex[i]->ItsHermiteData[0]-right.P_HermiteAtCornerVertex[i]->ItsHermiteData[0];
        HC.P_HermiteAtCornerVertex[i]->ItsHermiteData[1]=P_HermiteAtCornerVertex[i]->ItsHermiteData[1]-right.P_HermiteAtCornerVertex[i]->ItsHermiteData[1];
        HC.P_HermiteAtCornerVertex[i]->ItsHermiteData[2]=P_HermiteAtCornerVertex[i]->ItsHermiteData[2]-right.P_HermiteAtCornerVertex[i]->ItsHermiteData[2];
        HC.P_HermiteAtCornerVertex[i]->ItsHermiteData[3]=P_HermiteAtCornerVertex[i]->ItsHermiteData[3]-right.P_HermiteAtCornerVertex[i]->ItsHermiteData[3];
	}

    return HC;

}
//=
HermiteCell& HermiteCell::operator=(const HermiteCell &right)
{
    //HermiteCell *P_HC= new HermiteCell(*this);
    this->Index_Cell=right.Index_Cell;
    this->Index_InitialCell=right.Index_InitialCell;
    for(unsigned int i=0; i<this->P_HermiteAtCornerVertex.size(); i++)
    {
        delete this->P_HermiteAtCornerVertex[i];
    }
    this->P_HermiteAtCornerVertex.clear();
    //=====================================

    for (unsigned int i=0; i<right.P_HermiteAtCornerVertex.size(); i++)
	{
        HermiteData * P_HD=new HermiteData();
        P_HD->Index_InitialCell=right.P_HermiteAtCornerVertex[i]->Index_InitialCell;
        P_HD->ItsHermiteData.push_back(right.P_HermiteAtCornerVertex[i]->ItsHermiteData[0]);
        P_HD->ItsHermiteData.push_back(right.P_HermiteAtCornerVertex[i]->ItsHermiteData[1]);
        P_HD->ItsHermiteData.push_back(right.P_HermiteAtCornerVertex[i]->ItsHermiteData[2]);
        P_HD->ItsHermiteData.push_back(right.P_HermiteAtCornerVertex[i]->ItsHermiteData[3]);
        this->P_HermiteAtCornerVertex.push_back(P_HD);
        P_HD=NULL;
	}

    return *this;

};


HermiteCell::HermiteCell(unsigned int index_InitialCell, unsigned int index_Cell)
{
	Index_InitialCell = index_InitialCell;
	Index_Cell = index_Cell;
	//P_HermiteAtCornerVertex.clear();
	//vector<HermiteData *> P_HermiteAtCornerVertex;

	//cout<<Index_InitialCell<<" "<<Index_Cell<<endl;
	//cout<<P_HermiteAtCornerVertex.size()<<endl;

	for (unsigned int i=0; i<P_HermiteAtCornerVertex.size(); i++)
	{
		//HermiteData(double d, unsigned int Index);
		delete P_HermiteAtCornerVertex[i];
	}
	P_HermiteAtCornerVertex.clear();
	for(unsigned int i=0; i<4; i++)
	{
		HermiteData * P_H=new HermiteData(0,index_InitialCell);
		P_HermiteAtCornerVertex.push_back(P_H);
		P_H=NULL;
	}

    //

}

void HermiteCell::Cout_HermiteCell()
{
	cout<<"Index_InitialCell = "<<Index_InitialCell<<endl;
	cout<<"Index_Cell = "<< Index_Cell<<endl;
	for (unsigned int i=0; i<P_HermiteAtCornerVertex.size(); i++)
	{
		(*P_HermiteAtCornerVertex[i]).Cout_HermiteData();
	}
}

//======
//For One Point coodinateST=[s0, t0]
void HermiteCell::Evaluate(int DerOrderu, int DerOrderv, const vector<double> & coordinateST, double &Values, const double * PCellSize)
{
    //--------CellSize---------------------
    //unsigned int IndexCell=this->Index_Cell;
    double a=PCellSize[0];
    double b=PCellSize[1];
    double c=PCellSize[2];
    double d=PCellSize[3];
    //-------------------------------------

    //------Generate H Matrix
    vector<vector<double> > H;
    //----Hermite datas of this HermiteCell
    double f01=this->P_HermiteAtCornerVertex[0]->ItsHermiteData[0];
    double f11=this->P_HermiteAtCornerVertex[0]->ItsHermiteData[1];
    double f21=this->P_HermiteAtCornerVertex[0]->ItsHermiteData[2];
    double f31=this->P_HermiteAtCornerVertex[0]->ItsHermiteData[3];

    double f02=this->P_HermiteAtCornerVertex[1]->ItsHermiteData[0];
    double f12=this->P_HermiteAtCornerVertex[1]->ItsHermiteData[1];
    double f22=this->P_HermiteAtCornerVertex[1]->ItsHermiteData[2];
    double f32=this->P_HermiteAtCornerVertex[1]->ItsHermiteData[3];

    double f03=this->P_HermiteAtCornerVertex[2]->ItsHermiteData[0];
    double f13=this->P_HermiteAtCornerVertex[2]->ItsHermiteData[1];
    double f23=this->P_HermiteAtCornerVertex[2]->ItsHermiteData[2];
    double f33=this->P_HermiteAtCornerVertex[2]->ItsHermiteData[3];

    double f04=this->P_HermiteAtCornerVertex[3]->ItsHermiteData[0];
    double f14=this->P_HermiteAtCornerVertex[3]->ItsHermiteData[1];
    double f24=this->P_HermiteAtCornerVertex[3]->ItsHermiteData[2];
    double f34=this->P_HermiteAtCornerVertex[3]->ItsHermiteData[3];
    //----H-----
    vector<double> Hi;
    Hi.push_back(f01);
    Hi.push_back((d-c)*f21);
    Hi.push_back((d-c)*f24);
    Hi.push_back(f04);
    H.push_back(Hi);

    Hi.clear();
    Hi.push_back((b-a)*f11);
    Hi.push_back((b-a)*(d-c)*f31);
    Hi.push_back((b-a)*(d-c)*f34);
    Hi.push_back((b-a)*f14);
    H.push_back(Hi);

    Hi.clear();
    Hi.push_back((b-a)*f12);
    Hi.push_back((b-a)*(d-c)*f32);
    Hi.push_back((b-a)*(d-c)*f33);
    Hi.push_back((b-a)*f13);
    H.push_back(Hi);

    Hi.clear();
    Hi.push_back(f02);
    Hi.push_back((d-c)*f22);
    Hi.push_back((d-c)*f23);
    Hi.push_back(f03);
    H.push_back(Hi);

    //Generate [H_0(x), H_1(x), H_2(x), H_3(x)] for all the points, where H_i(x) is one of the Hermite bases.
    vector<double> s, t;
    double u=coordinateST[0];
    double v=coordinateST[1];
    s.push_back((u-a)/(b-a));
    t.push_back((v-c)/(d-c));

    vector<vector<double> > HValues_S, HValues_T;

    HermiteBaseValues(a, b, DerOrderu, s, HValues_S);
    HermiteBaseValues(c, d, DerOrderv, t, HValues_T);
    //-------------------------------------
    //double Value=0.0;
    Values=0.0;
    for(unsigned int i=0; i<4; i++)
    {
        for(unsigned int j=0; j<4; j++)
        {
            Values=Values+HValues_S[0][i]*H[i][j]*HValues_T[0][j];
        };
    };
    //

}
