#ifndef BASESVERTEX_H
#define BASESVERTEX_H

#include <vector>
using namespace std;
//-----------------
class basis;
class Mesh;
class Coordinate;
//-----------------

class BasesVertex
{
public:
	unsigned int Index_ItsVertex;
	vector<basis *> P_Base_Vertex;
public:
	BasesVertex(void);
	~BasesVertex(void);

    /**
     * @brief BasesVertex: Generate the bases at the index_vertex-th vertex
     * @param index_vertex
     * @param CurrentMesh
     */
	BasesVertex(unsigned int index_vertex, const Mesh & CurrentMesh);

    /**
     * @brief BasesVertex: Generate bases at the index_vertex-th vertex and over the index_cell-th cell
     * @param index_vertex
     * @param index_cell
     * @param CurrentMesh
     */
	BasesVertex(unsigned int index_vertex, unsigned int index_cell, const Mesh & CurrentMesh);

public:
	void Cout_BasesVertex(const Mesh & CurrentMesh);
    //void Evaluation(unsigned int i, 2, 0, CenterVCoordinateSet, CommonCellIndex, Values, *P_CMesh);
   void Evaluation(unsigned int iBasisPatch, int DerOrderu, int DerOrderv, const vector<Coordinate*> & Pcoordinate_Ps,
                   unsigned int Index_Cell, vector<double> &Values, const Mesh &CurrentMesh);
   unsigned int BasisIndex(unsigned int i);
};

#endif
