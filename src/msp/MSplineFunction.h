#ifndef MSPLINEFUNCTION_H
#define MSPLINEFUNCTION_H

#include <vector>
#include <map>
using namespace std;
//--------------------
class MSplineFunctionCell;
class PointSet;
class Mesh;
class Bases;
class Coordinate;
//--------------------

class MSplineFunction
{
public:
	vector<MSplineFunctionCell *> P_MSpline; 
public:
	MSplineFunction(void);
	~MSplineFunction(void);
    /**
     * @brief MSplineFunction: Generate a "zero" MSplineFunction
     * @param CurrentMesh
     */
    MSplineFunction(const Mesh & CurrentMesh);
	MSplineFunction(const vector<double> &Coeff, const Bases &BasisSet, const Mesh & CurrentMesh);
	MSplineFunction(const vector<double> &Coeff, const Mesh & CurrentMesh);
    MSplineFunction(unsigned int indexBasis, const Mesh & CurrentMesh);

public:

    void Evaluation(int DerOrderu, int DerOrderv, const vector<PointSet> & PsetS, vector<vector<double> > &Values, const Mesh & CurrentMesh);

    void Evaluation(int DerOrderu, int DerOrderv, const vector<Coordinate*> &Pcoordinate_Ps, vector<double> &Values, const unsigned int index_cell, const Mesh & CurrentMesh);

    void Cout_MSplineFunction();

    void ModifyMSplineFunction(const vector<double> &Coeff, unsigned int Index_Cell, const Mesh & CurrentMesh);

    void BSplineCoeffAxelOrder(unsigned int i_cell, double *CellSize, vector<double> &Coeffi);
public:
//    void ResetMSplineFunction(const vector<double> &Coeff, const Bases &BasisSet, const Mesh & CurrentMesh);
//    void ResetMSplineFunction(const vector<double> &Coeff, const Mesh & CurrentMesh);
private:
	//void MSplineFunctionMap(map<unsigned int, MSplineFunctionCell*> &MSplineFunMap);//MSplineFunction--->map<unsigned int(the index of cell), MSplineFunctionCell*>
public:

};

#endif
