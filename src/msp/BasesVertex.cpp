#include <iostream>

using namespace std;

#include "BasesVertex.h"
#include "Basis.h"

#include "Functions.h"

BasesVertex::BasesVertex(void)
{
}

BasesVertex::~BasesVertex(void)
{
}

BasesVertex::BasesVertex(unsigned int index_vertex, const Mesh & CurrentMesh)
{
	//Index_ItsVertex:
	Index_ItsVertex=index_vertex;

	//vector<basis *> P_Base_Vertex
	P_Base_Vertex.clear();
	//1.Set HermiteData for this vertex
	vector<vector<double> > HermiteDatas;
	Set_HermiteData_Vertex(index_vertex, CurrentMesh, HermiteDatas);
	//For each HermiteData at this vertex, solve the corresponding basis function
	for (unsigned int i_H=0; i_H<HermiteDatas.size(); i_H++)
    {
        //Generate the basis corresponding to HermiteDatas[i_H] at the index_vertex-th vertex
        basis * P_B=new basis(index_vertex,i_H, CurrentMesh, HermiteDatas[i_H]);
        P_Base_Vertex.push_back(P_B);
        P_B=NULL;
	}
}

BasesVertex::BasesVertex(unsigned int index_vertex, unsigned int index_cell, const Mesh & CurrentMesh)
{
	//Index_ItsVertex:
	Index_ItsVertex=index_vertex;

	//vector<basis *> P_Base_Vertex
	P_Base_Vertex.clear();
	//1.Set HermiteData for this vertex
	vector<vector<double> > HermiteDatas;
	Set_HermiteData_Vertex(index_vertex, CurrentMesh, HermiteDatas);
	//For each HermiteData at this vertex, solve the corresponding basis function
	for (unsigned int i_H=0; i_H<HermiteDatas.size(); i_H++)
	{
		//Generate the basis corresponding to HermiteDatas[i_H] at the index_vertex-th vertex
        basis * P_B=new basis(index_vertex, i_H, index_cell, CurrentMesh, HermiteDatas[i_H]);
		
		P_Base_Vertex.push_back(P_B);
		P_B=NULL;
	}
}

void BasesVertex::Cout_BasesVertex(const Mesh & CurrentMesh)
{
	cout<<"Index_ItsVertex = "<<Index_ItsVertex<<endl;
	for (unsigned int i=0; i<P_Base_Vertex.size(); i++)
	{
		(*P_Base_Vertex[i]).Cout_basis(CurrentMesh);
        //system("PAUSE");
        cin.get();
        
	}
}


void BasesVertex::Evaluation(unsigned int iBasisPatch, int DerOrderu, int DerOrderv, const vector<Coordinate*> & Pcoordinate_Ps,
                                  unsigned int Index_Cell, vector<double> &Values, const Mesh &CurrentMesh)
{
    this->P_Base_Vertex.at(iBasisPatch)->Evaluation(DerOrderu, DerOrderv, Pcoordinate_Ps, Index_Cell, Values, CurrentMesh);
}

unsigned int BasesVertex::BasisIndex(unsigned int i)
{
    return this->P_Base_Vertex[i]->Index_basis;
}
