#ifndef FEM_PRINT_H
#define FEM_PRINT_H

//PETSc
#include <petsc.h>
#include <petscksp.h>

#include "msp/FrameTransferMatrix.h"
#include "msp/Mesh.h"
#include "msp/MSplineFunctions.h"

static void PrintAxelSpheres(const vector< vector<double> >& TriangulesCenters, const vector<double> & Zs, char* filename)
{
    //cout<< "In PrintAxelSpheres"<<endl;

    ofstream ofSphere;
    ofSphere.open(filename);

    ofSphere<<"<axl>"<<endl;

    for(unsigned int i=0; i<TriangulesCenters.size(); i++)
    {

        ofSphere<<"<sphere size=\"0.01\" shader=\"\" name=\"axlSphere\" color=\"225 0 0 1\">"<<endl;
        ofSphere<<"<center>"<<TriangulesCenters[i][0]<<" "<<TriangulesCenters[i][1]<<" "<<Zs[i]<<" </center>"<<endl;
        ofSphere<<"<radius>"<< 0.05<<"</radius>"<<endl;
        ofSphere<<"</sphere>"<<endl;
    }
    ofSphere<<"</axl>"<<endl;
    ofSphere.close();

}

//========================================================================
static void PrintAxelPlanes(const vector< vector<double> >& TriangulesCenters, const vector<double> & Zs, char* filename)
{

    ofstream ofPlane;
    ofPlane.open(filename);

    ofPlane<<"<axl>"<<endl;

    for(unsigned int i=0; i<TriangulesCenters.size(); i++)
    {

        ofPlane<<"<plane size=\"0.00001\" shader=\"\" name=\"axlPlane\" color=\"224 224 224 1\">"<<endl;
        ofPlane<<"<point>"<<TriangulesCenters[i][0]<<" "<<TriangulesCenters[i][1]<<" "<<Zs[i]<<" </point>"<<endl;
        ofPlane<<"<normal>"<<0<<" "<<0<<" "<<1<<"</normal>"<<endl;
        ofPlane<<"</plane>"<<endl;
    }
    ofPlane<<"</axl>"<<endl;
    ofPlane.close();

}

//===============================================================================
static void PrintfCoeffMatrix(const vector<vector<double> > &coeff_matrix)
{
        unsigned int matrix_size=coeff_matrix.size();

        ofstream ofmatrix;
        ofmatrix.open("matrix.txt");
        for (unsigned int i=0; i<matrix_size;i++)
        {
                for(unsigned int j=0; j<matrix_size;j++)
                        ofmatrix<<coeff_matrix[i][j]<<"  ";
                ofmatrix<<endl;
        }
        ofmatrix.close();
}


//=========================================================================
static void PrintLoadVector(const vector<double> &load_vect)
{
        unsigned int matrix_size=load_vect.size();

        ofstream ofload;
        ofload.open("load.txt");
        for (unsigned int i=0;i<matrix_size;i++)
        {
                ofload<<load_vect[i]<<"  "<<endl;
        }
        ofload<<endl;
        ofload.close();
}
//========================================================================
static void PrintSolution(const Vec &m_solu)
{
    PetscInt m_size;
    VecGetSize(m_solu, &m_size);
    ofstream ofsolu;
    ofsolu.open("FEMSolution.txt");
    for (PetscInt i=0; i<m_size; i++)
    {
        PetscScalar v;
        PetscInt Ip=i;
        VecGetValues(m_solu,1,&Ip,&v);
            ofsolu<<v<<" "<<endl;
    }
    ofsolu<<endl;
    ofsolu.close();
}
//==========================================
static void PrintLoadVector(const Vec &LoadVec)
{
    PetscInt m_size;
    VecGetSize(LoadVec, &m_size);
    ofstream ofsolu;
    ofsolu.open("LoadVec.txt");
    for (PetscInt i=0; i<m_size; i++)
    {
        PetscScalar v;
        PetscInt Ip=i;
        VecGetValues(LoadVec,1,&Ip,&v);
            ofsolu<<v<<" "<<endl;
    }
    ofsolu<<endl;
    ofsolu.close();
}
//========================================================================
static void PrintErrors(const vector<double> &cell_errors,const double totol_Error,
                        const vector<double> & cell_errorsResidualMesh, const double total_Error_ResidualMesh,
                        const vector<double> &cell_errorsPhysDom, const double total_ErrorPhysDom,
                        const vector<double> & cell_errorsResidualPhsicalDom, const double total_Error_ResidualPhsicalDom,
                        const vector<double> & CellErrEnergyNormPhysDom, const double total_ErrEnergyNormPhysDom,
                        const vector<double> & CellErrEnergyNormMesh, const double total_ErrEnergyNormMesh)
{
  /*      ofstream ofErrors;
        ofErrors.open("L2ErrorsOverCellsOnMesh.txt");
        for (unsigned int i_error=0; i_error<cell_errors.size(); i_error++)
        {
                ofErrors<<cell_errors[i_error]<<" "<<endl;
        }
        ofErrors<<endl;
        ofErrors.close();
        //=====
        ofErrors.open("L2ResidualErrorsOverCellsOnMesh.txt");
        for(unsigned int i_error=0; i_error<cell_errorsResidualMesh.size(); i_error++)
        {
            ofErrors<<cell_errorsResidualMesh[i_error]<<" "<<endl;
        }
        ofErrors<<endl;
        ofErrors.close();
        //=====
        ofErrors.open("L2ResidualErrorsOverCellsOnPhyscialDomain.txt");
        for(unsigned int i_error=0; i_error<cell_errorsResidualMesh.size(); i_error++)
        {
            ofErrors<<cell_errorsResidualPhsicalDom[i_error]<<" "<<endl;
        }
        ofErrors<<endl;
        ofErrors.close();
        //=====
        ofErrors.open("L2ErrorsOverCellsOnPhysicalDomain.txt");
        for(unsigned int i_error=0; i_error<cell_errorsPhysDom.size(); i_error++)
        {
            ofErrors<<cell_errorsPhysDom[i_error]<<" "<<endl;
        }
        ofErrors<<endl;
        ofErrors.close();
        //=====
        ofErrors.open("EnergyNormErrorsOverCellsOnPhysicalDomain.txt");
        for(unsigned int i_error=0; i_error<CellErrEnergyNormPhysDom.size(); i_error++)
        {
            ofErrors<<CellErrEnergyNormPhysDom[i_error]<<" "<<endl;
        }
        ofErrors<<endl;
        ofErrors.close();
        //=====
        ofErrors.open("EnergyNormErrorsOverCellsOnMesh.txt");
        for(unsigned int i_error=0; i_error<CellErrEnergyNormMesh.size(); i_error++)
        {
            ofErrors<<CellErrEnergyNormMesh[i_error]<<" "<<endl;
        }
        ofErrors<<endl;
        ofErrors.close();*/
        //======================================

        //======================================
        ofstream ofTot_Error;
  /*      ofTot_Error.open("L2Total_ErrorOnMesh.txt");
        ofTot_Error<<totol_Error<<endl;
        ofTot_Error.close();
        */
        //=====
/*        ofTot_Error.open("L2ResidualTotal_ErrorOnMesh.txt");
        ofTot_Error<<total_Error_ResidualMesh<<endl;
        ofTot_Error.close();
        */
        //======
        ofTot_Error.open("L2ResidualTotal_ErrorOnPhysicalDomain.txt");
        ofTot_Error<<total_Error_ResidualPhsicalDom<<endl;
        ofTot_Error.close();
        //======
        ofTot_Error.open("L2TotalError.txt");
        ofTot_Error<<total_ErrorPhysDom<<endl;
        ofTot_Error.close();
        //======
        ofTot_Error.open("Energe_TotalError.txt");
        ofTot_Error<<total_ErrEnergyNormPhysDom<<endl;
        ofTot_Error.close();
        //======
 /*       ofTot_Error.open("EnergyNormTotal_ErrorOnMesh.txt");
        ofTot_Error<<total_ErrEnergyNormMesh<<endl;
        ofTot_Error.close();
        */
        //======
}

//========================================================================
static void PrintAxelFile_FEMSolution(const vector<vector<double> > &Coeff_Parametric_Map, Mesh * P_ParametricMesh, const unsigned int k, const Vec &m_solut)
{
    vector<vector<double> > CoeffsOverFinedMesh;

    MSplineFunctions * P_ParametricMap = new MSplineFunctions(Coeff_Parametric_Map, *P_ParametricMesh);

    Mesh* P_FEMMesh = P_ParametricMap->GenerateThisMSplineFunctionsOverAFinerMesh(k, P_ParametricMesh, CoeffsOverFinedMesh);

    //Modified the third-component
    for(PetscInt i = 0; i < CoeffsOverFinedMesh[2].size(); i ++)
    {
        PetscScalar v;
        VecGetValues(m_solut, 1, &i, &v);

        CoeffsOverFinedMesh[2][i]=v;

       // cout<<"v = "<<v<<endl;
    }
    //Now the FEMSolution is defined over P_FEMMesh and the coeffients is CoeffsOverFinedMesh


        ofstream ofAxel;
        ofAxel.open("FEMSolution_Axel.axl");

        ofAxel<<"<axl>"<<endl;
        ofAxel<<"<mspline type=\"InitialMesh\">"<<endl;
        ofAxel<<"<vertexnumber> "<<P_FEMMesh->P_Vertices->size()<<"</vertexnumber>"<<endl;

        for (unsigned int i=0; i<P_FEMMesh->P_Cells->size(); i++)
        {
                ofAxel<<"<cell>"<<endl;
                ofAxel<<P_FEMMesh->P_Cells->at(i).CellSize[0]<<" "<<P_FEMMesh->P_Cells->at(i).CellSize[1]<<" "<<P_FEMMesh->P_Cells->at(i).CellSize[2]<<" "<<P_FEMMesh->P_Cells->at(i).CellSize[3]<<endl;
                ofAxel<<P_FEMMesh->P_Cells->at(i).Index_ItsCornerVertices[0]<<" "<<P_FEMMesh->P_Cells->at(i).Index_ItsCornerVertices[1]<<" "<<P_FEMMesh->P_Cells->at(i).Index_ItsCornerVertices[2]<<" "<<P_FEMMesh->P_Cells->at(i).Index_ItsCornerVertices[3]<<endl;
                ofAxel<<"</cell>"<<endl;
        }
        for (unsigned int i=0; i<P_FEMMesh->P_Cells->size(); i++)
        {
                for (unsigned int j=0; j<i; j++)
                {
                    unsigned int J=P_FEMMesh->P_Cells->at(j).Index_InitialCell;
                    unsigned int I=P_FEMMesh->P_Cells->at(i).Index_InitialCell;
                    double O11, O12, O21, O22;
                    O11=(P_FEMMesh->P_FrameTranMatrix->at(I))[J].O11;
                    O12=(P_FEMMesh->P_FrameTranMatrix->at(I))[J].O12;
                    O21=(P_FEMMesh->P_FrameTranMatrix->at(I))[J].O21;
                    O22=(P_FEMMesh->P_FrameTranMatrix->at(I))[J].O22;

                    double IdError=(O11-1.0)*(O11-1.0)+O12*O12+O21*O21+(O22-1.0)*(O22-1.0);
                    if(IdError>1.0e-2)
                    {
                        ofAxel<<"<transition cells=\""<<i<<" "<<j<<"\">"<<endl;
                        ofAxel<<O11<<" "<<O12<<" "<<O21<<" "<<O22<<endl;
                        ofAxel<<"</transition>"<<endl;
                    }

                }
        }

        ofAxel<<"<points>"<<endl;
        switch(CoeffsOverFinedMesh.size())
        {
        case 0:
                cout<<"There is no point!!"<<endl;
                break;
        case 1:
                for (unsigned int i=0; i<CoeffsOverFinedMesh[0].size(); i++)
                {
                        ofAxel<<CoeffsOverFinedMesh[0][i]<<" "<<0.0<<" "<<0.0<<endl;
                }
                break;
        case 2:
                for (unsigned int i=0; i<CoeffsOverFinedMesh[0].size(); i++)
                {
                        ofAxel<<CoeffsOverFinedMesh[0][i]<<" "<<CoeffsOverFinedMesh[1][i]<<" "<<0.0<<endl;
                }
                break;
        case 3:
                for (unsigned int i=0; i<CoeffsOverFinedMesh[0].size(); i++)
                {
                        ofAxel<<CoeffsOverFinedMesh[0][i]<<" "<<CoeffsOverFinedMesh[1][i]<<" "<<CoeffsOverFinedMesh[2][i]<<endl;
                }
                break;
        default:
                for (unsigned int i=0; i<CoeffsOverFinedMesh[0].size(); i++)
                {
                        ofAxel<<CoeffsOverFinedMesh[0][i]<<" "<<CoeffsOverFinedMesh[1][i]<<" "<<CoeffsOverFinedMesh[2][i]<<endl;
                }
                cout<<"the dimesion of parametricMap > 3"<<endl;
                break;

        }
        ofAxel<<"</points>"<<endl;
        ofAxel<<"</mspline>"<<endl;
        ofAxel<<"</axl>"<<endl;

        //========================================================================================
//        ofAxel<<"<axl>"<<endl;
//        ofAxel<<"<msplineFEM type=\"InitialMesh\">"<<endl;
//        ofAxel<<"<vertexnumber> "<<P_ParametricMesh->P_Vertices->size()<<"</vertexnumber>"<<endl;
//        for (unsigned int i=0; i<P_ParametricMesh->P_Cells->size(); i++)
//        {
//                ofAxel<<"<cell>"<<endl;
//                ofAxel<<P_ParametricMesh->P_Cells->at(i).CellSize[0]<<" "<<P_ParametricMesh->P_Cells->at(i).CellSize[1]<<" "<<P_ParametricMesh->P_Cells->at(i).CellSize[2]<<" "<<P_ParametricMesh->P_Cells->at(i).CellSize[3]<<endl;
//                ofAxel<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[0]<<" "<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[1]<<" "<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[2]<<" "<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[3]<<endl;
//                ofAxel<<"</cell>"<<endl;
//        }
//        for (unsigned int i=0; i<P_ParametricMesh->P_Cells->size(); i++)
//        {
//                for (unsigned int j=0; j<i; j++)
//                {
//                    unsigned int J=P_ParametricMesh->P_Cells->at(j).Index_InitialCell;
//                    unsigned int I=P_ParametricMesh->P_Cells->at(i).Index_InitialCell;
//                    double O11, O12, O21, O22;
//                    O11=(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O11;
//                    O12=(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O12;
//                    O21=(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O21;
//                    O22=(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O22;

//                    double IdError=(O11-1.0)*(O11-1.0)+O12*O12+O21*O21+(O22-1.0)*(O22-1.0);
//                    if(IdError>1.0e-2)
//                    {
//                        ofAxel<<"<transition cells=\""<<i<<" "<<j<<"\">"<<endl;
//                        ofAxel<<O11<<" "<<O12<<" "<<O21<<" "<<O22<<endl;
//                        ofAxel<<"</transition>"<<endl;
//                    }

//                }
//        }

//        ofAxel<<"<points>"<<endl;
//        switch(Coeff_Parametric_Map.size())
//        {
//        case 0:
//                cout<<"There is no point!!"<<endl;
//                break;
//        case 1:
//                for (unsigned int i=0; i<Coeff_Parametric_Map[0].size(); i++)
//                {
//                        ofAxel<<Coeff_Parametric_Map[0][i]<<" "<<0.0<<" "<<0.0<<endl;
//                }
//                break;
//        case 2:
//                for (unsigned int i=0; i<Coeff_Parametric_Map[0].size(); i++)
//                {
//                        ofAxel<<Coeff_Parametric_Map[0][i]<<" "<<Coeff_Parametric_Map[1][i]<<" "<<0.0<<endl;
//                }
//                break;
//        case 3:
//                for (unsigned int i=0; i<Coeff_Parametric_Map[0].size(); i++)
//                {
//                        ofAxel<<Coeff_Parametric_Map[0][i]<<" "<<Coeff_Parametric_Map[1][i]<<" "<<Coeff_Parametric_Map[2][i]<<endl;
//                }
//                break;
//        default:
//                for (unsigned int i=0; i<Coeff_Parametric_Map[0].size(); i++)
//                {
//                        ofAxel<<Coeff_Parametric_Map[0][i]<<" "<<Coeff_Parametric_Map[1][i]<<" "<<Coeff_Parametric_Map[2][i]<<endl;
//                }
//                cout<<"the dimesion of parametricMap > 3"<<endl;
//                break;

//        }
//        ofAxel<<"</points>"<<endl;

//        ofAxel<<"<subdivisiontime> "<<k<<" </subdivisiontime>"<<endl;
//        ofAxel<<"<field>"<<endl;
//        PetscInt SoluSize;
//        VecGetSize(m_solut, &SoluSize);
//        for (PetscInt i=0; i< SoluSize; i++)
//        {
//            PetscScalar v;
//            VecGetValues(m_solut, 1, &i, &v);
//                ofAxel<< v<<endl;
//        }
//        ofAxel<<"</field>"<<endl;


//        ofAxel<<"</msplineFEM>"<<endl;
//        ofAxel<<"</axl>"<<endl;
        //==================================================================

        ofAxel.close();
}
//=========================================================================

static void PrintAxelFileWithField_FEMSolution(const vector<vector<double> > &Coeff_Parametric_Map, Mesh * P_ParametricMesh, const unsigned int k, const Vec &m_solut)
{
    //========================================================================================
    ofstream ofAxel;
    ofAxel.open("FEMSolution_Field_Axel.axl");

    ofAxel<<"<axl>"<<endl;
    ofAxel<<"<mspline type=\"InitialMesh\">"<<endl;
    ofAxel<<"<vertexnumber> "<<P_ParametricMesh->P_Vertices->size()<<"</vertexnumber>"<<endl;
    for (unsigned int i=0; i<P_ParametricMesh->P_Cells->size(); i++)
    {
        ofAxel<<"<cell>"<<endl;
        ofAxel<<P_ParametricMesh->P_Cells->at(i).CellSize[0]<<" "<<P_ParametricMesh->P_Cells->at(i).CellSize[1]<<" "<<P_ParametricMesh->P_Cells->at(i).CellSize[2]<<" "<<P_ParametricMesh->P_Cells->at(i).CellSize[3]<<endl;
        ofAxel<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[0]<<" "<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[1]<<" "<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[2]<<" "<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[3]<<endl;
        ofAxel<<"</cell>"<<endl;
    }
    for (unsigned int i=0; i<P_ParametricMesh->P_Cells->size(); i++)
    {
        for (unsigned int j=0; j<i; j++)
        {
            unsigned int J=P_ParametricMesh->P_Cells->at(j).Index_InitialCell;
            unsigned int I=P_ParametricMesh->P_Cells->at(i).Index_InitialCell;
            double O11, O12, O21, O22;
            O11=(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O11;
            O12=(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O12;
            O21=(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O21;
            O22=(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O22;

            double IdError=(O11-1.0)*(O11-1.0)+O12*O12+O21*O21+(O22-1.0)*(O22-1.0);
            if(IdError>1.0e-2)
            {
                ofAxel<<"<transition cells=\""<<i<<" "<<j<<"\">"<<endl;
                ofAxel<<O11<<" "<<O12<<" "<<O21<<" "<<O22<<endl;
                ofAxel<<"</transition>"<<endl;
            }

        }
    }

    ofAxel<<"<points>"<<endl;
    switch(Coeff_Parametric_Map.size())
    {
    case 0:
            cout<<"There is no point!!"<<endl;
            break;
    case 1:
            for (unsigned int i=0; i<Coeff_Parametric_Map[0].size(); i++)
            {
                ofAxel<<Coeff_Parametric_Map[0][i]<<" "<<0.0<<" "<<0.0<<endl;
            }
            break;
    case 2:
            for (unsigned int i=0; i<Coeff_Parametric_Map[0].size(); i++)
            {
                ofAxel<<Coeff_Parametric_Map[0][i]<<" "<<Coeff_Parametric_Map[1][i]<<" "<<0.0<<endl;
            }
            break;
    case 3:
            for (unsigned int i=0; i<Coeff_Parametric_Map[0].size(); i++)
            {
                ofAxel<<Coeff_Parametric_Map[0][i]<<" "<<Coeff_Parametric_Map[1][i]<<" "<<Coeff_Parametric_Map[2][i]<<endl;
            }
            break;
    default:
            for (unsigned int i=0; i<Coeff_Parametric_Map[0].size(); i++)
            {
                ofAxel<<Coeff_Parametric_Map[0][i]<<" "<<Coeff_Parametric_Map[1][i]<<" "<<Coeff_Parametric_Map[2][i]<<endl;
            }
            cout<<"the dimesion of parametricMap > 3"<<endl;
            break;

    }
    ofAxel<<"</points>"<<endl;

    ofAxel<<"<subdivisiontime> "<<k<<" </subdivisiontime>"<<endl;
    ofAxel<<"<field>"<<endl;
    PetscInt SoluSize;
    VecGetSize(m_solut, &SoluSize);
    for (PetscInt i=0; i< SoluSize; i++)
    {
        PetscScalar v;
        VecGetValues(m_solut, 1, &i, &v);
        ofAxel<< v<<endl;
    }
    ofAxel<<"</field>"<<endl;

    ofAxel<<"</mspline>"<<endl;
    ofAxel<<"</axl>"<<endl;
    //==================================================================

    ofAxel.close();
}
//=========================================================================

#endif // FEM_PRINT_H
