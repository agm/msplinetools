
#include <iostream>
using namespace std;
#include "HermiteTransferMatrix.h"
#include "FrameTransferMatrix.h"
#include "HermiteData.h"

HermiteTransferMatrix::HermiteTransferMatrix(const FrameTransferMatrix & O)
{
	a11=1.0;
	a22=O.O11;
	a23=O.O12;
	a32=O.O21;
	a33=O.O22;
	a44=O.O11*O.O22+O.O12*O.O21;
};

void HermiteTransferMatrix::HermiteDataTransfer(HermiteData &NewHermite, const HermiteData & OriginalHermite)
{
	NewHermite.ItsHermiteData.clear();

	NewHermite.ItsHermiteData.push_back(OriginalHermite.ItsHermiteData[0]);
	NewHermite.ItsHermiteData.push_back(a22*OriginalHermite.ItsHermiteData[1]+a23*OriginalHermite.ItsHermiteData[2]);
	NewHermite.ItsHermiteData.push_back(a32*OriginalHermite.ItsHermiteData[1]+a33*OriginalHermite.ItsHermiteData[2]);
	NewHermite.ItsHermiteData.push_back(a44*OriginalHermite.ItsHermiteData[3]);
};


void HermiteTransferMatrix::Cout_HermiteTransferMatrix()
{
	cout<<"HermiteTransferMatrix:"<<endl;
	cout<<a11<<" "<< 0<<" "<<0<<" "<<0<<endl;
	cout<<0<<" "<<a22<<" "<< a23<<" "<<0<<endl;
	cout<<0<<" "<<a32<<" "<<a33<<" "<<0<<endl;
	cout<<0<<" "<<0<<" "<<0<<" "<<a44<<endl;
}
