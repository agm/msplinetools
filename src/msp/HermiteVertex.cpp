//#include "StdAfx.h"

#include <map>

using namespace std;

#include "HermiteVertex.h"
#include "Mesh.h"
#include "Vertex.h"
#include "Cell.h"
#include "HermiteData.h"
#include "Coordinate.h"
#include "HermiteTransferMatrix.h"
#include "FrameTransferMatrix.h"

HermiteVertex::HermiteVertex(void)
{
}

HermiteVertex::~HermiteVertex(void)
{
}

HermiteVertex::HermiteVertex(const HermiteVertex &HV)
{
	//vector<HermiteData*> P_ItsHermiteData
	P_ItsHermiteData.clear();
	for (unsigned int i=0; i<HV.P_ItsHermiteData.size(); i++)
	{
		P_ItsHermiteData.push_back(HV.P_ItsHermiteData[i]);
	}
}

HermiteVertex::HermiteVertex(unsigned int index_vertex, const Mesh & CurrentMesh, const vector<double> & Hermitedata)
{
	P_ItsHermiteData.clear();
	//-----------------------
	//CurrentMesh.P_Vertices[index_vertex]->ItsCellsToCoordinatesMap has been arranged 
				//and the indices of cells are stored from lowest to highest  
	//Thus, the Hermitedata is given to the cell with the lowest index
	//
	double flag=Hermitedata[0]*Hermitedata[0]+Hermitedata[1]*Hermitedata[1]+Hermitedata[2]*Hermitedata[2]+Hermitedata[3]*Hermitedata[3];
	if (flag<1.0e-2)//almost zero, flag=1 or 0 
	{
        for(map<unsigned int, Coordinate>::const_iterator it=CurrentMesh.P_Vertices->at(index_vertex).ItsCellsToCoordinatesMap.begin(); it!=CurrentMesh.P_Vertices->at(index_vertex).ItsCellsToCoordinatesMap.end(); it++)
		{
			//----------------------------------
			HermiteData * P_HD=new HermiteData;
            P_HD->Index_InitialCell=CurrentMesh.P_Cells->at((*it).first).Index_InitialCell;
			P_HD->ItsHermiteData.push_back(0.0);
			P_HD->ItsHermiteData.push_back(0.0);
			P_HD->ItsHermiteData.push_back(0.0);
			P_HD->ItsHermiteData.push_back(0.0);
			//----------------------------------
			P_ItsHermiteData.push_back(P_HD);
			P_HD=NULL;
		}
	}
	else//
	{
		//The First element
        map<unsigned int, Coordinate>::const_iterator it=CurrentMesh.P_Vertices->at(index_vertex).ItsCellsToCoordinatesMap.begin();
		//--------------------------------------------
		HermiteData * P_HD=new HermiteData;
        P_HD->Index_InitialCell=CurrentMesh.P_Cells->at((*it).first).Index_InitialCell;
		P_HD->ItsHermiteData.push_back(Hermitedata[0]);
		P_HD->ItsHermiteData.push_back(Hermitedata[1]);
		P_HD->ItsHermiteData.push_back(Hermitedata[2]);
		P_HD->ItsHermiteData.push_back(Hermitedata[3]);
		//---------------------------------------------
		P_ItsHermiteData.push_back(P_HD);
		P_HD=NULL;
		//---------------------------------------------
        unsigned int Index_InitialCell0=CurrentMesh.P_Cells->at((*it).first).Index_InitialCell;
		//---------------------------------------------
		it++;
		//------
        for (;it!=CurrentMesh.P_Vertices->at(index_vertex).ItsCellsToCoordinatesMap.end(); it++)
		{
            unsigned int Index_InitialCelli = CurrentMesh.P_Cells->at((*it).first).Index_InitialCell;
			HermiteTransferMatrix H((CurrentMesh.P_FrameTranMatrix->at(Index_InitialCelli))[Index_InitialCell0]);

			HermiteData * P_HDi=new HermiteData;
			P_HDi->Index_InitialCell=Index_InitialCelli;
			H.HermiteDataTransfer(*(P_HDi), (*P_ItsHermiteData[0]));

// 			P_ItsHermiteData[0]->Cout_HermiteData();
// 			H.Cout_HermiteTransferMatrix();
// 			P_HDi->Cout_HermiteData();

			P_ItsHermiteData.push_back(P_HDi);
			P_HDi=NULL;
		}

	}

	//-----------------------
}

HermiteVertex::HermiteVertex(unsigned int index_vertex, unsigned int index_Cell, const Mesh & CurrentMesh, const vector<double> &Hermitedata)
{
	P_ItsHermiteData.clear();
	//-----------------------
	//
	double flag=Hermitedata[0]*Hermitedata[0]+Hermitedata[1]*Hermitedata[1]+Hermitedata[2]*Hermitedata[2]+Hermitedata[3]*Hermitedata[3];
	if (flag<1.0e-2)//almost zero, flag=1 or 0 
	{
		
			//----------------------------------
			HermiteData * P_HD=new HermiteData;
            P_HD->Index_InitialCell=CurrentMesh.P_Cells->at(index_Cell).Index_InitialCell;
			P_HD->ItsHermiteData.push_back(0.0);
			P_HD->ItsHermiteData.push_back(0.0);
			P_HD->ItsHermiteData.push_back(0.0);
			P_HD->ItsHermiteData.push_back(0.0);
			//----------------------------------
			P_ItsHermiteData.push_back(P_HD);
			P_HD=NULL;
		
	}
	else//
	{
		//The First element
        map<unsigned int, Coordinate>::const_iterator it=CurrentMesh.P_Vertices->at(index_vertex).ItsCellsToCoordinatesMap.begin();
		//--------------------------------------------
		HermiteData * P_HD=new HermiteData;
        P_HD->Index_InitialCell=CurrentMesh.P_Cells->at((*it).first).Index_InitialCell;
		P_HD->ItsHermiteData.push_back(Hermitedata[0]);
		P_HD->ItsHermiteData.push_back(Hermitedata[1]);
		P_HD->ItsHermiteData.push_back(Hermitedata[2]);
		P_HD->ItsHermiteData.push_back(Hermitedata[3]);
		//---------------------------------------------
        unsigned int Index_InitialCell0=CurrentMesh.P_Cells->at((*it).first).Index_InitialCell;
		//---------------------------------------------
		//------
	
        unsigned int Index_InitialCelli = CurrentMesh.P_Cells->at(index_Cell).Index_InitialCell;
		HermiteTransferMatrix H((CurrentMesh.P_FrameTranMatrix->at(Index_InitialCelli))[Index_InitialCell0]);


		HermiteData * P_HDi=new HermiteData;
		P_HDi->Index_InitialCell=Index_InitialCelli;
		H.HermiteDataTransfer(*(P_HDi), (*P_HD));

		P_ItsHermiteData.push_back(P_HDi);
		P_HDi=NULL;

	}
}

void HermiteVertex::Cout_HermiteVertex()
{//vector<HermiteData *> P_ItsHermiteData;
	for(unsigned int i=0; i<P_ItsHermiteData.size(); i++)
	{
		P_ItsHermiteData[i]->Cout_HermiteData();

	}
}
