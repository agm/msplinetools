#ifndef BASESCELL_H
#define BASESCELL_H

#include <vector>
using namespace std;
//-------------
class basis;
class Mesh;
class MSplineFunction;
//-------------


class BasesCell
{
public:
	unsigned int Index_Cell;
    /**
     * vector<basis *> P_Bases_Cell :: The pointers of bases defined over the Index_Cell-th Cell
     */
    vector<basis *> P_Bases_Cell;
public:
	BasesCell(void);
	~BasesCell(void);
    /**
     * Generate all the bases over the index_Cell-th Cell
    */
	BasesCell(unsigned int index_Cell, const Mesh & CurrentMesh);

public:
	void Cout_BasesCell(const Mesh & CurrentMesh);

    MSplineFunction * ToMSplineFunction(unsigned int k, const Mesh *P_Mesh);
};

#endif
