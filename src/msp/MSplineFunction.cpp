//#include "StdAfx.h"
#include <iostream>
using namespace std;

#include "MSplineFunction.h"
#include "MSplineFunctionCell.h"
#include "HermiteCell.h"
#include "HermiteData.h"
#include "PointSet.h"
#include "Mesh.h"
#include "Cell.h"
#include "Bases.h"
#include "Basis.h"

MSplineFunction::MSplineFunction(void)
{
    P_MSpline.clear();
}

MSplineFunction::~MSplineFunction(void)
{
}

//=================================================================

void MSplineFunction::Evaluation(int DerOrderu, int DerOrderv, const vector<Coordinate*> &Pcoordinate_Ps, vector<double> &Values, const unsigned int index_cell, const Mesh & CurrentMesh)
{
	Values.clear();
    if (index_cell<CurrentMesh.P_Cells->size())
	{
        //cout<<"MSplineFunction::Index_cell<CurrentMesh.P_Cells->size()"<<endl;
		PointSet Ps(index_cell, Pcoordinate_Ps);
		
        //cout<<"MSplineFunction::Evaluation"<<endl;
        //cout<<"index_cell="<<index_cell<<endl;
		P_MSpline[index_cell]->Evaluation(DerOrderu, DerOrderv, Ps, Values, CurrentMesh);//MSplineFunctionCell::Evaluation(int DerOrderu, int DerOrderv, const PointSet & Pset, vector<double> &Values, const Mesh & CurrentMesh)

	}
	else
	{

		Values.resize(Pcoordinate_Ps.size(), 0.0);
	}
}

void MSplineFunction::Evaluation(int DerOrderu, int DerOrderv, const vector<PointSet> & PsetS, vector<vector<double> > &Values, const Mesh & CurrentMesh)
{
    //cout<<"MSplineFunction::Evaluation()"<<endl;

	Values.clear();
	for (unsigned int i=0; i<PsetS.size(); i++)
	{
		unsigned int index_cell=PsetS[i].Index_ItsCell;
        if(index_cell<CurrentMesh.P_Cells->size())
		{
			vector<double> Values_i;
            P_MSpline[index_cell]->Evaluation(DerOrderu,DerOrderv,PsetS[i],Values_i, CurrentMesh);
			Values.push_back(Values_i);
		}
		else
		{
			vector<double> Values_i;
			Values_i.resize(PsetS[i].P_coordinates_Ps.size(),0);
			Values.push_back(Values_i);
		}
	}
}

//=================================================================================
MSplineFunction::MSplineFunction(const Mesh & CurrentMesh)
{
	P_MSpline.clear();
    for (unsigned int i=0; i<CurrentMesh.P_Cells->size(); i++)//with the same order as the cells
	{
        unsigned int Index_InitialCell=CurrentMesh.P_Cells->at(i).Index_InitialCell;
		unsigned int Index_Cell=i;
		MSplineFunctionCell *P_MSpFunCell=new MSplineFunctionCell(Index_InitialCell, Index_Cell);
		P_MSpline.push_back(P_MSpFunCell);
		P_MSpFunCell=NULL;
	}
}

MSplineFunction::MSplineFunction(const vector<double> &Coeff, const Bases &BasisSet, const Mesh & CurrentMesh)
{
	//this->MSplineFunction(const Mesh & CurrentMesh)
	//-----------------------------------------------------------
	P_MSpline.clear();
    for (unsigned int i=0; i<CurrentMesh.P_Cells->size(); i++)//with the same order as the cells
	{
        unsigned int Index_InitialCell=CurrentMesh.P_Cells->at(i).Index_InitialCell;
		unsigned int Index_Cell=i;

        MSplineFunctionCell *P_MSpFunCell=new MSplineFunctionCell(Index_InitialCell, Index_Cell);

		P_MSpline.push_back(P_MSpFunCell);
		P_MSpFunCell=NULL;
	}

//    //Cout: For testing
//    cout<<P_MSpline.size()<<endl;
//    //-----
//    P_MSpline[0]->Cout_MSplineFunctionCell();
//    //-----

	//------------------------------------------------------
	for (unsigned int i=0; i<BasisSet.P_BasesSet.size(); i++)
	{
		//BasisSet.P_BasesSet[i]->......
		unsigned int Index_Basis=BasisSet.P_BasesSet[i]->Index_basis;
		for (unsigned int j=0; j<BasisSet.P_BasesSet[i]->P_HermiteData_Cells.size(); j++)
		{
			unsigned int Index_Cell=BasisSet.P_BasesSet[i]->P_HermiteData_Cells[j]->Index_Cell;

            HermiteCell B_HC(*(BasisSet.P_BasesSet[i]->P_HermiteData_Cells[j]));

            //-----

          *(P_MSpline[Index_Cell]->P_MSplineCell)=(*(P_MSpline[Index_Cell]->P_MSplineCell))+(B_HC.Lambda(Coeff[Index_Basis]));

//            //For testing
//            cout<<"After adding: P_MSpline[Index_Cell]"<<endl;
//            P_MSpline[Index_Cell]->P_MSplineCell->Cout_HermiteCell();
//            cin.get();

		}
	}
}

//================================================================
MSplineFunction::MSplineFunction(const vector<double> &Coeff, const Mesh & CurrentMesh)
{
    P_MSpline.clear();
    for (unsigned int Index_Cell=0; Index_Cell<CurrentMesh.P_Cells->size(); Index_Cell++)//with the same order as the cells
    {
        MSplineFunctionCell * MSpFunCell= new MSplineFunctionCell(Coeff, Index_Cell, CurrentMesh);

        P_MSpline.push_back(MSpFunCell);
    }
}

MSplineFunction::MSplineFunction(unsigned int indexBasis, const Mesh & CurrentMesh)
{
    //
    vector<double> Coeff(4*CurrentMesh.P_Vertices->size(),0.0);
    Coeff[indexBasis]=1.0;
    new(this) MSplineFunction(Coeff, CurrentMesh);
}

// //================================================================

void  MSplineFunction::Cout_MSplineFunction()
{
    for(unsigned int i=0; i<P_MSpline.size(); i++)
    {
        cout<<i<<"-th cell"<<endl;
        P_MSpline[i]->Cout_MSplineFunctionCell();
    }
}

//=====================
void MSplineFunction::ModifyMSplineFunction(const vector<double> &Coeff, unsigned int Index_Cell, const Mesh & CurrentMesh)
{
    //vector<MSplineFunctionCell *> P_MSpline;
    MSplineFunctionCell * PMSplinei=new MSplineFunctionCell(Coeff, Index_Cell, CurrentMesh);

//    PMSplinei->Cout_MSplineFunctionCell();
//    cin.get();

//    cout<<"Before----"<<endl;
//    P_MSpline[Index_Cell]->Cout_MSplineFunctionCell();
//    cin.get();

    delete P_MSpline[Index_Cell];

    P_MSpline[Index_Cell]=PMSplinei;

//    cout<<"After"<<endl;
//    P_MSpline[Index_Cell]->Cout_MSplineFunctionCell();
//    cin.get();


}
//=========================
void MSplineFunction::BSplineCoeffAxelOrder(unsigned int i_cell, double *CellSize, vector<double> &Coeffi)
{
    this->P_MSpline[i_cell]->BSplineCoeffAxelOrder(CellSize, Coeffi);
}
