#include "Mesh.h"
#include <algorithm>
#include <iostream>
#include <fstream>

#include "Functions.h"

#include "Vertex.h"
#include "Cell.h"
#include "FrameTransferMatrix.h"
#include "Coordinate.h"
#include "Bases.h"
#include "Basis.h"

Mesh::Mesh(void)
{
	P_FrameTranMatrix=new vector< vector<FrameTransferMatrix> >;

    P_Cells=new vector<Cell>;
    P_Vertices=new vector<Vertex>;

}

Mesh::~Mesh(void)
{
}

//=====================================================
void Mesh::ReadFile_withoutHangingVertices(string file)
{
    ifstream infile(file.c_str());
    if(infile == NULL)//Do not find this file
    {
        cout<< "There is no elements in Initial Mesh File:" << file <<endl;
    }
    else
    {
        P_Vertices->clear();
        P_Cells->clear();
        //-------------------------------
        unsigned int nV, nC, nLF;
        infile >> nV;//the number of vertices
        infile >> nC;//the number of cells
        infile >> nLF;//the number of non-trivial local frame transformations from LF_j to LF_0
		//Here, LF_i is the local frame of Cells[i]
        cout<<nV <<" "<<nC<<" "<<nLF<<endl;
        //-----For Vertices
        //Vertices[i] is preparing to be set.
        for (unsigned int i=0; i<nV; i++)
        {
            Vertex V;
            P_Vertices->push_back(V);
            //P_V=NULL;
        }

        //-----For Cells
        for(unsigned int i=0; i<nC; i++)
        {
            Cell C;
            //-------------
            C.Index_InitialCell=i;
            //-------------
            infile >> C.CellSize[0];
            infile >> C.CellSize[1];
            infile >> C.CellSize[2];
            infile >> C.CellSize[3];

            //cout<<C.CellSize[0]<<" "<<C.CellSize[1]<<" "<<C.CellSize[2]<<" "<<C.CellSize[3]<<endl;
            //-------------
            for(unsigned int j=0; j<4; j++)
            {
                unsigned int k;
                infile >> k;
                C.Index_ItsCornerVertices.push_back(k);
            };
            //---For Vertices[k].Index_ItsVertices
            for(unsigned int j=0; j<4; j++)
            {
                unsigned int k=C.Index_ItsCornerVertices[j];//Vertices[k]'s vertices
                unsigned int back_vertex_index;
                unsigned int front_vertex_index;
                if(j==0)
                {
                    back_vertex_index=3;
                    front_vertex_index=j+1;
                }
                else
                {
                    if(j==3)
                    {
                        back_vertex_index=j-1;
                        front_vertex_index=0;
                    }
                    else
                    {
                        back_vertex_index=j-1;
                        front_vertex_index=j+1;
                    };
                };
                P_Vertices->at(k).Index_ItsVertices.insert(C.Index_ItsCornerVertices[back_vertex_index]);
                P_Vertices->at(k).Index_ItsVertices.insert(C.Index_ItsCornerVertices[front_vertex_index]);
            };


            for(unsigned int j=0; j<4; j++)
            {
                Coordinate V_Coor;
                switch (j)
                {
                case 0:
                    V_Coor.xy[0]=C.CellSize[0];
                    V_Coor.xy[1]=C.CellSize[2];
                    break;

                case 1:
                    V_Coor.xy[0]=C.CellSize[1];
                    V_Coor.xy[1]=C.CellSize[2];
                    break;

                case 2:
                    V_Coor.xy[0]=C.CellSize[1];
                    V_Coor.xy[1]=C.CellSize[3];
                    break;

                case 3:
                    V_Coor.xy[0]=C.CellSize[0];
                    V_Coor.xy[1]=C.CellSize[3];
                    break;
                }

                P_Vertices->at(C.Index_ItsCornerVertices[j]).ItsCellsToCoordinatesMap.insert(pair<unsigned int, Coordinate>(i, V_Coor));
        };
			//-------------

            P_Cells->push_back(C);
            //P_C=NULL;

        };
		//----For Local Frames
		FrameTransferMatrix ID_Matrix;
		ID_Matrix.O11=1;
		ID_Matrix.O12=0;
		ID_Matrix.O21=0;
		ID_Matrix.O22=1;

		vector<FrameTransferMatrix> Vector_Matrix;
		Vector_Matrix.clear();
		for(unsigned int j=0; j<nC; j++ )
		{
                    Vector_Matrix.push_back(ID_Matrix);
		};
		for (unsigned int j=0; j<nC; j++)
		{
                    P_FrameTranMatrix->push_back(Vector_Matrix);
		};
		//-------The Following is the modification of FrameTranMatrix

		for(unsigned int i=0; i<nLF; i++)
		{
			unsigned int k,l;
			infile >> k;
			infile >> l;
			infile >> (P_FrameTranMatrix->at(k))[l].O11;
			infile >> (P_FrameTranMatrix->at(k))[l].O12;
			infile >> (P_FrameTranMatrix->at(k))[l].O21;
			infile >> (P_FrameTranMatrix->at(k))[l].O22;

			//FrameTranMatix[l][k]=(FrameTranMatrix[k][l])^{-1}
			FrameTransferMatrix Matrix_inverse; 
			Matrix_inverse=FrameTranMatrix_inverse((P_FrameTranMatrix->at(k))[l]);
			(P_FrameTranMatrix->at(l))[k].O11=Matrix_inverse.O11;
			(P_FrameTranMatrix->at(l))[k].O12=Matrix_inverse.O12;
			(P_FrameTranMatrix->at(l))[k].O21=Matrix_inverse.O21;
			(P_FrameTranMatrix->at(l))[k].O22=Matrix_inverse.O22;
		};
	};

	this->Is_Interior_irregular_Vertex();
	this->Initialization_Index_AdjacentCells();
}
//=====================================================

void Mesh::Is_Interior_irregular_Vertex()
{
    MeshIrregularVerticesMap.clear();
	//----
	IrregularVerticesMap.clear();
	//-----------------------------------------------------------------
    for(unsigned int i_vertex=0; i_vertex < P_Vertices->size(); i_vertex++)
	{
		//set Vertices[i_vertex].is_interior
		bool is_interior = true;
		set<unsigned int> CellIndex0;
		//vector2set_unsigned_int(CellIndex0, P_Vertices[i_vertex]->Index_ItsCells);
        Map2set_unsigned_int(CellIndex0, P_Vertices->at(i_vertex).ItsCellsToCoordinatesMap);

        for(set<unsigned int>::const_iterator it=P_Vertices->at(i_vertex).Index_ItsVertices.begin(); it!=P_Vertices->at(i_vertex).Index_ItsVertices.end(); it++)
		{//vector<unsigned int> Index_ItsCells
			set<unsigned int> CellIndex1;
			//vector2set_unsigned_int(CellIndex1, P_Vertices[*it]->Index_ItsCells);
            Map2set_unsigned_int(CellIndex1, P_Vertices->at(*it).ItsCellsToCoordinatesMap);

			set<unsigned int> IntersectionSet;
			set_intersection(CellIndex0.begin(), CellIndex0.end(), CellIndex1.begin(), CellIndex1.end(),inserter(IntersectionSet, IntersectionSet.begin()));
			if(IntersectionSet.size()<2)
			{
				is_interior =false;
				break;
			};
		};

        P_Vertices->at(i_vertex).Is_interiorVertex = is_interior;
        if (is_interior && P_Vertices->at(i_vertex).Index_ItsVertices.size()%4)
		//Vertices[i_vertex] is an interior vertex and deg(Vertices[i_vertex])%4!=0
		{
            IrregularVerticesMap.insert(pair<unsigned int, unsigned int>(i_vertex, P_Vertices->at(i_vertex).Index_ItsVertices.size()));
		};
        if(is_interior && P_Vertices->at(i_vertex).Index_ItsVertices.size()!=4)
        {
            MeshIrregularVerticesMap.insert(pair<unsigned int, unsigned int>(i_vertex, P_Vertices->at(i_vertex).Index_ItsVertices.size()));
        }
        if(!is_interior && P_Vertices->at(i_vertex).Index_ItsVertices.size()>3)
        {
            MeshIrregularVerticesMap.insert(pair<unsigned int, unsigned int>(i_vertex, P_Vertices->at(i_vertex).Index_ItsVertices.size()));
        }

    };
};
//======================================================================================================================


void Mesh::SingularCellIndice_Finder(set<unsigned int> &SingularCellIndice, const set<unsigned int> &SingularVertexIndiceFEM)
{
    SingularCellIndice.clear();
    for(set<unsigned>::const_iterator it=SingularVertexIndiceFEM.begin(); it!=SingularVertexIndiceFEM.end(); it++)
    {
        //
        map<unsigned int, Coordinate> ItsCellsToCoordinatesMap=this->P_Vertices->at(*it).ItsCellsToCoordinatesMap;

        for(map<unsigned int, Coordinate>::iterator jt=ItsCellsToCoordinatesMap.begin(); jt!=ItsCellsToCoordinatesMap.end(); jt++)
        {
            //
            SingularCellIndice.insert((*jt).first);
        }
    }
}

//====================================================================================================================
void Mesh::Cout_Vertex_File()
{
    cout<<"the number of vertices: "<<P_Vertices->size()<<endl;

    for (unsigned int i=0; i<P_Vertices->size(); i++)
	{
        cout<<"Index of this vertex = "<< i <<" Is_interiorVertex="<<P_Vertices->at(i).Is_interiorVertex<<endl;

        cout<<"Index_ItsCells and ItsCoordinates_Cells: "<<endl;
        for (map<unsigned int, Coordinate>::const_iterator it=P_Vertices->at(i).ItsCellsToCoordinatesMap.begin(); it!=P_Vertices->at(i).ItsCellsToCoordinatesMap.end(); it++)
		{
			cout<<"Index_Cell = "<<(*it).first<<" , ItsCoordinate=("<<((*it).second).xy[0]<<", "<<((*it).second).xy[1]<<")"<<endl;
		}

		cout<<"Index_ItsVertices: "<<endl;
        for (set<unsigned int>::iterator it=P_Vertices->at(i).Index_ItsVertices.begin(); it!=P_Vertices->at(i).Index_ItsVertices.end();it++)
		{
			cout<<*it<<" ";
		};
		cout<<endl;
		cout<<"------------------------"<<endl;
		//system("PAUSE");

	};
};

void Mesh::Cout_Cells_File()
{
    cout<<"the number of cells: "<< P_Cells->size()<<endl;
    for (unsigned int i=0; i<P_Cells->size(); i++)
	{//Cells[i]
        cout<<"Index_InitialCell = "<< P_Cells->at(i).Index_InitialCell<<endl;
        cout<<"[x1,y1]X[x2,y2] = ["<<P_Cells->at(i).CellSize[0]<<","<<P_Cells->at(i).CellSize[1]<<"]X[" <<P_Cells->at(i).CellSize[2]<<","<<P_Cells->at(i).CellSize[3]<<"]"<<endl;

		cout<<"The indices of Its Corner Vertices: "<<endl;
        for (unsigned int ii=0; ii<P_Cells->at(i).Index_ItsCornerVertices.size();ii++)
		{
            cout<<P_Cells->at(i).Index_ItsCornerVertices[ii]<<"  ";
		};
		cout<<endl;

        cout<<"Its Parent :"<<P_Cells->at(i).ItsParent<<endl;

		cout<<"Its Children :"<<endl;
        for (set<unsigned int>::const_iterator it=P_Cells->at(i).Index_ItsSubCells.begin(); it!=P_Cells->at(i).Index_ItsSubCells.end(); it++)
		{
			cout<<*it<<" ";
		}
		cout<<endl;

		cout<<"The indices of its up, down ,left and right side of cells:"<<endl;
        if(P_Cells->at(i).Index_AdjacentCell_up.size())
        {
            cout<<"up-----> "<<*(P_Cells->at(i).Index_AdjacentCell_up.begin())<<endl;
        }
        else
        {
            cout<<"up-----> NULL"<<endl;
        }
        //****
        if(P_Cells->at(i).Index_AdjacentCell_down.size())
        {
            cout<<"down-----> "<<*(P_Cells->at(i).Index_AdjacentCell_down.begin())<<endl;
        }
        else
        {
            cout<<"down-----> NULL"<<endl;
        }
        //****
        if(P_Cells->at(i).Index_AdjacentCell_left.size())
        {
            cout<<"left-----> "<<*(P_Cells->at(i).Index_AdjacentCell_left.begin())<<endl;
        }
        else
        {
            cout<<"left-----> NULL"<<endl;
        }
        //****
        if(P_Cells->at(i).Index_AdjacentCell_right.size())
        {
            cout<<"right-----> "<<*(P_Cells->at(i).Index_AdjacentCell_right.begin())<<endl;
        }
        else
        {
            cout<<"right-----> NULL"<<endl;
        }
        //****
		cout<<"------------------------"<<endl;
                cin.get();
	};
};

void Mesh::Cout_FrameTransferMatrix()
{
	for (unsigned int i=0; i<P_FrameTranMatrix->size(); i++)
	{
		for (unsigned int j=0; j<(P_FrameTranMatrix->at(i)).size(); j++)
		{
			cout<<"--------------------------------------------"<<endl;
			cout<<" The Transition Matrix from "<< j <<" to "<<i<<" :"<<endl;
			cout<<(P_FrameTranMatrix->at(i))[j].O11<<" "<<(P_FrameTranMatrix->at(i))[j].O12<<"  "<<endl;
			cout<<(P_FrameTranMatrix->at(i))[j].O21<<" "<<(P_FrameTranMatrix->at(i))[j].O22<<"  "<<endl;
			//cout<<"--------------------------------------------"<<endl;
                        cin.get();
		};
	};
};

void Mesh::Cout_IrregularVertices()
{
	cout<<"the number of irregular vertices: "<<IrregularVerticesMap.size()<<endl;
	for(map<unsigned int,unsigned int>::const_iterator it=IrregularVerticesMap.begin(); it!=IrregularVerticesMap.end(); it++)
	{
		cout<<"Index of this irregular vertex: "<<(*it).first<<" and Its degree: "<<(*it).second<<endl;
	};
};

void Mesh::Cout_MeshIrregularVertices()
{
    cout<<"the number of mesh irregular vertices: "<<MeshIrregularVerticesMap.size()<<endl;
    for(map<unsigned int,unsigned int>::const_iterator it=MeshIrregularVerticesMap.begin(); it!=MeshIrregularVerticesMap.end(); it++)
    {
        cout<<"Index of this mesh irregular vertex: "<<(*it).first<<" and Its degree: "<<(*it).second<<endl;
    };
}

void Mesh::Cout_Mesh_File()
{
	cout<<"The Vertices of this Mesh:"<<endl;
	Cout_Vertex_File();
	cout<<"------------------"<<endl;
        cin.get();
	cout<<"where, the irregular vertices are"<<endl;
	Cout_IrregularVertices();
	cout<<"------------------"<<endl;
        cin.get();
        cout<<"where, the mesh irregular vertices are"<<endl;
        Cout_MeshIrregularVertices();
        cin.get();
	cout<<"The Cells of this Mesh:" <<endl;
	Cout_Cells_File();
	cout<<"------------------"<<endl;
        cin.get();
	cout<<"The FrameTransferMatrix of this Mesh:"<<endl;
        //Cout_FrameTransferMatrix();
        //cin.get();
	cout<<"==========================================="<<endl;
	
}

//====================================================================================================================
void Mesh::Initialization_Index_AdjacentCells()
{	
    for (unsigned int i_cell=0; i_cell<P_Cells->size(); i_cell++)
    {
        set<unsigned int> Corner_Cells0;
        set<unsigned int> Corner_Cells1;
        set<unsigned int> Corner_Cells2;
        set<unsigned int> Corner_Cells3;
        unsigned int vindex=P_Cells->at(i_cell).Index_ItsCornerVertices[0];
		//for(unsigned int i=0; i<P_Vertices[vindex]->Index_ItsCells.size();i++)
        for(map<unsigned int, Coordinate>::const_iterator it=P_Vertices->at(vindex).ItsCellsToCoordinatesMap.begin();
            it!=P_Vertices->at(vindex).ItsCellsToCoordinatesMap.end(); it++)
        {
            Corner_Cells0.insert((*it).first);
        };
        vindex=P_Cells->at(i_cell).Index_ItsCornerVertices[1];
		//for(unsigned int i=0; i<P_Vertices[vindex].Index_ItsCells.size();i++)
        for(map<unsigned int, Coordinate>::const_iterator it=P_Vertices->at(vindex).ItsCellsToCoordinatesMap.begin();
            it!=P_Vertices->at(vindex).ItsCellsToCoordinatesMap.end(); it++)
        {
            Corner_Cells1.insert((*it).first);
        };
        vindex=P_Cells->at(i_cell).Index_ItsCornerVertices[2];
		//for(unsigned int i=0; i<Vertices[vindex].Index_ItsCells.size();i++)
        for(map<unsigned int, Coordinate>::const_iterator it=P_Vertices->at(vindex).ItsCellsToCoordinatesMap.begin(); it!=P_Vertices->at(vindex).ItsCellsToCoordinatesMap.end(); it++)
		{
			Corner_Cells2.insert((*it).first);
		};
        vindex=P_Cells->at(i_cell).Index_ItsCornerVertices[3];
		//for(unsigned int i=0; i<P_Vertices[vindex].Index_ItsCells.size();i++)
        for(map<unsigned int, Coordinate>::const_iterator it=P_Vertices->at(vindex).ItsCellsToCoordinatesMap.begin();
            it!=P_Vertices->at(vindex).ItsCellsToCoordinatesMap.end(); it++)
        {
            Corner_Cells3.insert((*it).first);
        };
		//********************************************
        set<unsigned int> InterSection;
        if (P_Vertices->at(P_Cells->at(i_cell).Index_ItsCornerVertices[0]).Is_interiorVertex
                ||P_Vertices->at(P_Cells->at(i_cell).Index_ItsCornerVertices[1]).Is_interiorVertex)
        {
            set_intersection(Corner_Cells0.begin(), Corner_Cells0.end(),
                             Corner_Cells1.begin(), Corner_Cells1.end(),
                             inserter(InterSection, InterSection.begin()));

            for (set<unsigned int>::const_iterator it=InterSection.begin(); it!=InterSection.end(); it++)
            {
                if (*it!=i_cell)
                {
                    P_Cells->at(i_cell).Index_AdjacentCell_down.clear();
                    P_Cells->at(i_cell).Index_AdjacentCell_down.insert(*it);
                    break;
                };
            };
        }

		//******************************
        if (P_Vertices->at(P_Cells->at(i_cell).Index_ItsCornerVertices[0]).Is_interiorVertex
                ||P_Vertices->at(P_Cells->at(i_cell).Index_ItsCornerVertices[3]).Is_interiorVertex)
        {
			InterSection.clear();
			set_intersection(Corner_Cells0.begin(), Corner_Cells0.end(), Corner_Cells3.begin(), Corner_Cells3.end(),inserter(InterSection, InterSection.begin()));
			for (set<unsigned int>::const_iterator it=InterSection.begin(); it!=InterSection.end(); it++)
			{
				if (*it!=i_cell)
				{
                    P_Cells->at(i_cell).Index_AdjacentCell_left.clear();
                    P_Cells->at(i_cell).Index_AdjacentCell_left.insert(*it);
                    break;
				};
			};

        };
		//******************************
        if (P_Vertices->at(P_Cells->at(i_cell).Index_ItsCornerVertices[2]).Is_interiorVertex
                ||P_Vertices->at(P_Cells->at(i_cell).Index_ItsCornerVertices[3]).Is_interiorVertex)
        {
			InterSection.clear();
			set_intersection(Corner_Cells2.begin(), Corner_Cells2.end(), Corner_Cells3.begin(), Corner_Cells3.end(),inserter(InterSection, InterSection.begin()));
			for (set<unsigned int>::const_iterator it=InterSection.begin(); it!=InterSection.end(); it++)
			{
				if (*it!=i_cell)
				{
                    P_Cells->at(i_cell).Index_AdjacentCell_up.clear();
                    P_Cells->at(i_cell).Index_AdjacentCell_up.insert(*it);
                    break;
				};
			};

        };
		//******************************
        if (P_Vertices->at(P_Cells->at(i_cell).Index_ItsCornerVertices[2]).Is_interiorVertex
                ||P_Vertices->at(P_Cells->at(i_cell).Index_ItsCornerVertices[1]).Is_interiorVertex)
        {
			InterSection.clear();
			set_intersection(Corner_Cells1.begin(), Corner_Cells1.end(), Corner_Cells2.begin(), Corner_Cells2.end(),inserter(InterSection, InterSection.begin()));
			for (set<unsigned int>::const_iterator it=InterSection.begin(); it!=InterSection.end(); it++)
			{
				if (*it!=i_cell)
				{
                    P_Cells->at(i_cell).Index_AdjacentCell_right.clear();
                    P_Cells->at(i_cell).Index_AdjacentCell_right.insert(*it);
                    break;
				};
			};

        };
		
	};
}
//===========================================================

bool Mesh::Is_MeshIrregularVertexConnectedToMeshIrregularVertex()
{
    bool Key=false;//No such type of vertex

    for(map<unsigned int, unsigned int>::const_iterator itIrV=this->MeshIrregularVerticesMap.begin();
        itIrV!=this->MeshIrregularVerticesMap.end(); itIrV++)
    {
        unsigned int IrVIndex=itIrV->first;
        //For the IrVIndex-th vertex,
        //check if the vertices around it are regular,
        //if there is an irregular one, return Key=turn
        for(set<unsigned int>::const_iterator itV=this->P_Vertices->at(IrVIndex).Index_ItsVertices.begin();
            itV!=this->P_Vertices->at(IrVIndex).Index_ItsVertices.end(); itV++)
        {
            unsigned int VIndex=*itV;
            map<unsigned int, unsigned int>::iterator ItIrrgular=this->MeshIrregularVerticesMap.find(VIndex);
            if(ItIrrgular!=this->MeshIrregularVerticesMap.end())//the VIndex-th vertex is irregular
            {
                Key=true;
                break;
            }
        }
        if(Key)//Key==true
        {
            break;
        }
    }

    return Key;
}


//======Subdivision=========================================
//===============================================
struct NewMeshData_Globally
{
	vector<double> Position;//[x0, x1, y0, y1]
	vector<unsigned int> Index_Vertices;//[i1, i2, i3, i4]

	void Cout_NewMeshData_Globally()
	{
		cout<<"this cell's Position=["<<Position[0]<<", "<<Position[1]<<"]X["<<Position[2]<<", "<<Position[3]<<"]"<<endl;
		cout<<"Indices of Vertices: "<<Index_Vertices[0]<<", "<<Index_Vertices[1]<<", "<<Index_Vertices[2]<<", "<<Index_Vertices[3]<<endl;
	};
};
struct NewInformation//New Information for each original cell
{
	unsigned int cell_index;
	vector<NewMeshData_Globally> NewDatas;
	vector<unsigned int>  Up_indexSeq;
	vector<unsigned int>  Down_indexSeq;
	vector<unsigned int>  Left_indexSeq;
	vector<unsigned int>  Right_indexSeq;
	void Cout_NewInformation()
	{
		cout<<"The index of Original Cell: "<<cell_index<<endl;
		cout<<"SubCells:"<<endl;
		for (unsigned int i=0; i<NewDatas.size(); i++)
		{
			NewDatas[i].Cout_NewMeshData_Globally();
		}
		cout<<"Up_indexSeq:"<<endl;
		for (unsigned int i=0; i<Up_indexSeq.size(); i++)
		{
			cout<<Up_indexSeq[i]<<" ";
		}
		cout<<endl;
		cout<<"Down_indexSeq:"<<endl;
		for (unsigned int i=0; i<Down_indexSeq.size(); i++)
		{
			cout<<Down_indexSeq[i]<<" ";
		}
		cout<<endl;
		cout<<"Left_indexSeq:"<<endl;
		for (unsigned int i=0; i<Left_indexSeq.size(); i++)
		{
			cout<<Left_indexSeq[i]<<" ";
		}
		cout<<endl;
		cout<<"Right_indexSeq:"<<endl;
		for (unsigned int i=0; i<Right_indexSeq.size(); i++)
		{
			cout<<Right_indexSeq[i]<<" ";
		}
		cout<<endl;
        system("PAUSE");

	};

};
static unsigned int ComputerIndex(unsigned int i_s, unsigned int i_t, unsigned int Ns, unsigned int Nt)
{
	return (Nt+2)*i_s+i_t;
};
void Mesh::subdivide_OneCell(unsigned int cell_index, unsigned int k, NewInformation& NewInfo, map<unsigned int, set<unsigned int> > &OriginalVertexToCurrentVertices)
{//Subdivde Cells[cell_index]---
    NewInfo.NewDatas.clear();
    NewInfo.Up_indexSeq.clear();
    NewInfo.Down_indexSeq.clear();
    NewInfo.Left_indexSeq.clear();
    NewInfo.Right_indexSeq.clear();
    //--------
    NewInfo.cell_index=cell_index;
    //--------
    double s0=P_Cells->at(cell_index).CellSize[0];
    double s1=P_Cells->at(cell_index).CellSize[1];
    double t0=P_Cells->at(cell_index).CellSize[2];
    double t1=P_Cells->at(cell_index).CellSize[3];
    //--------
    double ds=(s1-s0)/(k+1);
    double dt=(t1-t0)/(k+1);
    //--NewDatas
    for(unsigned int i_s=0; i_s<k+1; i_s++)
    {
        for(unsigned int i_t=0; i_t<k+1; i_t++)
        {
            NewMeshData_Globally NewData;
            //NewData.Index_Vertices
            unsigned int i0= ComputerIndex(i_s, i_t, k, k)+(k+2)*(k+2)*cell_index;
            NewData.Index_Vertices.push_back(i0);
            //--Position
            double x0=s0+i_s*ds;
            double y0=t0+i_t*dt;
            //----------
            i0=ComputerIndex(i_s+1, i_t, k, k)+(k+2)*(k+2)*cell_index;
            NewData.Index_Vertices.push_back(i0);
            i0=ComputerIndex(i_s+1, i_t+1, k, k)+(k+2)*(k+2)*cell_index;
            NewData.Index_Vertices.push_back(i0);
            //--Position
            double x1=s0+(i_s+1)*ds;
            double y1=t0+(i_t+1)*dt;
            //----------
            i0=ComputerIndex(i_s, i_t+1, k, k)+(k+2)*(k+2)*cell_index;
            NewData.Index_Vertices.push_back(i0);
            //NewData.Position
            NewData.Position.push_back(x0);
            NewData.Position.push_back(x1);
            NewData.Position.push_back(y0);
            NewData.Position.push_back(y1);
            //NewDatas.push_back NewData
            NewInfo.NewDatas.push_back(NewData);
        };
    };

    //***********************indexSeq*************************************
	for (unsigned int i=0; i<k+2; i++)
	{
		//---NewInfo.Up_indexSeq
		NewInfo.Up_indexSeq.push_back((k+2)*(i+1)-1+(k+2)*(k+2)*cell_index);
		//---NewInfo.Down_indexSeq
		NewInfo.Down_indexSeq.push_back((k+2)*i+(k+2)*(k+2)*cell_index);
		//---NewInfo.Left_indexSeq
		NewInfo.Left_indexSeq.push_back(i+(k+2)*(k+2)*cell_index);
		//---NewInfo.Right_indexSeq
		NewInfo.Right_indexSeq.push_back(i+(k+1)*(k+2)+(k+2)*(k+2)*cell_index);
	}
	//********************************************************************

	//Initial OriginalVertexToCurrentVertex:
    for (unsigned int j=0; j<P_Cells->at(cell_index).Index_ItsCornerVertices.size(); j++)
    {
        map<unsigned int, set<unsigned int> >::iterator it=OriginalVertexToCurrentVertices.find(P_Cells->at(cell_index).Index_ItsCornerVertices[j]);
        if (it==OriginalVertexToCurrentVertices.end())
        {
            set<unsigned int> NewVertices;
            switch(j)
            {
            case 0:
                NewVertices.insert(cell_index*(k+2)*(k+2));
                OriginalVertexToCurrentVertices.insert(pair<unsigned int, set<unsigned int> >(P_Cells->at(cell_index).Index_ItsCornerVertices[0], NewVertices));
                break;

            case 1:
                NewVertices.insert(cell_index*(k+2)*(k+2)+(k+1)*(k+2));
                OriginalVertexToCurrentVertices.insert(pair<unsigned int, set<unsigned int> >(P_Cells->at(cell_index).Index_ItsCornerVertices[1], NewVertices));
                break;

            case 2:
                NewVertices.insert((cell_index+1)*(k+2)*(k+2)-1);
                OriginalVertexToCurrentVertices.insert(pair<unsigned int, set<unsigned int> >(P_Cells->at(cell_index).Index_ItsCornerVertices[2], NewVertices));
                break;

            case 3:
                NewVertices.insert(cell_index*(k+2)*(k+2)+(k+1));
                OriginalVertexToCurrentVertices.insert(pair<unsigned int, set<unsigned int> >(P_Cells->at(cell_index).Index_ItsCornerVertices[3], NewVertices));
                break;

            }
        }
        else
        {
            switch(j)
            {
            case 0:
                it->second.insert(cell_index*(k+2)*(k+2));
                break;

            case 1:
                it->second.insert(cell_index*(k+2)*(k+2)+(k+1)*(k+2));
                break;

            case 2:
                it->second.insert((cell_index+1)*(k+2)*(k+2)-1);
                break;

            case 3:
                it->second.insert(cell_index*(k+2)*(k+2)+(k+1));
                break;
            }

        }
    }
};
//--------------------------------
void Mesh::SubdivideCells(unsigned int k, vector<NewInformation>& NewInfoCells, map<unsigned int, set<unsigned int> > &OriginalVertexToCurrentVertices)
{
    NewInfoCells.clear();
    OriginalVertexToCurrentVertices.clear();
    //-------------
    //Subdivide all the cells
    for(unsigned int i_cell = 0; i_cell < P_Cells->size(); i_cell ++)
    {
        NewInformation NewInfo;
        subdivide_OneCell(i_cell, k, NewInfo, OriginalVertexToCurrentVertices);
        NewInfoCells.push_back(NewInfo);
    };


};


static void Correspondence_Positive(unsigned int low_index, unsigned int index, unsigned int flag, unsigned int flag2, vector<NewInformation> & NewInfoCells, map<unsigned int, set<unsigned int> > & OriginalVertexToCurrentVertices)
{
    vector<unsigned int> * P_low_index_verticesSeq = NULL;
    //ExtractVerticesSeq(flag, low_index, NewInfoCells[low_index], low_index_verticesSeq, Times_sDirection, Times_tDirections);
    switch(flag)
    {
    case 1://Up
            //low_index_verticesSeq(NewInfoCells[low_index].Up_indexSeq);
            P_low_index_verticesSeq = & NewInfoCells[low_index].Up_indexSeq;
            break;
    case 2://Down
            //low_index_verticesSeq(NewInfoCells[low_index].Down_indexSeq);
            P_low_index_verticesSeq = &NewInfoCells[low_index].Down_indexSeq;
            break;
    case 3://Left
            //low_index_verticesSeq(NewInfoCells[low_index].Left_indexSeq);
            P_low_index_verticesSeq = &NewInfoCells[low_index].Left_indexSeq;
            break;
    case 4://Right
            //low_index_verticesSeq(NewInfoCells[low_index].Right_indexSeq);
            P_low_index_verticesSeq= &NewInfoCells[low_index].Right_indexSeq;
            break;
    }


    vector<unsigned int> * P_index_verticesSeq;
    //ExtractVerticesSeq(flag2, index, NewInfoCells[index], index_verticesSeq, Times_sDirection, Times_tDirections);
    switch(flag2)
    {
    case 1://Up
            //index_verticesSeq(NewInfoCells[index].Up_indexSeq);
            P_index_verticesSeq = &NewInfoCells[index].Up_indexSeq;
            break;
    case 2://Down
            //index_verticesSeq(NewInfoCells[index].Down_indexSeq);
            P_index_verticesSeq = &NewInfoCells[index].Down_indexSeq;
            break;
    case 3://Left
            //index_verticesSeq(NewInfoCells[index].Left_indexSeq);
            P_index_verticesSeq = &NewInfoCells[index].Left_indexSeq;
            break;
    case 4://Right
            //index_verticesSeq(NewInfoCells[index].Right_indexSeq);
            P_index_verticesSeq = &NewInfoCells[index].Right_indexSeq;
            break;
    }

//        //For testing=======
//      cout<<"low_index = "<<low_index<<endl;
//        cout<<"index = "<<index<<endl;
//        for (unsigned int i=0; i<P_index_verticesSeq->size(); i++)
//        {
//            cout<<P_index_verticesSeq->at(i)<<endl;
//        };

//        cout<<"flag2"<<endl;
//        for(unsigned int i=0; i<P_low_index_verticesSeq->size(); i++)
//        {
//            cout<<P_low_index_verticesSeq->at(i)<<endl;
//        };
//        //===================



    //-------
    map<unsigned int, unsigned int> Correspondent, Correspondent2;
    for(unsigned int i=0; i<P_index_verticesSeq->size(); i++)
    {
         // //////////////////////////////
         Correspondent.insert(pair<unsigned int, unsigned int>(P_index_verticesSeq->at(i), P_low_index_verticesSeq->at(i)));
         Correspondent2.insert(pair<unsigned int, unsigned int>(P_low_index_verticesSeq->at(i), P_index_verticesSeq->at(i)) );
    };

    //=====================================================================
    map<unsigned int, unsigned int> IndexSeqCorrespondent, LowIndexSeqCorrespondent;
    //__________________________________________________________________________
    for(unsigned int i=0; i<NewInfoCells[index].NewDatas.size(); i++)
    {
        for(unsigned int j=0; j<NewInfoCells[index].NewDatas[i].Index_Vertices.size(); j++)
        {
            unsigned int vindex = NewInfoCells[index].NewDatas[i].Index_Vertices[j];
            map<unsigned int, unsigned int>::iterator it = Correspondent.find(vindex);
            if(it!=Correspondent.end())
            {
                if(it->second < it->first)
                {
                    NewInfoCells[index].NewDatas[i].Index_Vertices[j]=(*it).second;
                    IndexSeqCorrespondent.insert(pair<unsigned int, unsigned int>(vindex, it->second));
                }

            }

        };
    };

    for(unsigned int i=0; i < NewInfoCells[low_index].NewDatas.size(); i++)
    {
        for(unsigned int j=0; j < NewInfoCells[low_index].NewDatas[i].Index_Vertices.size(); j++)
        {
            unsigned int vindex = NewInfoCells[low_index].NewDatas[i].Index_Vertices[j];
            map<unsigned int, unsigned int>::iterator it = Correspondent2.find(vindex);
            if(it!=Correspondent2.end() && (it->second<it->first) )
            {
                NewInfoCells[low_index].NewDatas[i].Index_Vertices[j]=(*it).second;
                LowIndexSeqCorrespondent.insert(pair<unsigned int, unsigned int>(vindex, it->second));
            }
        }
    }
    //------------------------------------------------
    //-----NewInfoCells[index]------------------------
    //Up
    for (unsigned int i=0; i<NewInfoCells[index].Up_indexSeq.size();i++)
    {
        unsigned int vindex = NewInfoCells[index].Up_indexSeq[i];
        map<unsigned int, unsigned int>::iterator it=IndexSeqCorrespondent.find(vindex);
        if (it!=IndexSeqCorrespondent.end())
        {
                NewInfoCells[index].Up_indexSeq[i]=(*it).second;
        }
    }
    //Down
    for (unsigned int i=0; i<NewInfoCells[index].Down_indexSeq.size();i++)
    {
        unsigned int vindex = NewInfoCells[index].Down_indexSeq[i];
        map<unsigned int, unsigned int>::iterator it=IndexSeqCorrespondent.find(vindex);
        if (it!=IndexSeqCorrespondent.end())
        {
                NewInfoCells[index].Down_indexSeq[i]=(*it).second;
        }
    }
    //Left
    for (unsigned int i=0; i<NewInfoCells[index].Left_indexSeq.size(); i++)
    {
        unsigned int vindex = NewInfoCells[index].Left_indexSeq[i];
        map<unsigned int, unsigned int>::iterator it=IndexSeqCorrespondent.find(vindex);
        if (it!=IndexSeqCorrespondent.end())
        {
                NewInfoCells[index].Left_indexSeq[i]=(*it).second;
        }
    }
    //Right
    for (unsigned int i=0; i<NewInfoCells[index].Right_indexSeq.size(); i++)
    {
        unsigned int vindex = NewInfoCells[index].Right_indexSeq[i];
        map<unsigned int, unsigned int>::iterator it=IndexSeqCorrespondent.find(vindex);
        if (it!=IndexSeqCorrespondent.end())
        {
                NewInfoCells[index].Right_indexSeq[i]=(*it).second;
        }
    }

    //-----NewInfoCells[low_index]------------------------
    //Up
    for (unsigned int i=0; i<NewInfoCells[low_index].Up_indexSeq.size();i++)
    {
        unsigned int vindex = NewInfoCells[low_index].Up_indexSeq[i];
        map<unsigned int, unsigned int>::iterator it=LowIndexSeqCorrespondent.find(vindex);
        if (it!=LowIndexSeqCorrespondent.end())
        {
                NewInfoCells[low_index].Up_indexSeq[i]=(*it).second;
        }
    }
    //Down
    for (unsigned int i=0; i<NewInfoCells[low_index].Down_indexSeq.size();i++)
    {
        unsigned int vindex = NewInfoCells[low_index].Down_indexSeq[i];
        map<unsigned int, unsigned int>::iterator it=LowIndexSeqCorrespondent.find(vindex);
        if (it!=LowIndexSeqCorrespondent.end())
        {
                NewInfoCells[low_index].Down_indexSeq[i]=(*it).second;
        }
    }
    //Left
    for (unsigned int i=0; i<NewInfoCells[low_index].Left_indexSeq.size();i++)
    {
        unsigned int vindex = NewInfoCells[low_index].Left_indexSeq[i];
        map<unsigned int, unsigned int>::iterator it=LowIndexSeqCorrespondent.find(vindex);
        if (it!=LowIndexSeqCorrespondent.end())
        {
                NewInfoCells[low_index].Left_indexSeq[i]=(*it).second;
        }
    }
    //Right
    for (unsigned int i=0; i<NewInfoCells[low_index].Right_indexSeq.size();i++)
    {
        unsigned int vindex = NewInfoCells[low_index].Right_indexSeq[i];
        map<unsigned int, unsigned int>::iterator it=LowIndexSeqCorrespondent.find(vindex);
        if (it!=LowIndexSeqCorrespondent.end())
        {
                NewInfoCells[low_index].Right_indexSeq[i]=(*it).second;
        }
    }
//    //------------------------------------------------
//    //map<unsigned int, unsigned int> & OriginalVertexToCurrentVertex
//    for (map<unsigned int, unsigned int>::iterator itMap=OriginalVertexToCurrentVertex.begin(); itMap!=OriginalVertexToCurrentVertex.end(); itMap++)
//    {
//        map<unsigned int, unsigned int>::iterator it_LowIndex=LowIndexSeqCorrespondent.find(itMap->second);
//        map<unsigned int, unsigned int>::iterator it_Index=IndexSeqCorrespondent.find(itMap->second);
//        if (it_LowIndex!=LowIndexSeqCorrespondent.end())
//        {
//                itMap->second=it_LowIndex->second;
//        }
//        if(it_Index!=IndexSeqCorrespondent.end())
//        {
//            itMap->second=it_Index->second;
//        }
//    }
};
static void Correspondence_Nagetive(unsigned int low_index, unsigned int index, unsigned int flag, unsigned int flag2, vector<NewInformation> & NewInfoCells, map<unsigned int, set<unsigned int> > & OriginalVertexToCurrentVertices)//, unsigned int k)
//static void Correspondence_Nagetive(unsigned int dV, unsigned int low_index, unsigned int index, unsigned int flag,unsigned int flag2, vector<NewInformation> & NewInfoCells, const vector<unsigned int>& Times_sDirection, const vector<unsigned int>& Times_tDirections)
{
    vector<unsigned int>* P_low_index_verticesSeq=NULL;

    switch(flag)
    {
    case 1://Up
        //low_index_verticesSeq(NewInfoCells[low_index].Up_indexSeq);
        P_low_index_verticesSeq = &NewInfoCells[low_index].Up_indexSeq;
        break;
    case 2://Down
        //low_index_verticesSeq(NewInfoCells[low_index].Down_indexSeq);
        P_low_index_verticesSeq = &NewInfoCells[low_index].Down_indexSeq;
        break;
    case 3://Left
        //low_index_verticesSeq(NewInfoCells[low_index].Left_indexSeq);
        P_low_index_verticesSeq = &NewInfoCells[low_index].Left_indexSeq;
        break;
    case 4://Right
        //low_index_verticesSeq(NewInfoCells[low_index].Right_indexSeq);
        P_low_index_verticesSeq = &NewInfoCells[low_index].Right_indexSeq;
        break;
    }


	// 	//For testing=======
	// 	for (unsigned int i=0; i<low_index_verticesSeq.size(); i++)
	// 	{
	// 		cout<<low_index_verticesSeq[i]<<endl;
	// 	};
	// 	//===================


    vector<unsigned int>* P_index_verticesSeq;
    switch(flag2)
    {
    case 1://Up
        //index_verticesSeq(NewInfoCells[index].Up_indexSeq);
        P_index_verticesSeq = &NewInfoCells[index].Up_indexSeq;
        break;
    case 2://Down
        //index_verticesSeq(NewInfoCells[index].Down_indexSeq);
        P_index_verticesSeq = &NewInfoCells[index].Down_indexSeq;
        break;
    case 3://Left
        //index_verticesSeq(NewInfoCells[index].Left_indexSeq);
        P_index_verticesSeq = &NewInfoCells[index].Left_indexSeq;
        break;
    case 4://Right
        //index_verticesSeq(NewInfoCells[index].Right_indexSeq);
        P_index_verticesSeq = &NewInfoCells[index].Right_indexSeq;
        break;
    }


    //-------
    // 	//For testing=======
    // 	for (unsigned int i=0; i<index_verticesSeq.size(); i++)
    // 	{
    // 		cout<<index_verticesSeq[i]<<endl;
    // 	};
    // 	//===================


    map<unsigned int, unsigned int> Correspondent, Correspondent2;

    for(unsigned int i=0; i<P_index_verticesSeq->size(); i++)
    {
        Correspondent.insert(pair<unsigned int, unsigned int>(P_index_verticesSeq->at(i), P_low_index_verticesSeq->at(P_index_verticesSeq->size()-1-i)));
        Correspondent2.insert(pair<unsigned int, unsigned int>( P_low_index_verticesSeq->at(i), P_index_verticesSeq->at(P_index_verticesSeq->size()-1-i)) );
    };

    map<unsigned int, unsigned int> LowIndexSeqCorrespondent, IndexSeqCorrespondent;
    //Modify the indices in NewInfoCells[index]
    for(unsigned int i=0; i<NewInfoCells[index].NewDatas.size(); i++)
    {
        for(unsigned int j=0; j<NewInfoCells[index].NewDatas[i].Index_Vertices.size(); j++)
        {
            unsigned int vindex = NewInfoCells[index].NewDatas[i].Index_Vertices[j];
            map<unsigned int, unsigned int>::iterator it = Correspondent.find(vindex);
            if(it!=Correspondent.end() && (it->first > it->second) )
            {
                NewInfoCells[index].NewDatas[i].Index_Vertices[j]=(*it).second;//Map iterator!!!

                IndexSeqCorrespondent.insert(pair<unsigned int, unsigned int>(vindex, it->second) );
            };
        };
    };

    //Modify the indices in NewInfoCells[low_index]
    for(unsigned int i=0; i<NewInfoCells[low_index].NewDatas.size(); i++)
    {
        for(unsigned int j=0; j<NewInfoCells[low_index].NewDatas[i].Index_Vertices.size(); j++)
        {
            unsigned int vindex = NewInfoCells[low_index].NewDatas[i].Index_Vertices[j];
            map<unsigned int, unsigned int>::iterator it = Correspondent2.find(vindex);
            if(it!=Correspondent2.end() && (it->first > it->second) )
            {
                NewInfoCells[low_index].NewDatas[i].Index_Vertices[j]=it->second;
                LowIndexSeqCorrespondent.insert(pair<unsigned int, unsigned int>(vindex, it->second) );
            }
        }
    }
    //------------------------------------------------
    //NewInfoCells[index]
    //Up
    for (unsigned int i=0; i<NewInfoCells[index].Up_indexSeq.size();i++)
    {
        unsigned int vindex = NewInfoCells[index].Up_indexSeq[i];
        map<unsigned int, unsigned int>::iterator it=IndexSeqCorrespondent.find(vindex);
        if (it!=IndexSeqCorrespondent.end())
        {
            NewInfoCells[index].Up_indexSeq[i]=(*it).second;
        }
    }
    //Down
    for (unsigned int i=0; i<NewInfoCells[index].Down_indexSeq.size();i++)
    {
        unsigned int vindex = NewInfoCells[index].Down_indexSeq[i];
        map<unsigned int, unsigned int>::iterator it=IndexSeqCorrespondent.find(vindex);
        if (it!=IndexSeqCorrespondent.end())
        {
            NewInfoCells[index].Down_indexSeq[i]=(*it).second;
        }
    }
    //Left
    for (unsigned int i=0; i<NewInfoCells[index].Left_indexSeq.size();i++)
    {
        unsigned int vindex = NewInfoCells[index].Left_indexSeq[i];
        map<unsigned int, unsigned int>::iterator it=IndexSeqCorrespondent.find(vindex);
        if (it!=IndexSeqCorrespondent.end())
        {
            NewInfoCells[index].Left_indexSeq[i]=(*it).second;
        }
    }
    //Right
    for (unsigned int i=0; i<NewInfoCells[index].Right_indexSeq.size();i++)
    {
        unsigned int vindex = NewInfoCells[index].Right_indexSeq[i];
        map<unsigned int, unsigned int>::iterator it=IndexSeqCorrespondent.find(vindex);
        if (it!=IndexSeqCorrespondent.end())
        {
            NewInfoCells[index].Right_indexSeq[i]=(*it).second;
        }
    }

    //------------------------------------------------
    //NewInfoCells[low_index]
    //Up
    for (unsigned int i=0; i<NewInfoCells[low_index].Up_indexSeq.size();i++)
    {
        unsigned int vindex = NewInfoCells[low_index].Up_indexSeq[i];
        map<unsigned int, unsigned int>::iterator it=LowIndexSeqCorrespondent.find(vindex);
        if (it!=LowIndexSeqCorrespondent.end())
        {
            NewInfoCells[low_index].Up_indexSeq[i]=(*it).second;
        }
    }
    //Down
    for (unsigned int i=0; i<NewInfoCells[low_index].Down_indexSeq.size();i++)
    {
        unsigned int vindex = NewInfoCells[low_index].Down_indexSeq[i];
        map<unsigned int, unsigned int>::iterator it=LowIndexSeqCorrespondent.find(vindex);
        if (it!=LowIndexSeqCorrespondent.end())
        {
            NewInfoCells[low_index].Down_indexSeq[i]=(*it).second;
        }
    }
    //Left
    for (unsigned int i=0; i<NewInfoCells[low_index].Left_indexSeq.size();i++)
    {
        unsigned int vindex = NewInfoCells[low_index].Left_indexSeq[i];
        map<unsigned int, unsigned int>::iterator it=LowIndexSeqCorrespondent.find(vindex);
        if (it!=LowIndexSeqCorrespondent.end())
        {
            NewInfoCells[low_index].Left_indexSeq[i]=(*it).second;
        }
    }
    //Right
    for (unsigned int i=0; i<NewInfoCells[low_index].Right_indexSeq.size();i++)
    {
        unsigned int vindex = NewInfoCells[low_index].Right_indexSeq[i];
        map<unsigned int, unsigned int>::iterator it=LowIndexSeqCorrespondent.find(vindex);
        if (it!=LowIndexSeqCorrespondent.end())
        {
            NewInfoCells[low_index].Right_indexSeq[i]=(*it).second;
        }
    }

//    //------------------------------------------------
//    //map<unsigned int, unsigned int> & OriginalVertexToCurrentVertex
//    for (map<unsigned int, unsigned int>::iterator itMap=OriginalVertexToCurrentVertex.begin(); itMap!=OriginalVertexToCurrentVertex.end(); itMap++)
//    {
//        map<unsigned int, unsigned int>::iterator it_lowindex=LowIndexSeqCorrespondent.find(itMap->second);
//        map<unsigned int, unsigned int>::iterator it_index=IndexSeqCorrespondent.find(itMap->second);
//        if (it_lowindex!=LowIndexSeqCorrespondent.end())
//        {
//            itMap->second=it_lowindex->second;
//        }
//        if(it_index!=IndexSeqCorrespondent.end())
//        {
//            itMap->second=it_index->second;
//        }
//    }

};
static void DealWithNewInfoCells(unsigned int low_index, unsigned int index, unsigned int flag,
                                 unsigned int flag2, vector<NewInformation> & NewInfoCells,
                  map<unsigned int, set<unsigned int> > & OriginalVertexToCurrentVertices)
{//Here, we suppose that all the local frames of cells are right-handed. 

    //cout<<"Mesh::DealWithNewInfoCells"<<endl;

    //-------------------------
    if ((flag+flag2)%2)//odd
    {
        Correspondence_Positive(low_index, index, flag, flag2, NewInfoCells, OriginalVertexToCurrentVertices);
    }
    else
    {
        Correspondence_Nagetive(low_index, index, flag, flag2, NewInfoCells, OriginalVertexToCurrentVertices);
    }

// //For testing----------------------------
// 	for (unsigned int i=0; i<NewInfoCells.size(); i++)
// 	{
// 		cout<<"=============================="<<endl;
// 		for (unsigned int j=0; j<NewInfoCells[i].NewDatas.size(); j++)
// 		{
// 			cout<<"The Cell's Position: ["<<NewInfoCells[i].NewDatas[j].Position[0]<<", "<<NewInfoCells[i].NewDatas[j].Position[1]<<"]X["<<NewInfoCells[i].NewDatas[j].Position[2]<<", "<<NewInfoCells[i].NewDatas[j].Position[3]<<"]"<<endl;
// 			cout<<"The indices of Cell's vertices: "<<NewInfoCells[i].NewDatas[j].Index_Vertices[0]<<", "<<NewInfoCells[i].NewDatas[j].Index_Vertices[1]<<", "<<NewInfoCells[i].NewDatas[j].Index_Vertices[2]<<", "<<NewInfoCells[i].NewDatas[j].Index_Vertices[3]<<endl;
// 		};
// 		cout<<"=============================="<<endl;
// 	};
// 	//---------------------------------------

}
void Mesh::MergeVertices(unsigned int low_index, unsigned int index, vector<NewInformation> &NewInfoCells,
                         unsigned int flag, map<unsigned int, set<unsigned int> > & OriginalVertexToCurrentVertices)
{
    unsigned int flag2;
    bool key=true;
    //When for each vertex of the Initial Mesh, the 1-neighborhood of this vertex is homeomorphic to B(1)\subset R^2,
    //The Cells[index]'s relative position for Cells[low_index] is one and only one of "up, down, left, right".
    //--

    unsigned int It;
    if(P_Cells->at(index).Index_AdjacentCell_up.size())
    {
        It= *(P_Cells->at(index).Index_AdjacentCell_up.begin());
        if(It==low_index)
        {
            flag2=1;//"up";
            key=false;
        };
    };

    //--
    if((key) && (P_Cells->at(index).Index_AdjacentCell_down.size()))//if "low_index" doesn't find
    {
        It=*(P_Cells->at(index).Index_AdjacentCell_down.begin());
        if(It==low_index)
        {
            flag2=2;//"down";
            key=false;
        };
    };

    //--
    if(key && (P_Cells->at(index).Index_AdjacentCell_left.size()))//if "low_index" doesn't find
    {
        It=*(P_Cells->at(index).Index_AdjacentCell_left.begin());
        if(It==low_index)
        {
            flag2=3;//"left";
            key=false;
        };
    };

    //--
    if(key && (P_Cells->at(index).Index_AdjacentCell_right.size()))//if "low_index" doesn't find
    {
        It=*(P_Cells->at(index).Index_AdjacentCell_right.begin());
        if(It==low_index)
        {
            flag2=4;//"right";
            key=false;
        };
    };

    //---
    if(key)
    {
        cout<<"Mesh::ReOrganizeVerticesIndices_MergeTheSameVertices:: MergeVertices:: Adjacent cells are not adjacent????"<<endl;
        cin.get();
    };

    DealWithNewInfoCells(low_index, index, flag, flag2, NewInfoCells, OriginalVertexToCurrentVertices);

	// 	//For Testing---------
	// 	cout<<"======================================="<<endl;
	// 	for (unsigned int j=0; j<NewInfoCells[low_index].NewDatas.size(); j++)
	// 	{
	// 		cout<<"Cell's Position=["<<NewInfoCells[low_index].NewDatas[j].Position[0]<<", "<<NewInfoCells[low_index].NewDatas[j].Position[1]<<"]X[ "<<NewInfoCells[low_index].NewDatas[j].Position[2]<<", "<<NewInfoCells[low_index].NewDatas[j].Position[3]<<"] "<<endl;
	// 		cout<<"Indices of Cell's vertices "<<NewInfoCells[low_index].NewDatas[j].Index_Vertices[0]<<", "<<NewInfoCells[low_index].NewDatas[j].Index_Vertices[1]<<", "<<NewInfoCells[low_index].NewDatas[j].Index_Vertices[2]<<", "<<NewInfoCells[low_index].NewDatas[j].Index_Vertices[3]<<endl;
	// 	};
	// 	cout<<"======================================="<<endl;
	// 	for (unsigned int j=0; j<NewInfoCells[index].NewDatas.size(); j++)
	// 	{
	// 		cout<<"Cell's Position=["<<NewInfoCells[index].NewDatas[j].Position[0]<<", "<<NewInfoCells[index].NewDatas[j].Position[1]<<"]X[ "<<NewInfoCells[index].NewDatas[j].Position[2]<<", "<<NewInfoCells[index].NewDatas[j].Position[3]<<"] "<<endl;
	// 		cout<<"Indices of Cell's vertices "<<NewInfoCells[index].NewDatas[j].Index_Vertices[0]<<", "<<NewInfoCells[index].NewDatas[j].Index_Vertices[1]<<", "<<NewInfoCells[index].NewDatas[j].Index_Vertices[2]<<", "<<NewInfoCells[index].NewDatas[j].Index_Vertices[3]<<endl;
	// 	};

};
static void Re_ReOrganize(vector<NewInformation> &NewInfoCells, unsigned int &VertexNum, map<unsigned int, unsigned int> & OriginalVertexToCurrentVertex)
{
	set<unsigned int> VerticesIndex;
	for(unsigned int i=0; i<NewInfoCells.size(); i++)
	{
		for(unsigned int j=0; j<NewInfoCells[i].NewDatas.size(); j++)
		{
			for(unsigned int k=0; k<NewInfoCells[i].NewDatas[j].Index_Vertices.size(); k++)
			{
				VerticesIndex.insert(NewInfoCells[i].NewDatas[j].Index_Vertices[k]);
			};
		};
	};
	//The number of all the vertices
	VertexNum=VerticesIndex.size();

	//The map of index of vertices
	map<unsigned int, unsigned int> MAP_Index;
	unsigned int i=0;
	for(set<unsigned int>::const_iterator it=VerticesIndex.begin(); it!=VerticesIndex.end(); it++)
	{
		if(*it!=i)
		{
			MAP_Index[*it]=i;
		};
		i++;
	};
	//Modify the index of vertices
	for(unsigned int i=0; i<NewInfoCells.size(); i++)
	{
		for(unsigned int j=0; j<NewInfoCells[i].NewDatas.size(); j++)
		{
			for(unsigned int k=0; k<NewInfoCells[i].NewDatas[j].Index_Vertices.size(); k++)
			{
				unsigned int Index = NewInfoCells[i].NewDatas[j].Index_Vertices[k];
				map<unsigned int, unsigned int>::iterator it=MAP_Index.find(Index);
				if(it!=MAP_Index.end())
				{
					NewInfoCells[i].NewDatas[j].Index_Vertices[k]=(*it).second; 
				};
			};
		};
	};
	//map<unsigned int, unsigned int> & OriginalVertexToCurrentVertex
	for (map<unsigned int, unsigned int>::iterator itMap=OriginalVertexToCurrentVertex.begin(); itMap!=OriginalVertexToCurrentVertex.end(); itMap++)
	{
		map<unsigned int, unsigned int>::iterator it=MAP_Index.find(itMap->second);
		if (it!=MAP_Index.end())
		{
			itMap->second=it->second;
		}
	}
	//----------------------------------------------------------------------
};
void Mesh::ReOrganizeVerticesIndices_MergeTheSameVertices(vector<NewInformation> &NewInfoCells,
               unsigned int k, unsigned int &VertexNum,
               map<unsigned int, set<unsigned int> > & OriginalVertexToCurrentVertices,
               map<unsigned int, unsigned int> &OriginalVertexToCurrentVertex)
{
	
    for (unsigned int cell_index=0; cell_index < P_Cells->size(); cell_index++)
    {
		//For NewInfoCells[cell_index], Merge The Same Vertices in adjacent cells with Cells[cell_index]
		//Cells[cell_index].Index_AdjacentCells_up
        unsigned int flag, It;

        if(P_Cells->at(cell_index).Index_AdjacentCell_up.size())
        {

            It=*(P_Cells->at(cell_index).Index_AdjacentCell_up.begin());
            // ___________
            // |          |
            // |  It     |
            // |          |
            // |__________|
            // |          |
            // |P_Cells(ce|
            // | ll_index)|
            // |__________|
            if(It>cell_index)
            {
                flag=1;//--->up
                MergeVertices(cell_index, It, NewInfoCells, flag, OriginalVertexToCurrentVertices);
            };
        };


		//------
		//Cells[cell_index].Index_AdjacentCells_down
        if(P_Cells->at(cell_index).Index_AdjacentCell_down.size())
        {

            It=*(P_Cells->at(cell_index).Index_AdjacentCell_down.begin());
            // ___________
            // |          |
            // |P_Cells(ce|
            // |ll_index) |
            // |__________|
            // |          |
            // |  It     |
            // |          |
            // |__________|
            if(It>cell_index)
            {
                flag=2;//--->down
                MergeVertices(cell_index, It, NewInfoCells, flag, OriginalVertexToCurrentVertices);
            };
        };

		//------
		//Cells[cell_index].Index_AdjacentCells_left
        if(P_Cells->at(cell_index).Index_AdjacentCell_left.size())
        {

            It=*(P_Cells->at(cell_index).Index_AdjacentCell_left.begin());
            //_______________________
            //|         |            |
            //|  It    |P_Cells->at |
            //|         |(cell_index)|
            //|_________|____________|
            if(It>cell_index)
            {
                flag=3;//--->left
                MergeVertices(cell_index,It, NewInfoCells,flag, OriginalVertexToCurrentVertices);
            };
        };


		//------
		//Cells[cell_index].Index_AdjacentCells_right
        if(P_Cells->at(cell_index).Index_AdjacentCell_right.size())
        {

            It=*(P_Cells->at(cell_index).Index_AdjacentCell_right.begin());
            //_________________________
            //|            |           |
            //|P_Cells->at |    It    |
            //|(cell_index)|           |
            //|____________|___________|
            if(It>cell_index)
            {
                flag=4;//--->right
                MergeVertices(cell_index, It, NewInfoCells, flag, OriginalVertexToCurrentVertices);
            };
        };
    };
    //-----------------------------
    //Generate a map from CurrentVertices to the original vertices
    map<unsigned, unsigned> CurrentVerticesToMinIndex;
    OriginalVertexToCurrentVertex.clear();
    for(map<unsigned int, set<unsigned int> >::const_iterator itmapV=OriginalVertexToCurrentVertices.begin();
        itmapV!=OriginalVertexToCurrentVertices.end(); itmapV++)
    {
        //Get the minimue value from itmapV->second
        unsigned int minIndex=*(itmapV->second.begin());
        for(set<unsigned int>::const_iterator its=itmapV->second.begin();
            its!=itmapV->second.end(); its++)
        {
            if(*its<minIndex)
            {
                minIndex=*its;
            }
        }
    //=====================================================================
        //CurrentVertices to the minimue index
        for(set<unsigned int>::const_iterator itsetV=(itmapV->second).begin();
            itsetV!=(itmapV->second).end(); itsetV++)
        {
            CurrentVerticesToMinIndex.insert(pair<unsigned int, unsigned int>(*itsetV, minIndex));
        }

        //
        OriginalVertexToCurrentVertex.insert(pair<unsigned int, unsigned int>(itmapV->first, minIndex));

    }

    //vector<NewInformation> &NewInfoCells modify
    for(unsigned int iCell=0; iCell<NewInfoCells.size(); iCell++)
    {
        for(unsigned int jCell=0; jCell<NewInfoCells[iCell].NewDatas.size(); jCell++)
        {
            for(unsigned int ijVertex=0; ijVertex<NewInfoCells[iCell].NewDatas[jCell].Index_Vertices.size(); ijVertex++)
            {
                unsigned int vindex=NewInfoCells[iCell].NewDatas[jCell].Index_Vertices[ijVertex];
                map<unsigned int, unsigned int>::const_iterator itmapV=CurrentVerticesToMinIndex.find(vindex);
                if(itmapV!=CurrentVerticesToMinIndex.end())
                {
                    NewInfoCells[iCell].NewDatas[jCell].Index_Vertices[ijVertex]=itmapV->second;
                }

            }
        }
    }

   // cout<<"OriginalVertexToCurrentVertex.size()="<<OriginalVertexToCurrentVertex.size()<<endl;


    //Re-ReOrganize the indices of vertices
    Re_ReOrganize(NewInfoCells,VertexNum, OriginalVertexToCurrentVertex);
};
//-------------------------------
Mesh * Mesh::RestoreMesh_Globally(const vector<NewInformation> &NewInfoCells, unsigned int VertexNum)
{
	//-------------Clear initial Mesh's Indices of Its subcells--------------
    for (unsigned int i=0; i < P_Cells->size(); i++)
    {
        P_Cells->at(i).Index_ItsSubCells.clear();
    }

    Mesh * P_Mesh = new Mesh;
    P_Mesh->P_FrameTranMatrix = P_FrameTranMatrix;


    //FrameTranMatrix.clear();---should be updataed based on the original one
    //------------Re-Generated------------------------
    //For Vertices
    for (unsigned int i=0; i<VertexNum; i++)
    {
        Vertex V;
        P_Mesh->P_Vertices->push_back(V);
    }
    //For Cells
    unsigned int i_cell=0;
    for(unsigned int i=0; i < NewInfoCells.size(); i++)//For each original cell
    {
        for(unsigned int j=0; j < NewInfoCells[i].NewDatas.size(); j++)
        {

            //For each new cell: NewInfoCells[i].NewDatas[j]

            Cell C;

            //Cell::Index_InitialCell

            C.Index_InitialCell = P_Cells->at(NewInfoCells[i].cell_index).Index_InitialCell;//NewInfoCells[i].cell_index;

            //Cell::CellSize

            C.CellSize[0]=NewInfoCells[i].NewDatas[j].Position[0];

            C.CellSize[1]=NewInfoCells[i].NewDatas[j].Position[1];

            C.CellSize[2]=NewInfoCells[i].NewDatas[j].Position[2];

            C.CellSize[3]=NewInfoCells[i].NewDatas[j].Position[3];

            //Cell::Index_ItsCornerVertexSeq

            C.Index_ItsCornerVertices.clear();

            for(unsigned int i_temp=0; i_temp<NewInfoCells[i].NewDatas[j].Index_Vertices.size(); i_temp++)

            {

                C.Index_ItsCornerVertices.push_back(NewInfoCells[i].NewDatas[j].Index_Vertices[i_temp]);

            };

            //Vertex::Index_ItsVertices
            for(unsigned int i_temp=0; i_temp<4; i_temp++)
            {
                unsigned int k=C.Index_ItsCornerVertices[i_temp];
                unsigned int back_vertex_index;
                unsigned int front_vertex_index;
                if(i_temp==0)
                {
                    back_vertex_index=3;
                    front_vertex_index=i_temp+1;
                }
                else
                {
                    if(i_temp==3)
                    {
                        back_vertex_index=i_temp-1;
                        front_vertex_index=0;
                    }
                    else
                    {    
                        back_vertex_index=i_temp-1;
                        front_vertex_index=i_temp+1;
                    }
                }
                ///
                P_Mesh->P_Vertices->at(k).Index_ItsVertices.insert(C.Index_ItsCornerVertices[back_vertex_index]);
                P_Mesh->P_Vertices->at(k).Index_ItsVertices.insert(C.Index_ItsCornerVertices[front_vertex_index]);
            };

        for(unsigned int i_temp=0; i_temp<4; i_temp++)
        {
            //map<unsigned int, Coordinate> ItsCellsToCoordinatesMap;
            Coordinate V_Coor;
            switch(i_temp)
            {
                case 0:
                V_Coor.xy[0]=C.CellSize[0];
                V_Coor.xy[1]=C.CellSize[2];
                break;
                case 1:
                V_Coor.xy[0]=C.CellSize[1];
                V_Coor.xy[1]=C.CellSize[2];
                break;
                case 2:
                V_Coor.xy[0]=C.CellSize[1];
                V_Coor.xy[1]=C.CellSize[3];
                break;
                case 3:
                V_Coor.xy[0]=C.CellSize[0];
                V_Coor.xy[1]=C.CellSize[3];
                break;
            };

            P_Mesh->P_Vertices->at(C.Index_ItsCornerVertices[i_temp]).ItsCellsToCoordinatesMap.insert(pair<unsigned int, Coordinate>(i_cell, V_Coor));
    };
            C.ItsParent=NewInfoCells[i].cell_index;
            P_Mesh->P_Cells->push_back(C);
            P_Cells->at(NewInfoCells[i].cell_index).Index_ItsSubCells.insert(i_cell);

            i_cell = i_cell+1;
 };
    };
    //------------------------------------------------
    P_Mesh->Is_Interior_irregular_Vertex();
    P_Mesh->Initialization_Index_AdjacentCells();

    //-------------------------------------------------
    return P_Mesh;
};
//---------------------------------
Mesh *  Mesh::Subdivide_initialParametricMesh_Globally(unsigned int k, map<unsigned int, unsigned int> & OriginalVertexToCurrentVertex)//Each cell should be subdivided into k*k sub-cells
{
    OriginalVertexToCurrentVertex.clear();

    map<unsigned int, set<unsigned int> > OriginalVertexToCurrentVertices;

    vector<NewInformation> NewInfoCells;
    SubdivideCells(k, NewInfoCells, OriginalVertexToCurrentVertices);
	
//    cout<<"Mesh::Subdivide_initialParametricMesh_Globally:: After SubdivideCells():"<<endl;
//    for (unsigned int i=0; i<NewInfoCells.size(); i++)
//    {
//        cout<<"the "<<i<<" -th cell:"<<endl;
//        NewInfoCells[i].Cout_NewInformation();
//        cin.get();
//    }
//    cin.get();


    //Merge the same vertices based on the connecting relationship between Cells

    unsigned int VertexNum;

    ReOrganizeVerticesIndices_MergeTheSameVertices(NewInfoCells, k, VertexNum, OriginalVertexToCurrentVertices, OriginalVertexToCurrentVertex);

//    cout<<"Mesh::Subdivide_initialParametricMesh_Globally:: After ReOrganizeVerticesIndices_MergeTheSameVertices:"<<endl;
//    for (unsigned int i=0; i<NewInfoCells.size(); i++)
//    {
//        cout<<"the "<<i<<" -th cell:"<<endl;
//        NewInfoCells[i].Cout_NewInformation();
//        cin.get();
//    }
//    cin.get();


    //Restore the new mesh with the help of NewInfoCells

    Mesh * P_Mesh=RestoreMesh_Globally(NewInfoCells, VertexNum);

    return P_Mesh;

};
////===================================================
//static void Correspondence_Positive_Vertex(unsigned int low_index, unsigned int index,
//                                           unsigned int flag, unsigned int flag2,
//                                           vector<NewInformation> & NewInfoCells,
//                                           const map<unsigned int, vector<unsigned int> > &CellsIndexToEdgesIndex,
//                                           map<unsigned int, unsigned int> & OriginalVertexToCurrentVertex,
//                                           vector<vector<unsigned int> > &CurrentVerticesEdges,
//                                           map<unsigned int, pair<unsigned int, Coordinate*> > &CurrentVerticesIndexToCoordinate)
//{

//    vector<unsigned int> * P_low_index_verticesSeq = NULL;
//    switch(flag)
//    {
//    case 1://Up
//            //low_index_verticesSeq(NewInfoCells[low_index].Up_indexSeq);
//            P_low_index_verticesSeq = & NewInfoCells[low_index].Up_indexSeq;
//            break;
//    case 2://Down
//            //low_index_verticesSeq(NewInfoCells[low_index].Down_indexSeq);
//            P_low_index_verticesSeq = &NewInfoCells[low_index].Down_indexSeq;
//            break;
//    case 3://Left
//            //low_index_verticesSeq(NewInfoCells[low_index].Left_indexSeq);
//            P_low_index_verticesSeq = &NewInfoCells[low_index].Left_indexSeq;
//            break;
//    case 4://Right
//            //low_index_verticesSeq(NewInfoCells[low_index].Right_indexSeq);
//            P_low_index_verticesSeq= &NewInfoCells[low_index].Right_indexSeq;
//            break;
//    }


//    vector<unsigned int> * P_index_verticesSeq = NULL;
//    switch(flag2)
//    {
//    case 1://Up
//            //index_verticesSeq(NewInfoCells[index].Up_indexSeq);
//            P_index_verticesSeq = &NewInfoCells[index].Up_indexSeq;
//            break;
//    case 2://Down
//            //index_verticesSeq(NewInfoCells[index].Down_indexSeq);
//            P_index_verticesSeq = &NewInfoCells[index].Down_indexSeq;
//            break;
//    case 3://Left
//            //index_verticesSeq(NewInfoCells[index].Left_indexSeq);
//            P_index_verticesSeq = &NewInfoCells[index].Left_indexSeq;
//            break;
//    case 4://Right
//            //index_verticesSeq(NewInfoCells[index].Right_indexSeq);
//            P_index_verticesSeq = &NewInfoCells[index].Right_indexSeq;
//            break;
//    }
//    //-------
//    map<unsigned int, unsigned int> Correspondent;
//    for(unsigned int i=0; i<P_index_verticesSeq->size(); i++)
//    {
//         Correspondent.insert(pair<unsigned int, unsigned int>(P_index_verticesSeq->at(i), P_low_index_verticesSeq->at(i)));
//    };

//    //======================P_Cells->Index===============================================
//    for(unsigned int i=0; i<NewInfoCells[index].NewDatas.size(); i++)
//    {
//        for(unsigned int j=0; j<NewInfoCells[index].NewDatas[i].Index_Vertices.size(); j++)
//        {
//            unsigned int vindex = NewInfoCells[index].NewDatas[i].Index_Vertices[j];
//            map<unsigned int, unsigned int>::iterator it = Correspondent.find(vindex);
//            if(it!=Correspondent.end())
//            {
//                    NewInfoCells[index].NewDatas[i].Index_Vertices[j]=(*it).second;
//            };
//        };
//    };
//    //------------------------------------------------
//    //Up
//    for (unsigned int i=0; i<NewInfoCells[index].Up_indexSeq.size();i++)
//    {
//        unsigned int vindex = NewInfoCells[index].Up_indexSeq[i];
//        map<unsigned int, unsigned int>::iterator it=Correspondent.find(vindex);
//        if (it!=Correspondent.end())
//        {
//                NewInfoCells[index].Up_indexSeq[i]=(*it).second;
//        }
//    }
//    //Down
//    for (unsigned int i=0; i<NewInfoCells[index].Down_indexSeq.size();i++)
//    {
//        unsigned int vindex = NewInfoCells[index].Down_indexSeq[i];
//        map<unsigned int, unsigned int>::iterator it=Correspondent.find(vindex);
//        if (it!=Correspondent.end())
//        {
//                NewInfoCells[index].Down_indexSeq[i]=(*it).second;
//        }
//    }
//    //Left
//    for (unsigned int i=0; i<NewInfoCells[index].Left_indexSeq.size();i++)
//    {
//        unsigned int vindex = NewInfoCells[index].Left_indexSeq[i];
//        map<unsigned int, unsigned int>::iterator it=Correspondent.find(vindex);
//        if (it!=Correspondent.end())
//        {
//                NewInfoCells[index].Left_indexSeq[i]=(*it).second;
//        }
//    }
//    //Right
//    for (unsigned int i=0; i<NewInfoCells[index].Right_indexSeq.size();i++)
//    {
//        unsigned int vindex = NewInfoCells[index].Right_indexSeq[i];
//        map<unsigned int, unsigned int>::iterator it=Correspondent.find(vindex);
//        if (it!=Correspondent.end())
//        {
//                NewInfoCells[index].Right_indexSeq[i]=(*it).second;
//        }
//    }
//    //------------------------------------------------
//    //map<unsigned int, unsigned int> & OriginalVertexToCurrentVertex
//    for (map<unsigned int, unsigned int>::iterator itMap=OriginalVertexToCurrentVertex.begin(); itMap!=OriginalVertexToCurrentVertex.end(); itMap++)
//    {
//        map<unsigned int, unsigned int>::iterator it=Correspondent.find(itMap->second);
//        if (it!=Correspondent.end())
//        {
//                itMap->second=it->second;
//        }
//    }

//    //----------------------------------------------------
////const map<unsigned int, vector<unsigned int> > &CellsIndexToEdgesIndex,
//    map<unsigned int, vector<unsigned int> >::const_iterator itmap=CellsIndexToEdgesIndex.find(index) ;
//    if(itmap!=CellsIndexToEdgesIndex.end())
//    {
//    }
//};

//static void Correspondence_Nagetive_Vertex(unsigned int low_index, unsigned int index,
//                                           unsigned int flag, unsigned int flag2,
//                                           vector<NewInformation> & NewInfoCells,
//                                           const map<unsigned int, vector<unsigned int> > &CellsIndexToEdgesIndex,
//                                           map<unsigned int, unsigned int> & OriginalVertexToCurrentVertex,
//                                           vector<vector<unsigned int> > &CurrentVerticesEdges,
//                                           map<unsigned int, pair<unsigned int, Coordinate*> > &CurrentVerticesIndexToCoordinate)
//{
//    vector<unsigned int>* P_low_index_verticesSeq=NULL;

//    switch(flag)
//    {
//    case 1://Up
//            //low_index_verticesSeq(NewInfoCells[low_index].Up_indexSeq);
//            P_low_index_verticesSeq = &NewInfoCells[low_index].Up_indexSeq;
//            break;
//    case 2://Down
//            //low_index_verticesSeq(NewInfoCells[low_index].Down_indexSeq);
//            P_low_index_verticesSeq = &NewInfoCells[low_index].Down_indexSeq;
//            break;
//    case 3://Left
//            //low_index_verticesSeq(NewInfoCells[low_index].Left_indexSeq);
//            P_low_index_verticesSeq = &NewInfoCells[low_index].Left_indexSeq;
//            break;
//    case 4://Right
//            //low_index_verticesSeq(NewInfoCells[low_index].Right_indexSeq);
//            P_low_index_verticesSeq = &NewInfoCells[low_index].Right_indexSeq;
//            break;
//    }



//    vector<unsigned int>* P_index_verticesSeq;
//    switch(flag2)
//    {
//    case 1://Up
//            //index_verticesSeq(NewInfoCells[index].Up_indexSeq);
//            P_index_verticesSeq = &NewInfoCells[index].Up_indexSeq;
//            break;
//    case 2://Down
//            //index_verticesSeq(NewInfoCells[index].Down_indexSeq);
//            P_index_verticesSeq = &NewInfoCells[index].Down_indexSeq;
//            break;
//    case 3://Left
//            //index_verticesSeq(NewInfoCells[index].Left_indexSeq);
//            P_index_verticesSeq = &NewInfoCells[index].Left_indexSeq;
//            break;
//    case 4://Right
//            //index_verticesSeq(NewInfoCells[index].Right_indexSeq);
//            P_index_verticesSeq = &NewInfoCells[index].Right_indexSeq;
//            break;
//    }



//    map<unsigned int, unsigned int> Correspondent;
//    for(unsigned int i=0; i<P_index_verticesSeq->size(); i++)
//    {
//        Correspondent.insert(pair<unsigned int, unsigned int>(P_index_verticesSeq->at(i), P_low_index_verticesSeq->at(P_index_verticesSeq->size()-1-i)));
//    };

//    //Modify the indices in NewInfoCells[index]
//    for(unsigned int i=0; i<NewInfoCells[index].NewDatas.size(); i++)
//    {
//        for(unsigned int j=0; j<NewInfoCells[index].NewDatas[i].Index_Vertices.size(); j++)
//        {
//            unsigned int vindex = NewInfoCells[index].NewDatas[i].Index_Vertices[j];
//            map<unsigned int, unsigned int>::iterator it = Correspondent.find(vindex);
//            if(it!=Correspondent.end())
//            {
//                    //NewInfoCells[index].NewDatas[i].VerticesInCell[j]
//                    NewInfoCells[index].NewDatas[i].Index_Vertices[j]=(*it).second;//Map iterator!!!
//            };
//        };

//    };
//    //------------------------------------------------
//    //Up
//    for (unsigned int i=0; i<NewInfoCells[index].Up_indexSeq.size();i++)
//    {
//        unsigned int vindex = NewInfoCells[index].Up_indexSeq[i];
//        map<unsigned int, unsigned int>::iterator it=Correspondent.find(vindex);
//        if (it!=Correspondent.end())
//        {
//                NewInfoCells[index].Up_indexSeq[i]=(*it).second;
//        }
//    }
//    //Down
//    for (unsigned int i=0; i<NewInfoCells[index].Down_indexSeq.size();i++)
//    {
//        unsigned int vindex = NewInfoCells[index].Down_indexSeq[i];
//        map<unsigned int, unsigned int>::iterator it=Correspondent.find(vindex);
//        if (it!=Correspondent.end())
//        {
//                NewInfoCells[index].Down_indexSeq[i]=(*it).second;
//        }
//    }
//    //Left
//    for (unsigned int i=0; i<NewInfoCells[index].Left_indexSeq.size();i++)
//    {
//        unsigned int vindex = NewInfoCells[index].Left_indexSeq[i];
//        map<unsigned int, unsigned int>::iterator it=Correspondent.find(vindex);
//        if (it!=Correspondent.end())
//        {
//                NewInfoCells[index].Left_indexSeq[i]=(*it).second;
//        }
//    }
//    //Right
//    for (unsigned int i=0; i<NewInfoCells[index].Right_indexSeq.size();i++)
//    {
//        unsigned int vindex = NewInfoCells[index].Right_indexSeq[i];
//        map<unsigned int, unsigned int>::iterator it=Correspondent.find(vindex);
//        if (it!=Correspondent.end())
//        {
//                NewInfoCells[index].Right_indexSeq[i]=(*it).second;
//        }
//    }
//    //------------------------------------------------
//    //map<unsigned int, unsigned int> & OriginalVertexToCurrentVertex
//    for (map<unsigned int, unsigned int>::iterator itMap=OriginalVertexToCurrentVertex.begin(); itMap!=OriginalVertexToCurrentVertex.end(); itMap++)
//    {
//        map<unsigned int, unsigned int>::iterator it=Correspondent.find(itMap->second);
//        if (it!=Correspondent.end())
//        {
//                itMap->second=it->second;
//        }
//    }
//};
//static void DealWithNewInfoCells_Vertex
//            (unsigned int low_index, unsigned int index, unsigned int flag, unsigned int flag2,
//            vector<NewInformation> & NewInfoCells, const map<unsigned int, vector<unsigned int> > &CellsIndexToEdgesIndex,
//             map<unsigned int, unsigned int> & OriginalVertexToCurrentVertex,
//             vector<vector<unsigned int> > &CurrentVerticesEdges,
//             map<unsigned int, pair<unsigned int, Coordinate*> > &CurrentVerticesIndexToCoordinate)//
//{//Here, we suppose that all the local frames of cells are right-handed.
// //-------------------------
//    if ((flag+flag2)%2)//odd
//    {
//        Correspondence_Positive_Vertex(low_index, index, flag, flag2, NewInfoCells,
//                                       CellsIndexToEdgesIndex, OriginalVertexToCurrentVertex,
//                                       CurrentVerticesEdges, CurrentVerticesIndexToCoordinate);
//    }
//    else
//    {
//        Correspondence_Nagetive_Vertex(low_index, index, flag, flag2, NewInfoCells,
//                                       CellsIndexToEdgesIndex, OriginalVertexToCurrentVertex,
//                                       CurrentVerticesEdges, CurrentVerticesIndexToCoordinate);
//    }

//}
//void Mesh::MergeVertices_Vertex(unsigned int low_index, unsigned int index,
//                                vector<NewInformation> &NewInfoCells, unsigned int flag, unsigned int kk,
//                                 const map<unsigned int, vector<unsigned int> > &CellsIndexToEdgesIndex,
//                                map<unsigned int, unsigned int> & OriginalVertexToCurrentVertex,
//                                vector<vector<unsigned int> > &CurrentVerticesEdges,
//                                map<unsigned int, pair<unsigned int, Coordinate*> > &CurrentVerticesIndexToCoordinate)
//{
//    unsigned int flag2;
//    bool key=true;
//    //When for each vertex of the Initial Mesh, the 1-neighborhood of this vertex is homeomorphic to B(1)\subset R^2,
//    //The Cells[index]'s relative position for Cells[low_index] is one and only one of "up, down, left, right".
//    //--

//    unsigned int It;
//    if(P_Cells->at(index).Index_AdjacentCell_up.size())
//    {
//        It= *(P_Cells->at(index).Index_AdjacentCell_up.begin());
//        if(It==low_index)
//        {
//            flag2=1;//"up";
//            key=false;
//        };
//    };


//        //--
//    if((key) && (P_Cells->at(index).Index_AdjacentCell_down.size()))//if "low_index" doesn't find
//        {
//        It=*(P_Cells->at(index).Index_AdjacentCell_down.begin());
//                if(It==low_index)
//                {
//                        flag2=2;
//                        key=false;
//                };
//        };
//        //--
//    if(key && (P_Cells->at(index).Index_AdjacentCell_left.size()))//if "low_index" doesn't find
//        {
//        It=*(P_Cells->at(index).Index_AdjacentCell_left.begin());
//                if(It==low_index)
//                {
//                        flag2=3;//"left";
//                        key=false;
//                };

//        };
//        //--
//    if(key && (P_Cells->at(index).Index_AdjacentCell_right.size()))//if "low_index" doesn't find
//        {
//        It=*(P_Cells->at(index).Index_AdjacentCell_right.begin());
//                if(It==low_index)
//                {
//                        flag2=4;//"right";
//                        key=false;
//                };
//        };
//        //---
//        if(key)
//        {
//                cout<<"Mesh::ReOrganizeVerticesIndices_MergeTheSameVertices:: MergeVertices:: Adjacent cells are not adjacent????"<<endl;
//                system("PAUSE");
//        };


//        DealWithNewInfoCells_Vertex(low_index, index, flag, flag2, NewInfoCells, CellsIndexToEdgesIndex, OriginalVertexToCurrentVertex,
//                                    CurrentVerticesEdges,CurrentVerticesIndexToCoordinate);

//};

//void Mesh::ReOrganizeVerticesIndices_MergeTheSameVertices_Vertex
//          (vector<NewInformation> &NewInfoCells, unsigned int k, unsigned int &VertexNum,
//          const map<unsigned int, vector<unsigned int> > &CellsIndexToEdgesIndex,
//          map<unsigned int, unsigned int> & OriginalVertexToCurrentVertex,
//           vector<vector<unsigned int> > &CurrentVerticesEdges,
//           map<unsigned int, pair<unsigned int, Coordinate*> > &CurrentVerticesIndexToCoordinate)
//{

//    for (unsigned int cell_index=0; cell_index < P_Cells->size(); cell_index++)
//        {
//            //For NewInfoCells[cell_index], Merge The Same Vertices in adjacent cells with Cells[cell_index]
//            //Cells[cell_index].Index_AdjacentCells_up
//            unsigned int flag, It;

//            if(P_Cells->at(cell_index).Index_AdjacentCell_up.size())
//            {
//                It=*(P_Cells->at(cell_index).Index_AdjacentCell_up.begin());
//                if(It>cell_index)
//                {
//                    flag=1;//--->up
//                    MergeVertices_Vertex(cell_index, It, NewInfoCells, flag, k, CellsIndexToEdgesIndex, OriginalVertexToCurrentVertex, CurrentVerticesEdges, CurrentVerticesIndexToCoordinate);
//                };
//            };




//            //------
//            //Cells[cell_index].Index_AdjacentCells_down
//            if(P_Cells->at(cell_index).Index_AdjacentCell_down.size())
//            {
//                It=*(P_Cells->at(cell_index).Index_AdjacentCell_down.begin());
//                if(It>cell_index)
//                {
//                    flag=2;//--->down
//                    MergeVertices_Vertex(cell_index, It, NewInfoCells, flag, k, CellsIndexToEdgesIndex, OriginalVertexToCurrentVertex, CurrentVerticesEdges, CurrentVerticesIndexToCoordinate);
//                };
//            };

//            //------
//            //Cells[cell_index].Index_AdjacentCells_left
//            if(P_Cells->at(cell_index).Index_AdjacentCell_left.size())
//            {
//                It=*(P_Cells->at(cell_index).Index_AdjacentCell_left.begin());
//                if(It>cell_index)
//                {
//                    flag=3;//--->left
//                    MergeVertices_Vertex(cell_index, It, NewInfoCells, flag, k, CellsIndexToEdgesIndex, OriginalVertexToCurrentVertex, CurrentVerticesEdges, CurrentVerticesIndexToCoordinate);
//                };
//            };


//            //------
//            //Cells[cell_index].Index_AdjacentCells_right
//            if(P_Cells->at(cell_index).Index_AdjacentCell_right.size())
//            {
//                It=*(P_Cells->at(cell_index).Index_AdjacentCell_right.begin());
//                if(It>cell_index)
//                {
//                    flag=4;//--->right
//                    MergeVertices_Vertex(cell_index, It, NewInfoCells, flag, k, CellsIndexToEdgesIndex, OriginalVertexToCurrentVertex, CurrentVerticesEdges, CurrentVerticesIndexToCoordinate);
//                };
//            };


//            }

//            //-----------------------------
//            //Re-ReOrganize the indices of vertices
//            Re_ReOrganize(NewInfoCells,VertexNum, OriginalVertexToCurrentVertex);
//};


//unsigned int IndexEdge(const vector<unsigned int> &Index_ItsCornerVertices, unsigned int Vindex1, unsigned int Vindex2, bool Is_InOrder)
//{
//    unsigned int IndexE=5;
//    unsigned int positionV1=5, positionV2=5;
//    Is_InOrder=true;

//    if(Index_ItsCornerVertices.size()==4)//That is normal
//    {
//        for(unsigned int i=0; i<Index_ItsCornerVertices.size(); i++)
//        {
//            if(Vindex1==Index_ItsCornerVertices[i])
//            {
//                positionV1=i;
//            }
//            if(Vindex2==Index_ItsCornerVertices[i])
//            {
//                positionV2=i;
//            }
//        }
//        //-----
//        if(positionV1==5||positionV2==5)
//        {
//            cout<<"Mesh:: IndexEdge: positionV1 or positionV2 > 3"<<endl;
//            cin.get();
//        }
//        else
//        {
//            if((positionV1==0 && positionV2==1)||(positionV1==1 && positionV2==0))
//            {
//                IndexE=0;//the 0-th edge
//                if(positionV1==1 && positionV2==0)
//                {
//                    Is_InOrder=false;//the default of Is_InOrder=true
//                };
//            }
//            else
//            {
//                if((positionV1==1 && positionV2==2)||(positionV1==2 && positionV2==1))
//                {
//                    IndexE=1;//the 1st edge
//                    if(positionV1==2 && positionV2==1)
//                    {
//                        Is_InOrder=false;//the default of Is_InOrder=true
//                    }
//                }
//                else
//                {
//                    if((positionV1==2 && positionV2==3)||(positionV1==3 && positionV2==2))
//                    {
//                        IndexE=2;//the 2nd edge
//                        if(positionV1==2 && positionV2==3)
//                        {
//                            Is_InOrder=false;//the default of Is_InOrder=true
//                        }
//                    }
//                    else
//                    {
//                        if((positionV1==0 && positionV2==3)||(positionV1==3 && positionV2==0))
//                        {
//                            IndexE=3;//the 3rd edge
//                            if(positionV1==3 && positionV2==0)
//                            {
//                                Is_InOrder=false;//the default of Is_InOrder=true
//                            }
//                        }
//                        else
//                        {
//                            cout<<"Mesh::IndexEdge: there is an error of positionV1 and positionV2 "<<endl;
//                            cin.get();
//                        }
//                    }
//                }


//            }
//        }

//    }
//    else
//    {
//        cout<<"Mesh:: IndexEdge: Index_ItsCornerVertices.size()!=4"<<endl;
//    }

//    return IndexE;
//}


//void Mesh::subdivide_OneCell_Vertex(unsigned int cell_index, unsigned int k, NewInformation& NewInfo,
//                                    map<unsigned int, unsigned int> &OriginalVertexToCurrentVertex,
//                                    const vector<pair<unsigned int, unsigned int> > &InitialEdges,
//                                    const map<unsigned int, vector<unsigned int> > &CellsIndexToEdgesIndex,
//                                    vector< vector<unsigned int> > &CurrentVerticesEdges,
//                                    map<unsigned int, pair<unsigned int, Coordinate*> > &CurrentVerticesIndexToCoordinate)
//{
//    //
//    map<unsigned int, set<unsigned int> >::const_iterator itmap=CellsIndexToEdgesIndex.find(cell_index);
//    if(itmap==CellsIndexToEdgesIndex.end())//Do not find
//    {
//        //Subdivde Cells[cell_index]---
//        NewInfo.NewDatas.clear();
//        NewInfo.Up_indexSeq.clear();
//        NewInfo.Down_indexSeq.clear();
//        NewInfo.Left_indexSeq.clear();
//        NewInfo.Right_indexSeq.clear();

//        //--------
//        NewInfo.cell_index=cell_index;
//        //--------
//        double s0=P_Cells->at(cell_index).CellSize[0];
//        double s1=P_Cells->at(cell_index).CellSize[1];
//        double t0=P_Cells->at(cell_index).CellSize[2];
//        double t1=P_Cells->at(cell_index).CellSize[3];
//        //--------
//        double ds=(s1-s0)/(k+1);
//        double dt=(t1-t0)/(k+1);
//        //--NewDatas
//        for(unsigned int i_s=0; i_s<k+1; i_s++)
//        {
//            for(unsigned int i_t=0; i_t<k+1; i_t++)
//            {
//                NewMeshData_Globally NewData;
//                //NewData.Index_Vertices
//                unsigned int i0= ComputerIndex(i_s, i_t, k, k)+(k+2)*(k+2)*cell_index;
//                NewData.Index_Vertices.push_back(i0);
//                //--Position
//                double x0=s0+i_s*ds;
//                double y0=t0+i_t*dt;
//                //****Insert part*****
//                Coordinate * PCoor=new Coordinate;
//                PCoor->xy[0]=x0;
//                PCoor->xy[1]=y0;
//                pair<unsigned int, Coordinate *> SupCoor;
//                SupCoor.first=cell_index;
//                SupCoor.second->xy[0]=x0;
//                SupCoor.second->xy[1]=y0;
//                CurrentVerticesIndexToCoordinate.insert(pair<unsigned int, pair<unsigned int, Coordinate *> >(i0, SupCoor));
//                //********************
//                //----------
//                i0=ComputerIndex(i_s+1, i_t, k, k)+(k+2)*(k+2)*cell_index;
//                NewData.Index_Vertices.push_back(i0);
//                i0=ComputerIndex(i_s+1, i_t+1, k, k)+(k+2)*(k+2)*cell_index;
//                NewData.Index_Vertices.push_back(i0);
//                //--Position
//                double x1=s0+(i_s+1)*ds;
//                double y1=t0+(i_t+1)*dt;
//                //----------
//                i0=ComputerIndex(i_s, i_t+1, k, k)+(k+2)*(k+2)*cell_index;
//                NewData.Index_Vertices.push_back(i0);
//                //NewData.Position
//                NewData.Position.push_back(x0);
//                NewData.Position.push_back(x1);
//                NewData.Position.push_back(y0);
//                NewData.Position.push_back(y1);
//                //NewDatas.push_back NewData
//                NewInfo.NewDatas.push_back(NewData);
//            };
//        };
//        //*****Insert part*********
//        unsigned int i_s=k+1;
//        for(unsigned int i_t=0; i_t<k+2; i_t++)//i_s=k+1
//        {
//            unsigned int i0= ComputerIndex(i_s, i_t, k, k)+(k+2)*(k+2)*cell_index;
//            //--Position
//            double x0=s0+i_s*ds;
//            double y0=t0+i_t*dt;
//            //--------------------------------
//            Coordinate * PCoor=new Coordinate;
//            PCoor->xy[0]=x0;
//            PCoor->xy[1]=y0;
//            pair<unsigned int, Coordinate *> SupCoor;
//            SupCoor.first=cell_index;
//            SupCoor.second->xy[0]=x0;
//            SupCoor.second->xy[1]=y0;
//            CurrentVerticesIndexToCoordinate.insert(pair<unsigned int, pair<unsigned int, Coordinate *> >(i0, SupCoor));
//        }
//        //*************************


//        //***********************indexSeq*************************************
//        for (unsigned int i=0; i<k+2; i++)
//        {
//            //---NewInfo.Up_indexSeq
//            NewInfo.Up_indexSeq.push_back((k+2)*(i+1)-1+(k+2)*(k+2)*cell_index);
//            //---NewInfo.Down_indexSeq
//            NewInfo.Down_indexSeq.push_back((k+2)*i+(k+2)*(k+2)*cell_index);
//            //---NewInfo.Left_indexSeq
//            NewInfo.Left_indexSeq.push_back(i+(k+2)*(k+2)*cell_index);
//            //---NewInfo.Right_indexSeq
//            NewInfo.Right_indexSeq.push_back(i+(k+1)*(k+2)+(k+2)*(k+2)*cell_index);
//        }
//        //********************************************************************

//            //Initial OriginalVertexToCurrentVertex:
//        for (unsigned int j=0; j<P_Cells->at(cell_index).Index_ItsCornerVertices.size(); j++)
//        {
//            map<unsigned int, unsigned int>::iterator it=OriginalVertexToCurrentVertex.find(P_Cells->at(cell_index).Index_ItsCornerVertices[j]);
//            if (it==OriginalVertexToCurrentVertex.end())
//            {
//                switch(j)
//                {
//                case 0:
//                    OriginalVertexToCurrentVertex.insert(pair<unsigned int, unsigned int>(P_Cells->at(cell_index).Index_ItsCornerVertices[0], cell_index*(k+2)*(k+2)));
//                break;
//                case 1:
//                    OriginalVertexToCurrentVertex.insert(pair<unsigned int, unsigned int>(P_Cells->at(cell_index).Index_ItsCornerVertices[1], cell_index*(k+2)*(k+2)+(k+1)*(k+2)));
//                break;
//                case 2:
//                    OriginalVertexToCurrentVertex.insert(pair<unsigned int, unsigned int>(P_Cells->at(cell_index).Index_ItsCornerVertices[2], (cell_index+1)*(k+2)*(k+2)-1));
//                break;
//                case 3:
//                    OriginalVertexToCurrentVertex.insert(pair<unsigned int, unsigned int>(P_Cells->at(cell_index).Index_ItsCornerVertices[3], cell_index*(k+2)*(k+2)+(k+1)));
//                break;
//                }
//            }
//        }
//    }
//    else//find
//    {
//        //Subdivde Cells[cell_index]---
//        NewInfo.NewDatas.clear();
//        NewInfo.Up_indexSeq.clear();
//        NewInfo.Down_indexSeq.clear();
//        NewInfo.Left_indexSeq.clear();
//        NewInfo.Right_indexSeq.clear();
//        //--------
//        NewInfo.cell_index=cell_index;
//        //--------
//        double s0=P_Cells->at(cell_index).CellSize[0];
//        double s1=P_Cells->at(cell_index).CellSize[1];
//        double t0=P_Cells->at(cell_index).CellSize[2];
//        double t1=P_Cells->at(cell_index).CellSize[3];
//        //--------
//        double ds=(s1-s0)/(k+1);
//        double dt=(t1-t0)/(k+1);
//        //--NewDatas
//        for(unsigned int i_s=0; i_s<k+1; i_s++)
//        {
//            for(unsigned int i_t=0; i_t<k+1; i_t++)
//            {
//                NewMeshData_Globally NewData;
//                //NewData.Index_Vertices
//                unsigned int i0= ComputerIndex(i_s, i_t, k, k)+(k+2)*(k+2)*cell_index;
//                NewData.Index_Vertices.push_back(i0);
//                //--Position
//                double x0=s0+i_s*ds;
//                double y0=t0+i_t*dt;
//                //****Insert part*****
//                Coordinate * PCoor=new Coordinate;
//                PCoor->xy[0]=x0;
//                PCoor->xy[1]=y0;
//                pair<unsigned int, Coordinate *> SupCoor;
//                SupCoor.first=cell_index;
//                SupCoor.second->xy[0]=x0;
//                SupCoor.second->xy[1]=y0;
//                CurrentVerticesIndexToCoordinate.insert(pair<unsigned int, pair<unsigned int, Coordinate *> >(i0, SupCoor));
//                //********************
//                //----------
//                i0=ComputerIndex(i_s+1, i_t, k, k)+(k+2)*(k+2)*cell_index;
//                NewData.Index_Vertices.push_back(i0);
//                i0=ComputerIndex(i_s+1, i_t+1, k, k)+(k+2)*(k+2)*cell_index;
//                NewData.Index_Vertices.push_back(i0);
//                //--Position
//                double x1=s0+(i_s+1)*ds;
//                double y1=t0+(i_t+1)*dt;
//                //----------
//                i0=ComputerIndex(i_s, i_t+1, k, k)+(k+2)*(k+2)*cell_index;
//                NewData.Index_Vertices.push_back(i0);
//                //NewData.Position
//                NewData.Position.push_back(x0);
//                NewData.Position.push_back(x1);
//                NewData.Position.push_back(y0);
//                NewData.Position.push_back(y1);
//                //NewDatas.push_back NewData
//                NewInfo.NewDatas.push_back(NewData);
//            };
//        };
//        //*****Insert part*********
//        unsigned int i_s=k+1;
//        for(unsigned int i_t=0; i_t<k+2; i_t++)//i_s=k+1
//        {
//            unsigned int i0= ComputerIndex(i_s, i_t, k, k)+(k+2)*(k+2)*cell_index;
//            //--Position
//            double x0=s0+i_s*ds;
//            double y0=t0+i_t*dt;
//            //--------------------------------
//            Coordinate * PCoor=new Coordinate;
//            PCoor->xy[0]=x0;
//            PCoor->xy[1]=y0;
//            pair<unsigned int, Coordinate *> SupCoor;
//            SupCoor.first=cell_index;
//            SupCoor.second->xy[0]=x0;
//            SupCoor.second->xy[1]=y0;
//            CurrentVerticesIndexToCoordinate.insert(pair<unsigned int, pair<unsigned int, Coordinate *> >(i0, SupCoor));
//        }
//        //*************************


//        //***********************indexSeq*************************************
//        for (unsigned int i=0; i<k+2; i++)
//        {
//            //---NewInfo.Up_indexSeq
//            NewInfo.Up_indexSeq.push_back((k+2)*(i+1)-1+(k+2)*(k+2)*cell_index);
//            //---NewInfo.Down_indexSeq
//            NewInfo.Down_indexSeq.push_back((k+2)*i+(k+2)*(k+2)*cell_index);
//            //---NewInfo.Left_indexSeq
//            NewInfo.Left_indexSeq.push_back(i+(k+2)*(k+2)*cell_index);
//            //---NewInfo.Right_indexSeq
//            NewInfo.Right_indexSeq.push_back(i+(k+1)*(k+2)+(k+2)*(k+2)*cell_index);
//        }
//        //********************************************************************

//        //************************Insert Part***************************
//        for(unsigned int i=0; i<((*itmap).second).size(); i++)
//        {
//            unsigned int EdgeIndex=((*itmap).second)[i];
//            unsigned int Vindex1=InitialEdges[EdgeIndex].first;
//            unsigned int Vindex2=InitialEdges[EdgeIndex].second;

//            bool Is_InOrder;
//            unsigned int IndexE=IndexEdge(P_Cells->at(cell_index).Index_ItsCornerVertices, Vindex1, Vindex2, Is_InOrder);
//            switch(IndexE)
//            {
//                case 0://NewInfo.Down_indexSeq
//                if(Is_InOrder)
//                {//CurrentVerticesEdges[EdgeIndex]
//                    CurrentVerticesEdges[EdgeIndex].clear();
//                    for(unsigned int j=0; j<NewInfo.Down_indexSeq.size(); j++)
//                    {
//                        CurrentVerticesEdges[EdgeIndex].push_back(NewInfo.Down_indexSeq[j]);
//                    };
//                }
//                else
//                {
//                    CurrentVerticesEdges[EdgeIndex].clear();
//                    for(unsigned int j=0; j<NewInfo.Down_indexSeq.size(); j++)
//                    {
//                        CurrentVerticesEdges[EdgeIndex].push_back(NewInfo.Down_indexSeq[NewInfo.Down_indexSeq.size()-1-j]);
//                    };
//                }
//                    break;
//                case 1://NewInfo.Right_indexSeq
//                if(Is_InOrder)
//                {//CurrentVerticesEdges[EdgeIndex]
//                    CurrentVerticesEdges[EdgeIndex].clear();
//                    for(unsigned int j=0; j<NewInfo.Right_indexSeq.size(); j++)
//                    {
//                        CurrentVerticesEdges[EdgeIndex].push_back(NewInfo.Right_indexSeq[j]);
//                    };
//                }
//                else
//                {
//                    CurrentVerticesEdges[EdgeIndex].clear();
//                    for(unsigned int j=0; j<NewInfo.Right_indexSeq.size(); j++)
//                    {
//                        CurrentVerticesEdges[EdgeIndex].push_back(NewInfo.Right_indexSeq[NewInfo.Right_indexSeq.size()-1-j]);
//                    };
//                }
//                    break;
//                case 2://NewInfo.Up_indexSeq
//                if(Is_InOrder)
//                {//CurrentVerticesEdges[EdgeIndex]
//                    CurrentVerticesEdges[EdgeIndex].clear();
//                    for(unsigned int j=0; j<NewInfo.Up_indexSeq.size(); j++)
//                    {
//                        CurrentVerticesEdges[EdgeIndex].push_back(NewInfo.Up_indexSeq[j]);
//                    };
//                }
//                else
//                {
//                    CurrentVerticesEdges[EdgeIndex].clear();
//                    for(unsigned int j=0; j<NewInfo.Up_indexSeq.size(); j++)
//                    {
//                        CurrentVerticesEdges[EdgeIndex].push_back(NewInfo.Up_indexSeq[NewInfo.Up_indexSeq.size()-1-j]);
//                    };
//                }
//                    break;
//                case 3://NewInfo.Left_indexSeq
//                if(Is_InOrder)
//                {//CurrentVerticesEdges[EdgeIndex]
//                    CurrentVerticesEdges[EdgeIndex].clear();
//                    for(unsigned int j=0; j<NewInfo.Left_indexSeq.size(); j++)
//                    {
//                        CurrentVerticesEdges[EdgeIndex].push_back(NewInfo.Left_indexSeq[j]);
//                    };
//                }
//                else
//                {
//                    CurrentVerticesEdges[EdgeIndex].clear();
//                    for(unsigned int j=0; j<NewInfo.Left_indexSeq.size(); j++)
//                    {
//                        CurrentVerticesEdges[EdgeIndex].push_back(NewInfo.Left_indexSeq[NewInfo.Left_indexSeq.size()-1-j]);
//                    };
//                }
//                    break;
//                default:
//                    cout<<"class Mesh:: subdivide_OneCell_Vertex:: IndexEdge is not in {0,1,2,3}"<<endl;
//                    cin.get();
//                    break;
//            }
//        }
//        //**************************************************************

//            //Initial OriginalVertexToCurrentVertex:
//        for (unsigned int j=0; j<P_Cells->at(cell_index).Index_ItsCornerVertices.size(); j++)
//        {
//            map<unsigned int, unsigned int>::iterator it=OriginalVertexToCurrentVertex.find(P_Cells->at(cell_index).Index_ItsCornerVertices[j]);
//            if (it==OriginalVertexToCurrentVertex.end())
//            {
//                switch(j)
//                {
//                case 0:
//                    OriginalVertexToCurrentVertex.insert(pair<unsigned int, unsigned int>(P_Cells->at(cell_index).Index_ItsCornerVertices[0], cell_index*(k+2)*(k+2)));
//                break;
//                case 1:
//                    OriginalVertexToCurrentVertex.insert(pair<unsigned int, unsigned int>(P_Cells->at(cell_index).Index_ItsCornerVertices[1], cell_index*(k+2)*(k+2)+(k+1)*(k+2)));
//                break;
//                case 2:
//                    OriginalVertexToCurrentVertex.insert(pair<unsigned int, unsigned int>(P_Cells->at(cell_index).Index_ItsCornerVertices[2], (cell_index+1)*(k+2)*(k+2)-1));
//                break;
//                case 3:
//                    OriginalVertexToCurrentVertex.insert(pair<unsigned int, unsigned int>(P_Cells->at(cell_index).Index_ItsCornerVertices[3], cell_index*(k+2)*(k+2)+(k+1)));
//                break;
//                }
//            }
//        }
//    }


//};

//void Mesh::SubdivideCells_Vertex(unsigned int k, vector<NewInformation>& NewInfoCells,
//                                 const vector<pair<unsigned int, unsigned int> > &InitialEdges,
//                                 const map<unsigned int, vector<unsigned int> > &CellsIndexToEdgesIndex,
//                                 vector<vector<unsigned int> > &CurrentVerticesEdges,
//                                 map<unsigned int, unsigned int> &OriginalVertexToCurrentVertex,
//                                 map<unsigned int, Coordinate*> &CurrentVerticesIndexToCoordinate)
//{

//    NewInfoCells.clear();
//    CurrentVerticesEdges.clear();
//    CurrentVerticesEdges.resize(InitialEdges.size());
//    OriginalVertexToCurrentVertex.clear();
//    CurrentVerticesIndexToCoordinate.clear();
////    //-------------
////    map<unsigned int, vector<unsigned int> > CellsIndexToEdgesIndex;
////    for(unsigned int i=0; i<EdgesIndexofCells.size(); i++)
////    {
////        map<unsigned int, vector<unsigned int> >::const_iterator itmap=CellsIndexToEdgesIndex.find(EdgesIndexCells[i]);
////        if(itmap==CellsIndexToEdgesIndex.end())//Do not find
////        {
////            vector<unsigned int> EdgesIndex;
////            EdgesIndex.push_back(i);
////            CellsIndexToEdgesIndex.insert(pair<unsigned int, vector<unsigned int> >(EdgesIndexofCells[i], EdgesIndex));
////        }
////        else//Have found
////        {
////            ((*itmap).second).push_back(i);
////        }
////    }
//    //-------------
//    //Subdivide all the cells
//    for(unsigned int i_cell = 0; i_cell < P_Cells->size(); i_cell ++)
//    {
//        NewInformation NewInfo;
//        subdivide_OneCell_Vertex(i_cell, k, NewInfo, OriginalVertexToCurrentVertex, InitialEdges,CellsIndexToEdgesIndex,CurrentVerticesEdges,CurrentVerticesIndexToCoordinate);
//        NewInfoCells.push_back(NewInfo);
//    };


//};

///* InPut: unsigned int k => the times of subdivision
//        const vector<pair<unsigned int, unsigned int> > InitialEdges => the focused edges on the original mesh
//        const vector<unsigned int> EdgesIndexofCells => the indices of cells of these focused edges of the original mesh
//   OutPut: map<unsigned int, unsigned int> & OriginalVertexToCurrentVertex => a map from the indices of original vertice to
//                                                                                   the indices on the refinement mesh
//           vector<vector<unsigned int> > &CurrentVerticesEdges => CurrentVerticesEdges[i] is the indices of vertices which are on
//                                                                    the edge InitialEdges[i]
//           map<unsigned int, Coordinate*> &CurrentVerticesIndexToCoordinate => a map from the index of vertex of the new mesh
//                                                                               to one of its Coordinates on the original mesh
//*/
//Mesh * Mesh::Subdivide_initialParametricMesh_Globally_Vertex
//                        (unsigned int k,
//                        const vector<pair<unsigned int, unsigned int> > &InitialEdges,
//                        const vector<unsigned int> &EdgesIndexofCells,
//                        map<unsigned int, unsigned int> & OriginalVertexToCurrentVertex,
//                        vector<vector<unsigned int> > &CurrentVerticesEdges,
//                        map<unsigned int, pair<unsigned int, Coordinate*> > &CurrentVerticesIndexToCoordinate)
//{

//        OriginalVertexToCurrentVertex.clear();

//        //-------------
//        map<unsigned int, vector<unsigned int> > CellsIndexToEdgesIndex;
//        for(unsigned int i=0; i<EdgesIndexofCells.size(); i++)
//        {
//            map<unsigned int, vector<unsigned int> >::const_iterator itmap=CellsIndexToEdgesIndex.find(EdgesIndexCells[i]);
//            if(itmap==CellsIndexToEdgesIndex.end())//Do not find
//            {
//                vector<unsigned int> EdgesIndex;
//                EdgesIndex.push_back(i);
//                CellsIndexToEdgesIndex.insert(pair<unsigned int, vector<unsigned int> >(EdgesIndexofCells[i], EdgesIndex));
//            }
//            else//Have found
//            {
//                ((*itmap).second).push_back(i);
//            }
//        }

//        vector<NewInformation> NewInfoCells;

//        SubdivideCells_Vertex(k, NewInfoCells, InitialEdges, CellsIndexToEdgesIndex, CurrentVerticesEdges, OriginalVertexToCurrentVertex, CurrentVerticesIndexToCoordinate);


//        //Merge the same vertices based on the connecting relationship in Cells
//        unsigned int VertexNum;
//        ReOrganizeVerticesIndices_MergeTheSameVertices_Vertex(NewInfoCells, k, VertexNum, CellsIndexToEdgesIndex, OriginalVertexToCurrentVertex, CurrentVerticesEdges, CurrentVerticesIndexToCoordinate);

//        //Restore the new mesh with the help of NewInfoCells
//        Mesh * P_Mesh=RestoreMesh_Globally(NewInfoCells, VertexNum);
//        return P_Mesh;
//}
//============================================================================================================
unsigned int IndexEdge(const vector<unsigned int> &Index_ItsCornerVertices, unsigned int Vindex1, unsigned int Vindex2, bool &Is_InOrder)
{
    unsigned int IndexE=5;
    unsigned int positionV1=5, positionV2=5;
    Is_InOrder=true;

    if(Index_ItsCornerVertices.size()==4)//That is normal
    {
        for(unsigned int i=0; i<Index_ItsCornerVertices.size(); i++)
        {
            if(Vindex1==Index_ItsCornerVertices[i])
            {
                positionV1=i;
            }
            if(Vindex2==Index_ItsCornerVertices[i])
            {
                positionV2=i;
            }
        }
        //-----
        if(positionV1==5||positionV2==5)
        {
            cout<<"Mesh:: IndexEdge: positionV1 or positionV2 > 3"<<endl;
            cin.get();
        }
        else
        {
            if((positionV1==0 && positionV2==1)||(positionV1==1 && positionV2==0))
            {
                IndexE=0;//the 0-th edge
                if(positionV1==1 && positionV2==0)
                {
                    Is_InOrder=false;//the default of Is_InOrder=true
                };
            }
            else
            {
                if((positionV1==1 && positionV2==2)||(positionV1==2 && positionV2==1))
                {
                    IndexE=1;//the 1st edge
                    if(positionV1==2 && positionV2==1)
                    {
                        Is_InOrder=false;//the default of Is_InOrder=true
                    }
                }
                else
                {
                    if((positionV1==2 && positionV2==3)||(positionV1==3 && positionV2==2))
                    {
                        IndexE=2;//the 2nd edge
                        if(positionV1==2 && positionV2==3)
                        {
                            Is_InOrder=false;//the default of Is_InOrder=true
                        }
                    }
                    else
                    {
                        if((positionV1==0 && positionV2==3)||(positionV1==3 && positionV2==0))
                        {
                            IndexE=3;//the 3rd edge
                            if(positionV1==3 && positionV2==0)
                            {
                                Is_InOrder=false;//the default of Is_InOrder=true
                            }
                        }
                        else
                        {
                            cout<<"Mesh::IndexEdge: there is an error of positionV1 and positionV2 "<<endl;
                            cin.get();
                        }
                    }
                }

            }
        }

    }
    else
    {
        cout<<"Mesh:: IndexEdge: Index_ItsCornerVertices.size()!=4"<<endl;
    }

    return IndexE;
}
void Mesh::ConstructCurrentVerticesEdges(const vector<NewInformation> &NewInfoCells,
                                         const vector<pair<unsigned int, unsigned int> > &InitialEdges,
                                         const map<unsigned int, vector<unsigned int> > &CellsIndexToEdgesIndex,
                                         vector<vector<unsigned int> > &CurrentVerticesEdges)
{
    CurrentVerticesEdges.clear();
    vector<unsigned int> EmptyVerticesSeq;
    CurrentVerticesEdges.resize(InitialEdges.size(), EmptyVerticesSeq);


    for(unsigned int i_cell = 0; i_cell < NewInfoCells.size(); i_cell ++)
    {
        map<unsigned int, vector<unsigned int> >::const_iterator itmap = CellsIndexToEdgesIndex.find(i_cell);
        if(itmap != CellsIndexToEdgesIndex.end())//Have found
        {
            for(unsigned int i_edge = 0; i_edge < (itmap->second).size(); i_edge ++)
            {
                unsigned int EdgeIndex=(itmap->second).at(i_edge);
                unsigned int IndexV1=InitialEdges[EdgeIndex].first;
                unsigned int IndexV2=InitialEdges[EdgeIndex].second;

//                cout<<"cell_index = "<<i_cell<<endl;
//                cout<<"IndexV1= "<<IndexV1<<endl;
//                cout<<"IndexV2= "<<IndexV2<<endl;
//                for(unsigned int i_test=0; i_test<P_Cells->at(i_cell).Index_ItsCornerVertices.size(); i_test++)
//                {
//                    cout<<"cell_vertexIndex:"<<i_test<<" "<<P_Cells->at(i_cell).Index_ItsCornerVertices[i_test]<<endl;
//                }

                bool Is_InOrder;

                unsigned int PositionEdge=IndexEdge(P_Cells->at(i_cell).Index_ItsCornerVertices, IndexV1, IndexV2, Is_InOrder);

//                cout<<"Mesh::ConstructCurrentVerticesEdges::P_Cells->at(i_cell).Index_ItsCornerVertices"<<endl;
//                cout<<"i_cell="<<i_cell<<endl;
//                for(unsigned int i_test=0; i_test<P_Cells->at(i_cell).Index_ItsCornerVertices.size(); i_test++)
//                {
//                    cout<<P_Cells->at(i_cell).Index_ItsCornerVertices[i_test]<<endl;
//                }
//                cout<<"IndexV1 = "<<IndexV1<<endl;
//                cout<<"IndexV2 = "<<IndexV2<<endl;

//                cout<<"Is_InOrder = "<<Is_InOrder<<endl;
//                cout<<"PositionEdge = "<<PositionEdge<<endl;

                switch(PositionEdge)
                {
                case 0://the 0-th edge---> NewInfoCells[i_cell].Down_indexSeq
                    if(Is_InOrder)
                    {
                        for(unsigned int i_vertex=0; i_vertex < NewInfoCells[i_cell].Down_indexSeq.size(); i_vertex++)
                        {
                            CurrentVerticesEdges[EdgeIndex].push_back(NewInfoCells[i_cell].Down_indexSeq[i_vertex]);
                        }
                    }
                    else
                    {
                        for(unsigned int i_vertex=0; i_vertex < NewInfoCells[i_cell].Down_indexSeq.size(); i_vertex++)
                        {
                            CurrentVerticesEdges[EdgeIndex].push_back(NewInfoCells[i_cell].Down_indexSeq[NewInfoCells[i_cell].Down_indexSeq.size()-1-i_vertex]);
                        }
                    }
                    break;
                case 1://the 1st edge --> NewInfoCells[i_cell].Right_indexSeq
                    if(Is_InOrder)
                    {
                        for(unsigned int i_vertex=0; i_vertex < NewInfoCells[i_cell].Right_indexSeq.size(); i_vertex++)
                        {
                            CurrentVerticesEdges[EdgeIndex].push_back(NewInfoCells[i_cell].Right_indexSeq[i_vertex]);
                        }
                    }
                    else
                    {
                        for(unsigned int i_vertex=0; i_vertex < NewInfoCells[i_cell].Right_indexSeq.size(); i_vertex++)
                        {
                            CurrentVerticesEdges[EdgeIndex].push_back(NewInfoCells[i_cell].Right_indexSeq[NewInfoCells[i_cell].Right_indexSeq.size()-1-i_vertex]);
                        }
                    }
                    break;
                case 2://the 2nd edge --> NewInfoCells[i_cell].Up_indexSeq
                    if(Is_InOrder)
                    {
                        for(unsigned int i_vertex=0; i_vertex < NewInfoCells[i_cell].Up_indexSeq.size(); i_vertex++)
                        {
                            CurrentVerticesEdges[EdgeIndex].push_back(NewInfoCells[i_cell].Up_indexSeq[i_vertex]);
                        }
                    }
                    else
                    {
                        for(unsigned int i_vertex=0; i_vertex < NewInfoCells[i_cell].Up_indexSeq.size(); i_vertex++)
                        {
                            CurrentVerticesEdges[EdgeIndex].push_back(NewInfoCells[i_cell].Up_indexSeq[NewInfoCells[i_cell].Up_indexSeq.size()-1-i_vertex]);
                        }
                    }
                    break;
                case 3://the 3rd edge --> NewInfoCells[i_cell].Left_indexSeq
                    if(Is_InOrder)
                    {
                        for(unsigned int i_vertex=0; i_vertex < NewInfoCells[i_cell].Left_indexSeq.size(); i_vertex++)
                        {
                            CurrentVerticesEdges[EdgeIndex].push_back(NewInfoCells[i_cell].Left_indexSeq[i_vertex]);
                        }
                    }
                    else
                    {
                        for(unsigned int i_vertex=0; i_vertex < NewInfoCells[i_cell].Left_indexSeq.size(); i_vertex++)
                        {
                            CurrentVerticesEdges[EdgeIndex].push_back(NewInfoCells[i_cell].Left_indexSeq[NewInfoCells[i_cell].Left_indexSeq.size()-1-i_vertex]);
                        }
                    }
                    break;
                default:
                    cout<<"IndexV1 = "<<IndexV1<<endl;
                    cout<<"IndexV2 = "<<IndexV2<<endl;
                    cout<<"CellIndex = "<<i_cell<<endl;

                    cout<<"Mesh:: ConstructCurrentVerticesEdges::PositionEdge's value is wrong"<<endl;
                    break;
                }
            }
        }
    }
}

void Mesh::ConstructCurrentVerticesIndexToCoordinate(const vector<NewInformation> &NewInfoCells,
                                                     map<unsigned int, pair<unsigned int ,Coordinate*> > &CurrentVerticesIndexToCoordinate)
{
    CurrentVerticesIndexToCoordinate.clear();//

    for(unsigned int i_cell=0; i_cell < NewInfoCells.size(); i_cell++)
    {
        for(unsigned int i_subcell=0; i_subcell < NewInfoCells[i_cell].NewDatas.size(); i_subcell++)
        {
            for(unsigned int i_vertex=0; i_vertex < NewInfoCells[i_cell].NewDatas[i_subcell].Index_Vertices.size(); i_vertex++)
            {
                unsigned int IndexVertex=NewInfoCells[i_cell].NewDatas[i_subcell].Index_Vertices[i_vertex];
                map<unsigned int, pair<unsigned int, Coordinate*> >::const_iterator itmap=CurrentVerticesIndexToCoordinate.find(IndexVertex);
                if(itmap==CurrentVerticesIndexToCoordinate.end())
                {

                    double x0=NewInfoCells[i_cell].NewDatas[i_subcell].Position[0];
                    double x1=NewInfoCells[i_cell].NewDatas[i_subcell].Position[1];
                    double y0=NewInfoCells[i_cell].NewDatas[i_subcell].Position[2];
                    double y1=NewInfoCells[i_cell].NewDatas[i_subcell].Position[3];



                    pair<unsigned int, Coordinate*> supCoor;
                    Coordinate * Coor=new Coordinate;

                    switch(i_vertex)
                    {
                    case 0:
                        Coor->xy[0]=x0;
                        Coor->xy[1]=y0;

                        supCoor.first=i_cell;
                        supCoor.second=Coor;
                        break;

                    case 1:
                        Coor->xy[0]=x1;
                        Coor->xy[1]=y0;

                        supCoor.first=i_cell;
                        supCoor.second=Coor;
                        break;
                    case 2:
                        Coor->xy[0]=x1;
                        Coor->xy[1]=y1;

                        supCoor.first=i_cell;
                        supCoor.second=Coor;
                        break;
                    case 3:
                        Coor->xy[0]=x0;
                        Coor->xy[1]=y1;

                        supCoor.first=i_cell;
                        supCoor.second=Coor;
                        break;
                    default:
                        cout<<"Mesh::ConstructCurrentVerticesIndexToCoordinate:: i_vertex is not in {0 1 2 3}"<<endl;
                        cin.get();
                        break;
                    }
                    CurrentVerticesIndexToCoordinate.insert(pair<unsigned int, pair<unsigned int, Coordinate*> > (IndexVertex, supCoor) );
                }
            }
        }
    }
}

void ConstructVerticeSeqFromNewInfoCells(unsigned int k,  vector<NewInformation> &NewInfoCells)
{
    for(unsigned int i_cell=0; i_cell<NewInfoCells.size(); i_cell++)
    {
        //Up
        NewInfoCells[i_cell].Up_indexSeq.clear();
        NewInfoCells[i_cell].Up_indexSeq.push_back(NewInfoCells[i_cell].NewDatas[k].Index_Vertices[3]);
        NewInfoCells[i_cell].Up_indexSeq.push_back(NewInfoCells[i_cell].NewDatas[k].Index_Vertices[2]);
        for(unsigned i_k=1; i_k<k+1; i_k++)
        {
            NewInfoCells[i_cell].Up_indexSeq.push_back(NewInfoCells[i_cell].NewDatas[k+i_k*(k+1)].Index_Vertices[2]);
        }
        //Down
        NewInfoCells[i_cell].Down_indexSeq.clear();
        NewInfoCells[i_cell].Down_indexSeq.push_back(NewInfoCells[i_cell].NewDatas[0].Index_Vertices[0]);
        NewInfoCells[i_cell].Down_indexSeq.push_back(NewInfoCells[i_cell].NewDatas[0].Index_Vertices[1]);
        for(unsigned i_k=1; i_k<k+1; i_k++)
        {
            NewInfoCells[i_cell].Down_indexSeq.push_back(NewInfoCells[i_cell].NewDatas[i_k*(k+1)].Index_Vertices[1]);
        }
        //Left
        NewInfoCells[i_cell].Left_indexSeq.clear();
        NewInfoCells[i_cell].Left_indexSeq.push_back(NewInfoCells[i_cell].NewDatas[0].Index_Vertices[0]);
        NewInfoCells[i_cell].Left_indexSeq.push_back(NewInfoCells[i_cell].NewDatas[0].Index_Vertices[3]);
        for(unsigned i_k=1; i_k<k+1; i_k++)
        {
            NewInfoCells[i_cell].Left_indexSeq.push_back(NewInfoCells[i_cell].NewDatas[i_k].Index_Vertices[3]);
        }
        //Right
        NewInfoCells[i_cell].Right_indexSeq.clear();
        NewInfoCells[i_cell].Right_indexSeq.push_back(NewInfoCells[i_cell].NewDatas[k*(k+1)].Index_Vertices[1]);
        NewInfoCells[i_cell].Right_indexSeq.push_back(NewInfoCells[i_cell].NewDatas[k*(k+1)].Index_Vertices[2]);
        for(unsigned i_k=1; i_k<k+1; i_k++)
        {
            NewInfoCells[i_cell].Right_indexSeq.push_back(NewInfoCells[i_cell].NewDatas[k*(k+1)+i_k].Index_Vertices[2]);
        }

    }
}

Mesh *  Mesh::Subdivide_initialParametricMesh_Globally_Vertex(unsigned int k,
                                      const vector<pair<unsigned int, unsigned int> > &InitialEdges,
                                      const vector<unsigned int> &EdgesIndexofCells,
                                      map<unsigned int, unsigned int> & OriginalVertexToCurrentVertex,
                                      vector<vector<unsigned int> > &CurrentVerticesEdges,
                                      map<unsigned int, pair<unsigned int ,Coordinate*> > &CurrentVerticesIndexToCoordinate)
{
   cout<<"Mesh::Beginning--------------------"<<endl;

    OriginalVertexToCurrentVertex.clear();
    CurrentVerticesEdges.clear();
    CurrentVerticesIndexToCoordinate.clear();

    map<unsigned int, set<unsigned int> > OriginalVertexToCurrentVertices;

    map<unsigned int, vector<unsigned int> > CellsIndexToEdgesIndex;
    for(unsigned int i=0; i<EdgesIndexofCells.size(); i++)
    {
        map<unsigned int, vector<unsigned int> >::iterator itmap=CellsIndexToEdgesIndex.find(EdgesIndexofCells[i]);
        if(itmap==CellsIndexToEdgesIndex.end())//Do not find
        {
            vector<unsigned int> EdgesIndex;
            EdgesIndex.push_back(i);
            CellsIndexToEdgesIndex.insert(pair<unsigned int, vector<unsigned int> >(EdgesIndexofCells[i], EdgesIndex));
        }
        else//Have found
        {
            (itmap->second).push_back(i);
        }
    }

   cout<<"Mesh::Before SubdivideCells"<<endl;

    vector<NewInformation> NewInfoCells;
    SubdivideCells(k, NewInfoCells, OriginalVertexToCurrentVertices);

   cout<<"Mesh::After SubdivideCells()"<<endl;




   // cout<<"Test::Before ReOrganizeVerticesIndices_MergeTheSameVertices"<<endl;

    //Merge the same vertices based on the connecting relationship in Cells
    unsigned int VertexNum;
    ReOrganizeVerticesIndices_MergeTheSameVertices(NewInfoCells, k, VertexNum, OriginalVertexToCurrentVertices, OriginalVertexToCurrentVertex);

    cout<<"Test::Before ConstructCurrentVerticesEdges"<<endl;

//    cout<<"OriginalVertexToCurrentVertex"<<endl;
//    for(map<unsigned int, unsigned int>::const_iterator it=OriginalVertexToCurrentVertex.begin();
//        it!=OriginalVertexToCurrentVertex.end(); it++)
//    {
//        cout<<it->first<<"--->"<<it->second<<endl;
//    }


//Remake: before this function, all the content about verticesSeq can be removed
    ConstructVerticeSeqFromNewInfoCells(k, NewInfoCells);

//    cout<<"NewInfoCells k = "<<k<<endl;
//    for(unsigned int i_test=0; i_test < NewInfoCells.size(); i_test++)
//    {
//        cout<<"the "<<i_test<<"-th: "<<endl;
//        NewInfoCells[i_test].Cout_NewInformation();
//    }

    //Construct CurrentVerticesEdges and CurrentVerticesIndexToCoordinate
    //from NewInfoCells directly
    ConstructCurrentVerticesEdges(NewInfoCells, InitialEdges, CellsIndexToEdgesIndex, CurrentVerticesEdges);

//    cout<<"CurrentVerticesEdges::"<<endl;
//    for(unsigned int i_test=0; i_test<CurrentVerticesEdges.size(); i_test++)
//    {
//        cout<<"The index of edge = "<<i_test<<endl;
//        for(unsigned int j_test=0; j_test<CurrentVerticesEdges[i_test].size(); j_test++)
//        {
//            cout<<CurrentVerticesEdges[i_test][j_test]<<" ";
//        }
//        cout<<endl;
//    }


    //cout<<"Test::Before ConstructCurrentVerticesIndexToCoordinate"<<endl;

    ConstructCurrentVerticesIndexToCoordinate(NewInfoCells, CurrentVerticesIndexToCoordinate);


    //cout<<"Test::Before RestoreMesh_Globally"<<endl;

    //Restore the new mesh with the help of NewInfoCells
    Mesh * P_Mesh=RestoreMesh_Globally(NewInfoCells, VertexNum);

    //cout<<"Ending"<<endl;

    return P_Mesh;

};


void Mesh::NewIndexOfCellOnRefinedMesh(const Mesh* P_ParametricMesh,
                                  const double parameter_u, const double parameter_v,
                                  const unsigned int OriginalCellIndex, unsigned int & NewIndex)

{//*this is obtained by subdivided *P_ParametricMesh
//const double parameter_u, const double parameter_v, const unsigned int OriginalCellIndex
//are about P_ParametricMesh
    NewIndex=0;
    for(set<unsigned int>::const_iterator it=(P_ParametricMesh->P_Cells->at(OriginalCellIndex)).Index_ItsSubCells.begin();
        it!=(P_ParametricMesh->P_Cells->at(OriginalCellIndex)).Index_ItsSubCells.end(); it++)
    {
        unsigned int Index=*it;
        double x0 = this->P_Cells->at(Index).CellSize[0];
        double x1 = this->P_Cells->at(Index).CellSize[1];
        double y0 = this->P_Cells->at(Index).CellSize[2];
        double y1 = this->P_Cells->at(Index).CellSize[3];
        if((x0 <= parameter_u && parameter_u <= x1)&&(y0 <= parameter_v && parameter_v <= y1))
        {
            NewIndex=Index;
            break;
        }
    }
}

/*---------------------------------------------------
 * Get boundary edges from the current parametric mesh
------------------------------------------------------*/
void Mesh::GetBoundaryEdgesFromParametricMesh(set<pair<unsigned int, unsigned int> > &BoundaryEdges)
{
    BoundaryEdges.clear();
    //=======
    for(unsigned int i_vertex = 0; i_vertex < this->P_Vertices->size(); i_vertex++)
    {
        //This vertex is a boundary one
        if(this->P_Vertices->at(i_vertex).Is_interiorVertex==0)
        {
            //set<unsigned int> Index_ItsVertices;
            for(set<unsigned int>::const_iterator jt=this->P_Vertices->at(i_vertex).Index_ItsVertices.begin();
                jt!=this->P_Vertices->at(i_vertex).Index_ItsVertices.end(); jt++)
            {
                unsigned int j_vertex=*jt;
                if(this->P_Vertices->at(j_vertex).Is_interiorVertex==0)//This two vertices are boundary ones
                {
                    //-----Vertices[i_vertex] and Vertices[j_vertex]
                    //are two end vertices of a boundary edges

                    //the number of common cells is no more than 1.
                    bool key=this->Is_ABoundaryEdge(i_vertex, j_vertex);
                    if(key)
                    {
                        if(i_vertex<j_vertex)//The edge vivj, i<j
                        {
                            BoundaryEdges.insert(pair<unsigned int, unsigned int>(i_vertex, j_vertex));
                        }
                        else
                        {
                            BoundaryEdges.insert(pair<unsigned int, unsigned int>(j_vertex, i_vertex));
                        }
                    }

                }
            }
        }
    }
    //=======
}

//========================
bool Mesh::Is_MeshIrregularCenter(const bool &IndexVIsMeshIrregular, const unsigned int &IndexV, unsigned int &IrregularCenterIndex)
{
    bool key=false;
    IrregularCenterIndex=0;

    if(IndexVIsMeshIrregular==false)
    {
        //set<unsigned int> Index_ItsVertices;
        for(set<unsigned int>::const_iterator it=this->P_Vertices->at(IndexV).Index_ItsVertices.begin();
            it!=this->P_Vertices->at(IndexV).Index_ItsVertices.end(); it++)
        {
            if(this->MeshIrregularVerticesMap.find(*it)!=this->MeshIrregularVerticesMap.end())
            {
                //
                key=true;
                IrregularCenterIndex=*it;
                //
            }
        }
    }

    return key;
}

//========================================
void Mesh::IrrCellIndices(set<unsigned int>& IrrCells)
{
    IrrCells.clear();

    //
    for(map<unsigned, unsigned>::iterator it=this->MeshIrregularVerticesMap.begin();
        it!=this->MeshIrregularVerticesMap.end(); it++)
    {
        //IrrCells.insert((*it).first);
       for(map<unsigned int, Coordinate>::iterator jt=this->P_Vertices->at((*it).first).ItsCellsToCoordinatesMap.begin();
           jt!=this->P_Vertices->at((*it).first).ItsCellsToCoordinatesMap.end();jt++)
       {
           IrrCells.insert((*jt).first);
       };


    }
}

//========================================
bool Mesh::CommonCellsIndexOfAnEdges(set<unsigned int> &CommonCellsIndex, const unsigned int &IndexV1, const unsigned int &IndexV2)
{
    bool Has_CommonCell=false;

    CommonCellsIndex.clear();

    set<unsigned int> CellIndex1;
    Map2set_unsigned_int(CellIndex1, P_Vertices->at(IndexV1).ItsCellsToCoordinatesMap);

    set<unsigned int> CellIndex2;
    Map2set_unsigned_int(CellIndex2, P_Vertices->at(IndexV2).ItsCellsToCoordinatesMap);

    set_intersection(CellIndex1.begin(), CellIndex1.end(),
                     CellIndex2.begin(), CellIndex2.end(),
                     inserter(CommonCellsIndex, CommonCellsIndex.begin()));

//    cout<<"CommonCellsIndex"<<endl;
//    for(set<unsigned int>::const_iterator it=CommonCellsIndex.begin();
//        it!=CommonCellsIndex.end(); it++)
//    {
//        cout<<*it<<" "<<endl;

//        cin.get();
//        cin.get();
//        cin.get();
//    }

    if(CommonCellsIndex.size())
    {
        Has_CommonCell=true;
    }
    return Has_CommonCell;

}

//=========================================
bool Mesh::Is_DirectionAlongAnEdge(const unsigned int &index, const unsigned int &IndexV, const unsigned int &IrregularCenterIndex)
{
    //cout<<"Begin:Mesh::Is_DirectionAlongAnEdge()"<<endl;

    bool Is_direction=false;

    /*Find the common cell of the IndexV and IrregularCenterIndex-th vertices*/
    set<unsigned int> CommonCellsIndexSet;
    bool Is_CommonCell;
    Is_CommonCell=this->CommonCellsIndexOfAnEdges(CommonCellsIndexSet, IndexV, IrregularCenterIndex);

//    /*For testing*/
//    cout<<"IndexV="<<IndexV<<endl;
//    cout<<"IrregularCenterIndex="<<IrregularCenterIndex<<endl;
//    cout<<"Is_CommonCell="<<Is_CommonCell<<endl;

    if(Is_CommonCell)
    {
        unsigned int CommonCellIndex=*CommonCellsIndexSet.begin();
        //Generate BasesVertex at the IndexV-th vertex over the CommonCellIndex-th cell
        unsigned int IndexBV=index % 4; //"index" is the index in the coeff vector of this mspline

//        cout<<"CommonCellIndex="<<CommonCellIndex<<endl;
//        cout<<"IndexBV="<<IndexBV<<endl;

        if(IndexBV)//the index-th element of coeff vector doesn't describe the position of this vertex
        {
            //1.Set HermiteData for this vertex
            vector<vector<double> > HermiteDatas;
            Set_HermiteData_Vertex(IndexV, *this, HermiteDatas);

            //2. The basis with the index (IndexBV)
            basis *P_B=new basis(IndexV, IndexBV, CommonCellIndex, *this, HermiteDatas[IndexBV]);


            //3. Compute the direction (s or t) of this edge in the CommonCellIndex-th cell
            int LocalDirectionFlag;//s-direction (1), -s -direction (-1); t-direction (2), -t -direction (-2)
            LocalDirectionFlag=this->P_Cells->at(CommonCellIndex).TheLocalDirection(IndexV, IrregularCenterIndex);

            //4. Compute the coordinate of the IrregularVertex in the CommonCellIndex-th cell
            vector<double> XY;
            bool Is_IrregularVertex=this->P_Cells->at(CommonCellIndex).CoordinateOfACornerVertex(IrregularCenterIndex, XY);


//            cin.get();
//            cout<<"The basis:"<<endl;
//            P_B->Cout_basis(*this);
//            cout<<"-----------"<<endl;
//            cin.get();
//            cout<<"IrregularVertexCoordinates=("<<XY[0]<<", "<<XY[1]<<")"<<endl;
//            cout<<"-----------"<<endl;
//            cout<<"IndexV="<<IndexV<<endl;
//            cout<<"IrregularVertexIndex="<<IrregularCenterIndex<<endl;
//            cout<<"CommonCellIndex="<<CommonCellIndex<<endl;
//            cout<<"LocalDirectionFlag="<<LocalDirectionFlag<<endl;
//            cout<<endl;
//            cout<<endl;

            //5. Compute the 2-order derivative alone the direction
            double Values;
            if(Is_IrregularVertex)
            {
                if(LocalDirectionFlag==1|| LocalDirectionFlag==-1)//s-direction
                {
                    P_B->Evaluation(0, 2, 0, XY, Values, this->P_Cells->at(CommonCellIndex).CellSize);

                    //double v;
                    //P_B->Evaluation(0, 3, 0, XY, v, this->P_Cells->at(CommonCellIndex).CellSize);
                    Values=sqrt(Values*Values);//+sqrt(v*v);

                }
                else//t-direction
                {
                    P_B->Evaluation(0, 0, 2, XY, Values, this->P_Cells->at(CommonCellIndex).CellSize);

                    //double v;
                    //P_B->Evaluation(0, 0, 3, XY, v, this->P_Cells->at(CommonCellIndex).CellSize);//Position
                    Values=sqrt(Values*Values);//+sqrt(v*v);

                }
                //cout<<"Values= "<<Values<<endl;

                if(Values>0.5)
                {
                    Is_direction=true;
                }
            }
            //
            delete P_B;
            P_B=NULL;
        }


    }
    return Is_direction;

}

/*For the adjacent cell or vertex*/

unsigned int Mesh::NextIndexVertex(const unsigned int &IndexCell, const unsigned int &IndexCenterVertex, const unsigned int &IndexV1)
{
    unsigned int IndexV2=0;
    unsigned int PositionIndexCenterVertex;
    this->P_Cells->at(IndexCell).IndexInItsCornerVertices(IndexCenterVertex, PositionIndexCenterVertex);

    switch (PositionIndexCenterVertex)
    {
    case 0:
        if(this->P_Cells->at(IndexCell).Index_ItsCornerVertices[1]==IndexV1)
        {
            IndexV2=this->P_Cells->at(IndexCell).Index_ItsCornerVertices[3];
        }
        else
        {
            IndexV2=this->P_Cells->at(IndexCell).Index_ItsCornerVertices[1];
        }
        break;
    case 1:
        if(this->P_Cells->at(IndexCell).Index_ItsCornerVertices[0]==IndexV1)
        {
            IndexV2=this->P_Cells->at(IndexCell).Index_ItsCornerVertices[2];
        }
        else
        {
            IndexV2=this->P_Cells->at(IndexCell).Index_ItsCornerVertices[0];
        }
        break;
    case 2:
        if(this->P_Cells->at(IndexCell).Index_ItsCornerVertices[1]==IndexV1)
        {
            IndexV2=this->P_Cells->at(IndexCell).Index_ItsCornerVertices[3];
        }
        else
        {
            IndexV2=this->P_Cells->at(IndexCell).Index_ItsCornerVertices[1];
        }
        break;
    case 3:
        if(this->P_Cells->at(IndexCell).Index_ItsCornerVertices[0]==IndexV1)
        {
            IndexV2=this->P_Cells->at(IndexCell).Index_ItsCornerVertices[2];
        }
        else
        {
            IndexV2=this->P_Cells->at(IndexCell).Index_ItsCornerVertices[0];
        }
        break;
    default:
        std::cout<<"Mesh::NextIndexVertex()::IndexV1 doesn't equal to 1, 2, 3, 0"<<std::endl;
        std::cin.get();
        break;
    }

    return IndexV2;
};

unsigned int Mesh::NextIndexCell(const unsigned int &IndexCell1, const unsigned int &IndexV1, const unsigned int &IndexV2)
{
    unsigned int IndexCell2=0;
    set<unsigned int> CommonCellIndices;
    this->CommonCellsIndexOfAnEdges(CommonCellIndices, IndexV1, IndexV2);

    for(set<unsigned int>::const_iterator itCell=CommonCellIndices.begin();
        itCell!=CommonCellIndices.end(); itCell++)
    {
        if(*itCell!=IndexCell1)
        {
            IndexCell2=*itCell;
            break;
        }
    }

    return IndexCell2;
};

/*Compute the one neighbourhood Vertex sequence around a center vertex
Return: vector<unsigened int> & VertexIndicesSeqAroundCenterVertex
RMK: Vertex indices in VertexIndicesSeqAroundCenterVertex are arranged in some order*/
void Mesh::OneNeighbourhoodSequenceAroundCenterVertex(vector<unsigned int> & VertexIndicesSeqAroundCenterVertex, const unsigned int & CenterVIndex)
{
    VertexIndicesSeqAroundCenterVertex.clear();

    //Check the CenterVIndex-th vertex is a boundary vertex or an interior vertex
    if(this->P_Vertices->at(CenterVIndex).Is_interiorVertex)//This vertex is an interior vertex
    {
        unsigned int V0Index=*(this->P_Vertices->at(CenterVIndex).Index_ItsVertices.begin());
        VertexIndicesSeqAroundCenterVertex.push_back(V0Index);
        set<unsigned int> CommonCellIndices;
        this->CommonCellsIndexOfAnEdges(CommonCellIndices, V0Index, CenterVIndex);
        //
        unsigned int CellIndex0=*CommonCellIndices.begin();
        while (VertexIndicesSeqAroundCenterVertex.size()<this->P_Vertices->at(CenterVIndex).Index_ItsVertices.size())
        {
            unsigned int V1Index=this->NextIndexVertex(CellIndex0, CenterVIndex, V0Index);
            VertexIndicesSeqAroundCenterVertex.push_back(V1Index);
            V0Index=V1Index;


            unsigned int CellIndex1=this->NextIndexCell(CellIndex0, CenterVIndex, V0Index);
            CellIndex0=CellIndex1;
        }

    }
    else//This vertex is a boundary vertex
    {
        unsigned int V0Index(0), CellIndex0(0);

        //find another boundary vertex as a vertex beginner
        for(set<unsigned int>::const_iterator itV = this->P_Vertices->at(CenterVIndex).Index_ItsVertices.begin();
            itV!= this->P_Vertices->at(CenterVIndex).Index_ItsVertices.end(); itV ++)
        {
            unsigned int Vindex=*itV;
            if(this->P_Vertices->at(Vindex).Is_interiorVertex==false)
            {
                V0Index=Vindex;
                break;
            }
        }
        VertexIndicesSeqAroundCenterVertex.push_back(V0Index);
        //compute the common cell as a cell beginner
        set<unsigned int> CommonCellIndices;
        this->CommonCellsIndexOfAnEdges(CommonCellIndices, V0Index, CenterVIndex);
        CellIndex0=*CommonCellIndices.begin();
        //while loop
        while (VertexIndicesSeqAroundCenterVertex.size()<this->P_Vertices->at(CenterVIndex).Index_ItsVertices.size())
        {
            unsigned int V1Index=this->NextIndexVertex(CellIndex0, CenterVIndex, V0Index);
            VertexIndicesSeqAroundCenterVertex.push_back(V1Index);
            V0Index=V1Index;

            unsigned int CellIndex1=this->NextIndexCell(CellIndex0, CenterVIndex, V0Index);
            CellIndex0=CellIndex1;
        }
    }
}

/* if the edge=IndexV1IndexV2 is a boundary edge, the return true;
 * otherwise, return false
*/
bool Mesh::Is_ABoundaryEdge(const unsigned int IndexV1, const unsigned int IndexV2)
{
    bool is_aboundaryEdge=false;//

    set<unsigned int> CommCellsIndex;
    CommonCellsIndexOfAnEdges(CommCellsIndex, IndexV1, IndexV2);
    if(CommCellsIndex.size()==1)
    {
        is_aboundaryEdge=true;
    }

    return is_aboundaryEdge;
}






