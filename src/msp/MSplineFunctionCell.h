#ifndef MSPLINEFUNCTIONCELL_H
#define MSPLINEFUNCTIONCELL_H
#include <vector>
using namespace std;
//--------------------
class HermiteCell;
class PointSet;
class Mesh;
//--------------------
class MSplineFunctionCell
{
public:
	HermiteCell * P_MSplineCell;
public:
	MSplineFunctionCell(void);
	~MSplineFunctionCell(void);
	MSplineFunctionCell(const vector<double> & Coeff, unsigned int Index_Cell, const Mesh & CurrentMesh);
    /**
     * @brief MSplineFunctionCell: MSplineFunctionCell=coeff*P_MSpCell
     * @param coeff
     * @param P_MSpCell
     */
    MSplineFunctionCell(double coeff, const MSplineFunctionCell *P_MSpCell);

    /**
     * @brief MSplineFunctionCell: Generate a Zero function
     * @param index_InitialCell
     * @param index_Cell
     */
    MSplineFunctionCell(unsigned int index_InitialCell, unsigned int index_Cell);

public:
    void Evaluation(int DerOrderu, int DerOrderv, const PointSet & Pset, vector<double> &Values, const Mesh & CurrentMesh);
    void Cout_MSplineFunctionCell();

    void BSplineCoeffAxelOrder(double* CellSize, vector<double> &Coeffi);

public:
	//----overload---
	//PreCondition: *this and right share the same cell index.
	//multiply scalar
    /**
     * @brief Lambda:  coeff*thisMSplineFunctionCell
     * @param coeff
     * @return
     */
    MSplineFunctionCell Lambda(const double coeff);
	//+
    /**
     * @brief operator +: overload "+"
     * @param right (it shares the same cell index with *this)
     * @return
     */
    MSplineFunctionCell operator+(const MSplineFunctionCell &right);
	//-
    /**
     * @brief operator -: overload "-"
     * @param right (it shares the same cell index with *this)
     * @return
     */
    MSplineFunctionCell operator-(const MSplineFunctionCell &right);
	//=
    /**
     * @brief operator =: overload "="
     * @param right (it shares the same cell index with *this)
     * @return
     */
    MSplineFunctionCell& operator=(const MSplineFunctionCell &right);
};

#endif //MSPLINEFUNCTIONCELL_H
