#ifndef FEM_ERRORFUNCTIONS_H
#define FEM_ERRORFUNCTIONS_H

#include "FEM_Functions.h"
#include "MSplineFunctions.h"


//Residualbased error
static void ResidualError_Mesh_RefinedParametricMap(const vector<double>& u_gausspt, const vector<double> & v_gausspt,
                   double **w, const Vec & m_solut,
                   vector<double> &cell_errors, double &totol_Error,
                   const Mesh* P_FEMMesh, Mesh* P_ParametricMesh,
                   MSplineFunctions * P_MSplineParametricMap, unsigned int ExampleIndex)
{
    unsigned int u_gaussnum=u_gausspt.size();
    unsigned int v_gaussnum=v_gausspt.size();

    vector<double> fval;
    //vector<double> Jval;
    vector<double> UxxPUyy;

    cell_errors.clear();
    cell_errors.resize(P_FEMMesh->P_Cells->size());
    totol_Error=0.0;

    vector<double> solution;
    PetscInt SolutSize;
    VecGetSize(m_solut, &SolutSize);
    for(PetscInt Ipetsc = 0; Ipetsc < SolutSize; Ipetsc++)
    {
        PetscScalar v;
        VecGetValues(m_solut,1,&Ipetsc,&v);
        solution.push_back(v);
    }

    MSplineFunction * P_FEMSolution=new MSplineFunction(solution, *P_FEMMesh);//The Solution of FEM

    for (int i=0;i<P_FEMMesh->P_Cells->size();i++)
    {
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];

        vector<Coordinate*> Pcoordinate_Ps;
        for (int k=0; k<u_gaussnum; k++)
        {
            for (int l=0; l<v_gaussnum; l++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=0.5*(x1+x0)+0.5*(x1-x0)*u_gausspt[k];//u[k];
                CoorP->xy[1]=0.5*(y1+y0)+0.5*(y1-y0)*v_gausspt[l];//v[l];(y0+y1+(y1-y0)*gausspt[k]) * 0.5;
                Pcoordinate_Ps.push_back(CoorP);
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        //f(x,y)
        ComputeFunval_MSplineParametricMap(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent,
                                           fval, 1, *P_ParametricMesh, P_MSplineParametricMap,
                                           ExampleIndex);
//        //J(x,y)
//        JacobianValue(P_MSplineParametricMap->P_MSplineFunctions[0],P_MSplineParametricMap->P_MSplineFunctions[1],
//                      Pcoordinate_Ps, Jval, P_FEMMesh->P_Cells->at(i).ItsParent,
//                      P_FEMMesh->P_Cells->at(i).ItsParent, P_ParametricMesh, P_ParametricMesh);
        //Uxx+Uyy
        DUxxValue_PLUS_DUyyValue(P_MSplineParametricMap->P_MSplineFunctions[0], P_MSplineParametricMap->P_MSplineFunctions[1],
                                 P_FEMSolution, Pcoordinate_Ps, UxxPUyy, i, P_FEMMesh, P_ParametricMesh);

        cell_errors[i] = 0;
        for(int j=0;j<u_gaussnum*v_gaussnum;j++)
        {
            //cout<<"ResidualError_Mesh: UxxPUyy["<<j<<"]= "<<UxxPUyy[j]<<endl;
            int nn = j / v_gaussnum;
            int mm = j % v_gaussnum;
            cell_errors[i] += w[nn][mm]*pow(fval[j]+UxxPUyy[j],2);
        }
        //=======================================================
        totol_Error = totol_Error+cell_errors[i]*(y1-y0)*(x1-x0)/4;
        //=======================================================
        cell_errors[i] = sqrt(cell_errors[i] *(y1-y0) * (x1-x0)/4);
        //cout<<"Errors at "<<i<<"-th cell = "<<cell_errors[i]<<endl;
    }
    totol_Error = sqrt(totol_Error);
}

//Residualbased error  //
static void ResidualError_Mesh_RefinedParametricMap_PDE_PFEM(const vector<double>& u_gausspt, const vector<double> & v_gausspt,
                   double **w, const Vec & m_solut,
                   vector<double> &cell_errors, double &totol_Error,
                   const Mesh* P_FEMMesh, Mesh* P_ParametricMesh,
                   MSplineFunctions * P_MSplineParametricMap,
                   unsigned int ExampleIndex, unsigned int EquationIndex)
{
    unsigned int u_gaussnum=u_gausspt.size();
    unsigned int v_gaussnum=v_gausspt.size();

    vector<double> fval;
    //vector<double> Jval;
    vector<double> UxxPUyy;

    cell_errors.clear();
    cell_errors.resize(P_FEMMesh->P_Cells->size());
    totol_Error=0.0;

    vector<double> solution;
    PetscInt SolutSize;
    VecGetSize(m_solut, &SolutSize);
    for(PetscInt Ipetsc = 0; Ipetsc < SolutSize; Ipetsc++)
    {
        PetscScalar v;
        VecGetValues(m_solut,1,&Ipetsc,&v);
        solution.push_back(v);
    }

    MSplineFunction * P_FEMSolution=new MSplineFunction(solution, *P_FEMMesh);//The Solution of FEM

    for (int i=0;i<P_FEMMesh->P_Cells->size();i++)
    {
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];

        vector<Coordinate*> Pcoordinate_Ps;
        for (int k=0; k<u_gaussnum; k++)
        {
            for (int l=0; l<v_gaussnum; l++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=0.5*(x1+x0)+0.5*(x1-x0)*u_gausspt[k];//u[k];
                CoorP->xy[1]=0.5*(y1+y0)+0.5*(y1-y0)*v_gausspt[l];//v[l];(y0+y1+(y1-y0)*gausspt[k]) * 0.5;
                Pcoordinate_Ps.push_back(CoorP);
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        //f(x,y)
        ComputeFunval_MSplineParametricMap_PDE_PFEM(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent,
                                           fval, 1, *P_ParametricMesh, P_MSplineParametricMap,
                                           ExampleIndex, EquationIndex);
//        //J(x,y)
//        JacobianValue(P_MSplineParametricMap->P_MSplineFunctions[0],P_MSplineParametricMap->P_MSplineFunctions[1],
//                      Pcoordinate_Ps, Jval, P_FEMMesh->P_Cells->at(i).ItsParent,
//                      P_FEMMesh->P_Cells->at(i).ItsParent, P_ParametricMesh, P_ParametricMesh);
        //Uxx+Uyy
        DUxxValue_PLUS_DUyyValue(P_MSplineParametricMap->P_MSplineFunctions[0], P_MSplineParametricMap->P_MSplineFunctions[1],
                                 P_FEMSolution, Pcoordinate_Ps, UxxPUyy, i, P_FEMMesh, P_ParametricMesh);

        cell_errors[i] = 0;
        for(int j=0;j<u_gaussnum*v_gaussnum;j++)
        {
            //cout<<"ResidualError_Mesh: UxxPUyy["<<j<<"]= "<<UxxPUyy[j]<<endl;
            int nn = j / v_gaussnum;
            int mm = j % v_gaussnum;
            cell_errors[i] += w[nn][mm]*pow(fval[j]+UxxPUyy[j],2);
        }
        //=======================================================
        totol_Error = totol_Error+cell_errors[i]*(y1-y0)*(x1-x0)/4;
        //=======================================================
        cell_errors[i] = sqrt(cell_errors[i] *(y1-y0) * (x1-x0)/4);
        //cout<<"Errors at "<<i<<"-th cell = "<<cell_errors[i]<<endl;
    }
    totol_Error = sqrt(totol_Error);
}
//===========================================================================
static double HeronAreaFormula(double a, double b, double c)
{
    //the lenghts of the triangular are a, b, c, return the area of this triangular
    double p=(a+b+c)/2.0;
    double areai=sqrt(p*(p-a)*(p-b)*(p-c));
    return areai;
}
/* For the triangulars in a axel mesh (), compute the average hights for each cell, the residual errors for each cell */
static void ComputerMeanHightAndAreasForEachTrangules(const vector<vector<vector<double> > > &VerticesForCells,
                                                      const vector<vector<vector<unsigned int> > > &FacesForCells,
                                                      vector<vector<double> > &TrangulesCenters,
                                                      vector<double>& MeanHight,
                                                      vector<double>&TriangulesResidualErrors)
{
    //Preparation
    TrangulesCenters.clear();
    MeanHight.clear();
    TriangulesResidualErrors.clear();

    vector<vector<unsigned int> > VerticesIndexSeq;
    for(unsigned int i=0; i<VerticesForCells.size(); i++)
    {
        for(unsigned int j=0; j<VerticesForCells[i].size(); j++)
        {
            vector<unsigned int> VIndex;
            VIndex.push_back(i);
            VIndex.push_back(j);
            VerticesIndexSeq.push_back(VIndex);
        }
    }

    //cout<<"VerticesIndexSeq.size()="<<VerticesIndexSeq.size()<<endl;

    for(unsigned int iTrangular1=0; iTrangular1<FacesForCells.size(); iTrangular1++)
    {
        for(unsigned int iTrangular2=0; iTrangular2<FacesForCells[iTrangular1].size(); iTrangular2++)
        {
            //cout<<"iTr1="<<iTrangular1<<endl;
            //cout<<"iTr2="<<iTrangular2<<endl;

            unsigned int iV1=FacesForCells[iTrangular1][iTrangular2][0];
            unsigned int iV2=FacesForCells[iTrangular1][iTrangular2][1];
            unsigned int iV3=FacesForCells[iTrangular1][iTrangular2][2];

            //cout<<"iV1 = "<<iV1<<endl;
            //cout<<"iV2 = "<<iV2<<endl;
            //cout<<"iV3 = "<<iV3<<endl;
            //
            //For the iV1-th Vertices: The position: VerticesForCells[VerticesIndexSeq[iV1][0]][VerticesIndexSeq[iV1][1]]
            double x1=VerticesForCells[VerticesIndexSeq[iV1][0]][VerticesIndexSeq[iV1][1]][0];
            double y1=VerticesForCells[VerticesIndexSeq[iV1][0]][VerticesIndexSeq[iV1][1]][1];
            double z1=VerticesForCells[VerticesIndexSeq[iV1][0]][VerticesIndexSeq[iV1][1]][2];
            //For the iV2-th Vertices: The position: VerticesForCells[VerticesIndexSeq[iV2][0]][VerticesIndexSeq[iV2][1]]
            double x2=VerticesForCells[VerticesIndexSeq[iV2][0]][VerticesIndexSeq[iV2][1]][0];
            double y2=VerticesForCells[VerticesIndexSeq[iV2][0]][VerticesIndexSeq[iV2][1]][1];
            double z2=VerticesForCells[VerticesIndexSeq[iV2][0]][VerticesIndexSeq[iV2][1]][2];
            //For the iV3-th Vertices: The position: VerticesForCells[VerticesIndexSeq[iV3][0]][VerticesIndexSeq[iV3][1]]
            double x3=VerticesForCells[VerticesIndexSeq[iV3][0]][VerticesIndexSeq[iV3][1]][0];
            double y3=VerticesForCells[VerticesIndexSeq[iV3][0]][VerticesIndexSeq[iV3][1]][1];
            double z3=VerticesForCells[VerticesIndexSeq[iV3][0]][VerticesIndexSeq[iV3][1]][2];

            //cout<<"(x1, y1, z1)=("<<x1<<", "<<y1<<", "<<z1<<")"<<endl;
            //cout<<"(x2, y2, z2)=("<<x2<<", "<<y2<<", "<<z2<<")"<<endl;
            //cout<<"(x3, y3, z3)=("<<x3<<", "<<y3<<", "<<z3<<")"<<endl;
            //The planar trangular center position
            vector<double> TCenteri;
            TCenteri.push_back((x1+x2+x3)/3.0);
            TCenteri.push_back((y1+y2+y3)/3.0);
            TrangulesCenters.push_back(TCenteri);

            //The mean Hight
            MeanHight.push_back((z1+z2+z3)/3.0);

            //
            double a(0.0),b(0.0),c(0.0);//The lengths of this planar triangular
            a=sqrt((x1-x3)*(x1-x3)+(y1-y3)*(y1-y3));//V1V3
            b=sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));//V1V2
            c=sqrt((x2-x3)*(x2-x3)+(y2-y3)*(y2-y3));//V2V3
            double areai=HeronAreaFormula(a,b,c);//the area of this triangular
            //The residual error on this triangular
            TriangulesResidualErrors.push_back(areai*abs_double(z1+z2+z3)/3.0);

        }
    }

    //cout<<"MeanHight.size()="<<MeanHight.size()<<endl;
    //cout<<"TriangulesResidualErrors.size()="<<TriangulesResidualErrors.size()<<endl;

}

static void PrintAxelMeshFile_FEMSolution_SpecialParameterization(const Vec & m_solut, const Mesh*  P_FEMMesh,
                                                                  unsigned int ExampleIndex, unsigned int k, unsigned int SubExampleIndex)
{
    unsigned int n_v=1+40/(k+1);
    unsigned int n_u=1+40/(k+1);

    unsigned int nV=0;
    unsigned int nF=0;
    unsigned int nE=0;

    vector<vector<vector<double> > > VerticesForCells;
    //vector<vector<double> > VerticesSeq;
    vector<vector<vector<unsigned int> > > FacesForCells;
    vector<vector<vector<unsigned int> > > EdgesForCells;

    //---------
    vector<double> solution;
    PetscInt SolutSize;
    VecGetSize(m_solut, &SolutSize);
    for(PetscInt Ipetsc = 0; Ipetsc < SolutSize; Ipetsc++)
    {
        PetscScalar v;
        VecGetValues(m_solut,1,&Ipetsc,&v);
        solution.push_back(v);
    }

    MSplineFunction * P_FEMSolution=new MSplineFunction(solution, *P_FEMMesh);//The Solution of FEM

    for (int i=0;i<P_FEMMesh->P_Cells->size();i++)
    {
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];

        double dx=(x1-x0)/(n_u-1.0);
        double dy=(y1-y0)/(n_v-1.0);


        vector<Coordinate*> Pcoordinate_Ps;
        for (int u_index=0; u_index < n_u; u_index++)
        {
            for (int v_index=0; v_index < n_v; v_index++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=u_index*dx+x0;//
                CoorP->xy[1]=v_index*dy+y0;//
                Pcoordinate_Ps.push_back(CoorP);
            }
        }

        vector<double> FEMSolutionVal;

        P_FEMSolution->Evaluation(0,0,Pcoordinate_Ps,FEMSolutionVal,i,*P_FEMMesh);
        /////////////////////////////////////////////////////////////////////////////////////////////////
        //f(x,y)
        //ComputeFunval_MSplineParametricMap(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent,
        //                                  fval, 1, *P_ParametricMesh, P_MSplineParametricMap, ExampleIndex);
        //Uxx+Uyy
        //DUxxValue_PLUS_DUyyValue(P_MSplineParametricMap->P_MSplineFunctions[0], P_MSplineParametricMap->P_MSplineFunctions[1],
        //                         P_FEMSolution, Pcoordinate_Ps, UxxPUyy, i, P_FEMMesh, P_ParametricMesh);


        //=============================
        //vector<vector<double> > ValuesXYZ;
        //P_MSplineParametricMap->Evaluation(0, 0, Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent, ValuesXYZ, *P_ParametricMesh);
        vector<double> X, Y;
        ParametricMapSigmaVal(0,0,Pcoordinate_Ps,X,Y,ExampleIndex,SubExampleIndex);
        //
        vector<vector<double> > VerticesForCelli;
        for(unsigned int iv=0; iv<X.size(); iv++)
        {
            vector<double> Vi;
            Vi.push_back(X[iv]);
            Vi.push_back(Y[iv]);
            Vi.push_back(FEMSolutionVal[iv]);

            VerticesForCelli.push_back(Vi);
            //VerticesSeq.push_back(Vi);//The position:(VerticesSeq[i][0], VerticesSeq[i][1])
        }
        nV=nV+VerticesForCelli.size();
        VerticesForCells.push_back(VerticesForCelli);

        //====Faces==
        vector<vector<unsigned int> > FacesForCelli;
        unsigned cell_index=i;
        //For Faces(triangules)
        for(int u_index=0; u_index < n_u-1; u_index++)
        {
            for(int v_index=0; v_index < n_v-1; v_index++)
            {
                vector<unsigned int> Fi;
                Fi.push_back(u_index*n_v+v_index+n_u*n_v*cell_index);
                Fi.push_back(u_index*n_v+v_index+1+n_u*n_v*cell_index);
                Fi.push_back((u_index+1)*n_v+v_index+n_u*n_v*cell_index);
                FacesForCelli.push_back(Fi);

                Fi.clear();
                Fi.push_back(u_index*n_v+v_index+1+n_u*n_v*cell_index);
                Fi.push_back((u_index+1)*n_v+v_index+n_u*n_v*cell_index);
                Fi.push_back((u_index+1)*n_v+v_index+1+n_u*n_v*cell_index);
                FacesForCelli.push_back(Fi);
            };
        };
        nF=nF+FacesForCelli.size();
        FacesForCells.push_back(FacesForCelli);
        //Edges
        vector<vector<unsigned int> > EdgesForCelli;
        vector<unsigned int> Ei;
        //For Edges
        for(unsigned int ie=0; ie<n_v-1; ie++)
        {
            Ei.clear();
            Ei.push_back(ie+n_u*n_v*cell_index);
            Ei.push_back(ie+1+n_u*n_v*cell_index);
            EdgesForCelli.push_back(Ei);
        }
        for(int je = 0; je < n_u-1 ; je++)
        {
            Ei.clear();
            Ei.push_back(n_u-1+je*n_v+n_u*n_v*cell_index);
            Ei.push_back(n_u-1+(je+1)*n_v+n_u*n_v*cell_index);
            EdgesForCelli.push_back(Ei);
        }

        for(int ie = 0; ie < n_v-1 ; ie++)
        {
            Ei.clear();
            Ei.push_back((n_u-1)*n_v + (n_v-1-ie)+n_u*n_v*cell_index);
            Ei.push_back((n_u-1)*n_v + (n_v-1-(ie+1))+n_u*n_v*cell_index);
            EdgesForCelli.push_back(Ei);
        }

        for(int je = 0; je < n_u-1 ; je++)
        {
            Ei.clear();
            Ei.push_back((n_u-1-je)*n_v+n_u*n_v*cell_index);
            Ei.push_back((n_u-1-(je+1))*n_v+n_u*n_v*cell_index);
            EdgesForCelli.push_back(Ei);
        }
        nE=nE+EdgesForCelli.size();
        EdgesForCells.push_back(EdgesForCelli);
    }


    //--------------
    ofstream ofValueDistribution;
    ofValueDistribution.open("FEMSolutionWithSpecialParameterization_AxelMesh.axl");
    ofValueDistribution<<"<axl>"<<endl;
    ofValueDistribution<<"<mesh>"<<endl;

    ofValueDistribution<<"<count>"<<nV<<" "<<nE<<" "<<nF<<"</count>"<<endl;
    //Points
    ofValueDistribution<<"<points>"<<endl;
    for(unsigned int iv1=0; iv1<VerticesForCells.size(); iv1++)
    {
        for(unsigned int iv2=0; iv2<VerticesForCells[iv1].size(); iv2++)
        {
            //The first Choice
            for(unsigned int iv3=0; iv3<VerticesForCells[iv1][iv2].size(); iv3++)
            {
                ofValueDistribution<<VerticesForCells[iv1][iv2][iv3]<<" ";
            }
            ofValueDistribution<<endl;
        }
    }
    ofValueDistribution<<"</points>"<<endl;

    //===Faces
    ofValueDistribution<<"<faces>"<<endl;
    for(unsigned int iF1=0; iF1<FacesForCells.size(); iF1++)
    {
        for(unsigned int iF2=0; iF2<FacesForCells[iF1].size(); iF2++)
        {

            ofValueDistribution<<3<<" ";
            for(unsigned int iF3=0; iF3<FacesForCells[iF1][iF2].size();iF3++)
            {
                ofValueDistribution<<FacesForCells[iF1][iF2][iF3]<<" ";
            }
            ofValueDistribution<<endl;
            //
        }
    }
    ofValueDistribution<<"</faces>"<<endl;

    ofValueDistribution<<"<edges>"<<endl;
    for(unsigned int iE1=0; iE1<EdgesForCells.size(); iE1++)
    {
        for(unsigned int iE2=0; iE2<EdgesForCells[iE1].size(); iE2++)
        {
            ofValueDistribution<<2<<" ";
            for(unsigned int iE3=0; iE3<EdgesForCells[iE1][iE2].size(); iE3++)
            {
                ofValueDistribution<<EdgesForCells[iE1][iE2][iE3]<<" ";
            }
            ofValueDistribution<<endl;
        }
    }
    ofValueDistribution<<"</edges>"<<endl;

    ofValueDistribution<<"</mesh>"<<endl;
    ofValueDistribution<<"</axl>"<<endl;
    ofValueDistribution.close();

}

static void ResidualError_PhysicalDomain_AxelMeshSurface(const Vec & m_solut,
                                                         const Mesh*  P_FEMMesh, Mesh* P_ParametricMesh,
                                                         MSplineFunctions * P_MSplineParametricMap,
                                                         unsigned int ExampleIndex, unsigned int k)
{
    unsigned int n_v=1+40/(k+1);
    unsigned int n_u=1+40/(k+1);

    unsigned int nV=0;
    unsigned int nF=0;
    unsigned int nE=0;


    vector<vector<vector<double> > > VerticesForCells;
    //vector<vector<double> > VerticesSeq;
    vector<vector<vector<unsigned int> > > FacesForCells;
    vector<vector<vector<unsigned int> > > EdgesForCells;

    vector<double> fval;
    vector<double> UxxPUyy;


    //vector<double> MeanHight;
    //vector<double> TrangulesResidualErrors;
    //vector<vector<double> > TrangulesCenters;

    //---------
    vector<double> solution;
    PetscInt SolutSize;
    VecGetSize(m_solut, &SolutSize);
    for(PetscInt Ipetsc = 0; Ipetsc < SolutSize; Ipetsc++)
    {
        PetscScalar v;
        VecGetValues(m_solut,1,&Ipetsc,&v);
        solution.push_back(v);
    }

    MSplineFunction * P_FEMSolution=new MSplineFunction(solution, *P_FEMMesh);//The Solution of FEM

    for (int i=0;i<P_FEMMesh->P_Cells->size();i++)
    {
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];

        double dx=(x1-x0)/(n_u-1.0);
        double dy=(y1-y0)/(n_v-1.0);


        vector<Coordinate*> Pcoordinate_Ps;
        for (int u_index=0; u_index < n_u; u_index++)
        {
            for (int v_index=0; v_index < n_v; v_index++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=u_index*dx+x0;//
                CoorP->xy[1]=v_index*dy+y0;//
                Pcoordinate_Ps.push_back(CoorP);
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        //f(x,y)
        ComputeFunval_MSplineParametricMap(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent,
                                           fval, 1, *P_ParametricMesh, P_MSplineParametricMap, ExampleIndex);
        //Uxx+Uyy
        DUxxValue_PLUS_DUyyValue(P_MSplineParametricMap->P_MSplineFunctions[0], P_MSplineParametricMap->P_MSplineFunctions[1],
                                 P_FEMSolution, Pcoordinate_Ps, UxxPUyy, i, P_FEMMesh, P_ParametricMesh);


        //=============================
        vector<vector<double> > ValuesXYZ;
        P_MSplineParametricMap->Evaluation(0, 0, Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent, ValuesXYZ, *P_ParametricMesh);
        //
        vector<vector<double> > VerticesForCelli;
        for(unsigned int iv=0; iv<ValuesXYZ[0].size(); iv++)
        {
            vector<double> Vi;
            Vi.push_back(ValuesXYZ[0][iv]);
            Vi.push_back(ValuesXYZ[1][iv]);
            if(UxxPUyy[iv]!=UxxPUyy[iv])
            {
                Vi.push_back(1.0);
            }
            else
            {
                if(abs_double(fval[iv]+UxxPUyy[iv])>1)
                {
                    Vi.push_back(1.0);//the domain with Vi More than 1, take 1 for visualization
                }
                else
                {
                    //Vi.push_back(fval[iv]+UxxPUyy[iv]);
                    Vi.push_back(abs_double(fval[iv]+UxxPUyy[iv]));
                }

            }

            VerticesForCelli.push_back(Vi);
            //VerticesSeq.push_back(Vi);//The position:(VerticesSeq[i][0], VerticesSeq[i][1])
        }
        nV=nV+VerticesForCelli.size();
        VerticesForCells.push_back(VerticesForCelli);

        //====Faces==
        vector<vector<unsigned int> > FacesForCelli;
        unsigned cell_index=i;
        //For Faces(triangules)
        for(int u_index=0; u_index < n_u-1; u_index++)
        {
            for(int v_index=0; v_index < n_v-1; v_index++)
            {
                vector<unsigned int> Fi;
                Fi.push_back(u_index*n_v+v_index+n_u*n_v*cell_index);
                Fi.push_back(u_index*n_v+v_index+1+n_u*n_v*cell_index);
                Fi.push_back((u_index+1)*n_v+v_index+n_u*n_v*cell_index);
                FacesForCelli.push_back(Fi);

                Fi.clear();
                Fi.push_back(u_index*n_v+v_index+1+n_u*n_v*cell_index);
                Fi.push_back((u_index+1)*n_v+v_index+n_u*n_v*cell_index);
                Fi.push_back((u_index+1)*n_v+v_index+1+n_u*n_v*cell_index);
                FacesForCelli.push_back(Fi);
            };
        };
        nF=nF+FacesForCelli.size();
        FacesForCells.push_back(FacesForCelli);
        //Edges
        vector<vector<unsigned int> > EdgesForCelli;
        vector<unsigned int> Ei;
        //For Edges
        for(unsigned int ie=0; ie<n_v-1; ie++)
        {
            Ei.clear();
            Ei.push_back(ie+n_u*n_v*cell_index);
            Ei.push_back(ie+1+n_u*n_v*cell_index);
            EdgesForCelli.push_back(Ei);
        }
        for(int je = 0; je < n_u-1 ; je++)
        {
            Ei.clear();
            Ei.push_back(n_u-1+je*n_v+n_u*n_v*cell_index);
            Ei.push_back(n_u-1+(je+1)*n_v+n_u*n_v*cell_index);
            EdgesForCelli.push_back(Ei);
        }

        for(int ie = 0; ie < n_v-1 ; ie++)
        {
            Ei.clear();
            Ei.push_back((n_u-1)*n_v + (n_v-1-ie)+n_u*n_v*cell_index);
            Ei.push_back((n_u-1)*n_v + (n_v-1-(ie+1))+n_u*n_v*cell_index);
            EdgesForCelli.push_back(Ei);
        }

        for(int je = 0; je < n_u-1 ; je++)
        {
            Ei.clear();
            Ei.push_back((n_u-1-je)*n_v+n_u*n_v*cell_index);
            Ei.push_back((n_u-1-(je+1))*n_v+n_u*n_v*cell_index);
            EdgesForCelli.push_back(Ei);
        }
        nE=nE+EdgesForCelli.size();
        EdgesForCells.push_back(EdgesForCelli);
    }


    //--------------
    ofstream ofErrorDistribution;
    ofErrorDistribution.open("ResidualErrorsValuesDistributionAxelMesh.axl");
    ofErrorDistribution<<"<axl>"<<endl;
    ofErrorDistribution<<"<mesh>"<<endl;

    ofErrorDistribution<<"<count>"<<nV<<" "<<nE<<" "<<nF<<"</count>"<<endl;
    //Points
    ofErrorDistribution<<"<points>"<<endl;
    for(unsigned int iv1=0; iv1<VerticesForCells.size(); iv1++)
    {
        for(unsigned int iv2=0; iv2<VerticesForCells[iv1].size(); iv2++)
        {
            //The first Choice
            for(unsigned int iv3=0; iv3<VerticesForCells[iv1][iv2].size(); iv3++)
            {
                ofErrorDistribution<<VerticesForCells[iv1][iv2][iv3]<<" ";
            }
            ofErrorDistribution<<endl;
        }
    }
    ofErrorDistribution<<"</points>"<<endl;

    //===Faces
    ofErrorDistribution<<"<faces>"<<endl;
    for(unsigned int iF1=0; iF1<FacesForCells.size(); iF1++)
    {
        for(unsigned int iF2=0; iF2<FacesForCells[iF1].size(); iF2++)
        {

            ofErrorDistribution<<3<<" ";
            for(unsigned int iF3=0; iF3<FacesForCells[iF1][iF2].size();iF3++)
            {
                ofErrorDistribution<<FacesForCells[iF1][iF2][iF3]<<" ";
            }
            ofErrorDistribution<<endl;
            //
        }
    }
    ofErrorDistribution<<"</faces>"<<endl;

    ofErrorDistribution<<"<edges>"<<endl;
    for(unsigned int iE1=0; iE1<EdgesForCells.size(); iE1++)
    {
        for(unsigned int iE2=0; iE2<EdgesForCells[iE1].size(); iE2++)
        {
            ofErrorDistribution<<2<<" ";
            for(unsigned int iE3=0; iE3<EdgesForCells[iE1][iE2].size(); iE3++)
            {
                ofErrorDistribution<<EdgesForCells[iE1][iE2][iE3]<<" ";
            }
            ofErrorDistribution<<endl;
        }
    }
    ofErrorDistribution<<"</edges>"<<endl;

    ofErrorDistribution<<"</mesh>"<<endl;
    ofErrorDistribution<<"</axl>"<<endl;
    ofErrorDistribution.close();




//    //
//    ComputerMeanHightAndAreasForEachTrangules(VerticesForCells, FacesForCells, TrangulesCenters, MeanHight, TrangulesResidualErrors);
//    //
//    PrintAxelPlanes(TrangulesCenters, MeanHight, "MeanHight.axl");//TrangulesCenters are on the planar
//    PrintAxelPlanes(TrangulesCenters, TrangulesResidualErrors, "ResidualErrorOverEachCell.axl");
//    //
//    //Computer the total residual error
//    double TotalResidualError=0.0;
//    for(unsigned i=0; i<TrangulesResidualErrors.size(); i++)
//    {
//        TotalResidualError+=TrangulesResidualErrors[i];
//    }
//    //Print the total residual error
//    ofErrorDistribution.open("TotalResidualErrors.txt");
//    ofErrorDistribution<<TotalResidualError<<endl;
//    ofErrorDistribution.close();

}

static void ResidualError_PhysicalDomain_RefinedParametricMap(const vector<double>& u_gausspt, const vector<double> & v_gausspt,
                   double **w, const Vec & m_solut,
                   vector<double> &cell_errors, double &totol_Error,
                   const Mesh*  P_FEMMesh, Mesh* P_ParametricMesh,
                   MSplineFunctions * P_MSplineParametricMap, unsigned int ExampleIndex)
{
    unsigned int u_gaussnum=u_gausspt.size();
    unsigned int v_gaussnum=v_gausspt.size();

    vector<double> fval;
    vector<double> Jval;
    vector<double> UxxPUyy;

    //Residual error range--->area
    int N=1;

    int Sc=50;//should be k*10
    double de=1.0/(Sc*N); //e=de, 2de, ..., 10Nde=1

    vector<double> AreasDe(Sc*N,0); //AreasDe[i] is the sum of the Areas of the cells with
    //
    //---------
    ofstream ofErrorDistribution;
    //ofErrorDistribution.open("ResidualErrorsDistribution.axl");
    //ofErrorDistribution<<"<axl>"<<endl;
    //--------

    cell_errors.clear();
    cell_errors.resize(P_FEMMesh->P_Cells->size());
    totol_Error=0.0;

    vector<double> solution;
    PetscInt SolutSize;
    VecGetSize(m_solut, &SolutSize);
    for(PetscInt Ipetsc = 0; Ipetsc < SolutSize; Ipetsc++)
    {
        PetscScalar v;
        VecGetValues(m_solut,1,&Ipetsc,&v);
        solution.push_back(v);
    }

    MSplineFunction * P_FEMSolution=new MSplineFunction(solution, *P_FEMMesh);//The Solution of FEM

    for (int i=0;i<P_FEMMesh->P_Cells->size();i++)
    {
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];

        vector<Coordinate*> Pcoordinate_Ps;
        for (int k=0; k<u_gaussnum; k++)
        {
            for (int l=0; l<v_gaussnum; l++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=0.5*(x1+x0)+0.5*(x1-x0)*u_gausspt[k];//u[k];
                CoorP->xy[1]=0.5*(y1+y0)+0.5*(y1-y0)*v_gausspt[l];//v[l];(y0+y1+(y1-y0)*gausspt[k]) * 0.5;
                Pcoordinate_Ps.push_back(CoorP);
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        //f(x,y)
        ComputeFunval_MSplineParametricMap(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent,
                                           fval, 1, *P_ParametricMesh, P_MSplineParametricMap, ExampleIndex);
        //J(x,y)
        JacobianValue(P_MSplineParametricMap->P_MSplineFunctions[0],P_MSplineParametricMap->P_MSplineFunctions[1],
                      Pcoordinate_Ps, Jval, P_FEMMesh->P_Cells->at(i).ItsParent,
                      P_FEMMesh->P_Cells->at(i).ItsParent, P_ParametricMesh, P_ParametricMesh);
        //Uxx+Uyy
        DUxxValue_PLUS_DUyyValue(P_MSplineParametricMap->P_MSplineFunctions[0], P_MSplineParametricMap->P_MSplineFunctions[1],
                                 P_FEMSolution, Pcoordinate_Ps, UxxPUyy, i, P_FEMMesh, P_ParametricMesh);

        //============================


        //=============================

        vector<vector<double> > ValuesXYZ;
        P_MSplineParametricMap->Evaluation(0, 0, Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent, ValuesXYZ, *P_ParametricMesh);


        //double CellCenterPointX=0.0;
        //double CellCenterPointY=0.0;

        cell_errors[i] = 0;

        //vector<double> AreaDei();
        double value_key(0.0);
        double Areai(0.0);

        for(int j=0;j<u_gaussnum*v_gaussnum;j++)
        {
            //-----------
            //CellCenterPointX=CellCenterPointX+ValuesXYZ[0][j];
            //CellCenterPointY=CellCenterPointY+ValuesXYZ[1][j];
            //==================

            if(Jval[j]<0)
            {
                Jval[j]=-Jval[j];
            }

            int nn = j / v_gaussnum;
            int mm = j % v_gaussnum;

            cell_errors[i] += w[nn][mm]*pow(fval[j]+UxxPUyy[j],2)*Jval[j];

            value_key=value_key+w[nn][mm]*abs_double(fval[j]+UxxPUyy[j]);
            //value_key=value_key+abs_double(fval[j]+UxxPUyy[j]);
            Areai=Areai+w[nn][mm]*Jval[j];
        }

        value_key=value_key/Areai;
        Areai=Areai*(y1-y0)*(x1-x0)/4.0;

        //value_key=value_key/(u_gaussnum*v_gaussnum*1.0);

        for(unsigned errorIndex=0; errorIndex<AreasDe.size(); errorIndex++)
        {
            if(value_key<=(errorIndex+1)*de)
            {
                AreasDe[errorIndex]+=Areai;
            }
        }

        //-------------
        //CellCenterPointX=CellCenterPointX/(1.0*u_gaussnum*v_gaussnum);
        //CellCenterPointY=CellCenterPointY/(1.0*u_gaussnum*v_gaussnum);
        //=======================================================
        totol_Error = totol_Error+cell_errors[i]*(y1-y0)*(x1-x0)/4.0;
        //=======================================================
        cell_errors[i] = sqrt(cell_errors[i]*(y1-y0)*(x1-x0)/4.0);
        //cout<<"Errors at "<<i<<"-th cell = "<<cell_errors[i]<<endl;

        //---------
        //ofErrorDistribution<<"<sphere size=\"0.01\" shader=\"\" name=\"axlSphere\" color=\"225 0 0 1\">"<<endl;
        //ofErrorDistribution<<"<center>"<<CellCenterPointX<<" "<<CellCenterPointY<<" "<<cell_errors[i]<<" </center>"<<endl;
        //ofErrorDistribution<<"<radius>"<< 0.01<<"</radius>"<<endl;
        //ofErrorDistribution<<"</sphere>"<<endl;

    }
    totol_Error = sqrt(totol_Error);

    //-----
    //ofErrorDistribution<<"</axl>"<<endl;
    //ofErrorDistribution.close();

    ofErrorDistribution.open("errorToleranceAndAreas.txt");
    for(unsigned errorIndex=0; errorIndex<AreasDe.size(); errorIndex++)
    {
        ofErrorDistribution<<(1.0+errorIndex)*de<<"  "<<AreasDe[errorIndex]<<endl;
    }

    ofErrorDistribution.close();

}

static void ResidualError_PhysicalDomain_RefinedParametricMap_PDE_PFEM(const vector<double>& u_gausspt, const vector<double> & v_gausspt,
                   double **w, const Vec & m_solut,
                   vector<double> &cell_errors, double &totol_Error,
                   const Mesh*  P_FEMMesh, Mesh* P_ParametricMesh,
                   MSplineFunctions * P_MSplineParametricMap,
                   unsigned int ExampleIndex, unsigned int EquationIndex)
{
    unsigned int u_gaussnum=u_gausspt.size();
    unsigned int v_gaussnum=v_gausspt.size();

    vector<double> fval;
    vector<double> Jval;
    vector<double> UxxPUyy;

    cell_errors.clear();
    cell_errors.resize(P_FEMMesh->P_Cells->size());
    totol_Error=0.0;

    vector<double> solution;
    PetscInt SolutSize;
    VecGetSize(m_solut, &SolutSize);
    for(PetscInt Ipetsc = 0; Ipetsc < SolutSize; Ipetsc++)
    {
        PetscScalar v;
        VecGetValues(m_solut,1,&Ipetsc,&v);
        solution.push_back(v);
    }

    MSplineFunction * P_FEMSolution=new MSplineFunction(solution, *P_FEMMesh);//The Solution of FEM

    for (int i=0;i<P_FEMMesh->P_Cells->size();i++)
    {
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];

        vector<Coordinate*> Pcoordinate_Ps;
        for (int k=0; k<u_gaussnum; k++)
        {
            for (int l=0; l<v_gaussnum; l++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=0.5*(x1+x0)+0.5*(x1-x0)*u_gausspt[k];//u[k];
                CoorP->xy[1]=0.5*(y1+y0)+0.5*(y1-y0)*v_gausspt[l];//v[l];(y0+y1+(y1-y0)*gausspt[k]) * 0.5;
                Pcoordinate_Ps.push_back(CoorP);
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        //f(x,y)
        ComputeFunval_MSplineParametricMap_PDE_PFEM(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent,
                                           fval, 1, *P_ParametricMesh, P_MSplineParametricMap,
                                           ExampleIndex, EquationIndex);
        //J(x,y)
        JacobianValue(P_MSplineParametricMap->P_MSplineFunctions[0],P_MSplineParametricMap->P_MSplineFunctions[1],
                      Pcoordinate_Ps, Jval, P_FEMMesh->P_Cells->at(i).ItsParent,
                      P_FEMMesh->P_Cells->at(i).ItsParent, P_ParametricMesh, P_ParametricMesh);
        //Uxx+Uyy
        DUxxValue_PLUS_DUyyValue(P_MSplineParametricMap->P_MSplineFunctions[0], P_MSplineParametricMap->P_MSplineFunctions[1],
                                 P_FEMSolution, Pcoordinate_Ps, UxxPUyy, i, P_FEMMesh, P_ParametricMesh);


        //=============================

        vector<vector<double> > ValuesXYZ;
        P_MSplineParametricMap->Evaluation(0, 0, Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent, ValuesXYZ, *P_ParametricMesh);

        cell_errors[i] = 0;
        for(int j=0;j<u_gaussnum*v_gaussnum;j++)
        {
            //==================
            if(Jval[j]<0)
            {
                Jval[j]=-Jval[j];
            }

            int nn = j / v_gaussnum;
            int mm = j % v_gaussnum;
            cell_errors[i] += w[nn][mm]*pow(fval[j]+UxxPUyy[j],2)*Jval[j];
        }
        //=======================================================
        totol_Error = totol_Error+cell_errors[i]*(y1-y0)*(x1-x0)/4;
        //=======================================================
        cell_errors[i] = sqrt(cell_errors[i] *(y1-y0) * (x1-x0)/4);
        //cout<<"Errors at "<<i<<"-th cell = "<<cell_errors[i]<<endl;
    }
    totol_Error = sqrt(totol_Error);
}
//Energy norm error on mesh
static void EnergyNormError_Mesh_RefinedParametricMap(const vector<double>& u_gausspt, const vector<double> & v_gausspt,
                   double **w, const Vec & m_solut,
                   vector<double> &cell_errors, double &totol_Error,
                   const Mesh*  P_FEMMesh, Mesh* P_ParametricMesh,
                   MSplineFunctions * P_MSplineParametricMap, unsigned int ExampleIndex)
{
    unsigned int u_gaussnum=u_gausspt.size();
    unsigned int v_gaussnum=v_gausspt.size();

    vector<double> ExacSou_x;
    vector<double> ExacSou_y;

    //vector<double> Jval;
    vector<double> FEMSou_x;
    vector<double> FEMSou_y;

    cell_errors.clear();
    cell_errors.resize(P_FEMMesh->P_Cells->size());
    totol_Error=0.0;

    vector<double> solution;
    PetscInt SolutSize;
    VecGetSize(m_solut, &SolutSize);
    for(PetscInt Ipetsc = 0; Ipetsc < SolutSize; Ipetsc++)
    {
        PetscScalar v;
        VecGetValues(m_solut,1,&Ipetsc,&v);
        solution.push_back(v);
    }

    MSplineFunction * P_FEMSolution=new MSplineFunction(solution, *P_FEMMesh);//The Solution of FEM

    for (int i=0;i<P_FEMMesh->P_Cells->size();i++)
    {
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];

        vector<Coordinate*> Pcoordinate_Ps;
        for (int k=0; k<u_gaussnum; k++)
        {
            for (int l=0; l<v_gaussnum; l++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=0.5*(x1+x0)+0.5*(x1-x0)*u_gausspt[k];//u[k];
                CoorP->xy[1]=0.5*(y1+y0)+0.5*(y1-y0)*v_gausspt[l];//v[l];(y0+y1+(y1-y0)*gausspt[k]) * 0.5;
                Pcoordinate_Ps.push_back(CoorP);
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        //Fx
        ComputeFunval_MSplineParametricMap(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent,
                                           ExacSou_x, 2, *P_ParametricMesh, P_MSplineParametricMap, ExampleIndex);
        //Fy
        ComputeFunval_MSplineParametricMap(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent,
                                           ExacSou_y, 3, *P_ParametricMesh, P_MSplineParametricMap, ExampleIndex);
//        //J(x,y)
//        JacobianValue(P_MSplineParametricMap->P_MSplineFunctions[0],P_MSplineParametricMap->P_MSplineFunctions[1],
//                      Pcoordinate_Ps, Jval, P_FEMMesh->P_Cells->at(i).ItsParent,
//                      P_FEMMesh->P_Cells->at(i).ItsParent, P_ParametricMesh, P_ParametricMesh);
        //FEMSou_x

        DUx_AND_DUy(P_MSplineParametricMap->P_MSplineFunctions[0],P_MSplineParametricMap->P_MSplineFunctions[1],
                    P_FEMSolution, Pcoordinate_Ps, FEMSou_x, FEMSou_y, i, P_FEMMesh, P_ParametricMesh);

        //FEMSou_y

        cell_errors[i] = 0;
        for(int j=0;j<u_gaussnum*v_gaussnum;j++)
        {
            int nn = j / v_gaussnum;
            int mm = j % v_gaussnum;
            //cell_errors[i] += w[nn][mm]*pow(fval[j]+UxxPUyy[j],2);
            cell_errors[i] += w[nn][mm]*((ExacSou_x[j]-FEMSou_x[j])*(ExacSou_x[j]-FEMSou_x[j])+(ExacSou_y[j]-FEMSou_y[j])*((ExacSou_y[j]-FEMSou_y[j])));
        }
        //=======================================================
        totol_Error = totol_Error+cell_errors[i]*(y1-y0)*(x1-x0)/4;
        //=======================================================
        cell_errors[i] = sqrt(cell_errors[i] *(y1-y0) * (x1-x0)/4);
        //cout<<"Errors at "<<i<<"-th cell = "<<cell_errors[i]<<endl;
    }
    totol_Error = sqrt(totol_Error);
}

//Energy norm error on mesh
static void EnergyNormError_Mesh_RefinedParametricMap_PDE_PFEM(const vector<double>& u_gausspt, const vector<double> & v_gausspt,
                   double **w, const Vec & m_solut,
                   vector<double> &cell_errors, double &totol_Error,
                   const Mesh*  P_FEMMesh, Mesh* P_ParametricMesh,
                   MSplineFunctions * P_MSplineParametricMap,
                   unsigned int ExampleIndex, unsigned int EquationIndex)
{
    unsigned int u_gaussnum=u_gausspt.size();
    unsigned int v_gaussnum=v_gausspt.size();

    vector<double> ExacSou_x;
    vector<double> ExacSou_y;

    //vector<double> Jval;
    vector<double> FEMSou_x;
    vector<double> FEMSou_y;

    cell_errors.clear();
    cell_errors.resize(P_FEMMesh->P_Cells->size());
    totol_Error=0.0;

    vector<double> solution;
    PetscInt SolutSize;
    VecGetSize(m_solut, &SolutSize);
    for(PetscInt Ipetsc = 0; Ipetsc < SolutSize; Ipetsc++)
    {
        PetscScalar v;
        VecGetValues(m_solut,1,&Ipetsc,&v);
        solution.push_back(v);
    }

    MSplineFunction * P_FEMSolution=new MSplineFunction(solution, *P_FEMMesh);//The Solution of FEM

    for (int i=0;i<P_FEMMesh->P_Cells->size();i++)
    {
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];

        vector<Coordinate*> Pcoordinate_Ps;
        for (int k=0; k<u_gaussnum; k++)
        {
            for (int l=0; l<v_gaussnum; l++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=0.5*(x1+x0)+0.5*(x1-x0)*u_gausspt[k];//u[k];
                CoorP->xy[1]=0.5*(y1+y0)+0.5*(y1-y0)*v_gausspt[l];//v[l];(y0+y1+(y1-y0)*gausspt[k]) * 0.5;
                Pcoordinate_Ps.push_back(CoorP);
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        //Fx
        ComputeFunval_MSplineParametricMap_PDE_PFEM(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent,
                                           ExacSou_x, 2, *P_ParametricMesh, P_MSplineParametricMap,
                                                    ExampleIndex, EquationIndex);
        //Fy
        ComputeFunval_MSplineParametricMap_PDE_PFEM(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent,
                                           ExacSou_y, 3, *P_ParametricMesh, P_MSplineParametricMap,
                                                    ExampleIndex, EquationIndex);
//        //J(x,y)
//        JacobianValue(P_MSplineParametricMap->P_MSplineFunctions[0],P_MSplineParametricMap->P_MSplineFunctions[1],
//                      Pcoordinate_Ps, Jval, P_FEMMesh->P_Cells->at(i).ItsParent,
//                      P_FEMMesh->P_Cells->at(i).ItsParent, P_ParametricMesh, P_ParametricMesh);
        //FEMSou_x

        DUx_AND_DUy(P_MSplineParametricMap->P_MSplineFunctions[0],P_MSplineParametricMap->P_MSplineFunctions[1],
                    P_FEMSolution, Pcoordinate_Ps, FEMSou_x, FEMSou_y, i, P_FEMMesh, P_ParametricMesh);

        //FEMSou_y

        cell_errors[i] = 0;
        for(int j=0;j<u_gaussnum*v_gaussnum;j++)
        {
            int nn = j / v_gaussnum;
            int mm = j % v_gaussnum;
            //cell_errors[i] += w[nn][mm]*pow(fval[j]+UxxPUyy[j],2);
            cell_errors[i] += w[nn][mm]*((ExacSou_x[j]-FEMSou_x[j])*(ExacSou_x[j]-FEMSou_x[j])+(ExacSou_y[j]-FEMSou_y[j])*((ExacSou_y[j]-FEMSou_y[j])));
        }
        //=======================================================
        totol_Error = totol_Error+cell_errors[i]*(y1-y0)*(x1-x0)/4;
        //=======================================================
        cell_errors[i] = sqrt(cell_errors[i] *(y1-y0) * (x1-x0)/4);
        //cout<<"Errors at "<<i<<"-th cell = "<<cell_errors[i]<<endl;
    }
    totol_Error = sqrt(totol_Error);
}
//=====================================================
//Energy norm error on mesh
static void EnergyNormError_PhyscialDomain_RefinedParametricMap(const vector<double>& u_gausspt, const vector<double> & v_gausspt,
                   double **w, const Vec & m_solut,
                   vector<double> &cell_errors, double &totol_Error,
                   const Mesh*  P_FEMMesh, Mesh* P_ParametricMesh,
                   MSplineFunctions * P_MSplineParametricMap, unsigned int ExampleIndex)
{
    unsigned int u_gaussnum=u_gausspt.size();
    unsigned int v_gaussnum=v_gausspt.size();

    vector<double> ExacSou_x;
    vector<double> ExacSou_y;

    vector<double> Jval;
    vector<double> FEMSou_x;
    vector<double> FEMSou_y;

    cell_errors.clear();
    cell_errors.resize(P_FEMMesh->P_Cells->size());
    totol_Error=0.0;

    vector<double> solution;
    PetscInt SolutSize;
    VecGetSize(m_solut, &SolutSize);
    for(PetscInt Ipetsc = 0; Ipetsc < SolutSize; Ipetsc++)
    {
        PetscScalar v;
        VecGetValues(m_solut,1,&Ipetsc,&v);
        solution.push_back(v);
    }

    MSplineFunction * P_FEMSolution=new MSplineFunction(solution, *P_FEMMesh);//The Solution of FEM

    for (int i=0;i<P_FEMMesh->P_Cells->size();i++)
    {
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];

        vector<Coordinate*> Pcoordinate_Ps;
        for (int k=0; k<u_gaussnum; k++)
        {
            for (int l=0; l<v_gaussnum; l++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=0.5*(x1+x0)+0.5*(x1-x0)*u_gausspt[k];//u[k];
                CoorP->xy[1]=0.5*(y1+y0)+0.5*(y1-y0)*v_gausspt[l];//v[l];(y0+y1+(y1-y0)*gausspt[k]) * 0.5;
                Pcoordinate_Ps.push_back(CoorP);
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        //Fx
        ComputeFunval_MSplineParametricMap(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent,
                                           ExacSou_x, 2, *P_ParametricMesh, P_MSplineParametricMap, ExampleIndex);
        //Fy
        ComputeFunval_MSplineParametricMap(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent,
                                           ExacSou_y, 3, *P_ParametricMesh, P_MSplineParametricMap, ExampleIndex);
        //J(x,y)
        JacobianValue(P_MSplineParametricMap->P_MSplineFunctions[0],P_MSplineParametricMap->P_MSplineFunctions[1],
                      Pcoordinate_Ps, Jval, P_FEMMesh->P_Cells->at(i).ItsParent,
                      P_FEMMesh->P_Cells->at(i).ItsParent, P_ParametricMesh, P_ParametricMesh);
        //FEMSou_x
        DUx_AND_DUy(P_MSplineParametricMap->P_MSplineFunctions[0],P_MSplineParametricMap->P_MSplineFunctions[1],
                    P_FEMSolution, Pcoordinate_Ps, FEMSou_x, FEMSou_y, i, P_FEMMesh, P_ParametricMesh);

        //FEMSou_y

        cell_errors[i] = 0;
        for(int j=0;j<u_gaussnum*v_gaussnum;j++)
        {
            int nn = j / v_gaussnum;
            int mm = j % v_gaussnum;

            if(Jval[j]<0)
            {
                Jval[j]=-Jval[j];
            }

            cell_errors[i] += w[nn][mm]*((ExacSou_x[j]-FEMSou_x[j])*(ExacSou_x[j]-FEMSou_x[j])+(ExacSou_y[j]-FEMSou_y[j])*((ExacSou_y[j]-FEMSou_y[j])))*Jval[j];
        }
        //=======================================================
        totol_Error = totol_Error+cell_errors[i]*(y1-y0)*(x1-x0)/4;
        //=======================================================
        cell_errors[i] = sqrt(cell_errors[i] *(y1-y0) * (x1-x0)/4);
        //cout<<"Errors at "<<i<<"-th cell = "<<cell_errors[i]<<endl;
    }
    totol_Error = sqrt(totol_Error);
}

//Energy norm error on mesh
static void EnergyNormError_PhyscialDomain_RefinedParametricMap_PDE_PFEM(const vector<double>& u_gausspt, const vector<double> & v_gausspt,
                   double **w, const Vec & m_solut,
                   vector<double> &cell_errors, double &totol_Error,
                   const Mesh*  P_FEMMesh, Mesh* P_ParametricMesh,
                   MSplineFunctions * P_MSplineParametricMap,
                   unsigned int ExampleIndex, unsigned int EquationIndex)
{
    unsigned int u_gaussnum=u_gausspt.size();
    unsigned int v_gaussnum=v_gausspt.size();

    vector<double> ExacSou_x;
    vector<double> ExacSou_y;

    vector<double> Jval;
    vector<double> FEMSou_x;
    vector<double> FEMSou_y;

    cell_errors.clear();
    cell_errors.resize(P_FEMMesh->P_Cells->size());
    totol_Error=0.0;

    vector<double> solution;
    PetscInt SolutSize;
    VecGetSize(m_solut, &SolutSize);
    for(PetscInt Ipetsc = 0; Ipetsc < SolutSize; Ipetsc++)
    {
        PetscScalar v;
        VecGetValues(m_solut,1,&Ipetsc,&v);
        solution.push_back(v);
    }

    MSplineFunction * P_FEMSolution=new MSplineFunction(solution, *P_FEMMesh);//The Solution of FEM

    for (int i=0;i<P_FEMMesh->P_Cells->size();i++)
    {
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];

        vector<Coordinate*> Pcoordinate_Ps;
        for (int k=0; k<u_gaussnum; k++)
        {
            for (int l=0; l<v_gaussnum; l++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=0.5*(x1+x0)+0.5*(x1-x0)*u_gausspt[k];//u[k];
                CoorP->xy[1]=0.5*(y1+y0)+0.5*(y1-y0)*v_gausspt[l];//v[l];(y0+y1+(y1-y0)*gausspt[k]) * 0.5;
                Pcoordinate_Ps.push_back(CoorP);
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        //Fx
        ComputeFunval_MSplineParametricMap_PDE_PFEM(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent,
                                           ExacSou_x, 2, *P_ParametricMesh, P_MSplineParametricMap, ExampleIndex, EquationIndex);
        //Fy
        ComputeFunval_MSplineParametricMap_PDE_PFEM(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent,
                                           ExacSou_y, 3, *P_ParametricMesh, P_MSplineParametricMap,
                                           ExampleIndex, EquationIndex);
        //J(x,y)
        JacobianValue(P_MSplineParametricMap->P_MSplineFunctions[0],P_MSplineParametricMap->P_MSplineFunctions[1],
                      Pcoordinate_Ps, Jval, P_FEMMesh->P_Cells->at(i).ItsParent,
                      P_FEMMesh->P_Cells->at(i).ItsParent, P_ParametricMesh, P_ParametricMesh);
        //FEMSou_x

        DUx_AND_DUy(P_MSplineParametricMap->P_MSplineFunctions[0],P_MSplineParametricMap->P_MSplineFunctions[1],
                    P_FEMSolution, Pcoordinate_Ps, FEMSou_x, FEMSou_y, i, P_FEMMesh, P_ParametricMesh);

        //FEMSou_y

        cell_errors[i] = 0;
        for(int j=0;j<u_gaussnum*v_gaussnum;j++)
        {
            int nn = j / v_gaussnum;
            int mm = j % v_gaussnum;

            if(Jval[j]<0)
            {
                Jval[j]=-Jval[j];
            }

            cell_errors[i] += w[nn][mm]*((ExacSou_x[j]-FEMSou_x[j])*(ExacSou_x[j]-FEMSou_x[j])+(ExacSou_y[j]-FEMSou_y[j])*((ExacSou_y[j]-FEMSou_y[j])))*Jval[j];
        }
        //=======================================================
        totol_Error = totol_Error+cell_errors[i]*(y1-y0)*(x1-x0)/4;
        //=======================================================
        cell_errors[i] = sqrt(cell_errors[i] *(y1-y0) * (x1-x0)/4);
        //cout<<"Errors at "<<i<<"-th cell = "<<cell_errors[i]<<endl;
    }
    totol_Error = sqrt(totol_Error);
}

//=====================================================
//L2 error on the physical Domain
static void ComputError_MSplineParametricMap_OnPhysicalDomain(const vector<double>& u_gausspt, const vector<double> & v_gausspt,
                                                              double **w, const Vec & m_solut,
                                                       vector<double> &cell_errors, double &totol_Error,
                                                        const Mesh*  P_FEMMesh, const Mesh * P_ParametricMesh,
                                                       MSplineFunctions * P_MSplineParametricMap, unsigned int ExampleIndex)
{
    ofstream ofJacobian;
    ofJacobian.open("Jacobian.txt");
    double MaxMaxJ=-100.0;
    double MinMinJ=100.0;

    unsigned int u_gaussnum=u_gausspt.size();
    unsigned int v_gaussnum=v_gausspt.size();

    //cout<<"u_gausspt.size()="<<u_gausspt.size();
    //cout<<"v_gausspt.size()="<<v_gausspt.size();
    //cin.get();

    vector<double> temp_Error;
    vector<double> exactval;
    vector<double> Jval;

    cell_errors.clear();
    cell_errors.resize(P_FEMMesh->P_Cells->size());
    totol_Error=0.0;

    vector<double> solution;
    PetscInt SolutSize;
    VecGetSize(m_solut, &SolutSize);
    for(PetscInt Ipetsc = 0; Ipetsc < SolutSize; Ipetsc++)
    {
        PetscScalar v;
        VecGetValues(m_solut,1,&Ipetsc,&v);
        solution.push_back(v);
    }

    MSplineFunction * P_FEMSolution=new MSplineFunction(solution, *P_FEMMesh);//The Solution of FEM

    for (int i=0;i<P_FEMMesh->P_Cells->size();i++)
    {
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];

        vector<Coordinate*> Pcoordinate_Ps;
        for (int k=0; k<u_gaussnum; k++)
        {
            for (int l=0; l<v_gaussnum; l++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=0.5*(x1+x0)+0.5*(x1-x0)*u_gausspt[k];//u[k];
                CoorP->xy[1]=0.5*(y1+y0)+0.5*(y1-y0)*v_gausspt[l];//v[l];(y0+y1+(y1-y0)*gausspt[k]) * 0.5;
                Pcoordinate_Ps.push_back(CoorP);
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        ComputeFunval_MSplineParametricMap(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent, exactval,
                                           0, *P_ParametricMesh, P_MSplineParametricMap, ExampleIndex);


        P_FEMSolution->Evaluation(0, 0, Pcoordinate_Ps, temp_Error, i, *P_FEMMesh);

//        P_MSplineParametricMap->Cout_MSplineFunctions();
//        cin.get();

        //Jacobian
        JacobianValue(P_MSplineParametricMap->P_MSplineFunctions[0], P_MSplineParametricMap->P_MSplineFunctions[1],
                      Pcoordinate_Ps,Jval,P_FEMMesh->P_Cells->at(i).ItsParent,P_FEMMesh->P_Cells->at(i).ItsParent,
                      P_ParametricMesh,P_ParametricMesh);


        cell_errors[i] = 0.0;

        double maxJ=-100.0;
        double minJ=100.0;

        for(int j=0;j<u_gaussnum*v_gaussnum;j++)
        {
            //cout<<"L2 error on the physical Domain::Jacobian["<<j<<"] = "<<Jval[j]<<endl;
            if(Jval[j]>maxJ)
            {
                maxJ=Jval[j];
            }
            if(Jval[j]<minJ)
            {
                minJ=Jval[j];
            }
            if(Jval[j]>MaxMaxJ)
            {
                MaxMaxJ=Jval[j];
            }
            if(Jval[j]<MinMinJ)
            {
                MinMinJ=Jval[j];
            }


            if(Jval[j]<0)
            {
                Jval[j]=-Jval[j];
            }

            int nn = j / v_gaussnum;
            int mm = j % v_gaussnum;
            cell_errors[i] += w[nn][mm]*pow(exactval[j]-temp_Error[j],2)*Jval[j];

            //cout<<"exactval-temp_Error["<<j<<"]="<<exactval[j]-temp_Error[j]<<endl;

        }


        ofJacobian<<"the"<<i<<"-th cell:"<<endl;
        ofJacobian<<"maxJ = "<<maxJ<<endl;
        ofJacobian<<"minJ = "<<minJ<<endl;


        //=======================================================
        totol_Error = totol_Error+cell_errors[i]*(y1-y0)*(x1-x0)/4;
        //=======================================================
        cell_errors[i] = sqrt(cell_errors[i] *(y1-y0) * (x1-x0)/4);
        //cout<<"Errors at "<<i<<"-th cell = "<<cell_errors[i]<<endl;
    }
    totol_Error = sqrt(totol_Error);

    ofJacobian<<endl;
    ofJacobian<<endl;

    ofJacobian<<"MaxMaxJ= "<<MaxMaxJ<<endl;
    ofJacobian<<"MinMinJ= "<<MinMinJ<<endl;

    ofJacobian.close();
}

//L2 predict_ error on the physical Domain
/*static void Predict_ComputError_MSplineParametricMap_OnPhysicalDomain(const vector<double>& u_gausspt, const vector<double> & v_gausspt,
                                                              double **w, const vector<double>& solution,
                                                       vector<double> &cell_errors, double &totol_Error,
                                                        unsigned int k, Mesh * P_ParametricMesh,
                                                        unsigned int ExampleIndex)*/
static void Predict_ComputError_MSplineParametricMap_OnPhysicalDomain(const vector<double>& u_gausspt, const vector<double> & v_gausspt,
                                                                      double **w, const vector<double>& solution,
                                                               vector<double> &cell_errors, double &totol_Error,
                                                                const Mesh*  P_FEMMesh, Mesh * P_ParametricMesh,
                                                               MSplineFunctions * P_MSplineParametricMap, unsigned int ExampleIndex, unsigned int SubExampleIndex)
{
    unsigned int u_gaussnum=u_gausspt.size();
    unsigned int v_gaussnum=v_gausspt.size();

    vector<double> temp_Error;
    vector<double> exactval;
    vector<double> Jval;

    cell_errors.clear();
    cell_errors.resize(P_FEMMesh->P_Cells->size());
    totol_Error=0.0;



    MSplineFunction * P_FEMSolution=new MSplineFunction(solution, *P_FEMMesh);//The Solution of FEM

    vector<vector<double> > ExactSolution;
    Mesh * P_ExactSoluMesh=new Mesh();

    map<unsigned int, unsigned int> OrigialVertexToCurrentVertex;

    switch(ExampleIndex)
    {
    case 46:
        switch(SubExampleIndex)
        {

        case 1:
            //Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(k, OrigialVertexToCurrentVertex);
            P_ExactSoluMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(40,OrigialVertexToCurrentVertex);
            map<unsigned int, unsigned int> (OrigialVertexToCurrentVertex).swap(OrigialVertexToCurrentVertex);

            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_LShape/1/FEMSolving/k=40/FEMSolution.txt",
                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);

            break;
        case 5:
            //Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(k, OrigialVertexToCurrentVertex);
            P_ExactSoluMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(36,OrigialVertexToCurrentVertex);
            map<unsigned int, unsigned int> (OrigialVertexToCurrentVertex).swap(OrigialVertexToCurrentVertex);

            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_LShape/5/FEMSolving/k=36/FEMSolution.txt",
                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);
            break;
        case 6:
            //Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(k, OrigialVertexToCurrentVertex);
            P_ExactSoluMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(36,OrigialVertexToCurrentVertex);
            map<unsigned int, unsigned int> (OrigialVertexToCurrentVertex).swap(OrigialVertexToCurrentVertex);

            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_LShape/6/FEMSolving/k=36/FEMSolution.txt",
                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);
            break;

        case 8:
            //Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(k, OrigialVertexToCurrentVertex);
            P_ExactSoluMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(36,OrigialVertexToCurrentVertex);
            map<unsigned int, unsigned int> (OrigialVertexToCurrentVertex).swap(OrigialVertexToCurrentVertex);

            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_LShape/8/FEMSolving/k=36/FEMSolution.txt",
                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);
            break;

        case 2:
            //Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(k, OrigialVertexToCurrentVertex);
            P_ExactSoluMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(50,OrigialVertexToCurrentVertex);
            map<unsigned int, unsigned int> (OrigialVertexToCurrentVertex).swap(OrigialVertexToCurrentVertex);

            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_LShape/2/FEMSolving/k=50/FEMSolution.txt",
                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);
            break;
        case 3:
            //Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(k, OrigialVertexToCurrentVertex);
            P_ExactSoluMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(50,OrigialVertexToCurrentVertex);
            map<unsigned int, unsigned int> (OrigialVertexToCurrentVertex).swap(OrigialVertexToCurrentVertex);

            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_LShape/3/FEMSolving/k=50/FEMSolution.txt",
                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);
            break;
        case 4:
            //Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(k, OrigialVertexToCurrentVertex);
            P_ExactSoluMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(50,OrigialVertexToCurrentVertex);
            map<unsigned int, unsigned int> (OrigialVertexToCurrentVertex).swap(OrigialVertexToCurrentVertex);

            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_LShape/4/FEMSolving/k=50/FEMSolution.txt",
                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);
            break;

        case 7:
            //Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(k, OrigialVertexToCurrentVertex);
            P_ExactSoluMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(50,OrigialVertexToCurrentVertex);
            map<unsigned int, unsigned int> (OrigialVertexToCurrentVertex).swap(OrigialVertexToCurrentVertex);

            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_LShape/7/FEMSolving/k=50/FEMSolution.txt",
                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);
            break;
        }
        break;

    case 47:
        switch(SubExampleIndex)
        {
        case 1:
            //Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(k, OrigialVertexToCurrentVertex);
            P_ExactSoluMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(63,OrigialVertexToCurrentVertex);
            map<unsigned int, unsigned int> (OrigialVertexToCurrentVertex).swap(OrigialVertexToCurrentVertex);

            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_Rectangular/1/FEMSolving/ExampleIndex47/k=63/FEMSolution.txt",
                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);
            break;
        case 2:
            //Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(k, OrigialVertexToCurrentVertex);
            P_ExactSoluMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(63,OrigialVertexToCurrentVertex);
            map<unsigned int, unsigned int> (OrigialVertexToCurrentVertex).swap(OrigialVertexToCurrentVertex);

            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_Rectangular/2/FEMSolving/ExampleIndex47/k=63/FEMSolution.txt",
                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);
            break;
        case 3:
            //Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(k, OrigialVertexToCurrentVertex);
            P_ExactSoluMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(63,OrigialVertexToCurrentVertex);
            map<unsigned int, unsigned int> (OrigialVertexToCurrentVertex).swap(OrigialVertexToCurrentVertex);

            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_Rectangular/3/FEMSolving/ExampleIndex47/k=63/FEMSolution.txt",
                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);
            break;
        }
        break;

    case 51:
    {
        switch (SubExampleIndex)
        {
        case 1://alpha=2.0; x=s^3cos(alpha*pi*t/2), y=s^3cos(alpha*pi*t/2)
            P_ExactSoluMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(50,OrigialVertexToCurrentVertex);
            map<unsigned int, unsigned int> (OrigialVertexToCurrentVertex).swap(OrigialVertexToCurrentVertex);

            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_fanShape/1/k=50/FEMSolution.txt",
                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);
            break;
            //===================
        case 2://alpha=3.0/2.0; x=s^3cos(alpha*pi*t/2), y=s^3cos(alpha*pi*t/2)
            P_ExactSoluMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(50,OrigialVertexToCurrentVertex);
            map<unsigned int, unsigned int> (OrigialVertexToCurrentVertex).swap(OrigialVertexToCurrentVertex);

            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_fanShape/2/k=50/FEMSolution.txt",
                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);
            break;
        case 3://alpha=2; x=s^{7/2}cos(alpha*pi*t/2), y=s^{7/2}cos(alpha*pi*t/2)
            P_ExactSoluMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(46,OrigialVertexToCurrentVertex);
            map<unsigned int, unsigned int> (OrigialVertexToCurrentVertex).swap(OrigialVertexToCurrentVertex);

            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_fanShape/3/k=46/FEMSolution.txt",
                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);
            break;
        case 4://alpha=2; x=s^{2}cos(alpha*pi*t/2), y=s^{2}cos(alpha*pi*t/2)
            P_ExactSoluMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(50,OrigialVertexToCurrentVertex);
            map<unsigned int, unsigned int> (OrigialVertexToCurrentVertex).swap(OrigialVertexToCurrentVertex);

//            //delta=2
//            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_fanShape/4/delta=2/k=50/FEMSolution.txt",
//                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);
//            //delta=1.2
//            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_fanShape/4/delta=1_2/k=50/FEMSolution.txt",
//                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);
 //           //delta=1.5
//            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_fanShape/4/delta=1_5/k=50/FEMSolution.txt",
//                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);

//            //delta=1.7
//            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_fanShape/4/delta=1_7/k=50/FEMSolution.txt",
//                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);
            //delta=2.5
            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_fanShape/4/delta=2_5/k=50/FEMSolution.txt",
                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);

            break;
        case 5://alpha=3/2; x=s*cos(alpha*pi*t/2), y=s*cos(alpha*pi*t/2)
            P_ExactSoluMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(50,OrigialVertexToCurrentVertex);
            map<unsigned int, unsigned int> (OrigialVertexToCurrentVertex).swap(OrigialVertexToCurrentVertex);

            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_fanShape/5/k=50/FEMSolution.txt",
                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);
            break;
        case 6://alpha=3/2; x=s^2*cos(alpha*pi*t/2), y=s^2*cos(alpha*pi*t/2)
            P_ExactSoluMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(50,OrigialVertexToCurrentVertex);
            map<unsigned int, unsigned int> (OrigialVertexToCurrentVertex).swap(OrigialVertexToCurrentVertex);

//            //delta=2.0
//            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_fanShape/6/delta=2/k=50/FEMSolution.txt",
//                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);

//            //delta=1.2
//            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_fanShape/6/delta=1_2/k=50/FEMSolution.txt",
//                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);
//            //delta=1.5
//            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_fanShape/6/delta=1_5/k=50/FEMSolution.txt",
//                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);
//            //delta=1.7
//            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_fanShape/6/delta=1_7/k=50/FEMSolution.txt",
//                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);
            //delta=2.5
            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_fanShape/6/delta=2_5/k=50/FEMSolution.txt",
                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);
            break;
            //==========================================
        default:
            break;
        }
    }
        break;

    case 52:

        switch (SubExampleIndex)
        {
        case 1:
            P_ExactSoluMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(40,OrigialVertexToCurrentVertex);
            map<unsigned int, unsigned int> (OrigialVertexToCurrentVertex).swap(OrigialVertexToCurrentVertex);

            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_CrackRectangular/1/FEMSolving/k=40/FEMSolution.txt",
                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);

            break;
        case 2:
            P_ExactSoluMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(40,OrigialVertexToCurrentVertex);
            map<unsigned int, unsigned int> (OrigialVertexToCurrentVertex).swap(OrigialVertexToCurrentVertex);

            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_CrackRectangular/2/FEMSolving/k=40/FEMSolution.txt",
                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);

            break;
        case 3:
            P_ExactSoluMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(40,OrigialVertexToCurrentVertex);
            map<unsigned int, unsigned int> (OrigialVertexToCurrentVertex).swap(OrigialVertexToCurrentVertex);

            Reader_Coeff_ParametricMap("/Users/wumeng/MyWork/msplinetools/data/Examples/SingularityTest_CrackRectangular/3/FEMSolving/k=40/FEMSolution.txt",
                                       1, P_ExactSoluMesh->P_Vertices->size(), ExactSolution);

            break;
        }
        break;


    }

    MSplineFunction *P_ExactSolution=new MSplineFunction(ExactSolution[0], *P_ExactSoluMesh);
    //


    vector<vector<double> > (ExactSolution).swap(ExactSolution);

    for (int i=0;i<P_FEMMesh->P_Cells->size();i++)
    {
        exactval.clear();
        temp_Error.clear();

        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];

        vector<Coordinate*> Pcoordinate_Ps;
        vector<unsigned int> PsIndex;
        PsIndex.resize(u_gaussnum*v_gaussnum,0);
        unsigned int Fg=0;
        unsigned int ParentIndex=P_FEMMesh->P_Cells->at(i).ItsParent;
        for (int k=0; k<u_gaussnum; k++)
        {
            for (int l=0; l<v_gaussnum; l++)
            {


                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=0.5*(x1+x0)+0.5*(x1-x0)*u_gausspt[k];//u[k];
                CoorP->xy[1]=0.5*(y1+y0)+0.5*(y1-y0)*v_gausspt[l];//v[l];(y0+y1+(y1-y0)*gausspt[k]) * 0.5;
                Pcoordinate_Ps.push_back(CoorP);

                for(unsigned int jc=0; jc<P_ExactSoluMesh->P_Cells->size(); jc++)
                {
                    if(P_ExactSoluMesh->P_Cells->at(jc).ItsParent==ParentIndex)
                    {
                        double XF0=P_ExactSoluMesh->P_Cells->at(jc).CellSize[0];
                        double XF1=P_ExactSoluMesh->P_Cells->at(jc).CellSize[1];
                        double YF0=P_ExactSoluMesh->P_Cells->at(jc).CellSize[2];
                        double YF1=P_ExactSoluMesh->P_Cells->at(jc).CellSize[3];
                        if(XF0<=CoorP->xy[0] && CoorP->xy[0]<=XF1 && YF0<=CoorP->xy[1] && CoorP->xy[1]<=YF1)
                        {
                            PsIndex[Fg]=jc;
                            break;
                        }
                    }
                }

                Fg=Fg+1;
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////

        for(unsigned int j=0; j<Pcoordinate_Ps.size(); j++)
        {
            //For each point in FEMMeshCell, find the cell index in Exactsolution
            vector<Coordinate*> Cd;
            Cd.push_back(Pcoordinate_Ps[j]);
            vector<double> vals;


            P_ExactSolution->Evaluation(0,0,Cd,vals,PsIndex[j], *P_ExactSoluMesh);

//            cout<<"("<<Cd[0]->xy[0]<<", "<<Cd[0]->xy[1]<<")"<<endl;
//            cout<<"vals.size()="<<vals.size()<<endl;
//            cout<<"FEMCellIndex="<<i<<endl;
//            cout<<"PsIndex[j]="<<PsIndex[j]<<endl;
//            cout<<"vals[0]="<<vals[0]<<endl;

            exactval.push_back(vals[0]);

        }

        //-----


        P_FEMSolution->Evaluation(0, 0, Pcoordinate_Ps, temp_Error, i, *P_FEMMesh);

//        P_MSplineParametricMap->Cout_MSplineFunctions();
//        cin.get();

        //Jacobian
        if(ExampleIndex==51)
        {
            vector<double> dXds, dYds;
            ParametricMapSigmaVal(1,0,Pcoordinate_Ps,dXds,dYds,ExampleIndex, SubExampleIndex);
            vector<double> dXdt, dYdt;
            ParametricMapSigmaVal(0,1,Pcoordinate_Ps,dXdt,dYdt,ExampleIndex, SubExampleIndex);

            Jval.clear();
            for(unsigned int iv=0; iv<dXds.size(); iv++)
            {
                Jval.push_back(dXds[iv]*dYdt[iv]-dXdt[iv]*dYds[iv]);//VXs*VYt-VXt*VYs
            }

        }
        else
        {
            JacobianValue(P_MSplineParametricMap->P_MSplineFunctions[0], P_MSplineParametricMap->P_MSplineFunctions[1],
                          Pcoordinate_Ps,Jval,P_FEMMesh->P_Cells->at(i).ItsParent,P_FEMMesh->P_Cells->at(i).ItsParent,
                          P_ParametricMesh,P_ParametricMesh);//the General case
        }
        /*switch (ExampleIndex)
        {
        case 51:
            vector<double> dXds, dYds;
            ParametricMapSigmaVal(1,0,Pcoordinate_Ps,dXds,dYds,ExampleIndex);
            vector<double> dXdt, dYdt;
            ParametricMapSigmaVal(0,1,Pcoordinate_Ps,dXdt,dYdt,ExampleIndex);

            Jval.clear();
            for(unsigned int iv=0; iv<dXds.size(); iv++)
            {
                Jval.push_back(dXds[iv]*dYdt[iv]-dXdt[iv]*dYds[iv]);//VXs*VYt-VXt*VYs
            }

            break;

        default:
            JacobianValue(P_MSplineParametricMap->P_MSplineFunctions[0], P_MSplineParametricMap->P_MSplineFunctions[1],
                          Pcoordinate_Ps,Jval,P_FEMMesh->P_Cells->at(i).ItsParent,P_FEMMesh->P_Cells->at(i).ItsParent,
                          P_ParametricMesh,P_ParametricMesh);//the General case
            break;
        }*/

        cell_errors[i] = 0.0;

        for(int j=0;j<u_gaussnum*v_gaussnum;j++)
        {
            int nn = j / v_gaussnum;
            int mm = j % v_gaussnum;
            cell_errors[i] += w[nn][mm]*pow(exactval[j]-temp_Error[j],2)*abs_double(Jval[j]);
        }



        //=======================================================
        totol_Error = totol_Error+cell_errors[i]*(y1-y0)*(x1-x0)/4;
        //=======================================================
        cell_errors[i] = sqrt(cell_errors[i] *(y1-y0) * (x1-x0)/4);
        //cout<<"Errors at "<<i<<"-th cell = "<<cell_errors[i]<<endl;
    }
    totol_Error = sqrt(totol_Error);



}

//L2 Error on Mesh (for PDE_PFEM)
static void ComputError_MSplineParametricMap_PDE_PFEM(const vector<double>& u_gausspt, const vector<double> & v_gausspt,double **w, const Vec & m_solut,
                                                    vector<double> &cell_errors, double &totol_Error,
                                                    const Mesh*  P_FEMMesh, const Mesh * P_ParametricMesh,
                                                    MSplineFunctions * P_MSplineParametricMap,
                                                    unsigned int ExampleIndex, unsigned int EquationIndex)
{
        unsigned int u_gaussnum=u_gausspt.size();
        unsigned int v_gaussnum=v_gausspt.size();

        vector<double> temp_Error;
        vector<double> exactval;

        cell_errors.clear();
        cell_errors.resize(P_FEMMesh->P_Cells->size());
        totol_Error=0.0;

        vector<double> solution;
        PetscInt SolutSize;
        VecGetSize(m_solut, &SolutSize);
        for(PetscInt Ipetsc = 0; Ipetsc < SolutSize; Ipetsc++)
        {
            PetscScalar v;
            VecGetValues(m_solut,1,&Ipetsc,&v);
            solution.push_back(v);
        }

        MSplineFunction * P_FEMSolution=new MSplineFunction(solution, *P_FEMMesh);//The Solution of FEM

        for (int i=0;i<P_FEMMesh->P_Cells->size();i++)
        {
                double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
                double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
                double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
                double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];

                vector<Coordinate*> Pcoordinate_Ps;
                for (int k=0; k<u_gaussnum; k++)
                {
                        for (int l=0; l<v_gaussnum; l++)
                        {
                                Coordinate* CoorP=new Coordinate;
                                CoorP->xy[0]=0.5*(x1+x0)+0.5*(x1-x0)*u_gausspt[k];//u[k];
                                CoorP->xy[1]=0.5*(y1+y0)+0.5*(y1-y0)*v_gausspt[l];//v[l];(y0+y1+(y1-y0)*gausspt[k]) * 0.5;
                                Pcoordinate_Ps.push_back(CoorP);
                        }
                }

                /////////////////////////////////////////////////////////////////////////////////////////////////
                ComputeFunval_MSplineParametricMap_PDE_PFEM(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent, exactval,
                                                            0, *P_ParametricMesh, P_MSplineParametricMap,
                                                            ExampleIndex, EquationIndex);


                P_FEMSolution->Evaluation(0,0,Pcoordinate_Ps,temp_Error,i,*P_FEMMesh);

                cell_errors[i] = 0;
                for(int j=0;j<u_gaussnum*v_gaussnum;j++)
                {
                        int nn = j / v_gaussnum;
                        int mm = j % v_gaussnum;
                        cell_errors[i] += w[nn][mm]*pow(exactval[j]-temp_Error[j],2);
                }
                //=======================================================
                totol_Error = totol_Error+cell_errors[i]*(y1-y0)*(x1-x0)/4.0;
                //=======================================================
                cell_errors[i] = sqrt(cell_errors[i] *(y1-y0) * (x1-x0)/4.0);
                //cout<<"Errors at "<<i<<"-th cell = "<<cell_errors[i]<<endl;
        }
        totol_Error = sqrt(totol_Error);
}


//=====================================================
//L2 error on the physical Domain
static void ComputError_MSplineParametricMap_OnPhysicalDomain_PDE_PFEM(const vector<double>& u_gausspt,
                                                                       const vector<double> & v_gausspt,double **w, const Vec & m_solut,
                                                       vector<double> &cell_errors, double &totol_Error,
                                                        const Mesh*  P_FEMMesh, const Mesh * P_ParametricMesh,
                                                       MSplineFunctions * P_MSplineParametricMap,
                                                       unsigned int ExampleIndex, unsigned int EquationIndex)
{
    ofstream ofJacobian;
    ofJacobian.open("Jacobian.txt");
    double MaxMaxJ=-100.0;
    double MinMinJ=100.0;

    unsigned int u_gaussnum=u_gausspt.size();
    unsigned int v_gaussnum=v_gausspt.size();

    vector<double> temp_Error;
    vector<double> exactval;
    vector<double> Jval;

    cell_errors.clear();
    cell_errors.resize(P_FEMMesh->P_Cells->size());
    totol_Error=0.0;

    vector<double> solution;
    PetscInt SolutSize;
    VecGetSize(m_solut, &SolutSize);
    for(PetscInt Ipetsc = 0; Ipetsc < SolutSize; Ipetsc++)
    {
        PetscScalar v;
        VecGetValues(m_solut,1,&Ipetsc,&v);
        solution.push_back(v);
    }

    MSplineFunction * P_FEMSolution=new MSplineFunction(solution, *P_FEMMesh);//The Solution of FEM

    for (int i=0;i<P_FEMMesh->P_Cells->size();i++)
    {
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];

        vector<Coordinate*> Pcoordinate_Ps;
        for (int k=0; k<u_gaussnum; k++)
        {
            for (int l=0; l<v_gaussnum; l++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=0.5*(x1+x0)+0.5*(x1-x0)*u_gausspt[k];//u[k];
                CoorP->xy[1]=0.5*(y1+y0)+0.5*(y1-y0)*v_gausspt[l];//v[l];(y0+y1+(y1-y0)*gausspt[k]) * 0.5;
                Pcoordinate_Ps.push_back(CoorP);
            }
        }


        // ///////////////////////////////////////////////////////////////////////////////////////////////

        ComputeFunval_MSplineParametricMap_PDE_PFEM(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent,
                                                    exactval, 0, *P_ParametricMesh, P_MSplineParametricMap,
                                                    ExampleIndex, EquationIndex);


        P_FEMSolution->Evaluation(0,0,Pcoordinate_Ps,temp_Error,i,*P_FEMMesh);

        //Jacobian
        JacobianValue(P_MSplineParametricMap->P_MSplineFunctions[0], P_MSplineParametricMap->P_MSplineFunctions[1],
                      Pcoordinate_Ps,Jval,P_FEMMesh->P_Cells->at(i).ItsParent,P_FEMMesh->P_Cells->at(i).ItsParent,
                      P_ParametricMesh,P_ParametricMesh);

        cell_errors[i] = 0.0;

        double maxJ=-100.0;
        double minJ=100.0;

        for(int j=0;j<u_gaussnum*v_gaussnum;j++)
        {
            //cout<<"L2 error on the physical Domain::Jacobian["<<j<<"] = "<<Jval[j]<<endl;
            if(Jval[j]>maxJ)
            {
                maxJ=Jval[j];
            }
            if(Jval[j]<minJ)
            {
                minJ=Jval[j];
            }
            if(Jval[j]>MaxMaxJ)
            {
                MaxMaxJ=Jval[j];
            }
            if(Jval[j]<MinMinJ)
            {
                MinMinJ=Jval[j];
            }


            if(Jval[j]<0)
            {
                Jval[j]=-Jval[j];
            }

            int nn = j / v_gaussnum;
            int mm = j % v_gaussnum;
            cell_errors[i] += w[nn][mm]*pow(exactval[j]-temp_Error[j],2)*Jval[j];
        }
        ofJacobian<<"the"<<i<<"-th cell:"<<endl;
        ofJacobian<<"maxJ = "<<maxJ<<endl;
        ofJacobian<<"minJ = "<<minJ<<endl;


        //=======================================================
        totol_Error = totol_Error+cell_errors[i]*(y1-y0)*(x1-x0)/4;
        //=======================================================
        cell_errors[i] = sqrt(cell_errors[i] *(y1-y0) * (x1-x0)/4);
        //cout<<"Errors at "<<i<<"-th cell = "<<cell_errors[i]<<endl;
    }
    totol_Error = sqrt(totol_Error);

    ofJacobian<<endl;
    ofJacobian<<endl;

    ofJacobian<<"MaxMaxJ= "<<MaxMaxJ<<endl;
    ofJacobian<<"MinMinJ= "<<MinMinJ<<endl;

    ofJacobian.close();
}

//L2 Error on Mesh
static void ComputError_MSplineParametricMap(const vector<double>& u_gausspt, const vector<double> & v_gausspt,double **w, const Vec & m_solut,
                                                                          vector<double> &cell_errors, double &totol_Error,
                                                                          const Mesh*  P_FEMMesh, const Mesh * P_ParametricMesh,
                                             MSplineFunctions * P_MSplineParametricMap, unsigned int ExampleIndex)
{
        unsigned int u_gaussnum=u_gausspt.size();
        unsigned int v_gaussnum=v_gausspt.size();

        vector<double> temp_Error;
        vector<double> exactval;

        cell_errors.clear();
        cell_errors.resize(P_FEMMesh->P_Cells->size());
        totol_Error=0.0;

        vector<double> solution;
        PetscInt SolutSize;
        VecGetSize(m_solut, &SolutSize);
        for(PetscInt Ipetsc = 0; Ipetsc < SolutSize; Ipetsc++)
        {
            PetscScalar v;
            VecGetValues(m_solut,1,&Ipetsc,&v);
            solution.push_back(v);
        }

        MSplineFunction * P_FEMSolution=new MSplineFunction(solution, *P_FEMMesh);//The Solution of FEM

        for (int i=0;i<P_FEMMesh->P_Cells->size();i++)
        {
                double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
                double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
                double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
                double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];

                vector<Coordinate*> Pcoordinate_Ps;
                for (int k=0; k<u_gaussnum; k++)
                {
                        for (int l=0; l<v_gaussnum; l++)
                        {
                                Coordinate* CoorP=new Coordinate;
                                CoorP->xy[0]=0.5*(x1+x0)+0.5*(x1-x0)*u_gausspt[k];//u[k];
                                CoorP->xy[1]=0.5*(y1+y0)+0.5*(y1-y0)*v_gausspt[l];//v[l];(y0+y1+(y1-y0)*gausspt[k]) * 0.5;
                                Pcoordinate_Ps.push_back(CoorP);
                        }
                }

                /////////////////////////////////////////////////////////////////////////////////////////////////
                ComputeFunval_MSplineParametricMap(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent, exactval, 0, *P_ParametricMesh, P_MSplineParametricMap, ExampleIndex);


                P_FEMSolution->Evaluation(0,0,Pcoordinate_Ps,temp_Error,i,*P_FEMMesh);

                cell_errors[i] = 0;
                for(int j=0;j<u_gaussnum*v_gaussnum;j++)
                {
                        int nn = j / v_gaussnum;
                        int mm = j % v_gaussnum;
                        cell_errors[i] += w[nn][mm]*pow(exactval[j]-temp_Error[j],2);
                }
                //=======================================================
                totol_Error = totol_Error+cell_errors[i]*(y1-y0)*(x1-x0)/4.0;
                //=======================================================
                cell_errors[i] = sqrt(cell_errors[i] *(y1-y0) * (x1-x0)/4.0);
                //cout<<"Errors at "<<i<<"-th cell = "<<cell_errors[i]<<endl;
        }
        totol_Error = sqrt(totol_Error);
}


////L2 Error on Mesh (for PDE_PFEM)
//static void ComputError_MSplineParametricMap_PDE_PFEM(const vector<double>& u_gausspt, const vector<double> & v_gausspt,double **w, const Vec & m_solut,
//                                                            vector<double> &cell_errors, double &totol_Error,
//                                                            const Mesh*  P_FEMMesh, const Mesh * P_ParametricMesh,
//                                                            MSplineFunctions * P_MSplineParametricMap,
//                                                      unsigned int ExampleIndex, unsigned int EquationIndex)
//{
//        unsigned int u_gaussnum=u_gausspt.size();
//        unsigned int v_gaussnum=v_gausspt.size();

//        vector<double> temp_Error;
//        vector<double> exactval;

//        cell_errors.clear();
//        cell_errors.resize(P_FEMMesh->P_Cells->size());
//        totol_Error=0.0;

//        vector<double> solution;
//        PetscInt SolutSize;
//        VecGetSize(m_solut, &SolutSize);
//        for(PetscInt Ipetsc = 0; Ipetsc < SolutSize; Ipetsc++)
//        {
//            PetscScalar v;
//            VecGetValues(m_solut,1,&Ipetsc,&v);
//            solution.push_back(v);
//        }

//        MSplineFunction * P_FEMSolution=new MSplineFunction(solution, *P_FEMMesh);//The Solution of FEM

//        for (int i=0;i<P_FEMMesh->P_Cells->size();i++)
//        {
//                double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
//                double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
//                double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
//                double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];

//                vector<Coordinate*> Pcoordinate_Ps;
//                for (int k=0; k<u_gaussnum; k++)
//                {
//                        for (int l=0; l<v_gaussnum; l++)
//                        {
//                                Coordinate* CoorP=new Coordinate;
//                                CoorP->xy[0]=0.5*(x1+x0)+0.5*(x1-x0)*u_gausspt[k];//u[k];
//                                CoorP->xy[1]=0.5*(y1+y0)+0.5*(y1-y0)*v_gausspt[l];//v[l];(y0+y1+(y1-y0)*gausspt[k]) * 0.5;
//                                Pcoordinate_Ps.push_back(CoorP);
//                        }
//                }

//                /////////////////////////////////////////////////////////////////////////////////////////////////
//                ComputeFunval_MSplineParametricMap_PDE_PFEM(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent,
//                                                            exactval, 0, *P_ParametricMesh, P_MSplineParametricMap,
//                                                            ExampleIndex, EquationIndex);


//                P_FEMSolution->Evaluation(0,0,Pcoordinate_Ps,temp_Error,i,*P_FEMMesh);

//                cell_errors[i] = 0;
//                for(int j=0;j<u_gaussnum*v_gaussnum;j++)
//                {
//                        int nn = j / v_gaussnum;
//                        int mm = j % v_gaussnum;
//                        cell_errors[i] += w[nn][mm]*pow(exactval[j]-temp_Error[j],2);
//                }
//                //=======================================================
//                totol_Error = totol_Error+cell_errors[i]*(y1-y0)*(x1-x0)/4.0;
//                //=======================================================
//                cell_errors[i] = sqrt(cell_errors[i] *(y1-y0) * (x1-x0)/4.0);
//                //cout<<"Errors at "<<i<<"-th cell = "<<cell_errors[i]<<endl;
//        }
//        totol_Error = sqrt(totol_Error);
//}



static void ComputError_RefinedParametrization(const vector<double>& u_gausspt, const vector<double> & v_gausspt,
                                                                                double **w, const Vec & m_solut, vector<double> &cell_errors,
                                                                                 double &totol_Error, const Mesh*  P_FEMMesh,
                                                                                MSplineFunctions * P_RefinedParametricMap, unsigned int ExampleIndex)
{
    unsigned int u_gaussnum=u_gausspt.size();
    unsigned int v_gaussnum=v_gausspt.size();

    vector<double> temp_Error;
    vector<double> exactval;

    cell_errors.clear();
    cell_errors.resize(P_FEMMesh->P_Cells->size());
    totol_Error=0.0;

    vector<double> solution;
    PetscInt SolutSize;
    VecGetSize(m_solut, &SolutSize);
    for(PetscInt Ipetsc = 0; Ipetsc < SolutSize; Ipetsc++)
    {
        PetscScalar v;
        VecGetValues(m_solut,1,&Ipetsc,&v);
        solution.push_back(v);
    }

    MSplineFunction * P_FEMSolution=new MSplineFunction(solution, *P_FEMMesh);//The Solution of FEM

    for (int i=0;i<P_FEMMesh->P_Cells->size();i++)
    {
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];

        vector<Coordinate*> Pcoordinate_Ps;
        for (int k=0; k<u_gaussnum; k++)
        {
            for (int l=0; l<v_gaussnum; l++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=0.5*(x1+x0)+0.5*(x1-x0)*u_gausspt[k];//u[k];
                CoorP->xy[1]=0.5*(y1+y0)+0.5*(y1-y0)*v_gausspt[l];//v[l];(y0+y1+(y1-y0)*gausspt[k]) * 0.5;
                Pcoordinate_Ps.push_back(CoorP);
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        ComputeFunval_MSplineParametricMap(Pcoordinate_Ps, i, exactval, 0, *P_FEMMesh, P_RefinedParametricMap, ExampleIndex);


        P_FEMSolution->Evaluation(0,0,Pcoordinate_Ps,temp_Error,i,*P_FEMMesh);

        cell_errors[i] = 0;
        for(int j=0;j<u_gaussnum*v_gaussnum;j++)
        {
            int nn = j / v_gaussnum;
            int mm = j % v_gaussnum;
            cell_errors[i] += w[nn][mm]*pow(exactval[j]-temp_Error[j],2);
        }
        //=======================================================
        totol_Error=totol_Error+cell_errors[i]*(y1-y0)*(x1-x0)/4;
        //=======================================================
        cell_errors[i] = sqrt(cell_errors[i] *(y1-y0) * (x1-x0)/4);
        //cout<<"Errors at "<<i<<"-th cell = "<<cell_errors[i]<<endl;
    }
    totol_Error = sqrt(totol_Error);
}

void FEML2PosteriorError(const vector<double> & gausspt, double ** w,
                         Vec &m_solut,
                         Mesh* P_FEMMesh, Mesh* P_ParametricMesh,
                         MSplineFunctions* P_ParametricMap, unsigned int ExampleIndex,
                         vector<double> &cell_errorsPhysDom, double total_ErrorPhysDom)
{
    //Just compute the L2 posterior error
    ComputError_MSplineParametricMap_OnPhysicalDomain(gausspt, gausspt, w, m_solut,
                                                      cell_errorsPhysDom, total_ErrorPhysDom,
                                                       P_FEMMesh,  P_ParametricMesh,
                                                      P_ParametricMap, ExampleIndex);

    //Print the error
    ofstream ofErrors;
    ofErrors.open("L2ErrorsOverCellsOnPhysicalDomain.txt");
    for(unsigned int i_error=0; i_error<cell_errorsPhysDom.size(); i_error++)
    {
        ofErrors<<cell_errorsPhysDom[i_error]<<" "<<endl;
    }
    ofErrors<<endl;
    ofErrors.close();

    ofstream ofTotalError;
    ofTotalError.open("L2TotalErrors.txt");
    ofTotalError<<total_ErrorPhysDom<<endl;
    ofTotalError<<endl;
    ofTotalError.close();

}

//For appriximation exact solution
void Predict_FEML2PosteriorError(const vector<double> & gausspt, double ** w,
                            const vector<double> &solution,
                            unsigned int SubExampleIndex, Mesh* P_ParametricMesh,Mesh* P_FEMMesh,
                             unsigned int ExampleIndex,  MSplineFunctions* P_ParametricMap,
                            vector<double> &cell_errorsPhysDom, double total_ErrorPhysDom)
{

    //Just Predict the L2 posterior error
    Predict_ComputError_MSplineParametricMap_OnPhysicalDomain(gausspt, gausspt, w, solution,
                                                      cell_errorsPhysDom, total_ErrorPhysDom,P_FEMMesh,
                                                      P_ParametricMesh,P_ParametricMap,
                                                      ExampleIndex, SubExampleIndex);


    //Print the error
    ofstream ofErrors;
    ofErrors.open("L2ErrorsOverCellsOnPhysicalDomain.txt");
    for(unsigned int i_error=0; i_error<cell_errorsPhysDom.size(); i_error++)
    {
        ofErrors<<cell_errorsPhysDom[i_error]<<" "<<endl;
    }
    ofErrors<<endl;
    ofErrors.close();

    ofstream ofTotalError;
    ofTotalError.open("L2TotalErrors.txt");
    ofTotalError<<total_ErrorPhysDom<<endl;
    ofTotalError<<endl;
    ofTotalError.close();

}

//For PDE_FEM
void FEML2PosteriorError_PDE_FEM(const vector<double> & gausspt, double ** w,
                         Vec &m_solut,
                         Mesh* P_FEMMesh, Mesh* P_ParametricMesh,
                         MSplineFunctions* P_ParametricMap, unsigned int ExampleIndex,
                                 unsigned int EquationIndex,
                         vector<double> &cell_errorsPhysDom, double total_ErrorPhysDom)
{
    //Just compute the L2 posterior error
    ComputError_MSplineParametricMap_OnPhysicalDomain_PDE_PFEM(gausspt, gausspt, w, m_solut,
                                                      cell_errorsPhysDom, total_ErrorPhysDom,
                                                       P_FEMMesh,  P_ParametricMesh,
                                                      P_ParametricMap, ExampleIndex, EquationIndex);

    //Print the error
    ofstream ofErrors;
    ofErrors.open("L2ErrorsOverCellsOnPhysicalDomain.txt");
    for(unsigned int i_error=0; i_error<cell_errorsPhysDom.size(); i_error++)
    {
        ofErrors<<cell_errorsPhysDom[i_error]<<" "<<endl;
    }
    ofErrors<<endl;
    ofErrors.close();

    ofstream ofTotalError;
    ofTotalError.open("L2TotalErrorsOverPhysicalDomain.txt");
    ofTotalError<<total_ErrorPhysDom<<endl;
    ofTotalError<<endl;
    ofTotalError.close();

}


void FEMErrors(const vector<double> & gausspt, double ** w,
               Vec &m_solut,
               Mesh* P_FEMMesh, Mesh* P_ParametricMesh,
               MSplineFunctions* P_ParametricMap, unsigned int ExampleIndex,
               vector<double> &cell_errors, double totol_Error,
               vector<double> &cell_errorsPhysDom, double total_ErrorPhysDom,
               vector<double> &cell_errorsResidual, double total_Error_Residual,
               vector<double> &cell_errorsResidualPhyDom, double  total_Error_ResidualPhyDom,
               vector<double> &CellErrEnergyNormMesh, double total_ErrEnergyNormMesh,
               vector<double> &CellErrEnergyNormPhysDom, double total_ErrEnergyNormPhysDom)
{
    ComputError_MSplineParametricMap(gausspt, gausspt, w, m_solut,
            cell_errors, totol_Error, P_FEMMesh, P_ParametricMesh,
                                     P_ParametricMap,ExampleIndex);

    ComputError_MSplineParametricMap_OnPhysicalDomain(gausspt, gausspt, w, m_solut,
                                                      cell_errorsPhysDom, total_ErrorPhysDom,
                                                       P_FEMMesh,  P_ParametricMesh,
                                                      P_ParametricMap, ExampleIndex);

    ResidualError_Mesh_RefinedParametricMap(gausspt, gausspt, w, m_solut,
                       cell_errorsResidual, total_Error_Residual,
                        P_FEMMesh,  P_ParametricMesh,
                       P_ParametricMap, ExampleIndex);
    ResidualError_PhysicalDomain_RefinedParametricMap(gausspt, gausspt, w, m_solut,
                                                      cell_errorsResidualPhyDom, total_Error_ResidualPhyDom,
                                                       P_FEMMesh,  P_ParametricMesh,
                                                      P_ParametricMap, ExampleIndex);

    EnergyNormError_PhyscialDomain_RefinedParametricMap(gausspt, gausspt,
                       w, m_solut, CellErrEnergyNormPhysDom, total_ErrEnergyNormPhysDom,
                       P_FEMMesh, P_ParametricMesh, P_ParametricMap, ExampleIndex);

    EnergyNormError_Mesh_RefinedParametricMap(gausspt, gausspt,
                       w, m_solut, CellErrEnergyNormMesh, total_ErrEnergyNormMesh,
                       P_FEMMesh, P_ParametricMesh, P_ParametricMap, ExampleIndex);

    //=========PutOut Errors=============
    PrintErrors(cell_errors, totol_Error,
                cell_errorsResidual, total_Error_Residual,
                cell_errorsPhysDom, total_ErrorPhysDom,
                cell_errorsResidualPhyDom, total_Error_ResidualPhyDom,
                CellErrEnergyNormPhysDom, total_ErrEnergyNormPhysDom,
                CellErrEnergyNormMesh, total_ErrEnergyNormMesh);
    //===================================
}

//For PDE_PFEM
void FEMErrors_PDE_PFEM(const vector<double> & gausspt, double ** w,
               Vec &m_solut,
               Mesh* P_FEMMesh, Mesh* P_ParametricMesh,
               MSplineFunctions* P_ParametricMap, unsigned int ExampleIndex,
               unsigned int EquationIndex,
               vector<double> &cell_errors, double totol_Error,
               vector<double> &cell_errorsPhysDom, double total_ErrorPhysDom,
               vector<double> &cell_errorsResidual, double total_Error_Residual,
               vector<double> &cell_errorsResidualPhyDom, double  total_Error_ResidualPhyDom,
               vector<double> &CellErrEnergyNormMesh, double total_ErrEnergyNormMesh,
               vector<double> &CellErrEnergyNormPhysDom, double total_ErrEnergyNormPhysDom)
{
    ComputError_MSplineParametricMap_PDE_PFEM(gausspt, gausspt, w, m_solut,
            cell_errors, totol_Error, P_FEMMesh, P_ParametricMesh,
                                     P_ParametricMap, ExampleIndex, EquationIndex);

    ComputError_MSplineParametricMap_OnPhysicalDomain_PDE_PFEM(gausspt, gausspt, w, m_solut,
                                                      cell_errorsPhysDom, total_ErrorPhysDom,
                                                       P_FEMMesh,  P_ParametricMesh,
                                                      P_ParametricMap, ExampleIndex, EquationIndex);

    ResidualError_Mesh_RefinedParametricMap_PDE_PFEM(gausspt, gausspt, w, m_solut,
                       cell_errorsResidual, total_Error_Residual,
                        P_FEMMesh,  P_ParametricMesh,
                       P_ParametricMap, ExampleIndex, EquationIndex);
    ResidualError_PhysicalDomain_RefinedParametricMap_PDE_PFEM(gausspt, gausspt, w, m_solut,
                                                      cell_errorsResidualPhyDom, total_Error_ResidualPhyDom,
                                                       P_FEMMesh,  P_ParametricMesh,
                                                      P_ParametricMap, ExampleIndex, EquationIndex);

    EnergyNormError_PhyscialDomain_RefinedParametricMap_PDE_PFEM(gausspt, gausspt,
                       w, m_solut, CellErrEnergyNormPhysDom, total_ErrEnergyNormPhysDom,
                       P_FEMMesh, P_ParametricMesh, P_ParametricMap, ExampleIndex, EquationIndex);

    EnergyNormError_Mesh_RefinedParametricMap_PDE_PFEM(gausspt, gausspt,
                       w, m_solut, CellErrEnergyNormMesh, total_ErrEnergyNormMesh,
                       P_FEMMesh, P_ParametricMesh, P_ParametricMap, ExampleIndex, EquationIndex);

    //=========PutOut Errors=============
    PrintErrors(cell_errors, totol_Error,
                cell_errorsResidual, total_Error_Residual,
                cell_errorsPhysDom, total_ErrorPhysDom,
                cell_errorsResidualPhyDom, total_Error_ResidualPhyDom,
                CellErrEnergyNormPhysDom, total_ErrEnergyNormPhysDom,
                CellErrEnergyNormMesh, total_ErrEnergyNormMesh);
    //===================================
}

#endif // FEM_ERRORFUNCTIONS_H
