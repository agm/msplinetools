#ifndef BASES_H
#define BASES_H

#include <vector>
using namespace std;
//-------------
class basis;
class Mesh;
class HermiteVertex;
//-------------

class Bases
{
public:
	vector<basis *> P_BasesSet;
public:
	Bases(void);
	~Bases(void);
	
	Bases(const Mesh & CurrentMesh);

public:
	//Generate all the Hermite data at each vertex of this Mesh
	//The second method to generate all the basis: store all the information at each vertex
	void HVerticesData_All_bases (vector<vector<HermiteVertex *> >& P_HVertices, const Mesh & CurrentMesh);

public:
	void Cout_Bases(const Mesh & CurrentMesh);
	basis * Return_Basis(unsigned int Index_Vertex, unsigned int i_H, const vector<vector<HermiteVertex *> > &P_HVertices, const Mesh & CurrentMesh);

	basis * Return_Basis_Cell(unsigned int Index_Vertex, unsigned int i_H, unsigned int Index_Cell, const vector<vector<HermiteVertex *> > &P_HVertices, const Mesh & CurrentMesh);
};

#endif
