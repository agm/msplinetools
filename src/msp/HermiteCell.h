#ifndef HERMITECELL_H
#define HERMITECELL_H

#include <vector>
using namespace std;
//----------------------
class HermiteData;
class Mesh;

//----------------------


class HermiteCell
{
public:
	unsigned int Index_InitialCell;
	unsigned int Index_Cell;
	vector<HermiteData *> P_HermiteAtCornerVertex;

public://Initialize

    HermiteCell(void);

    ~HermiteCell(void);

    HermiteCell(const HermiteCell & HC);

    HermiteCell(unsigned int index_InitialCell, unsigned int index_Cell);

    /**
     * @brief HermiteCell: Generate a HermiteCell such that it correspondents to the index_cell-th cell
     with the index_vertex-th vertex's Hermitedata as *P_HD_V and other vertices' Hermitedata=[0,0,0,0]
     * @param index_vertex
     * @param index_cell
     * @param P_HD_V
     * @param CurrentMesh
     */
    HermiteCell(unsigned int index_vertex, unsigned int index_cell, HermiteData * P_HD_V, const Mesh & CurrentMesh);

public:
    /**
     * @brief ResetHermiteCell: Copy HC
     * @param const HermiteCell & HC
     */
    void ResetHermiteCell(const HermiteCell & HC);

    //----overload
    /**
     * @brief Lambda
     * @param coeff
     * @return coeff*(this HermiteCell)
     */
    HermiteCell Lambda(const double coeff);
    //+
    /**
     * @brief operator +: overload "+"
     * @param right
     * @return
     */
    HermiteCell operator+(const HermiteCell &right);
    //-
    /**
     * @brief operator -: overload "-"
     * @param right
     * @return
     */
    HermiteCell operator-(const HermiteCell &right);
    //=
    /**
     * @brief operator =: overload "="
     * @param right
     * @return
     */
    HermiteCell& operator=(const HermiteCell &right);

public:
    /**
     * @brief Evaluate: return Values as the values of the function's (DerOrderu, DerOrderv)-th deriviation
     *  defined by this Hermite cell
     * @param DerOrderu
     * @param DerOrderv
     * @param coordinateST
     * @param Values
     * @param PCellSize
     */
    void Evaluate(int DerOrderu, int DerOrderv, const vector<double> & coordinateST, double &Values, const double * PCellSize);

public:
    void Cout_HermiteCell();
};

#endif //HERMITECELL_H
