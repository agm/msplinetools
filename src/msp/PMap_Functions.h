#ifndef PMAP_FUNCTIONS_H
#define PMAP_FUNCTIONS_H

#include <vector>
#include <set>
#include <iostream>
#include <math.h>
#include <algorithm>
#include <utility>

//Spline
#include "msp/PointSet.h"
#include "msp/MSplineFunctions.h"
#include "msp/Coordinate.h"
#include "msp/Mesh.h"
#include "msp/BasesCell.h"
#include "msp/Basis.h"
//Data structure
#include "msp/Node.h"
#include "msp/HermiteTransferMatrix.h"
#include "msp/HermiteData.h"

#include "msp/Functions.h"

////PETSc
#include <petsc.h>
#include <petscksp.h>

using namespace std;
/*-------------------------------------------
  InPut: the point set Qs in physcial domain
  OutPut: Qs=Qs-(P_ParametricMap)[Ps]
---------------------------------------------*/
void ModifyQs(const vector<PointSet> &Ps, vector<Coordinate> &Qs, MSplineFunctions* P_ParametricMap,const Mesh* P_ParametricMesh)
{
    unsigned int PointIndex=0;
    for(unsigned int i=0; i<Ps.size(); i++)
    {
        vector<vector<double> > Values;
        P_ParametricMap->Evaluation(0,0,Ps[i].P_coordinates_Ps, Ps[i].Index_ItsCell, Values, *P_ParametricMesh);
        if(Values.size()>1)
        {
            for(unsigned int j=0; j<Values[0].size(); j++)
            {
                //Qs[PointIndex]=Qs[PointIndex]-[Values[0][j], Values[1][j]];
                Qs[PointIndex].xy[0]=Qs[PointIndex].xy[0]-Values[0][j];
                Qs[PointIndex].xy[1]=Qs[PointIndex].xy[1]-Values[1][j];
                PointIndex=PointIndex+1;
            }
        }
        else
        {
            cout<<"the dimension of parametric map is less than 2!"<<endl;
            cin.get();
        }
    }
}

/*--------------------------------------------
  InPut: Ps(Points in the parametric domain),
         Qs(Points in the physical domain, denote as [x,y]),
         P_ParametricMesh(The parametric mesh)
  OutPut:A=\sum_{Ps[i]\in Ps} [Bk(Ps[i])Bl(Ps[i])]
         bx=\sum_{i}xi*B(i); where B(i)=[B1(Ps[i]), B2(Ps[i]), ..., Bn(Ps[i])]^T
         by=\sum_{i}yi*B(i); where B(i)=[B1(Ps[i]), B2(Ps[i]), ..., Bn(Ps[i])]^T
----------------------------------------------*/
void GenerateParametrixMatAndVec(const vector<PointSet> &Ps, const vector<Coordinate> &Qs, const Mesh* P_ParametricMesh,
                                 map<unsigned int, unsigned int> &BasisIndex, Mat& A, Vec& bx, Vec& by)
{
    PetscInt matrix_size, MatrixSize;
    MatGetSize(A, &matrix_size, &MatrixSize);

    //A=0; bx=0; by=0---------------------
    MatZeroEntries(A);
    VecSet(bx, 0.0);
    VecSet(by, 0.0);
    //------------------------------------
    unsigned int PointIndex = 0;
    //------------------------------------
    for(unsigned int i_Ps = 0; i_Ps < Ps.size(); i_Ps ++)
    {
        unsigned int CellIndex = Ps[i_Ps].Index_ItsCell;
        BasesCell * P_BC = new BasesCell(CellIndex, *P_ParametricMesh);

        vector<vector<double> > Values;
        for(unsigned int i_basis = 0; i_basis < P_BC->P_Bases_Cell.size(); i_basis ++)
        {
            unsigned int basisIndex = P_BC->P_Bases_Cell[i_basis]->Index_basis;
            map<unsigned int, unsigned int>::const_iterator itBasis = BasisIndex.find(basisIndex);
            vector<double> Value_ibasis;
            if(itBasis == BasisIndex.end())
            {
                 Value_ibasis.resize(Ps[i_Ps].P_coordinates_Ps.size(),0.0);
            }
            else
            {
               P_BC->P_Bases_Cell[i_basis]->Evaluation(0, 0, Ps[i_Ps].P_coordinates_Ps, CellIndex, Value_ibasis, *P_ParametricMesh);
            }
            Values.push_back(Value_ibasis);
        }

        for(unsigned int iPsi_Ps = 0; iPsi_Ps < Ps[i_Ps].P_coordinates_Ps.size(); iPsi_Ps ++)
        {
            //-------------------------
            double xi=Qs[PointIndex].xy[0];
            double yi=Qs[PointIndex].xy[1];

            //B, for Ps[i_Ps].P_coorinates_Ps[iPsi_Ps]
            for(unsigned int i_basis=0; i_basis< P_BC->P_Bases_Cell.size(); i_basis++)
            {
                //bx, by
                unsigned int BasisIndex1=P_BC->P_Bases_Cell[i_basis]->Index_basis;
                map<unsigned int, unsigned int>::const_iterator itBasis=BasisIndex.find(BasisIndex1);
                if(itBasis!=BasisIndex.end())//
                {
                    PetscScalar Bpi=xi*Values[i_basis][iPsi_Ps];
                    PetscInt Index=(*itBasis).second;

                    /*bx[Index]=bx[Index]+Bpi;*/
                    VecSetValues(bx, 1, &Index, &Bpi, ADD_VALUES);

                    Bpi=yi*Values[i_basis][iPsi_Ps];
                    /*by[Index]=by[Index]+Bpi;*/
                    VecSetValues(by, 1, &Index, &Bpi, ADD_VALUES);


                    //A
                    for(unsigned int j_basis=0; j_basis< P_BC->P_Bases_Cell.size(); j_basis++)
                    {
                        unsigned int BasisIndex2=P_BC->P_Bases_Cell[j_basis]->Index_basis;
                        map<unsigned int, unsigned int>::const_iterator jtBasis=BasisIndex.find(BasisIndex2);
                        if(jtBasis!=BasisIndex.end())//
                        {
                            PetscScalar Bqi=Values[i_basis][iPsi_Ps]*Values[j_basis][iPsi_Ps];
                            PetscInt Index2=(*jtBasis).second;
                            //A[Index][Index2]=A[Index][Index2]+Bqi;
                            MatSetValues(A, 1, &Index, 1, &Index2, &Bqi, ADD_VALUES);
                        }
                    }
                    //-----
                }
            }
            //-------------------------
            PointIndex=PointIndex+1;
        }
        delete P_BC;
    }
}



/*-----------------------------------------
    Generate the Parametric Map again
-------------------------------------------*/
void ReNewParametricMap(const Vec &cx, const Vec &cy, const Mesh* P_ParametricMesh, MSplineFunctions* &P_ParametricMap,
                                vector<vector<double> > &Coeff_Parametric_Map, map<unsigned int, unsigned int> BasisIndex)
{
    delete P_ParametricMap;
    P_ParametricMap=NULL;

    for(map<unsigned int, unsigned int>::const_iterator it=BasisIndex.begin(); it!=BasisIndex.end(); it++)
    {
        unsigned int i=it->first;
        PetscInt j=it->second;
        PetscScalar x,y;

        VecGetValues(cx,1,&j,&x);
        VecGetValues(cy,1,&j,&y);

//        cout<<i<<"-th---> x= "<<x<<" y= "<<y<<endl;
//        cout<<"+ ("<<Coeff_Parametric_Map[0][i]<<", "<<Coeff_Parametric_Map[1][i]<<")"<<endl;

        Coeff_Parametric_Map[0][i]=Coeff_Parametric_Map[0][i]+x;
        Coeff_Parametric_Map[1][i]=Coeff_Parametric_Map[1][i]+y;

//        cout<<"----------------------------------------------------------------------"<<endl;
//        cout<<"("<<Coeff_Parametric_Map[0][i]<<", "<<Coeff_Parametric_Map[1][i]<<")"<<endl;

    }

    P_ParametricMap=new MSplineFunctions(Coeff_Parametric_Map, *P_ParametricMesh);
}

//=================================================================================
/*
  InPut:(Vset, Tv), Mesh* P_ParametricMesh
  OutPut: the edges of ParametricMesh with the vertices in Vset
          and the indices in Qset
*/
void GetEdgesOfMesh(const vector<unsigned int> &Vset, const vector<Node<unsigned int>* >& Tv,
                    const map<unsigned int, unsigned int> &IVQ, vector<pair<unsigned int, unsigned int> > &VecEdges,
                    vector<pair<unsigned int, unsigned int> > &VecQIndex)
{
    VecEdges.clear();
    VecQIndex.clear();

    set<pair<unsigned int, unsigned int> > Edges;

    for(unsigned int i=0; i<Vset.size(); i++)
    {
        //Vset[i] and Tv[i]
        for(unsigned int i_children=0; i_children<Tv[i]->ItsChildren.size(); i_children++)
        {
            pair<unsigned int, unsigned int> E;
            pair<unsigned int, unsigned int> qIndex;
            map<unsigned int, unsigned int>::const_iterator itIVQ;
            if(Vset[i]<Vset[Tv[i]->ItsChildren[i_children]])//E.first<E.second
            {
                E.first=Vset[i];
                itIVQ=IVQ.find(i);
                qIndex.first=(*itIVQ).second;
                E.second=Vset[Tv[i]->ItsChildren[i_children]];
                itIVQ=IVQ.find(Tv[i]->ItsChildren[i_children]);
                qIndex.second=(*itIVQ).second;

                set<pair<unsigned int, unsigned int> >::const_iterator itEdges=Edges.find(E);
                if(itEdges==Edges.end())
                {
                    Edges.insert(E);
                    VecEdges.push_back(E);
                    VecQIndex.push_back(qIndex);
                }

            }
            else
            {
                E.first=Vset[Tv[i]->ItsChildren[i_children]];
                itIVQ=IVQ.find(Tv[i]->ItsChildren[i_children]);
                qIndex.first=(*itIVQ).second;
                E.second=Vset[i];
                itIVQ=IVQ.find(i);
                qIndex.second=(*itIVQ).second;

                set<pair<unsigned int, unsigned int> >::const_iterator itEdges=Edges.find(E);
                if(itEdges==Edges.end())
                {
                    Edges.insert(E);
                    VecEdges.push_back(E);
                    VecQIndex.push_back(qIndex);
                }
            }
        }
        for(unsigned int i_parents=0; i_parents<Tv[i]->ItsParents.size(); i_parents++)
        {
            pair<unsigned int, unsigned int> E;
            pair<unsigned int, unsigned int> qIndex;
            map<unsigned int, unsigned int>::const_iterator itIVQ;
            if(Vset[i]<Vset[Tv[i]->ItsParents[i_parents]])
            {
                E.first=Vset[i];
                itIVQ=IVQ.find(i);
                qIndex.first=(*itIVQ).second;
                E.second=Vset[Tv[i]->ItsParents[i_parents]];
                itIVQ=IVQ.find(Tv[i]->ItsParents[i_parents]);
                qIndex.second=(*itIVQ).second;

                set<pair<unsigned int, unsigned int> >::const_iterator itEdges=Edges.find(E);
                if(itEdges==Edges.end())
                {
                    Edges.insert(E);
                    VecEdges.push_back(E);
                    VecQIndex.push_back(qIndex);
                }
            }
            else
            {
                E.first=Vset[Tv[i]->ItsParents[i_parents]];
                itIVQ=IVQ.find(Tv[i]->ItsParents[i_parents]);
                qIndex.first=(*itIVQ).second;
                E.second=Vset[i];
                itIVQ=IVQ.find(i);
                qIndex.second=(*itIVQ).second;
                set<pair<unsigned int, unsigned int> >::const_iterator itEdges=Edges.find(E);
                if(itEdges==Edges.end())
                {
                    Edges.insert(E);
                    VecEdges.push_back(E);
                    VecQIndex.push_back(qIndex);
                }
            }
        }
    }
}
//a.
/*
  InPut:
  OutPut:vector<Coordinate *> &Qcurve
*/
void GetCurveOnPhysicalDomain(unsigned int Eindex, const vector<pair<unsigned int, unsigned int> > &QIndex,
                              const set<unsigned int> &QKnots, const vector<Coordinate *> & Qset,
                              const vector<Node<unsigned int>* > & Tq, vector<Coordinate *> &Qcurve)
{
    Qcurve.clear();

    unsigned int qIndex1=QIndex[Eindex].first;
    unsigned int qIndex2=QIndex[Eindex].second;


    vector<Node<unsigned int> > EQPaths;//from qIndex1 to qIndex2
    //Initialize
    bool Does_Find;//Does find qIndex2?
    if(qIndex1==qIndex2)
    {
        Does_Find=true;
    }
    else
    {
        Does_Find=false;
    }
    vector<unsigned int> Leaves;
    Leaves.push_back(qIndex1);
    vector<unsigned int> LeavesParentsIndex;
    LeavesParentsIndex.push_back(0);


    while(Does_Find==false)//Doesn't find qIndex2
    {
        vector<unsigned int> leafKnots;
        vector<unsigned int> leafParentsIndex;

        for(unsigned int i=0; i<Leaves.size(); i++)
        {

            Node<unsigned int> eq_path;
            //---------Leaves[i]---------
            eq_path.ItsParents.push_back(Leaves[i]);
            eq_path.ItsParents.push_back(LeavesParentsIndex[i]);//ItsParents=[Qindex,indexItsParent]

            //P_EQpath->ItsChildren
            for(unsigned int i_Children = 0; i_Children < Tq[Leaves[i]]->ItsChildren.size(); i_Children++ )
            {
                if(Tq[Leaves[i]]->ItsChildren[i_Children]==qIndex2)
                {
                    Does_Find=true;
                    eq_path.ItsChildren.push_back(Tq[Leaves[i]]->ItsChildren[i_Children]);
                    break;
                }
                set<unsigned int>::const_iterator it = QKnots.find(Tq[Leaves[i]]->ItsChildren[i_Children]);
                if(it==QKnots.end())
                {
                    eq_path.ItsChildren.push_back(Tq[Leaves[i]]->ItsChildren[i_Children]);
                }

            }
            if(Does_Find==false)
            {
                for(unsigned int i_Parents=0; i_Parents < Tq[Leaves[i]]->ItsParents.size(); i_Parents++)
                {
                    if(Tq[Leaves[i]]->ItsParents[i_Parents]==qIndex2)
                    {
                        Does_Find=true;
                        eq_path.ItsChildren.push_back(Tq[Leaves[i]]->ItsParents[i_Parents]);
                        break;
                    }
                    set<unsigned int>::const_iterator it = QKnots.find(Tq[Leaves[i]]->ItsParents[i_Parents]);
                    if(it==QKnots.end())
                    {
                        eq_path.ItsChildren.push_back(Tq[Leaves[i]]->ItsParents[i_Parents]);
                    }
                }
            }

            //---------------------------
            EQPaths.push_back(eq_path);


            if(Does_Find==true)
            {
                break;
            }
            for(unsigned int i_leaf=0; i_leaf<eq_path.ItsChildren.size(); i_leaf++)
            {
                leafKnots.push_back(eq_path.ItsChildren[i_leaf]);
                leafParentsIndex.push_back(EQPaths.size()-1);
            }
        }
        if(Does_Find==false)
        {
            //up data Leaves
            Leaves.clear();
            LeavesParentsIndex.clear();
            for(unsigned int i=0; i<leafKnots.size(); i++)
            {
                Leaves.push_back(leafKnots[i]);
                LeavesParentsIndex.push_back(leafParentsIndex[i]);
            }

        }

    }

    //Based on EQPaths and EQPathIndex, get Qcurve
    unsigned int EQPathIndex=EQPaths.size()-1;
    vector<unsigned int> Qcurveindex;

    bool key=(EQPaths[EQPathIndex].ItsParents[0]==qIndex1);
    while(key==false)
    {
        Qcurveindex.insert(Qcurveindex.begin(), EQPaths[EQPathIndex].ItsParents[0]);
        EQPathIndex=EQPaths[EQPathIndex].ItsParents[1];

        key=(EQPaths[EQPathIndex].ItsParents[0]==qIndex1);
    }

    //Qcurve
    Qcurve.push_back(Qset[qIndex1]);
    for(unsigned int i=0; i<Qcurveindex.size(); i++)
    {
        Qcurve.push_back(Qset[Qcurveindex[i]]);
    }
    Qcurve.push_back(Qset[qIndex2]);
}


//b.
void GetQcurveParameters(const vector<Coordinate *> &Qcurve, vector<double> &QcurveParameters)
{
    QcurveParameters.clear();
    QcurveParameters.push_back(0.0);
    vector<double> Distance;
    for(unsigned int i=0; i<Qcurve.size()-1; i++)
    {
        //d=distance(Qcurve[i+1], Qcurve[i])
        double d=sqrt((Qcurve[i+1]->xy[0]-Qcurve[i]->xy[0])*(Qcurve[i+1]->xy[0]-Qcurve[i]->xy[0])+(Qcurve[i+1]->xy[1]-Qcurve[i]->xy[1])*(Qcurve[i+1]->xy[1]-Qcurve[i]->xy[1]));

        //d1=distance(Qcurve[i+1], Qcurve[0])
        double d1;
        if(Distance.size())
        {
            d1=d+Distance[Distance.size()-1];
        }
        else
        {
            d1=d;
        }

        Distance.push_back(d1);
    }
    /*--------------------------*/
    double length=Distance[Distance.size()-1];
    for(unsigned int i=0; i<Distance.size(); i++)
    {
        QcurveParameters.push_back(Distance[i]/length);
    }
}
//c.
/*------------------------------------------------
  InPut:
  Edge, P_ParametricMesh
  QcurveParameters: the parameters of the points on the curve of the physcial domain

  OutPut: vector<vector<double> > EdgeParameters
          unsigned int CommCellIndex
--------------------------------------------------*/
static double Abs(double t)
{
    if(t>0)
    {
        return t;
    }
    else
    {
        return -t;
    }

}
static void CoordinateScale(double s1, double s2, const vector<double> &QcurveParameters, vector<double> & EdgeParameters_i)
{
    EdgeParameters_i.clear();
    for(unsigned int i=0; i<QcurveParameters.size(); i++)
    {
        EdgeParameters_i.push_back(s1+QcurveParameters[i]*(s2-s1));
    }
}
void GetParametersOnEdge(pair<unsigned int, unsigned int> Edge, const Mesh* P_ParametricMesh, const vector<double> &QcurveParameters,
                         unsigned int &CommCellIndex, vector<vector<double> > &EdgeParameters)
{
    EdgeParameters.clear();
    vector<double> Ei;
    EdgeParameters.push_back(Ei);
    EdgeParameters.push_back(Ei);

    unsigned int vindex1=Edge.first;
    unsigned int vindex2=Edge.second;//Edge=<vindex1, vindex2>

    //Find the index of common cell of vindex1 and vindex2
    set<unsigned int> CellsV1, CellsV2;
    for(map<unsigned int, Coordinate>::const_iterator it=(P_ParametricMesh->P_Vertices->at(vindex1)).ItsCellsToCoordinatesMap.begin(); it!=(P_ParametricMesh->P_Vertices->at(vindex1)).ItsCellsToCoordinatesMap.end(); it++)
    {
        CellsV1.insert((*it).first);
    }

    for(map<unsigned int, Coordinate>::const_iterator it=(P_ParametricMesh->P_Vertices->at(vindex2)).ItsCellsToCoordinatesMap.begin(); it!=(P_ParametricMesh->P_Vertices->at(vindex2)).ItsCellsToCoordinatesMap.end(); it++)
    {
        //cout<<"(*it).first="<<(*it).first<<endl;
        CellsV2.insert((*it).first);
    }

    set<unsigned int> CommCells;
    set_intersection(CellsV1.begin(), CellsV1.end(), CellsV2.begin(), CellsV2.end(),inserter(CommCells, CommCells.begin()));
    if(CommCells.size())
    {
        CommCellIndex=*CommCells.begin();
    }
    else
    {
        cout<<"GetParametersOnEdge: There is no common cell of vindex1 and vindex2"<<endl;
        cin.get();
    }


    //------------------------------
    map<unsigned int, Coordinate>::const_iterator it1 = (P_ParametricMesh->P_Vertices->at(vindex1)).ItsCellsToCoordinatesMap.find(CommCellIndex);
    map<unsigned int, Coordinate>::const_iterator it2 = (P_ParametricMesh->P_Vertices->at(vindex2)).ItsCellsToCoordinatesMap.find(CommCellIndex);
    //------------------------------
    double s1=((*it1).second).xy[0];
    double t1=((*it1).second).xy[1];
    double s2=((*it2).second).xy[0];
    double t2=((*it2).second).xy[1];
    double length=sqrt((s1-s2)*(s1-s2)+(t1-t2)*(t1-t2));
    if(Abs(s1-s2)/length>0.5)// Edge is a horizonal edge
    {//t1=t2:[0,1]--->[s1, s2]
        CoordinateScale(s1, s2, QcurveParameters, EdgeParameters[0]);
        //For EdgeParameters[1]
        EdgeParameters[1].resize(QcurveParameters.size(),(t1+t2)*0.5);
    }
    else//Edge is a vertical edge
    {//s1=s2:[0,1]--->[t1,t2]
        CoordinateScale(t1, t2, QcurveParameters, EdgeParameters[1]);
        //For EdgeParameters[0]
        EdgeParameters[0].resize(QcurveParameters.size(),(s1+s2)*0.5);
    }
}

//d. get the points in Ps
void GetPsOnEdge(unsigned int CommCellIndex, const vector<vector<double> > & EdgeParameters,
                                  PointSet &Psi)
{
        Psi.P_coordinates_Ps.clear();

        Psi.Index_ItsCell=CommCellIndex;
        for(unsigned int i=0; i<EdgeParameters[0].size(); i++)
        {
                Coordinate* Coor=new Coordinate;
                Coor->xy[0]=EdgeParameters[0][i];
                Coor->xy[1]=EdgeParameters[1][i];
                Psi.P_coordinates_Ps.push_back(Coor);
        }

}

//f.Modify Ps and Qs
void ModifyPsQs(const PointSet &Psi,const vector<Coordinate *> &Qcurve, vector<PointSet> & Ps, vector<Coordinate> & Qs)
{
    //Ps:
    Ps.push_back(Psi);
    //Qs:vector<Coordinate> & Qs
    for(unsigned int i=0; i<Qcurve.size(); i++)
    {
        Qs.push_back(*(Qcurve[i]));
    }
}

/*-------------------------------------------------------------------------
  InPut:the edge segments of the parametric mesh (Vset,Tv)
        the curves on the physical domain (Qset, Tq)
  OutPut:the Point Sets Ps and Qs, such that Ps[i]-->Qs[i]
---------------------------------------------------------------------------*/
void ComputeCorrespondencePointSets(const vector<unsigned int> &Vset, const vector<Node<unsigned int>* >& Tv,
                                    const vector<Coordinate *> &Qset, const vector<Node<unsigned int>* > &Tq,
                                    const Mesh* P_ParametricMesh, MSplineFunctions* P_ParametricMap,
                                                                        const map<unsigned int, unsigned int> &IVQ, vector<PointSet> &Ps,
                                                                        vector<Coordinate> &Qs)
{
    Ps.clear();
    Qs.clear();

    //Get the edges from Vset and Tv
    vector<pair<unsigned int, unsigned int> > Edges;
    vector<pair<unsigned int, unsigned int> > QIndex;

    GetEdgesOfMesh(Vset, Tv,  IVQ, Edges, QIndex);

    set<unsigned int> QKnots;
    for(vector<pair<unsigned int, unsigned int> >::const_iterator it=QIndex.begin(); it!=QIndex.end(); it++)
    {
        QKnots.insert((*it).first);
        QKnots.insert((*it).second);
    }

    //For each edge in Edges, there is a loop
    unsigned int Eindex=0;
    cout<<"Edges.size = "<<Edges.size()<<endl;

    for(unsigned int i_edge=0; i_edge < Edges.size(); i_edge++)
    {
        //a. get a part of curve from Qset and Tq (with *it)
        vector<Coordinate *> Qcurve;//the order: from *it's starting point to its endpoint
        /*Qcurve is output*/
        GetCurveOnPhysicalDomain(Eindex, QIndex, QKnots, Qset, Tq, Qcurve);
        //GetCurveOnPhysicalDomain2(Eindex, QIndex, QKnots, Qset, Tq, Qcurve);

        //b. get the parameters of the points in Qcurve which compute by the distances between points
        vector<double> QcurveParameters;//parameters from 0 to 1
        /*QcurveParameters is output*/
        GetQcurveParameters(Qcurve, QcurveParameters);

        //c. get the parameters of corresponding points on this edge (*it)
        vector<vector<double> > EdgeParameters;
        unsigned int CommCellIndex;
        /*EdgeParameters, CommCellIndex are outputs*/

        GetParametersOnEdge(Edges[i_edge], P_ParametricMesh, QcurveParameters, CommCellIndex, EdgeParameters);

        //d. get the points in Ps
        PointSet Psi;
        /*Psi is output*/
        //GetPsOnEdge(CommCellIndex, EdgeParameters, P_ParametricMap, P_ParametricMesh, Psi);
                GetPsOnEdge(CommCellIndex, EdgeParameters, Psi);

        //f. Modify Ps and Qs
        /*Ps, Qs are output*/
        ModifyPsQs(Psi, Qcurve, Ps, Qs);

        //g. Delete new
        //DeletePointers(Qcurve);
                cout<<"ComputeCorrespondencePointSets::It's Here"<<endl;
        //h:
        Eindex=Eindex+1;
    }
}

/*------------------------------------------------------------------------
  InPut: The indices of Vertices
  OutPut: The indices of Bases
--------------------------------------------------------------------------*/
void ChooseBasisIndices(const vector<unsigned int> &Vset, map<unsigned int, unsigned int> &BasisIndex)
{
    BasisIndex.clear();
    unsigned int Index=0;
    for(unsigned int i=0; i<Vset.size(); i++)
    {
        unsigned i_temp=Vset[i];
        BasisIndex.insert(pair<unsigned int, unsigned int>(i_temp*4, Index));
        Index=Index+1;
        BasisIndex.insert(pair<unsigned int, unsigned int>(i_temp*4+1, Index));
        Index=Index+1;
        BasisIndex.insert(pair<unsigned int, unsigned int>(i_temp*4+2, Index));
        Index=Index+1;
        BasisIndex.insert(pair<unsigned int, unsigned int>(i_temp*4+3, Index));
        Index=Index+1;
    }
}

//==Modify A bx by======================================================
void ModifyAbxby(Mat &A, Vec &bx, Vec &by)
{
    //unsigned int matrix_size = A.size();
    PetscInt matrix_size, MatrixSize;
    MatGetSize(A, &matrix_size, &MatrixSize);

//    PetscInt IStart, IEnd;
//    MatGetOwnershipRange(A,&IStart, &IEnd);
//    cout<<"IStart= "<<IStart<<" IEnd= "<<IEnd<<endl;

    for (PetscInt i=0; i<matrix_size; i++)//The i-th row
    {
        bool Is_modify=true;
        for (PetscInt j=0; j<matrix_size; j++)
        {
            MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);

            PetscScalar aij;
            MatGetValues(A,1, &i, 1, &j, &aij);
            if(aij<0)
            {
                aij=-aij;
            }
            if(aij>1.0e-5)
            {
                Is_modify=false;
                break;
            };
        }
        //-----------------------------------------------------
        if (Is_modify)//All the elements in the i-th row of A are zeros
        {
            PetscScalar One=1.0;
            MatSetValues(A,1,&i,1,&i,&One,INSERT_VALUES);// A[i][0]=1;//A[i][i] in fact
            PetscScalar Z=0.0;
            VecSetValues(bx,1,&i,&Z,INSERT_VALUES);//bx[i]=0;
            VecSetValues(by,1,&i,&Z,INSERT_VALUES);//by[i]=0;
        }
    }
}



//===================================================================
void UpDataParametricMap(int argc,char **args, const vector<unsigned int>& Vset, const vector<Node<unsigned int>* >& Tv,
                         const vector<Coordinate *>& Qset, const vector<Node<unsigned int>* >& Tq,
                         const Mesh *P_ParametricMesh, const map<unsigned int, unsigned int>& IVQ,
                         MSplineFunctions* &P_ParametricMap, vector<vector<double> > &Coeff_Parametric_Map)
{
    //Declare the variables
    Mat A;//
    Vec bx,by,cx,cy;
    PetscInt matrix_size;//the number of basis functions
    KSP ksp;

    vector<PointSet> Ps;
    vector<Coordinate> Qs;

    //Compute the point sets of Ps and Qs
    ComputeCorrespondencePointSets(Vset, Tv, Qset, Tq, P_ParametricMesh, P_ParametricMap, IVQ, Ps, Qs);

    //Generate the index set of bases which associate to the Vertices in Vset;
    map<unsigned int, unsigned int> BasisIndex;

    ChooseBasisIndices(Vset, BasisIndex);
    matrix_size=BasisIndex.size();

    /*----------------------------------------------------*/
    /*Initalize PETSc*/
    PetscInitialize(&argc, &args, (char*)0, NULL);
//     PetscInitialize(&argc, &args, (char*) 0, NULL);
//     PetscOptionsGetInt(NULL, "-m", &m, NULL);
//     PetscOptionsGetInt(NULL, "-n", &n, NULL);
    //-------------------------------------------
    /* Initalize Mat A */
    MatCreate(PETSC_COMM_WORLD,&A);
    MatSetSizes(A,PETSC_DECIDE, PETSC_DECIDE, matrix_size, matrix_size);
    MatSetFromOptions(A);
    MatMPIAIJSetPreallocation(A,matrix_size,NULL,matrix_size,NULL);
    MatSeqAIJSetPreallocation(A, matrix_size, NULL);
    MatSetUp(A);
//     vector<double> Ai;
//     Ai.resize(matrix_size, 0.0);
//     for (unsigned int i=0; i<matrix_size; i++)
//     {
//         A.push_back(Ai);
//     }

    /* Initalize Vec bx */
    VecCreate(PETSC_COMM_WORLD, &bx);
    VecSetSizes(bx, PETSC_DECIDE, matrix_size);
    VecSetFromOptions(bx);
    //bx.resize(matrix_size,0.0);
    /* Initalize Vec by */
    VecCreate(PETSC_COMM_WORLD, &by);
    VecSetSizes(by, PETSC_DECIDE, matrix_size);
    VecSetFromOptions(by);
    //by.resize(matrix_size,0.0);
    /* Initalize Vec cx */
    VecCreate(PETSC_COMM_WORLD, &cx);
    VecSetSizes(cx, PETSC_DECIDE, matrix_size);
    VecSetFromOptions(cx);
    //cx.resize(matrix_size,0.0);
    /* Initalize Vec cy */
    VecCreate(PETSC_COMM_WORLD, &cy);
    VecSetSizes(cy, PETSC_DECIDE, matrix_size);
    VecSetFromOptions(cy);
    //cy.resize(matrix_size,0.0);
    //--------------------------------------------

    /*--------------------------------------------
      InPut: the point set Qs in physcial domain
      OutPut: Qs=Qs-(P_ParametricMap)[Ps]
    ----------------------------------------------*/
    ModifyQs(Ps, Qs, P_ParametricMap, P_ParametricMesh);
    /*--------------------------------------------
      InPut: Ps(Points in the parametric domain),
             Qs(Points in the physical domain, denote as [x,y]),
             P_ParametricMesh(The parametric mesh)
      OutPut:A=\sum_{Ps[i]\in Ps} [Bk(Ps[i])Bl(Ps[i])]
             bx=\sum_{i}xi*B(i); where B(i)=[B1(Ps[i]), B2(Ps[i]), ..., Bn(Ps[i])]^T
             by=\sum_{i}yi*B(i); where B(i)=[B1(Ps[i]), B2(Ps[i]), ..., Bn(Ps[i])]^T
    ----------------------------------------------*/
    GenerateParametrixMatAndVec(Ps, Qs, P_ParametricMesh, BasisIndex, A, bx, by);
    //--------------------------------------------
    ModifyAbxby(A, bx, by);

    MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);

    /*Initalize KSP: ksp*/
    KSPCreate(PETSC_COMM_WORLD, &ksp);
    KSPSetOperators(ksp, A, A);
    KSPSetTolerances(ksp, 1.0e-8, 1.0e-8, PETSC_DEFAULT, PETSC_DEFAULT);
    KSPSetFromOptions(ksp);

    /*Solving Linear systems*/
    KSPSolve(ksp, bx, cx);
    KSPSolve(ksp, by, cy);


    /*-----------------------------------------
        Generate the Parametric Map again
    -------------------------------------------*/
    ReNewParametricMap(cx, cy, P_ParametricMesh, P_ParametricMap, Coeff_Parametric_Map, BasisIndex);

    /*Destroy*/
    MatDestroy(&A);
    VecDestroy(&bx);
    VecDestroy(&by);
    VecDestroy(&cx);
    VecDestroy(&cy);
    KSPDestroy(&ksp);

    /*Finish PETSc program*/
    PetscFinalize();
}

//==============================================================================================
//The following functions for mspP_FEM
//==============================================================================================
/*----------------------------------------------------------------------------------------------
  InPut: MSplineFunctions* P_ParametricMap: the original parametrization
         map<unsigned int, pair<unsigned int ,Coordinate*> > CurrentVerticesIndexToCoordinate
         CurrentVerticesEdges: on the P_FEMMesh, the focused edges
         vector<vector<Coordinate *> > Qs: the points on the physical domain
         Mesh* P_FEMMesh: the current mesh
  OutPut: MSplineFunction * P_RefinedParametricMap --> the new parametrization
          vector<vector<double> > CoeffRefinedParametricMap --> the new parametrization's coeff over P_EFMMesh
------------------------------------------------------------------------------------------------*/
void ChangeStructureCurrentVerticesIndexToCoordinate(const map<unsigned int, pair<unsigned int ,Coordinate*> > &CurrentVerticesIndexToCoordinate,
                                                    const set<unsigned int>& FocusedVertex,
                                                     map<unsigned int, pair<vector<unsigned>, vector<Coordinate *> > > &VerticesCoordinate)
{
    for(map<unsigned int, pair<unsigned int ,Coordinate*> >::const_iterator it=CurrentVerticesIndexToCoordinate.begin();
        it!=CurrentVerticesIndexToCoordinate.end(); it++)
    {
        unsigned int currentVindex=(*it).first;
        unsigned int originalCindex=((*it).second).first;
        Coordinate * P_CoorV=((*it).second).second;


        map<unsigned int, pair<vector<unsigned int>, vector<Coordinate *> > >::iterator
                itV= VerticesCoordinate.find(originalCindex);
        set<unsigned int>::const_iterator itFV=FocusedVertex.find(currentVindex);
        if (itFV==FocusedVertex.end())
        {
            if(itV==VerticesCoordinate.end())
            {
                vector<unsigned int> VIndex;
                VIndex.push_back(currentVindex);
                vector<Coordinate *> VCoor;
                VCoor.push_back(P_CoorV);
                VerticesCoordinate.insert(pair<unsigned int, pair<vector<unsigned int>, vector<Coordinate *> > >
                        (originalCindex, pair<vector<unsigned int>, vector<Coordinate *> > (VIndex, VCoor) ));
            }
            else
            {
//                unsigned int currentVindex=(*it).first;
//                Coordinate * P_CoorV=((*it).second).second;

                (((*itV).second).first).push_back(currentVindex);
                (((*itV).second).second).push_back(P_CoorV);
            }
        }
    }
}

void ComputerCoeffDirectly(const unsigned int OriginalCIndex,
                           MSplineFunctions* P_ParametricMap,
                           const vector<unsigned int>& VIndex,
                           vector<Coordinate *>& VCoordinate,
                           const Mesh * P_ParametricMesh,
                           vector<vector<double> > & CoeffRefinedParametricMap)
{
    vector<vector<double> > CoeffInitialValue;
    P_ParametricMap->Evaluation(0,0,VCoordinate,OriginalCIndex, CoeffInitialValue, *P_ParametricMesh);
    vector<vector<double> > CoeffInitialS;
    P_ParametricMap->Evaluation(1,0,VCoordinate,OriginalCIndex, CoeffInitialS, *P_ParametricMesh);
    vector<vector<double> > CoeffInitialT;
    P_ParametricMap->Evaluation(0,1,VCoordinate,OriginalCIndex, CoeffInitialT, *P_ParametricMesh);
    vector<vector<double> > CoeffInitialST;
    P_ParametricMap->Evaluation(1,1,VCoordinate,OriginalCIndex, CoeffInitialST, *P_ParametricMesh);

    for (unsigned int i=0; i<VIndex.size(); i++)
    {
        double x,y;
        //CoeffRefinedParametricMap[0][VIndex[i]*4], CoeffRefinedParametricMap[1][VIndex[i]*4]
        x=CoeffInitialValue[0][i];
        y=CoeffInitialValue[1][i];
        CoeffRefinedParametricMap[0][VIndex[i]*4]=x;
        CoeffRefinedParametricMap[1][VIndex[i]*4]=y;

        //CoeffRefinedParametricMap[0][VIndex[i]*4+1] CoeffRefinedParametricMap[1][VIndex[i]*4+1]


        x=CoeffInitialS[0][i];
        y=CoeffInitialS[1][i];
        CoeffRefinedParametricMap[0][VIndex[i]*4+1]=x;
        CoeffRefinedParametricMap[1][VIndex[i]*4+1]=y;

        //CoeffRefinedParametricMap[0][VIndex[i]*4+2] CoeffRefinedParametricMap[1][VIndex[i]*4+2]



        x=CoeffInitialT[0][i];
        y=CoeffInitialT[1][i];
        CoeffRefinedParametricMap[0][VIndex[i]*4+2]=x;
        CoeffRefinedParametricMap[1][VIndex[i]*4+2]=y;

        //CoeffRefinedParametricMap[0][VIndex[i]*4+3] CoeffRefinedParametricMap[1][VIndex[i]*4+3]
//        cout<<"It is here!!!"<<VIndex[i]<<endl;
//        cout<<CoeffInitialST.size()<<endl;
//        cout<<CoeffInitialST[0].size()<<endl;

//        cout<<CoeffRefinedParametricMap.size()<<endl;
//        cout<<CoeffRefinedParametricMap[0].size()<<endl;
        x=CoeffInitialST[0][i];
        y=CoeffInitialST[1][i];
        CoeffRefinedParametricMap[0][VIndex[i]*4+3]=x;
        CoeffRefinedParametricMap[1][VIndex[i]*4+3]=y;
    }
	
}

void FillInCoeffByVerticesCoordinate(const unsigned int OriginalCIndex,
                                    MSplineFunctions* P_ParametricMap,
                                    const vector<unsigned int>& VIndex,
                                    vector<Coordinate *>& VCoordinate,
                                    const Mesh* P_FEMMesh,const Mesh * P_ParametricMesh,
                                    vector<vector<double> > & CoeffRefinedParametricMap)
{
    ComputerCoeffDirectly(OriginalCIndex, P_ParametricMap, VIndex, VCoordinate, P_ParametricMesh, CoeffRefinedParametricMap);

    for(unsigned int i = 0; i < VIndex.size(); i ++)
    {
        unsigned int IndexV = VIndex[i];
        //P_FEMMesh->P_Vertices[IndexV]->ItsCellsToCoordinatesMap
        map<unsigned int, Coordinate>::const_iterator
                itmap=P_FEMMesh->P_Vertices->at(IndexV).ItsCellsToCoordinatesMap.begin();

        unsigned int IndexCellRef=itmap->first;
        unsigned int IndexCellfirst=P_FEMMesh->P_Cells->at(IndexCellRef).ItsParent;


        if (IndexCellfirst!=OriginalCIndex)//Modify CoeffRefinedParametricMap[4*VIndex[i]+j][?], j=0, 1, 2, 3
        {
            //HermiteTransferMatrix H((P_ParametricMesh->P_FrameTranMatrix->at(OriginalCIndex))[IndexCellfirst]);
            HermiteTransferMatrix H((P_ParametricMesh->P_FrameTranMatrix->at(IndexCellfirst))[OriginalCIndex]);
            //x:
            HermiteData Hfirstx;
            //Hfirstx.Index_InitialCell=IndexCellfirst;
            Hfirstx.Index_InitialCell=OriginalCIndex;
            Hfirstx.ItsHermiteData.push_back(CoeffRefinedParametricMap[0][4*IndexV]);
            Hfirstx.ItsHermiteData.push_back(CoeffRefinedParametricMap[0][4*IndexV+1]);
            Hfirstx.ItsHermiteData.push_back(CoeffRefinedParametricMap[0][4*IndexV+2]);
            Hfirstx.ItsHermiteData.push_back(CoeffRefinedParametricMap[0][4*IndexV+3]);
            HermiteData Hx;
            //Hx.Index_InitialCell=OriginalCIndex;
            Hx.Index_InitialCell=IndexCellfirst;
            H.HermiteDataTransfer(Hx, Hfirstx);
            CoeffRefinedParametricMap[0][4*IndexV]=Hx.ItsHermiteData[0];
            CoeffRefinedParametricMap[0][4*IndexV+1]=Hx.ItsHermiteData[1];
            CoeffRefinedParametricMap[0][4*IndexV+2]=Hx.ItsHermiteData[2];
            CoeffRefinedParametricMap[0][4*IndexV+3]=Hx.ItsHermiteData[3];

            //y:
            HermiteData Hfirsty;
            //Hfirsty.Index_InitialCell=IndexCellfirst;
            Hfirsty.Index_InitialCell=OriginalCIndex;
            Hfirsty.ItsHermiteData.push_back(CoeffRefinedParametricMap[1][4*IndexV]);
            Hfirsty.ItsHermiteData.push_back(CoeffRefinedParametricMap[1][4*IndexV+1]);
            Hfirsty.ItsHermiteData.push_back(CoeffRefinedParametricMap[1][4*IndexV+2]);
            Hfirsty.ItsHermiteData.push_back(CoeffRefinedParametricMap[1][4*IndexV+3]);
            HermiteData Hy;
            //Hy.Index_InitialCell=OriginalCIndex;
            Hy.Index_InitialCell=IndexCellfirst;
            H.HermiteDataTransfer(Hy, Hfirsty);
            CoeffRefinedParametricMap[1][4*IndexV]=Hy.ItsHermiteData[0];
            CoeffRefinedParametricMap[1][4*IndexV+1]=Hy.ItsHermiteData[1];
            CoeffRefinedParametricMap[1][4*IndexV+2]=Hy.ItsHermiteData[2];
            CoeffRefinedParametricMap[1][4*IndexV+3]=Hy.ItsHermiteData[3];
        }
    }

}

MSplineFunctions* ReExpressionParametrization(const unsigned int k, const unsigned int pho, MSplineFunctions * P_ParametricMap,
                                const map<unsigned int, pair<unsigned int ,Coordinate* > > &CurrentVerticesIndexToCoordinate,
                                const vector<vector<unsigned int> > &CurrentVerticesEdges,
                                const vector<vector<Coordinate *> > &Qs,
                                const Mesh* P_FEMMesh, const Mesh * P_ParametricMesh,
                                 vector<vector<double> > &CoeffRefinedParametricMap)
{//CoeffRefinedParametricMap=[x,y]; x=[x0, x1, x2,..., xn], y=[y0, y1, y2, ..., yn]
    vector<double> Coeffi;
    Coeffi.resize(4*P_FEMMesh->P_Vertices->size(), 0.0);
    CoeffRefinedParametricMap.clear();
    CoeffRefinedParametricMap.push_back(Coeffi);
    CoeffRefinedParametricMap.push_back(Coeffi);


    set<unsigned int> FocusedVertex;
    //////////////////////////////////
    //Part1: Qs, CurrentVerticesEdges to fill CoeffRefinedParametricMap
    //For Ps, modified Ps[i]'s cellindex
    for(unsigned int i=0; i< Qs.size(); i++)//Qs and CurrentVerticesEdges are corresponding to each other
    {
        unsigned int j=0;
        for(unsigned int indexJ=0; indexJ < CurrentVerticesEdges[i].size(); indexJ++)
        {
            double x = Qs[i][j]->xy[0];
            double y = Qs[i][j]->xy[1];

            unsigned int IndexVertex = CurrentVerticesEdges[i][indexJ];

            set<unsigned int>::const_iterator itset=FocusedVertex.find(IndexVertex);
            if(itset==FocusedVertex.end())
            {
                CoeffRefinedParametricMap[0][IndexVertex*4] = x;
                CoeffRefinedParametricMap[1][IndexVertex*4] = y;

                FocusedVertex.insert(IndexVertex);
            }
            j=j+pho+1;
        }
        //----------------------------------------------------------

    }

    //Part2: P_ParametricMap, CurrentVerticesIndexToCoordinate, P_FEMMesh to fill CoeffRefinedParametricMap
    //---a. Change the structure of CurrentVerticesIndexToCoordinate to:
    //map<unsigned int, pair<vector<unsigned>, vector<Coordinate *> > >:
    //original cell index, pair(new vertex indices, their coordinates)

    map<unsigned int, pair<vector<unsigned int>, vector<Coordinate *> > > VerticesCoordinate;
    ChangeStructureCurrentVerticesIndexToCoordinate(CurrentVerticesIndexToCoordinate, FocusedVertex, VerticesCoordinate);

//    cout<<"VerticesCoordinate: "<<endl;
//    for(map<unsigned int, pair<vector<unsigned int>, vector<Coordinate *> > >::const_iterator
//        itV=VerticesCoordinate.begin(); itV!=VerticesCoordinate.end(); itV++)
//    {
//        cout<<"the orginal cellindex= "<<itV->first<<endl;
//        cout<<"The vertices in this cell and their coordinates"<<endl;
//        for(unsigned int i_test=0; i_test<(itV->second).first.size() ; i_test++)
//        {
//            cout<<(itV->second).first[i_test]<<" ("<<(itV->second).second[i_test]->xy[0]<<", "<<(itV->second).second[i_test]->xy[1]<<" )"<<endl;
//        }
//    }

    //---b.
    for(map<unsigned int, pair<vector<unsigned int>, vector<Coordinate *> > >::iterator
        it = VerticesCoordinate.begin(); it != VerticesCoordinate.end(); it++)
    {
        unsigned int OriginalCIndex=(*it).first;

        //fill in CoeffRefinedParametricMap

        FillInCoeffByVerticesCoordinate(OriginalCIndex, P_ParametricMap,
                                        ((*it).second).first, ((*it).second).second,
                                        P_FEMMesh, P_ParametricMesh, CoeffRefinedParametricMap);
    }



    //---c: generate MSplineFunctions* P_RefinedParametricMap
    MSplineFunctions * PFun=new MSplineFunctions(CoeffRefinedParametricMap, *P_FEMMesh);

    return PFun;

}


void GenerateVSet_ModifiedPsQs(unsigned int k, unsigned int pho, const Mesh *P_FEMMesh,
                             const vector<vector<unsigned int> >& CurrentVerticesEdges,
                             const vector<PointSet> &Ps, const vector< vector<Coordinate *> > &Qs,
                             set<unsigned int> &VSet, vector<PointSet> & ModifiedPs,
                             vector< vector<Coordinate *> > & ModifiedQs)
{
    ModifiedPs.clear();
    ModifiedQs.clear();


    for (unsigned int i=0; i<CurrentVerticesEdges.size(); i++)
    {
        unsigned int indexj;
        for(indexj=0; indexj<CurrentVerticesEdges[i].size()-1; indexj++)
        {
            //--------Modified Ps
            PointSet MPsi;
            vector<Coordinate *> MQsi;
            //Determind MPsi.index_cell
            //by the vertices with the indices CurrentVerticesEdges[i][indexj] and [i][indexj+pho+1]
            unsigned int v1=CurrentVerticesEdges[i][indexj];
            unsigned int v2=CurrentVerticesEdges[i][indexj+1];
            set<unsigned int> Cellindex1, Cellindex2, IndexIntersection;
            Map2set_unsigned_int(Cellindex1, P_FEMMesh->P_Vertices->at(v1).ItsCellsToCoordinatesMap);
            Map2set_unsigned_int(Cellindex2, P_FEMMesh->P_Vertices->at(v2).ItsCellsToCoordinatesMap);
            set_intersection(Cellindex1.begin(), Cellindex1.end(), Cellindex2.begin(), Cellindex2.end(), inserter(IndexIntersection, IndexIntersection.begin()));


            if (IndexIntersection.size())
            {
                MPsi.Index_ItsCell=*(IndexIntersection.begin());

                for (unsigned int iPs=0; iPs<pho+2; iPs++)
                {
                    MPsi.P_coordinates_Ps.push_back(Ps[i].P_coordinates_Ps[indexj*(pho+1)+iPs]);
                    Coordinate * PQ1=new Coordinate(*Qs[i][indexj*(pho+1)+iPs]);
                    MQsi.push_back(PQ1);

                    //VSet:
                    VSet.insert(CurrentVerticesEdges[i][indexj]);

                }

                ModifiedPs.push_back(MPsi);
                ModifiedQs.push_back(MQsi);
            }
            else
            {
                cout<<"PMap_Functions.h::GenerateVSet_ModifiedPsQs::ModifyRefinedParametrization:: IndexInterSection.size=0"<<endl;
                cin.get();
            }

        }

    }

}

void ModifyQs_RefineParametricMap(const vector<PointSet> &Ps, vector<vector<Coordinate *> > &Qs,
                                  MSplineFunctions* P_ParametricMap, const Mesh* P_ParametricMesh)
{
//    cout<<"ModifyQs_RefineParametricMap::Qs"<<endl;
//    for(unsigned int i=0; i<Qs.size(); i++)
//    {
//        for(unsigned int j=0; j<Qs[i].size(); j++)
//        {
//            cout<<"Qs["<<i<<"]["<<j<<"] = ("<<Qs[i][j]->xy[0]<<", "<<Qs[i][j]->xy[1]<<")"<<endl;

//            cin.get();
//        }
//    }


    //cout<<"P_ParametricMap->P_MSplineFunctions.size() = "<<P_ParametricMap->P_MSplineFunctions.size()<<endl;
    for(unsigned int i=0; i<Ps.size(); i++)
    {
        vector<vector<double> > Values;
        P_ParametricMap->Evaluation(0,0,Ps[i].P_coordinates_Ps, Ps[i].Index_ItsCell, Values, *P_ParametricMesh);
        if(Values.size()>1)
        {
            for(unsigned int j=0; j<Values[0].size(); j++)
            {
//                cout<<"1:Qs["<<i<<"]["<<j<<"] = ("<<Qs[i][j]->xy[0]<<", "<<Qs[i][j]->xy[1]<<")"<<endl;
//                cout<<"-->    - ("<<Values[0][j]<<", "<<Values[1][j]<<")"<<endl;

                Qs[i][j]->xy[0]=Qs[i][j]->xy[0]-Values[0][j];
                Qs[i][j]->xy[1]=Qs[i][j]->xy[1]-Values[1][j];

//                cout<<"--------------------------------------------"<<endl;
//                cout<<"2:Qs["<<i<<"]["<<j<<"] = ("<<Qs[i][j]->xy[0]<<", "<<Qs[i][j]->xy[1]<<")"<<endl;
//                cin.get();
//                cout<<endl;
            }
        }
        else
        {
            cout<<"ModifyQs_RefineParametricMap::the dimension of parametric map is less than 2!"<<endl;
            cin.get();
        }
    }
}

void GenerateRefinedParametricMatAndVec(const vector<PointSet> &Ps, const vector<vector<Coordinate*> > &Qs,
                                        const Mesh* P_ParametricMesh, map<unsigned int, unsigned int> &BasisIndex,
                                        Mat& A, Vec& bx, Vec& by)
{
    PetscInt matrix_size, MatrixSize;
    MatGetSize(A, &matrix_size, &MatrixSize);
//    cout<<"sizeA="<<matrix_size<<"X"<<MatrixSize<<endl;

//    cout<<"Here1"<<endl;
//    cin.get();

    //A=0; bx=0; by=0---------------------
    MatZeroEntries(A);
    VecSet(bx, 0.0);
    VecSet(by, 0.0);
    //------------------------------------

    //unsigned int PointIndex = 0;
    //------------------------------------
    for(unsigned int i_Ps = 0; i_Ps < Ps.size(); i_Ps ++)
    {
        unsigned int CellIndex = Ps[i_Ps].Index_ItsCell;
        BasesCell * P_BC = new BasesCell(CellIndex, *P_ParametricMesh);

        vector<vector<double> > Values;
        for(unsigned int i_basis = 0; i_basis < P_BC->P_Bases_Cell.size(); i_basis ++)
        {
            unsigned int basisIndex = P_BC->P_Bases_Cell[i_basis]->Index_basis;
            map<unsigned int, unsigned int>::const_iterator itBasis = BasisIndex.find(basisIndex);
            vector<double> Value_ibasis;
            if(itBasis == BasisIndex.end())
            {
                 Value_ibasis.resize(Ps[i_Ps].P_coordinates_Ps.size(),0.0);
            }
            else
            {
               P_BC->P_Bases_Cell[i_basis]->Evaluation(0, 0, Ps[i_Ps].P_coordinates_Ps, CellIndex, Value_ibasis, *P_ParametricMesh);
            }
            Values.push_back(Value_ibasis);
        }

        for(unsigned int iPsi_Ps = 0; iPsi_Ps < Ps[i_Ps].P_coordinates_Ps.size(); iPsi_Ps ++)
        {
            //-------------------------
            double xi=Qs[i_Ps][iPsi_Ps]->xy[0];
            double yi=Qs[i_Ps][iPsi_Ps]->xy[1];
            //cout<<"yi= "<<yi<<endl;

            //B, for Ps[i_Ps].P_coorinates_Ps[iPsi_Ps]
            for(unsigned int i_basis=0; i_basis< P_BC->P_Bases_Cell.size(); i_basis++)
            {
                //bx, by
                unsigned int BasisIndex1=P_BC->P_Bases_Cell[i_basis]->Index_basis;
                map<unsigned int, unsigned int>::const_iterator itBasis=BasisIndex.find(BasisIndex1);
                if(itBasis!=BasisIndex.end())//
                {
                    PetscInt Index=(*itBasis).second;

                    PetscScalar Bpi=xi*Values[i_basis][iPsi_Ps];
                    /*bx[Index]=bx[Index]+Bpi;*/
                    VecSetValues(bx, 1, &Index, &Bpi, ADD_VALUES);

                    Bpi=yi*Values[i_basis][iPsi_Ps];
                    /*by[Index]=by[Index]+Bpi;*/
                    VecSetValues(by, 1, &Index, &Bpi, ADD_VALUES);

                    //A
                    for(unsigned int j_basis=0; j_basis< P_BC->P_Bases_Cell.size(); j_basis++)
                    {
                        unsigned int BasisIndex2=P_BC->P_Bases_Cell[j_basis]->Index_basis;
                        map<unsigned int, unsigned int>::const_iterator jtBasis=BasisIndex.find(BasisIndex2);
                        if(jtBasis!=BasisIndex.end())//
                        {
                            PetscScalar Bqi=Values[i_basis][iPsi_Ps]*Values[j_basis][iPsi_Ps];
                            PetscInt Index2=(*jtBasis).second;
                            //A[Index][Index2]=A[Index][Index2]+Bqi;
                            MatSetValues(A, 1, &Index, 1, &Index2, &Bqi, ADD_VALUES);
                        }
                    }
                    //-----
                }
            }
            //-------------------------
            //PointIndex=PointIndex+1;
        }
        delete P_BC;
    }
}


void ModifyRefinedParametrization(int argc,char **args, unsigned int k, unsigned int pho,
                                  const vector<vector<unsigned int> >& CurrentVerticesEdges,
                                  const Mesh *P_FEMMesh, vector<vector<double> > &CoeffRefinedParametricMap,
                                  const vector<PointSet> &Ps, const vector<vector<Coordinate *> > &Qs,
                                  vector<PointSet> & ModifiedPs, vector< vector<Coordinate *> > & ModifiedQs,
                                  MSplineFunctions* &P_RefinedParametricMap)
{

   // PetscInitialize(&argc, &args, (char*)0, NULL);
        //Declare the variables
        Mat A;//
        Vec bx, by, cx, cy;
        PetscInt matrix_size;//the number of basis functions
        KSP ksp;


    //Generate the index set of bases which associate to the Vertices in Vset;
    set<unsigned int> VSet;

    //cout<<"Before:GenerateVSet_ModifiedPsQs()"<<endl;
    GenerateVSet_ModifiedPsQs(k, pho, P_FEMMesh, CurrentVerticesEdges, Ps, Qs, VSet, ModifiedPs, ModifiedQs);
    //cout<<"End:GenerateVSet_ModifiedPsQs()"<<endl;



    map<unsigned int, unsigned int> BasisIndex;
    unsigned int Index=0;
    for (set<unsigned int>::const_iterator it=VSet.begin(); it!=VSet.end(); it++)
    {
        unsigned i_temp=*it;//VSet[i];
//        cout<<"Index of Basis Vertex = "<<i_temp<<endl;

        BasisIndex.insert(pair<unsigned int, unsigned int>(i_temp*4, Index));
        Index=Index+1;
        BasisIndex.insert(pair<unsigned int, unsigned int>(i_temp*4+1, Index));
        Index=Index+1;
        BasisIndex.insert(pair<unsigned int, unsigned int>(i_temp*4+2, Index));
        Index=Index+1;
        BasisIndex.insert(pair<unsigned int, unsigned int>(i_temp*4+3, Index));
        Index=Index+1;
    }

    matrix_size=BasisIndex.size();
    /*----------------------------------------------------*/
    /*Initalize PETSc*/
 //   PetscInitialize(&argc, &args, (char*)0, NULL);


    //-------------------------------------------
    /* Initalize Mat A */
    MatCreate(PETSC_COMM_WORLD,&A);
    MatSetSizes(A,PETSC_DECIDE, PETSC_DECIDE, matrix_size, matrix_size);
    MatSetFromOptions(A);
    MatMPIAIJSetPreallocation(A,matrix_size,NULL,matrix_size,NULL);
    MatSeqAIJSetPreallocation(A, matrix_size, NULL);
    MatSetUp(A);


    /* Initalize Vec bx */
    VecCreate(PETSC_COMM_WORLD, &bx);
    VecSetSizes(bx, PETSC_DECIDE, matrix_size);
    VecSetFromOptions(bx);
 
    /* Initalize Vec by */
    VecCreate(PETSC_COMM_WORLD, &by);
    VecSetSizes(by, PETSC_DECIDE, matrix_size);
    VecSetFromOptions(by);
 
    /* Initalize Vec cx */
    VecCreate(PETSC_COMM_WORLD, &cx);
    VecSetSizes(cx, PETSC_DECIDE, matrix_size);
    VecSetFromOptions(cx);

    /* Initalize Vec cy */
    VecCreate(PETSC_COMM_WORLD, &cy);
    VecSetSizes(cy, PETSC_DECIDE, matrix_size);
    VecSetFromOptions(cy);

    //--------------------------------------------

    /*--------------------------------------------
      InPut: the point set Qs in physcial domain
      OutPut: ModifiedQs=ModifiedQs-(P_ParametricMap)[ModifiedPs]
    ----------------------------------------------*/
//    cout<<"1. ModifiedQs"<<endl;
//    for(unsigned int i_test=0; i_test<ModifiedQs.size(); i_test++)
//    {
//        for(unsigned int j_test=0; j_test<ModifiedQs[i_test].size(); j_test++)
//        {
//            cout<<"i_test-->"<<i_test<<" ("<<ModifiedQs[i_test][j_test]->xy[0]<<", "<<ModifiedQs[i_test][j_test]->xy[1]<<")"<<endl;
//        }
//    }

    ModifyQs_RefineParametricMap(ModifiedPs, ModifiedQs, P_RefinedParametricMap, P_FEMMesh);

//    cout<<"2. ModifiedQs"<<endl;
//    for(unsigned int i_test=0; i_test<ModifiedQs.size(); i_test++)
//    {
//        for(unsigned int j_test=0; j_test<ModifiedQs[i_test].size(); j_test++)
//        {
//            cout<<"i_test-->"<<i_test<<" ("<<ModifiedQs[i_test][j_test]->xy[0]<<", "<<ModifiedQs[i_test][j_test]->xy[1]<<")"<<endl;
//        }
//    }
//    cin.get();
    /*--------------------------------------------
      InPut: Ps(Points in the parametric domain),
             Qs(Points in the physical domain, denote as [x,y]),
             P_ParametricMesh(The parametric mesh)
      OutPut:A=\sum_{Ps[i]\in Ps} [Bk(Ps[i])Bl(Ps[i])]
             bx=\sum_{i}xi*B(i); where B(i)=[B1(Ps[i]), B2(Ps[i]), ..., Bn(Ps[i])]^T
             by=\sum_{i}yi*B(i); where B(i)=[B1(Ps[i]), B2(Ps[i]), ..., Bn(Ps[i])]^T
    ----------------------------------------------*/

    //cout<<"Before:GenerateRefinedParametricMatAndVec()"<<endl;
    //--------------------------------------------
    GenerateRefinedParametricMatAndVec(ModifiedPs, ModifiedQs, P_FEMMesh, BasisIndex, A, bx, by);
    //--------------------------------------------
   // cout<<"After:GenerateRefinedParametricMatAndVec()"<<endl;


    ModifyAbxby(A, bx, by);

   //ModifyAbxbyInterpolation(A, bx, by, BasisIndex, ModifiedQs, VSet);



    MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);

    //MatSetOption(A, MAT_SYMMETRIC, PETSC_TRUE);

//    cout<<"A matrix"<<endl;
//    MatView(A, PETSC_VIEWER_STDOUT_SELF);
//    cin.get();

//    MatView(A, PETSC_VIEWER_DRAW_WORLD);
//    cin.get();

//    cout<<"bx matrix"<<endl;
//    VecView(bx, PETSC_VIEWER_STDOUT_SELF);

//    cin.get();

//    cout<<"by matrix"<<endl;
//    VecView(by, PETSC_VIEWER_STDOUT_SELF);
//    cin.get();

    cout<<"Before Initalize KSP"<<endl;

    /*Initalize KSP: ksp*/
    KSPCreate(PETSC_COMM_WORLD, &ksp);
    KSPSetOperators(ksp, A, A);
    //KSPSetTolerances(ksp, 1.0e-8, 1.0e-8, PETSC_DEFAULT, PETSC_DEFAULT);
    KSPSetTolerances(ksp, 1.0e-4, 1.0e-4, PETSC_DEFAULT, PETSC_DEFAULT);
    KSPSetFromOptions(ksp);

    cout<<"After Initalize KSP"<<endl;
    /*Solving Linear systems*/
    KSPSolve(ksp, bx, cx);
    KSPSolve(ksp, by, cy);

    cout<<"After Solving linear systems"<<endl;


//    cout<<"A"<<endl;
//    MatView(A,	PETSC_VIEWER_STDOUT_SELF);
//    cout<<"cx"<<endl;
//    VecView(cx, PETSC_VIEWER_STDOUT_SELF);
//    cout<<"cy"<<endl;
//    VecView(cy, PETSC_VIEWER_STDOUT_SELF);

//    cout<<"BasisIndex"<<endl;
//    for(map<unsigned int, unsigned int>::const_iterator itB=BasisIndex.begin(); itB!=BasisIndex.end(); itB++)
//    {
//        cout<<itB->first<<" "<<itB->second<<endl;
//    }

//    cout<<"1: CoeffRefinedParametricMap"<<endl;
//    for(unsigned int i=0; i<CoeffRefinedParametricMap[0].size(); i++)
//    {
//        cout<<i<<"-->"<<CoeffRefinedParametricMap[0][i]<<" "<<CoeffRefinedParametricMap[1][i]<<endl;
//    }
    /*-----------------------------------------
        Generate the Parametric Map again
    -------------------------------------------*/
    ReNewParametricMap(cx, cy, P_FEMMesh, P_RefinedParametricMap, CoeffRefinedParametricMap, BasisIndex);

//    cout<<"2: CoeffRefinedParametricMap"<<endl;
//    for(unsigned int i=0; i<CoeffRefinedParametricMap[0].size(); i++)
//    {
//        cout<<i<<"-->"<<CoeffRefinedParametricMap[0][i]<<" "<<CoeffRefinedParametricMap[1][i]<<endl;
//    }

    /*Destroy*/
    MatDestroy(&A);
    VecDestroy(&bx);
    VecDestroy(&by);
    VecDestroy(&cx);
    VecDestroy(&cy);
    KSPDestroy(&ksp);

    /*Finish PETSc program*/
    //PetscFinalize();
}


//Errors of Map
void ParametricMapError(MSplineFunctions* P_RefinedParametricMap, Mesh *P_FEMMesh,
                        const vector<PointSet> & ModifiedPs,
                        const vector< vector<Coordinate *> > ModifiedQs,
                        double &maxErrorMap, vector<double>& ErrorMap)
{
    ErrorMap.clear();
    maxErrorMap=0.0;

    double x, y;

    double d=0.0;
    for(unsigned int i=0; i<ModifiedQs.size(); i++)
    {

        if(ModifiedQs[i].size()>2)
        {
            for(unsigned int j=0; j<ModifiedQs[i].size(); j++)
            {
                x=ModifiedQs[i][j]->xy[0];
                y=ModifiedQs[i][j]->xy[1];
                d=sqrt(x*x+y*y);
                ErrorMap.push_back(d);
                if(d>maxErrorMap)
                {
                    maxErrorMap=d;
                }
            }
        }
        else
        {
            unsigned int cellindex=ModifiedPs[i].Index_ItsCell;

            double s1=ModifiedPs[i].P_coordinates_Ps[0]->xy[0];
            double t1=ModifiedPs[i].P_coordinates_Ps[0]->xy[1];
            double s2=ModifiedPs[i].P_coordinates_Ps[1]->xy[0];
            double t2=ModifiedPs[i].P_coordinates_Ps[1]->xy[1];

            Coordinate * P_coor=new Coordinate;
            P_coor->xy[0]=0.5*(s1+s2);
            P_coor->xy[1]=0.5*(t1+t2);
            vector<Coordinate *> VCoor;
            VCoor.push_back(P_coor);

            vector<vector<double> > ValueXYZ;

            P_RefinedParametricMap->Evaluation(0, 0, VCoor, cellindex, ValueXYZ, *P_FEMMesh);

            x=ValueXYZ[0][0];
            y=ValueXYZ[1][0];
            d=1.0-sqrt(x*x+y*y);//Just for Example 4
            if(d<0)
            {
                d=-d;
            }
            ErrorMap.push_back(d);
            if(d>maxErrorMap)
            {
                maxErrorMap=d;
            }

            delete P_coor;

        }
    }

}

//PrintMapError(maxErrorMap, ErrorMap);
void PrintMapError(double maxErrorMap, const vector<double> ErrorMap)
{
    ofstream ofErrors;
    ofErrors.open("Errors_Map.txt");
    for (unsigned int i_error=0; i_error<ErrorMap.size(); i_error++)
    {
            ofErrors<<ErrorMap[i_error]<<" "<<endl;
    }
    ofErrors<<endl;
    ofErrors.close();

    ofstream ofTot_Error;
    ofTot_Error.open("maxError_Map.txt");
    ofTot_Error<<maxErrorMap<<endl;
    ofTot_Error.close();
}


#endif // PMAP_FUNCTIONS_H
