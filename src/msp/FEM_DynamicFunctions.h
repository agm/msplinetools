#ifndef FEM_DYNAMICFUNCTIONS_H
#define FEM_DYNAMICFUNCTIONS_H

#include "time.h"
#include <map>
#include <set>

#include "petsc.h"
#include "petscksp.h"

using namespace std;
/**\file */
//==================================

/**
 * @brief ExactFunctionValue.
 * @param x
 * @param y
 * @param ExampleIndex
 * @return
 *
 *  What is this function for ?
 *  Similarly for the functions which should be modified if the equation is not the one we want.
 *
 */
static double ExactFunctionValue(double x, double y, unsigned int ExampleIndex)
{
    double d_temp;
    switch(ExampleIndex)
    {
    case 1://Example 1_1
        d_temp=(1-(x*x+y*y))*(x*x+y*y-1.0/16.0);
        break;
    case 2://Example 2_0
        d_temp=(x-0.5)*(x-0.5)+y*y-1.0/50;
        d_temp=d_temp*((x-2.0)*(x-2.0)+y*y-0.25);
        d_temp=d_temp*(y*y-x*(x-1.0)*(x-1.0)-2.0);
        d_temp=d_temp*(x-2.0);
        break;
    case 3://Example 2_1
        d_temp=(x-0.5)*(x-0.5)+y*y-1.0/50;
        d_temp=d_temp*((x-2.0)*(x-2.0)+y*y-0.25);
        d_temp=d_temp*(y*y-x*(x-1.0)*(x-1.0)-2.0);
        d_temp=d_temp*(x-2.0);
        d_temp=d_temp*(x+0.6956);
        break;
    case 4://Example 4
        d_temp=1.0-(x*x+y*y);
        break;
    case 5://Test Example 1-->BiCubic Example
        d_temp=x*(1.0-x)*y*(1.0-y)*(0.5-x)*(0.5-y);
        break;
    case 6://Test Example 2---> GeneralSolution
        d_temp=x*(1.0-x)*y*(1.0-y)*sin(x)*cos(y);
        break;
    case 7://-div(K divF)=f: with the same mesh with Example 4
        d_temp=1.0-(x*x+y*y);
        break;
    case 8:
        d_temp=(1.0+x)*(1.0-x)*(1.0+y)*(1.0-y);
        break;
    case 9:
        d_temp=(1.0+x)*(1.0-x)*(1.0+y)*(1.0-y);
        break;
    case 10:
        d_temp=(1.0-(x*x+y*y))*x*y;
        break;
    case 11:
        d_temp=(1.0-(x*x+y*y))*(1.0-(x*x+y*y));
        break;
    case 12:
        d_temp=1.0-(x*x+y*y);
        break;
    case 13:
        d_temp=1.0-(x*x+y*y);
        break;
    case 14:
        d_temp=1.0-(x*x+y*y);
        break;
    case 15:
        d_temp=(1.0-x*x)*(1.0-y*y);
        break;
    case 16:
        d_temp=1.0-(x*x+y*y);
        break;
    case 17:
        d_temp=1.0-(x*x+y*y);
        break;
    case 18:
        d_temp=1.0-(x*x+y*y);
        break;
    case 19:
        d_temp=1.0-(x*x+y*y);
        break;
    case 20:
        d_temp=1.0-(x*x+y*y);
        break;
    case 21:
        d_temp=1.0-(x*x+y*y);
        break;
    case 22:
        d_temp=1.0-(x*x+y*y);
        break;
    case 23:
        d_temp=1.0-(x*x+y*y);
        break;
    case 24://Example 2_2
        d_temp=(x-0.5)*(x-0.5)+y*y-1.0/50;
        d_temp=d_temp*((x-2.0)*(x-2.0)+y*y-0.25);
        d_temp=d_temp*(y*y-x*(x-1.0)*(x-1.0)-2.0);
        d_temp=d_temp*(x-2.0);
        d_temp=d_temp*(x+0.6956);
        break;
    case 25:
        d_temp=(1.0-x*x)*(1.0-y*y);//For all 3 equations, they have the same exact solutions.
        break;

    case 27:// is the same with case 24
        d_temp=(x-0.5)*(x-0.5)+y*y-1.0/50;
        d_temp=d_temp*((x-2.0)*(x-2.0)+y*y-0.25);
        d_temp=d_temp*(y*y-x*(x-1.0)*(x-1.0)-2.0);
        d_temp=d_temp*(x-2.0);
        d_temp=d_temp*(x+0.6956);
        break;

    case 28://Example 6
        d_temp=(x*x+1.0-y)*(y-x*x)*(x+1.0)*(1.0-x);
        break;

    case 30://Example 6 non-trivial
        d_temp=(x*x+1.0-y)*(y-x*x)*(x+1.0)*(1.0-x)*sin(x);
        break;


    case 31:
        d_temp=(-2.0*x+2.0-y)*(2.0*x+2.0-y)*y*sin(x);
        break;

    case 32:
        d_temp=(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x-2.0)*((x-2.0)*(x-2.0)+y*y-0.25);
        break;

    case 37:
        double F1, F2, F3, F4, F5, F6, F7, F8, F9;
        F1=y-(3.0-3.0*((5.0-x)/2.0)+2.0*((5.0-x)*(5.0-x))/4.0);

        F2=y-(2.0+(3.0-x)/2.0+(3.0-x)*(3.0-x)-(3.0-x)*(3.0-x)*(3.0-x)/2.0);

        F3=x-(1.0-2.0*(3.0-y)/3.0-5.0*(1.0-y/3.0)*(1.0-y/3.0)+4.0*((3.0-y)/3.0)*((3.0-y)/3.0)*((3.0-y)/3.0));

        F4=x-(-2.0+7.0*y*y/9.0+4.0*(y/3.0)*(y/3.0)*(y/3.0));

        F5=y-(-3.0-3.0*(x-1.0)/2.0+2.0*(x-1.0)*(x-1.0)-(x-1.0)*(x-1.0)*(x-1.0)/2.0);

        F6=y-(-2.0+(x-3.0)/2.0-(x-3.0)*(x-3.0)/2.0);

        F7=x-(4.0+y*y);

        F8=x-(5.0-2.0*(1.0-y)+(1.0-y)*(1.0-y));

        F9=5.0-x;

        d_temp=F1*F2*F3*F4*F5*F6*F7*F8*F9*(x+2.0)*(x+1.0)/10000.0;


        /*double F1, F2, F3, F4, F5, F6, F7;
        F1=-0.25*(x-1.0)*(x-1.0)*(x-1.0)+1.25*(x-1.0)*(x-1.0)-(x-1.0)+y-3.0;
        F2=1.0/9.0*y*y*y-2.0/3.0*y*y+x+2.0;
        F3=-1.0/9.0*y*y*y-2.0/3.0*y*y+x+2.0;
        F4=0.25*(x-1.0)*(x-1.0)*(x-1.0)-1.25*(x-1.0)*(x-1.0)+(x-1.0)+y+3.0;
        F5=(1.0+y)*(1.0+y)*(1.0+y)-(1.0+y)*(1.0+y)-(1.0+y)-x+5.0;
        F6=(1.0-y)*(1.0-y)*(1.0-y)-(1.0-y)*(1.0-y)-(1.0-y)-x+5.0;
        F7=x-5.0;

        d_temp=x*y*F1*F2*F3*F4*F5*F6*F7/300.0;*/

        break;

    case 46:
        d_temp=0.0;//This is not the exact solution:
        break;

    case 47:
        d_temp=0.0;//This is not the exact solution
        break;

    case 48:
        d_temp=(1-x*x)*(1-y*y)*sin(x*y);
        break;

    case 49:
        d_temp=(1-x*x)*(1-y*y)*(sin(x*y)+1)*(y*y-x*x);
        break;

    case 50:
        d_temp=(1-x*x)*(1-y*y)/(1.0e-1+pow((y*y-x*x),2)*1.0e-6);
        break;

    case 52:
        d_temp=0;//Unknow this value
        break;

    case 53:
        ////Ex3_1_1
        //d_temp=y*(4.0-x*x)*(4.0-y*y)*pow(x*x+y*y,-0.25);

        ////Ex3_1_2
        //d_temp=y*pow(x*x+y*y,0.25)*(4.0-x*x)*(4.0-y*y);

        ////Ex3_1_3/Ex3_1_0
        d_temp=(4.0-x*x)*(4.0-y*y)*sin(y);

        break;

    case 54:
        //d_temp=x*(1.0-x*x)*y*(1.0-y*y)*pow(x*x+y*y,1.0/3.0);
        //d_temp=pow(x*x+y*y,1.0/3.0)*((x*y)/(x*x+y*y))*(1.0-x*x)*(1.0-y*y);

        ////Ex3_2_1
        //d_temp=(1.0-x*x)*(1.0-y*y)*x*y*pow(x*x+y*y,-2.0/3.0);

        ////Ex3_2_2
        //d_temp=pow(x*x+y*y,-0.25)*x*y*(1.0-x*x)*(1.0-y*y);

        ////Ex3_2_3/Ex3_2_0
        d_temp=(1.0-x*x)*(1.0-y*y)*x*(4.0-x*x)*y*y;

        //Test smooth
        //d_temp=(1.0-x*x)*(1.0-y*y);

        break;

    case 55:
        d_temp=exp(x/4.0 - y/4.0)*(x*x - 4.0)*(y*y - 4.0);
        break;

    case 56:
        d_temp=x*(2.0-x)*y*(2.0-y);
        break;

    default:
        cout<<"FEM_DynamicFunctions.h:: ExactFunctionValue:: ExampleIndex is out of the range!"<<endl;
        cin.get();
        break;
    }

	return d_temp;
}
//============================

//==============================
/**
 * @brief FxFunctionValue
 * @param x
 * @param y
 * @param ExampleIndex
 * @return
 */
static double FxFunctionValue(double x, double y, unsigned int ExampleIndex)
{
    double d_temp=0.0;
    switch(ExampleIndex)
    {
    case 4:
        d_temp=-2.0*x;
        break;

    case 6:
        d_temp=(1.0-x)*y*(1.0-y)*sin(x)*cos(y);
        d_temp=d_temp-x*y*(1.0-y)*sin(x)*cos(y);
        d_temp=d_temp+x*(1.0-x)*y*(1.0-y)*cos(x)*cos(y);
        break;


    case 15:
        d_temp=-2.0*x*(1.0-y*y);
        break;

    case 24:
//        double A=(x-0.5)*(x-0.5)+y*y-1.0/50;
//        double B=((x-2.0)*(x-2.0)+y*y-0.25);
//        double C=(y*y-x*(x-1.0)*(x-1.0)-2.0);
//        double D=x-2.0;
//        double E=x+0.6956;
        d_temp=(2*x)*((x-2.0)*(x-2.0)+y*y-0.25)*(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x-2.0)*(x+0.6956);
        d_temp=d_temp+((x-0.5)*(x-0.5)+y*y-1.0/50)*(2*x)*(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x-2.0)*(x+0.6956);
        d_temp=d_temp+((x-0.5)*(x-0.5)+y*y-1.0/50)*((x-2.0)*(x-2.0)+y*y-0.25)*(-(x-1)*(x-1)-2*x*(x-1))*(x-2.0)*(x+0.6956);
        d_temp=d_temp+((x-0.5)*(x-0.5)+y*y-1.0/50)*((x-2.0)*(x-2.0)+y*y-0.25)*(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x+0.6956);
        d_temp=d_temp+((x-0.5)*(x-0.5)+y*y-1.0/50)*((x-2.0)*(x-2.0)+y*y-0.25)*(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x-2.0);
        break;
    case 25:
        d_temp=-2.0*x*(1.0-y*y);
        break;

    case 27:
        d_temp=(2.0*x)*((x-2.0)*(x-2.0)+y*y-0.25)*(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x-2.0)*(x+0.6956);
        d_temp=d_temp+((x-0.5)*(x-0.5)+y*y-1.0/50.0)*(2*x)*(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x-2.0)*(x+0.6956);
        d_temp=d_temp+((x-0.5)*(x-0.5)+y*y-1.0/50.0)*((x-2.0)*(x-2.0)+y*y-0.25)*(-(x-1.0)*(x-1.0)-2.0*x*(x-1.0))*(x-2.0)*(x+0.6956);
        d_temp=d_temp+((x-0.5)*(x-0.5)+y*y-1.0/50.0)*((x-2.0)*(x-2.0)+y*y-0.25)*(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x+0.6956);
        d_temp=d_temp+((x-0.5)*(x-0.5)+y*y-1.0/50.0)*((x-2.0)*(x-2.0)+y*y-0.25)*(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x-2.0);
        break;

    case 28:
        //d_temp=2.0*x*(y-x*x)*(1.0-x*x)-2.0*x*(x*x+1.0-y)*(1.0-x*x)-2.0*x*(x*x+1.0-y)*(y-x*x);
        d_temp=-2.0*x+2.0*x*y-8.0*x*x*x*y+6.0*x*x*x*x*x+2.0*x*y*y;
        break;

    case 30:
        d_temp=(2.0*x*y-8.0*x*x*x*y+6.0*x*x*x*x*x+2.0*x*y*y-2.0*x)*sin(x)+((x*x+1.0-y)*(y-x*x)*(1.0-x*x))*cos(x);
        break;


    case 37:
        d_temp=((x + 1.0)*(x + 2.0)*(x - 5.0)*(pow(y,2.0) - x + 4.0)*(y - x/2.0 + pow(x - 3.0,2.0)/2.0 + 7.0/2.0)*(x - (7.0*pow(y,2.0))/9.0 - (4.0*pow(y,3.0))/27.0 + 2.0)*((3.0*x)/2.0 - y + pow(x - 5.0, 2.0)/2.0 - 9.0/2.0)*(x - (2.0*y)/3.0 + pow((y/3.0 - 1.0),2.0)*((4.0*y)/3.0 - 4.0) + (y/3.0 - 1.0)*((5.0*y)/3.0 - 5.0) + 1.0)*((3.0*x)/2.0 + y + pow((x - 1.0),3.0)/2.0 - (2.0*x - 2.0)*(x - 1.0) + 3.0/2.0)*(pow((x - 3.0),2.0) - y - x/2.0 + pow(x - 3.0,3.0)/2.0 + 7.0/2.0))/10000.0 - ((x + 1.0)*(x + 2.0)*(x - 5.0)*(pow(y,2.0) - x + 4.0)*(y - x/2.0 + pow(x - 3.0,2.0)/2.0 + 7.0/2.0)*(2.0*y - x + pow(y - 1.0,2.0) + 3.0)*((3.0*x)/2.0 - y + pow((x - 5.0),2.0)/2.0 - 9.0/2.0)*(x - (2.0*y)/3.0 + pow((y/3.0 - 1.0),2.0)*((4.0*y)/3.0 - 4.0) + (y/3.0 - 1.0)*((5.0*y)/3.0 - 5.0) + 1.0)*((3.0*x)/2.0 + y + pow(x - 1.0,3.0)/2.0 - (2.0*x - 2.0)*(x - 1.0) + 3.0/2.0)*(pow(x - 3.0,2.0) - y - x/2.0 + pow(x - 3.0, 3.0)/2.0 + 7.0/2.0))/10000.0 + ((x + 1.0)*(x + 2.0)*(x - 5.0)*(y - x/2.0 + pow(x - 3.0,2.0)/2.0 + 7.0/2.0)*(2.0*y - x + pow(y - 1.0,2.0) + 3.0)*(x - (7.0*pow(y,2.0))/9.0 - (4.0*pow(y,3.0))/27.0 + 2.0)*((3.0*x)/2.0 - y + pow(x - 5.0,2.0)/2.0 - 9.0/2.0)*(x - (2.0*y)/3.0 + pow((y/3.0 - 1.0),2.0)*((4.0*y)/3.0 - 4.0) + (y/3.0 - 1.0)*((5.0*y)/3.0 - 5.0) + 1.0)*((3.0*x)/2.0 + y + pow(x - 1.0,3.0)/2.0 - (2.0*x - 2.0)*(x - 1.0) + 3.0/2.0)*(pow(x - 3.0,2.0) - y - x/2.0 + pow(x - 3.0,3.0)/2.0 + 7.0/2.0))/10000.0 - ((x + 1.0)*(x + 2.0)*(pow(y,2.0) - x + 4.0)*(y - x/2.0 + pow(x - 3.0,2.0)/2.0 + 7.0/2.0)*(2.0*y - x + pow((y - 1.0),2.0) + 3.0)*(x - (7.0*pow(y,2.0))/9.0 - (4.0*pow(y,3.0))/27.0 + 2.0)*((3.0*x)/2.0 - y + pow(x - 5.0,2.0)/2.0 - 9.0/2.0)*(x - (2.0*y)/3.0 + pow((y/3.0 - 1.0),2.0)*((4.0*y)/3.0 - 4.0) + (y/3.0 - 1.0)*((5.0*y)/3.0 - 5.0) + 1.0)*((3.0*x)/2.0 + y + pow(x - 1.0,3.0)/2.0 - (2.0*x - 2.0)*(x - 1.0) + 3.0/2.0)*(pow(x - 3.0,2.0) - y - x/2.0 + pow(x - 3.0,3.0)/2.0 + 7.0/2.0))/10000.0 - ((x + 1.0)*(x - 5.0)*(pow(y,2.0) - x + 4.0)*(y - x/2.0 + pow(x - 3.0,2.0)/2.0 + 7.0/2.0)*(2.0*y - x + pow(y - 1.0,2.0) + 3.0)*(x - (7.0*pow(y,2.0))/9.0 - (4.0*pow(y,3.0))/27.0 + 2.0)*((3.0*x)/2.0 - y + pow(x - 5.0,2.0)/2.0 - 9.0/2.0)*(x - (2.0*y)/3.0 + pow(y/3.0 - 1.0,2.0)*((4.0*y)/3.0 - 4.0) + (y/3.0 - 1.0)*((5.0*y)/3.0 - 5.0) + 1.0)*((3.0*x)/2.0 + y + pow(x - 1.0,3.0)/2.0 - (2.0*x - 2.0)*(x - 1.0) + 3.0/2.0)*(pow(x - 3.0,2.0) - y - x/2.0 + pow(x - 3.0,3.0)/2.0 + 7.0/2.0))/10000.0 - ((x + 2.0)*(x - 5.0)*(pow(y,2.0) - x + 4.0)*(y - x/2.0 + pow(x - 3.0,2.0)/2.0 + 7.0/2.0)*(2.0*y - x + pow(y - 1.0,2.0) + 3.0)*(x - (7.0*pow(y,2.0))/9.0 - (4*pow(y,3.0))/27.0 + 2.0)*((3.0*x)/2.0 - y + pow(x - 5.0,2.0)/2.0 - 9.0/2.0)*(x - (2.0*y)/3.0 + pow((y/3.0 - 1.0),2.0)*((4.0*y)/3.0 - 4.0) + (y/3.0 - 1.0)*((5.0*y)/3.0 - 5.0) + 1.0)*((3.0*x)/2.0 + y + pow(x - 1.0,3.0)/2.0 - (2.0*x - 2.0)*(x - 1.0) + 3.0/2.0)*(pow(x - 3.0,2.0) - y - x/2.0 + pow(x - 3.0,3.0)/2.0 + 7.0/2.0))/10000.0 - ((x + 1.0)*(x + 2.0)*(x - 5.0)*(pow(y,2.0) - x + 4.0)*(y - x/2.0 + pow(x - 3.0,2.0)/2.0 + 7.0/2.0)*(2.0*y - x + pow(y - 1.0,2.0) + 3.0)*(x - (7.0*pow(y,2.0))/9.0 - (4*pow(y,3.0))/27.0 + 2.0)*((3.0*x)/2.0 - y + pow(x - 5.0,2.0)/2.0 - 9.0/2.0)*((3.0*x)/2.0 + y + pow(x - 1.0,3.0)/2.0 - (2.0*x - 2.0)*(x - 1.0) + 3.0/2.0)*(pow(x - 3.0,2.0) - y - x/2.0 + pow(x - 3.0,3.0)/2.0 + 7.0/2.0))/10000.0 - ((x + 1.0)*(x + 2.0)*(x - 5.0)*(x - 7.0/2.0)*(pow(y,2.0) - x + 4.0)*(2.0*y - x + pow(y - 1.0,2.0) + 3.0)*(x - (7.0*pow(y,2.0))/9.0 - (4.0*pow(y,3.0))/27.0 + 2.0)*((3.0*x)/2.0 - y + pow(x - 5.0,2.0)/2.0 - 9.0/2.0)*(x - (2.0*y)/3.0 + pow((y/3.0 - 1.0),2.0)*((4.0*y)/3.0 - 4.0) + (y/3.0 - 1.0)*((5.0*y)/3.0 - 5.0) + 1.0)*((3.0*x)/2.0 + y + pow(x - 1.0,3.0)/2.0 - (2.0*x - 2.0)*(x - 1.0) + 3.0/2.0)*(pow(x - 3.0,2.0) - y - x/2.0 + pow(x - 3.0,3.0)/2.0 + 7.0/2.0))/10000.0 - ((x + 1.0)*(x + 2.0)*(x - 5.0)*(pow(y,2.0) - x + 4.0)*(2.0*x + (3.0*pow(x - 3.0,2.0))/2.0 - 13.0/2.0)*(y - x/2.0 + pow(x - 3.0,2.0)/2.0 + 7.0/2.0)*(2.0*y - x + pow(y - 1.0,2) + 3.0)*(x - (7.0*pow(y,2.0))/9.0 - (4.0*pow(y,3.0))/27.0 + 2.0)*((3.0*x)/2.0 - y + pow(x - 5.0,2.0)/2.0 - 9.0/2.0)*(x - (2.0*y)/3.0 + pow(y/3.0 - 1.0, 2.0)*((4.0*y)/3.0 - 4.0) + (y/3.0 - 1.0)*((5.0*y)/3.0 - 5.0) + 1.0)*((3.0*x)/2.0 + y + pow(x - 1.0,3.0)/2.0 - (2.0*x - 2.0)*(x - 1.0) + 3.0/2.0))/10000.0 - ((x + 1.0)*(x + 2.0)*(x - 5.0)*(pow(y,2.0) - x + 4.0)*((3.0*pow(x - 1.0,2.0))/2.0 - 4.0*x + 11.0/2.0)*(y - x/2.0 + pow(x - 3.0,2.0)/2.0 + 7.0/2.0)*(2.0*y - x + pow(y - 1.0,2.0) + 3.0)*(x - (7.0*pow(y,2.0))/9.0 - (4.0*pow(y,3.0))/27.0 + 2.0)*((3.0*x)/2.0 - y + pow(x - 5.0,2.0)/2.0 - 9.0/2.0)*(x - (2.0*y)/3.0 + pow(y/3.0 - 1.0,2.0)*((4.0*y)/3.0 - 4.0) + (y/3.0 - 1.0)*((5.0*y)/3.0 - 5.0) + 1.0)*(pow(x - 3.0,2.0) - y - x/2.0 + pow(x - 3.0,3.0)/2.0 + 7.0/2.0))/10000.0 - ((x + 1.0)*(x + 2.0)*(x - 5.0)*(x - 7.0/2.0)*(pow(y,2.0) - x + 4.0)*(y - x/2.0 + pow(x - 3.0,2.0)/2.0 + 7.0/2.0)*(2.0*y - x + pow(y - 1.0,2.0) + 3.0)*(x - (7.0*pow(y,2.0))/9.0 - (4.0*pow(y,3.0))/27.0 + 2.0)*(x - (2.0*y)/3.0 + pow(y/3.0 - 1.0,2.0)*((4.0*y)/3.0 - 4.0) + (y/3.0 - 1.0)*((5.0*y)/3.0 - 5.0) + 1.0)*((3.0*x)/2.0 + y + pow(x - 1.0,3.0)/2.0 - (2.0*x - 2.0)*(x - 1.0) + 3.0/2.0)*(pow(x - 3.0,2.0) - y - x/2.0 + pow(x - 3.0,3.0)/2.0 + 7.0/2.0))/10000.0;
        break;

    case 48:
        d_temp=-2*x*(1-y*y)*sin(x*y)+y*(1-x*x)*(1-y*y)*cos(x*y);
        break;

    case 49:
        d_temp=- 2*x*(sin(x*y) + 1)*(pow(x,2) - 1)*(pow(y,2) - 1) - 2*x*(sin(x*y) + 1)*(pow(y,2) - 1)*(pow(x,2) - pow(y,2)) - y*cos(x*y)*(pow(x,2) - 1)*(pow(y,2) - 1)*(pow(x,2) - pow(y,2));
        break;

    case 50:
        d_temp=(2*x*(y*y - 1))/(pow((x*x - y*y),2)/1000000.0 + 1.0/10.0) - (x*(x*x - 1)*(y*y - 1)*(x*x - y*y))/(250000.0*pow((pow((x*x - y*y),2)/1000000.0 + 1/10.0),2));
        break;

    case 53:
        ////Ex3_1_1
        //d_temp=(x*y*(pow(y,2.0) - 4.0)*(3.0*pow(x,2.0) + 4.0*pow(y,2.0) + 4.0))/(2.0*pow((pow(x,2.0) + pow(y,2.0)),(5.0/4.0)));
        ////Ex3_1_2
        //d_temp=(x*y*(pow(y,2.0) - 4.0)*(5.0*pow(x,2.0) + 4.0*pow(y,2.0) - 4.0))/(2.0*pow((pow(x,2.0) + pow(y,2.0)),(3.0/4.0)));

        ////Ex3_1_3
        d_temp=2.0*x*sin(y)*(y*y - 4.0);

        break;

    case 54:
        //d_temp=(y*(y*y - 1.0)*(11.0*pow(x,4.0) + 9.0*pow(x,2.0)*pow(y,2.0) - 5.0*pow(x,2.0) - 3.0*pow(y,2.0)))/(3.0*pow((pow(x,2.0) + pow(y,2.0)),(2.0/3.0)));
        //d_temp=(y*(y*y - 1.0)*(5.0*pow(x,4.0) + 9.0*pow(x,2.0)*pow(y,2.0) + pow(x,2.0) - 3.0*pow(y,2.0)))/(3.0*pow((pow(x,2.0) + pow(y,2.0)),(5.0/3.0)));
        //

        ///Ex3_2_1
        //d_temp=(y*(y*y-1.0)*(5.0*pow(x,4.0)+9.0*pow(x,2.0)*pow(y,2.0)+pow(x,2.0)-3.0*pow(y,2.0)))/(3.0*pow(x*x + y*y,5.0/3.0));

        ///Ex3_2_2
        //d_temp=(y*(y*y - 1.0)*(5.0*pow(x,4.0) + 6.0*pow(x,2.0)*pow(y,2.0) - pow(x,2.0) - 2.0*y*y))/(2*pow((x*x + y*y),(5.0/4.0)));

        ////Ex3_2_3
        d_temp=-y*y*(y*y - 1.0)*(5.0*pow(x,4.0) - 15.0*x*x + 4.0);
        break;

    case 55:
        d_temp=(exp(x/4.0 - y/4.0)*(x*x - 4.0)*(y*y - 4.0))/4.0 + 2.0*x*exp(x/4.0 - y/4.0)*(y*y - 4.0);
        break;

    case 56:
        d_temp=(2.0-2.0*x)*y*(2.0-y);
        break;

    default:
        cout<<"FxFunctionValue:: ExampleIndex is out of the range"<<endl;
        break;
    }

    return d_temp;
}
//=============================
/**
 * @brief FyFunctionValue
 * @param x
 * @param y
 * @param ExampleIndex
 * @return
 */
static double FyFunctionValue(double x,double y, unsigned int ExampleIndex)
{
    double d_temp=0.0;
    switch(ExampleIndex)
    {
    case 4:
        d_temp=-2.0*y;
        break;

    case 6:
        d_temp=x*(1.0-x)*(1.0-y)*sin(x)*cos(y);
        d_temp=d_temp-x*(1.0-x)*y*sin(x)*cos(y);
        d_temp=d_temp-x*(1.0-x)*y*(1.0-y)*sin(x)*sin(y);
        break;

    case 15:
        d_temp=-2.0*y*(1.0-x*x);
        break;

    case 19:
        break;

    case 24:
//        double A=(x-0.5)*(x-0.5)+y*y-1.0/50.0;
//        double B=((x-2.0)*(x-2.0)+y*y-0.25);
//        double C=(y*y-x*(x-1.0)*(x-1.0)-2.0);
//        double D=x-2.0;
//        double E=x+0.6956;
        d_temp=(2.0*y)*((x-2.0)*(x-2.0)+y*y-0.25)*(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x-2.0)*(x+0.6956);
        d_temp=d_temp+((x-0.5)*(x-0.5)+y*y-1.0/50.0)*(2.0*y)*(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x-2.0)*(x+0.6956);
        d_temp=d_temp+((x-0.5)*(x-0.5)+y*y-1.0/50.0)*((x-2.0)*(x-2.0)+y*y-0.25)*(2.0*y)*(x-2.0)*(x+0.6956);
        break;

    case 25:
        d_temp=-2.0*y*(1.0-x*x);
        break;

    case 27://is the same with case 24
        d_temp=(2.0*y)*((x-2.0)*(x-2.0)+y*y-0.25)*(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x-2.0)*(x+0.6956);
        d_temp=d_temp+((x-0.5)*(x-0.5)+y*y-1.0/50.0)*(2.0*y)*(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x-2.0)*(x+0.6956);
        d_temp=d_temp+((x-0.5)*(x-0.5)+y*y-1.0/50.0)*((x-2.0)*(x-2.0)+y*y-0.25)*(2.0*y)*(x-2.0)*(x+0.6956);
        break;
    case 28:
        //d_temp=-(y-x*x)*(1.0-x*x)+(x*x+1.0-y)*(1.0-x*x);
        d_temp=1.0-2.0*y+x*x-2.0*x*x*x*x+2.0*x*x*y;
        break;

    case 30:
        d_temp=(1.0-2.0*y+x*x-2.0*x*x*x*x+2.0*x*x*y)*sin(x);
        break;

    case 37:
        d_temp=((x + 1.0)*(x + 2.0)*(x - 5.0)*(pow(y,2.0) - x + 4.0)*(y - x/2.0 + pow(x - 3.0,2.0)/2.0 + 7.0/2.0)*(2.0*y - x + pow(y - 1.0,2.0) + 3.0)*(x - (7.0*pow(y,2.0))/9.0 - (4.0*pow(y,3.0))/27.0 + 2.0)*(x - (2.0*y)/3.0 + pow(y/3.0 - 1.0,2.0)*((4.0*y)/3.0 - 4.0) + (y/3.0 - 1.0)*((5.0*y)/3.0 - 5.0) + 1.0)*((3.0*x)/2.0 + y + pow(x - 1.0,3.0)/2.0 - (2.0*x - 2.0)*(x - 1.0) + 3.0/2.0)*(pow(x - 3.0,2.0) - y - x/2.0 + pow(x - 3.0,3.0)/2.0 + 7.0/2.0))/10000.0 - ((x + 1.0)*(x + 2.0)*(x - 5.0)*(pow(y,2.0) - x + 4.0)*(2.0*y - x + pow(y - 1.0,2.0) + 3.0)*(x - (7.0*pow(y,2.0))/9.0 - (4.0*pow(y,3.0))/27.0 + 2.0)*((3.0*x)/2.0 - y + pow(x - 5.0,2.0)/2.0 - 9.0/2.0)*(x - (2.0*y)/3.0 + pow(y/3.0 - 1.0,2.0)*((4.0*y)/3.0 - 4.0) + (y/3.0 - 1.0)*((5.0*y)/3.0 - 5.0) + 1.0)*((3.0*x)/2.0 + y + pow(x - 1.0,3.0)/2.0 - (2.0*x - 2.0)*(x - 1.0) + 3.0/2.0)*(pow(x - 3.0,2.0) - y - x/2.0 + pow(x - 3.0,3.0)/2.0 + 7.0/2.0))/10000.0 + ((x + 1.0)*(x + 2.0)*(x - 5.0)*(pow(y,2.0) - x + 4.0)*(y - x/2.0 + pow(x - 3.0,2.0)/2.0 + 7.0/2.0)*(2.0*y - x + pow(y - 1.0,2.0) + 3.0)*(x - (7.0*pow(y,2.0))/9.0 - (4.0*pow(y,3.0))/27.0 + 2.0)*((3.0*x)/2.0 - y + pow(x - 5.0,2.0)/2.0 - 9.0/2.0)*(x - (2.0*y)/3.0 + pow(y/3.0 - 1.0,2.0)*((4.0*y)/3.0 - 4.0) + (y/3.0 - 1.0)*((5.0*y)/3.0 - 5.0) + 1.0)*((3.0*x)/2.0 + y + pow(x - 1.0,3.0)/2.0 - (2.0*x - 2.0)*(x - 1.0) + 3.0/2.0))/10000.0 - ((x + 1.0)*(x + 2.0)*(x - 5.0)*(pow(y,2.0) - x + 4.0)*(y - x/2.0 + pow((x - 3.0),2.0)/2.0 + 7.0/2.0)*(2.0*y - x + pow(y - 1.0,2.0) + 3.0)*(x - (7.0*pow(y,2.0))/9.0 - (4.0*pow(y,3.0))/27.0 + 2.0)*((3.0*x)/2.0 - y + pow(x - 5.0,2.0)/2.0 - 9.0/2.0)*(x - (2.0*y)/3.0 + pow(y/3.0 - 1.0,2.0)*((4.0*y)/3.0 - 4.0) + (y/3.0 - 1.0)*((5.0*y)/3.0 - 5.0) + 1.0)*(pow(x - 3.0,2.0) - y - x/2.0 + pow(x - 3.0,3.0)/2.0 + 7.0/2.0))/10000.0 - (y*(x + 1.0)*(x + 2.0)*(x - 5.0)*(y - x/2.0 + pow(x - 3.0,2.0)/2.0 + 7.0/2.0)*(2.0*y - x + pow(y - 1.0,2.0) + 3.0)*(x - (7.0*pow(y,2.0))/9.0 - (4.0*pow(y,3.0))/27.0 + 2.0)*((3.0*x)/2.0 - y + pow(x - 5.0,2.0)/2.0 - 9.0/2.0)*(x - (2.0*y)/3.0 + pow(y/3.0 - 1.0,2.0)*((4.0*y)/3.0 - 4.0) + (y/3.0 - 1.0)*((5.0*y)/3.0 - 5.0) + 1.0)*((3.0*x)/2.0 + y + pow(x - 1.0,3.0)/2.0 - (2.0*x - 2.0)*(x - 1.0) + 3.0/2.0)*(pow(x - 3.0,2.0) - y - x/2.0 + pow(x - 3.0,3.0)/2.0 + 7.0/2.0))/5000.0 + (((4.0*pow(y,2.0))/9.0 + (14.0*y)/9.0)*(x + 1.0)*(x + 2.0)*(x - 5.0)*(pow(y,2.0) - x + 4.0)*(y - x/2.0 + pow(x - 3.0,2.0)/2.0 + 7.0/2.0)*(2.0*y - x + pow(y - 1.0,2.0) + 3.0)*((3.0*x)/2.0 - y + pow(x - 5.0,2.0)/2.0 - 9.0/2.0)*(x - (2.0*y)/3.0 + pow(y/3.0 - 1.0,2.0)*((4.0*y)/3.0 - 4.0) + (y/3.0 - 1.0)*((5.0*y)/3.0 - 5.0) + 1.0)*((3.0*x)/2.0 + y + pow((x - 1.0),3.0)/2.0 - (2.0*x - 2.0)*(x - 1.0) + 3.0/2.0)*(pow(x - 3.0,2.0) - y - x/2.0 + pow(x - 3.0,3.0)/2.0 + 7.0/2.0))/10000.0 - (y*(x + 1.0)*(x + 2.0)*(x - 5.0)*(pow(y,2.0) - x + 4.0)*(y - x/2.0 + pow(x - 3.0,2.0)/2.0 + 7.0/2.0)*(x - (7.0*pow(y,2.0))/9.0 - (4.0*pow(y,3.0))/27.0 + 2.0)*((3.0*x)/2.0 - y + pow(x - 5.0,2.0)/2.0 - 9.0/2.0)*(x - (2.0*y)/3.0 + pow((y/3.0 - 1.0),2.0)*((4.0*y)/3.0 - 4.0) + (y/3.0 - 1.0)*((5.0*y)/3.0 - 5.0) + 1.0)*((3.0*x)/2.0 + y + pow(x - 1.0,3.0)/2.0 - (2.0*x - 2.0)*(x - 1.0) + 3.0/2.0)*(pow(x - 3.0,2.0) - y - x/2.0 + pow(x - 3.0,3.0)/2.0 + 7.0/2.0))/5000.0 - ((x + 1.0)*(x + 2.0)*(x - 5.0)*(pow(y,2.0) - x + 4.0)*(y - x/2.0 + pow(x - 3.0,2.0)/2.0 + 7.0/2.0)*(2.0*y - x + pow(y - 1.0,2.0) + 3.0)*(x - (7.0*pow(y,2.0))/9.0 - (4.0*pow(y,3.0))/27.0 + 2.0)*((3.0*x)/2.0 - y + pow(x - 5.0,2.0)/2.0 - 9.0/2.0)*((10.0*y)/9.0 + (4.0*pow((y/3.0 - 1.0),2.0))/3.0 + ((4.0*y)/3.0 - 4.0)*((2.0*y)/9.0 - 2.0/3.0) - 4.0)*((3.0*x)/2.0 + y + pow(x - 1.0,3.0)/2.0 - (2.0*x - 2.0)*(x - 1.0) + 3.0/2.0)*(pow(x - 3.0,2.0) - y - x/2.0 + pow(x - 3.0,3.0)/2.0 + 7.0/2.0))/10000.0;
        break;

    case 48:
        d_temp=-2*y*(1-x*x)*sin(x*y)+x*(1-x*x)*(1-y*y)*cos(x*y);
        break;

    case 49:
        d_temp=2*y*(sin(x*y) + 1)*(pow(x,2) - 1)*(pow(y,2) - 1) - 2*y*(sin(x*y) + 1)*(pow(x,2) - 1)*(pow(x,2) - pow(y,2)) - x*cos(x*y)*(pow(x,2) - 1)*(pow(y,2) - 1)*(pow(x,2) - pow(y,2));
        break;

    case 50:
        d_temp=(2.0*y*(x*x - 1.0))/(pow((x*x - y*y),2)/1000000.0 + 1.0/10.0) + (y*(x*x - 1.0)*(y*y - 1.0)*(x*x - y*y))/(250000.0*pow(pow((x*x - y*y),2)/1000000.0 + 1.0/10.0,2));
        break;

    case 53:
        ////Ex3_1_1
        //d_temp=((pow(x,2.0) - 4.0)*(6.0*pow(x,2.0)*pow(y,2.0) - 8.0*pow(x,2.0) + 5.0*pow(y,4.0) - 4.0*pow(y,2.0)))/(2.0*pow((pow(x,2.0) + pow(y,2.0)),(5.0/4.0)));

        ////Ex3_1_2
        //d_temp=((x*x - 4.0)*(6.0*x*x*y*y - 8.0*x*x + 7.0*pow(y,4.0) - 12.0*y*y))/(2.0*pow((x*x + y*y),(3.0/4.0)));

        ////Ex3_1_3
        d_temp=cos(y)*(x*x - 4.0)*(y*y - 4.0) + 2.0*y*sin(y)*(x*x - 4.0);
        break;

    case 54:
        //d_temp=(x*(x*x - 1.0)*(9.0*x*x*y*y - 3.0*x*x + 11.0*pow(y,4.0) - 5.0*pow(y,2.0)))/(3.0*pow((x*x + y*y),(2.0/3.0)));
        //d_temp=(x*(x*x - 1.0)*(9.0*x*x*y*y - 3.0*x*x + 5.0*pow(y,4.0) + y*y))/(3.0*pow((x*x + y*y),(5.0/3.0)));
        //

        ///Ex3_2_1
        //d_temp=(x*(x*x-1)*(9.0*x*x*y*y-3.0*x*x+5.0*pow(y,4.0)+y*y))/(3.0*pow(x*x+y*y,5.0/3.0));

        ///Ex3_2_2
        //d_temp=(x*(x*x - 1.0)*(6.0*x*x*y*y - 2.0*x*x + 5.0*pow(y,4.0) - y*y))/(2.0*pow((x*x + y*y),(5.0/4.0)));

        ////Ex3_2_3
        d_temp=-2.0*x*y*(2.0*y*y - 1.0)*(pow(x,4.0) - 5.0*x*x + 4.0);
        break;

    case 55:
        d_temp=2.0*y*exp(x/4.0 - y/4.0)*(x*x - 4.0) - (exp(x/4.0 - y/4.0)*(x*x - 4.0)*(y*y - 4.0))/4.0;
        break;

    case 56:
        d_temp=x*(2.0-x)*(2.0-2.0*y);
        break;


    default:
        cout<<"FyFunctionValue:: ExampleIndex is out of the range"<<endl;
        break;
    }

    return d_temp;
}
//===================================
static double RightSideFunctionValue(double x, double y, unsigned int ExampleIndex)
{
    double d_temp;
    double Fxx, Fyy;
    switch(ExampleIndex)
    {
    case 1://Example 1_1
        d_temp=16.0*(x*x+y*y)-17.0/4.0;
        break;
    case 2://Example 2_0
        d_temp=-21.205*x+112.58*y*y+730.37*x*x+659.76*x*x*y*y;
        d_temp=d_temp-521.16*x*y*y-1349.6*x*x*x-406*x*x*x*x*x+1071.4*x*x*x*x;
        d_temp=d_temp+88.0*y*y*y*y+60.0*x*x*x*x*x*x-352.0*y*y*x*x*x-66.0*y*y*y*y*x+72.0*y*y*x*x*x*x+12.0*y*y*y*y*x*x;
        d_temp=d_temp-83.605;
        break;
    case 3://Example 2_1
        d_temp=-208.7852*x;
        d_temp=d_temp+97.9056*y*y;
        d_temp=d_temp+446.0404*x*x;
        d_temp=d_temp-415.6309*x*x*y*y;
        d_temp=d_temp-119.8189*x*y*y;
        d_temp=d_temp+742.7488*y*y*x*x*x;
        d_temp=d_temp+98.0904*y*y*y*y*x;
        d_temp=d_temp-441.9168*y*y*x*x*x*x;
        d_temp=d_temp-93.6528*y*y*y*y*x*x;
        d_temp=d_temp+96.0*y*y*x*x*x*x*x;
        d_temp=d_temp+235.1282*x*x*x;
        d_temp=d_temp+1184.7*x*x*x*x*x;
        d_temp=d_temp-1220.3*x*x*x*x;
        d_temp=d_temp+33.2528*y*y*y*y;
        d_temp=d_temp-490.2640*x*x*x*x*x*x;
        d_temp=d_temp+76.0*x*x*x*x*x*x*x;
        d_temp=d_temp+20.0*y*y*y*y*x*x*x;
        d_temp=d_temp-2.0*y*y*y*y*y*y;
        d_temp=d_temp-20.7956;
        break;
    case 4://Example 4
        d_temp=4.0;
        break;
    case 5://Test Example 1-->BiCubic Example
        d_temp=1.5*x+1.5*y-6.0*y*x-4.5*y*y;
        d_temp=d_temp+9.0*y*y*x-6.0*y*y*y*x-4.5*x*x;
        d_temp=d_temp+9.0*x*x*y-6.0*x*x*x*y+3.0*y*y*y+3.0*x*x*x;
        break;
    case 6://Test Example 2-->GeneralSolution Example
        d_temp=2*y*sin(x)*cos(y)-2*y*y*sin(x)*cos(y)-2*y*cos(x)*cos(y);
        d_temp=d_temp+2*y*y*cos(x)*cos(y)+4*y*cos(x)*cos(y)*x-4*y*y*cos(x)*cos(y)*x;
        d_temp=d_temp+2*x*y*sin(x)*cos(y)-2*x*y*y*sin(x)*cos(y)-2*x*x*y*sin(x)*cos(y);
        d_temp=d_temp+2*x*x*y*y*sin(x)*cos(y)+2*x*sin(x)*cos(y)-2*x*x*sin(x)*cos(y);
        d_temp=d_temp+2*x*sin(x)*sin(y)-4*x*sin(x)*sin(y)*y-2*x*x*sin(x)*sin(y)+4*x*x*sin(x)*sin(y)*y;
        break;
    case 7://-div(K divF)=f: with the same mesh with Example 4
        d_temp=4.0/3.0;
        break;
    case 8:
        d_temp=-(2.0/3.0)*(x*x*x*x+y*y*y*y+x*x+y*y-6*x*x*y*y-2);
        break;
    case 9:
        d_temp=2.0*(2.0-x*x-y*y);
        break;
    case 10:
        d_temp=12.0*x*y;
        break;
    case 11:
        d_temp=-16.0*(x*x+y*y)+8.0;
        break;
    case 12:
        d_temp=4.0;
    case 13:
        d_temp=4.0;
        break;
    case 14:
        d_temp=4.0;
        break;
    case 15:
        d_temp=4.0-2.0*(x*x+y*y);
        break;
    case 16:
        d_temp=4.0;
        break;
    case 17:
        d_temp=4.0;
        break;
    case 18:
        d_temp=4.0;
        break;
    case 19:
        d_temp=4.0;
        break;
    case 20:
        d_temp=4.0;
        break;
    case 21:
        d_temp=4.0;
        break;
    case 22:
        d_temp=4.0;
        break;
    case 23:
        d_temp=4.0;
        break;
    case 24://Example 2_2
        d_temp=-208.7852*x;
        d_temp=d_temp+97.9056*y*y;
        d_temp=d_temp+446.0404*x*x;
        d_temp=d_temp-415.6309*x*x*y*y;
        d_temp=d_temp-119.8189*x*y*y;
        d_temp=d_temp+742.7488*y*y*x*x*x;
        d_temp=d_temp+98.0904*y*y*y*y*x;
        d_temp=d_temp-441.9168*y*y*x*x*x*x;
        d_temp=d_temp-93.6528*y*y*y*y*x*x;
        d_temp=d_temp+96.0*y*y*x*x*x*x*x;
        d_temp=d_temp+235.1282*x*x*x;
        d_temp=d_temp+1184.7*x*x*x*x*x;
        d_temp=d_temp-1220.3*x*x*x*x;
        d_temp=d_temp+33.2528*y*y*y*y;
        d_temp=d_temp-490.2640*x*x*x*x*x*x;
        d_temp=d_temp+76.0*x*x*x*x*x*x*x;
        d_temp=d_temp+20.0*y*y*y*y*x*x*x;
        d_temp=d_temp-2.0*y*y*y*y*y*y;
        d_temp=d_temp-20.7956;
        break;

    case 25:
        d_temp=2.0*((1.0-y*y)*(2.0-x)/((x+2.0)*(x+2.0)*(x+2.0))+(1.0-x*x)/((x+2.0)*(x+2.0)));
        break;

    case 27:// is the same with case 24
        d_temp=-208.7852*x;
        d_temp=d_temp+97.9056*y*y;
        d_temp=d_temp+446.0404*x*x;
        d_temp=d_temp-415.6309*x*x*y*y;
        d_temp=d_temp-119.8189*x*y*y;
        d_temp=d_temp+742.7488*y*y*x*x*x;
        d_temp=d_temp+98.0904*y*y*y*y*x;
        d_temp=d_temp-441.9168*y*y*x*x*x*x;
        d_temp=d_temp-93.6528*y*y*y*y*x*x;
        d_temp=d_temp+96.0*y*y*x*x*x*x*x;
        d_temp=d_temp+235.1282*x*x*x;
        d_temp=d_temp+1184.7*x*x*x*x*x;
        d_temp=d_temp-1220.3*x*x*x*x;
        d_temp=d_temp+33.2528*y*y*y*y;
        d_temp=d_temp-490.2640*x*x*x*x*x*x;
        d_temp=d_temp+76.0*x*x*x*x*x*x*x;
        d_temp=d_temp+20.0*y*y*y*y*x*x*x;
        d_temp=d_temp-2.0*y*y*y*y*y*y;
        d_temp=d_temp-20.7956;
        break;

    case 28:
//        d_temp=2.0*(1.0-x*x);
//        d_temp=d_temp-(2.0*(y-x*x)*(1.0-x*x)-4.0*x*x*(1.0-x*x)-4.0*x*x*(y-x*x));
//        d_temp=d_temp+2.0*((x*x+1.0-y)*(1.0-x*x)+2.0*x*x*(1.0-x*x)-2.0*x*x*(x*x+1.0-y));
//        d_temp=d_temp+2.0*((x*x+1.0-y)*(y-x*x)+2.0*x*x*(y-x*x)-2.0*x*x*(x*x+1.0-y));
        d_temp=4.0-2.0*x*x;
        d_temp=d_temp-2.0*y;
        d_temp=d_temp+24.0*x*x*y;
        d_temp=d_temp-30.0*x*x*x*x;
        d_temp=d_temp-2.0*y*y;
        break;

    case 30:
        Fxx=(2.0*y-24.0*x*x*y+30.0*x*x*x*x+2.0*y*y-2.0)*sin(x);
        Fxx=Fxx+2.0*(2.0*x*y-8.0*x*x*x*y+6.0*x*x*x*x*x+2.0*x*y*y-2.0*x)*cos(x);
        Fxx=Fxx-((x*x+1.0-y)*(y-x*x)*(1.0-x*x))*sin(x);

        Fyy=(-2.0+2.0*x*x)*sin(x);

        d_temp=-(Fxx+Fyy);
        break;

    case 36://Example 9
        //d_temp=sin(x*y);
        //d_temp=4.0;
        //d_temp=x;
        d_temp=x*sin(2.0*y);

        break;

    case 37:
        d_temp=-1.0/243000.0*pow(x,15.0)*pow(y,4.0)-1896676.0/5625.0-502796.0/1875.0*x+2465513.0/16875.0*pow(y,2.0);
        d_temp=d_temp-182287.0/100.0*pow(x,3.0)-38164697.0/18000.0*pow(x,4.0)+52365203.0/20000.0*pow(x,5.0)+6419987.0/2250.0*pow(x,2.0);
        d_temp=d_temp+21849469.0/607500.0*pow(y,4.0)-38037241.0/90000.0*pow(x,6.0)-10377349.0/16875.0*pow(y,2.0)*x;
        d_temp=d_temp+24889873.0/243000.0*pow(y,4.0)*x-118369459.0/151875.0*pow(y,4.0)*pow(x,2.0)+145593773.0/243000.0*pow(y,4.0)*pow(x,3.0);
        d_temp=d_temp-8053837.0/33750.0*pow(x,2.0)*pow(y,2.0)+17731537.0/6750.0*pow(x,3.0)*pow(y,2.0)-66639529.0/30000.0*pow(x,4.0)*pow(y,2.0);
        d_temp=d_temp-29498921.0/60000.0*pow(x,5.0)*pow(y,2.0)+42318383.0/27000.0*pow(x,6.0)*pow(y,2.0)+128660213.0/1822500.0*pow(y,6.0)*x;
        d_temp=d_temp-4596197.0/67500.0*pow(y,6.0)*pow(x,2.0)+6717619.0/10800.0*pow(y,4.0)*pow(x,4.0)-34108903.0/202500.0*pow(y,6.0)*pow(x,3.0);
        d_temp=d_temp-1592003.0/182250.0*pow(y,6.0)-158261.0/810000.0*pow(y,8.0)-1640197.0/2400.0*pow(x,7.0)+221507273.0/810000.0*pow(x,4.0)*pow(y,6.0);
        d_temp=d_temp-578970971.0/540000.0*pow(x,5.0)*pow(y,4.0)-1777986889.0/14580000.0*pow(x,5.0)*pow(y,6.0)+1199200327.0/2430000.0*pow(y,4.0)*pow(x,6.0);
        d_temp=d_temp+155164613.0/360000.0*pow(x,8.0)-5041711.0/80000.0*pow(x,9.0)-215372467.0/270000.0*pow(x,7.0)*pow(y,2.0)+10540201.0/270000.0*pow(x,8.0)*pow(y,2.0);
        d_temp=d_temp+72105377.0/540000.0*pow(x,9.0)*pow(y,2.0)+2284481.0/1620000.0*pow(y,8.0)*x+14842561.0/810000.0*pow(y,8.0)*pow(x,2.0)+209419553.0/4860000.0*pow(y,4.0)*pow(x,7.0);
        d_temp=d_temp-53571.0/2000.0*pow(y,6.0)*pow(x,6.0)-55415243.0/2916000.0*pow(y,8.0)*pow(x,3.0)+767352841.0/14580000.0*pow(x,7.0)*pow(y,6.0);
        d_temp=d_temp-1279669.0/36000.0*pow(x,10.0)-35823991.0/243000.0*pow(x,8.0)*pow(y,4.0)-18946561.0/729000.0*pow(x,8.0)*pow(y,6.0);
        d_temp=d_temp-1071331.0/15000.0*pow(x,10.0)*pow(y,2.0)+1157771.0/60000.0*pow(x,11.0)*pow(y,2.0)+317359.0/388800.0*pow(y,8.0)*pow(x,4.0);
        d_temp=d_temp+167573161.0/19440000.0*pow(y,8.0)*pow(x,5.0)+861869.0/40000.0*pow(x,11.0)+146345281.0/1944000.0*pow(y,4.0)*pow(x,9.0);
        d_temp=d_temp-2111141.0/364500.0*pow(y,8.0)*pow(x,6.0)+351026.0/50625.0*pow(x,9.0)*pow(y,6.0)-5623993.0/270000.0*pow(x,10.0)*pow(y,4.0);
        d_temp=d_temp-2588881.0/2430000.0*pow(x,10.0)*pow(y,6.0)-323921.0/1620000.0*pow(y,10.0)*x-1589.0/2025.0*pow(y,10.0)*pow(x,2.0);
        d_temp=d_temp-243313.0/2430000.0*pow(y,10.0)-2011759.0/360000.0*pow(x,12.0)+6899.0/1822500.0*pow(y,12.0)+199859.0/240000.0*pow(x,13.0);
        d_temp=d_temp+7552333.0/7290000.0*pow(y,10.0)*pow(x,3.0)+1198027.0/810000.0*pow(x,7.0)*pow(y,8.0)+11819.0/216000.0*pow(x,8.0)*pow(y,8.0);
        d_temp=d_temp-423683.0/135000.0*pow(x,12.0)*pow(y,2.0)+949327.0/270000.0*pow(x,11.0)*pow(y,4.0)-34877.0/97200.0*pow(y,10.0)*pow(x,4.0);
        d_temp=d_temp-930407.0/4860000.0*pow(y,10.0)*pow(x,5.0)+39017.0/151875.0*pow(y,10.0)*pow(x,6.0)-1636573.0/11664000.0*pow(x,9.0)*pow(y,8.0);
        d_temp=d_temp+1200709.0/14580000.0*pow(x,11.0)*pow(y,6.0)+197147.0/4860000.0*pow(x,10.0)*pow(y,8.0)+4.0/455625.0*pow(y,14.0);
        d_temp=d_temp-3233.0/45000.0*pow(x,14.0)+1523.0/729000.0*pow(y,12.0)*x+3724.0/455625.0*pow(y,12.0)*pow(x,2.0)+20771.0/67500.0*pow(x,13.0)*pow(y,2.0);
        d_temp=d_temp-887.0/455625.0*pow(y,12.0)*pow(x,3.0)-10297.0/81000.0*pow(x,7.0)*pow(y,10.0)+21661.0/607500.0*pow(x,8.0)*pow(y,10.0);
        d_temp=d_temp-867977.0/2430000.0*pow(x,12.0)*pow(y,4.0)-11.0/3375.0*pow(y,12.0)*pow(x,4.0)+217.0/101250.0*pow(y,12.0)*pow(x,5.0);
        d_temp=d_temp-224.0/455625.0*pow(y,12.0)*pow(x,6.0)-21571.0/3645000.0*pow(x,9.0)*pow(y,10.0)-4417.0/7290000.0*pow(x,12.0)*pow(y,6.0);
        d_temp=d_temp-56051.0/9720000.0*pow(x,11.0)*pow(y,8.0)+11.0/20250.0*pow(x,10.0)*pow(y,10.0)+63.0/20000.0*pow(x,15.0)-2.0/151875.0*pow(y,14.0)*x;
        d_temp=d_temp-278.0/16875.0*pow(x,14.0)*pow(y,2.0)+37679.0/1944000.0*pow(x,13.0)*pow(y,4.0)+2.0/50625.0*pow(y,12.0)*pow(x,7.0);
        d_temp=d_temp-4949.0/14580000.0*pow(x,13.0)*pow(y,6.0)+383.0/911250.0*pow(x,12.0)*pow(y,8.0)-13.0/607500.0*pow(x,11.0)*pow(y,10.0);
        d_temp=d_temp+191.0/540000.0*pow(x,15.0)*pow(y,2.0)-11.0/30375.0*pow(x,14.0)*pow(y,4.0)+7.0/455625.0*pow(x,14.0)*pow(y,6.0)-1.0/81000.0*pow(x,13.0)*pow(y,8.0)-1.0/22500.0*pow(x,16.0);
        /*d_temp=-692.0/75.0*y-206.0/75.0*pow(y,3.0)-3139.0/30.0*x*y-17437.0/675.0*pow(x,2.0)*pow(y,3.0);
        d_temp=d_temp-30991.0/675.0*x*pow(y,3.0)-260743.0/4050.0*pow(x,2.0)*pow(y,5.0)+85009.0/270.0*pow(x,3.0)*pow(y,3.0);
        d_temp=d_temp+32938.0/2025.0*x*pow(y,5.0)+801.0/5.0*pow(x,2.0)*y+2822.0/675.0*pow(y,5.0)-122161.0/4050.0*pow(x,2.0)*pow(y,7.0);
        d_temp=d_temp+110311.0/16200.0*x*pow(y,7.0)+3377.0/150.0*pow(x,3.0)*y-20567.0/100.0*pow(x,4.0)*y+23719.0/1350.0*pow(x,2.0)*pow(y,9.0);
        d_temp=d_temp+2366437.0/16200.0*pow(x,3.0)*pow(y,5.0)-25757.0/108.0*pow(x,4.0)*pow(y,3.0)+109.0/100.0*x*pow(y,9.0);
        d_temp=d_temp+16607.0/12150.0*pow(y,7.0)-1619.0/50.0*pow(x,5.0)*pow(y,3.0)-1621837.0/10800.0*pow(x,5.0)*pow(y,5.0);
        d_temp=d_temp+157853.0/2025.0*pow(x,4.0)*pow(y,5.0)+241819.0/24300.0*pow(x,5.0)*pow(y,9.0)-96709.0/4860.0*pow(x,4.0)*pow(y,9.0);
        d_temp=d_temp+43999.0/4860.0*pow(x,3.0)*pow(y,9.0)-2052959.0/16200.0*pow(x,3.0)*pow(y,7.0)-39167.0/1620.0*pow(x,5.0)*pow(y,7.0);
        d_temp=d_temp+44071.0/400.0*pow(x,4.0)*pow(y,7.0)+79751.0/1200.0*pow(x,6.0)*pow(y,5.0)+280759.0/2400.0*pow(x,5.0)*y;
        d_temp=d_temp+8749.0/90.0*pow(x,6.0)*pow(y,3.0)-29627.0/675.0*pow(x,7.0)*pow(y,3.0)-622.0/675.0*pow(y,9.0)+823.0/80.0*pow(x,8.0)*y;
        d_temp=d_temp+2257.0/900.0*pow(x,7.0)*pow(y,7.0)-2497.0/1350.0*pow(x,2.0)*pow(y,11.0)-1151.0/2025.0*x*pow(y,11.0);
        d_temp=d_temp-3457.0/16200.0*pow(x,5.0)*pow(y,11.0)-27989.0/12150.0*pow(x,6.0)*pow(y,9.0)+329.0/3600.0*pow(x,4.0)*pow(y,11.0);
        d_temp=d_temp+104137.0/97200.0*pow(x,3.0)*pow(y,11.0)+439.0/10800.0*pow(x,2.0)*pow(y,13.0)-9421.0/2430.0*pow(x,6.0)*pow(y,7.0);
        d_temp=d_temp-19547.0/1620.0*pow(x,7.0)*pow(y,5.0)+1583.0/64800.0*x*pow(y,13.0)+3193.0/600.0*pow(x,6.0)*y+5003.0/540.0*pow(x,8.0)*pow(y,3.0);
        d_temp=d_temp+176.0/2025.0*pow(y,11.0)+19.0/12150.0*pow(y,13.0)-379.0/9720.0*pow(x,3.0)*pow(y,13.0)-7769.0/300.0*pow(x,7.0)*y;
        d_temp=d_temp-4583.0/2400.0*pow(x,9.0)*y+3191.0/48600.0*pow(x,6.0)*pow(y,11.0)+2093.0/8100.0*pow(x,7.0)*pow(y,9.0)+7.0/48600.0*pow(x,6.0)*pow(y,13.0);
        d_temp=d_temp-469.0/1200.0*pow(x,8.0)*pow(y,7.0)-49.0/21600.0*pow(x,5.0)*pow(y,13.0)+89.0/6480.0*pow(x,4.0)*pow(y,13.0)+9109.0/16200.0*pow(x,8.0)*pow(y,5.0);
        d_temp=d_temp-131.0/135.0*pow(x,9.0)*pow(y,3.0)+71.0/400.0*pow(x,10.0)*y-91.0/10800.0*pow(x,7.0)*pow(y,11.0)-11.0/972.0*pow(x,8.0)*pow(y,9.0);
        d_temp=d_temp+14.0/675.0*pow(x,9.0)*pow(y,7.0)+581.0/6480.0*pow(x,9.0)*pow(y,5.0)+11.0/270.0*pow(x,10.0)*pow(y,3.0)-1.0/150.0*pow(x,11.0)*y-1.0/12150.0*pow(y,15.0);
        d_temp=d_temp+13.0/32400.0*pow(x,8.0)*pow(y,11.0)-287.0/32400.0*pow(x,10.0)*pow(y,5.0);*/

        break;

    case 41://is the same as case 36 (Example 9)
        //d_temp=sin(x*y);
        //d_temp=4.0;
        //d_temp=x;
        d_temp=x*sin(2.0*y);

        break;

    case 42:// case 2, Example 2_0
        d_temp=-21.205*x+112.58*y*y+730.37*x*x+659.76*x*x*y*y;
        d_temp=d_temp-521.16*x*y*y-1349.6*x*x*x-406*x*x*x*x*x+1071.4*x*x*x*x;
        d_temp=d_temp+88.0*y*y*y*y+60.0*x*x*x*x*x*x-352.0*y*y*x*x*x-66.0*y*y*y*y*x+72.0*y*y*x*x*x*x+12.0*y*y*y*y*x*x;
        d_temp=d_temp-83.605;
        break;

    case 43:
        d_temp=4.0;
        break;

    case 44:
        d_temp=4.0;
        break;

    case 45:
        d_temp=4.0;
        break;

    case 46:
        d_temp=1.0;
        break;

    case 47:
        d_temp=1.0;
        break;

    case 48:
        d_temp=(2.0-x*x-y*y)*(2.0*sin(x*y)+4.0*x*y*cos(x*y))+(x*x+y*y)*sin(x*y)*(1-x*x)*(1-y*y);
        break;

    case 49:
        d_temp=2*(sin(x*y) + 1)*(pow(x,2) - 1)*(pow(x,2) - pow(y,2)) + 2*(sin(x*y) + 1)*(pow(y,2) - 1)*(pow(x,2) - pow(y,2)) + 8*pow(x,2)*(sin(x*y) + 1)*(pow(y,2) - 1) - 8*pow(y,2)*(sin(x*y) + 1)*(pow(x,2) - 1) + 4*x*y*cos(x*y)*(pow(x,2) - 1)*(pow(x,2) - pow(y,2)) + 4*x*y*cos(x*y)*(pow(y,2) - 1)*(pow(x,2) - pow(y,2)) - pow(x,2)*sin(x*y)*(pow(x,2) - 1)*(pow(y,2) - 1)*(pow(x,2) - pow(y,2)) - pow(y,2)*sin(x*y)*(pow(x,2) - 1)*(pow(y,2) - 1)*(pow(x,2) - pow(y,2));
        break;

    case 50:
        d_temp=-(2000000.0*(pow(x,10) + 9.0*pow(x,8.0)*pow(y,2) - 6.0*pow(x,8) - 10.0*pow(x,6)*pow(y,4) - 24.0*pow(x,6)*pow(y,2) + 200012.0*pow(x,6) - 10.0*pow(x,4)*pow(y,6) + 60.0*pow(x,4)*pow(y,4) - 600012.0*pow(x,4)*pow(y,2) + 800000.0*pow(x,4) + 9.0*pow(x,2)*pow(y,8) - 24.0*pow(x,2)*pow(y,6) - 600012.0*pow(x,2)*pow(y,4) + 9999600000.0*pow(x,2) + pow(y,10) - 6.0*pow(y,8) + 200012.0*pow(y,6) + 800000.0*pow(y,4) + 9999600000.0*pow(y,2) - 20000000000.0))/pow((pow(x,4) - 2.0*pow(x,2)*pow(y,2) + pow(y,4) + 100000.0),3);
        break;

    case 51:
        d_temp=1.0;
        break;

    case 52:
        d_temp=1.0;
        break;

    case 53:
        ////Ex3_1_1
        //d_temp=(y*(- 24.0*pow(x,6.0) - 37.0*pow(x,4.0)*pow(y,2.0) + 84.0*pow(x,4.0) - 21.0*pow(x,2.0)*pow(y,4.0) + 168.0*pow(x,2.0)*pow(y,2.0) + 48.0*pow(x,2.0) - 8.0*pow(y,6.0) + 84.0*pow(y,4.0) + 48.0*pow(y,2.0)))/(4.0*pow((pow(x,2.0) + pow(y,2.0)),(9.0/4.0)));
        ////Ex3_1_2
        //d_temp=-(y*(24.0*pow(x,6.0) + 77.0*pow(x,4.0)*pow(y,2.0) - 180.0*pow(x,4.0) + 61.0*pow(x,2.0)*pow(y,4.0) - 360.0*pow(x,2.0)*pow(y,2.0) + 80.0*pow(x,2.0) + 8.0*pow(y,6.0) - 180.0*pow(y,4.0) + 80.0*pow(y,2.0)))/(4.0*pow((pow(x,2.0) + pow(y,2.0)),(7.0/4.0)));

        ////Ex3_1_3
        d_temp=sin(y)*(x*x - 4.0)*(y*y - 4.0) - 2.0*sin(y)*(y*y - 4.0) - 2.0*sin(y)*(x*x - 4.0) - 4.0*y*cos(y)*(x*x - 4.0);
        break;

    case 54:
        //d_temp=-(2.0*x*y*(27.0*pow(x,6.0) + 119.0*pow(x,4.0)*pow(y,2.0) - 80.0*pow(x,4.0) + 119.0*pow(x,2.0)*pow(y,4.0) - 160.0*pow(x,2.0)*pow(y,2.0) + 14.0*pow(x,2.0) + 27.0*pow(y,6.0) - 80.0*pow(y,4.0) + 14.0*pow(y,2.0)))/(9.0*pow((pow(x,2.0) + pow(y,2.0)),(5.0/3.0)));
        //d_temp=(2.0*x*y*(- 27.0*pow(x,6.0) - 17.0*pow(x,4.0)*pow(y,2.0) + 14.0*pow(x,4.0) - 17.0*pow(x,2.0)*pow(y,4.0) + 28.0*pow(x,2.0)*pow(y,2.0) + 16.0*pow(x,2.0) - 27.0*pow(y,6.0) + 14.0*pow(y,4.0) + 16.0*pow(y,2.0)))/(9.0*pow((x*x + y*y),(8.0/3.0)));
        //
        ///Ex3_2_1:
        //d_temp=(2.0*x*y*(-27.0*pow(x,6.0)-17.0*pow(x,4.0)*pow(y,2.0)+14.0*pow(x,4.0)-17.0*pow(x,2.0)*pow(y,4.0)+28.0*pow(x,2.0)*pow(y,2.0)+16.0*pow(x,2.0)-27.0*pow(y,6.0)+14.0*pow(y,4.0)+16.0*pow(y,2.0)))/(9.0*pow(pow(x,2.0)+pow(y,2.0),8.0/3.0));
        ///Ex3_2_2
        d_temp=(x*y*(- 24.0*pow(x,6.0) - 49.0*pow(x,4.0)*pow(y,2.0) + 33.0*pow(x,4.0) - 49.0*pow(x,2.0)*pow(y,4.0) + 66.0*pow(x,2.0)*pow(y,2.0) + 7.0*pow(x,2.0) - 24.0*pow(y,6.0) + 33.0*pow(y,4.0) + 7.0*pow(y,2.0)))/(4.0*pow((x*x + y*y),(9.0/4.0)));

        ////Ex3_2_3
        d_temp=-2.0*x*(- 6.0*pow(x,4.0)*pow(y,2.0) + pow(x,4.0) - 10.0*pow(x,2.0)*pow(y,4.0) + 40.0*pow(x,2.0)*pow(y,2.0) - 5.0*pow(x,2.0) + 15.0*pow(y,4.0) - 39.0*pow(y,2.0) + 4.0);
        break;


    case 55:
        d_temp=y*exp(x/4.0 - y/4.0)*(x*x - 4.0) - 2.0*exp(x/4.0 - y/4.0)*(y*y - 4.0) - (exp(x/4.0 - y/4.0)*(x*x - 4.0)*(y*y - 4.0))/8.0 - x*exp(x/4.0 - y/4.0)*(y*y - 4.0) - 2.0*exp(x/4.0 - y/4.0)*(x*x - 4.0);
        break;

    case 56:
        d_temp=2.0*y-y*y+4.0*x-2.0*x*x;
        break;

    default://Example 4
        d_temp=4.0;
        break;
    }
	return d_temp;
//==================================
}

// /////////////////////////////
//PDE_PFEM
//==============================
static double ExactFunctionValue_PDE_PFEM(double x, double y, unsigned int ExampleIndex, unsigned int EquationIndex)
{
    double d_temp;
    switch(ExampleIndex)
    {

    case 25:
        switch(EquationIndex)
        {
        case 1:
            d_temp=(1.0-x*x)*(1.0-y*y);
            break;
        case 2:
            d_temp=(1.0-x*x)*(1.0-y*y);
            break;
        case 3:
            d_temp=(1.0-x*x)*(1.0-y*y);
            break;

        case 4:
            d_temp=(1.0-x*x)*(1.0-y*y);
            break;
        default://
            cout<<"case 25: EquationIndex is out of range!"<<endl;
            cin.get();
            break;
        }

        break;

    case 29:
        switch (EquationIndex)
        {
        case 1:
            //F1
            d_temp=-(1.0/4.0)*(x-1.0)*(x-1.0)*(x-1.0)+(5.0/4.0)*(x-1.0)*(x-1.0)-(x-1.0)+y-3.0;
            //F4
            d_temp=d_temp*((1.0/4.0)*(x-1.0)*(x-1.0)*(x-1.0)-(5.0/4.0)*(x-1.0)*(x-1.0)+(x-1.0)+y+3.0);

            //F2
            d_temp=d_temp*((1.0/9.0)*y*y*y-(2.0/3.0)*y*y+x+2.0);
            //F3
            d_temp=d_temp*(-(1.0/9.0)*y*y*y-(2.0/3.0)*y*y+x+2.0);

            //F6
            d_temp=d_temp*((1.0-y)*(1.0-y)*(1.0-y)-(1.0-y)*(1.0-y)-(1.0-y)-x+5.0);
            //F5
            d_temp=d_temp*((1.0+y)*(1.0+y)*(1.0+y)-(1.0+y)*(1.0+y)-(1.0+y)-x+5.0);

            //F7
            d_temp=d_temp*(x-5.0);

            //======================
            d_temp=d_temp*x*y/300.0;
            //======================
            break;
        case 2:
            //F1
            d_temp=-(1.0/4.0)*(x-1.0)*(x-1.0)*(x-1.0)+(5.0/4.0)*(x-1.0)*(x-1.0)-(x-1.0)+y-3.0;
            //F4
            d_temp=d_temp*((1.0/4.0)*(x-1.0)*(x-1.0)*(x-1.0)-(5.0/4.0)*(x-1.0)*(x-1.0)+(x-1.0)+y+3.0);

            //F2
            d_temp=d_temp*((1.0/9.0)*y*y*y-(2.0/3.0)*y*y+x+2.0);
            //F3
            d_temp=d_temp*(-(1.0/9.0)*y*y*y-(2.0/3.0)*y*y+x+2.0);

            //F6
            d_temp=d_temp*((1.0-y)*(1.0-y)*(1.0-y)-(1.0-y)*(1.0-y)-(1.0-y)-x+5.0);
            //F5
            d_temp=d_temp*((1.0+y)*(1.0+y)*(1.0+y)-(1.0+y)*(1.0+y)-(1.0+y)-x+5.0);

            //F7
            d_temp=d_temp*(x-5.0);

            //======================
            d_temp=d_temp*x*y/300.0;
            //======================
            break;
        case 3:
            //F1
            d_temp=-(1.0/4.0)*(x-1.0)*(x-1.0)*(x-1.0)+(5.0/4.0)*(x-1.0)*(x-1.0)-(x-1.0)+y-3.0;
            //F4
            d_temp=d_temp*((1.0/4.0)*(x-1.0)*(x-1.0)*(x-1.0)-(5.0/4.0)*(x-1.0)*(x-1.0)+(x-1.0)+y+3.0);

            //F2
            d_temp=d_temp*((1.0/9.0)*y*y*y-(2.0/3.0)*y*y+x+2.0);
            //F3
            d_temp=d_temp*(-(1.0/9.0)*y*y*y-(2.0/3.0)*y*y+x+2.0);

            //F6
            d_temp=d_temp*((1.0-y)*(1.0-y)*(1.0-y)-(1.0-y)*(1.0-y)-(1.0-y)-x+5.0);
            //F5
            d_temp=d_temp*((1.0+y)*(1.0+y)*(1.0+y)-(1.0+y)*(1.0+y)-(1.0+y)-x+5.0);

            //F7
            d_temp=d_temp*(x-5.0);

            //======================
            d_temp=d_temp*x*y/300.0;
            //======================
            break;
        case 4:
            //F1
            d_temp=-(1.0/4.0)*(x-1.0)*(x-1.0)*(x-1.0)+(5.0/4.0)*(x-1.0)*(x-1.0)-(x-1.0)+y-3.0;
            //F4
            d_temp=d_temp*((1.0/4.0)*(x-1.0)*(x-1.0)*(x-1.0)-(5.0/4.0)*(x-1.0)*(x-1.0)+(x-1.0)+y+3.0);

            //F2
            d_temp=d_temp*((1.0/9.0)*y*y*y-(2.0/3.0)*y*y+x+2.0);
            //F3
            d_temp=d_temp*(-(1.0/9.0)*y*y*y-(2.0/3.0)*y*y+x+2.0);

            //F6
            d_temp=d_temp*((1.0-y)*(1.0-y)*(1.0-y)-(1.0-y)*(1.0-y)-(1.0-y)-x+5.0);
            //F5
            d_temp=d_temp*((1.0+y)*(1.0+y)*(1.0+y)-(1.0+y)*(1.0+y)-(1.0+y)-x+5.0);

            //F7
            d_temp=d_temp*(x-5.0);

            //======================
            d_temp=d_temp*x*y/300.0;
            //======================
            break;

        default:
            cout<<"case 29: EquationIndex is out of range!"<<endl;
            break;
        }
        break;

    case 31:
        switch (EquationIndex)
        {
        case 1:
            d_temp=(-2.0*x+2.0-y)*(2.0*x+2.0-y)*y*sin(x);
            break;
        default:
            cout<<"case 31:EquationIndex is out of range!"<<endl;
            cin.get();
            break;
        }
        break;

    case 32:
        switch (EquationIndex)
        {
        case 1:
            d_temp=(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x-2.0)*((x-2.0)*(x-2.0)+y*y-0.25);
            break;
        case 2:
            d_temp=(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x-2.0)*((x-2.0)*(x-2.0)+y*y-0.25);
            break;
        case 3:
            d_temp=(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x-2.0)*((x-2.0)*(x-2.0)+y*y-0.25);
            break;
        case 4:
            d_temp=(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x-2.0)*((x-2.0)*(x-2.0)+y*y-0.25);
            break;
        default:
            cout<<"case 32: EquationIndex is out of the range!"<<endl;
            cin.get();
            break;
        }
        break;

    case 33:
        switch (EquationIndex)
        {
        case 1:
            d_temp=(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x-2.0)*(2.0*y*y+1.5-x);
            break;
        case 2:
            d_temp=(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x-2.0)*(2.0*y*y+1.5-x);
            break;
        case 3:
            d_temp=(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x-2.0)*(2.0*y*y+1.5-x);
            break;
        case 4:
            d_temp=(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x-2.0)*(2.0*y*y+1.5-x);
            break;
        default:
            cout<<"case 33: EquationIndex is out of the range!"<<endl;
            cin.get();
            break;
        }
        break;

    case 34:
        switch(EquationIndex)
        {
        case 1:
            d_temp=1.0-x*x-y*y;
            break;
        case 2:
            d_temp=1.0-x*x-y*y;
            break;
        case 3:
            d_temp=1.0-x*x-y*y;
            break;
        case 4:
            d_temp=1.0-x*x-y*y;
            break;
        }
        break;

    case 35:
        switch(EquationIndex)
        {
        case 1:
            d_temp=(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x-2.0)*(2.0*y*y+1.5-x);
            break;
        }
        break;


    default://Example 4
        d_temp=1.0-(x*x+y*y);
        cout<<"ExactFunctionValue_PDE_PFEM: ExampleIndex is out of range!"<<endl;
        cin.get();
        break;
    }

    return d_temp;
}
//============================

//======================================
static double FxFunctionValue_PDE_PFEM(double x, double y, unsigned int ExampleIndex, unsigned int EquationIndex)
{
    double d_temp=0.0;
    switch(ExampleIndex)
    {

    case 25:
        switch(EquationIndex)
        {
        case 1:
            d_temp=-2.0*x*(1.0-y*y);
            break;
        case 2:
            d_temp=-2.0*x*(1.0-y*y);
            break;
        case 3:
            d_temp=-2.0*x*(1.0-y*y);
            break;
        default:
            cout<<"EquationIndex is out of range!"<<endl;
            cin.get();
            break;
        }
        break;

    case 29:
        switch (EquationIndex)
        {
        //case 1:
           // d_temp=(1.0/300.0)*(-(1.0/4.0)*pow((x-1),2)+2.0*(-(1.0/4.0)*x+(1.0/4.0))*(x-1.0)+(5.0/2.0)*x-(7.0/2.0))*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/300.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/300.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/300.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*((1.0/4.0)*pow((x-1),2)+2.0*((1.0/4.0)*x-(1.0/4.0))*(x-1.0)-(5.0/2.0)*x+(7.0/2.0))*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y-(1.0/300.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y-(1.0/300.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(x-5.0)*x*y+(1.0/300.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*x*y+(1.0/300.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*y;
         //   break;


        default:
            cout<<"FxFunctionValue_PDE_PFEM:: case29 EquationIndex is out of the range"<<endl;
            cin.get();
            break;
        }
        break;

    case 31:
        switch(EquationIndex)
        {
        case 1:
            d_temp=-8.0*x*y*sin(x)+(-2.0*x+2.0-y)*(2.0*x+2.0-y)*y*cos(x);
            break;

        default:
            cout<<"FxFunctionValue_PDE_PFEM:: case32 EquationIndex is out of the range"<<endl;
            cin.get();
            break;
        }

        break;

    default:
        cout<<"FxFunctionValue_PDE_PFEM:: ExampleIndex is out of the range"<<endl;
        cin.get();
        break;
    }

    return d_temp;
}

//======================================
static double FyFunctionValue_PDE_PFEM(double x, double y, unsigned int ExampleIndex, unsigned int EquationIndex)
{
    double d_temp=0.0;
    switch(ExampleIndex)
    {

    case 25:
        switch(EquationIndex)
        {
        case 1:
            d_temp=-2.0*y*(1.0-x*x);
            break;
        case 2:
            d_temp=-2.0*y*(1.0-x*x);
            break;
        case 3:
            d_temp=-2.0*y*(1.0-x*x);
            break;
        default:
            cout<<"FyFunctionValue_PDE_PFEM:case25: EquationIndex is out of range!"<<endl;
            cin.get();
            break;
        }
        break;

    case 29:
        switch(EquationIndex)
        {
        //case 1:
          //  d_temp=(1.0/300.0)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/300.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/3.0)*pow(y,2)-(4.0/3.0)*y)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/300.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/3.0)*pow(y,2)-(4.0/3.0)*y)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/300.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/300.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(3.0*pow((1+y),2)-3.0-2.0*y)*(pow((1.0-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/300.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(-3.0*pow((1-y),2)+3.0-2.0*y)*(x-5.0)*x*y+(1.0/300.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x;
          //  break;


        default:
            cout<<"FyFunctionValue_PDE_PFEM:case29: EquationIndex is out of range!"<<endl;
            cin.get();
            break;
        }
        break;

    case 31:
        switch(EquationIndex)
        {
        case 1:
            d_temp=-y*sin(x)*(4.0-2.0*y)+(-2.0*x+2.0-y)*(2.0*x+2.0-y)*sin(x);
            break;
        }

        break;


    default:
        cout<<"FyFunctionValue_PDE_PFEM:: ExampleIndex is out of the range"<<endl;
        cin.get();
        break;
    }

    return d_temp;
}
//===================================
static double RightSideFunctionValue_PDE_PFEM(double x, double y, unsigned int ExampleIndex, unsigned int EquationIndex)
{
    double d_temp;
    switch(ExampleIndex)
    {

    case 25:
        switch(EquationIndex)
        {
        case 1:
            d_temp=2.0*((1.0-y*y)*(2.0-x)/((x+2.0)*(x+2.0)*(x+2.0))+(1.0-x*x)/((x+2.0)*(x+2.0)));
            break;
        case 2:
            d_temp=2.0*((1.0-y*y)*(2.0-x)/((x+2.0)*(x+2.0)*(x+2.0))+(1.0-x*x)/((x+2.0)*(x+2.0)));
            d_temp=d_temp+(1.0-x*x)*(1.0-y*y)/((x+2.0)*(x+2.0));
            break;
        case 3:
            d_temp=2.0*((1.0-y*y)*(2.0-x)/((x+2.0)*(x+2.0)*(x+2.0))+(1.0-x*x)/((x+2.0)*(x+2.0)));
            d_temp=d_temp+(1.0-x*x)*(1.0-x*x)*(1.0-y*y)*(1.0-y*y)/((x+2.0)*(x+2.0));
            break;
        case 4:
            d_temp=(8.0-4.0*pow(x,2.0)-4.0*pow(y,2.0)+4.0*pow(x,2.0)*pow(y,2.0)-4.0*pow(x,2.0)*pow(y,4.0)-4.0*pow(x,4.0)*pow(y,2.0)+2.0*pow(x,4.0)*pow(y,4.0)+2.0*pow(y,2.0)*x+pow(y,4.0)*x+2.0*pow(x,3.0)*pow(y,2.0)-2.0*pow(x,3.0)*pow(y,4.0)-2.0*pow(x,5.0)*pow(y,2.0)+pow(x,5.0)*pow(y,4.0)+2.0*pow(x,4.0)+2.0*pow(y,4.0)-2.0*pow(x,3.0)+pow(x,5.0))/pow(x+2.0,3.0);
            break;
        case 42:
            d_temp=-1.0/((x+2.0)*(x+2.0));
            break;
        default:
            cout<<"EquationIndex is out of the range!"<<endl;
            break;
        }

        break;

    case 29:
        switch(EquationIndex)
        {
        case 1:
           // d_temp=-(1.0/300.0*(-(3.0/2.0)*x+4.0)*((1.0/9.0)*pow(y, 3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/150.0)*(-(1.0/4.0)*pow((x-1),2)+2.0*(-(1.0/4.0)*x+(1.0/4.0))*(x-1.0)+(5.0/2.0)*x-(7.0/2.0))*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1.0),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/150.0)*(-(1.0/4.0)*pow((x-1),2)+2.0*(-(1.0/4.0)*x+(1.0/4.0))*(x-1.0)+(5.0/2.0)*x-(7.0/2.0))*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/150.0)*(-(1.0/4.0)*pow((x-1),2)+2.0*(-(1.0/4.0)*x+(1.0/4.0))*(x-1.0)+(5.0/2.0)*x-(7.0/2.0))*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*((1.0/4.0)*pow((x-1),2)+2.0*((1.0/4.0)*x-(1.0/4.0))*(x-1.0)-(5.0/2.0)*x+(7.0/2.0))*(pow((1.0+y),3)-pow((1.0+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/150.0)*(-(1.0/4.0)*pow((x-1),2)+2.0*(-(1.0/4.0)*x+(1.0/4.0))*(x-1.0)+(5.0/2.0)*x-(7.0/2.0))*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*x*y+(1.0/150.0)*(-(1.0/4.0)*pow((x-1),2)+2.0*(-(1.0/4.0)*x+(1.0/4.0))*(x-1.0)+(5.0/2.0)*x-(7.0/2.0))*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*y-(1.0/150.0)*(-(1.0/4.0)*pow((x-1),2)+2.0*(-(1.0/4.0)*x+(1.0/4.0))*(x-1.0)+(5.0/2.0)*x-(7.0/2.0))*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y-(1.0/150.0)*(-(1.0/4.0)*pow((x-1),2)+2.0*(-(1.0/4.0)*x+(1.0/4.0))*(x-1.0)+(5.0/2.0)*x-(7.0/2.0))*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*((0.25*x-0.25)*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(x-5.0)*x*y+(1.0/150.0)*((-0.25*x+0.25)*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*((1.0/4.0)*pow((x-1),2)+2.0*((1.0/4.0)*x-(1.0/4.0))*(x-1.0)-(5.0/2.0)*x+(7.0/2.0))*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/150.0)*((-0.25*x+0.25)*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*(-1.0/9.0*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*x*y+(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1.0+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*y-(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y-(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(x-5.0)*x*y+1.0/150.0*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*((1.0/4.0)*pow((x-1),2)+2.0*((1.0/4.0)*x-(1.0/4.0))*(x-1.0)-(5.0/2.0)*x+(7.0/2.0))*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*x*y+(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*y-(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y-(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(x-5.0)*x*y+(1.0/300.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*((3.0/2.0)*x-4.0)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*((1.0/4.0)*pow((x-1),2)+2.0*((1.0/4.0)*x-(1.0/4.0))*(x-1.0)-(5.0/2.0)*x+(7.0/2.0))*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*x*y+(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*((1.0/4.0)*pow((x-1),2)+2.0*((1.0/4.0)*x-(1.0/4.0))*(x-1.0)-(5.0/2.0)*x+(7.0/2.0))*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*y-(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*((1.0/4.0)*pow((x-1),2)+2.0*((1.0/4.0)*x-(1.0/4.0))*(x-1.0)-(5.0/2.0)*x+(7.0/2.0))*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y-(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*((1.0/4.0)*pow((x-1),2)+2.0*((1.0/4.0)*x-(1.0/4.0))*(x-1.0)-(5.0/2.0)*x+(7.0/2.0))*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(x-5.0)*x*y-(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*x*y-(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*y+(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(x-5.0)*x*y-(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*x*y-(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(x-5.0)*y+(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*y)/pow((x+3),2)+2.0*((1.0/300.0)*(-(1.0/4.0)*pow((x-1),2)+2.0*(-(1.0/4.0)*x+(1.0/4.0))*(x-1.0)+(5.0/2.0)*x-(7.0/2.0))*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/300.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/300.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/300.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*((1.0/4.0)*pow((x-1),2)+2.0*((1.0/4.0)*x-(1.0/4.0))*(x-1.0)-(5.0/2.0)*x+(7.0/2.0))*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y-(1.0/300.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1.0-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y-(1.0/300.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(x-5.0)*x*y+(1.0/300.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*x*y+(1.0/300.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1.0-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*y)/pow((x+3),3)-((1.0/150.0)*((1.0/3.0)*pow(y,2)-(4.0/3.0)*y)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/150.0)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/3.0)*pow(y,2)-(4.0/3.0)*y)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/150.0)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/150.0)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(3.0*pow((1+y),2)-3.0-2.0*y)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/150.0)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(-3*pow((1-y),2)+3.0-2.0*y)*(x-5.0)*x*y+(1.0/150.0)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x+(1.0/300.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((2.0/3.0)*y-(4.0/3.0))*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/3.0)*pow(y,2)-(4.0/3.0)*y)*(-(1.0/3.0)*pow(y,2)-(4.0/3.0)*y)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/3.0)*pow(y,2)-(4.0/3.0)*y)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/3.0)*pow(y,2)-(4.0/3.0)*y)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(3.0*pow((1+y),2)-3.0-2.0*y)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/3.0)*pow(y,2)-(4.0/3.0)*y)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(-3.0*pow((1-y),2)+3.0-2.0*y)*(x-5.0)*x*y+(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/3.0)*pow(y,2)-(4.0/3.0)*y)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x+(1.0/300.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(2.0/3.0)*y-(4.0/3.0))*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/3.0)*pow(y,2)-(4.0/3.0)*y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/3.0)*pow(y,2)-(4.0/3.0)*y)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(3.0*pow((1+y),2)-3.0-2.0*y)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/3.0)*pow(y,2)-(4.0/3.0)*y)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(-3.0*pow((1-y),2)+3.0-2.0*y)*(x-5.0)*x*y+(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/3.0)*pow(y,2)-(4.0/3.0)*y)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x+(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(3.0*pow((1+y),2)-3.0-2.0*y)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(-3.0*pow((1-y),2)+3.0-2.0*y)*(x-5.0)*x*y+(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x+(1.0/300.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(4.0+6.0*y)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x*y+(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(3.0*pow((1+y),2)-3.0-2.0*y)*(-3.0*pow((1-y),2)+3.0-2.0*y)*(x-5.0)*x*y+(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(3.0*pow((1+y),2)-3.0-2.0*y)*(pow((1-y),3)-pow((1-y),2)+4.0+y-x)*(x-5.0)*x+(1.0/300.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(4.0-6.0*y)*(x-5.0)*x*y+(1.0/150.0)*((-(1.0/4.0)*x+(1.0/4.0))*pow((x-1),2)+((5.0/4.0)*x-(5.0/4.0))*(x-1.0)-x-2.0+y)*((1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(-(1.0/9.0)*pow(y,3)-(2.0/3.0)*pow(y,2)+x+2.0)*(((1.0/4.0)*x-(1.0/4.0))*pow((x-1),2)-((5.0/4.0)*x-(5.0/4.0))*(x-1.0)+x+2.0+y)*(pow((1+y),3)-pow((1+y),2)+4.0-y-x)*(-3.0*pow((1-y),2)+3.0-2.0*y)*(x-5.0)*x)/pow((x+3),2);

            d_temp=-1.0/194400.0*y*(5277312.0+59228496.0*x+19647504.0*pow(x,2.0)*pow(y,6.0)-78.0*pow(x,9.0)*pow(y,10.0)+1982016.0*pow(y,2)-94569120.0*pow(x,2.0)+13366368.0*pow(x,2.0)*pow(y,2.0)+26242272.0*x*pow(y,2.0)+39944352.0*pow(x,2.0)*pow(y,4.0)-187363728.0*pow(x,3.0)*pow(y,2.0)-3705484.0*x*pow(y,6.0)-22442832.0*pow(x,3.0)+123070752.0*pow(x,4.0)-911296.0*pow(y,6.0)-2268288.0*pow(y,4.0)-8673408.0*x*pow(y,4.0)-4717998.0*pow(x,5.0)*pow(y,6.0)-43863540.0*pow(x,4.0)*pow(y,6.0)+80160364.0*pow(x,3.0)*pow(y,6.0)+58765320.0*pow(x,5.0)*pow(y,2.0)+99218880.0*pow(x,4.0)*pow(y,2.0)+77667630.0*pow(x,5.0)*pow(y,4.0)-71490468.0*pow(x,4.0)*pow(y,4.0)-84739812.0*pow(x,3)*pow(y,4)-54076032.0*pow(x,6.0)*pow(y,2.0)-814824.0*x*pow(y,8.0)-11804208.0*pow(x,2.0)*pow(y,8.0)-41605245.0*pow(x,5.0)-22532094.0*pow(x,6.0)+15289884.0*pow(x,7.0)-2077280.0*pow(x,5.0)*pow(y,8.0)+6377056.0*pow(x,6.0)*pow(y,6.0)+9435680.0*pow(x,4.0)*pow(y,8.0)-7037912.0*pow(x,3.0)*pow(y,8.0)+1329864.0*pow(x,2.0)*pow(y,10.0)+480288.0*pow(y,8.0)-13356000.0*pow(x,6.0)*pow(y,4.0)+9602928.0*pow(x,7.0)*pow(y,2.0)+348384.0*x*pow(y,10.0)-1829952.0*pow(x,8.0)-22368.0*pow(y,10.0)-32106.0*pow(x,2.0)*pow(y,12.0)-13943.0*x*pow(y,12.0)+63966.0*pow(x,5.0)*pow(y,10.0)-502096.0*pow(x,6.0)*pow(y,8.0)-279.0*pow(x,5.0)*pow(y,12.0)-680792.0*pow(x,7.0)*pow(y,6.0)-109972.0*pow(x,4.0)*pow(y,10.0)-4220.0*pow(x,4.0)*pow(y,12.0)-520774.0*pow(x,3.0)*pow(y,10.0)+21226.0*pow(x,3.0)*pow(y,12.0)-4791798.0*pow(x,7.0)*pow(y,4.0)+2349792.0*pow(x,8.0)*pow(y,2.0)-4132.0*pow(y,12.0)-683721.0*pow(x,9.0)+9072.0*pow(x,6.0)*pow(y,10.0)+281064.0*pow(x,7.0)*pow(y,8.0)+210.0*pow(x,6.0)*pow(y,12.0)-252108.0*pow(x,8.0)*pow(y,6.0)+1869324.0*pow(x,8.0)*pow(y,4.0)-1137960.0*pow(x,9.0)*pow(y,2.0)+245430.0*pow(x,10.0)-16.0*x*pow(y,14.0)-42624.0*pow(x,8.0)*pow(y,8.0)-8170.0*pow(x,7.0)*pow(y,10.0)-153678.0*pow(x,9.0)*pow(y,4.0)+1404.0*pow(x,8.0)*pow(y,10.0)+63062.0*pow(x,9.0)*pow(y,6.0)-12264.0*pow(x,10.0)*pow(y,4.0)+128.0*pow(y,14.0)-20.0*pow(x,7.0)*pow(y,12.0)+160128.0*pow(x,10.0)*pow(y,2.0)-29646.0*pow(x,11.0)+2200.0*pow(x,9.0)*pow(y,8.0)-7920.0*pow(x,11.0)*pow(y,2.0)-4032.0*pow(x,10.0)*pow(y,6.0)+1296.0*pow(x,12.0)+1722.0*pow(x,11.0)*pow(y,4.0));
            d_temp=(1.0/pow(x+3.0, 3.0))*d_temp;

            break;
//        case 2:
//            break;
//        case 3:
//            break;
//        case 4:
//            break;

        }
        break;

    case 31:
        switch(EquationIndex)
        {
        case 1:
            d_temp=-(-8.0*y*sin(x)-16.0*x*y*cos(x)-(-2.0*x+2.0-y)*(2.0*x+2.0-y)*y*sin(x));
            d_temp=d_temp-(sin(x)*(-8.0+6.0*y));
            break;
        default:
            cout<<"case 31: EquationIndex is out of the range!"<<endl;
            cin.get();
            break;
        }
        break;

    case 32:
        switch(EquationIndex)
        {
//        case 1:
//            d_temp=0.5*(-210.0+40.0*pow(x,5.0)-1323.0*pow(x,2.0)-72.0*pow(x,2.0)*pow(y,2.0)+172.0*x*y*y-308.0*pow(x,4.0)+922.0*pow(x,3.0)+4.0*pow(y,4.0)+8.0*pow(x,3.0)*pow(y,2.0)+889.0*x-137.0*y*y)/(pow(x-2.0, 3.0));
//            break;
        case 1:
            d_temp=0.5*(82.0+231.0*pow(y,2.0)-979.0*x-438.0*pow(x,3.0)+1229.0*pow(x,2.0)-164.0*x*pow(y,2.0)+24.0*pow(y,2.0)*pow(x,2.0)-52.0*pow(x,4.0)+40.0*pow(x,5.0)+8.0*pow(y,2.0)*pow(x,3.0)+4.0*pow(y,4.0))/pow((x+2.0),3.0);
            break;

        case 2:
            d_temp=0.5*(82.0+231.0*pow(y,2.0)-979.0*x-438.0*pow(x,3.0)+1229.0*pow(x,2.0)-164.0*x*pow(y,2.0)+24.0*pow(y,2.0)*pow(x,2.0)-52.0*pow(x,4.0)+40.0*pow(x,5.0)+8.0*pow(y,2.0)*pow(x,3.0)+4.0*pow(y,4.0))/pow((x+2.0),3.0);
            d_temp=d_temp+((x-2.0)*(x-2.0)+y*y-0.25)*(2.0-x)*(2.0+x*(x-1.0)*(x-1.0)-y*y)/((x+2.0)*(x+2.0));
            break;

        case 3:
            d_temp=0.5*(82.0+231.0*pow(y,2.0)-979.0*x-438.0*pow(x,3.0)+1229.0*pow(x,2.0)-164.0*x*pow(y,2.0)+24.0*pow(y,2.0)*pow(x,2.0)-52.0*pow(x,4.0)+40.0*pow(x,5.0)+8.0*pow(y,2.0)*pow(x,3.0)+4.0*pow(y,4.0))/pow((x+2.0),3.0);
            d_temp=d_temp+pow(((x-2.0)*(x-2.0)+y*y-0.25),2.0)*pow((2.0-x),2.0)*pow((2.0+x*(x-1.0)*(x-1.0)-y*y),2.0)/((x+2.0)*(x+2.0));
            break;

        default:
            cout<<"case 32: EquationIndex is out of the range!"<<endl;
            cin.get();
            break;
        }
        break;

    case 33:
        switch(EquationIndex)
        {
        case 1:
            d_temp=(2.0+147.0*pow(y,2.0)+169.0*pow(x,2.0)+24.0*pow(x,2.0)*pow(y,2.0)-118.0*x*pow(y,2.0)+8.0*pow(x,3.0)*pow(y,2.0)-171.0*x-28.0*pow(x,3.0)-18.0*pow(x,4.0)+4.0*pow(y,4.0)+4.0*pow(x,5.0))/pow((x+2.0),3.0);
            break;

        case 2:
            d_temp=(2.0+147.0*pow(y,2.0)+169.0*pow(x,2.0)+24.0*pow(x,2.0)*pow(y,2.0)-118.0*x*pow(y,2.0)+8.0*pow(x,3.0)*pow(y,2.0)-171.0*x-28.0*pow(x,3.0)-18.0*pow(x,4.0)+4.0*pow(y,4.0)+4.0*pow(x,5.0))/pow((x+2.0),3.0);
            d_temp=d_temp+(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x-2.0)*(2.0*y*y+1.5-x)/((x+2.0)*(x+2.0));
            break;

        case 3:
            d_temp=(2.0+147.0*pow(y,2.0)+169.0*pow(x,2.0)+24.0*pow(x,2.0)*pow(y,2.0)-118.0*x*pow(y,2.0)+8.0*pow(x,3.0)*pow(y,2.0)-171.0*x-28.0*pow(x,3.0)-18.0*pow(x,4.0)+4.0*pow(y,4.0)+4.0*pow(x,5.0))/pow((x+2.0),3.0);
            d_temp=d_temp+(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x-2.0)*(2.0*y*y+1.5-x)*(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x-2.0)*(2.0*y*y+1.5-x)/((x+2.0)*(x+2.0));
            break;

        case 4:
        {
            double Xc=0.46158;
            double Yc=0.0;
            double Fc=(Yc*Yc-Xc*(Xc-1.0)*(Xc-1.0)-2.0)*(Xc-2.0)*(2.0*Yc*Yc+1.5-Xc);

            double Xp=1.0;
            double Yp=0.0;
            double FXp=(Yp*Yp-Xp*(Xp-1.0)*(Xp-1.0)-2.0)*(Xp-2.0)*(2.0*Yp*Yp+1.5-Xp);

            double F=(y*y-x*(x-1.0)*(x-1.0)-2.0)*(x-2.0)*(2.0*y*y+1.5-x);

            d_temp=(2.0+147.0*pow(y,2.0)+169.0*pow(x,2.0)+24.0*pow(x,2.0)*pow(y,2.0)-118.0*x*pow(y,2.0)+8.0*pow(x,3.0)*pow(y,2.0)-171.0*x-28.0*pow(x,3.0)-18.0*pow(x,4.0)+4.0*pow(y,4.0)+4.0*pow(x,5.0))/pow((x+2.0),3.0);
            d_temp=d_temp+(F-Fc)*(F-Fc)/((FXp-Fc)*(FXp-Fc)*(x+2.0)*(x+2.0));
         }

            break;

        case 42:
            d_temp=-1.0/((x+2.0)*(x+2.0));
            break;

        default:
            cout<<"case 32: EquationIndex is out of the range!"<<endl;
            cin.get();
            break;
        }
        break;

    case 34:
        switch (EquationIndex)
        {
        case 1:
            d_temp=-8.0/(pow(x-2.0,3.0));
            break;
        case 2:
            d_temp=-8.0/(pow(x-2.0,3.0))+(1.0-x*x-y*y)/((x-2.0)*(x-2.0));
            break;
        case 3:
            d_temp=-8.0/(pow(x-2.0,3.0))+((1.0-x*x-y*y)*(1.0-x*x-y*y))/((x-2.0)*(x-2.0));
            break;
        case 4:
            d_temp=-8.0/(pow(x-2.0, 3.0))+((x*x+y*y)*(x*x+y*y))/((x-2.0)*(x-2.0));
            break;
        case 42:
            d_temp=-1.0/((x-2.0)*(x-2.0));
        default:
            break;
        }
        break;

    case 35:
        switch(EquationIndex)
        {
        case 1:
            d_temp=5.0+70.0*y*y+88.0*x*x+24*x*x*y*y-72.0*x*y*y-36.0*x*x*x+4.0*x*x*x*x-73.0*x;
            break;
        }
        break;


    default://Example 4
        cout<<"ExampleIndex is out of the range!"<<endl;
        d_temp=4.0;
        break;
    }

    return d_temp;
}
//==================================

double EquationsRightSideElementValue_PDE_PFEM(double x, double y, unsigned int ExampleIndex, unsigned int EquationIndex)
{
    double d_temp=0.0;

    switch(ExampleIndex)
    {
    case 25:
        switch(EquationIndex)
        {
        case 2:
            d_temp=-1.0/((x+2.0)*(x+2.0));
            break;
        case 3:
            d_temp=-1.0/((x+2.0)*(x+2.0));
            break;

        case 421:
            d_temp=2.0/((x+2.0)*(x+2.0));
            break;

        case 422:
            d_temp=-1.0/((x+2.0)*(x+2.0));
            break;
        default:
            cout<<"EquationsRightSideElementValue_PDE_PFEM::EquationIndex is out of the range!"<<endl;
            cin.get();
            break;
        }
        break;

    case 29:
        switch(EquationIndex)
        {
        //case 1:
        //do not need!
        }
        break;

    case 32:
        switch(EquationIndex)
        {
        //For this example, we just compute Equation 1.
        //case 1:
            //d_temp=-1.0/((x+2.0)*(x+2.0));
            //break;
        case 2:
            d_temp=-1.0/((x+2.0)*(x+2.0));
            break;

        case 3:
            d_temp=-1.0/((x+2.0)*(x+2.0));
            break;
        default:
            cout<<"EquationsRightSideElementValue_PDE_PFEM::case 32: EquationIndex is out of the range!"<<endl;
            cin.get();
            break;
        }
        break;

    case 33:
        switch(EquationIndex)
        {
        case 2:
            d_temp=-1.0/((x+2.0)*(x+2.0));
            break;

        case 3:
            d_temp=-1.0/((x+2.0)*(x+2.0));
            break;

        case 421:
            d_temp=2.0/((x+2.0)*(x+2.0));
            break;
        case 422:
            d_temp=-1.0/((x+2.0)*(x+2.0));
            break;

        default:
            cout<<"EquationsRightSideElementValue_PDE_PFEM::case 33: EquationIndex is out of the range!"<<endl;
            cin.get();
            break;

        }
        break;

    case 34:
        switch(EquationIndex)
        {
        case 2:
            d_temp=-1.0/((x-2.0)*(x-2.0));
            break;

        case 3:
            d_temp=-1.0/((x-2.0)*(x-2.0));
            break;

        case 421:
            d_temp=2.0/((x-2.0)*(x-2.0));
            break;
        case 422:
            d_temp=-1.0/((x-2.0)*(x-2.0));
            break;

        default:
            cout<<"EquationsRightSideElementValue_PDE_PFEM::case 33: EquationIndex is out of the range!"<<endl;
            cin.get();
            break;

        }
        break;

    default:
        cout<<"EquationsRightSideElementValue_PDE_PFEM::ExampleIndex is out of the range!"<<endl;
        cin.get();
        break;
    }

    return d_temp;
}
//========================================



//========================================
/**
 * @brief BMatrixValue
 * @param x
 * @param y
 * @param Bxy
 * @param ExampleIndex
 */
void BMatrixValue(double x, double y, vector<double> &Bxy, unsigned int ExampleIndex)
{
    Bxy.clear();
    //Bxy=[B1,B2];
    switch(ExampleIndex)
    {
    case 1:
        Bxy.push_back(0.0);
        Bxy.push_back(0.0);
        break;
    case 2:
        Bxy.push_back(0.0);
        Bxy.push_back(0.0);
        break;
    case 3:
        Bxy.push_back(0.0);
        Bxy.push_back(0.0);
        break;
    case 4:
        Bxy.push_back(0.0);
        Bxy.push_back(0.0);
        break;
    case 5:
        Bxy.push_back(0.0);
        Bxy.push_back(0.0);
        break;
    case 6:
        Bxy.push_back(0.0);
        Bxy.push_back(0.0);
        break;
    case 7:
        Bxy.push_back(-y);
        Bxy.push_back(x);
        break;
    case 8:
        Bxy.push_back(-y);
        Bxy.push_back(x);
        break;
    case 9:
        Bxy.push_back(0.0);
        Bxy.push_back(0.0);
        break;
    case 10:
        Bxy.push_back(0.0);
        Bxy.push_back(0.0);
        break;
    case 11:
        Bxy.push_back(0.0);
        Bxy.push_back(0.0);
        break;
    case 12:
        Bxy.push_back(0.0);
        Bxy.push_back(0.0);
        break;
    case 13:
        Bxy.push_back(0.0);
        Bxy.push_back(0.0);
        break;
    case 14:
        Bxy.push_back(0.0);
        Bxy.push_back(0.0);
        break;
    case 15:
        Bxy.push_back(0.0);
        Bxy.push_back(0.0);
        break;
    case 16:
        Bxy.push_back(0.0);
        Bxy.push_back(0.0);
        break;
    case 17:
        Bxy.push_back(0.0);
        Bxy.push_back(0.0);
        break;
    case 18:
        Bxy.push_back(0.0);
        Bxy.push_back(0.0);
        break;
    case 19:
        Bxy.push_back(0.0);
        Bxy.push_back(0.0);
        break;
    case 20:
        Bxy.push_back(0.0);
        Bxy.push_back(0.0);
        break;
    case 21:
        Bxy.push_back(0.0);
        Bxy.push_back(0.0);

        break;
    case 22:
        Bxy.push_back(0.0);
        Bxy.push_back(0.0);
        break;
    case 23:
        Bxy.push_back(0.0);
        Bxy.push_back(0.0);
        break;
    case 24:
        Bxy.push_back(0.0);
        Bxy.push_back(0.0);
        break;
    default:
        cout<<"You have to choosen an example:"<<endl;
        Bxy.push_back(0.0);
        Bxy.push_back(0.0);
        break;

    }


}

//=========================================
/**
 * @brief KMatrixValue
 * @param x
 * @param y
 * @param k1
 * @param k2
 * @param Kxy
 * @param ExampleIndex
 */
static void KMatrixValue(double x, double y, double k1, double k2, vector<double> & Kxy, unsigned int ExampleIndex)
{
    //Kxy is the value of the matrix K,
    // which is what we want to get
    Kxy.clear();
    vector<double> Bxy;

    if(ExampleIndex<25)
    {
        BMatrixValue(x,y,Bxy,ExampleIndex);
        //Kxy=[k11,k12,k21,k22]
        Kxy.push_back(k1*Bxy[0]*Bxy[0]+k2*(1-Bxy[0]*Bxy[0]));
        Kxy.push_back((k1-k2)*Bxy[0]*Bxy[1]);
        Kxy.push_back((k1-k2)*Bxy[0]*Bxy[1]);
        Kxy.push_back(k1*Bxy[1]*Bxy[1]+k2*(1.0-Bxy[1]*Bxy[1]));
    }
    else
    {
        switch(ExampleIndex)
        {
        case 25:
            Kxy.push_back(1.0/((x+2.0)*(x+2.0)));
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0/((x+2.0)*(x+2.0)));
            break;
        case 26:
            Kxy.push_back(1.0/((x+2.0)*(x+2.0)));
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0/((x+2.0)*(x+2.0)));
            break;
        case 27://is the same with case 24
            BMatrixValue(x,y,Bxy,24);
            //Kxy=[k11,k12,k21,k22]
            Kxy.push_back(k1*Bxy[0]*Bxy[0]+k2*(1-Bxy[0]*Bxy[0]));
            Kxy.push_back((k1-k2)*Bxy[0]*Bxy[1]);
            Kxy.push_back((k1-k2)*Bxy[0]*Bxy[1]);
            Kxy.push_back(k1*Bxy[1]*Bxy[1]+k2*(1.0-Bxy[1]*Bxy[1]));
            break;
        case 28:
            Kxy.push_back(1.0);
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0);
            break;
        case 29:
            Kxy.push_back(1.0/((x+3.0)*(x+3.0)));
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0/((x+3.0)*(x+3.0)));
            break;

        case 30:
            Kxy.push_back(1.0);
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0);
            break;

        case 31:
            Kxy.push_back(1.0);
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0);
            break;

        case 32:
//            Kxy.push_back(1.0/((x-2.0)*(x-2.0)));
//            Kxy.push_back(0.0);
//            Kxy.push_back(0.0);
//            Kxy.push_back(1.0/((x-2.0)*(x-2.0)));
            Kxy.push_back(1.0/((x+2.0)*(x+2.0)));
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0/((x+2.0)*(x+2.0)));
            break;

        case 33://Modify
            Kxy.push_back(1.0/((x+2.0)*(x+2.0)));
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0/((x+2.0)*(x+2.0)));
            break;

        case 34:
            Kxy.push_back(1.0/((x-2.0)*(x-2.0)));
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0/((x-2.0)*(x-2.0)));
            break;

        case 35:
            Kxy.push_back(1.0);
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0);
            break;

        case 36:
            Kxy.push_back(1.0);
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0);
            break;

        case 37:
            Kxy.push_back(1.0);
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0);
            break;

        case 38:
            Kxy.push_back(1.0);
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0);
            break;

        case 39:
            Kxy.push_back(1.0);
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0);
            break;

        case 40:
            Kxy.push_back(1.0);
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0);
            break;

        case 41:
            Kxy.push_back(1.0);
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0);
            break;

        case 42:
            Kxy.push_back(1.0);
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0);
            break;

        case 43:
            Kxy.push_back(1.0);
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0);
            break;

        case 44:
            Kxy.push_back(1.0);
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0);
            break;

        case 45:
            Kxy.push_back(1.0);
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0);
            break;

        case 46:
            Kxy.push_back(1.0);
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0);
            break;

        case 47:
            Kxy.push_back(1.0);
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0);
            break;

        case 48:
            Kxy.push_back(1.0);
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0);
            break;

        case 49:
            Kxy.push_back(1.0);
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0);
            break;
        case 50:
            Kxy.push_back(1.0);
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0);
            break;
        case 51:
            Kxy.push_back(1.0);
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0);
            break;

        case 52:
            Kxy.push_back(1.0);
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0);
            break;
        case 53:
            Kxy.push_back(1.0);
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0);
            break;
        case 54:
            Kxy.push_back(1.0);
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0);
            break;
        case 55:
            Kxy.push_back(1.0);
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0);
            break;

        case 56:
            Kxy.push_back(1.0);
            Kxy.push_back(0.0);
            Kxy.push_back(0.0);
            Kxy.push_back(1.0);
            break;

            //=================================================================

        default:
            cout<<"FEM_DynamicFunctions::KMatrixValue::The ExampleIndex is out of the range."<<endl;
            cin.get();
            break;

        }
    }



}
//=========================================
//void Modification_LoadVector_CoeffMatrix(double C, const vector<unsigned int>& BasesType, vector<vector<double> >& coeff_matrix, vector<double>& load_vect)
//{
//	cout<<"Modify the load vector:"<<endl;

//	unsigned int matrix_size=BasesType.size();

//	for (int i=0; i<matrix_size; i++)
//	{
//		if (BasesType[i])
//		{
//			if (BasesType[i]==3)
//			{
//				load_vect[i]=C;
//				//modify other load_vect's elements
//				for (unsigned int ii=0; ii<load_vect.size(); ii++)
//				{
//					if (ii<i)
//					{
//						load_vect[ii]=load_vect[ii]-C*coeff_matrix[ii][i-ii];//M[ii][i-ii]
//					}
//					if (ii>i)
//					{
//						load_vect[ii]=load_vect[ii]-C*coeff_matrix[i][ii-i];
//					}
//				}
//			}
//			else
//			{
//				load_vect[i]=0.0;

//			}

//			//Modify Coeff_Matrix==============
//			for (int j=0; j<matrix_size; j++)
//			{
//				coeff_matrix[i][j]=0.0;
//				if (i-j>=0)
//				{

//					coeff_matrix[j][i-j]=0.0;
//				}
//			}
//			coeff_matrix[i][0]=1.0;
//			//==================================

//		}
//	}

//}
static  void Modification_LoadVector_CoeffMatrix(double C, const vector<unsigned int>& BasesType, Mat& coeff_matrix, Vec& load_vect, const map<unsigned int, set<unsigned int> >& HTable)
{
    double t0, t1;
    double ModLoadVecTime=0.0, ModCoeffVecTime=0.0;

    unsigned int matrix_size=BasesType.size();

    unsigned int NumZeroBases=0;

    for (unsigned int i=0; i<matrix_size; i++)
    {
        if (BasesType[i])
        {
            NumZeroBases=NumZeroBases+1;

            //cout<<"Modify the load vector:"<<endl;

            t0=(double)clock();
            if (BasesType[i]==3)
            {

                //modify load_vect's elements
                PetscInt LoadSize;
                VecGetSize(load_vect, &LoadSize);
                for (PetscInt ii = 0; ii < LoadSize; ii ++)
                {
                    PetscScalar v;
                    PetscInt i_temp=i;
                    MatGetValues(coeff_matrix,1,&ii,1,&i_temp,&v);
                    v=-C*v;
                    VecSetValues(load_vect, 1, &ii, &v, ADD_VALUES);
                }

                //load_vect[i]=C;
                PetscScalar Cpetsc = C;
                PetscInt ipetsc = i;
                VecSetValues(load_vect, 1, &ipetsc, &Cpetsc, INSERT_VALUES);
            }
            else
            {
                //load_vect[i]=0.0;
                PetscInt ip=i;
                PetscScalar v=0.0;
                VecSetValues(load_vect, 1, &ip, &v, INSERT_VALUES);

            }

            VecAssemblyBegin(load_vect);
            VecAssemblyEnd(load_vect);

            t1=(double)clock();
            //cout<<"The time costed by modification the load vector = "<<t1-t0<<endl;
            ModLoadVecTime=ModLoadVecTime+(t1-t0)/CLOCKS_PER_SEC;


            //cout<<"Modify the coeff_matrix:"<<endl;

            t0=(double)clock();
            //Modify Coeff_Matrix==============

            //Based on H_Table,Jp will be decided
            unsigned int IndexV = i/4;//the index of vertex such that the i-th basis is associated with it

            map<unsigned int, set<unsigned int> >::const_iterator Imap=HTable.find(IndexV);

            PetscInt Jpsize = 4*((*Imap).second).size();
            PetscInt Jp[Jpsize];
            PetscScalar Vp[Jpsize];


            set<unsigned int>::iterator itHTable;//= HTable[IndexV].begin();

            for(PetscInt jindex=0; jindex<((*Imap).second).size(); jindex++)
            {//4*jindex, 4*jindex+1, 4*jindex+2, 4*jindex+3
                if(jindex==0)
                {
                    itHTable= ((*Imap).second).begin();
                }
                else
                {
                    itHTable++;
                }

                PetscInt VHTable=*itHTable;
                Jp[4*jindex] = 4*VHTable;
                Jp[4*jindex+1] = 4*VHTable+1;
                Jp[4*jindex+2] = 4*VHTable+2;
                Jp[4*jindex+3] = 4*VHTable+3;
                //-------------
                Vp[4*jindex] = 0.0;
                Vp[4*jindex+1] = 0.0;
                Vp[4*jindex+2] = 0.0;
                Vp[4*jindex+3] = 0.0;
            }

            PetscInt Ip = i;
            MatSetValues(coeff_matrix, 1, &Ip, Jpsize, Jp, Vp, INSERT_VALUES);
            MatSetValues(coeff_matrix, Jpsize, Jp, 1, &Ip, Vp,INSERT_VALUES);

            //coeff_matrix[i][i]=1.0;
            PetscScalar vp=1.0;
            MatSetValues(coeff_matrix, 1, &Ip, 1, &Ip, &vp, INSERT_VALUES);

            MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);
            //==================================

            t1=(double)clock();
            //cout<<"The time costed by modification the coeff_matrix = "<<t1-t0<<endl;
            ModCoeffVecTime=ModCoeffVecTime+(t1-t0)/CLOCKS_PER_SEC;

        }
    }

    //cout<<"the number of non-zero bases = "<<NumZeroBases<<endl;
    //cout<<"The time costed by modification the load vector = "<<ModLoadVecTime<<endl;
    //cout<<"The time costed by modification the coeff_matrix = "<<ModCoeffVecTime<<endl;

}

void ParametricMapSigmaVal(unsigned int s_der_order,unsigned int t_der_order, const vector<Coordinate*> &Pcoordinate_Ps,
                           vector<double> &X,vector<double> &Y, unsigned int ExampleIndex, unsigned int SubExampleIndex)
{
    X.clear();
    Y.clear();
    //
    if(s_der_order==0 && t_der_order==0)
    {
        switch(ExampleIndex)
        {
        case 51:
            switch (SubExampleIndex)
            {
            case 1:
                for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                {
                    //
                    double s=Pcoordinate_Ps[i]->xy[0];
                    double t=Pcoordinate_Ps[i]->xy[1];

                    double pi_double=3.141592653589793238;
                    double alpha=2.0;
                    //--------------------------
                    double Vs=s*s*s*cos(alpha*pi_double*t/2.0);
                    double Vt=s*s*s*sin(alpha*pi_double*t/2.0);
                    //--------------------------

                    X.push_back(Vs);
                    Y.push_back(Vt);
                }
                break;
            case 2:
                for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                {
                    //
                    double s=Pcoordinate_Ps[i]->xy[0];
                    double t=Pcoordinate_Ps[i]->xy[1];

                    double pi_double=3.141592653589793238;
                    double alpha=3.0/2.0;
                    //--------------------------
                    double Vs=s*s*s*cos(alpha*pi_double*t/2.0);
                    double Vt=s*s*s*sin(alpha*pi_double*t/2.0);
                    //--------------------------

                    X.push_back(Vs);
                    Y.push_back(Vt);
                }
                break;
            case 3:
                for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                {
                    //
                    double s=Pcoordinate_Ps[i]->xy[0];
                    double t=Pcoordinate_Ps[i]->xy[1];

                    double pi_double=3.141592653589793238;
                    double alpha=2.0;
                    //--------------------------
                    double Vs=s*cos(alpha*pi_double*t/2.0);
                    double Vt=s*sin(alpha*pi_double*t/2.0);
                    //--------------------------

                    X.push_back(Vs);
                    Y.push_back(Vt);
                }
                break;
            case 4:
                for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                {
                    //
                    double s=Pcoordinate_Ps[i]->xy[0];
                    double t=Pcoordinate_Ps[i]->xy[1];

                    double pi_double=3.141592653589793238;
                    double alpha=2.0;
                    double delta=2.5;
                    //--------------------------
                    double Vs=pow(s,delta)*cos(alpha*pi_double*t/2.0);
                    double Vt=pow(s,delta)*sin(alpha*pi_double*t/2.0);
                    //--------------------------

                    X.push_back(Vs);
                    Y.push_back(Vt);
                }
                break;
            case 5:
                for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                {
                    //
                    double s=Pcoordinate_Ps[i]->xy[0];
                    double t=Pcoordinate_Ps[i]->xy[1];

                    double pi_double=3.141592653589793238;
                    double alpha=3.0/2.0;
                    //--------------------------
                    double Vs=s*cos(alpha*pi_double*t/2.0);
                    double Vt=s*sin(alpha*pi_double*t/2.0);
                    //--------------------------

                    X.push_back(Vs);
                    Y.push_back(Vt);
                }
                break;
            case 6:
                for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                {
                    //
                    double s=Pcoordinate_Ps[i]->xy[0];
                    double t=Pcoordinate_Ps[i]->xy[1];

                    double pi_double=3.141592653589793238;
                    double alpha=3.0/2.0;
                    double delta=2.5;
                    //--------------------------
                    double Vs=pow(s,delta)*cos(alpha*pi_double*t/2.0);
                    double Vt=pow(s,delta)*sin(alpha*pi_double*t/2.0);
                    //--------------------------

                    X.push_back(Vs);
                    Y.push_back(Vt);
                }
                break;
            }

            break;
        }
    }
    else
    {
        if(s_der_order==1 && t_der_order==0)
        {
            switch(ExampleIndex)
            {
            case 51:
                switch (SubExampleIndex)
                {
                case 1:
                    for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                    {
                        //
                        double s=Pcoordinate_Ps[i]->xy[0];
                        double t=Pcoordinate_Ps[i]->xy[1];

                        double pi_double=3.141592653589793238;
                        double alpha=2.0;
                        //--------------------------
                        double Vs=3.0*s*s*cos(alpha*pi_double*t/2.0);
                        double Vt=3.0*s*s*sin(alpha*pi_double*t/2.0);
                        //--------------------------

                        X.push_back(Vs);
                        Y.push_back(Vt);
                    }
                    break;
                case 2:
                    for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                    {
                        //
                        double s=Pcoordinate_Ps[i]->xy[0];
                        double t=Pcoordinate_Ps[i]->xy[1];

                        double pi_double=3.141592653589793238;
                        double alpha=3.0/2.0;
                        //--------------------------
                        double Vs=3.0*s*s*cos(alpha*pi_double*t/2.0);
                        double Vt=3.0*s*s*sin(alpha*pi_double*t/2.0);
                        //--------------------------

                        X.push_back(Vs);
                        Y.push_back(Vt);
                    }
                    break;
                case 3:
                    for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                    {
                        //
                        double s=Pcoordinate_Ps[i]->xy[0];
                        double t=Pcoordinate_Ps[i]->xy[1];

                        double pi_double=3.141592653589793238;
                        double alpha=2.0;
                        //--------------------------
                        double Vs=cos(alpha*pi_double*t/2.0);
                        double Vt=sin(alpha*pi_double*t/2.0);
                        //--------------------------

                        X.push_back(Vs);
                        Y.push_back(Vt);
                    }
                    break;

                case 4:
                    for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                    {
                        //
                        double s=Pcoordinate_Ps[i]->xy[0];
                        double t=Pcoordinate_Ps[i]->xy[1];

                        double pi_double=3.141592653589793238;
                        double alpha=2.0;
                        double delta=2.5;
                        //--------------------------
                        double Vs=delta*pow(s,delta-1.0)*cos(alpha*pi_double*t/2.0);
                        double Vt=delta*pow(s,delta-1.0)*sin(alpha*pi_double*t/2.0);
                        //--------------------------

                        X.push_back(Vs);
                        Y.push_back(Vt);
                    }
                    break;
                case 5:
                    for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                    {
                        //
                        double s=Pcoordinate_Ps[i]->xy[0];
                        double t=Pcoordinate_Ps[i]->xy[1];

                        double pi_double=3.141592653589793238;
                        double alpha=3.0/2.0;
                        //--------------------------
                        double Vs=cos(alpha*pi_double*t/2.0);
                        double Vt=sin(alpha*pi_double*t/2.0);
                        //--------------------------

                        X.push_back(Vs);
                        Y.push_back(Vt);
                    }
                    break;
                case 6:
                    for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                    {
                        //
                        double s=Pcoordinate_Ps[i]->xy[0];
                        double t=Pcoordinate_Ps[i]->xy[1];

                        double pi_double=3.141592653589793238;
                        double alpha=3.0/2.0;
                        double delta=2.5;
                        //--------------------------
                        double Vs=delta*pow(s,delta-1.0)*cos(alpha*pi_double*t/2.0);
                        double Vt=delta*pow(s,delta-1.0)*sin(alpha*pi_double*t/2.0);
                        //--------------------------

                        X.push_back(Vs);
                        Y.push_back(Vt);
                    }
                    break;

                }


                break;
            }
        }
        else
        {
            if(s_der_order==0 && t_der_order==1)
            {

                switch(ExampleIndex)
                {
                case 51:
                    switch (SubExampleIndex)
                    {
                    case 1:
                        for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                        {
                            //
                            double s=Pcoordinate_Ps[i]->xy[0];
                            double t=Pcoordinate_Ps[i]->xy[1];

                            double pi_double=3.141592653589793238;
                            double alpha=2.0;
                            //--------------------------
                            double Vs=-s*s*s*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            double Vt=s*s*s*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            //--------------------------

                            X.push_back(Vs);
                            Y.push_back(Vt);
                        }
                        break;

                    case 2:
                        for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                        {
                            //
                            double s=Pcoordinate_Ps[i]->xy[0];
                            double t=Pcoordinate_Ps[i]->xy[1];

                            double pi_double=3.141592653589793238;
                            double alpha=3.0/2.0;
                            //--------------------------
                            double Vs=-s*s*s*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            double Vt=s*s*s*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            //--------------------------

                            X.push_back(Vs);
                            Y.push_back(Vt);
                        }
                        break;
                    case 3:
                        for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                        {
                            //
                            double s=Pcoordinate_Ps[i]->xy[0];
                            double t=Pcoordinate_Ps[i]->xy[1];

                            double pi_double=3.141592653589793238;
                            double alpha=2.0;
                            //--------------------------
                            double Vs=-s*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            double Vt=s*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            //--------------------------

                            X.push_back(Vs);
                            Y.push_back(Vt);
                        }
                        break;
                    case 4:
                        for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                        {
                            //
                            double s=Pcoordinate_Ps[i]->xy[0];
                            double t=Pcoordinate_Ps[i]->xy[1];

                            double pi_double=3.141592653589793238;
                            double alpha=2.0;
                            double delta=2.5;
                            //--------------------------
                            double Vs=-pow(s,delta)*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            double Vt=pow(s,delta)*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            //--------------------------

                            X.push_back(Vs);
                            Y.push_back(Vt);
                        }
                        break;
                    case 5:
                        for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                        {
                            //
                            double s=Pcoordinate_Ps[i]->xy[0];
                            double t=Pcoordinate_Ps[i]->xy[1];

                            double pi_double=3.141592653589793238;
                            double alpha=3.0/2.0;
                            //--------------------------
                            double Vs=-s*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            double Vt=s*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            //--------------------------

                            X.push_back(Vs);
                            Y.push_back(Vt);
                        }
                        break;
                    case 6:
                        for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                        {
                            //
                            double s=Pcoordinate_Ps[i]->xy[0];
                            double t=Pcoordinate_Ps[i]->xy[1];

                            double pi_double=3.141592653589793238;
                            double alpha=3.0/2.0;
                            double delta=2.5;
                            //--------------------------
                            double Vs=-pow(s,delta)*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            double Vt=pow(s,delta)*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                            //--------------------------

                            X.push_back(Vs);
                            Y.push_back(Vt);
                        }
                        break;

                    }

                    break;
                }

            }
            else
            {
                if(s_der_order==1 && t_der_order==1)
                {
                    switch(ExampleIndex)
                    {
                    case 51:
                        switch(SubExampleIndex)
                        {
                        case 1:
                            for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                            {
                                //
                                double s=Pcoordinate_Ps[i]->xy[0];
                                double t=Pcoordinate_Ps[i]->xy[1];

                                double pi_double=3.141592653589793238;
                                double alpha=2.0;
                                //--------------------------
                                double Vs=-3.0*s*s*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                                double Vt=3.0*s*s*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                                //--------------------------

                                X.push_back(Vs);
                                Y.push_back(Vt);
                            }
                            break;
                        case 2:
                            for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                            {
                                //
                                double s=Pcoordinate_Ps[i]->xy[0];
                                double t=Pcoordinate_Ps[i]->xy[1];

                                double pi_double=3.141592653589793238;
                                double alpha=3.0/2.0;
                                //--------------------------
                                double Vs=-3.0*s*s*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                                double Vt=3.0*s*s*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                                //--------------------------

                                X.push_back(Vs);
                                Y.push_back(Vt);
                            }
                            break;

                        case 3:
                            for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                            {
                                //
                                double s=Pcoordinate_Ps[i]->xy[0];
                                double t=Pcoordinate_Ps[i]->xy[1];

                                double pi_double=3.141592653589793238;
                                double alpha=2.0;
                                //--------------------------
                                double Vs=-sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                                double Vt=cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                                //--------------------------

                                X.push_back(Vs);
                                Y.push_back(Vt);
                            }
                            break;
                        case 4:
                            for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                            {
                                //
                                double s=Pcoordinate_Ps[i]->xy[0];
                                double t=Pcoordinate_Ps[i]->xy[1];

                                double pi_double=3.141592653589793238;
                                double alpha=2.0;
                                double delta=2.5;
                                //--------------------------
                                double Vs=-delta*pow(s,delta-1.0)*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                                double Vt=delta*pow(s,delta-1.0)*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                                //--------------------------

                                X.push_back(Vs);
                                Y.push_back(Vt);
                            }
                            break;
                        case 5:
                            for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                            {
                                //
                                double s=Pcoordinate_Ps[i]->xy[0];
                                double t=Pcoordinate_Ps[i]->xy[1];

                                double pi_double=3.141592653589793238;
                                double alpha=3.0/2.0;
                                //--------------------------
                                double Vs=-sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                                double Vt=cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                                //--------------------------

                                X.push_back(Vs);
                                Y.push_back(Vt);
                            }
                            break;
                        case 6:
                            for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
                            {
                                //
                                double s=Pcoordinate_Ps[i]->xy[0];
                                double t=Pcoordinate_Ps[i]->xy[1];

                                double pi_double=3.141592653589793238;
                                double alpha=3.0/2.0;
                                double delta=2.5;
                                //--------------------------
                                double Vs=-delta*pow(s,delta-1.0)*sin(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                                double Vt=delta*pow(s,delta-1.0)*cos(alpha*pi_double*t/2.0)*(alpha*pi_double/2.0);
                                //--------------------------

                                X.push_back(Vs);
                                Y.push_back(Vt);
                            }
                            break;

                        }

                        break;
                    }

                }
                else
                {
                    cout<<"ParametricMapSigmaVal::s_der_order or t_der_order is out of range!"<<endl;
                    cin.get();
                }
            }
        }
    }

}
//========================================================================
#endif
