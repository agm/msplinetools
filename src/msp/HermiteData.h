#ifndef HERMITEDATA_H
#define HERMITEDATA_H

#include <vector>
using namespace std;
class HermiteData
{
public:
    /**
     * @brief ItsHermiteData=[fvalue, fx, fy, fxy];
     */
    vector<double> ItsHermiteData;

    /**
     * @brief Index_InitialCell: for the local frame
     */
    unsigned int Index_InitialCell;

public:
    HermiteData(void);
    ~HermiteData(void);
    HermiteData(double d, unsigned int Index);
    HermiteData(const HermiteData & HD);

public:
	void Cout_HermiteData();
};

#endif
