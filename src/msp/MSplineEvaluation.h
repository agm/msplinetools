#ifndef MSPLINEEVALUATION_H
#define MSPLINEEVALUATION_H

#include <vector>
#include <iostream>
using namespace std;

#include "HermiteCell.h"
#include "Coordinate.h"
#include "Mesh.h"
#include "Cell.h"
#include "HermiteData.h"


//=========Compute based on Hermite datas directly================================================================
static void  HermiteBaseValuesDer0(double si, vector<double> &HVi)//HVi=[H_0(si), H_1(si), H_2(si), H_3(si)]
{
        HVi.clear();
        //-------------------------------
        if ((si<=1) && (si>=0))
        {
                HVi.push_back((1-si)*(1-si)*(1+2*si));//(1-s)*(1-s)*(1+2s)
                HVi.push_back((1-si)*(1-si)*si);//(1-s)*(1-s)*s
                HVi.push_back(-1.0*(1-si)*si*si);//-(1-s)*s*s
                HVi.push_back(si*si*(3-2*si));//s*s*(3-2s)
        }
        else
        {
            cout<<"HermiteBaseValuesDer0:00000000000"<<endl;
                HVi.push_back(0.0);
                HVi.push_back(0.0);
                HVi.push_back(0.0);
                HVi.push_back(0.0);
        }

}
//---
static void HermiteBaseValuesDer1(double a, double b, double si, vector<double> &HVi)//HVi=[dH_0(si)/ds,dH_1(si)/ds,dH_2(si)/ds,dH_3(si)/ds]
{
        HVi.clear();
        //-----------------------------
        if ((si<=1)&&(si>=0))
        {
                double scale=1.0/(b-a);
                HVi.push_back(scale*(-6.0*si*(1.0-si)));//-6s(1-s)
                HVi.push_back(scale*(1.0-si)*(1.0-3*si));//(1-s)(1-3s)
                HVi.push_back(scale*si*(3.0*si-2.0));//s(3s-2)
                HVi.push_back(scale*6.0*si*(1.0-si));//6s(1-s)
        }
        else
        {
            cout<<"HermiteBaseValuesDer1::HVi00000000000"<<endl;
                HVi.push_back(0.0);
                HVi.push_back(0.0);
                HVi.push_back(0.0);
                HVi.push_back(0.0);
        }

}

static void HermiteBaseValuesDer2(double a, double b, double si, vector<double> &HVi)//HVi=[dH_0(si)/ds,dH_1(si)/ds,dH_2(si)/ds,dH_3(si)/ds]
{
        HVi.clear();
        //-----------------------------
        if ((si<=1)&&(si>=0))
        {
                double scale=1.0/((b-a)*(b-a));
                HVi.push_back(scale*(12.0*si-6.0));//12s-6
                HVi.push_back(scale*(6.0*si-4.0));//6s-4
                HVi.push_back(scale*(-2.0+6.0*si));//-2+6s
                HVi.push_back(scale*(6.0-12.0*si));//6-12s
        }
        else
        {
            cout<<"HermiteBaseValuesDer2:HVi000000000000"<<endl;
                HVi.push_back(0.0);
                HVi.push_back(0.0);
                HVi.push_back(0.0);
                HVi.push_back(0.0);
        }

}
static void HermiteBaseValuesDer3(double a, double b, double si, vector<double> &HVi)//HVi=[dH_0(si)/ds,dH_1(si)/ds,dH_2(si)/ds,dH_3(si)/ds]
{
        HVi.clear();
        //-----------------------------
        if ((si<=1)&&(si>=0))
        {
                double scale=1.0/((b-a)*(b-a)*(b-a));
                HVi.push_back(scale*(12.0));//12
                HVi.push_back(scale*(6.0));//6
                HVi.push_back(scale*(6.0));//6
                HVi.push_back(scale*(-12.0));//-12
        }
        else
        {
            cout<<"HermiteBaseValuesDer3:HVi000000000000"<<endl;
                HVi.push_back(0.0);
                HVi.push_back(0.0);
                HVi.push_back(0.0);
                HVi.push_back(0.0);
        }

}
// However, if DerOrder=1, then dx/du=(1/(b-a))*dx/ds
static void HermiteBaseValues(double a, double b, int DerOrder,const vector<double> &s, vector<vector<double> > &HValues)
{
    //cout<<"HermiteBasesValues()"<<endl;
        HValues.clear();
        vector<double> HVi;
        for (unsigned int i=0; i<s.size(); i++)
        {
                HVi.clear();
                if(DerOrder==0)
                {
                        HermiteBaseValuesDer0(s[i],HVi);
                }
                else
                {
                        if (DerOrder==1)
                        {
                                HermiteBaseValuesDer1(a,b,s[i],HVi);
                        }
                        else
                        {
                                if (DerOrder==2)
                                {
                                        HermiteBaseValuesDer2(a,b,s[i],HVi);
                                }
                                else
                                {
                                    if(DerOrder==3)
                                    {
                                        HermiteBaseValuesDer3(a,b,s[i],HVi);
                                    }
                                    else
                                    {
                                        cout<<"BSpline.h::HermiteBaseValues:: the order of derivative must be 0, 1 or 2!"<<endl;
                                        //system("PAUSE");
                                        cin.get();
                                    }

                                }

                        }

                }
                HValues.push_back(HVi);

        }

}

static void value_Cell_Hermite(int DerOrderu, int DerOrderv, const vector<Coordinate*> & P_coordinate_Ps, const HermiteCell * P_HC, vector<double> &Values, const Mesh &CurrentMesh)
{
        Values.clear();
        //--------CellSize---------------------
        unsigned int IndexCell=P_HC->Index_Cell;
    double a=CurrentMesh.P_Cells->at(IndexCell).CellSize[0];
    double b=CurrentMesh.P_Cells->at(IndexCell).CellSize[1];
    double c=CurrentMesh.P_Cells->at(IndexCell).CellSize[2];
    double d=CurrentMesh.P_Cells->at(IndexCell).CellSize[3];
        //-------------------------------------

        //------Generate H Matrix
        vector<vector<double> > H;
        //----Hermite datas of this HermiteCell
        double f01=P_HC->P_HermiteAtCornerVertex[0]->ItsHermiteData[0];
        double f11=P_HC->P_HermiteAtCornerVertex[0]->ItsHermiteData[1];
        double f21=P_HC->P_HermiteAtCornerVertex[0]->ItsHermiteData[2];
        double f31=P_HC->P_HermiteAtCornerVertex[0]->ItsHermiteData[3];

        double f02=P_HC->P_HermiteAtCornerVertex[1]->ItsHermiteData[0];
        double f12=P_HC->P_HermiteAtCornerVertex[1]->ItsHermiteData[1];
        double f22=P_HC->P_HermiteAtCornerVertex[1]->ItsHermiteData[2];
        double f32=P_HC->P_HermiteAtCornerVertex[1]->ItsHermiteData[3];

        double f03=P_HC->P_HermiteAtCornerVertex[2]->ItsHermiteData[0];
        double f13=P_HC->P_HermiteAtCornerVertex[2]->ItsHermiteData[1];
        double f23=P_HC->P_HermiteAtCornerVertex[2]->ItsHermiteData[2];
        double f33=P_HC->P_HermiteAtCornerVertex[2]->ItsHermiteData[3];

        double f04=P_HC->P_HermiteAtCornerVertex[3]->ItsHermiteData[0];
        double f14=P_HC->P_HermiteAtCornerVertex[3]->ItsHermiteData[1];
        double f24=P_HC->P_HermiteAtCornerVertex[3]->ItsHermiteData[2];
        double f34=P_HC->P_HermiteAtCornerVertex[3]->ItsHermiteData[3];
        //----H-----
        vector<double> Hi;
        Hi.push_back(f01);
        Hi.push_back((d-c)*f21);
        Hi.push_back((d-c)*f24);
        Hi.push_back(f04);
        H.push_back(Hi);

        Hi.clear();
        Hi.push_back((b-a)*f11);
        Hi.push_back((b-a)*(d-c)*f31);
        Hi.push_back((b-a)*(d-c)*f34);
        Hi.push_back((b-a)*f14);
        H.push_back(Hi);

        Hi.clear();
        Hi.push_back((b-a)*f12);
        Hi.push_back((b-a)*(d-c)*f32);
        Hi.push_back((b-a)*(d-c)*f33);
        Hi.push_back((b-a)*f13);
        H.push_back(Hi);

        Hi.clear();
        Hi.push_back(f02);
        Hi.push_back((d-c)*f22);
        Hi.push_back((d-c)*f23);
        Hi.push_back(f03);
        H.push_back(Hi);

        //Generate [H_0(x), H_1(x), H_2(x), H_3(x)] for all the points, where H_i(x) is one of the Hermite bases.
        vector<double> s, t;
        for (unsigned int i=0; i<P_coordinate_Ps.size(); i++)
        {
                double u=P_coordinate_Ps[i]->xy[0];
                double v=P_coordinate_Ps[i]->xy[1];
                s.push_back((u-a)/(b-a));
                t.push_back((v-c)/(d-c));
        };

        vector<vector<double> > HValues_S, HValues_T;

        HermiteBaseValues(a, b, DerOrderu, s, HValues_S);
        HermiteBaseValues(c, d, DerOrderv, t, HValues_T);
        //-------------------------------------

        for (unsigned int index=0; index < HValues_S.size(); index++)
        {//HValues_S[index]*H*(HValues_T[index])^T
                double TemValue=0.0;
                for(unsigned int i=0; i<4; i++)
                {
                        for(unsigned int j=0; j<4; j++)
                        {
                                TemValue=TemValue+HValues_S[index][i]*H[i][j]*HValues_T[index][j];

                        };
                };
                Values.push_back(TemValue);
        };


}

//====
//N1=N1+Ni
static bool VectorSum(vector<double> &N1, const vector<double> &Ni)
{
    bool Key=(N1.size()==Ni.size());
    if(Key)
    {
        for(unsigned int i=0; i<Ni.size(); i++)
        {
            N1[i]=N1[i]+Ni[i];
        }
    }
    else
    {
        cout<<"MSplineEvaluation::VectorSum::N1.size()!= N2.size()"<<endl;
    }
    return Key;
}

//Ni = N1 X N2
static bool VectorCrossProduct(vector<double> &Ni,const vector<double> &N1, const vector<double> &N2)
{
    bool Key=((N1.size()==3)&&(N2.size()==3));

    if(Key)
    {
        Ni.clear();

        double x1=N1[0];
        double y1=N1[1];
        double z1=N1[2];

        double x2=N2[0];
        double y2=N2[1];
        double z2=N2[2];

        Ni.push_back(y1*z2-y2*z1);
        Ni.push_back(-x1*z2+x2*z1);
        Ni.push_back(x1*y2-x2*y1);
    }
    else
    {
        cout<<"MSplineEvaluation::VectorCrossProduct:: N1.size!=3 or N2.size!=3"<<endl;
    }

    return Key;
}

//DotProduct=N1*N2
static double VectorDotProduct(const vector<double> &N1, const vector<double> &N2)
{
    double dotproduct=0;
    for(unsigned int i=0; i<N1.size(); i++)
    {
        dotproduct=dotproduct+N1[i]*N2[i];
    }
    return dotproduct;
}

static void VectorScalarProduct(const double & lambda, vector<double> & NVector)
{

    for(unsigned int i=0; i<NVector.size(); i++)
    {
        NVector[i]=lambda*NVector[i];
    }
}

static double VectorMixProduct(const vector<double> & N1, const vector<double> &N2, const vector<double> &N3)
{
    double x1=N1[0];
    double y1=N1[1];
    double z1=N1[2];
    double x2=N2[0];
    double y2=N2[1];
    double z2=N2[2];
    double x3=N3[0];
    double y3=N3[1];
    double z3=N3[2];

    double mixPro=x3*(y1*z2-z1*y2)-y3*(x1*z2-x2*z1)+z3*(x1*y2-x2*y1);

    return mixPro;
}

#endif
