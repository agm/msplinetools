#ifndef POINTSET_H
#define POINTSET_H

#include <vector>
#include "Coordinate.h"
using namespace std;
//----------------
//class Coordinate;
//----------------

class PointSet
{
public:
	unsigned int Index_ItsCell;
	vector<Coordinate*> P_coordinates_Ps;
public:
    PointSet(const PointSet & PS);
    PointSet(void);
    ~PointSet(void);
    PointSet(unsigned int index_cell, const vector<Coordinate*> &Pcoordinate_Ps);

    void DeleteClear();
};

#endif //POINTSET_H
