#include <vector>
#include <iostream>
using namespace std;

#include "MSplineFunctionCell.h"
#include "HermiteCell.h"
#include "PointSet.h"
#include "MSplineEvaluation.h"
#include "BasesCell.h"
#include "Basis.h"

MSplineFunctionCell::MSplineFunctionCell(void)
{
    P_MSplineCell= new HermiteCell();
}

MSplineFunctionCell::~MSplineFunctionCell(void)
{
}

void MSplineFunctionCell::Evaluation(int DerOrderu, int DerOrderv, const PointSet & Pset, vector<double> &Values, const Mesh & CurrentMesh)
{
    //cout<<"MSplineFunctionCell::Evaluation()"<<endl;
	Values.clear();
	unsigned int IndexCell=P_MSplineCell->Index_Cell;
    //cout<<"IndexCell="<<IndexCell<<endl;
    //cout<<"Pset.Index_ItsCell="<<Pset.Index_ItsCell<<endl;

	if (IndexCell==Pset.Index_ItsCell)
	{
		//static void value_Cell_Hermite(int DerOrderu, int DerOrderv, const vector<Coordinate> & coordinate_Ps, const HermiteCell * P_HC, vector<double> &Values)
		value_Cell_Hermite(DerOrderu, DerOrderv, Pset.P_coordinates_Ps, P_MSplineCell, Values, CurrentMesh);
	}
	else
	{
        cout<<"MSplineFunctionCell::Evaluation()::0000000000"<<endl;
		Values.resize(Pset.P_coordinates_Ps.size(),0);
	}

}

MSplineFunctionCell::MSplineFunctionCell(double coeff, const MSplineFunctionCell * P_MSpCell)
{
    P_MSplineCell->P_HermiteAtCornerVertex.clear();

    for (unsigned int i=0; i<4; i++)
    {
        HermiteData * P_HD = new HermiteData();
        P_HD->Index_InitialCell=P_MSpCell->P_MSplineCell->P_HermiteAtCornerVertex[i]->Index_InitialCell;
        P_HD->ItsHermiteData[0]=coeff * (P_MSpCell->P_MSplineCell->P_HermiteAtCornerVertex[i]->ItsHermiteData[0]);
        P_HD->ItsHermiteData[1]=coeff * (P_MSpCell->P_MSplineCell->P_HermiteAtCornerVertex[i]->ItsHermiteData[1]);
        P_HD->ItsHermiteData[2]=coeff * (P_MSpCell->P_MSplineCell->P_HermiteAtCornerVertex[i]->ItsHermiteData[2]);
        P_HD->ItsHermiteData[3]=coeff * (P_MSpCell->P_MSplineCell->P_HermiteAtCornerVertex[i]->ItsHermiteData[3]);
        P_MSplineCell->P_HermiteAtCornerVertex.push_back(P_HD);
    }
}

//----overload
//multiply scalar
MSplineFunctionCell MSplineFunctionCell::Lambda(const double coeff)
{
    MSplineFunctionCell SpC;

    SpC.P_MSplineCell->ResetHermiteCell(P_MSplineCell->Lambda(coeff));

    return SpC;
};
//+
MSplineFunctionCell MSplineFunctionCell::operator+(const MSplineFunctionCell &right)
{
    MSplineFunctionCell SpC;
    SpC.P_MSplineCell->ResetHermiteCell(*(this->P_MSplineCell)+*(right.P_MSplineCell));
    return SpC;
};
//-
MSplineFunctionCell MSplineFunctionCell::operator-(const MSplineFunctionCell &right)
{
    MSplineFunctionCell SpC;
    SpC.P_MSplineCell->ResetHermiteCell((*(this->P_MSplineCell)-*(right.P_MSplineCell)));
    return SpC;
};
//=
MSplineFunctionCell& MSplineFunctionCell::operator=(const MSplineFunctionCell &right)
{
    this->P_MSplineCell->ResetHermiteCell(*(right.P_MSplineCell));
	return *this;
};

MSplineFunctionCell::MSplineFunctionCell(unsigned int index_InitialCell, unsigned int index_Cell)
{
      P_MSplineCell= new HermiteCell(index_InitialCell, index_Cell);
}

//========================================================================
MSplineFunctionCell::MSplineFunctionCell(const vector<double> & Coeff, unsigned int Index_Cell, const Mesh & CurrentMesh)
{

    P_MSplineCell=new HermiteCell(CurrentMesh.P_Cells->at(Index_Cell).Index_InitialCell, Index_Cell);
    //HermiteCell HC(CurrentMesh.P_Cells->at(Index_Cell).Index_InitialCell, Index_Cell);

	BasesCell * P_BC=new BasesCell(Index_Cell, CurrentMesh);
	for (unsigned int i=0; i<P_BC->P_Bases_Cell.size(); i++)
	{
        HermiteCell HC0(*(P_BC->P_Bases_Cell[i]->P_HermiteData_Cells[0]));

//        cout<<"HC0::"<<endl;
//        HC0.Cout_HermiteCell();

//        cout<<"Index_basis = "<<P_BC->P_Bases_Cell[i]->Index_basis<<endl;

//        cout<<"HC0.Lambda(?)"<<endl;
//        cout<<"Lambda = "<<Coeff[P_BC->P_Bases_Cell[i]->Index_basis]<<endl;
//        cin.get();
//        cin.get();

//        (HC0.Lambda(Coeff[P_BC->P_Bases_Cell[i]->Index_basis])).Cout_HermiteCell();
//        cout<<"Before: P_MSplineCell"<<endl;
//        P_MSplineCell->Cout_HermiteCell();
// 		
         P_MSplineCell->ResetHermiteCell((*P_MSplineCell)+(HC0.Lambda(Coeff[P_BC->P_Bases_Cell[i]->Index_basis])));

//        cout<<"After: P_MSplineCell"<<endl;
//        P_MSplineCell->Cout_HermiteCell();

	}

}

void MSplineFunctionCell::Cout_MSplineFunctionCell()
{
	P_MSplineCell->Cout_HermiteCell();
}

void MSplineFunctionCell::BSplineCoeffAxelOrder(double* CellSize, vector<double> &Coeffi)
{
    Coeffi.clear();
    //---
    double x0=CellSize[0];
    double x1=CellSize[1];
    double y0=CellSize[2];
    double y1=CellSize[3];
    //---
    vector<vector<double> > VerticesHermite;
    vector<double> VertexHermite;
    for(unsigned int i=0; i<this->P_MSplineCell->P_HermiteAtCornerVertex.size(); i++)
    {
        VertexHermite.clear();
        //
        VertexHermite.push_back(this->P_MSplineCell->P_HermiteAtCornerVertex.at(i)->ItsHermiteData[0]);
        VertexHermite.push_back(this->P_MSplineCell->P_HermiteAtCornerVertex.at(i)->ItsHermiteData[1]*(x1-x0));
        VertexHermite.push_back(this->P_MSplineCell->P_HermiteAtCornerVertex.at(i)->ItsHermiteData[2]*(y1-y0));
        VertexHermite.push_back(this->P_MSplineCell->P_HermiteAtCornerVertex.at(i)->ItsHermiteData[3]*((x1-x0)*(y1-y0)));
        //
        VerticesHermite.push_back(VertexHermite);
    }
    //
    Coeffi.push_back(VerticesHermite[3][0]);
    Coeffi.push_back(VerticesHermite[3][0]+(1.0/3.0)*VerticesHermite[3][1]);
    Coeffi.push_back(VerticesHermite[2][0]-(1.0/3.0)*VerticesHermite[2][1]);
    Coeffi.push_back(VerticesHermite[2][0]);
    //
    Coeffi.push_back(VerticesHermite[3][0]-(1.0/3.0)*VerticesHermite[3][2]);
    Coeffi.push_back(VerticesHermite[3][0]+(1.0/3.0)*VerticesHermite[3][1]-(1.0/3.0)*VerticesHermite[3][2]-(1.0/9.0)*VerticesHermite[3][3]);
    Coeffi.push_back(VerticesHermite[2][0]-(1.0/3.0)*VerticesHermite[2][1]-(1.0/3.0)*VerticesHermite[2][2]+(1.0/9.0)*VerticesHermite[2][3]);
    Coeffi.push_back(VerticesHermite[2][0]-(1.0/3.0)*VerticesHermite[2][2]);
    //
    Coeffi.push_back(VerticesHermite[0][0]+(1.0/3.0)*VerticesHermite[0][2]);
    Coeffi.push_back(VerticesHermite[0][0]+(1.0/3.0)*VerticesHermite[0][1]+(1.0/3.0)*VerticesHermite[0][2]+(1.0/9.0)*VerticesHermite[0][3]);
    Coeffi.push_back(VerticesHermite[1][0]-(1.0/3.0)*VerticesHermite[1][1]+(1.0/3.0)*VerticesHermite[1][2]-(1.0/9.0)*VerticesHermite[1][3]);
    Coeffi.push_back(VerticesHermite[1][0]+(1.0/3.0)*VerticesHermite[1][2]);
    //
    Coeffi.push_back(VerticesHermite[0][0]);
    Coeffi.push_back(VerticesHermite[0][0]+(1.0/3.0)*VerticesHermite[0][1]);
    Coeffi.push_back(VerticesHermite[1][0]-(1.0/3.0)*VerticesHermite[1][1]);
    Coeffi.push_back(VerticesHermite[1][0]);

}















