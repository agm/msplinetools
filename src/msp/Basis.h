#ifndef BASIS_H
#define BASIS_H

#include <vector>
using namespace std;
//------------------
class HermiteCell;
class Mesh;
class Coordinate;
//------------------


class basis
{
public:
	unsigned int Index_basis;
	vector<HermiteCell *> P_HermiteData_Cells;

public://Initialize
	basis(void);
	~basis(void);

    /**
     * @brief basis: Generated the (4*index_vertex+i_H)-th basis function. it must be the basis function at the index_vertex-th vertex
     * @param index_vertex
     * @param i_H
     * @param CurrentMesh
     * @param Hermitedata
     */
	basis(unsigned int index_vertex, unsigned int i_H, const Mesh & CurrentMesh, const vector<double> & Hermitedata);

    /**
     * @brief basis: Generated the Generated the (4*index_vertex+i_H)-th basis function over the index_cell-th Cell
     * @param index_vertex
     * @param i_H
     * @param index_Cell
     * @param CurrentMesh
     * @param Hermitedata
     */
	basis(unsigned int index_vertex, unsigned int i_H, unsigned int index_Cell, const Mesh & CurrentMesh, const vector<double> & Hermitedata);

public:
	void Cout_basis(const Mesh & CurrentMesh);
	void Evaluation(int DerOrderu, int DerOrderv, const vector<Coordinate*> & Pcoordinate_Ps, unsigned int Index_Cell, vector<double> &Values, const Mesh &CurrentMesh);

    void Evaluation(const unsigned int HermiteCellIndex, int DerOrderu, int DerOrderv, const vector<double> & coordinateST, double &Values, const double * PCellSize);
};

#endif //BASIS_H
