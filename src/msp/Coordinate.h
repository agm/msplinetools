#ifndef COORDINATE_H
#define COORDINATE_H

class Coordinate
{
public:
    /**
     * @brief xy[2]=(x,y) is used to describe coordinate
     */
    double xy[2];//(xy[0], xy[1])
public:
	Coordinate(void);
	~Coordinate(void);

    /**
     * @brief Coordinate: generate a coordinate with (x, y) as its coordinate
     * @param x
     * @param y
     */
    Coordinate(const double x, const double y);

    /**
       * @brief Coordinate: generate a Coordinate by copying the const Coordinate Coor
       * @param const Coordinate& Coor
    */
    Coordinate(const Coordinate& Coor)
    {
        xy[0]=Coor.xy[0];
        xy[1]=Coor.xy[1];
    }
};

#endif
