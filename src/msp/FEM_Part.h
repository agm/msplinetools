#ifndef FEM_PART_H
#define FEM_PART_H

#include "msp/FEM_Functions.h"
#include "msp/FEM_CoeffMatrixLoadVector.h"
#include "msp/FEM_ErrorFunctions.h"

//==============================================================================
void FEMPart_RefinedParametricMap(ofstream &OfTime, Mesh* P_ParametricMesh, MSplineFunctions * P_ParametricMap,
                                  unsigned int k1, unsigned int k2, unsigned int ExampleIndex, unsigned int k,
                                  vector<vector<double> > &CoeffRefinedParametricMap)
{

    Vec load_vect, m_solut;
    Mat coeff_matrix;
    KSPConvergedReason reason;

    map<unsigned int, unsigned int> OrigialVertexToCurrentVertex;

    Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(0, OrigialVertexToCurrentVertex);

    cout<<"Generate Gauss points and Weights"<<endl;

    OfTime<<endl;
    OfTime<<"Generate Guass points and Weights"<<endl;
    double t0=(double)clock();

    //Preparation: GuassPoints
    cout<<"Please input the number of Guass Points:"<<endl;
    unsigned int Gp;
    cin>>Gp;
    unsigned int u_gaussnum=Gp, v_gaussnum=Gp;
    unsigned int load_gaussnum=5;
    vector<double> gausspt, m_IntePt, m_loadw;
    //Weights/////////
    double **w;
     w=GaussPoints_Weights(u_gaussnum, load_gaussnum, gausspt, m_IntePt, m_loadw);
    double t1=(double)clock();
    cout<<"\"Generate Guass points and Weights\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<endl;

    OfTime<<"\"Generate Guass points and Weights\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<endl;
    OfTime<<"u_gaussnum="<<u_gaussnum<<"  v_gaussnum="<<v_gaussnum<<endl;
    OfTime<<"The Basis Number = "<<4*(P_FEMMesh->P_Vertices->size())<<endl;

    cout<<"The Basis Number = "<<4*(P_FEMMesh->P_Vertices->size())<<endl;
    PetscInt NumVertex = P_FEMMesh->P_Vertices->size();



    cout<<"Initialized coeffMatrix and Generate_coeffMatrix"<<endl;

    OfTime<<endl;
    OfTime<<"Initialized coeffMatrix and Generate_coeffMatrix"<<endl;

    t0=(double)clock();

    MatCreate(PETSC_COMM_WORLD, &coeff_matrix);

    MatSetSizes(coeff_matrix, PETSC_DECIDE, PETSC_DECIDE, NumVertex*4, NumVertex*4);
    MatSetFromOptions(coeff_matrix);
    MatMPIAIJSetPreallocation(coeff_matrix, 64, NULL, 64, NULL);
    MatSeqAIJSetPreallocation(coeff_matrix, PETSC_DEFAULT, NULL);
    MatSetUp(coeff_matrix);

    cout<<"Generate_CoeffMatrix_Based on Cells"<<endl;
//    GenerateCoeffMatrix_RefinedParametrization(u_gaussnum, v_gaussnum, coeff_matrix, m_IntePt,
//                                               m_IntePt, w, P_FEMMesh, *P_RefinedParametricMap,
//                                               k1, k2, ExampleIndex);
    Generate_CoeffMatrix_Cell(u_gaussnum, v_gaussnum, coeff_matrix, m_IntePt, m_IntePt, w, P_FEMMesh,
                              P_ParametricMesh, *P_ParametricMap, k1, k2, ExampleIndex);


    t1=(double)clock();
    cout<<"\"Initialized coeffMatrix and Generate_coeffMatrix\" costs ="<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    OfTime<<"\"Initialized coeffMatrix and Generate_coeffMatrix\" costs ="<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    //load_vect
    //vector<double> load_vect;
    cout<<"Initialized load Vector and Generate load Vector"<<endl;

    OfTime<<endl;
    OfTime<<"Initialized load Vector and Generate load Vector"<<endl;
    t0=(double)clock();


    Initialized_loadVector(load_vect, P_FEMMesh->P_Vertices->size());
//    GenerateLoadVector_RefinedParametrization(load_vect, gausspt, m_loadw, P_FEMMesh,
//                                              *P_RefinedParametricMap, k1, k2, ExampleIndex);
    Generate_LoadVector(load_vect, gausspt, m_loadw, P_FEMMesh, P_ParametricMesh,
                        *P_ParametricMap, k1, k2, ExampleIndex);

    t1=(double)clock();

    cout<<"Finished and it costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    OfTime<<"Finished and it costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    //Set Bases type
    cout<<"Set Bases type"<<endl;
    t0=(double)clock();
    vector<unsigned int> BasesType;
    BasesType.resize(4*P_FEMMesh->P_Vertices->size(), 0);
    Set_Bases_type(P_FEMMesh, BasesType);

    t1=(double)clock();
    cout<<"\"Set Bases type\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    OfTime<<endl;
    OfTime<<"There are "<< 4*P_FEMMesh->P_Vertices->size()<<" bases"<<endl;
    OfTime<<"\"Set Bases type\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;



    //Modify LoadVector and CoeffMatrix
    double C=0;

    MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);



    map<unsigned int, set<unsigned int> > HTable;
    Initialized_HTable_withoutHangingVertices_1(P_FEMMesh, HTable);
    cout<<"Modification_LoadVector_CoeffMatrix"<<endl;

    OfTime<<endl;
    OfTime<<"Modification_LoadVector_CoeffMatrix"<<endl;
    t0=(double)clock();
    Modification_LoadVector_CoeffMatrix(C, BasesType, coeff_matrix, load_vect, HTable);

    MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);



    MatSetOption(coeff_matrix, MAT_SYMMETRIC, PETSC_TRUE);

    t1=(double)clock();
    cout<<"\"Modification_LoadVector_CoeffMatrix\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    OfTime<<"\"Modification_LoadVector_CoeffMatrix\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    //
    VecCreate(PETSC_COMM_WORLD, &m_solut);
    VecSetSizes(m_solut, PETSC_DECIDE, 4*P_FEMMesh->P_Vertices->size());
    VecSetFromOptions(m_solut);

    //Solving the Linear System Part======================================================
    cout<<"solve the Linear system"<<endl;
    t0=(double)clock();
    KSP ksp;
    PC pc;
    KSPCreate(PETSC_COMM_WORLD, &ksp);
   // KSPSetType(ksp, KSPCG);
    KSPSetType(ksp, KSPGMRES);
    KSPSetOperators(ksp, coeff_matrix, coeff_matrix);
    //PetscReal rtol=1.e-18;
    //PetscReal stol=1.e-16;

    PetscReal rtol=(1.e-4)*pow(1.0/(1.0*k+1.0),4.0);
    PetscReal stol=(1.e-3)*pow(1.0/(1.0*k+1.0),4.0);
    if(ExampleIndex==27)
    {
        rtol=(1.e-4)*pow(1.0/(1.0*k+2.0),4.0);
        stol=(1.e-4)*pow(1.0/(1.0*k+2.0),4.0);
    }
    KSPSetTolerances(ksp, rtol, stol, PETSC_DEFAULT, PETSC_DEFAULT);

    KSPGetPC(ksp, &pc);
    PCSetType(pc, PCGAMG);
    KSPSetFromOptions(ksp);

    KSPSolve(ksp, load_vect, m_solut);

    KSPGetConvergedReason(ksp,&reason);
    //printf
    PetscInt its;
    KSPGetIterationNumber(ksp, &its);
    PetscPrintf(PETSC_COMM_WORLD, "The number of iteration %D\n", its);

    t1=(double)clock();
    cout<<"\"Solving the linear system\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;

    OfTime<<endl;
    OfTime<<"\"Solving the linear system\" costs "<<(t1-t0)/CLOCKS_PER_SEC<<" s"<<endl;
    OfTime<<"k = "<<k<<endl;
    if(ExampleIndex!=27)
    {
        OfTime<<"where, rtol=(1.e-4)*pow(1/(k+1),4.0)="<<rtol<<" and  stol=(1.e-3)*pow(1/(k+1),4.0)= "<<stol<<endl;
    }
    else
    {
        OfTime<<"where, rtol=(1.e-4)*pow(1/(k+2),4.0)="<<rtol<<" and  stol=(1.e-3)*pow(1/(k+2),4.0)= "<<stol<<endl;
    }

    OfTime<<"KSP ConvergedReason= "<<reason<<endl;
    OfTime<<"the number of iteration = "<<its<<endl;
    OfTime<<endl;
    //-------
    OfTime.close();




//    //Print coeffmatrix
//    PetscViewer viewer1;
//    PetscViewerASCIIOpen(PETSC_COMM_WORLD, "coeffMatrix.m", &viewer1);
//    PetscViewerSetFormat(viewer1, PETSC_VIEWER_ASCII_MATLAB);
//    MatView(coeff_matrix, viewer1);
//    PetscViewerDestroy(&viewer1);

    //Print load_vect
    PrintLoadVector(load_vect);

    PrintSolution(m_solut);

    PrintAxelFile_FEMSolution(CoeffRefinedParametricMap, P_ParametricMesh, 0, m_solut);

    //Error========================================================================
//    vector<double> cell_errors;//Store the errors over all the cells
//    double totol_Error=1.0;
//    //----
//    vector<double> cell_errorsPhysDom;
//    double total_ErrorPhysDom;
//    //---
//    vector<double> cell_errorsResidual;
//    double total_Error_Residual;
//    //---
//    vector<double> cell_errorsResidualPhsicalDom;
//    double total_Error_ResidualPhsicalDom;
//    //---
//    vector<double> CellErrEnergyNormPhysDom;
//    double total_ErrEnergyNormPhysDom;
//    //---
//    vector<double> CellErrEnergyNormMesh;
//    double total_ErrEnergyNormMesh;


//    FEMErrors(gausspt, w, m_solut, P_FEMMesh, P_ParametricMesh,
//              P_ParametricMap, ExampleIndex,
//              cell_errors, totol_Error,
//              cell_errorsPhysDom, total_ErrorPhysDom,
//              cell_errorsResidual, total_Error_Residual,
//              cell_errorsResidualPhsicalDom,  total_Error_ResidualPhsicalDom,
//              CellErrEnergyNormMesh, total_ErrEnergyNormMesh,
//              CellErrEnergyNormPhysDom, total_ErrEnergyNormPhysDom);

    //-----------Just the L2 posterior error----------------------------
    vector<double> cell_errorsPhysDom;
    double total_ErrorPhysDom;

    FEML2PosteriorError(gausspt, w, m_solut, P_FEMMesh, P_ParametricMesh,
                        P_ParametricMap, ExampleIndex, cell_errorsPhysDom,
                        total_ErrorPhysDom);

    //Destroy ksp coeff_matrix load_vec m_solut
    KSPDestroy(&ksp);
    MatDestroy(&coeff_matrix);
    VecDestroy(&load_vect);
    VecDestroy(&m_solut);

}

//=============================================================================

#endif // FEM_PART_H
