#ifndef CELL_H
#define CELL_H

#include <vector>
#include <set>
using namespace std;

class Cell
{
public:

    unsigned int Index_InitialCell;
    /**
     * @brief CellSize = [x0, x1, y0, y1], the cell is [x0, x1]X[y0, y1]
     */
    double CellSize[4];
    vector<unsigned int> Index_ItsCornerVertices;
    set<unsigned int> Index_ItsSubCells;
    int ItsParent;
    //======for subdivision===============
    set<unsigned int> Index_AdjacentCell_up;
    set<unsigned int> Index_AdjacentCell_down;
    set<unsigned int> Index_AdjacentCell_left;
    set<unsigned int> Index_AdjacentCell_right;
    //====================================
private:
    bool Is_CornerVertex(const unsigned int & IndexVertex, unsigned int &IndexInThisCell);
public:
	Cell(void);
	~Cell(void);
public:
	Cell(const Cell &C);

public:
    /**
     * @brief TheLocalDirection: detect in *this cell, the edge with endpoints as the IndexV1-th and the IndexV2-th
     * vertices is along s direction (-s direction) or t direction (-t direction).
     * @param IndexV1
     * @param IndexV2
     * @return s-direction (1), -s -direction (-1); t-direction (2), -t -direction (-2)
     */
    int TheLocalDirection(const unsigned int &IndexV1, const unsigned int &IndexV2);

    /**
     * @brief CoordinateOfACornerVertex: return the IrregularCenterIndex-th vertex's corrdinates in this cell
     * @param IrregularCenterIndex
     * @return vector<double> ST
     */
    bool CoordinateOfACornerVertex(const unsigned int &IrregularCenterIndex, vector<double> &ST);

    /**
     * @brief IndexInItsCornerVertices: the IndexV-th vertex is the Index-th vertex in the sequence of this cell's vector<unsigned int> Index_ItsCornerVertices
     * @param IndexV
     * @return unsigned int &Index
     */
    bool IndexInItsCornerVertices(const unsigned int &IndexV, unsigned int &Index);
};

#endif // CELL_H
