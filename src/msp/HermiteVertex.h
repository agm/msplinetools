#ifndef HERMITEVERTEX_H
#define HERMITEVERTEX_H

#include <vector>
using namespace std;

//--------------
class HermiteData;
class Mesh;
//--------------

class HermiteVertex
{
public:
    vector<HermiteData *> P_ItsHermiteData;
public:
    HermiteVertex(void);
    ~HermiteVertex(void);

    /**
     * @brief HermiteVertex: Copy const HermiteVertex & HV
     * @param HV
     */
    HermiteVertex(const HermiteVertex &HV);


    /**
     * @brief HermiteVertex: Generate a HermiteVertex corresponding to the index_vertex-th vertex
     * @param index_vertex
     * @param CurrentMesh
     * @param Hermitedata
     */
    HermiteVertex(unsigned int index_vertex, const Mesh & CurrentMesh, const vector<double> & Hermitedata);

    /**
     * @brief HermiteVertex: Generate a HermiteVertex corresponding to the index_vertex-th vertex,from index_Cell0 to index_Cell
     * @param index_vertex
     * @param index_Cell
     * @param CurrentMesh
     * @param Hermitedata
     */
    HermiteVertex(unsigned int index_vertex, unsigned int index_Cell, const Mesh & CurrentMesh, const vector<double> &Hermitedata);

public:
	void Cout_HermiteVertex();
};

#endif //HERMITEVERTEX_H
