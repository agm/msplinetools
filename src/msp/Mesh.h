#ifndef MESH_H
#define MESH_H

#include <vector>
#include <map>
#include <set>
#include <string>

#include "Vertex.h"
#include "Cell.h"
using namespace std;
//--------------------
class Vertex;
class Cell;
class FrameTransferMatrix;
struct NewInformation;

//--------------------


class Mesh
{
public:
    vector<Vertex>  *P_Vertices;
    vector<Cell>  *P_Cells;
    vector< vector<FrameTransferMatrix > >  *P_FrameTranMatrix;

    //Irregular vertices should be interior vertices
    map<unsigned int, unsigned int> IrregularVerticesMap;//From the indices of irregular vertices to their degrees.

    map<unsigned int, unsigned int> MeshIrregularVerticesMap;

public://Initialize
    Mesh(void);
    ~Mesh(void);
    void ReadFile_withoutHangingVertices(string file);

public:
    void Cout_Mesh_File();
private:
    void Cout_Vertex_File();
    void Cout_IrregularVertices();
    void Cout_MeshIrregularVertices();
    void Cout_Cells_File();
    void Cout_FrameTransferMatrix();
	
public://Initialize
    void Is_Interior_irregular_Vertex();
    void Initialization_Index_AdjacentCells();

    /**
     * @brief Is_MeshIrregularVertexConnectedToMeshIrregularVertex:
     * Return if there is a irregular vertex such that one of its adjcent vertex is irregular
     * @return
     */
    bool Is_MeshIrregularVertexConnectedToMeshIrregularVertex();

    /**
     * @brief InitialSetCG1ConditionAtAMeshIrregularVertexBasedOnPositions:
     * For the IrVindex-th meshirregular vertex, Reset Coeff such that this surface is CG1 at this irregular vertex
     * @param IrVindex
     * @param Coeff
     */
    void InitialSetCG1ConditionAtAMeshIrregularVertexBasedOnPositions(unsigned int IrVindex, vector<vector<double> > &Coeff);

    /**
     * @brief Is_MeshIrregularCenter: For the regular vertex (the IndexV-th vertex)
     * @param IndexVIsMeshIrregular
     * @param IndexV
     * @param IrregularCenterIndex
     * @return
     */
    bool Is_MeshIrregularCenter(const bool &IndexVIsMeshIrregular, const unsigned int &IndexV, unsigned int &IrregularCenterIndex);

    //
    /**
     * @brief Is_DirectionAlongAnEdge:
     * return the index-th basis is the basis which is the derivation along the edge with endpoints as the IndexV-th
     * and the IrregularCenterIndex-th vertices
     * @param index
     * @param IndexV
     * @param IrregularCenterIndex
     * @return
     */
    bool Is_DirectionAlongAnEdge(const unsigned int &index, const unsigned int &IndexV, const unsigned int &IrregularCenterIndex);

    bool CommonCellsIndexOfAnEdges(set<unsigned int> &CommonCellsIndex, const unsigned int &IndexV1, const unsigned int &IndexV2);

public://Subdivision// Return a new mesh
	Mesh * Subdivide_initialParametricMesh_Globally(unsigned int k, map<unsigned int, unsigned int> & OriginalVertexToCurrentVertex);

    //With returing some vertices on the given edges
    Mesh * Subdivide_initialParametricMesh_Globally_Vertex(unsigned int k,
                               const vector<pair<unsigned int, unsigned int> > &InitialEdges,
                               const vector<unsigned int> &EdgesIndexofCells,
                               map<unsigned int, unsigned int> & OriginalVertexToCurrentVertex,
                               vector<vector<unsigned int> > &CurrentVerticesEdges,
                               map<unsigned int, pair<unsigned int ,Coordinate*> > &CurrentVerticesIndexToCoordinate);

public://For a parameter point on the original mesh (P_ParametricMesh),
        //obtain its correspondence parameter point on the mesh (P_FEMMesh) given by subdividing P_ParametricMesh
        void NewIndexOfCellOnRefinedMesh(const Mesh* P_ParametricMesh,
                                         const double parameter_u, const double parameter_v,
                                         const unsigned int OriginalCellIndex, unsigned int & NewIndex);


private://Subdivision
    void subdivide_OneCell(unsigned int cell_index, unsigned int k, NewInformation& NewInfo, map<unsigned int, set<unsigned int> > &OriginalVertexToCurrentVertices);
    void SubdivideCells(unsigned int k, vector<NewInformation>& NewInfoCells, map<unsigned int, set<unsigned int> > &OriginalVertexToCurrentVertices);
    void ReOrganizeVerticesIndices_MergeTheSameVertices(vector<NewInformation> &NewInfoCells, unsigned int k, unsigned int &VertexNum, map<unsigned int, set<unsigned int> > & OriginalVertexToCurrentVertices, map<unsigned int, unsigned int> &OriginalVertexToCurrentVertex);
	Mesh * RestoreMesh_Globally(const vector<NewInformation> &NewInfoCells, unsigned int VertexNum);
    void MergeVertices(unsigned int low_index, unsigned int index, vector<NewInformation> &NewInfoCells, unsigned int flag,  map<unsigned int, set<unsigned int> > & OriginalVertexToCurrentVertices);
	void DeterminedTheAdjacentCells(unsigned int i_cell,set<unsigned int> & IndexCells_waiting_Next,vector<unsigned int> & Times_sDirection, vector<unsigned int> & Times_tDirection , set <unsigned int> &Index_Cells_determined_S, set <unsigned int> & Index_Cells_determined_T);
        //----------------------------------------
//        void subdivide_OneCell_Vertex(unsigned int cell_index, unsigned int k, NewInformation& NewInfo, map<unsigned int, unsigned int> &OriginalVertexToCurrentVertex,
//                                            const vector<pair<unsigned int, unsigned int> > &InitialEdges,
//                                            const map<unsigned int, vector<unsigned int> > &CellsIndexToEdgesIndex,
//                                            vector< vector<unsigned int> > &CurrentVerticesEdges,
//                                            map<unsigned int, pair<unsigned int, Coordinate*> > &CurrentVerticesIndexToCoordinate);
//        void SubdivideCells_Vertex(unsigned int k, vector<NewInformation>& NewInfoCells,
//                                   const vector<pair<unsigned int, unsigned int> > &InitialEdges,
//                                   const map<unsigned int, vector<unsigned int> > &CellsIndexToEdgesIndex,
//                                   vector<vector<unsigned int> > &CurrentVerticesEdges,
//                                   map<unsigned int, unsigned int> &OriginalVertexToCurrentVertex,
//                                   map<unsigned int, Coordinate*> &CurrentVerticesIndexToCoordinate);
//        void ReOrganizeVerticesIndices_MergeTheSameVertices_Vertex
//                                            (vector<NewInformation> &NewInfoCells, unsigned int k, unsigned int &VertexNum,
//                                            map<unsigned int, unsigned int> & OriginalVertexToCurrentVertex,
//                                             vector<vector<unsigned int> > &CurrentVerticesEdges,
//                                             map<unsigned int, pair<unsigned int, Coordinate*> > &CurrentVerticesIndexToCoordinate);
//        void MergeVertices_Vertex(unsigned int low_index, unsigned int index,
//                                vector<NewInformation> &NewInfoCells, unsigned int flag, unsigned int kk,
//                                 const map<unsigned int, vector<unsigned int> > &CellsIndexToEdgesIndex,
//                                map<unsigned int, unsigned int> & OriginalVertexToCurrentVertex,
//                                vector<vector<unsigned int> > &CurrentVerticesEdges,
//                                map<unsigned int, pair<unsigned int, Coordinate*> > &CurrentVerticesIndexToCoordinate);
        void ConstructCurrentVerticesIndexToCoordinate(const vector<NewInformation> &NewInfoCells,
                                                             map<unsigned int, pair<unsigned int ,Coordinate*> > &CurrentVerticesIndexToCoordinate);

        void ConstructCurrentVerticesEdges(const vector<NewInformation> &NewInfoCells,
                                                 const vector<pair<unsigned int, unsigned int> > &InitialEdges,
                                                 const map<unsigned int, vector<unsigned int> > &CellsIndexToEdgesIndex,
                                                 vector<vector<unsigned int> > &CurrentVerticesEdges);

public:
        /**
         * @brief GetBoundaryEdgesFromParametricMesh: Get all the boundary edges from the current parametric mesh
         * @param BoundaryEdges (be returned)
         */
        void GetBoundaryEdgesFromParametricMesh(set<pair<unsigned int, unsigned int> > &BoundaryEdges);

        /**
         * @brief Is_ABoundaryEdge: return if the edge (IndexV1IndexV2) is a boundary edge or not
         * @param IndexV1
         * @param IndexV2
         * @return
         */
        bool Is_ABoundaryEdge(const unsigned int IndexV1, const unsigned int IndexV2);

        /**
         * Compute the one neighbourhood Vertex sequence around a center vertex
         * Return: vector<unsigened int> & VertexIndicesSeqAroundCenterVertex
         * RM: Vertex indices in VertexIndicesSeqAroundCenterVertex are arranged in some order
       */
        void OneNeighbourhoodSequenceAroundCenterVertex(vector<unsigned int> & VertexIndicesSeqAroundCenterVertex, const unsigned int & CenterVIndex);


        void IrrCellIndices(set<unsigned int> &IrrCells);

        void SingularCellIndice_Finder(set<unsigned int> &SingularCellIndice, const set<unsigned int> &SingularVertexIndiceFEM);

private:
        unsigned int NextIndexVertex(const unsigned int &IndexCell, const unsigned int &IndexCenterVertex, const unsigned int &IndexV1);

        unsigned int NextIndexCell(const unsigned int &IndexCell1, const unsigned int &IndexV1, const unsigned int &IndexV2);
};

#endif//MESH_H
