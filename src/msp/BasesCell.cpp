//#include "StdAfx.h"
#include <iostream>
using namespace std;

#include "BasesCell.h"
#include "Mesh.h"
#include "Cell.h"
#include "Functions.h"
#include "Basis.h"
#include "MSplineFunction.h"

BasesCell::BasesCell(void)
{
}

BasesCell::~BasesCell(void)
{
}

BasesCell::BasesCell(unsigned int index_Cell, const Mesh & CurrentMesh)
{
	Index_Cell=index_Cell;
	P_Bases_Cell.clear();
    for (unsigned int i=0; i<CurrentMesh.P_Cells->at(index_Cell).Index_ItsCornerVertices.size(); i++)
	{
        unsigned int Index_Vertex=CurrentMesh.P_Cells->at(index_Cell).Index_ItsCornerVertices[i];

		vector<vector<double> > HermiteDatas;
		Set_HermiteData_Vertex(Index_Vertex, CurrentMesh, HermiteDatas);
		for(unsigned int i_H=0; i_H < HermiteDatas.size(); i_H++)
		{
			basis * P_BC=new basis(Index_Vertex, i_H, Index_Cell, CurrentMesh, HermiteDatas[i_H]);
			P_Bases_Cell.push_back(P_BC);
			P_BC=NULL;
		}
		

	}
}

void BasesCell::Cout_BasesCell(const Mesh & CurrentMesh)
{
	for(unsigned int i=0; i<P_Bases_Cell.size(); i++)
	{
		P_Bases_Cell[i]->Cout_basis(CurrentMesh);

        //system("PAUSE");
        cin.get();

	}

}

MSplineFunction *  BasesCell::ToMSplineFunction(unsigned int k, const Mesh * P_Mesh)
{
    unsigned int indexBasis=this->P_Bases_Cell[k]->Index_basis;

    //cout<<"BasesCell::ToMSplineFunction::indexBasis="<<indexBasis<<endl;

    MSplineFunction * P_MSpFun=new MSplineFunction(indexBasis, *P_Mesh);

    //P_MSpFun->Cout_MSplineFunction();

   // cin.get();

    return P_MSpFun;
}
