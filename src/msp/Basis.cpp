//#include "StdAfx.h"

#include <map>
#include <iostream>

using namespace std;

#include "Basis.h"
#include "HermiteVertex.h"
#include "Coordinate.h"
#include "Mesh.h"
#include "Vertex.h"
#include "HermiteCell.h"
#include "HermiteData.h"
#include "Cell.h"
#include "MSplineEvaluation.h"

basis::basis(void)
{
}

basis::~basis(void)
{
}


//Generated the (4*index_vertex+i_H)-th basis function. it must be the basis function at the index_vertex-th vertex
basis::basis(unsigned int index_vertex, unsigned int i_H, const Mesh & CurrentMesh, const vector<double> & Hermitedata)
{
	//Index_basis
	Index_basis=index_vertex*4+i_H;//--Here, the "basis" functions at each basis vertex is always 4, 
									//although there are some zero "bases" at irregular vertices.

	//--For each cell of this vertex, Gernerate a HermiteVertex for this vertex
	HermiteVertex * P_HV=new HermiteVertex(index_vertex, CurrentMesh, Hermitedata);

	//P_HV->Cout_HermiteVertex();

	//Generate basis functions at this vertex
	P_HermiteData_Cells.clear();
	//==========================
	unsigned int index=0;
    for (map<unsigned int, Coordinate>::const_iterator it=CurrentMesh.P_Vertices->at(index_vertex).ItsCellsToCoordinatesMap.begin(); it!=CurrentMesh.P_Vertices->at(index_vertex).ItsCellsToCoordinatesMap.end(); it++)
	{//=P_HV->P_ItsHermiteData.size()
		unsigned int index_cell=(*it).first;

		HermiteCell *P_HC = new HermiteCell(index_vertex, index_cell, P_HV->P_ItsHermiteData[index], CurrentMesh);
		P_HermiteData_Cells.push_back(P_HC);

		index=index+1;
		P_HC=NULL;
	}

	P_HV=NULL;
}

basis::basis(unsigned int index_vertex, unsigned int i_H, unsigned int index_Cell, const Mesh & CurrentMesh, const vector<double> & Hermitedata)
{
	//Index_basis
	Index_basis=index_vertex*4+i_H;//--Here, the "basis" functions at each basis vertex is always 4, 
	//although there are some zero "bases" at irregular vertices.

	//--For each cell of this vertex, Gernerate a HermiteVertex for this vertex
	HermiteVertex * P_HV=new HermiteVertex(index_vertex, index_Cell, CurrentMesh, Hermitedata);

	//Generate basis functions at this vertex
	P_HermiteData_Cells.clear();
	//==========================

	HermiteCell *P_HC = new HermiteCell(index_vertex, index_Cell, P_HV->P_ItsHermiteData[0], CurrentMesh);
	P_HermiteData_Cells.push_back(P_HC);
	P_HC=NULL;

	P_HV=NULL;
}

void basis::Cout_basis(const Mesh & CurrentMesh)
{
	cout<<"---------------------------------------------------------"<<endl;
	cout<<"Index_basis = "<<Index_basis<<endl;
	cout<<"P_HermiteData_Cells:"<<endl;
	for (unsigned int i=0; i<P_HermiteData_Cells.size(); i++)
	{
		//P_HermiteData_Cells[i]-->HermiteCell
		cout<<"the "<<i<<" -th HermiteCell:"<<endl;
		cout<<"Index_InitialCell = "<<(P_HermiteData_Cells[i])->Index_InitialCell<<endl;
		cout<<"Index_Cell = "<<(P_HermiteData_Cells[i])->Index_Cell<<endl;
		cout<<"Indices of Vertices of this Cell : "<<endl;
        for (unsigned int k=0; k<CurrentMesh.P_Cells->at((P_HermiteData_Cells[i])->Index_Cell).Index_ItsCornerVertices.size(); k++)
		{
            cout<<CurrentMesh.P_Cells->at((P_HermiteData_Cells[i])->Index_Cell).Index_ItsCornerVertices[k]<<"  ";
		}
		cout<<endl;
		for (unsigned int j=0; j<(P_HermiteData_Cells[i])->P_HermiteAtCornerVertex.size(); j++)
		{
			HermiteData *P=(P_HermiteData_Cells[i])->P_HermiteAtCornerVertex[j];
			cout<<"the "<<j<<" -th HermiteData : "<<endl;
			cout<<"Index_InitialCell = "<<P->Index_InitialCell<<endl;
			cout<<"ItsHermiteData=["<<P->ItsHermiteData[0]<<", "<<P->ItsHermiteData[1]<<", "<<P->ItsHermiteData[2]<<", "<<P->ItsHermiteData[3]<<"]"<<endl;
		}
	}
	cout<<"---------------------------------------------------------"<<endl;
}

//=======================================================================
void basis::Evaluation(int DerOrderu, int DerOrderv, const vector<Coordinate*> & Pcoordinate_Ps, unsigned int Index_Cell, vector<double> &Values, const Mesh &CurrentMesh)
{
	bool flag=true;
	for (unsigned int i=0; i<P_HermiteData_Cells.size(); i++)
	{
		if (P_HermiteData_Cells[i]->Index_Cell==Index_Cell)
		{
			value_Cell_Hermite(DerOrderu, DerOrderv, Pcoordinate_Ps, P_HermiteData_Cells[i], Values, CurrentMesh);
			flag=false;
			break;
		}
		
	}
	if (flag)
	{
		Values.resize(Pcoordinate_Ps.size(), 0.0);
	}
}

//======================================================================
void basis::Evaluation(const unsigned int HermiteCellIndex, int DerOrderu, int DerOrderv, const vector<double> & coordinateST, double &Values, const double * PCellSize)
{
    this->P_HermiteData_Cells[HermiteCellIndex]->Evaluate(DerOrderu, DerOrderv, coordinateST, Values, PCellSize);
}
