#ifndef FEM_FUNCTIONS_H
#define FEM_FUNCTIONS_H

#include <math.h>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

#include "msp/Basis.h"
#include "msp/MSplineEvaluation.h"
#include "msp/MSplineFunctionCell.h"
#include "msp/HermiteData.h"
#include "msp/HermiteCell.h"
#include "msp/Functions.h"
#include "msp/BasesVertex.h"
#include "msp/MSplineFunctions.h"

#include "msp/FEM_DynamicFunctions.h"
#include "msp/FEM_Print.h"



#include <petsc.h>
#include <petscksp.h>

const double PI = 3.14159265358979;
//====================FEM solver=========================================
void GaussIntegalPoint(int n, vector<double> &w,vector<double> &gausspoint)
{
	int iter,m,el;
	int mm;
	double nn;
	for(int i = 0; i < n; i++)
	{
		w.push_back(0);	gausspoint.push_back(0);
	}
	iter = 2;
	m = (n + 1) / 2; el = n * (n + 1);
	mm = 4 * m -1;
	nn = (1 - (1 - 1.0 / n) / (8 * n * n));
	vector<double> t,x0;
	double tt;
	for(int i = 3; i <= mm; i+=4)
	{
		tt = (PI / (4 * n + 2)) * i;
		t.push_back(tt);
		x0.push_back(nn*cos(tt));
	}
	vector<double> pkm1,pkp1,t1,pk;
	int x0_size = x0.size();
	vector<double> den,d1,dpn,d2pn,d3pn,d4pn,u,v,h,p,dp,fx;
	t1.resize(x0_size);  pkm1.resize(x0_size);
	pk.resize(x0_size);  t1.resize(x0_size);
	pkp1.resize(x0_size);den.resize(x0_size);
	dpn.resize(x0_size); d2pn.resize(x0_size);
	d3pn.resize(x0_size);d4pn.resize(x0_size);
	d1.resize(x0_size);  u.resize(x0_size);
	v.resize(x0_size);   h.resize(x0_size);
	p.resize(x0_size);   fx.resize(x0_size);
	dp.resize(x0_size);

	for(int i = 0; i < iter; i++)
	{
		for(int j = 0; j < x0_size; j++)
		{
			pkm1[j] = 1;
			pk[j] = x0[j];
		}
		for(int k = 2; k <= n; k++)
			for(int r = 0; r < x0_size; r++)
			{
				t1[r] = x0[r] * pk[r];
				pkp1[r] = t1[r] - pkm1[r] - 1.0 * (t1[r] - pkm1[r]) / k + t1[r];
				pkm1[r] = pk[r]; pk[r] = pkp1[r];
			}
			for(int j = 0; j < x0_size; j++)
			{
				den[j] = 1.0 - x0[j] * x0[j];
				d1[j] = n * (pkm1[j] - x0[j] * pk[j]);
				dpn[j] = d1[j] / den[j];
				d2pn[j] = (2.0 * x0[j] * dpn[j] - el * pk[j]) / den[j];
				d3pn[j] = (4 * x0[j] * d2pn[j] + (2-el) * dpn[j]) / den[j];
				d4pn[j] = (6 * x0[j] * d3pn[j] + (6-el) * d2pn[j]) / den[j];
				u[j] = pk[j] / dpn[j]; v[j] = d2pn[j] / dpn[j];
				h[j] = -u[j] * (1 + 0.5 * u[j] * (v[j] + u[j] * (v[j] * v[j] - u[j] * d3pn[j] / (3 * dpn[j]))));
				p[j] = pk[j] + h[j] * (dpn[j] + 0.5 * h[j] * (d2pn[j] + 1.0 * h[j] / 3 * (d3pn[j] + 0.25 * h[j] * d4pn[j])));
				dp[j] = dpn[j] + h[j] * (d2pn[j] + 0.5 * h[j] * (d3pn[j] + h[j] * d4pn[j] / 3));
				h[j] = h[j] - p[j] / dp[j]; x0[j] = x0[j] + h[j];
			}
	}

	for(int j = 0; j < x0_size; j++)
	{
		gausspoint[j] = - x0[j] - h[j];
		fx[j] = d1[j] - h[j] * el * (pk[j] + h[j]/2 * (dpn[j] + h[j]/3 * (d2pn[j] + h[j] / 4 * (d3pn[j] + 0.2 * h[j] * d4pn[j]))));
		w[j] = 2 * (1.0-gausspoint[j] * gausspoint[j]) / (fx[j] * fx[j]);
	}
	if(m + m > n) gausspoint[m-1] = 0;
	if((m + m) != n) m = m - 1;
	vector<int> jj,n1j;
	jj.resize(m); n1j.resize(m);
	for(int j = 0; j < m; j++)
	{
		jj[j] = j; n1j[j] = n - jj[j] - 1;
		gausspoint[n1j[j]] = -gausspoint[jj[j]];
		w[n1j[j]] = w[jj[j]];
	}
}

//====================================================================================================
static double ** GaussPoints_Weights(unsigned int u_gaussnum, unsigned int load_gaussnum, vector<double> &gausspt,
                                     vector<double> &m_IntePt, vector<double> &m_loadw)
{
	gausspt.clear();
	m_IntePt.clear();
	m_loadw.clear();

	//Gauss Points
	vector<double> m_Weight;
	GaussIntegalPoint(u_gaussnum, m_Weight, m_IntePt);
	GaussIntegalPoint(load_gaussnum, m_loadw, gausspt);

//    cout<<"GaussPoints"<<endl;
//    for(unsigned int i=0; i<m_IntePt.size(); i++)
//    {
//        cout<<"i="<<i<<"->"<<m_IntePt[i]<<endl;
//    }
//    cout<<"Weights"<<endl;
//    for(unsigned int i=0; i<m_Weight.size(); i++)
//    {
//        cout<<"i="<<i<<"->"<<m_Weight[i]<<endl;
//    }

	//Weights
	double **w;
	w = new double *[u_gaussnum];
	for (unsigned int i=0; i < u_gaussnum; i++)
	{
		w[i] = new double[u_gaussnum];
	};
	for (unsigned int i=0; i<u_gaussnum; i++)
	{
		for (unsigned int j=0; j<u_gaussnum; j++)
		{
			w[i][j]=m_Weight[i]*m_Weight[j];
		};
	};

	return w;
}
//====================================================================================================

//void LUsolve(int matrixorder, int p, vector< vector<double> > matrix, vector<double> b,vector<double> &solution)
//{
//	int i,j;
//	int h,s,pp;
//	double c;
//	double **M;
//	M = new double*[matrixorder];
//	for(i = 0; i < matrixorder; i++)
//		M[i] = new double[p];
//	for (i = 0; i < matrixorder; i++)
//	{
//		for (j = 0; j < p; j++)
//		{
//			M[i][j] = matrix[i][j];
//		}
//		solution[i] = b[i];
//	}
//	for(i = 2; i <= matrixorder; i++)
//	{
//		if(matrixorder >= i + p ) h = i + p - 1;
//		else h = matrixorder;
//		for(j = i; j <= h; j++)
//		{
//			c=0.0;
//			if(j > p) s = j - p + 1;
//			else s = 1;
//			for(int pp = s; pp <= i - 1; pp++)
//				c += matrix[pp-1][i-pp] * matrix[pp-1][j-pp] / matrix[pp-1][0];

//			matrix[i-1][j-i] -= c;
//		}
//	}

//	for(i = 2; i <= matrixorder; i++)
//	{
//		c = 0.0;
//		if(i > p) s = i - p + 1;
//		else s = 1;
//		for(pp = s; pp <= i - 1; pp++)
//			c += matrix[pp-1][i-pp] * solution[pp-1] / matrix[pp-1][0];
//		solution[i-1] = solution[i-1] - c;
//	}

//	for(i = matrixorder; i >= 1; i--)
//	{
//		c = 0.0;
//		if(matrixorder >= i + p) s = i + p - 1;
//		else s = matrixorder;
//		for(pp = i + 1; pp <= s; pp++)
//			c += matrix[i-1][pp-i] * solution[pp-1];

//		solution[i-1] = (solution[i-1] - c) / matrix[i-1][0];
//	}
//	for(i = 0; i < matrixorder; i++)
//		delete[] M[i];
//	delete[] M;
//}

//====================Initialized coeff_matrix and load_vect============================
//void Initialized_coeffMatrix(vector<vector<double> > &coeff_matrix, unsigned int VertexNum)
//{
//	unsigned int matrix_size=VertexNum*4;

//	coeff_matrix.resize(matrix_size);
//	for (unsigned int i=0; i<matrix_size; i++)
//	{
//		coeff_matrix[i].resize(matrix_size,0.0);
//	};

//}

void Initialized_coeffMatrix(Mat &coeff_matrix, unsigned int VertexNum)
{
    MatCreate(PETSC_COMM_WORLD, &coeff_matrix);
    unsigned int matrix_size=VertexNum*4;
    MatSetSizes(coeff_matrix, PETSC_DECIDE, PETSC_DECIDE, matrix_size, matrix_size);
    MatSetFromOptions(coeff_matrix);
    MatMPIAIJSetPreallocation(coeff_matrix, 64, NULL, 64, NULL);
    MatSeqAIJSetPreallocation(coeff_matrix, 64, NULL);
    MatSetUp(coeff_matrix);
}

//void Initialized_loadVector(vector<double>  &load_vect, unsigned int VertexNum)
//{
//	unsigned int matrix_size=4*VertexNum;
//	load_vect.resize(matrix_size,0.0);
//}
void Initialized_loadVector(Vec &load_vect, unsigned int VertexNum)
{
        unsigned int matrix_size=4*VertexNum;
        //load_vect.resize(matrix_size,0.0);
        VecCreate(PETSC_COMM_WORLD, &load_vect);
        VecSetSizes(load_vect, PETSC_DECIDE, matrix_size);
        VecSetFromOptions(load_vect);
}
////================ParametricMap Part===========================================================
////Reader Coeff
//void Reader_Coeff_ParametricMap(string file, unsigned int dimension, unsigned int VerNum , vector< vector<double> > &Coeff_Parametric_Map)
//{
//	unsigned int BasesNum= VerNum*4;
//	ifstream infile(file.c_str());

//	Coeff_Parametric_Map.clear();
//	vector<double> Coeff_i;
//	for (unsigned int i=0; i<dimension; i++)
//	{
//		Coeff_Parametric_Map.push_back(Coeff_i);
//	}
//	for (unsigned int i=0; i<BasesNum; i++)
//	{
//		for (unsigned int j=0; j<dimension; j++)
//		{
//			double values;
//			infile>>values;
//			Coeff_Parametric_Map[j].push_back(values);
//		}

//	}

//};
//==================================
void ComputeFunval_MSplineParametricMap(const vector<Coordinate*> &Pcoordinate_Ps, unsigned int Index_Cell, vector<double> &funval, int flag,
                                        const Mesh & CurrentParametricMesh, MSplineFunctions * P_MSplineParametricMap, unsigned int ExampleIndex)
{

    double funcval;
    funval.clear();

    vector<vector<double> > XYZ;
    P_MSplineParametricMap->Evaluation(0,0,Pcoordinate_Ps, Index_Cell, XYZ, CurrentParametricMesh);


    //-----------------------------------
    if(flag == 0)//the value of function F
    {
        for (unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
        {
            double x=XYZ[0][i];
            double y=XYZ[1][i];
            funcval=ExactFunctionValue(x, y, ExampleIndex);
            funval.push_back(funcval);
        }

    }
    else if(flag == 1)//f=-Delta(F)
    {
        for (unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
        {

            double x=XYZ[0][i];
            double y=XYZ[1][i];

            funcval=RightSideFunctionValue(x, y, ExampleIndex);

            funval.push_back(funcval);
        }
    }
    else if(flag == 2)// Fx
    {
        for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
        {
            double x=XYZ[0][i];
            double y=XYZ[1][i];
            funcval=FxFunctionValue(x,y,ExampleIndex);

            funval.push_back(funcval);
        }
    }
    else if(flag == 3)// Fy
    {
        for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
        {
            double x=XYZ[0][i];
            double y=XYZ[1][i];
            funcval=FyFunctionValue(x,y,ExampleIndex);

            funval.push_back(funcval);
        }
    }
    else
    {
        cout<<"FunctionsFEM.h:ComputeFunval_MSplineParametricMap():: the flag should be 0(F) , 1(-Delta F), 2 (Fx), 3 (Fy)!"<<endl;
        system("PAUSE");
    }
    vector<double>(funval).swap(funval);
}
//==========================
//==================================
void ComputeFunval_MSplineParametricMap_PDE_PFEM(const vector<Coordinate*> &Pcoordinate_Ps, unsigned int Index_Cell, vector<double> &funval, int flag,
                                        const Mesh & CurrentParametricMesh, MSplineFunctions * P_MSplineParametricMap, unsigned int ExampleIndex,
                                                 unsigned int EquationIndex)
{
    double funcval;
    funval.clear();

    vector<vector<double> > XYZ;
    P_MSplineParametricMap->Evaluation(0,0,Pcoordinate_Ps, Index_Cell, XYZ, CurrentParametricMesh);


    //-----------------------------------
    if(flag == 0)//the value of function F
    {
        for (unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
        {
            double x=XYZ[0][i];
            double y=XYZ[1][i];
            funcval=ExactFunctionValue_PDE_PFEM(x, y, ExampleIndex, EquationIndex);
            funval.push_back(funcval);
        }

    }
    else if(flag == 1)//f=-Delta(F)
    {
        for (unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
        {

            double x=XYZ[0][i];
            double y=XYZ[1][i];

            funcval=RightSideFunctionValue_PDE_PFEM(x, y, ExampleIndex, EquationIndex);

            funval.push_back(funcval);
        }
    }
    else if(flag == 2)// Fx
    {
        for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
        {
            double x=XYZ[0][i];
            double y=XYZ[1][i];
            funcval=FxFunctionValue_PDE_PFEM(x,y,ExampleIndex, EquationIndex);

            funval.push_back(funcval);
        }
    }
    else if(flag == 3)// Fy
    {
        for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
        {
            double x=XYZ[0][i];
            double y=XYZ[1][i];
            funcval=FyFunctionValue_PDE_PFEM(x,y,ExampleIndex, EquationIndex);

            funval.push_back(funcval);
        }
    }
    else if(flag == 4)//The Value of BMatrix
    {
        for(unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
        {
            double x=XYZ[0][i];
            double y=XYZ[1][i];
            funcval=EquationsRightSideElementValue_PDE_PFEM(x, y, ExampleIndex, EquationIndex);

            funval.push_back(funcval);
        }

    }
    else
    {
        cout<<"FunctionsFEM.h:ComputeFunval_MSplineParametricMap():: the flag should be 0(F) , 1(-Delta F), 2 (Fx), 3 (Fy), 4(The value of BMatrix)!"<<endl;
        cin.get();
    }
    vector<double>(funval).swap(funval);
}
//==========================
static double abs_double(double d)
{
    if (d>=0)
    {
            return d;
    }
    else
    {
            return -d;
    }
};
//void ParametricMapSigma_MSplineParametricMap(const vector<Coordinate*> &Pcoordinate_Ps, vector<vector<double> > &m_JPart, vector<double> &DetJ, unsigned int Index_ParametricMeshCell, const MSplineFunctions &MSplineParametricMap, const Mesh * P_ParametricMesh)
//{//dimParametricMap=2
//	//ParametricMapSigma_MSplineParametricMap(coordinate_Ps, m_JPart, detJ , P_FEMMesh->P_Cells[i]->ItsParent, ParametricMap, P_ParametricMesh);
//	m_JPart.clear();
//	DetJ.clear();
//	//X(s,t): MSplineParametricMap.P_MSplineFunctions[0]
//	vector<double> dXds, dXdt;


//	MSplineParametricMap.P_MSplineFunctions[0]->Evaluation(1,0,Pcoordinate_Ps, dXds, Index_ParametricMeshCell, *P_ParametricMesh);
//	MSplineParametricMap.P_MSplineFunctions[0]->Evaluation(0,1,Pcoordinate_Ps, dXdt, Index_ParametricMeshCell, *P_ParametricMesh);

//	//Y(s,t): MSplineParametricMap.P_MSplineFunctions[1]
//	vector<double> dYds, dYdt;
//	MSplineParametricMap.P_MSplineFunctions[1]->Evaluation(1,0,Pcoordinate_Ps, dYds, Index_ParametricMeshCell, *P_ParametricMesh);
//	MSplineParametricMap.P_MSplineFunctions[1]->Evaluation(0,1,Pcoordinate_Ps, dYdt, Index_ParametricMeshCell, *P_ParametricMesh);

//	//----det(Jacobian)
//	for (unsigned int ii=0; ii<Pcoordinate_Ps.size(); ii++)
//	{
//		DetJ.push_back(dXds[ii]*dYdt[ii]-dXdt[ii]*dYds[ii]);

//// 		//For testing--------------------------------------------------
//// 		cout<<"dXds[ii]="<<dXds[ii]<<", "<<"dXdt[ii]="<<dXdt[ii]<<endl;
//// 		cout<<"dYds[ii]="<<dYds[ii]<<", "<<"dYdt[ii]="<<dYdt[ii]<<endl;
//// 		//-------------------------------------------------------------
//	}

//	vector<double> m_JParti;
//	for (unsigned int ii=0; ii<Pcoordinate_Ps.size(); ii++)
//	{
//		m_JParti.clear();
//		double a11=dXds[ii]*dXds[ii]+dYds[ii]*dYds[ii];
//		double a12=dXds[ii]*dXdt[ii]+dYds[ii]*dYdt[ii];
//		double a21=dXds[ii]*dXdt[ii]+dYds[ii]*dYdt[ii];
//		double a22=dXdt[ii]*dXdt[ii]+dYdt[ii]*dYdt[ii];
//		double det=a11*a22-a12*a21;
//		if (abs_double(det)<1.0e-10)
//		{
//			cout<<"CellIndex = "<<Index_ParametricMeshCell<<endl;
//			//P_ParametricMesh->P_Cells->at(Index_ParametricMeshCell)
//			cout<<"MSplineParametricMap["<<Index_ParametricMeshCell<<"]"<<endl;
//			MSplineParametricMap.P_MSplineFunctions[0]->P_MSpline[Index_ParametricMeshCell]->Cout_MSplineFunctionCell();
//			MSplineParametricMap.P_MSplineFunctions[1]->P_MSpline[Index_ParametricMeshCell]->Cout_MSplineFunctionCell();

//			cout<<"dXds[ii]="<<dXds[ii]<<", "<<"dXdt[ii]="<<dXdt[ii]<<endl;
//			cout<<"dYds[ii]="<<dYds[ii]<<", "<<"dYdt[ii]="<<dYdt[ii]<<endl;

//			cout<<"Jocabian Matrix is nearly singular!"<<endl;
//			system("PAUSE");
//		}
//		m_JParti.push_back(DetJ[ii]*a22/det);
//		m_JParti.push_back(-DetJ[ii]*a12/det);
//		m_JParti.push_back(-DetJ[ii]*a21/det);
//		m_JParti.push_back(DetJ[ii]*a11/det);
//		m_JPart.push_back(m_JParti);

//	}

//};
//====
void ParametricMapSigma_MSplineParametricMap(const vector<Coordinate*> &Pcoordinate_Ps, vector<vector<double> > &m_JPart,
                                             vector<double> &DetJ, unsigned int Index_ParametricMeshCell,
                                             const MSplineFunctions &MSplineParametricMap, const Mesh * P_ParametricMesh,
                                             double k1, double k2, unsigned int ExampleIndex)
{//dimParametricMap=2
    //ParametricMapSigma_MSplineParametricMap(coordinate_Ps, m_JPart, detJ , P_FEMMesh->P_Cells[i]->ItsParent, ParametricMap, P_ParametricMesh);
    m_JPart.clear();
    DetJ.clear();

    vector <double> X, Y;
    MSplineParametricMap.P_MSplineFunctions[0]->Evaluation(0,0,Pcoordinate_Ps, X, Index_ParametricMeshCell, *P_ParametricMesh);
    MSplineParametricMap.P_MSplineFunctions[1]->Evaluation(0,0,Pcoordinate_Ps, Y, Index_ParametricMeshCell, *P_ParametricMesh);

    //X(s,t): MSplineParametricMap.P_MSplineFunctions[0]
    vector<double> dXds, dXdt;

    MSplineParametricMap.P_MSplineFunctions[0]->Evaluation(1,0,Pcoordinate_Ps, dXds, Index_ParametricMeshCell, *P_ParametricMesh);
    MSplineParametricMap.P_MSplineFunctions[0]->Evaluation(0,1,Pcoordinate_Ps, dXdt, Index_ParametricMeshCell, *P_ParametricMesh);

    //Y(s,t): MSplineParametricMap.P_MSplineFunctions[1]
    vector<double> dYds, dYdt;
    MSplineParametricMap.P_MSplineFunctions[1]->Evaluation(1,0,Pcoordinate_Ps, dYds, Index_ParametricMeshCell, *P_ParametricMesh);
    MSplineParametricMap.P_MSplineFunctions[1]->Evaluation(0,1,Pcoordinate_Ps, dYdt, Index_ParametricMeshCell, *P_ParametricMesh);




    //----det(Jacobian)
    for (unsigned int ii=0; ii<Pcoordinate_Ps.size(); ii++)
    {
        DetJ.push_back(dXds[ii]*dYdt[ii]-dXdt[ii]*dYds[ii]);

        //cout<<"dXds["<<ii<<"]= "<<dXds[ii]<<endl;
        //cout<<"dYdt["<<ii<<"]= "<<dYdt[ii]<<endl;
        //cout<<"dXdt["<<ii<<"]= "<<dXdt[ii]<<endl;
        //cout<<"dYds["<<ii<<"]= "<<dYds[ii]<<endl;

        //cout<<"FEM_Functions.h::DetJ= "<<dXds[ii]*dYdt[ii]-dXdt[ii]*dYds[ii]<<endl;
    }

    vector<double> m_JParti;
    for (unsigned int ii=0; ii<Pcoordinate_Ps.size(); ii++)
    {
        m_JParti.clear();

        //J^(-1)====================================
        double Jinv11(0.0);
        double Jinv12(0.0);
        double Jinv21(0.0);
        double Jinv22(0.0);

        if (abs_double(DetJ[ii])<1.0e-10)
        {

            cout<<"CellIndex = "<<Index_ParametricMeshCell<<endl;
            //P_ParametricMesh->P_Cells->at(Index_ParametricMeshCell)
            cout<<"MSplineParametricMap["<<Index_ParametricMeshCell<<"]"<<endl;
            MSplineParametricMap.P_MSplineFunctions[0]->P_MSpline[Index_ParametricMeshCell]->Cout_MSplineFunctionCell();
            MSplineParametricMap.P_MSplineFunctions[1]->P_MSpline[Index_ParametricMeshCell]->Cout_MSplineFunctionCell();

            cout<<"dXds[ii]="<<dXds[ii]<<", "<<"dXdt[ii]="<<dXdt[ii]<<endl;
            cout<<"dYds[ii]="<<dYds[ii]<<", "<<"dYdt[ii]="<<dYdt[ii]<<endl;

            cout<<"Jocabian Matrix is nearly singular!"<<endl;
            cin.get();
        }
        else
        {
            Jinv11=dYdt[ii]/DetJ[ii];
            Jinv12=-dXdt[ii]/DetJ[ii];
            Jinv21=-dYds[ii]/DetJ[ii];
            Jinv22=dXds[ii]/DetJ[ii];
        }

        vector<double> Kxy;
        double x=X[ii];
        double y=Y[ii];
        KMatrixValue(x,y,k1,k2,Kxy,ExampleIndex);

        double a11=Jinv11*(Jinv11*Kxy[0]+Jinv12*Kxy[1])+Jinv12*(Jinv11*Kxy[2]+Jinv12*Kxy[3]);
        double a12=Jinv21*(Jinv11*Kxy[0]+Jinv12*Kxy[1])+Jinv22*(Jinv11*Kxy[2]+Jinv12*Kxy[3]);
        double a21=Jinv11*(Jinv21*Kxy[0]+Jinv22*Kxy[1])+Jinv12*(Jinv21*Kxy[2]+Jinv22*Kxy[3]);
        double a22=Jinv21*(Jinv21*Kxy[0]+Jinv22*Kxy[1])+Jinv22*(Jinv21*Kxy[2]+Jinv22*Kxy[3]);

        m_JParti.push_back(DetJ[ii]*a11);
        m_JParti.push_back(DetJ[ii]*a12);
        m_JParti.push_back(DetJ[ii]*a21);
        m_JParti.push_back(DetJ[ii]*a22);
        m_JPart.push_back(m_JParti);
    }
}

//====
void ParametricMapSigma(const vector<Coordinate*> &Pcoordinate_Ps, vector<vector<double> > &m_JPart,
                        vector<double> &DetJ, double k1, double k2, unsigned int ExampleIndex, unsigned SubExampleIndex)
{//dimParametricMap=2
    m_JPart.clear();
    DetJ.clear();

    vector <double> X, Y;
    //MSplineParametricMap.P_MSplineFunctions[0]->Evaluation(0,0,Pcoordinate_Ps, X, Index_ParametricMeshCell, *P_ParametricMesh);
    //MSplineParametricMap.P_MSplineFunctions[1]->Evaluation(0,0,Pcoordinate_Ps, Y, Index_ParametricMeshCell, *P_ParametricMesh);
    ParametricMapSigmaVal(0,0,Pcoordinate_Ps,X,Y, ExampleIndex, SubExampleIndex);//This Sigma parametrization is special. It is global.

    //X(s,t): MSplineParametricMap.P_MSplineFunctions[0]
    vector<double> dXds, dXdt;
    //MSplineParametricMap.P_MSplineFunctions[0]->Evaluation(1,0,Pcoordinate_Ps, dXds, Index_ParametricMeshCell, *P_ParametricMesh);
    //MSplineParametricMap.P_MSplineFunctions[0]->Evaluation(0,1,Pcoordinate_Ps, dXdt, Index_ParametricMeshCell, *P_ParametricMesh);
    //Y(s,t): MSplineParametricMap.P_MSplineFunctions[1]
    vector<double> dYds, dYdt;
    //MSplineParametricMap.P_MSplineFunctions[1]->Evaluation(1,0,Pcoordinate_Ps, dYds, Index_ParametricMeshCell, *P_ParametricMesh);
    //MSplineParametricMap.P_MSplineFunctions[1]->Evaluation(0,1,Pcoordinate_Ps, dYdt, Index_ParametricMeshCell, *P_ParametricMesh);
    ParametricMapSigmaVal(1,0,Pcoordinate_Ps,dXds,dYds, ExampleIndex, SubExampleIndex);
    ParametricMapSigmaVal(0,1,Pcoordinate_Ps,dXdt,dYdt, ExampleIndex, SubExampleIndex);



    //----det(Jacobian)
    for (unsigned int ii=0; ii<Pcoordinate_Ps.size(); ii++)
    {
        DetJ.push_back(dXds[ii]*dYdt[ii]-dXdt[ii]*dYds[ii]);
    }

    vector<double> m_JParti;
    for (unsigned int ii=0; ii<Pcoordinate_Ps.size(); ii++)
    {
        m_JParti.clear();

        //J^(-1)====================================
        double Jinv11(0.0);
        double Jinv12(0.0);
        double Jinv21(0.0);
        double Jinv22(0.0);

        if (abs_double(DetJ[ii])<1.0e-18)
        {
            cout<<"Jocabian Matrix is nearly singular!"<<endl;
            //cin.get();
            Jinv11=dYdt[ii]/DetJ[ii];
            Jinv12=-dXdt[ii]/DetJ[ii];
            Jinv21=-dYds[ii]/DetJ[ii];
            Jinv22=dXds[ii]/DetJ[ii];
        }
        else
        {
            Jinv11=dYdt[ii]/DetJ[ii];
            Jinv12=-dXdt[ii]/DetJ[ii];
            Jinv21=-dYds[ii]/DetJ[ii];
            Jinv22=dXds[ii]/DetJ[ii];
        }

        vector<double> Kxy;
        double x=X[ii];
        double y=Y[ii];
        KMatrixValue(x,y,k1,k2,Kxy,ExampleIndex);

//        double a11=Jinv11*(Jinv11*Kxy[0]+Jinv12*Kxy[1])+Jinv12*(Jinv11*Kxy[2]+Jinv12*Kxy[3]);
//        double a12=Jinv21*(Jinv11*Kxy[0]+Jinv12*Kxy[1])+Jinv22*(Jinv11*Kxy[2]+Jinv12*Kxy[3]);
//        double a21=Jinv11*(Jinv21*Kxy[0]+Jinv22*Kxy[1])+Jinv12*(Jinv21*Kxy[2]+Jinv22*Kxy[3]);
//        double a22=Jinv21*(Jinv21*Kxy[0]+Jinv22*Kxy[1])+Jinv22*(Jinv21*Kxy[2]+Jinv22*Kxy[3]);
//        m_JParti.push_back(DetJ[ii]*a11);
//        m_JParti.push_back(DetJ[ii]*a12);
//        m_JParti.push_back(DetJ[ii]*a21);
//        m_JParti.push_back(DetJ[ii]*a22);

        double b11=(dYdt[ii]*dYdt[ii]*Kxy[0]-dYdt[ii]*dXdt[ii]*Kxy[1]-dXdt[ii]*dYdt[ii]*Kxy[2]+dXdt[ii]*dXdt[ii]*Kxy[3])/DetJ[ii];
        double b12=(-dYds[ii]*dYdt[ii]*Kxy[0]+dYds[ii]*dXdt[ii]*Kxy[1]+dXds[ii]*dYdt[ii]*Kxy[2]-dXds[ii]*dXdt[ii]*Kxy[3])/DetJ[ii];
        double b21=(-dYds[ii]*dYdt[ii]*Kxy[0]+dYdt[ii]*dXds[ii]*Kxy[1]+dXdt[ii]*dYds[ii]*Kxy[2]-dXds[ii]*dXdt[ii]*Kxy[3])/DetJ[ii];
        double b22=(dYds[ii]*dYds[ii]*Kxy[0]-dYds[ii]*dXds[ii]*Kxy[1]-dXds[ii]*dYds[ii]*Kxy[2]+dXds[ii]*dXds[ii]*Kxy[3])/DetJ[ii];
        m_JParti.push_back(b11);
        m_JParti.push_back(b12);
        m_JParti.push_back(b21);
        m_JParti.push_back(b22);

        m_JPart.push_back(m_JParti);
    }
}


//======================================================================================
//void Generate_CoeffMatrix_Cell(unsigned int u_gaussnum, unsigned int v_gaussnum, vector<vector<double> > &coeff_matrix, vector<double> u_gausspt,
// vector<double> v_gausspt, double **w, const Mesh*  P_FEMMesh, const Mesh * P_ParametricMesh, const MSplineFunctions &ParametricMap)
//{
//	//vector<double> u,v;//store Gauss points in a cell
//	/*--Compute the stiff matrix(the upper triangular matrix)--*/
//	vector<double> m_kDervalu;
//	vector<double> m_kDervalv;
//	vector<double> m_lDervalu;
//	vector<double> m_lDervalv;
//	vector<vector<double> > m_JPart;
//	double m_temp;

//	for(unsigned int i=0; i<P_FEMMesh->P_Cells->size(); i++)//fill in the coeff matrix based on cells
//	{
//		//===========Guass Points============================
//		double u0=P_FEMMesh->P_Cells->at(i).CellSize[0];
//		double u1=P_FEMMesh->P_Cells->at(i).CellSize[1];
//		double v0=P_FEMMesh->P_Cells->at(i).CellSize[2];
//		double v1=P_FEMMesh->P_Cells->at(i).CellSize[3];

//		vector<Coordinate*> Pcoordinate_Ps;
//		for (unsigned int j=0; j<u_gaussnum; j++)
//		{
//			for (unsigned int l=0; l<v_gaussnum; l++)
//			{
//				Coordinate * CoorP=new Coordinate;
//				//CoorP->xy[0]=u[j];
//				CoorP->xy[0]=((u1-u0) * u_gausspt[j] + u0 + u1) * 0.5;
//				//CoorP->xy[1]=v[l];
//				CoorP->xy[1]=((v1-v0) * v_gausspt[l] + v1 + v0) * 0.5;
//				Pcoordinate_Ps.push_back(CoorP);
//				CoorP=NULL;
//			}
//		}
//		//============Bases over this cell===============================================

//		BasesCell * P_BC=new BasesCell(i, *P_FEMMesh);//BasesCell(unsigned int index_Cell, const Mesh & CurrentMesh);

//		for (unsigned int k=0; k<P_BC->P_Bases_Cell.size(); k++)//the FEM-bases over the i-th cells
//		{
//			m_kDervalu.clear();
//			m_kDervalv.clear();//m_val.clear();
//			//==========================
//			value_Cell_Hermite(1, 0, Pcoordinate_Ps, P_BC->P_Bases_Cell[k]->P_HermiteData_Cells[0], m_kDervalu, *P_FEMMesh);
//			value_Cell_Hermite(0, 1, Pcoordinate_Ps, P_BC->P_Bases_Cell[k]->P_HermiteData_Cells[0], m_kDervalv, *P_FEMMesh);

//			vector<double> detJ;
//			ParametricMapSigma_MSplineParametricMap(Pcoordinate_Ps, m_JPart, detJ , P_FEMMesh->P_Cells->at(i).ItsParent, ParametricMap, P_ParametricMesh);

//			//==========================
//			for(unsigned int kk = 0; kk < P_BC->P_Bases_Cell.size(); kk++)
//			{
//				if(P_BC->P_Bases_Cell[kk]->Index_basis>=P_BC->P_Bases_Cell[k]->Index_basis)
//				{
//					m_lDervalu.clear();
//					m_lDervalv.clear();
//					value_Cell_Hermite(1, 0, Pcoordinate_Ps, P_BC->P_Bases_Cell[kk]->P_HermiteData_Cells[0], m_lDervalu, *P_FEMMesh);
//					value_Cell_Hermite(0, 1, Pcoordinate_Ps, P_BC->P_Bases_Cell[kk]->P_HermiteData_Cells[0], m_lDervalv, *P_FEMMesh);

//					//================================================================================
//					m_temp = 0;
//					for(unsigned int l = 0; l < u_gaussnum*v_gaussnum; l++)
//					{
//						int nn = l / v_gaussnum;
//						int mm = l % v_gaussnum;


//						double xx1=m_kDervalu[l];
//						double yy1=m_kDervalv[l];
//						double xx2=m_lDervalu[l];
//						double yy2=m_lDervalv[l];
//						double J11=m_JPart[l][0];
//						double J12=m_JPart[l][1];
//						double J21=m_JPart[l][2];
//						double J22=m_JPart[l][3];

//						m_temp += w[nn][mm]*(xx1*J11*xx2+yy1*J21*xx2+xx1*J12*yy2+yy1*J22*yy2);
//					}
//					coeff_matrix[P_BC->P_Bases_Cell[k]->Index_basis][P_BC->P_Bases_Cell[kk]->Index_basis-P_BC->P_Bases_Cell[k]->Index_basis] += m_temp * (u1-u0) * (v1-v0) / 4;

//				}
//			}
//		}

//		delete P_BC;
//		P_BC=NULL;

//	}
//}


 //======================================================================================
static void Initialized_HTable_withoutHangingVertices_1(const Mesh *P_FEMMesh, map<unsigned int, set<unsigned int> > &HTable)
{
	HTable.clear();
	for (unsigned int i=0; i<P_FEMMesh->P_Vertices->size(); i++)//Just store a half of HTable
	{
		set<unsigned int> Table_i;//map<unsigned int, Coordinate> ItsCellsToCoordinatesMap;
		for (map<unsigned int, Coordinate>::const_iterator it=P_FEMMesh->P_Vertices->at(i).ItsCellsToCoordinatesMap.begin(); it!=P_FEMMesh->P_Vertices->at(i).ItsCellsToCoordinatesMap.end(); it++)
		{
			unsigned int i_cell=(*it).first;
			for (unsigned int j=0; j<P_FEMMesh->P_Cells->at(i_cell).Index_ItsCornerVertices.size(); j++)
			{
                 Table_i.insert(P_FEMMesh->P_Cells->at(i_cell).Index_ItsCornerVertices[j]);
			}
		}
		HTable.insert(pair<unsigned int, set<unsigned int> >(i,Table_i));
	}
}
//=======================================================================================
static void Initialized_HTable_withoutHangingVertices(const Mesh *P_FEMMesh, map<unsigned int, set<unsigned int> > &HTable)
{
        HTable.clear();
        for (unsigned int i=0; i<P_FEMMesh->P_Vertices->size(); i++)//Just store a half of HTable
        {
                set<unsigned int> Table_i;//map<unsigned int, Coordinate> ItsCellsToCoordinatesMap;
                for (map<unsigned int, Coordinate>::const_iterator it=P_FEMMesh->P_Vertices->at(i).ItsCellsToCoordinatesMap.begin(); it!=P_FEMMesh->P_Vertices->at(i).ItsCellsToCoordinatesMap.end(); it++)
                {
                        unsigned int i_cell=(*it).first;
                        for (unsigned int j=0; j<P_FEMMesh->P_Cells->at(i_cell).Index_ItsCornerVertices.size(); j++)
                        {
                                if (P_FEMMesh->P_Cells->at(i_cell).Index_ItsCornerVertices[j]>=i)
                                {
                                        Table_i.insert(P_FEMMesh->P_Cells->at(i_cell).Index_ItsCornerVertices[j]);
                                }
                        }
                }
                HTable.insert(pair<unsigned int, set<unsigned int> >(i,Table_i));
        }
}
//=======================================================================================
//void Generate_CoeffMatrix_Vertex_HTable(unsigned int u_gaussnum, unsigned int v_gaussnum, vector<vector<double> > &coeff_matrix, vector<double> u_gausspt,
//										vector<double> v_gausspt, double **w, const Mesh*  P_FEMMesh, const Mesh * P_ParametricMesh, const MSplineFunctions &ParametricMap,
//										const map<unsigned int, set<unsigned int> > & HTable)
//{
//	//vector<double> u,v;//store Gauss points in a cell
//	/*--Compute the stiff matrix(the upper triangular matrix)--*/
//	vector<double> m_kDervalu;
//	vector<double> m_kDervalv;
//	vector<double> m_lDervalu;
//	vector<double> m_lDervalv;
//	vector<vector<double> > m_JPart;
//	double m_temp;

//	for (unsigned int i_v=0; i_v<P_FEMMesh->P_Vertices->size(); i_v++)
//	{
//		set<unsigned int> CellsSet_i;
//		Map2set_unsigned_int(CellsSet_i, P_FEMMesh->P_Vertices->at(i_v).ItsCellsToCoordinatesMap);
//		map<unsigned int, set<unsigned int> >::const_iterator i_map=HTable.find(i_v);
//		for (set<unsigned int>::const_iterator it = (i_map->second).begin(); it != (i_map->second).end(); it++)
//		{
//			unsigned int i_vertex = *it;
//			set<unsigned int> CellsSet;
//			Map2set_unsigned_int(CellsSet, P_FEMMesh->P_Vertices->at(i_vertex).ItsCellsToCoordinatesMap);
//			set<unsigned int> CellSetIntersection;
//			set_intersection(CellsSet_i.begin(), CellsSet_i.end(), CellsSet.begin(), CellsSet.end(),inserter(CellSetIntersection, CellSetIntersection.begin()));
//			//=======================================
//			for (set<unsigned int>::const_iterator jc=CellSetIntersection.begin(); jc!=CellSetIntersection.end(); jc++)
//			{
//				unsigned int i = *jc;//For Cells[i]
//				//===========Guass Points=========
//				double u0=P_FEMMesh->P_Cells->at(i).CellSize[0];
//				double u1=P_FEMMesh->P_Cells->at(i).CellSize[1];
//				double v0=P_FEMMesh->P_Cells->at(i).CellSize[2];
//				double v1=P_FEMMesh->P_Cells->at(i).CellSize[3];

//				vector<Coordinate*> Pcoordinate_Ps;
//				for (unsigned int j=0; j<u_gaussnum; j++)
//				{
//					for (unsigned int l=0; l<v_gaussnum; l++)
//					{
//						Coordinate* CoorP=new Coordinate;
//						//CoorP->xy[0]=u[j];
//						CoorP->xy[0]=((u1-u0) * u_gausspt[j] + u0 + u1) * 0.5;;
//						//CoorP->xy[1]=v[l];
//						CoorP->xy[1]=((v1-v0) * v_gausspt[l] + v1 + v0) * 0.5;;
//						Pcoordinate_Ps.push_back(CoorP);
//						CoorP=NULL;
//					}
//				}
//				//============Bases at the i_v-th vertex and the i_vertex-th vertex===============

//				BasesVertex * P_BV0=new BasesVertex(i_v, i , *P_FEMMesh);//BasesVertex(unsigned int index_vertex, unsigned int index_Cell, const Mesh & CurrentMesh);
//				BasesVertex * P_BV1=new BasesVertex(i_vertex, i, *P_FEMMesh);
//				//=================================================================================

//				//====Compute the Integration=========
//				for (unsigned int k=0; k<P_BV0->P_Base_Vertex.size(); k++)
//				{
//					m_kDervalu.clear();
//					m_kDervalv.clear();//m_val.clear();
//					//-------
//					value_Cell_Hermite(1, 0, Pcoordinate_Ps, P_BV0->P_Base_Vertex[k]->P_HermiteData_Cells[0], m_kDervalu, *P_FEMMesh);
//					value_Cell_Hermite(0, 1, Pcoordinate_Ps, P_BV0->P_Base_Vertex[k]->P_HermiteData_Cells[0], m_kDervalv, *P_FEMMesh);

//					vector<double> detJ;
//					ParametricMapSigma_MSplineParametricMap(Pcoordinate_Ps, m_JPart, detJ , P_FEMMesh->P_Cells->at(i).ItsParent, ParametricMap, P_ParametricMesh);

//					for (unsigned int kk=0; kk<P_BV1->P_Base_Vertex.size(); kk++)
//					{
//						if(P_BV1->P_Base_Vertex[kk]->Index_basis>=P_BV0->P_Base_Vertex[k]->Index_basis)
//						{
//							m_lDervalu.clear();
//							m_lDervalv.clear();
//							value_Cell_Hermite(1, 0, Pcoordinate_Ps, P_BV1->P_Base_Vertex[kk]->P_HermiteData_Cells[0], m_lDervalu, *P_FEMMesh);
//							value_Cell_Hermite(0, 1, Pcoordinate_Ps, P_BV1->P_Base_Vertex[kk]->P_HermiteData_Cells[0], m_lDervalv, *P_FEMMesh);
//							//-----------
//							m_temp = 0;
//							for(unsigned int l = 0; l < u_gaussnum*v_gaussnum; l++)
//							{
//								int nn = l / v_gaussnum;
//								int mm = l % v_gaussnum;


//								double xx1=m_kDervalu[l];
//								double yy1=m_kDervalv[l];
//								double xx2=m_lDervalu[l];
//								double yy2=m_lDervalv[l];
//								double J11=m_JPart[l][0];
//								double J12=m_JPart[l][1];
//								double J21=m_JPart[l][2];
//								double J22=m_JPart[l][3];
//								m_temp += w[nn][mm]*(xx1*J11*xx2+yy1*J21*xx2+xx1*J12*yy2+yy1*J22*yy2);
//							}
//							coeff_matrix[P_BV0->P_Base_Vertex[k]->Index_basis][(P_BV1->P_Base_Vertex[kk]->Index_basis)-(P_BV0->P_Base_Vertex[k]->Index_basis)] += m_temp * (u1-u0) * (v1-v0) / 4;

//						}
//					}
//				}
//                                //----------
//				delete P_BV0;
//				delete P_BV1;
//				P_BV0=NULL;
//				P_BV1=NULL;
//			}
//		}
//			//=======================================
//	}
	
//}



// ==========================================================================================================

static void Bases_type_Cell(unsigned int i_vertex, const Mesh * P_FEMMesh ,vector<unsigned int> &BasesType)
{
    //Consider this mesh's vertices
	set<unsigned int> V0Cells;
    /*Output: V0Cells->the indice of cells with the i_vertex as its corner vertex*/
    Map2set_unsigned_int(V0Cells, P_FEMMesh->P_Vertices->at(i_vertex).ItsCellsToCoordinatesMap);

    for (set<unsigned int>::const_iterator it=P_FEMMesh->P_Vertices->at(i_vertex).Index_ItsVertices.begin();
         it!=P_FEMMesh->P_Vertices->at(i_vertex).Index_ItsVertices.end(); it++)//it->the vertex adjacent to the i_vertex-th vertex
	{
        if(*it>i_vertex &&!P_FEMMesh->P_Vertices->at(*it).Is_interiorVertex)//it-> the boundary vertex
		{
			set<unsigned int> V1Cells;
            /*Output: V1Cells-> the indices of cells with the *it-th vertex as its corner vertex*/
			Map2set_unsigned_int(V1Cells, P_FEMMesh->P_Vertices->at(*it).ItsCellsToCoordinatesMap);

            set<unsigned int> InterSection;
            /*Output: InterSection: the indices of common cells for the i_vertex-th and *it-th vertices*/
			set_intersection(V0Cells.begin(), V0Cells.end(), V1Cells.begin(), V1Cells.end(),inserter(InterSection, InterSection.begin()));

            if (InterSection.size()!=1)//Impossible
			{
                cout<<"Consider this mesh's vertices = "<< i_vertex<<endl;
                cout<<"the index of vertex = "<<*it<<endl;
                cout<<"V0Cells:"<<endl;
                for(set<unsigned int>::const_iterator jt=V0Cells.begin(); jt !=V0Cells.end(); jt++)
                {
                    cout<<*jt<<" ";
                }
                cout<<endl;
                cout<<"V1Cells:"<<endl;
                for(set<unsigned int>::const_iterator jt=V1Cells.begin(); jt!=V1Cells.end(); jt++)
                {
                    cout<<*jt<<" ";
                }
                cout<<endl;
                cout<<"InterSection.size = "<<InterSection.size()<<endl;
				cout<<"FEM_Functions::Set_Bases_type::There is something wrong with determining the interior vertex"<<endl;
                                 cin.get();
			}
            else//there is only one common cells with the i_vertex-th and *it-th vertices as its corner vertex
			{
                unsigned int i_cell = *InterSection.begin();//the index of this common cells
				//Is V0V1 an s-edge or t-edge?
                int i_v0=-1, i_v1=-1;//i_v0 (i_v1) is the order of the i_vertex-th (*it-th) vertex in this common cells.
				for (int i=0; i<P_FEMMesh->P_Cells->at(i_cell).Index_ItsCornerVertices.size(); i++)
				{
					if (i_vertex==P_FEMMesh->P_Cells->at(i_cell).Index_ItsCornerVertices[i])
					{
						i_v0=i;
					}
					if (*it==P_FEMMesh->P_Cells->at(i_cell).Index_ItsCornerVertices[i])
					{
						i_v1=i;
					}
				}

                if (i_v0==-1||i_v1==-1)//Impossible
				{
					cout<<"FEM_Functions::Bases_type_Cell:: There is an error : i_v0=-1 or i_v1=-1"<<endl;
					system("PAUSE");
				}
				//cout<<i_v0<<" "<<i_v1<<endl;
				bool is_s_edge=false;
				if ((i_v0==0&&i_v1==1)||(i_v0==1&&i_v1==0)||(i_v0==2&&i_v1==3)||(i_v0==3&&i_v1==2))
				{
                    is_s_edge=true;// the i_vertex and *it-th edge is an s-edge.
				}

				//========Bases========================
				BasesVertex * P_BC0=new BasesVertex(i_vertex, i_cell, *P_FEMMesh);
				BasesVertex * P_BC1=new BasesVertex(*it, i_cell, *P_FEMMesh);

				BasesType[P_BC0->P_Base_Vertex[0]->Index_basis]=3;
				BasesType[P_BC1->P_Base_Vertex[0]->Index_basis]=3;
				if (is_s_edge)//is an s-edge
				{
                    for (unsigned int i_basis=1; i_basis<3; i_basis++)//the basis indices at the i_vertex-th and *it-th vertices
					{
						//P_BC0
						if(P_BC0->P_Base_Vertex[i_basis]->P_HermiteData_Cells[0]->P_HermiteAtCornerVertex[i_v0]->ItsHermiteData[1])
						{
							BasesType[P_BC0->P_Base_Vertex[i_basis]->Index_basis]=1;
						};
						//P_BC1
						if(P_BC1->P_Base_Vertex[i_basis]->P_HermiteData_Cells[0]->P_HermiteAtCornerVertex[i_v1]->ItsHermiteData[1])
						{
							//cout<<P_BC1->P_Base_Vertex[i_basis]->Index_basis<<endl;

							BasesType[P_BC1->P_Base_Vertex[i_basis]->Index_basis]=1;
						};
					}
				}
				else// is not an s-edge// it is a t-edge
				{
					for (unsigned int i_basis=1; i_basis<3; i_basis++)
					{
						//P_BC0

						if(P_BC0->P_Base_Vertex[i_basis]->P_HermiteData_Cells[0]->P_HermiteAtCornerVertex[i_v0]->ItsHermiteData[2])
						{
							//cout<<P_BC0->P_Base_Vertex[i_basis]->Index_basis<<endl;

							BasesType[P_BC0->P_Base_Vertex[i_basis]->Index_basis]=1;
						};
						//P_BC1

						if(P_BC1->P_Base_Vertex[i_basis]->P_HermiteData_Cells[0]->P_HermiteAtCornerVertex[i_v1]->ItsHermiteData[2])
						{
							//cout<<P_BC1->P_Base_Vertex[i_basis]->Index_basis<<endl;

							BasesType[P_BC1->P_Base_Vertex[i_basis]->Index_basis]=1;
						};
					}
				}
				//================================
				delete P_BC0;
				delete P_BC1;
				P_BC0=NULL;
				P_BC1=NULL;

			}
		}
	}

	//
}
//==========================================================================================
static void Set_Bases_type(const Mesh * P_FEMMesh, vector<unsigned int> &BasesType)
{
    //Here BasesType.resize(0.0, 4*P_FEMMesh->P_Vertices->size());

    //elements in BasesType have three possible values: 0, 1, 2;
    //0: a true basis and the value of this basis is zero on the boundary of the domain
    //1: a true basis with non-zero values  (ds or dt) along the boundary of the domain.
    //2: a zero "basis"
    //3: a true basis with non-zero values on the boundary of the domain: [1,0,0,0]
    for (unsigned int i_vertex=0; i_vertex < P_FEMMesh->P_Vertices->size(); i_vertex++)
    {
        //classify the type of P_FEMMesh->P_Vertices[i_vertex]
        if (P_FEMMesh->P_Vertices->at(i_vertex).Is_interiorVertex)//It is an interior vertex
        {
            unsigned int flag=(P_FEMMesh->P_Vertices->at(i_vertex).Index_ItsVertices.size())%4;
            switch(flag)
            {
            case 1:
                BasesType[4*i_vertex+1]=2;
                BasesType[4*i_vertex+2]=2;
                BasesType[4*i_vertex+3]=2;
                break;
            case 2:
                BasesType[4*i_vertex+1]=2;
                BasesType[4*i_vertex+2]=2;
                break;
            case 3:
                BasesType[4*i_vertex+1]=2;
                BasesType[4*i_vertex+2]=2;
                BasesType[4*i_vertex+3]=2;
                break;
            }
        }
        else//is a boundary vertex
        {
            Bases_type_Cell(i_vertex, P_FEMMesh , BasesType);//(BasesType);
        }
    }
}




void ChangedElementsofSolution(Mesh* P_FEMMesh, const Vec & m_solut, Vec &Solution)
{
	PetscInt SolutSize;
	VecGetSize(m_solut, &SolutSize);
	
	VecCreate(PETSC_COMM_WORLD, &Solution);
	VecSetSizes(Solution, PETSC_DECIDE, SolutSize);
	VecSetFromOptions(Solution);

	VecZeroEntries(Solution);

	map<unsigned int, unsigned int> OrigialVertexToCurrentVertex;
        Mesh *P_FEMMesh1 = P_FEMMesh->Subdivide_initialParametricMesh_Globally(0, OrigialVertexToCurrentVertex);

	for (map<unsigned int, unsigned int>::const_iterator itmap=OrigialVertexToCurrentVertex.begin(); 
                itmap!=OrigialVertexToCurrentVertex.end(); itmap++)
	{
            PetscInt originalIndex=itmap->first;
            PetscScalar v;
            VecGetValues(m_solut,1,&originalIndex, &v);
            PetscInt CurrentIndex=itmap->second;


            VecSetValues(Solution, 1, &CurrentIndex, &v, INSERT_VALUES);
	}
}
//===============================================================================




#endif//FEM_FUNCTIONS_H
