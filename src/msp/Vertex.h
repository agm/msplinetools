#ifndef VERTEX_H
#define VERTEX_H

#include <vector>
#include <set>
#include <map>
using namespace std;
//--------------
class Coordinate;
//---------------

class Vertex
{
public:
    /**
     * @brief Is_interiorVertex: Is this vertex an interior vertex?
     */
    bool Is_interiorVertex;
	map<unsigned int, Coordinate> ItsCellsToCoordinatesMap;
	set<unsigned int> Index_ItsVertices;

public:
	Vertex(void);
	~Vertex(void);
public:
	Vertex(const Vertex & V);
    unsigned int Deg();
};


#endif

