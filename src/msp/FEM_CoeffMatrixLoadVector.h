#ifndef FEM_COEFFMATRIXLOADVECTOR_H
#define FEM_COEFFMATRIXLOADVECTOR_H


#include "msp/Mesh.h"
#include "msp/MSplineFunctions.h"
#include "msp/BasesCell.h"
#include "msp/Bases.h"

#include "msp/FEM_Functions.h"
#include "msp/Functions.h"


#include "petsc.h"

void Generate_CoeffMatrix_Cell(unsigned int u_gaussnum, unsigned int v_gaussnum, Mat &coeff_matrix,
                               vector<double> u_gausspt, vector<double> v_gausspt, double **w,
                               const Mesh*  P_FEMMesh, const Mesh * P_ParametricMesh,
                               const MSplineFunctions &ParametricMap, double k1, double k2,
                               unsigned int ExampleIndex)
{

    MatZeroEntries(coeff_matrix);

    //-----
    cout<<"Input the SubExampleIndex again:"<<endl;
    //
    unsigned int SubExampleIndex;
    cin>>SubExampleIndex;
    //-----
    //vector<double> u,v;//store Gauss points in a cell
    /*--Compute the stiff matrix(the upper triangular matrix)--*/
    vector<double> m_kDervalu;
    vector<double> m_kDervalv;
    vector<double> m_lDervalu;
    vector<double> m_lDervalv;
    vector<vector<double> > m_JPart;
    vector<double> detJ;
    double m_temp;

    PetscInt I;
    PetscInt J;

    for(unsigned int i=0; i<P_FEMMesh->P_Cells->size(); i++)//fill in the coeff matrix based on cells
    {
        //===========Guass Points============================
        double u0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double u1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double v0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double v1=P_FEMMesh->P_Cells->at(i).CellSize[3];

        vector<Coordinate*> Pcoordinate_Ps;
        for (unsigned int j=0; j<u_gaussnum; j++)
        {
            for (unsigned int l=0; l<v_gaussnum; l++)
            {
                Coordinate * CoorP=new Coordinate;
                //CoorP->xy[0]=u[j];
                CoorP->xy[0]=((u1-u0) * u_gausspt[j] + u0 + u1) * 0.5;
                //CoorP->xy[1]=v[l];
                CoorP->xy[1]=((v1-v0) * v_gausspt[l] + v1 + v0) * 0.5;
                Pcoordinate_Ps.push_back(CoorP);
                CoorP=NULL;
            }
        }

        //============Bases over this cell===============================================
        BasesCell * P_BC=new BasesCell(i, *P_FEMMesh);//BasesCell(unsigned int index_Cell, const Mesh & CurrentMesh);

        for (unsigned int k=0; k<P_BC->P_Bases_Cell.size(); k++)//the FEM-bases over the i-th cells
        {
            m_kDervalu.clear();
            m_kDervalv.clear();//m_val.clear();
            //==========================
            value_Cell_Hermite(1, 0, Pcoordinate_Ps, P_BC->P_Bases_Cell[k]->P_HermiteData_Cells[0], m_kDervalu, *P_FEMMesh);
            value_Cell_Hermite(0, 1, Pcoordinate_Ps, P_BC->P_Bases_Cell[k]->P_HermiteData_Cells[0], m_kDervalv, *P_FEMMesh);

            if(ExampleIndex!=51)
            {
                ParametricMapSigma_MSplineParametricMap(Pcoordinate_Ps, m_JPart, detJ,
                                    P_FEMMesh->P_Cells->at(i).ItsParent, ParametricMap,
                                                 P_ParametricMesh, k1, k2, ExampleIndex);
            }
            else
            {
                ParametricMapSigma(Pcoordinate_Ps, m_JPart, detJ, k1, k2, ExampleIndex, SubExampleIndex);
            }


            //==========================
            for(unsigned int kk = 0; kk < P_BC->P_Bases_Cell.size(); kk++)
            {
                if(P_BC->P_Bases_Cell[kk]->Index_basis >= P_BC->P_Bases_Cell[k]->Index_basis)
                {
                    m_lDervalu.clear();
                    m_lDervalv.clear();
                    value_Cell_Hermite(1, 0, Pcoordinate_Ps, P_BC->P_Bases_Cell[kk]->P_HermiteData_Cells[0], m_lDervalu, *P_FEMMesh);
                    value_Cell_Hermite(0, 1, Pcoordinate_Ps, P_BC->P_Bases_Cell[kk]->P_HermiteData_Cells[0], m_lDervalv, *P_FEMMesh);

                    //================================================================================
                    m_temp = 0;
                    for(unsigned int l = 0; l < u_gaussnum*v_gaussnum; l++)
                    {
                        int nn = l / v_gaussnum;
                        int mm = l % v_gaussnum;

                        double xx1=m_kDervalu[l];
                        double yy1=m_kDervalv[l];
                        double xx2=m_lDervalu[l];
                        double yy2=m_lDervalv[l];
                        double J11=m_JPart[l][0];
                        double J12=m_JPart[l][1];
                        double J21=m_JPart[l][2];
                        double J22=m_JPart[l][3];

                        m_temp += w[nn][mm]*(xx1*J11*xx2+yy1*J21*xx2+xx1*J12*yy2+yy1*J22*yy2);
                    }
                    I=P_BC->P_Bases_Cell[k]->Index_basis;
                    J=P_BC->P_Bases_Cell[kk]->Index_basis;
                    PetscScalar v=m_temp * (u1-u0) * (v1-v0) / 4.0;



                    if(I!=J)
                    {
                        MatSetValues(coeff_matrix, 1, &I, 1, &J, &v, ADD_VALUES);
                        MatSetValues(coeff_matrix, 1, &J, 1, &I, &v, ADD_VALUES);
                    }
                    else
                    {
                        MatSetValues(coeff_matrix, 1, &I, 1, &J, &v, ADD_VALUES);
                    }
                }
            }
        }



        delete P_BC;
        P_BC=NULL;

    }

}


void Generate_CoeffMatrix_Vertex_HTable(unsigned int u_gaussnum, unsigned int v_gaussnum, Mat &coeff_matrix, vector<double> u_gausspt,
                                        vector<double> v_gausspt, double **w, const Mesh*  P_FEMMesh,
                                        const Mesh * P_ParametricMesh, const MSplineFunctions &ParametricMap,
                                        const map<unsigned int, set<unsigned int> > & HTable,
                                        double k1, double k2, unsigned int ExampleIndex)
{
        //vector<double> u,v;//store Gauss points in a cell
        /*--Compute the stiff matrix(the upper triangular matrix)--*/
        vector<double> m_kDervalu;
        vector<double> m_kDervalv;
        vector<double> m_lDervalu;
        vector<double> m_lDervalv;
        vector<vector<double> > m_JPart;
        double m_temp;

        PetscScalar v;
        PetscInt I;
        PetscInt J;

        for (unsigned int i_v=0; i_v<P_FEMMesh->P_Vertices->size(); i_v++)
        {
                set<unsigned int> CellsSet_i;
                Map2set_unsigned_int(CellsSet_i, P_FEMMesh->P_Vertices->at(i_v).ItsCellsToCoordinatesMap);
                map<unsigned int, set<unsigned int> >::const_iterator i_map=HTable.find(i_v);
                for (set<unsigned int>::const_iterator it = (i_map->second).begin(); it != (i_map->second).end(); it++)
                {
                        unsigned int i_vertex = *it;
                        set<unsigned int> CellsSet;
                        Map2set_unsigned_int(CellsSet, P_FEMMesh->P_Vertices->at(i_vertex).ItsCellsToCoordinatesMap);
                        set<unsigned int> CellSetIntersection;
                        set_intersection(CellsSet_i.begin(), CellsSet_i.end(), CellsSet.begin(), CellsSet.end(),inserter(CellSetIntersection, CellSetIntersection.begin()));
                        //=======================================
                        for (set<unsigned int>::const_iterator jc=CellSetIntersection.begin(); jc!=CellSetIntersection.end(); jc++)
                        {
                                unsigned int i = *jc;//For Cells[i]
                                //===========Guass Points=========
                                double u0=P_FEMMesh->P_Cells->at(i).CellSize[0];
                                double u1=P_FEMMesh->P_Cells->at(i).CellSize[1];
                                double v0=P_FEMMesh->P_Cells->at(i).CellSize[2];
                                double v1=P_FEMMesh->P_Cells->at(i).CellSize[3];

                                vector<Coordinate*> Pcoordinate_Ps;
                                for (unsigned int j=0; j<u_gaussnum; j++)
                                {
                                        for (unsigned int l=0; l<v_gaussnum; l++)
                                        {
                                                Coordinate* CoorP=new Coordinate;
                                                //CoorP->xy[0]=u[j];
                                                CoorP->xy[0]=((u1-u0) * u_gausspt[j] + u0 + u1) * 0.5;;
                                                //CoorP->xy[1]=v[l];
                                                CoorP->xy[1]=((v1-v0) * v_gausspt[l] + v1 + v0) * 0.5;;
                                                Pcoordinate_Ps.push_back(CoorP);
                                                CoorP=NULL;
                                        }
                                }
                                //============Bases at the i_v-th vertex and the i_vertex-th vertex===============

                                BasesVertex * P_BV0=new BasesVertex(i_v, i , *P_FEMMesh);
                                BasesVertex * P_BV1=new BasesVertex(i_vertex, i, *P_FEMMesh);
                                //=================================================================================

                                //====Compute the Integration=========
                                for (unsigned int k=0; k<P_BV0->P_Base_Vertex.size(); k++)
                                {
                                        m_kDervalu.clear();
                                        m_kDervalv.clear();//m_val.clear();
                                        //-------
                                        value_Cell_Hermite(1, 0, Pcoordinate_Ps, P_BV0->P_Base_Vertex[k]->P_HermiteData_Cells[0], m_kDervalu, *P_FEMMesh);
                                        value_Cell_Hermite(0, 1, Pcoordinate_Ps, P_BV0->P_Base_Vertex[k]->P_HermiteData_Cells[0], m_kDervalv, *P_FEMMesh);

                                        vector<double> detJ;
                                        ParametricMapSigma_MSplineParametricMap(Pcoordinate_Ps, m_JPart, detJ ,
                                                                                P_FEMMesh->P_Cells->at(i).ItsParent, ParametricMap, P_ParametricMesh,
                                                                                k1, k2, ExampleIndex);

                                        for (unsigned int kk=0; kk<P_BV1->P_Base_Vertex.size(); kk++)
                                        {
                                                if(P_BV1->P_Base_Vertex[kk]->Index_basis>=P_BV0->P_Base_Vertex[k]->Index_basis)
                                                {
                                                        m_lDervalu.clear();
                                                        m_lDervalv.clear();
                                                        value_Cell_Hermite(1, 0, Pcoordinate_Ps, P_BV1->P_Base_Vertex[kk]->P_HermiteData_Cells[0], m_lDervalu, *P_FEMMesh);
                                                        value_Cell_Hermite(0, 1, Pcoordinate_Ps, P_BV1->P_Base_Vertex[kk]->P_HermiteData_Cells[0], m_lDervalv, *P_FEMMesh);
                                                        //-----------
                                                        m_temp = 0;
                                                        for(unsigned int l = 0; l < u_gaussnum*v_gaussnum; l++)
                                                        {
                                                                int nn = l / v_gaussnum;
                                                                int mm = l % v_gaussnum;


                                                                double xx1=m_kDervalu[l];
                                                                double yy1=m_kDervalv[l];
                                                                double xx2=m_lDervalu[l];
                                                                double yy2=m_lDervalv[l];
                                                                double J11=m_JPart[l][0];
                                                                double J12=m_JPart[l][1];
                                                                double J21=m_JPart[l][2];
                                                                double J22=m_JPart[l][3];
                                                                m_temp += w[nn][mm]*(xx1*J11*xx2+yy1*J21*xx2+xx1*J12*yy2+yy1*J22*yy2);
                                                        }
                                                        //coeff_matrix[][()-(P_BV0->P_Base_Vertex[k]->Index_basis)] +=
                                                        I=P_BV0->P_Base_Vertex[k]->Index_basis;
                                                        J=P_BV1->P_Base_Vertex[kk]->Index_basis;
                                                        v=m_temp * (u1-u0) * (v1-v0) / 4;
//                                                        MatSetValues(coeff_matrix,1, &I, 1, &J, &v, ADD_VALUES);
//                                                        MatSetValues(coeff_matrix,1, &J, 1, &I, &v, ADD_VALUES);
                                                        if(I!=J)
                                                        {
                                                            MatSetValues(coeff_matrix, 1, &I, 1, &J, &v, ADD_VALUES);
                                                            MatSetValues(coeff_matrix, 1, &J, 1, &I, &v, ADD_VALUES);
                                                        }
                                                        else
                                                        {
                                                            MatSetValues(coeff_matrix, 1, &I, 1, &J, &v, ADD_VALUES);
                                                        }

                                                }
                                        }
                                }
                                //----------
                                delete P_BV0;
                                delete P_BV1;
                                P_BV0=NULL;
                                P_BV1=NULL;
                        }
                }
                        //=======================================
        }


}
//======================================================================================
//void Generate_CoeffMatrix_All_Bases(unsigned int u_gaussnum, unsigned int v_gaussnum, vector<vector<double> > &coeff_matrix, vector<double> u_gausspt,
//									vector<double> v_gausspt, double **w, const Mesh*  P_FEMMesh, const Mesh * P_ParametricMesh, const MSplineFunctions &ParametricMap,
//									const map<unsigned int, set<unsigned int> > & HTable)
//{
//	//Generate all the bases----------------
//	Bases * P_Bases=new Bases(*P_FEMMesh);
//	//--------------------------------------

//	//vector<double> u,v;//store Gauss points in a cell
//	/*--Compute the stiff matrix(the upper triangular matrix)--*/
//	vector<double> m_kDervalu;
//	vector<double> m_kDervalv;
//	vector<double> m_lDervalu;
//	vector<double> m_lDervalv;
//	vector<vector<double> > m_JPart;
//	double m_temp;

//	for (unsigned int i_v = 0; i_v < P_FEMMesh->P_Vertices->size(); i_v ++)
//	{
//		//--Indices of Bases-----------
//		vector<unsigned int> IndexB1;//For i_v
//		IndexB1.push_back(4*i_v);
//		IndexB1.push_back(4*i_v+1);
//		IndexB1.push_back(4*i_v+2);
//		IndexB1.push_back(4*i_v+3);

//		set<unsigned int> CellsSet_i;
//		Map2set_unsigned_int(CellsSet_i, P_FEMMesh->P_Vertices->at(i_v).ItsCellsToCoordinatesMap);
//		map<unsigned int, set<unsigned int> >::const_iterator i_map=HTable.find(i_v);
//		for (set<unsigned int>::const_iterator it = (i_map->second).begin(); it != (i_map->second).end(); it++)
//		{
//			unsigned int i_vertex = *it;

//			//--Indices of Bases-----------
//			vector<unsigned int> IndexB2;//For i_vertex
//			IndexB2.push_back(4*i_vertex);
//			IndexB2.push_back(4*i_vertex+1);
//			IndexB2.push_back(4*i_vertex+2);
//			IndexB2.push_back(4*i_vertex+3);

//			//-----------------------------

//			set<unsigned int> CellsSet;
//			Map2set_unsigned_int(CellsSet, P_FEMMesh->P_Vertices->at(i_vertex).ItsCellsToCoordinatesMap);
//			set<unsigned int> CellSetIntersection;
//			set_intersection(CellsSet_i.begin(), CellsSet_i.end(), CellsSet.begin(), CellsSet.end(),inserter(CellSetIntersection, CellSetIntersection.begin()));
//			//=======================================
//			for (set<unsigned int>::const_iterator jc=CellSetIntersection.begin(); jc!=CellSetIntersection.end(); jc++)
//			{
//				unsigned int i = *jc;//For Cells[i]
//				//===========Guass Points=========
//				double u0=P_FEMMesh->P_Cells->at(i).CellSize[0];
//				double u1=P_FEMMesh->P_Cells->at(i).CellSize[1];
//				double v0=P_FEMMesh->P_Cells->at(i).CellSize[2];
//				double v1=P_FEMMesh->P_Cells->at(i).CellSize[3];

//				vector<Coordinate*> Pcoordinate_Ps;
//				for (unsigned int j=0; j<u_gaussnum; j++)
//				{
//					for (unsigned int l=0; l<v_gaussnum; l++)
//					{
//						Coordinate* CoorP=new Coordinate;
//						//CoorP->xy[0]=u[j];
//						CoorP->xy[0]=((u1-u0) * u_gausspt[j] + u0 + u1) * 0.5;;
//						//CoorP->xy[1]=v[l];
//						CoorP->xy[1]= ((v1-v0) * v_gausspt[l] + v1 + v0) * 0.5;;
//						Pcoordinate_Ps.push_back(CoorP);
//						CoorP=NULL;
//					}
//				}


//				//====Compute the Integration=========
//				for (unsigned int k=0; k<IndexB1.size(); k++)
//				{
//					m_kDervalu.clear();
//					m_kDervalv.clear();//m_val.clear();
//					//-------
//					P_Bases->P_BasesSet[IndexB1[k]]->Evaluation(1, 0, Pcoordinate_Ps, i, m_kDervalu, *P_FEMMesh);
//					P_Bases->P_BasesSet[IndexB1[k]]->Evaluation(0, 1, Pcoordinate_Ps, i, m_kDervalv, *P_FEMMesh);

//					vector<double> detJ;
//					ParametricMapSigma_MSplineParametricMap(Pcoordinate_Ps, m_JPart, detJ , P_FEMMesh->P_Cells->at(i).ItsParent, ParametricMap, P_ParametricMesh);

//					for (unsigned int kk=0; kk<IndexB2.size(); kk++)
//					{
//						if(IndexB2[kk]>=IndexB1[k])
//						{
//							m_lDervalu.clear();
//							m_lDervalv.clear();
//							P_Bases->P_BasesSet[IndexB2[kk]]->Evaluation(1, 0, Pcoordinate_Ps, i, m_lDervalu, *P_FEMMesh);
//							P_Bases->P_BasesSet[IndexB2[kk]]->Evaluation(0, 1, Pcoordinate_Ps, i, m_lDervalv, *P_FEMMesh);
//							//-----------
//							m_temp = 0;
//							for(unsigned int l = 0; l < u_gaussnum*v_gaussnum; l++)
//							{
//								int nn = l / v_gaussnum;
//								int mm = l % v_gaussnum;


//								double xx1=m_kDervalu[l];
//								double yy1=m_kDervalv[l];
//								double xx2=m_lDervalu[l];
//								double yy2=m_lDervalv[l];
//								double J11=m_JPart[l][0];
//								double J12=m_JPart[l][1];
//								double J21=m_JPart[l][2];
//								double J22=m_JPart[l][3];
//								m_temp += w[nn][mm]*(xx1*J11*xx2+yy1*J21*xx2+xx1*J12*yy2+yy1*J22*yy2);
//							}
//							coeff_matrix[IndexB1[k]][IndexB2[kk]-IndexB1[k]] += m_temp * (u1-u0) * (v1-v0) / 4;

//						}
//					}
//				}
//				//=====
//			}
//		}
//		//=======================================
//	}

//	delete P_Bases;
//	P_Bases=NULL;
//}

void Generate_CoeffMatrix_All_Bases(unsigned int u_gaussnum, unsigned int v_gaussnum, Mat &coeff_matrix, vector<double> u_gausspt,
                                    vector<double> v_gausspt, double **w, const Mesh*  P_FEMMesh, const Mesh * P_ParametricMesh, const MSplineFunctions &ParametricMap,
                                    const map<unsigned int, set<unsigned int> > & HTable,
                                    double k1, double k2, unsigned int ExampleIndex)
{
        //Generate all the bases----------------
        Bases * P_Bases=new Bases(*P_FEMMesh);
        //--------------------------------------

        //vector<double> u,v;//store Gauss points in a cell
        /*--Compute the stiff matrix(the upper triangular matrix)--*/
        vector<double> m_kDervalu;
        vector<double> m_kDervalv;
        vector<double> m_lDervalu;
        vector<double> m_lDervalv;
        vector<vector<double> > m_JPart;
        double m_temp;

        PetscScalar v;
        PetscInt I;
        PetscInt J;

        for (unsigned int i_v = 0; i_v < P_FEMMesh->P_Vertices->size(); i_v ++)
        {
                //--Indices of Bases-----------
                vector<unsigned int> IndexB1;//For i_v
                IndexB1.push_back(4*i_v);
                IndexB1.push_back(4*i_v+1);
                IndexB1.push_back(4*i_v+2);
                IndexB1.push_back(4*i_v+3);

                set<unsigned int> CellsSet_i;
                Map2set_unsigned_int(CellsSet_i, P_FEMMesh->P_Vertices->at(i_v).ItsCellsToCoordinatesMap);
                map<unsigned int, set<unsigned int> >::const_iterator i_map=HTable.find(i_v);
                for (set<unsigned int>::const_iterator it = (i_map->second).begin(); it != (i_map->second).end(); it++)
                {
                        unsigned int i_vertex = *it;

                        //--Indices of Bases-----------
                        vector<unsigned int> IndexB2;//For i_vertex
                        IndexB2.push_back(4*i_vertex);
                        IndexB2.push_back(4*i_vertex+1);
                        IndexB2.push_back(4*i_vertex+2);
                        IndexB2.push_back(4*i_vertex+3);

                        //-----------------------------

                        set<unsigned int> CellsSet;
                        Map2set_unsigned_int(CellsSet, P_FEMMesh->P_Vertices->at(i_vertex).ItsCellsToCoordinatesMap);
                        set<unsigned int> CellSetIntersection;
                        set_intersection(CellsSet_i.begin(), CellsSet_i.end(), CellsSet.begin(), CellsSet.end(),inserter(CellSetIntersection, CellSetIntersection.begin()));
                        //=======================================
                        for (set<unsigned int>::const_iterator jc=CellSetIntersection.begin(); jc!=CellSetIntersection.end(); jc++)
                        {
                                unsigned int i = *jc;//For Cells[i]
                                //===========Guass Points=========
                                double u0=P_FEMMesh->P_Cells->at(i).CellSize[0];
                                double u1=P_FEMMesh->P_Cells->at(i).CellSize[1];
                                double v0=P_FEMMesh->P_Cells->at(i).CellSize[2];
                                double v1=P_FEMMesh->P_Cells->at(i).CellSize[3];

                                vector<Coordinate*> Pcoordinate_Ps;
                                for (unsigned int j=0; j<u_gaussnum; j++)
                                {
                                        for (unsigned int l=0; l<v_gaussnum; l++)
                                        {
                                                Coordinate* CoorP=new Coordinate;
                                                //CoorP->xy[0]=u[j];
                                                CoorP->xy[0]=((u1-u0) * u_gausspt[j] + u0 + u1) * 0.5;;
                                                //CoorP->xy[1]=v[l];
                                                CoorP->xy[1]= ((v1-v0) * v_gausspt[l] + v1 + v0) * 0.5;;
                                                Pcoordinate_Ps.push_back(CoorP);
                                                CoorP=NULL;
                                        }
                                }


                                //====Compute the Integration=========
                                for (unsigned int k=0; k<IndexB1.size(); k++)
                                {
                                        m_kDervalu.clear();
                                        m_kDervalv.clear();//m_val.clear();
                                        //-------
                                        P_Bases->P_BasesSet[IndexB1[k]]->Evaluation(1, 0, Pcoordinate_Ps, i, m_kDervalu, *P_FEMMesh);
                                        P_Bases->P_BasesSet[IndexB1[k]]->Evaluation(0, 1, Pcoordinate_Ps, i, m_kDervalv, *P_FEMMesh);

                                        vector<double> detJ;
                                        ParametricMapSigma_MSplineParametricMap(Pcoordinate_Ps, m_JPart, detJ , P_FEMMesh->P_Cells->at(i).ItsParent,
                                                                                ParametricMap, P_ParametricMesh, k1, k2,ExampleIndex);

                                        for (unsigned int kk=0; kk<IndexB2.size(); kk++)
                                        {
                                                if(IndexB2[kk]>=IndexB1[k])
                                                {
                                                        m_lDervalu.clear();
                                                        m_lDervalv.clear();
                                                        P_Bases->P_BasesSet[IndexB2[kk]]->Evaluation(1, 0, Pcoordinate_Ps, i, m_lDervalu, *P_FEMMesh);
                                                        P_Bases->P_BasesSet[IndexB2[kk]]->Evaluation(0, 1, Pcoordinate_Ps, i, m_lDervalv, *P_FEMMesh);
                                                        //-----------
                                                        m_temp = 0;
                                                        for(unsigned int l = 0; l < u_gaussnum*v_gaussnum; l++)
                                                        {
                                                            int nn = l / v_gaussnum;
                                                            int mm = l % v_gaussnum;


                                                            double xx1=m_kDervalu[l];
                                                            double yy1=m_kDervalv[l];
                                                            double xx2=m_lDervalu[l];
                                                            double yy2=m_lDervalv[l];
                                                            double J11=m_JPart[l][0];
                                                            double J12=m_JPart[l][1];
                                                            double J21=m_JPart[l][2];
                                                            double J22=m_JPart[l][3];
                                                            m_temp += w[nn][mm]*(xx1*J11*xx2+yy1*J21*xx2+xx1*J12*yy2+yy1*J22*yy2);
                                                        }
                                                        //coeff_matrix[][-IndexB1[k]] +=
                                                        v= m_temp * (u1-u0) * (v1-v0) / 4;
                                                        I=IndexB1[k];
                                                        J=IndexB2[kk];
//                                                        MatSetValues(coeff_matrix,1, &I, 1, &J, &v, ADD_VALUES);
//                                                        MatSetValues(coeff_matrix,1, &J, 1, &I, &v, ADD_VALUES);
                                                        if(I!=J)
                                                        {
                                                            MatSetValues(coeff_matrix, 1, &I, 1, &J, &v, ADD_VALUES);
                                                            MatSetValues(coeff_matrix, 1, &J, 1, &I, &v, ADD_VALUES);
                                                        }
                                                        else
                                                        {
                                                            MatSetValues(coeff_matrix, 1, &I, 1, &J, &v, ADD_VALUES);
                                                        }

                                                }
                                        }
                                }
                                //=====
                        }
                }
                //=======================================
        }

        delete P_Bases;
        P_Bases=NULL;
}

//======================================================================================
//void Generate_CoeffMatrix_HVertices(unsigned int u_gaussnum, unsigned int v_gaussnum, vector<vector<double> > &coeff_matrix, vector<double> u_gausspt,
//									vector<double> v_gausspt, double **w, const Mesh*  P_FEMMesh, const Mesh * P_ParametricMesh, const MSplineFunctions &ParametricMap,
//									const map<unsigned int, set<unsigned int> > & HTable)
//{
//	//Generate all the bases----------------
//	Bases * P_Bases=new Bases;//HVerticesData_All_bases (vector<vector<HermiteVertex *> > P_HVertices, const Mesh & CurrentMesh);
//	vector<vector<HermiteVertex *> > P_HVertices;
//	P_Bases->HVerticesData_All_bases(P_HVertices,*P_FEMMesh);
//	//--------------------------------------

//	//vector<double> u,v;//store Gauss points in a cell
//	/*--Compute the stiff matrix(the upper triangular matrix)--*/
//	vector<double> m_kDervalu;
//	vector<double> m_kDervalv;
//	vector<double> m_lDervalu;
//	vector<double> m_lDervalv;
//	vector<vector<double> > m_JPart;
//	double m_temp;

//	for (unsigned int i_v = 0; i_v < P_FEMMesh->P_Vertices->size(); i_v ++)
//	{
//		set<unsigned int> CellsSet_i;
//		Map2set_unsigned_int(CellsSet_i, P_FEMMesh->P_Vertices->at(i_v).ItsCellsToCoordinatesMap);
//		map<unsigned int, set<unsigned int> >::const_iterator i_map=HTable.find(i_v);
//		for (set<unsigned int>::const_iterator it = (i_map->second).begin(); it != (i_map->second).end(); it++)
//		{
//			unsigned int i_vertex = *it;

//			set<unsigned int> CellsSet;
//			Map2set_unsigned_int(CellsSet, P_FEMMesh->P_Vertices->at(i_vertex).ItsCellsToCoordinatesMap);

//			set<unsigned int> CellSetIntersection;
//			set_intersection(CellsSet_i.begin(), CellsSet_i.end(), CellsSet.begin(), CellsSet.end(), inserter(CellSetIntersection, CellSetIntersection.begin()));
//			//=======================================
//			for (set<unsigned int>::const_iterator jc=CellSetIntersection.begin(); jc!=CellSetIntersection.end(); jc++)
//			{
//				unsigned int i = *jc;//For Cells[i]
//				//===========Guass Points=========
//				double u0=P_FEMMesh->P_Cells->at(i).CellSize[0];
//				double u1=P_FEMMesh->P_Cells->at(i).CellSize[1];
//				double v0=P_FEMMesh->P_Cells->at(i).CellSize[2];
//				double v1=P_FEMMesh->P_Cells->at(i).CellSize[3];

//// 				for(unsigned int k = 0; k < u_gaussnum; k++)
//// 				{
//// 					u[k] = ((u1-u0) * u_gausspt[k] + u0 + u1) * 0.5;
//// 					v[k] = ((v1-v0) * v_gausspt[k] + v1 + v0) * 0.5;
//// 				}

//				vector<Coordinate*> Pcoordinate_Ps;
//				for (unsigned int j=0; j<u_gaussnum; j++)
//				{
//					for (unsigned int l=0; l<v_gaussnum; l++)
//					{
//						Coordinate* CoorP=new Coordinate;
//						//CoorP->xy[0]=u[j];
//						CoorP->xy[0]=((u1-u0) * u_gausspt[j] + u0 + u1) * 0.5;
//						//CoorP->xy[1]=v[l];
//						CoorP->xy[1]=((v1-v0) * v_gausspt[l] + v1 + v0) * 0.5;
//						Pcoordinate_Ps.push_back(CoorP);
//						CoorP=NULL;
//					}
//				}


//				//====Compute the Integration=========
//				for (unsigned int k=0; k<4; k++)
//				{
//					m_kDervalu.clear();
//					m_kDervalv.clear();//m_val.clear();
//					//-------
//					basis * P_basis=P_Bases->Return_Basis_Cell(i_v, k, i, P_HVertices, *P_FEMMesh);// Return_Basis_Cell(unsigned int Index_Vertex, unsigned int i_H, unsigned int Index_Cell, const vector<vector<HermiteVertex *> > &P_HVertices, const Mesh & CurrentMesh);
//					P_basis->Evaluation(1, 0, Pcoordinate_Ps, i, m_kDervalu, *P_FEMMesh);
//					P_basis->Evaluation(0, 1, Pcoordinate_Ps, i, m_kDervalv, *P_FEMMesh);

//					vector<double> detJ;
//					ParametricMapSigma_MSplineParametricMap(Pcoordinate_Ps, m_JPart, detJ , P_FEMMesh->P_Cells->at(i).ItsParent, ParametricMap, P_ParametricMesh);

//					for (unsigned int kk=0; kk<4; kk++)
//					{
//						basis * P_basis2=P_Bases->Return_Basis_Cell(i_vertex, kk, i, P_HVertices, *P_FEMMesh);
//						if(P_basis2->Index_basis>=P_basis->Index_basis)
//						{
//							m_lDervalu.clear();
//							m_lDervalv.clear();
//							P_basis2->Evaluation(1, 0, Pcoordinate_Ps, i, m_lDervalu, *P_FEMMesh);
//							P_basis2->Evaluation(0, 1, Pcoordinate_Ps, i, m_lDervalv, *P_FEMMesh);
//							//-----------
//							m_temp = 0;
//							for(unsigned int l = 0; l < u_gaussnum*v_gaussnum; l++)
//							{
//								int nn = l / v_gaussnum;
//								int mm = l % v_gaussnum;


//								double xx1=m_kDervalu[l];
//								double yy1=m_kDervalv[l];
//								double xx2=m_lDervalu[l];
//								double yy2=m_lDervalv[l];
//								double J11=m_JPart[l][0];
//								double J12=m_JPart[l][1];
//								double J21=m_JPart[l][2];
//								double J22=m_JPart[l][3];
//								m_temp += w[nn][mm]*(xx1*J11*xx2+yy1*J21*xx2+xx1*J12*yy2+yy1*J22*yy2);
//							}
//							coeff_matrix[P_basis->Index_basis][(P_basis2->Index_basis)-(P_basis->Index_basis)] += m_temp * (u1-u0) * (v1-v0) / 4;

//						}
//						delete P_basis2;
//						P_basis2=NULL;
//					}

//					delete P_basis;
//					P_basis=NULL;
//				}
//				//=====
//			}
//		}
//		//=======================================
//	}
//	delete P_Bases;
//	P_Bases=NULL;

//}

void Generate_CoeffMatrix_HVertices(unsigned int u_gaussnum, unsigned int v_gaussnum, Mat &coeff_matrix, vector<double> u_gausspt,
                                                                        vector<double> v_gausspt, double **w, const Mesh*  P_FEMMesh, const Mesh * P_ParametricMesh, const MSplineFunctions &ParametricMap,
                                                                        const map<unsigned int, set<unsigned int> > & HTable,
                                                                        double k1, double k2, unsigned int ExampleIndex)
{
        //Generate all the bases----------------
        Bases * P_Bases=new Bases;//HVerticesData_All_bases (vector<vector<HermiteVertex *> > P_HVertices, const Mesh & CurrentMesh);
        vector<vector<HermiteVertex *> > P_HVertices;
        P_Bases->HVerticesData_All_bases(P_HVertices,*P_FEMMesh);
        //--------------------------------------

        //vector<double> u,v;//store Gauss points in a cell
        /*--Compute the stiff matrix(the upper triangular matrix)--*/
        vector<double> m_kDervalu;
        vector<double> m_kDervalv;
        vector<double> m_lDervalu;
        vector<double> m_lDervalv;
        vector<vector<double> > m_JPart;
        double m_temp;

        PetscInt I;
        PetscInt J;
        PetscScalar v;

        for (unsigned int i_v = 0; i_v < P_FEMMesh->P_Vertices->size(); i_v ++)
        {
                set<unsigned int> CellsSet_i;
                Map2set_unsigned_int(CellsSet_i, P_FEMMesh->P_Vertices->at(i_v).ItsCellsToCoordinatesMap);
                map<unsigned int, set<unsigned int> >::const_iterator i_map=HTable.find(i_v);
                for (set<unsigned int>::const_iterator it = (i_map->second).begin(); it != (i_map->second).end(); it++)
                {
                        unsigned int i_vertex = *it;

                        set<unsigned int> CellsSet;
                        Map2set_unsigned_int(CellsSet, P_FEMMesh->P_Vertices->at(i_vertex).ItsCellsToCoordinatesMap);

                        set<unsigned int> CellSetIntersection;
                        set_intersection(CellsSet_i.begin(), CellsSet_i.end(), CellsSet.begin(), CellsSet.end(), inserter(CellSetIntersection, CellSetIntersection.begin()));
                        //=======================================
                        for (set<unsigned int>::const_iterator jc=CellSetIntersection.begin(); jc!=CellSetIntersection.end(); jc++)
                        {
                                unsigned int i = *jc;//For Cells[i]
                                //===========Guass Points=========
                                double u0=P_FEMMesh->P_Cells->at(i).CellSize[0];
                                double u1=P_FEMMesh->P_Cells->at(i).CellSize[1];
                                double v0=P_FEMMesh->P_Cells->at(i).CellSize[2];
                                double v1=P_FEMMesh->P_Cells->at(i).CellSize[3];

                                vector<Coordinate*> Pcoordinate_Ps;
                                for (unsigned int j=0; j<u_gaussnum; j++)
                                {
                                        for (unsigned int l=0; l<v_gaussnum; l++)
                                        {
                                                Coordinate* CoorP=new Coordinate;
                                                //CoorP->xy[0]=u[j];
                                                CoorP->xy[0]=((u1-u0) * u_gausspt[j] + u0 + u1) * 0.5;
                                                //CoorP->xy[1]=v[l];
                                                CoorP->xy[1]=((v1-v0) * v_gausspt[l] + v1 + v0) * 0.5;
                                                Pcoordinate_Ps.push_back(CoorP);
                                                CoorP=NULL;
                                        }
                                }


                                //====Compute the Integration=========
                                for (unsigned int k=0; k<4; k++)
                                {
                                        m_kDervalu.clear();
                                        m_kDervalv.clear();//m_val.clear();
                                        //-------
                                        basis * P_basis=P_Bases->Return_Basis_Cell(i_v, k, i, P_HVertices, *P_FEMMesh);// Return_Basis_Cell(unsigned int Index_Vertex, unsigned int i_H, unsigned int Index_Cell, const vector<vector<HermiteVertex *> > &P_HVertices, const Mesh & CurrentMesh);
                                        P_basis->Evaluation(1, 0, Pcoordinate_Ps, i, m_kDervalu, *P_FEMMesh);
                                        P_basis->Evaluation(0, 1, Pcoordinate_Ps, i, m_kDervalv, *P_FEMMesh);

                                        vector<double> detJ;
                                        ParametricMapSigma_MSplineParametricMap(Pcoordinate_Ps, m_JPart, detJ , P_FEMMesh->P_Cells->at(i).ItsParent, ParametricMap, P_ParametricMesh,k1, k2, ExampleIndex);

                                        for (unsigned int kk=0; kk<4; kk++)
                                        {
                                                basis * P_basis2=P_Bases->Return_Basis_Cell(i_vertex, kk, i, P_HVertices, *P_FEMMesh);
                                                if(P_basis2->Index_basis>=P_basis->Index_basis)
                                                {
                                                        m_lDervalu.clear();
                                                        m_lDervalv.clear();
                                                        P_basis2->Evaluation(1, 0, Pcoordinate_Ps, i, m_lDervalu, *P_FEMMesh);
                                                        P_basis2->Evaluation(0, 1, Pcoordinate_Ps, i, m_lDervalv, *P_FEMMesh);
                                                        //-----------
                                                        m_temp = 0;
                                                        for(unsigned int l = 0; l < u_gaussnum*v_gaussnum; l++)
                                                        {
                                                                int nn = l / v_gaussnum;
                                                                int mm = l % v_gaussnum;


                                                                double xx1=m_kDervalu[l];
                                                                double yy1=m_kDervalv[l];
                                                                double xx2=m_lDervalu[l];
                                                                double yy2=m_lDervalv[l];
                                                                double J11=m_JPart[l][0];
                                                                double J12=m_JPart[l][1];
                                                                double J21=m_JPart[l][2];
                                                                double J22=m_JPart[l][3];
                                                                m_temp += w[nn][mm]*(xx1*J11*xx2+yy1*J21*xx2+xx1*J12*yy2+yy1*J22*yy2);
                                                        }
                                                        //coeff_matrix[][()-(P_basis->Index_basis)] +=
                                                        v=m_temp * (u1-u0) * (v1-v0) / 4;
                                                        I=P_basis->Index_basis;
                                                        J=P_basis2->Index_basis;

//                                                        MatSetValues(coeff_matrix, 1, &I, 1, &J, &v, ADD_VALUES);
//                                                        MatSetValues(coeff_matrix, 1, &J, 1, &I, &v, ADD_VALUES);
                                                        if(I!=J)
                                                        {
                                                            MatSetValues(coeff_matrix, 1, &I, 1, &J, &v, ADD_VALUES);
                                                            MatSetValues(coeff_matrix, 1, &J, 1, &I, &v, ADD_VALUES);
                                                        }
                                                        else
                                                        {
                                                            MatSetValues(coeff_matrix, 1, &I, 1, &J, &v, ADD_VALUES);
                                                        }

                                                }
                                                delete P_basis2;
                                                P_basis2=NULL;
                                        }

                                        delete P_basis;
                                        P_basis=NULL;
                                }
                                //=====
                        }
                }
                //=======================================
        }
        delete P_Bases;
        P_Bases=NULL;


}
 //======================================================================================
//void Generate_LoadVector(vector<double> &load_vect, vector<double> gausspt, vector<double> gaussw, const Mesh * P_FEMMesh, const Mesh * P_ParametricMesh, MSplineFunctions & MSplineParametricMap, unsigned int ExampleIndex)
//{
//	int gaussnum = gausspt.size();
//	vector<double> u,v;
//	u.resize(gaussnum);
//	v.resize(gaussnum);
//	for(unsigned int i=0;i<P_FEMMesh->P_Cells->size();i++)// Based on each cell
//	{
//		double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
//		double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
//		double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
//		double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];

//		for(int k = 0; k < gaussnum; k++)
//		{
//			u[k] = (x1+x0+(x1-x0)*gausspt[k]) * 0.5;
//			v[k] = (y0+y1+(y1-y0)*gausspt[k]) * 0.5;
//		}
//		//=============================================================
//		vector<double> funval, m_Baseval;
//		double tmpx;
//		vector<vector<double> > m_JPart;
//		//==============================================================
//		vector<Coordinate*> Pcoordinate_Ps;
//		for (int l=0; l<gaussnum; l++)
//		{
//			for (int j=0; j<gaussnum; j++)
//			{
//				Coordinate* CoorP=new Coordinate;
//				CoorP->xy[0]=u[l];
//				CoorP->xy[1]=v[j];
//				Pcoordinate_Ps.push_back(CoorP);
//			}
//		}
//		//===============================================================
//		MSplineFunctions * P_MSplineParametricMap= & MSplineParametricMap;
//        ComputeFunval_MSplineParametricMap(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent, funval, 1, *P_ParametricMesh, P_MSplineParametricMap,ExampleIndex);//The value of right side
//		//===============================================================


//		BasesCell *P_BC=new BasesCell(i, *P_FEMMesh);//Bases over the i-th cell

//		for(unsigned int k = 0; k < P_BC->P_Bases_Cell.size(); k++)
//		{
//			tmpx = 0;

//			vector<double> DetJ;
//			ParametricMapSigma_MSplineParametricMap(Pcoordinate_Ps, m_JPart, DetJ, P_FEMMesh->P_Cells->at(i).ItsParent, MSplineParametricMap, P_ParametricMesh);
//// 			//==============
//			m_Baseval.clear();

//			value_Cell_Hermite(0, 0, Pcoordinate_Ps, P_BC->P_Bases_Cell[k]->P_HermiteData_Cells[0], m_Baseval, *P_FEMMesh);

//			for(unsigned int j = 0; j < m_Baseval.size(); j++)
//			{
//				int nn = j / gaussnum;
//				int mm = j % gaussnum;
//				tmpx += gaussw[nn] * gaussw[mm] * funval[j] * m_Baseval[j] * DetJ[j];
//			}
//			load_vect[P_BC->P_Bases_Cell[k]->Index_basis] += tmpx * (x1-x0) * (y1-y0)/ 4;
//		}
//	}
//// 	for (unsigned int i=0; i<load_vect.size(); i++)
//// 	{
//// 		cout<<load_vect[i]<<" "<<endl;
//// 	}
//// 	cout<<endl;
//}

void Generate_LoadVector(Vec &load_vect, vector<double>& gausspt, vector<double>& gaussw, const Mesh * P_FEMMesh,
                         const Mesh * P_ParametricMesh, MSplineFunctions & MSplineParametricMap,
                         double k1, double k2, unsigned int ExampleIndex)
{
    //---------------------------------------------
    cout<<"Input the SubExampleIndex again:"<<endl;
    //
    unsigned int SubExampleIndex;
    cin>>SubExampleIndex;
    //---------------------------------------------

    int gaussnum = gausspt.size();

    VecZeroEntries(load_vect);

    PetscScalar v;
    PetscInt I;

    for(unsigned int i=0;i<P_FEMMesh->P_Cells->size();i++)// Based on each cell
    {
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];
        //=============================================================
        vector<double> funval, m_Baseval;
        double tmpx;
        vector<vector<double> > m_JPart;
        //==============================================================
        vector<Coordinate*> Pcoordinate_Ps;
        for (int l=0; l<gaussnum; l++)
        {
            for (int j=0; j<gaussnum; j++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=(x1+x0+(x1-x0)*gausspt[l]) * 0.5;//u[l];
                CoorP->xy[1]=(y0+y1+(y1-y0)*gausspt[j]) * 0.5;//v[j];
                Pcoordinate_Ps.push_back(CoorP);
            }
        }
        //===============================================================
        MSplineFunctions * P_MSplineParametricMap= & MSplineParametricMap;


        ComputeFunval_MSplineParametricMap(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent,
                                           funval, 1, *P_ParametricMesh, P_MSplineParametricMap,
                                           ExampleIndex);//The value of right side
        //===============================================================

        BasesCell *P_BC=new BasesCell(i, *P_FEMMesh);//Bases over the i-th cell

        for(unsigned int k = 0; k < P_BC->P_Bases_Cell.size(); k++)
        {
            tmpx = 0;

            vector<double> DetJ;

            if(ExampleIndex!=51)
            {
                ParametricMapSigma_MSplineParametricMap(Pcoordinate_Ps, m_JPart, DetJ,
                                                        P_FEMMesh->P_Cells->at(i).ItsParent, MSplineParametricMap,
                                                        P_ParametricMesh, k1, k2, ExampleIndex);
            }
            else
            {
                ParametricMapSigma(Pcoordinate_Ps,m_JPart, DetJ,k1,k2,ExampleIndex, SubExampleIndex);
            }

// 			//==============
            m_Baseval.clear();

            value_Cell_Hermite(0, 0, Pcoordinate_Ps, P_BC->P_Bases_Cell[k]->P_HermiteData_Cells[0], m_Baseval, *P_FEMMesh);

            for(unsigned int j = 0; j < m_Baseval.size(); j++)
            {
                int nn = j / gaussnum;
                int mm = j % gaussnum;
                tmpx += gaussw[nn] * gaussw[mm] * funval[j] * m_Baseval[j] * DetJ[j];
            }
            //load_vect[P_BC->P_Bases_Cell[k]->Index_basis] += tmpx * (x1-x0) * (y1-y0)/ 4;
            I=P_BC->P_Bases_Cell[k]->Index_basis;
            v=tmpx * (x1-x0) * (y1-y0)/ 4;

            VecSetValues(load_vect, 1, &I, &v, ADD_VALUES);
        }
    }
}


void Generate_LoadVector_PDE_PFEM(Vec &load_vect, vector<double>& gausspt, vector<double>& gaussw, const Mesh * P_FEMMesh,
                         const Mesh * P_ParametricMesh, MSplineFunctions & MSplineParametricMap,
                         double k1, double k2, unsigned int ExampleIndex, unsigned int EquationIndex)
{
    int gaussnum = gausspt.size();

    VecZeroEntries(load_vect);

    PetscScalar v;
    PetscInt I;

    for(unsigned int i=0;i<P_FEMMesh->P_Cells->size();i++)// Based on each cell
    {
        double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
        double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
        double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
        double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];
        //=============================================================
        vector<double> funval, m_Baseval;
        double tmpx;
        vector<vector<double> > m_JPart;
        //==============================================================
        vector<Coordinate*> Pcoordinate_Ps;
        for (int l=0; l<gaussnum; l++)
        {
            for (int j=0; j<gaussnum; j++)
            {
                Coordinate* CoorP=new Coordinate;
                CoorP->xy[0]=(x1+x0+(x1-x0)*gausspt[l]) * 0.5;//u[l];
                CoorP->xy[1]=(y0+y1+(y1-y0)*gausspt[j]) * 0.5;//v[j];
                Pcoordinate_Ps.push_back(CoorP);
            }
        }
        //===============================================================
        MSplineFunctions * P_MSplineParametricMap= & MSplineParametricMap;

        //The value of right side
        ComputeFunval_MSplineParametricMap_PDE_PFEM(Pcoordinate_Ps, P_FEMMesh->P_Cells->at(i).ItsParent,
                                                    funval, 1, *P_ParametricMesh, P_MSplineParametricMap,
                                                    ExampleIndex, EquationIndex);

        //===============================================================

        //=========================

        BasesCell *P_BC=new BasesCell(i, *P_FEMMesh);//Bases over the i-th cell

        for(unsigned int k = 0; k < P_BC->P_Bases_Cell.size(); k++)
        {
            tmpx = 0;

            vector<double> DetJ;
            ParametricMapSigma_MSplineParametricMap(Pcoordinate_Ps, m_JPart, DetJ, P_FEMMesh->P_Cells->at(i).ItsParent, MSplineParametricMap, P_ParametricMesh, k1, k2, ExampleIndex);
// 			//==============
            m_Baseval.clear();

            value_Cell_Hermite(0, 0, Pcoordinate_Ps, P_BC->P_Bases_Cell[k]->P_HermiteData_Cells[0], m_Baseval, *P_FEMMesh);

            for(unsigned int j = 0; j < m_Baseval.size(); j++)
            {
                int nn = j / gaussnum;
                int mm = j % gaussnum;
                tmpx += gaussw[nn] * gaussw[mm] * funval[j] * m_Baseval[j] * DetJ[j];
            }
            //load_vect[P_BC->P_Bases_Cell[k]->Index_basis] += tmpx * (x1-x0) * (y1-y0)/ 4;
            I=P_BC->P_Bases_Cell[k]->Index_basis;
            v=tmpx * (x1-x0) * (y1-y0)/ 4;

            VecSetValues(load_vect, 1, &I, &v, ADD_VALUES);
        }
    }
}

// The following functions for mspP_FEM


/*-------------------------------------------------------------

---------------------------------------------------------------*/

void GenerateCoeffMatrix_RefinedParametrization(unsigned int u_gaussnum, unsigned int v_gaussnum, Mat &coeff_matrix, vector<double> u_gausspt,
                                                vector<double> v_gausspt, double **w, const Mesh*  P_FEMMesh, const MSplineFunctions &RefinedParametricMap,
                                                double k1, double k2, unsigned int ExampleIndex)
{
        MatZeroEntries(coeff_matrix);

        //vector<double> u,v;//store Gauss points in a cell
        /*--Compute the stiff matrix(the upper triangular matrix)--*/
        vector<double> m_kDervalu;
        vector<double> m_kDervalv;
        vector<double> m_lDervalu;
        vector<double> m_lDervalv;
        vector<vector<double> > m_JPart;
        double m_temp;

        PetscInt I;
        PetscInt J;

        for(unsigned int i=0; i<P_FEMMesh->P_Cells->size(); i++)//fill in the coeff matrix based on cells
        {
                //===========Guass Points============================
                double u0=P_FEMMesh->P_Cells->at(i).CellSize[0];
                double u1=P_FEMMesh->P_Cells->at(i).CellSize[1];
                double v0=P_FEMMesh->P_Cells->at(i).CellSize[2];
                double v1=P_FEMMesh->P_Cells->at(i).CellSize[3];

                vector<Coordinate*> Pcoordinate_Ps;
                for (unsigned int j=0; j<u_gaussnum; j++)
                {
                        for (unsigned int l=0; l<v_gaussnum; l++)
                        {
                                Coordinate * CoorP=new Coordinate;
                                //CoorP->xy[0]=u[j];
                                CoorP->xy[0]=((u1-u0) * u_gausspt[j] + u0 + u1) * 0.5;
                                //CoorP->xy[1]=v[l];
                                CoorP->xy[1]=((v1-v0) * v_gausspt[l] + v1 + v0) * 0.5;
                                Pcoordinate_Ps.push_back(CoorP);
                                CoorP=NULL;
                        }
                }
                //============Bases over this cell===============================================
                BasesCell * P_BC=new BasesCell(i, *P_FEMMesh);//BasesCell(unsigned int index_Cell, const Mesh & CurrentMesh);

                for (unsigned int k=0; k<P_BC->P_Bases_Cell.size(); k++)//the FEM-bases over the i-th cells
                {
                        m_kDervalu.clear();
                        m_kDervalv.clear();//m_val.clear();
                        //==========================
                        value_Cell_Hermite(1, 0, Pcoordinate_Ps, P_BC->P_Bases_Cell[k]->P_HermiteData_Cells[0], m_kDervalu, *P_FEMMesh);
                        value_Cell_Hermite(0, 1, Pcoordinate_Ps, P_BC->P_Bases_Cell[k]->P_HermiteData_Cells[0], m_kDervalv, *P_FEMMesh);

                        vector<double> detJ;
                        ParametricMapSigma_MSplineParametricMap(Pcoordinate_Ps, m_JPart, detJ , i, RefinedParametricMap, P_FEMMesh, k1,k2,ExampleIndex);

                        //==========================
                        for(unsigned int kk = 0; kk < P_BC->P_Bases_Cell.size(); kk++)
                        {
                                if(P_BC->P_Bases_Cell[kk]->Index_basis>=P_BC->P_Bases_Cell[k]->Index_basis)
                                {
                                        m_lDervalu.clear();
                                        m_lDervalv.clear();
                                        value_Cell_Hermite(1, 0, Pcoordinate_Ps, P_BC->P_Bases_Cell[kk]->P_HermiteData_Cells[0], m_lDervalu, *P_FEMMesh);
                                        value_Cell_Hermite(0, 1, Pcoordinate_Ps, P_BC->P_Bases_Cell[kk]->P_HermiteData_Cells[0], m_lDervalv, *P_FEMMesh);

                                        //================================================================================
                                        m_temp = 0;
                                        for(unsigned int l = 0; l < u_gaussnum*v_gaussnum; l++)
                                        {
                                                int nn = l / v_gaussnum;
                                                int mm = l % v_gaussnum;


                                                double xx1=m_kDervalu[l];
                                                double yy1=m_kDervalv[l];
                                                double xx2=m_lDervalu[l];
                                                double yy2=m_lDervalv[l];
                                                double J11=m_JPart[l][0];
                                                double J12=m_JPart[l][1];
                                                double J21=m_JPart[l][2];
                                                double J22=m_JPart[l][3];

                                                m_temp += w[nn][mm]*(xx1*J11*xx2+yy1*J21*xx2+xx1*J12*yy2+yy1*J22*yy2);
                                        }
                                        I=P_BC->P_Bases_Cell[k]->Index_basis;
                                        J=P_BC->P_Bases_Cell[kk]->Index_basis;
                                        PetscScalar v=m_temp * (u1-u0) * (v1-v0) / 4;
                                        if(I!=J)
                                        {
                                                MatSetValues(coeff_matrix, 1, &I, 1, &J, &v, ADD_VALUES);
                                                MatSetValues(coeff_matrix, 1, &J, 1, &I, &v, ADD_VALUES);
                                        }
                                        else
                                        {
                                                MatSetValues(coeff_matrix, 1, &I, 1, &J, &v, ADD_VALUES);
                                        }
                                }
                        }
                }

                delete P_BC;
                P_BC=NULL;
        }


}

void GenerateLoadVector_RefinedParametrization(Vec &load_vect, vector<double> gausspt, vector<double> gaussw,
                                               const Mesh * P_FEMMesh, MSplineFunctions & RefinedParametricMap,
                                               double k1, double k2, unsigned int ExampleIndex)
{
        int gaussnum = gausspt.size();

        VecZeroEntries(load_vect);

        PetscScalar v;
        PetscInt I;

        for(unsigned int i=0;i<P_FEMMesh->P_Cells->size();i++)// Based on each cell
        {
                double x0=P_FEMMesh->P_Cells->at(i).CellSize[0];
                double x1=P_FEMMesh->P_Cells->at(i).CellSize[1];
                double y0=P_FEMMesh->P_Cells->at(i).CellSize[2];
                double y1=P_FEMMesh->P_Cells->at(i).CellSize[3];
                //=============================================================
                vector<double> funval, m_Baseval;
                double tmpx;
                vector<vector<double> > m_JPart;
                //==============================================================
                vector<Coordinate*> Pcoordinate_Ps;
                for (int l=0; l<gaussnum; l++)
                {
                        for (int j=0; j<gaussnum; j++)
                        {
                                Coordinate* CoorP=new Coordinate;
                                CoorP->xy[0]=(x1+x0+(x1-x0)*gausspt[l]) * 0.5;//u[l];
                                CoorP->xy[1]=(y0+y1+(y1-y0)*gausspt[j]) * 0.5;//v[j];
                                Pcoordinate_Ps.push_back(CoorP);
                        }
                }
                //===============================================================
                MSplineFunctions * P_RefinedParametricMap= & RefinedParametricMap;


                ComputeFunval_MSplineParametricMap(Pcoordinate_Ps, i, funval, 1, *P_FEMMesh, P_RefinedParametricMap, ExampleIndex);//The value of right side
                //===============================================================

                //=========================

                BasesCell *P_BC=new BasesCell(i, *P_FEMMesh);//Bases over the i-th cell

                for(unsigned int k = 0; k < P_BC->P_Bases_Cell.size(); k++)
                {
                        tmpx = 0;

                        vector<double> DetJ;
                        ParametricMapSigma_MSplineParametricMap(Pcoordinate_Ps, m_JPart, DetJ, i, RefinedParametricMap, P_FEMMesh, k1, k2, ExampleIndex);
                        //==============
                        m_Baseval.clear();

                        value_Cell_Hermite(0, 0, Pcoordinate_Ps, P_BC->P_Bases_Cell[k]->P_HermiteData_Cells[0], m_Baseval, *P_FEMMesh);

                        for(unsigned int j = 0; j < m_Baseval.size(); j++)
                        {
                                int nn = j / gaussnum;
                                int mm = j % gaussnum;
                                tmpx += gaussw[nn] * gaussw[mm] * funval[j] * m_Baseval[j] * DetJ[j];
                        }
                        //load_vect[P_BC->P_Bases_Cell[k]->Index_basis] += tmpx * (x1-x0) * (y1-y0)/ 4;
                        I=P_BC->P_Bases_Cell[k]->Index_basis;
                        v=tmpx * (x1-x0) * (y1-y0)/ 4;

                        VecSetValues(load_vect, 1, &I, &v, ADD_VALUES);
                }
        }
}


//===========================
/*
 * Return Coeff_Matrix such that it is the stiff matrix based on the P_ParametricMap over the mesh P_ParametricMesh
*/
//static void GenerateStiffMatrixWithHomogeneousBoundary
//            ( Mesh * P_ParametricMesh, const MSplineFunctions *P_ParametricMap,
//              ofstream &ofProgress,  unsigned int ExampleIndex, Mat &coeff_matrix)
//{
////    Mat * Pcoeff_Matrix;
//    Vec load_vect;
//    //The FEM Mesh
//    map<unsigned int, unsigned int> OrigialVertexToCurrentVertex;

//    //subdivide the initial parametric mesh
//    Mesh *P_FEMMesh=P_ParametricMesh->Subdivide_initialParametricMesh_Globally(0, OrigialVertexToCurrentVertex);

//    ofProgress<<"The Basis Number = "<<4*(P_FEMMesh->P_Vertices->size())<<endl;
//    //======================================================================
//    //Preparation: GuassPoint
//    unsigned int Gp=0;
//    cout<<"Input the number of Guass points:"<<endl;
//    cin>>Gp;

//    ofProgress<<"the number of Guass Points ="<<Gp<<endl;

//    unsigned int u_gaussnum=Gp, v_gaussnum=Gp;
//    unsigned int load_gaussnum=5;
//    vector<double> gausspt, m_IntePt, m_loadw;
//    //Weights/////////
//    double **w;
//     w=GaussPoints_Weights(u_gaussnum, load_gaussnum, gausspt, m_IntePt, m_loadw);

//    //==============================PETSC===================================
////    PetscInt m = P_FEMMesh->P_Vertices->size();
//    //PetscInt n=4;

////    MatCreate(PETSC_COMM_WORLD, &coeff_matrix);

////    MatSetSizes(coeff_matrix, PETSC_DECIDE, PETSC_DECIDE, m*4, m*4);
////    MatSetFromOptions(coeff_matrix);
////    MatMPIAIJSetPreallocation(coeff_matrix, 64, NULL, 64, NULL);
////    MatSeqAIJSetPreallocation(coeff_matrix, PETSC_DEFAULT, NULL);
////    MatSetUp(coeff_matrix);

////    PetscInitialize(&argc, &args, (char*) 0, NULL);
////    PetscOptionsGetInt(NULL, "-m", &m, NULL);
////    PetscOptionsGetInt(NULL, "-n", &n, NULL);



//   //=========================================================================

//    double k1=0;
//    double k2=0;

//    Generate_CoeffMatrix_Cell(u_gaussnum, v_gaussnum, coeff_matrix, m_IntePt, m_IntePt, w,
//                              P_FEMMesh, P_ParametricMesh, *P_ParametricMap, k1, k2, ExampleIndex);
//    //========================================================================

//    Initialized_loadVector(load_vect, P_FEMMesh->P_Vertices->size());
//    //Generate_LoadVector(load_vect, gausspt, m_loadw, P_FEMMesh, P_ParametricMesh, ParametricMap, k1, k2, ExampleIndex);
//    //
//    VecZeroEntries(load_vect);

//    //Set Bases type
//    cout<<"Set Bases type"<<endl;
//    vector<unsigned int> BasesType;
//    BasesType.resize(4*P_FEMMesh->P_Vertices->size(), 0);
//    Set_Bases_type(P_FEMMesh, BasesType);

//    //Modify LoadVector and CoeffMatrix
//    double C=0.0;

//    MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
//    MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);


//    map<unsigned int, set<unsigned int> > HTable;

//    Initialized_HTable_withoutHangingVertices_1(P_FEMMesh, HTable);

//    Modification_LoadVector_CoeffMatrix(C, BasesType, coeff_matrix, load_vect, HTable);

//    MatAssemblyBegin(coeff_matrix, MAT_FINAL_ASSEMBLY);
//    MatAssemblyEnd(coeff_matrix, MAT_FINAL_ASSEMBLY);
//    MatSetOption(coeff_matrix, MAT_SYMMETRIC, PETSC_TRUE);

//}


#endif // FEM_COEFFMATRIXLOADVECTOR_H
