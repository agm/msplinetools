#ifndef HERMITETRANSFERMATRIX_H
#define HERMITETRANSFERMATRIX_H

//---------------------------
class FrameTransferMatrix;
class HermiteData;
//---------------------------
class HermiteTransferMatrix
{
public: 
    /**
     * @brief [aij] is used to describe HermiteTransferMatrix
     */
    double a11, a22, a23, a32, a33, a44;
public:
    HermiteTransferMatrix():a11(1){};

    HermiteTransferMatrix(const FrameTransferMatrix & O);

    /**
     * @brief HermiteDataTransfer: generate a new Hermite data with this HermiteTransferMatrix*OriginalHermite
     * @param NewHermite
     * @param OriginalHermite
     */
    void HermiteDataTransfer(HermiteData &NewHermite, const HermiteData & OriginalHermite);
    void Cout_HermiteTransferMatrix();
};

#endif // HERMITETRANSFERMATRIX_H
