#ifndef PMAP_DYNAMICFUNCTIONS_H
#define PMAP_DYNAMICFUNCTIONS_H

#include <vector>
#include <string>

//msp
#include "msp/PointSet.h"
#include "msp/Coordinate.h"

//PMap
#include "msp/PMap_Functions.h"


using namespace std;
//==============================================================================================
//The following functions for mspP_FEM
//==============================================================================================

/*-------------------------------------------------------------
  Read the focused edges on the initial mesh
  OutPut: vector<pair <unsigned int, unsigned int> > &InitialEdges
    where InitialEdges[i].first and InitialEdges[i].second are the indices
    of vertices of the initial mesh
-------------------------------------------------------------*/
void REdges(string file, vector<pair <unsigned int, unsigned int> > &InitialEdges, vector<unsigned int> &EdgesIndexofCells)
{
    InitialEdges.clear();
    EdgesIndexofCells.clear();

    ifstream infile(file.c_str());

    if(infile == NULL)//Do not find this file
    {
        cout<< "There is no elements in Initial Mesh File:" << file <<endl;
    }
    else
    {
        unsigned int nE;
        infile >> nE;//the number of edges

        for(unsigned int i=0; i<nE; i++)
        {
            pair<unsigned int, unsigned int> Edge;
            infile >> Edge.first;
            infile >> Edge.second;
            InitialEdges.push_back(Edge);

            unsigned int cellindex;
            infile >> cellindex;
            EdgesIndexofCells.push_back(cellindex);
        }

    };
}
void ReadTheEdges(unsigned int ExampleIndex, vector<pair <unsigned int, unsigned int> > &InitialEdges, vector<unsigned int> &EdgesIndexofCells)
{
    InitialEdges.clear();
    EdgesIndexofCells.clear();

    switch(ExampleIndex)
    {

    case 4://Example 4
        REdges("/Users/wumeng/MyWork/msplinetools/data/Examples/EdgesForExamples/Example4/Edges.txt",InitialEdges, EdgesIndexofCells);

        break;

    case 15://==>is the same with case 25
        REdges("../data/Examples/EdgesForExamples/Example4-square/Edges.txt",InitialEdges, EdgesIndexofCells);
        break;

    case 24://Example 2_2
        REdges("../data/Examples/EdgesForExamples/Example2_2/Edges.txt",InitialEdges, EdgesIndexofCells);
        break;

    case 25://==>is the same with case 15
        REdges("../data/Examples/EdgesForExamples/Example4-square/Edges.txt",InitialEdges, EdgesIndexofCells);
        break;

    case 27://==>is the same with case 24
        REdges("../data/Examples/EdgesForExamples/Example2_2/k=1/Edges.txt",InitialEdges, EdgesIndexofCells);
        break;


    case 32:
        REdges("/Users/wumeng/MyWork/msplinetools/data/Examples/EdgesForExamples/Example7_1/Edges.txt",InitialEdges, EdgesIndexofCells);
        break;

    case 33:
        REdges("/Users/wumeng/MyWork/msplinetools/data/Examples/EdgesForExamples/Example7_0/Edges.txt",InitialEdges, EdgesIndexofCells);
        break;

    case 34://Example 34//
        REdges("/Users/wumeng/MyWork/msplinetools/data/Examples/EdgesForExamples/Example4/Edges.txt",InitialEdges, EdgesIndexofCells);
        break;

    case 35:
        REdges("/Users/wumeng/MyWork/msplinetools/data/Examples/EdgesForExamples/Example7_0/Edges.txt",InitialEdges, EdgesIndexofCells);
        break;

    default:
        cout<<"PMap_DynamicFunctions::ReadTheEdges:: ExampleIndex is out of the range!"<<endl;
        cin.get();

        break;
    };
    if(InitialEdges.size()==0)
    {
        cout<<"PMap_DynamicFunctions.h::ReadTheEdges::InitialEdges.size()=0!"<<endl;

        cin.get();

    }
}

/*--------------------------------------------------------------
  InPut:unsigned int ExampleIndex => Index of this example
        unsigned int IndexEdge => Index of the initial focused edge
        unsigned int NumPEdge => the number of points which we will take from the IndexEdge-th edge
  OutPut:
        vector<PointSet> Ps => the points on the initial mesh
        vector<Coordinate *> Qs => the points on the physical domain
----------------------------------------------------------------*/
void Example4PhysicalCurve(unsigned int IndexEdge, unsigned int NumPEdge, unsigned int IndexCell,
                           PointSet &Psi, vector<Coordinate *> &Qsi)
{
    Psi.Index_ItsCell=IndexCell;
    double dst=1.0/(NumPEdge-1.0);

    double Pi=3.14159265358979;

    switch(IndexEdge)
    {
    case 0://t 0-->1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=i*dst;
            Psi.P_coordinates_Ps.push_back(PCoorPs);

            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=cos(i*dst*Pi/8.0);
            PCoorPhyCur->xy[1]=sin(i*dst*Pi/8.0);
            Qsi.push_back(PCoorPhyCur);
        }

        break;
    case 1://s 1-->0
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0-i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);

            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=cos(i*dst*Pi/8.0+Pi/8.0);
            PCoorPhyCur->xy[1]=sin(i*dst*Pi/8.0+Pi/8.0);
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 2://t: 0-->1 s=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=i*dst;
            Psi.P_coordinates_Ps.push_back(PCoorPs);

            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=cos(i*dst*Pi/8.0+2.0*Pi/8.0);
            PCoorPhyCur->xy[1]=sin(i*dst*Pi/8.0+2.0*Pi/8.0);
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 3://s 1-->0 t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0-i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);

            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=cos(i*dst*Pi/8.0+3.0*Pi/8.0);
            PCoorPhyCur->xy[1]=sin(i*dst*Pi/8.0+3.0*Pi/8.0);
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 4://t 0-->1 s=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=i*dst;
            Psi.P_coordinates_Ps.push_back(PCoorPs);

            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=cos(i*dst*Pi/8.0+4.0*Pi/8.0);
            PCoorPhyCur->xy[1]=sin(i*dst*Pi/8.0+4.0*Pi/8.0);
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 5://s 1-->0 t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0-i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);

            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=cos(i*dst*Pi/8.0+5.0*Pi/8.0);
            PCoorPhyCur->xy[1]=sin(i*dst*Pi/8.0+5.0*Pi/8.0);
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 6://t 0-->1 s=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=i*dst;
            Psi.P_coordinates_Ps.push_back(PCoorPs);

            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=cos(i*dst*Pi/8.0+6.0*Pi/8.0);
            PCoorPhyCur->xy[1]=sin(i*dst*Pi/8.0+6.0*Pi/8.0);
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 7://s 1-->0 t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0-i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);

            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=cos(i*dst*Pi/8.0+7.0*Pi/8.0);
            PCoorPhyCur->xy[1]=sin(i*dst*Pi/8.0+7.0*Pi/8.0);
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 8://t 0-->1 s=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=i*dst;
            Psi.P_coordinates_Ps.push_back(PCoorPs);

            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=cos(i*dst*Pi/8.0+8.0*Pi/8.0);
            PCoorPhyCur->xy[1]=sin(i*dst*Pi/8.0+8.0*Pi/8.0);
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 9://s 1-->0 t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0-i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);

            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=cos(i*dst*Pi/8.0+9.0*Pi/8.0);
            PCoorPhyCur->xy[1]=sin(i*dst*Pi/8.0+9.0*Pi/8.0);
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 10://t 0-->1 s=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=i*dst;
            Psi.P_coordinates_Ps.push_back(PCoorPs);

            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=cos(i*dst*Pi/8.0+10.0*Pi/8.0);
            PCoorPhyCur->xy[1]=sin(i*dst*Pi/8.0+10.0*Pi/8.0);
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 11://s 1-->0 t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0-i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);

            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=cos(i*dst*Pi/8.0+11.0*Pi/8.0);
            PCoorPhyCur->xy[1]=sin(i*dst*Pi/8.0+11.0*Pi/8.0);
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 12://t 0-->1 s=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=i*dst;
            Psi.P_coordinates_Ps.push_back(PCoorPs);

            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=cos(i*dst*Pi/8.0+12.0*Pi/8.0);
            PCoorPhyCur->xy[1]=sin(i*dst*Pi/8.0+12.0*Pi/8.0);
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 13://s 1-->0 t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0-i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);

            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=cos(i*dst*Pi/8.0+13.0*Pi/8.0);
            PCoorPhyCur->xy[1]=sin(i*dst*Pi/8.0+13.0*Pi/8.0);
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 14://t 0-->1 s=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=i*dst;
            Psi.P_coordinates_Ps.push_back(PCoorPs);

            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=cos(i*dst*Pi/8.0+14.0*Pi/8.0);
            PCoorPhyCur->xy[1]=sin(i*dst*Pi/8.0+14.0*Pi/8.0);
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 15://s 1-->0 t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0-i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);

            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=cos(i*dst*Pi/8.0+15.0*Pi/8.0);
            PCoorPhyCur->xy[1]=sin(i*dst*Pi/8.0+15.0*Pi/8.0);
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    default:
        cout<<"Pease check the index of edge"<<endl;
        cin.get();
        break;
    }
}

void Example2_2PhysicalCurve_1(unsigned int IndexEdge,
                             unsigned int NumPEdge, unsigned int IndexCell,
                             PointSet &Psi, vector<Coordinate *> &Qsi)
{
    Psi.Index_ItsCell=IndexCell;
    Psi.P_coordinates_Ps.clear();

    double dst=1.0/(NumPEdge-1.0);

    double Pi=3.14159265358979;

    vector<double> QcurveParameters;//From 0.0 --> 1.0

    switch(IndexEdge)
    {
    case 0://s 0-->1 t=1, x=2.0-->1.0, y=sqrt(2+x*(x-1)^2)
        //V16->V15

        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=2.0-i*dst;
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);

        }
        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0.0 --> 1.0 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=QcurveParameters[i];
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;

    case 1://s 0-->0.5 t=1, x=1.0-->0.5, y=sqrt(2+x*(x-1)^2)
        //V15->V14
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=1.0-0.5*i*dst;
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0.0 --> 1.0 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=QcurveParameters[i]*0.5;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;

    case 2://s 0-->0.5 t=1, x=0.5-->-0.4, y=sqrt(2+x*(x-1)^2)
        //V14->V30
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=0.5-0.9*i*dst;
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0.0 --> 0.5 is different from QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=QcurveParameters[i]*0.5;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;

    case 3://s 0-->0.5 t=1, x=-0.4-->-0.6956, y=sqrt(2+x*(x-1)^2)
        //V30 V13
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=-0.4-(0.6956-0.4)*i*dst;
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0.0 --> 0.5 is different from QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=QcurveParameters[i]*0.5;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;

    case 4://s 0-->0.5 t=1, x=-0.6956-->-0.4, y=-sqrt(2+x*(x-1)^2)
        //V13 V31
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=-0.6956+(0.6956-0.4)*i*dst;
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=-sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0.0 --> 0.5 is different from QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=QcurveParameters[i]*0.5;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;

    case 5://s 0-->0.5 t=1, x=-0.4-->0.5, y=-sqrt(2+x*(x-1)^2)
        //V31->V12
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=-0.4+(0.5+0.4)*i*dst;
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=-sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0.0 --> 0.5 is different from QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=QcurveParameters[i]*0.5;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }

        break;

    case 6://s 0-->0.5 t=1, x=0.5-->1.0, y=-sqrt(2+x*(x-1)^2)
        //V12 V11
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=0.5+0.5*i*dst;
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=-sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->1 is different from QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=QcurveParameters[i]*0.5;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;

    case 7://s 0-->1 t=1, x=1-->2, y=-sqrt(2+x*(x-1)^2)
        //V11 V10
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=1.0+i*dst;
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=-sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->1 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=QcurveParameters[i];
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }

        break;


    case 8://s 0-->1 t=0, r=pi/2-->pi,
           //x=2+0.5*cos(r), y=0.5*sin(r)
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double r=Pi/2.0+(Pi-Pi/2.0)*i*dst;
            PCoorPhyCur->xy[0]=2.0+0.5*cos(r);
            PCoorPhyCur->xy[1]=0.5*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->1 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=QcurveParameters[i];
            PCoorPs->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }

        break;



    case 15://s 0-->1 t=0, r=pi-->3*pi/2,
           //x=2+0.5*cos(r), y=0.5*sin(r)
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double r=Pi+(3.0*Pi/2.0-Pi)*i*dst;
            PCoorPhyCur->xy[0]=2.0+0.5*cos(r);
            PCoorPhyCur->xy[1]=0.5*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->1 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=QcurveParameters[i];
            PCoorPs->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }

        break;
 //------------------------------------------------------------------

    case 9://s 0-->0.5 t=0, r=0-->pi/2,
           //x=0.5+sqrt(2.0)*cos(r)/10.0, y=(sqrt(2.0)/10.0)*sin(r)
        //V26 V23
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double r=Pi/2.0*i*dst;
            PCoorPhyCur->xy[0]=0.5+(sqrt(2.0)/10.0)*cos(r);
            PCoorPhyCur->xy[1]=(sqrt(2.0)/10.0)*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->1 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=QcurveParameters[i]*0.5;
            PCoorPs->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }

        break;


    case 10://s 0-->0.5 t=0, r=pi/2-->3pi/4,
           //x=0.5+(sqrt(2.0)/10.0)*cos(r), y=(sqrt(2.0)/10.0)*sin(r)
        //V23 V27
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double r=Pi/2.0+(3.0*Pi/4.0-Pi/2.0)*i*dst;
            PCoorPhyCur->xy[0]=0.5+(sqrt(2.0)/10.0)*cos(r);
            PCoorPhyCur->xy[1]=(sqrt(2.0)/10.0)*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is different from QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=QcurveParameters[i]*0.5;
            PCoorPs->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }

        break;


    case 11://s 0-->0.5 t=0, r=3pi/4-->pi,
           //x=0.5+(sqrt(2.0)/10.0)*cos(r), y=(sqrt(2.0)/10.0)*sin(r)
        //V27 V24
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double r=3.0*Pi/4.0+(Pi-3.0*Pi/4.0)*i*dst;
            PCoorPhyCur->xy[0]=0.5+(sqrt(2.0)/10.0)*cos(r);
            PCoorPhyCur->xy[1]=(sqrt(2.0)/10.0)*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is different from QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=QcurveParameters[i]*0.5;
            PCoorPs->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;


    case 12://s 0-->0.5 t=0, r=pi-->5pi/4,
           //x=0.5+0.1414*cos(r), y=0.1414*sin(r)
        //V24 V34
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double r=5.0*Pi/4.0+(5.0*Pi/4.0-Pi)*i*dst;
            PCoorPhyCur->xy[0]=0.5+(sqrt(2.0)/10.0)*cos(r);
            PCoorPhyCur->xy[1]=(sqrt(2.0)/10.0)*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is different from QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=QcurveParameters[i]*0.5;
            PCoorPs->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;
    case 13://s 0-->0.5 t=0, r=5pi/4-->6pi/4,
           //x=0.5+0.1414*cos(r), y=0.1414*sin(r)
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double r=5.0*Pi/4.0+(6.0*Pi/4.0-5.0*Pi/4.0)*i*dst;
            PCoorPhyCur->xy[0]=0.5+(sqrt(2.0)/10.0)*cos(r);
            PCoorPhyCur->xy[1]=(sqrt(2.0)/10.0)*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is different from QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=QcurveParameters[i]*0.5;
            PCoorPs->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;
    case 14://s 0-->0.5 t=0, r=6pi/4-->2pi,
           //x=0.5+0.1414*cos(r), y=0.1414*sin(r)
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double r=6.0*Pi/4.0+(2.0*Pi-6.0*Pi/4.0)*i*dst;
            PCoorPhyCur->xy[0]=0.5+(sqrt(2.0)/10.0)*cos(r);
            PCoorPhyCur->xy[1]=(sqrt(2.0)/10.0)*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is different from QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=QcurveParameters[i]*0.5;
            PCoorPs->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;
//------------------------------------------------------------------
    case 16://s=0 t=0-->1
           //x=2, y=1.7122+(2.0000-1.7122)*i*dst
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double y=1.7122+(2.0000-1.7122)*i*dst;
            PCoorPhyCur->xy[0]=2.0000;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=0.0;
            PCoorPs->xy[1]=QcurveParameters[i];
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;

    case 17://s=0 t=0-->1
           //x=2, y=1.3990+(1.7122-1.3990)*i*dst
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double y=1.3990+(1.7122-1.3990)*i*dst;
            PCoorPhyCur->xy[0]=2.0000;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=0.0;
            PCoorPs->xy[1]=QcurveParameters[i];
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;

    case 18://s=0 t=0-->1
           //x=2, y=0.4989+(1.3990-0.4989)*i*dst
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double y=0.4989+(1.3990-0.4989)*i*dst;
            PCoorPhyCur->xy[0]=2.0000;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=0.0;
            PCoorPs->xy[1]=QcurveParameters[i];
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;

    case 19://s=1 t=0-->1
           //x=2, y=-1.7364+(-2.0+1.7364)*i*dst
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double y=-1.7364+(-2.0+1.7364)*i*dst;
            PCoorPhyCur->xy[0]=2.0000;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=QcurveParameters[i];
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;

    case 20://s=1 t=0-->1
           //x=2, y=-1.4160+(-1.7364+1.4160)*i*dst
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double y=-1.4160+(-1.7364+1.4160)*i*dst;
            PCoorPhyCur->xy[0]=2.0000;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=QcurveParameters[i];
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;


    case 21://s=1 t=0-->1
           //x=2, y=-0.4983+(-1.4160+0.4983)*i*dst
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double y=-0.4983+(-1.4160+0.4983)*i*dst;
            PCoorPhyCur->xy[0]=2.0000;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=QcurveParameters[i];
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;


    default:
        cout<<"Pease check the index of edge"<<endl;
        cin.get();
        break;
    }
}

void Example2_2PhysicalCurve_2(unsigned int IndexEdge,
                             unsigned int NumPEdge, unsigned int IndexCell,
                             MSplineFunctions * P_InitialMap, const Mesh * P_InitialMesh,
                             PointSet &Psi, vector<Coordinate *> &Qsi)
{
    Psi.Index_ItsCell=IndexCell;
    Psi.P_coordinates_Ps.clear();

    double dst=1.0/(NumPEdge-1.0);

    double Pi=3.14159265358979;

    vector<vector<double> > Values;
    vector<double> QcurveParameters;

    switch(IndexEdge)
    {
    case 0://s 0-->1 t=1, x=2.0-->1.0, y=sqrt(2+x*(x-1)^2)
        //V16->V15
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst;
            PCoor->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoor);
        }

        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }

        break;

    case 1://s 0-->0.5 t=1, x=1.0-->0.5, y=sqrt(2+x*(x-1)^2)
        //V15->V14
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst*0.5;
            PCoor->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoor);
        }


        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }

        break;

    case 2://s 0-->0.5 t=1, x=0.5-->-0.4, y=sqrt(2+x*(x-1)^2)
        //V14->V30
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst*0.5;
            PCoor->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoor);
        }


        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }
        break;

    case 3://s 0-->0.5 t=1, x=-0.4-->-0.6956, y=sqrt(2+x*(x-1)^2)
        //V30 V13
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst*0.5;
            PCoor->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoor);
        }


        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }

        break;

    case 4://s 0-->0.5 t=1, x=-0.6956-->-0.4, y=-sqrt(2+x*(x-1)^2)
        //V13 V31
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst*0.5;
            PCoor->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoor);
        }


        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=-sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }

        break;

    case 5://s 0-->0.5 t=1, x=-0.4-->0.5, y=-sqrt(2+x*(x-1)^2)
        //V31->V12
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst*0.5;
            PCoor->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoor);
        }


        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=-sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }
        break;

    case 6://s 0-->0.5 t=1, x=0.5-->1.0, y=-sqrt(2+x*(x-1)^2)
        //V12 V11
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst*0.5;
            PCoor->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoor);
        }


        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=-sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }

        break;

    case 7://s 0-->1 t=1, x=1-->2, y=-sqrt(2+x*(x-1)^2)
        //V11 V10
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst;
            PCoor->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoor);
        }


        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=-sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }

        break;


    case 8://s 0-->1 t=0, r=pi/2-->pi,
           //x=2+0.5*cos(r), y=0.5*sin(r)
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst;
            PCoor->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoor);

            double r=Pi/2.0+(Pi-Pi/2.0)*i*dst;
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=2.0+0.5*cos(r);
            PCoorPhyCur->xy[1]=0.5*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }


//        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

//        for(unsigned int i=0; i<NumPEdge; i++)
//        {
//            //Qsi
//            Coordinate * PCoorPhyCur = new Coordinate;
//            double x=Values[0][i];
//            PCoorPhyCur->xy[0]=x;
//            PCoorPhyCur->xy[1]=sqrt(0.25-(x-2.0)*(x-2.0));
//            Qsi.push_back(PCoorPhyCur);
//        }

        break;



    case 15://s 0-->1 t=0, r=pi-->3*pi/2,
           //x=2+0.5*cos(r), y=0.5*sin(r)
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst;
            PCoor->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoor);

            double r=Pi+(3.0*Pi/2.0-Pi)*i*dst;
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=2.0+0.5*cos(r);
            PCoorPhyCur->xy[1]=0.5*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }


//        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

//        for(unsigned int i=0; i<NumPEdge; i++)
//        {
//            //Qsi
//            Coordinate * PCoorPhyCur = new Coordinate;
//            double x=Values[0][i];
//            PCoorPhyCur->xy[0]=x;
//            PCoorPhyCur->xy[1]=-sqrt(0.25-(x-2.0)*(x-2.0));;
//            Qsi.push_back(PCoorPhyCur);
//        }
        break;
 //------------------------------------------------------------------

    case 9://s 0-->0.5 t=0, r=0-->pi/2,
           //x=0.5+sqrt(2.0)*cos(r)/10.0, y=(sqrt(2.0)/10.0)*sin(r)
        //V26 V23
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst*0.5;
            PCoor->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoor);

            double r=i*dst*Pi/2.0;
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=0.5+sqrt(2.0)*cos(r)/10.0;
            PCoorPhyCur->xy[1]=(sqrt(2.0)/10.0)*sin(r);
            Qsi.push_back(PCoorPhyCur);

        }


//        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

//        for(unsigned int i=0; i<NumPEdge; i++)
//        {
//            //Qsi
//            Coordinate * PCoorPhyCur = new Coordinate;
//            double x=Values[0][i];
//            PCoorPhyCur->xy[0]=x;
//            PCoorPhyCur->xy[1]=sqrt(0.02-(x-0.5)*(x-0.5));
//            Qsi.push_back(PCoorPhyCur);
//        }

        break;


    case 10://s 0-->0.5 t=0, r=pi/2-->3pi/4,
           //x=0.5+(sqrt(2.0)/10.0)*cos(r), y=(sqrt(2.0)/10.0)*sin(r)
        //V23 V27
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst*0.5;
            PCoor->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoor);

            double r=Pi/2.0+(3.0*Pi/4.0-Pi/2.0)*i*dst;
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=0.5+sqrt(2.0)*cos(r)/10.0;
            PCoorPhyCur->xy[1]=(sqrt(2.0)/10.0)*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }


//        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

//        for(unsigned int i=0; i<NumPEdge; i++)
//        {
//            //Qsi
//            Coordinate * PCoorPhyCur = new Coordinate;
//            double x=Values[0][i];
//            PCoorPhyCur->xy[0]=x;
//            PCoorPhyCur->xy[1]=sqrt(0.02-(x-0.5)*(x-0.5));
//            Qsi.push_back(PCoorPhyCur);
//        }

        break;


    case 11://s 0-->0.5 t=0, r=3pi/4-->pi,
           //x=0.5+(sqrt(2.0)/10.0)*cos(r), y=(sqrt(2.0)/10.0)*sin(r)
        //V27 V24
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst*0.5;
            PCoor->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoor);

            double r=3.0*Pi/4.0+(Pi-3.0*Pi/4.0)*i*dst;
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=0.5+sqrt(2.0)*cos(r)/10.0;
            PCoorPhyCur->xy[1]=(sqrt(2.0)/10.0)*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }


//        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

//        for(unsigned int i=0; i<NumPEdge; i++)
//        {
//            //Qsi
//            Coordinate * PCoorPhyCur = new Coordinate;
//            double x=Values[0][i];
//            PCoorPhyCur->xy[0]=x;
//            PCoorPhyCur->xy[1]=sqrt(0.02-(x-0.5)*(x-0.5));
//            Qsi.push_back(PCoorPhyCur);
//        }
        break;


    case 12://s 0-->0.5 t=0, r=pi-->5pi/4,
           //x=0.5+0.1414*cos(r), y=0.1414*sin(r)
        //V24 V34
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst*0.5;
            PCoor->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoor);

            double r=Pi+(5.0*Pi/4.0-Pi)*i*dst;
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=0.5+sqrt(2.0)*cos(r)/10.0;
            PCoorPhyCur->xy[1]=(sqrt(2.0)/10.0)*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }


//        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

//        for(unsigned int i=0; i<NumPEdge; i++)
//        {
//            //Qsi
//            Coordinate * PCoorPhyCur = new Coordinate;
//            double x=Values[0][i];
//            PCoorPhyCur->xy[0]=x;
//            PCoorPhyCur->xy[1]=-sqrt(0.02-(x-0.5)*(x-0.5));
//            Qsi.push_back(PCoorPhyCur);
//        }
        break;
    case 13://s 0-->0.5 t=0, r=5pi/4-->6pi/4,
           //x=0.5+0.1414*cos(r), y=0.1414*sin(r)
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst*0.5;
            PCoor->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoor);

            double r=5.0*Pi/4.0+(6.0*Pi/4.0-5.0*Pi/4.0)*i*dst;
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=0.5+sqrt(2.0)*cos(r)/10.0;
            PCoorPhyCur->xy[1]=(sqrt(2.0)/10.0)*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }


//        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

//        for(unsigned int i=0; i<NumPEdge; i++)
//        {
//            //Qsi
//            Coordinate * PCoorPhyCur = new Coordinate;
//            double x=Values[0][i];
//            PCoorPhyCur->xy[0]=x;
//            PCoorPhyCur->xy[1]=-sqrt(0.02-(x-0.5)*(x-0.5));
//            Qsi.push_back(PCoorPhyCur);
//        }
        break;
    case 14://s 0-->0.5 t=0, r=6pi/4-->2pi,
           //x=0.5+0.1414*cos(r), y=0.1414*sin(r)
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst*0.5;
            PCoor->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoor);

            double r=6.0*Pi/4.0+(2.0*Pi-6.0*Pi/4.0)*i*dst;
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=0.5+sqrt(2.0)*cos(r)/10.0;
            PCoorPhyCur->xy[1]=(sqrt(2.0)/10.0)*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }


//        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

//        for(unsigned int i=0; i<NumPEdge; i++)
//        {
//            //Qsi
//            Coordinate * PCoorPhyCur = new Coordinate;
//            double x=Values[0][i];
//            PCoorPhyCur->xy[0]=x;
//            PCoorPhyCur->xy[1]=-sqrt(0.02-(x-0.5)*(x-0.5));
//            Qsi.push_back(PCoorPhyCur);
//        }
        break;
//------------------------------------------------------------------
    case 16://s=0 t=0-->1
           //x=2, y=1.7122+(2.0000-1.7122)*i*dst
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double y=1.7122+(2.0000-1.7122)*i*dst;
            PCoorPhyCur->xy[0]=2.0000;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=0.0;
            PCoorPs->xy[1]=QcurveParameters[i];
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;

    case 17://s=0 t=0-->1
           //x=2, y=1.3990+(1.7122-1.3990)*i*dst
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double y=1.3990+(1.7122-1.3990)*i*dst;
            PCoorPhyCur->xy[0]=2.0000;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=0.0;
            PCoorPs->xy[1]=QcurveParameters[i];
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;

    case 18://s=0 t=0-->1
           //x=2, y=0.4989+(1.3990-0.4989)*i*dst
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double y=0.4989+(1.3990-0.4989)*i*dst;
            PCoorPhyCur->xy[0]=2.0000;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=0.0;
            PCoorPs->xy[1]=QcurveParameters[i];
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;

    case 19://s=1 t=0-->1
           //x=2, y=-1.7364+(-2.0+1.7364)*i*dst
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double y=-1.7364+(-2.0+1.7364)*i*dst;
            PCoorPhyCur->xy[0]=2.0000;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=QcurveParameters[i];
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;

    case 20://s=1 t=0-->1
           //x=2, y=-1.4160+(-1.7364+1.4160)*i*dst
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double y=-1.4160+(-1.7364+1.4160)*i*dst;
            PCoorPhyCur->xy[0]=2.0000;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=QcurveParameters[i];
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;


    case 21://s=1 t=0-->1
           //x=2, y=-0.4983+(-1.4160+0.4983)*i*dst
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double y=-0.4983+(-1.4160+0.4983)*i*dst;
            PCoorPhyCur->xy[0]=2.0000;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=QcurveParameters[i];
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;


    default:
        cout<<"Pease check the index of edge"<<endl;
        cin.get();
        break;
    }
}
//======
void Example2_2PhysicalCurve_27(unsigned int IndexEdge,
                             unsigned int NumPEdge, unsigned int IndexCell,
                             MSplineFunctions * P_InitialMap, const Mesh * P_InitialMesh,
                             PointSet &Psi, vector<Coordinate *> &Qsi)
{
    Psi.Index_ItsCell=IndexCell;
    Psi.P_coordinates_Ps.clear();

    double dst=0.5/(NumPEdge-1.0);

    double Pi=3.14159265358979;

    vector<vector<double> > Values;
    vector<double> QcurveParameters;

    switch(IndexEdge)
    {
    case 0://s 0-->1 t=1, x=2.0-->1.0, y=sqrt(2+x*(x-1)^2)
        //V16->V15
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst;
            PCoor->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoor);
        }

        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }

        break;
    case 1://s 0-->1 t=1, x=2.0-->1.0, y=sqrt(2+x*(x-1)^2)
        //V16->V15
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst+0.5;
            PCoor->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoor);
        }

        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }

        break;

    case 2://s 0--> t=1, x=1.0-->0.5, y=sqrt(2+x*(x-1)^2)
        //V15->V14
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst;
            PCoor->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoor);
        }


        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }

        break;
    case 3://s 0--> t=1, x=1.0-->0.5, y=sqrt(2+x*(x-1)^2)
        //V15->V14
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst+0.50;
            PCoor->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoor);
        }


        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }

        break;


    case 4://s 0--> t=1, x=0.5-->-0.4, y=sqrt(2+x*(x-1)^2)
        //V14->V30
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst;
            PCoor->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoor);
        }


        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 5://s 0--> t=1, x=0.5-->-0.4, y=sqrt(2+x*(x-1)^2)
        //V14->V30
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst+0.5;
            PCoor->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoor);
        }


        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }
        break;


    case 6://s 0--> t=1, x=-0.4-->-0.6956, y=sqrt(2+x*(x-1)^2)
        //V30 V13
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst;
            PCoor->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoor);
        }


        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }

        break;
    case 7://s 0--> t=1, x=-0.4-->-0.6956, y=sqrt(2+x*(x-1)^2)
        //V30 V13
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst+0.50;
            PCoor->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoor);
        }


        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }

        break;

    case 8://s 0--> t=1, x=-0.6956-->-0.4, y=-sqrt(2+x*(x-1)^2)
        //V13 V31
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst;
            PCoor->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoor);
        }


        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=-sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }

        break;

    case 9://s 0--> t=1, x=-0.6956-->-0.4, y=-sqrt(2+x*(x-1)^2)
        //V13 V31
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst+0.5;
            PCoor->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoor);
        }


        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=-sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }

        break;

    case 10://s 0--> t=1, x=-0.4-->0.5, y=-sqrt(2+x*(x-1)^2)
        //V31->V12
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst;
            PCoor->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoor);
        }


        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=-sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }
        break;

    case 11://s 0--> t=1, x=-0.4-->0.5, y=-sqrt(2+x*(x-1)^2)
        //V31->V12
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst+0.5;
            PCoor->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoor);
        }


        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=-sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }
        break;

    case 12://s 0--> t=1, x=0.5-->1.0, y=-sqrt(2+x*(x-1)^2)
        //V12 V11
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst;
            PCoor->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoor);
        }


        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=-sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }

        break;

    case 13://s 0--> t=1, x=0.5-->1.0, y=-sqrt(2+x*(x-1)^2)
        //V12 V11
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst+0.5;
            PCoor->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoor);
        }


        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=-sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }

        break;

    case 14://s 0-->1 t=1, x=1-->2, y=-sqrt(2+x*(x-1)^2)
        //V11 V10
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst;
            PCoor->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoor);
        }


        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=-sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }

        break;

    case 15://s 0-->1 t=1, x=1-->2, y=-sqrt(2+x*(x-1)^2)
        //V11 V10
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst+0.5;
            PCoor->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoor);
        }


        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);

        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double x=Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=-sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }

        break;


    case 16://s 0-->1 t=0, r=pi/2-->pi,
           //x=2+0.5*cos(r), y=0.5*sin(r)
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst;
            PCoor->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoor);

            double r=Pi/2.0+(Pi-Pi/2.0)*i*dst;
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=2.0+0.5*cos(r);
            PCoorPhyCur->xy[1]=0.5*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }

        break;
    case 17://s 0-->1 t=0, r=pi/2-->pi,
           //x=2+0.5*cos(r), y=0.5*sin(r)
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst+0.5;
            PCoor->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoor);

            double r=Pi/2.0+(Pi-Pi/2.0)*(i*dst+0.5);
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=2.0+0.5*cos(r);
            PCoorPhyCur->xy[1]=0.5*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }

        break;


//-------------------------------------------------------------------
    case 30://s 0-->1 t=0, r=pi-->3*pi/2,
           //x=2+0.5*cos(r), y=0.5*sin(r)
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst;
            PCoor->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoor);

            double r=Pi+(3.0*Pi/2.0-Pi)*i*dst;
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=2.0+0.5*cos(r);
            PCoorPhyCur->xy[1]=0.5*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }

        break;
    case 31://s 0-->1 t=0, r=pi-->3*pi/2,
           //x=2+0.5*cos(r), y=0.5*sin(r)
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst+0.5;
            PCoor->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoor);

            double r=Pi+(3.0*Pi/2.0-Pi)*(i*dst+0.5);
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=2.0+0.5*cos(r);
            PCoorPhyCur->xy[1]=0.5*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }

        break;
 //------------------------------------------------------------------

    case 18://s 0--> t=0, r=0-->pi/2,
           //x=0.5+sqrt(2.0)*cos(r)/10.0, y=(sqrt(2.0)/10.0)*sin(r)
        //V26 V23
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst;
            PCoor->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoor);

            double r=i*dst*Pi/2.0;
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=0.5+sqrt(2.0)*cos(r)/10.0;
            PCoorPhyCur->xy[1]=(sqrt(2.0)/10.0)*sin(r);
            Qsi.push_back(PCoorPhyCur);

        }
        break;

    case 19://s 0--> t=0, r=0-->pi/2,
           //x=0.5+sqrt(2.0)*cos(r)/10.0, y=(sqrt(2.0)/10.0)*sin(r)
        //V26 V23
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst+0.5;
            PCoor->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoor);

            double r=(i*dst+0.5)*Pi/2.0;
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=0.5+sqrt(2.0)*cos(r)/10.0;
            PCoorPhyCur->xy[1]=(sqrt(2.0)/10.0)*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }

        break;

    case 20://s 0--> t=0, r=pi/2-->3pi/4,
           //x=0.5+(sqrt(2.0)/10.0)*cos(r), y=(sqrt(2.0)/10.0)*sin(r)
        //V23 V27
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst;
            PCoor->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoor);

            double r=Pi/2.0+(3.0*Pi/4.0-Pi/2.0)*i*dst;
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=0.5+sqrt(2.0)*cos(r)/10.0;
            PCoorPhyCur->xy[1]=(sqrt(2.0)/10.0)*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }


        break;
    case 21://s 0--> t=0, r=pi/2-->3pi/4,
           //x=0.5+(sqrt(2.0)/10.0)*cos(r), y=(sqrt(2.0)/10.0)*sin(r)
        //V23 V27
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst+0.5;
            PCoor->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoor);

            double r=Pi/2.0+(3.0*Pi/4.0-Pi/2.0)*(i*dst+0.5);
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=0.5+sqrt(2.0)*cos(r)/10.0;
            PCoorPhyCur->xy[1]=(sqrt(2.0)/10.0)*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }


        break;


    case 22://s 0--> t=0, r=3pi/4-->pi,
           //x=0.5+(sqrt(2.0)/10.0)*cos(r), y=(sqrt(2.0)/10.0)*sin(r)
        //V27 V24
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst;
            PCoor->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoor);

            double r=3.0*Pi/4.0+(Pi-3.0*Pi/4.0)*i*dst;
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=0.5+sqrt(2.0)*cos(r)/10.0;
            PCoorPhyCur->xy[1]=(sqrt(2.0)/10.0)*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }

        break;
    case 23://s 0--> t=0, r=3pi/4-->pi,
           //x=0.5+(sqrt(2.0)/10.0)*cos(r), y=(sqrt(2.0)/10.0)*sin(r)
        //V27 V24
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst+0.5;
            PCoor->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoor);

            double r=3.0*Pi/4.0+(Pi-3.0*Pi/4.0)*(i*dst+0.5);
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=0.5+sqrt(2.0)*cos(r)/10.0;
            PCoorPhyCur->xy[1]=(sqrt(2.0)/10.0)*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }

        break;


    case 24://s 0--> t=0, r=pi-->5pi/4,
           //x=0.5+0.1414*cos(r), y=0.1414*sin(r)
        //V24 V34
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst;
            PCoor->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoor);

            double r=Pi+(5.0*Pi/4.0-Pi)*i*dst;
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=0.5+sqrt(2.0)*cos(r)/10.0;
            PCoorPhyCur->xy[1]=(sqrt(2.0)/10.0)*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 25://s 0--> t=0, r=pi-->5pi/4,
           //x=0.5+0.1414*cos(r), y=0.1414*sin(r)
        //V24 V34
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst+0.5;
            PCoor->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoor);

            double r=Pi+(5.0*Pi/4.0-Pi)*(i*dst+0.5);
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=0.5+sqrt(2.0)*cos(r)/10.0;
            PCoorPhyCur->xy[1]=(sqrt(2.0)/10.0)*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }
        break;

    case 26://s 0--> t=0, r=5pi/4-->6pi/4,
           //x=0.5+0.1414*cos(r), y=0.1414*sin(r)
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst;
            PCoor->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoor);

            double r=5.0*Pi/4.0+(6.0*Pi/4.0-5.0*Pi/4.0)*i*dst;
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=0.5+sqrt(2.0)*cos(r)/10.0;
            PCoorPhyCur->xy[1]=(sqrt(2.0)/10.0)*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }
        break;

    case 27://s 0--> t=0, r=5pi/4-->6pi/4,
           //x=0.5+0.1414*cos(r), y=0.1414*sin(r)
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst+0.5;
            PCoor->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoor);

            double r=5.0*Pi/4.0+(6.0*Pi/4.0-5.0*Pi/4.0)*(i*dst+0.5);
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=0.5+sqrt(2.0)*cos(r)/10.0;
            PCoorPhyCur->xy[1]=(sqrt(2.0)/10.0)*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }
        break;

    case 28://s 0--> t=0, r=6pi/4-->2pi,
           //x=0.5+0.1414*cos(r), y=0.1414*sin(r)
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst;
            PCoor->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoor);

            double r=6.0*Pi/4.0+(2.0*Pi-6.0*Pi/4.0)*i*dst;
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=0.5+sqrt(2.0)*cos(r)/10.0;
            PCoorPhyCur->xy[1]=(sqrt(2.0)/10.0)*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 29://s 0--> t=0, r=6pi/4-->2pi,
           //x=0.5+0.1414*cos(r), y=0.1414*sin(r)
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoor=new Coordinate;
            PCoor->xy[0]=i*dst+0.5;
            PCoor->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoor);

            double r=6.0*Pi/4.0+(2.0*Pi-6.0*Pi/4.0)*(i*dst+0.5);
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=0.5+sqrt(2.0)*cos(r)/10.0;
            PCoorPhyCur->xy[1]=(sqrt(2.0)/10.0)*sin(r);
            Qsi.push_back(PCoorPhyCur);
        }
        break;
//------------------------------------------------------------------
    case 32://s=0 t=0-->1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double y=1.50097+(1.75-1.50097)*i*dst*2.0;
            PCoorPhyCur->xy[0]=2.0000;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=0.0;
            PCoorPs->xy[1]=QcurveParameters[i]*0.5;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;

    case 33://s=0 t=0-->1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double y=1.75+(2.0000-1.75)*i*dst*2.0;
            PCoorPhyCur->xy[0]=2.0000;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=0.0;
            PCoorPs->xy[1]=QcurveParameters[i]*0.5+0.5;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;

    case 34://s=0 t=0-->1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double y=1.00755+(1.25202-1.00755)*i*dst*2.0;
            PCoorPhyCur->xy[0]=2.0000;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=0.0;
            PCoorPs->xy[1]=QcurveParameters[i]*0.5;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;

    case 35:
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double y=1.25202+(1.50097-1.25202)*i*dst*2.0;
            PCoorPhyCur->xy[0]=2.0000;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=0.0;
            PCoorPs->xy[1]=QcurveParameters[i]*0.5+0.5;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;

    case 36://s=0 t=0-->1
           //x=2, y=0.4989+(1.3990-0.4989)*i*dst
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double y=0.499871+(0.750057-0.499871)*i*dst*2.0;
            PCoorPhyCur->xy[0]=2.0000;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=0.0;
            PCoorPs->xy[1]=QcurveParameters[i]*0.5;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;
    case 37:
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double y=0.750057+(1.00755-0.750057)*i*dst*2.0;
            PCoorPhyCur->xy[0]=2.0000;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=0.0;
            PCoorPs->xy[1]=QcurveParameters[i]*0.5+0.5;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;

    case 38:
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double y=-1.50347+(-1.75+1.50347)*i*dst*2.0;
            PCoorPhyCur->xy[0]=2.0000;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=QcurveParameters[i]*0.5;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;

    case 39:
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double y=-1.75+(-2.0+1.75)*i*dst*2.0;
            PCoorPhyCur->xy[0]=2.0000;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=QcurveParameters[i]*0.5+0.5;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;

    case 40:
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double y=-1.00421+(-1.2519+1.00421)*i*dst;
            PCoorPhyCur->xy[0]=2.0000;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=QcurveParameters[i]*0.5;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;

    case 41://s=1 t=0-->1
           //x=2, y=-1.4160+(-1.7364+1.4160)*i*dst
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double y=-1.2519+(-1.50347+1.2519)*i*dst*2.0;
            PCoorPhyCur->xy[0]=2.0000;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=QcurveParameters[i]*0.5+0.5;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;



    case 42:
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double y=-0.499588+(-0.75396+0.499588)*i*dst*2.0;
            PCoorPhyCur->xy[0]=2.0000;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=QcurveParameters[i]*0.5;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;


    case 43:
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Qsi
            Coordinate * PCoorPhyCur = new Coordinate;
            double y=-0.75396+(-1.00421+0.75396)*i*dst*2.0;
            PCoorPhyCur->xy[0]=2.0000;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        QcurveParameters.clear();//From 0.0 --> 1.0
        GetQcurveParameters(Qsi, QcurveParameters);

        //s: From 0-->0.5 is the same with QcurveParameters
        for(unsigned int i=0; i< QcurveParameters.size(); i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=QcurveParameters[i]*0.5+0.5;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        break;

    default:
        cout<<"Pease check the index of edge"<<endl;
        cin.get();
        break;
    }
}

//==============================
void Example4_square_PhysicalCurve(unsigned int IndexEdge,
                                   unsigned int NumPEdge, unsigned int IndexCell,
                                   MSplineFunctions * P_InitialMap, const Mesh * P_InitialMesh,
                                   PointSet &Psi, vector<Coordinate *> &Qsi)
{
    Psi.Index_ItsCell=IndexCell;
    Psi.P_coordinates_Ps.clear();
    Qsi.clear();

    double dst=1.0/(NumPEdge-1.0);
    vector<vector<double> >  Values;


    switch(IndexEdge)
    {
    case 0://t 0-->1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=i*dst;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=Values[0][i];
            PCoorPhyCur->xy[1]=Values[1][i];
            Qsi.push_back(PCoorPhyCur);
        }

        break;
    case 1://s 1-->0
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0-i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=Values[0][i];
            PCoorPhyCur->xy[1]=Values[1][i];
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 2://t: 0-->1 s=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=i*dst;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=Values[0][i];
            PCoorPhyCur->xy[1]=Values[1][i];
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 3://s 1-->0 t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0-i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=Values[0][i];
            PCoorPhyCur->xy[1]=Values[1][i];
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 4://t 0-->1 s=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=i*dst;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=Values[0][i];
            PCoorPhyCur->xy[1]=Values[1][i];
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 5://s 1-->0 t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0-i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=Values[0][i];
            PCoorPhyCur->xy[1]=Values[1][i];
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 6://t 0-->1 s=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=i*dst;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=Values[0][i];
            PCoorPhyCur->xy[1]=Values[1][i];
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 7://s 1-->0 t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0-i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=Values[0][i];
            PCoorPhyCur->xy[1]=Values[1][i];
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 8://t 0-->1 s=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=i*dst;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=Values[0][i];
            PCoorPhyCur->xy[1]=Values[1][i];
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 9://s 1-->0 t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0-i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=Values[0][i];
            PCoorPhyCur->xy[1]=Values[1][i];
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 10://t 0-->1 s=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=i*dst;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=Values[0][i];
            PCoorPhyCur->xy[1]=Values[1][i];
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 11://s 1-->0 t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0-i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=Values[0][i];
            PCoorPhyCur->xy[1]=Values[1][i];
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 12://t 0-->1 s=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=i*dst;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=Values[0][i];
            PCoorPhyCur->xy[1]=Values[1][i];
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 13://s 1-->0 t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0-i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=Values[0][i];
            PCoorPhyCur->xy[1]=Values[1][i];
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 14://t 0-->1 s=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=i*dst;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=Values[0][i];
            PCoorPhyCur->xy[1]=Values[1][i];
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    case 15://s 1-->0 t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0-i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            PCoorPhyCur->xy[0]=Values[0][i];
            PCoorPhyCur->xy[1]=Values[1][i];
            Qsi.push_back(PCoorPhyCur);
        }
        break;
    default:
        cout<<"Pease check the index of edge"<<endl;
        cin.get();
        break;
    }
}


//==============================
void Example7_1PhysicalCurve(unsigned int IndexEdge,
                             unsigned int NumPEdge, unsigned int IndexCell,
                             MSplineFunctions * P_InitialMap, const Mesh * P_InitialMesh,
                             PointSet &Psi, vector<Coordinate *> &Qsi)
{
//    cout<<"Example7_1PhysicalCurve"<<endl;
//    cout<<"IndexEdge = "<<IndexEdge<<endl;
    double pi=3.1415926;

    Psi.Index_ItsCell=IndexCell;
    Psi.P_coordinates_Ps.clear();
    Qsi.clear();

    double dst=1.0/(NumPEdge-1.0);
    vector<vector<double> >  Values;


    switch(IndexEdge)
    {
    //-------------------y^2-x(x-1)^2=2
    case 0://s=0-->1  t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            double x= Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }
        break;

    case 5://s=0-->1  t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            double x= Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=-1.0*sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }
        break;

    case 1://s=0-->1  t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            double x= Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }
        break;

    case 4://s=0-->1  t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            double x= Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=-1.0*sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }
        break;

    case 2://s=0-->1  t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            double x= Values[0][i];
            double y= 2.0+x*(x-1.0)*(x-1.0);

            while(y<0)
            {
                x=x-(2.0+x*(x-1.0)*(x-1.0))/((3*x-1.0)*(x-1.0)); //x-->x+F(x)/DF(x)
                y=2.0+x*(x-1.0)*(x-1.0);
                if(y<0 && y>-1.0e-9)
                {
                    y=0;
                    break;

                }
            }
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=sqrt(y);

//            PCoorPhyCur->xy[0]=x;
//            if(2.0+x*(x-1.0)*(x-1.0)>0)
//            {
//                PCoorPhyCur->xy[1]=sqrt(2.0+x*(x-1.0)*(x-1.0));
//            }
//            else
//            {
//                cout<<"y1="<<2.0+x*(x-1.0)*(x-1.0)<<endl;

//                //double dx=0.001;
//                while(2.0+x*(x-1.0)*(x-1.0)<0)
//                {
//                    x=x-(2.0+x*(x-1.0)*(x-1.0))/((3*x-1.0)*(x-1.0)); //x-->x+F(x)/DF(x)

//                    cout<<"y2="<<2.0+x*(x-1.0)*(x-1.0)<<endl;
//                }
//                PCoorPhyCur->xy[0]=x;
//                PCoorPhyCur->xy[1]=sqrt(2.0+x*(x-1.0)*(x-1.0));
//            }

            Qsi.push_back(PCoorPhyCur);
        }
        break;

    case 3://s=0-->1  t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            double x= Values[0][i];
            double y=2.0+x*(x-1.0)*(x-1.0);


            while(y<0)
            {
                x=x-(2.0+x*(x-1.0)*(x-1.0))/((3*x-1.0)*(x-1.0)); //x-->x+F(x)/DF(x)
                y=2.0+x*(x-1.0)*(x-1.0);

                if(y>-1.0e-9 && y<0)
                {
                    y=0;
                    break;
                }
            }
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=-1.0*sqrt(y);

            Qsi.push_back(PCoorPhyCur);
        }
        break;

        //--------x=2-------------
    case 6://s=1.0, t:1.0-->0
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=1.0-i*dst;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            double y= Values[1][i];
            PCoorPhyCur->xy[0]=2.0;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }
        break;

    case 7://s=1.0, t:1.0-->0
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=1.0-i*dst;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            double y= Values[1][i];
            PCoorPhyCur->xy[0]=2.0;
            PCoorPhyCur->xy[1]=y;
            if(y>-0.5)
            {
                PCoorPhyCur->xy[1]=-0.5;
            }
            Qsi.push_back(PCoorPhyCur);
        }
        break;

    case 10://s=0.0, t:0-->1.0
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=0.0;
            PCoorPs->xy[1]=i*dst;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            double y= Values[1][i];
            PCoorPhyCur->xy[0]=2.0;
            PCoorPhyCur->xy[1]=y;
            if(y<0.5)
            {
                PCoorPhyCur->xy[1]=0.5;
            }
            Qsi.push_back(PCoorPhyCur);
        }
        break;

    case 11://s=0.0, t:0-->1.0
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=0.0;
            PCoorPs->xy[1]=i*dst;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            double y= Values[1][i];
            PCoorPhyCur->xy[0]=2.0;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }
        break;

        //----(x-2)^2+y^2=(0.5)^2
    case 8://s=1.0-->0.0, t=0.0
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0-i*dst;
            PCoorPs->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
//        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
//        //Qsi
//        for(unsigned int i=0; i<NumPEdge; i++)
//        {
//            Coordinate * PCoorPhyCur = new Coordinate;
//            double x= Values[0][i];
//            PCoorPhyCur->xy[0]=x;
//            PCoorPhyCur->xy[1]=-sqrt(0.25-(x-2.0)*(x-2.0));
//            Qsi.push_back(PCoorPhyCur);
//        }
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            double theta=3.0*pi/2.0+(pi-3.0*pi/2.0)*i*dst;
            PCoorPhyCur->xy[0]=2.0+0.5*cos(theta);
            PCoorPhyCur->xy[1]=0.5*sin(theta);
            Qsi.push_back(PCoorPhyCur);
        }
        break;

    case 9://s=1.0-->0.0, t=0.0
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0-i*dst;
            PCoorPs->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
//        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
//        //Qsi
//        for(unsigned int i=0; i<NumPEdge; i++)
//        {
//            Coordinate * PCoorPhyCur = new Coordinate;
//            double x= Values[0][i];
//            PCoorPhyCur->xy[0]=x;
//            PCoorPhyCur->xy[1]=sqrt(0.25-(x-2.0)*(x-2.0));
//            Qsi.push_back(PCoorPhyCur);
//        }

        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            double theta=pi+(pi/2.0-pi)*i*dst;
            PCoorPhyCur->xy[0]=2.0+0.5*cos(theta);
            PCoorPhyCur->xy[1]=0.5*sin(theta);
            Qsi.push_back(PCoorPhyCur);
        }
        break;


    default:
        cout<<"Pease check the index of edge"<<endl;
        cin.get();
        break;
    }
}

//==============================
void Example7_0PhysicalCurve(unsigned int IndexEdge,
                             unsigned int NumPEdge, unsigned int IndexCell,
                             MSplineFunctions * P_InitialMap, const Mesh * P_InitialMesh,
                             PointSet &Psi, vector<Coordinate *> &Qsi)
{

    Psi.Index_ItsCell=IndexCell;
    Psi.P_coordinates_Ps.clear();
    Qsi.clear();

    double dst=1.0/(NumPEdge-1.0);
    vector<vector<double> >  Values;


    switch(IndexEdge)
    {
    //-------------------y^2-x(x-1)^2=2
    case 0://s=0-->1  t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            double x= Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }
        break;

    case 5://s=0-->1  t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            double x= Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=-1.0*sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }
        break;

    case 1://s=0-->1  t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            double x= Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }
        break;

    case 4://s=0-->1  t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            double x= Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=-1.0*sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }
        break;

    case 2://s=0-->1  t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            double x= Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }
        break;

    case 3://s=0-->1  t=1
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=i*dst;
            PCoorPs->xy[1]=1.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            double x= Values[0][i];
            PCoorPhyCur->xy[0]=x;
            PCoorPhyCur->xy[1]=-sqrt(2.0+x*(x-1.0)*(x-1.0));
            Qsi.push_back(PCoorPhyCur);
        }
        break;

        //--------x=2-------------
    case 6://s=1.0, t:1.0-->0
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=1.0-i*dst;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            double y= Values[1][i];
            PCoorPhyCur->xy[0]=2.0;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }
        break;

    case 7://s=1.0, t:1.0-->0
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0;
            PCoorPs->xy[1]=1.0-i*dst;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            double y= Values[1][i];
            PCoorPhyCur->xy[0]=2.0;
            PCoorPhyCur->xy[1]=y;
            if(y>-0.5)
            {
                PCoorPhyCur->xy[1]=-0.5;
            }
            Qsi.push_back(PCoorPhyCur);
        }
        break;

    case 10://s=0.0, t:0-->1.0
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=0.0;
            PCoorPs->xy[1]=i*dst;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            double y= Values[1][i];
            PCoorPhyCur->xy[0]=2.0;
            PCoorPhyCur->xy[1]=y;
            if(y<0.5)
            {
                PCoorPhyCur->xy[1]=0.5;
            }
            Qsi.push_back(PCoorPhyCur);
        }
        break;

    case 11://s=0.0, t:0-->1.0
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=0.0;
            PCoorPs->xy[1]=i*dst;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            double y= Values[1][i];
            PCoorPhyCur->xy[0]=2.0;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }
        break;

        //----(x-2)^2+y^2=(0.5)^2
    case 8://s=1.0-->0.0, t=0.0
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0-i*dst;
            PCoorPs->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            double y= Values[1][i];
            PCoorPhyCur->xy[0]=2.0*y*y+1.5;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        break;

    case 9://s=1.0-->0.0, t=0.0
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            //Psi.P_coordinates_Ps
            Coordinate * PCoorPs = new Coordinate;
            PCoorPs->xy[0]=1.0-i*dst;
            PCoorPs->xy[1]=0.0;
            Psi.P_coordinates_Ps.push_back(PCoorPs);
        }
        P_InitialMap->Evaluation(0, 0, Psi.P_coordinates_Ps, IndexCell, Values, *P_InitialMesh);
        //Qsi
        for(unsigned int i=0; i<NumPEdge; i++)
        {
            Coordinate * PCoorPhyCur = new Coordinate;
            double y= Values[1][i];
            PCoorPhyCur->xy[0]=2.0*y*y+1.5;
            PCoorPhyCur->xy[1]=y;
            Qsi.push_back(PCoorPhyCur);
        }

        break;


    default:
        cout<<"Pease check the index of edge"<<endl;
        cin.get();
        break;
    }
}

//======================================
void GetPsQsOverPhysicalDomain(unsigned int ExampleIndex, unsigned int IndexEdge, unsigned int NumPEdge,
                               MSplineFunctions * P_InitialMap, Mesh * P_InitialMesh,
                               const vector<pair<unsigned int, unsigned int> >& InitialEdges,
                               const vector<unsigned int>& EdgesIndexofCells,
                               PointSet &Psi, vector<Coordinate *> &Qsi)
{
    Psi.P_coordinates_Ps.clear();
    Qsi.clear();

    switch(ExampleIndex)
    {
    case 4:
        Example4PhysicalCurve(IndexEdge, NumPEdge, EdgesIndexofCells[IndexEdge], Psi, Qsi);
        break;
    case 24:
       // Example2_2PhysicalCurve_1(IndexEdge, NumPEdge, EdgesIndexofCells[IndexEdge], Psi, Qsi);

        Example2_2PhysicalCurve_2(IndexEdge, NumPEdge, EdgesIndexofCells[IndexEdge], P_InitialMap, P_InitialMesh, Psi, Qsi);
        break;
    case 15:
        Example4_square_PhysicalCurve(IndexEdge, NumPEdge, EdgesIndexofCells[IndexEdge], P_InitialMap, P_InitialMesh, Psi, Qsi);
        break;
    case 25:
        Example4_square_PhysicalCurve(IndexEdge, NumPEdge, EdgesIndexofCells[IndexEdge], P_InitialMap, P_InitialMesh, Psi, Qsi);
        break;

    case 27:
         Example2_2PhysicalCurve_27(IndexEdge, NumPEdge, EdgesIndexofCells[IndexEdge], P_InitialMap, P_InitialMesh, Psi, Qsi);
        break;

    case 32:
        Example7_1PhysicalCurve(IndexEdge, NumPEdge, EdgesIndexofCells[IndexEdge], P_InitialMap, P_InitialMesh, Psi, Qsi);
        break;

    case 33:
        Example7_0PhysicalCurve(IndexEdge, NumPEdge, EdgesIndexofCells[IndexEdge], P_InitialMap, P_InitialMesh, Psi, Qsi);
        break;

    case 34:
        Example4PhysicalCurve(IndexEdge, NumPEdge, EdgesIndexofCells[IndexEdge], Psi, Qsi);
        break;

    case 35:
        Example7_0PhysicalCurve(IndexEdge, NumPEdge, EdgesIndexofCells[IndexEdge], P_InitialMap, P_InitialMesh, Psi, Qsi);
        break;

    default:
        cout<<"PMap_DynamicFunctions::GetPsQsOverPhysicalDomain:: ExampleIndex is out of the range!"<<endl;

        cin.get();
        break;
    }
}


void InitialExample32InsidePoints(double dl, unsigned int k, vector<vector<double> >& InsidePoints)
{
    InsidePoints.clear();
    //double dl=0.7/(1.0*k);
    double dx=0.1;
    double x1=2.0-dl;
    double r=sqrt(0.25+dl);
//    double F1=(x-2.0)*(x-2.0)+y*y-0.25-dl;//(x-2.0)*(x-2.0)+y*y-0.25;
//    double F2=x-2.0+dl;//x-2.0
//    double F3=y*y-x*(x-1.0)*(x-1.0)-2.0+dl;//y*y-x*(x-1.0)*(x-1.0)-2.0;

    vector<double> InsideP;
    //The first type points: F3=y*y-x*(x-1.0)*(x-1.0)-2.0+dl
    double x=x1;
    double Key= x*(x-1.0)*(x-1.0)+2.0-dl;
    while(Key>0)
    {
        InsideP.clear();
        InsideP.push_back(x);
        InsideP.push_back(sqrt(Key));
        InsidePoints.push_back(InsideP);

        InsideP.clear();
        InsideP.push_back(x);
        InsideP.push_back(-sqrt(Key));
        InsidePoints.push_back(InsideP);

        x=x-dx;
        Key=x*(x-1.0)*(x-1.0)+2.0-dl;
    }

    //The second type points: F1=(x-2.0)*(x-2.0)+y*y-r^2
    double Pi=3.14159265358979;
    double theta=Pi/2.0;
    while(theta<3.0*Pi/2.0)
    {
        x=2.0+r*cos(theta);
        if(x < x1)
        {
            double y=r*sin(theta);
            InsideP.clear();
            InsideP.push_back(x);
            InsideP.push_back(y);
            InsidePoints.push_back(InsideP);
        }

        theta=theta+dx;

    }

    //The third type points: F2=x-x1;

    double y3=-2.0+2.0*dl;
    while(y3<2.0-2.0*dl)
    {
        //(x1, y3) is in the physical domain or not
        double F1=(x1-2.0)*(x1-2.0)+y3*y3-0.25-dl;//(x-2.0)*(x-2.0)+y*y-0.25;
        //double F2=x-2.0+dl;//x-2.0
        double F3=y3*y3-x1*(x1-1.0)*(x1-1.0)-2.0+dl;//y*y-x*(x-1.0)*(x-1.0)-2.0;
        if(F1>0&&F3<0)
        {
            InsideP.clear();
            InsideP.push_back(x1);
            InsideP.push_back(y3);
            InsidePoints.push_back(InsideP);
        }
        //
        y3=y3+dx;
    }



}


#endif // PMAP_DYNAMICFUNCTIONS_H
