//#include "StdAfx.h"
#include <iostream>
using namespace std;
#include "PointSet.h"

PointSet::PointSet(void)
{
    Index_ItsCell=0;
    P_coordinates_Ps.clear();
}

PointSet::~PointSet(void)
{
}

PointSet::PointSet(unsigned int index_cell, const vector<Coordinate *> &Pcoordinate_Ps)
{
	Index_ItsCell=index_cell;
	P_coordinates_Ps.clear();
	for (unsigned int i=0; i<Pcoordinate_Ps.size(); i++)
	{
		P_coordinates_Ps.push_back(Pcoordinate_Ps[i]);
	}
	//cout<<coordinates_Ps.size()<<endl;

}

PointSet::PointSet(const PointSet & PS)
{
    Index_ItsCell=PS.Index_ItsCell;
    P_coordinates_Ps.clear();//vector<Coordinate*>

    for(unsigned int i=0; i<PS.P_coordinates_Ps.size(); i++)
    {
        P_coordinates_Ps.push_back(PS.P_coordinates_Ps.at(i));
    }
}

void PointSet::DeleteClear()
{
   for(unsigned int i=0; i<this->P_coordinates_Ps.size(); i++)
   {
       delete this->P_coordinates_Ps[i];
       this->P_coordinates_Ps[i]=NULL;
   }
   vector<Coordinate* >().swap(this->P_coordinates_Ps);
}
