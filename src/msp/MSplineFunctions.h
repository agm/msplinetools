#ifndef MSPLINEFUNCTIONS_H
#define MSPLINEFUNCTIONS_H

#include <vector>
using namespace std;
//--------------------
class MSplineFunction;
class Mesh;
class Bases;
class PointSet;
class Coordinate;
//--------------------

class MSplineFunctions
{
public:
    vector<MSplineFunction*> P_MSplineFunctions;

public:

    MSplineFunctions(void);
    ~MSplineFunctions(void);
    MSplineFunctions(const vector<vector<double> > &Coeff, const Bases &BasisSet, const Mesh & CurrentMesh);
    MSplineFunctions(const vector<vector<double> > &Coeff, const Mesh & CurrentMesh);

    /**
     * @brief MSplineFunctions: Generate Hermite Data Based On Vertices Position And Distance
     * @param VerticesPosition
     * @param P_ParametricMesh
     * @param Coeff
     */
    MSplineFunctions(const vector<vector<double> > & VerticesPosition, Mesh *P_ParametricMesh, vector<vector<double> > &Coeff);
public:
//    void Reset(const vector<vector<double> > &Coeff, const Bases &BasisSet, const Mesh & CurrentMesh);
//    void Reset(const vector<vector<double> > &Coeff, const Mesh & CurrentMesh);
    void ComputeVerticesPositions(vector<vector<double> >& VerticesPosition, Mesh* P_ParametricMesh);


public:

    void Evaluation(int DerOrderu, int DerOrderv, const vector<PointSet> & PsetS, vector<vector<vector<double> > > &ValuesXYZ, const Mesh & CurrentMesh);

    void Evaluation(int DerOrderu, int DerOrderv, const vector<Coordinate*> & Pcoordinate_Ps, unsigned int index_cell, vector<vector<double> > &ValuesXYZ, const Mesh & CurrentMesh);

    void Cout_MSplineFunctions();

    void ModifyMSplineFunctions(const vector<vector<double> > &Coeff, unsigned int Index_Cell, const Mesh & CurrentMesh);

    void NormalVectorAtAVertex(const unsigned int & IrregularCenterIndex, Mesh * P_CMesh, vector<double> &initialNormalVector);

    void NormalVectorAtAIrregularVertex(const unsigned int & IrregularCenterIndex, Mesh * P_CMesh, vector<double> &initialNormalVector);

    void GaussianCurvature(std::vector<double> &GaussianCurvatures, const std::vector<PointSet> &PSet, Mesh & CurrentMesh);

    /**
     * @brief TangentDirectionFromTheCenterVertexToTheAdjacentVertex
     * @param IndexV
     * @param CenterIndexV
     * @param P_CMesh
     * @param initialT: initialT will be returned which describes the tengent direction from the center vertex (the CenterIndexV-th vertex) to the IndexV-th vertex
     * @param CommonCellIndex
     * @return
     */
    bool TangentDirectionFromTheCenterVertexToTheAdjacentVertex
    (unsigned int IndexV, unsigned int CenterIndexV, Mesh * P_CMesh, vector<double> &initialT, unsigned int &CommonCellIndex);


    /**
     * @brief TangentVectorsFromAVertex
     * @param TangentVectors
     * @param IndexTangentVectors
     * @param CenterVIndex
     * @param P_CMesh
     * At first, we arrange the vertices around the center vertex with the order (here, the type of center vertex is classified into two)
     * return: TangentVectors[i] is the i-th tangent vector from the center vertex to the i-th vertex;
       IndexTangentVectors[i] is the vector of indices of coeffs at the i-th vertex connected to this center vertex
       and these indices with the coeffs at the center vertex decide TangentVectors[i]
     */
    void TangentVectorsFromAVertex(vector<vector<double> >& TangentVectors, vector<vector<unsigned int> > & IndexTangentVectors, const unsigned int CenterVIndex, Mesh* P_CMesh);

    void TangentVectorsFromAVertex_BasedOnPositions(vector<vector<double> >& TangentVectors, vector<vector<unsigned int> > & IndexTangentVectors, const unsigned int CenterVIndex, Mesh* P_CMesh);
    /**
     * @brief ModifyTangentVectorsAtAVertexBasedonGivenNormalVector
     * @param NormalVector
     * @param TangentVectors: TangentVectors are projected to the plane which is vertical the NormalVector, and all the angles between these tangent vectors are positive (or negetive)
     */
    void ModifyTangentVectorsAtAVertexBasedonGivenNormalVector(const vector<double> &NormalVector, vector<vector<double> >& TangentVectors);

    /**/
     void UpdataMSplineFunctionsAndCoeffAroundAnIrregularVertexByItsTangentVectors(vector<vector<double> > &Coeff, const vector<vector<double> >&TangentVectors, const vector<vector<unsigned int> > &IndexTangentVectors, const unsigned int &CenterVIndex, const Mesh *P_CMesh);


     void UnitNormalVectorsByGivenTangentVectorsSeq(vector<double> &NormalVector, const vector<vector<double> > & TangentVectors);

     /* Set CG1 condition for a general mspline surfaces at an irregular vertex (its index is IrVindex)
     */
     void InitialSetCG1ConditionAtAIrregularVertexBasedOnPositions(unsigned int IrVindex, Mesh *P_CMesh, vector<vector<double> > & Coeff);


     /*Generate the coefficient of this mspline function over a finer parametric mesh*/
     Mesh * GenerateThisMSplineFunctionsOverAFinerMesh(const unsigned int &subdivision_k, Mesh *P_CMesh, vector<vector<double> > &CoeffMap);
     /*Print this mspline functions over a finer parametric mesh*/
     void CoutThisMSplineFunctionsOverAFinerMesh(const unsigned int &subdivision_k, Mesh *P_CMesh, vector<vector<double> > &CoeffMap);
     //
     void CoutThisMSplineFunctionsAsBSplineSurfaceAxelFile(Mesh * P_CMesh);
     //
     void CoutThisMSplineFunctionsJacobianMeshAxelFile(Mesh * P_CMesh, const unsigned int n_u, const unsigned int n_v, const char* filename, const char* filenameAreas);
private:
    bool NormalVectorAtAVertexOverOneCell(const unsigned int &indexCell, const Coordinate &Coor,
                                          vector<double> &Ni, Mesh * P_CMesh);
    bool NormalVectorAtAIrregularVertexOverOneCell(const unsigned int &indexCell, const Coordinate &Coor,
                                                   vector<double> &Ni, Mesh * P_CMesh);
    void TheVectorFromOneVertexToTheOther(const unsigned int &Index1, const unsigned int &Index2,  Mesh *P_CMesh, vector<double>& TDirection);

    void PrintElementInformation(const vector<vector<double> >& VerticesPosition, Mesh *P_ParametricMesh, char * H_CellbyCell, char * Theta_CellbyCell, char * Is_Concave_CellbyCell);

public:
    //
   MSplineFunctions * ModifyParameterizationBySpringModelAndBoundaryFixing(const double TimeSize, const double Hc, const double CoulombCoeff, const double epsilon0,
                                                                           const unsigned n_u, const unsigned n_v, const unsigned MaxIterNum, Mesh * P_ParametricMesh,
                                                                                              const  vector<vector<double> > & Coeff_Parametric_Map, vector<vector<double> > &NewCoeff_ParametricMap,
                                                                                                              char * ModifyTheParameterizationComputationalProgress,
                                                                                                              char * SpringMethodVerticesDistanceConvergenceEpsilons,
                                                                                                              char * ModifiedParameterization_axl,
                                                                                                              char * ModifiedParameterizationWithSpringModel_JacobianAxelMesh_axl,
                                                                                                              char * ModifiedVerticesPosition_Axel_axl,
                                                                                                              char * ModifiedParameterizationWithSpringModel_PhysicalAreas,
                                                                                                              char * H_CellbyCell,
                                                                                                              char * CosTheta_CellbyCell,
                                                                                                              char * Is_Concave_CellbyCell);
   MSplineFunctions * ModifyParameterizationBySpringModelAndBoundaryFixing_Double(const double TimeSize, const double Hc, const double CoulombCoeff, const double epsilon0,
                                                          const unsigned n_u, const unsigned n_v, const unsigned MaxIterNum, const unsigned int N, Mesh * P_ParametricMesh,
                                                                             const  vector<vector<double> > & Coeff_Parametric_Map, vector<vector<double> > &NewCoeff_ParametricMap);

   MSplineFunctions * SetInitialVerticesPositions(Mesh *P_FEMMesh,
                                                  vector<vector<double> > &CoeffRefinedParametricMap);
};

#endif
