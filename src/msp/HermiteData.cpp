//#include "StdAfx.h"
#include <iostream>
using namespace std;

#include "HermiteData.h"

HermiteData::HermiteData(void)
{
}

HermiteData::~HermiteData(void)
{
}

HermiteData::HermiteData(double d, unsigned int Index)
{
	Index_InitialCell=Index;
	ItsHermiteData.clear();
	ItsHermiteData.push_back(d);
	ItsHermiteData.push_back(d);
	ItsHermiteData.push_back(d);
	ItsHermiteData.push_back(d);
}

HermiteData::HermiteData(const HermiteData & HD)
{
    Index_InitialCell=HD.Index_InitialCell;
    ItsHermiteData.clear();
    for(unsigned int i=0; i<HD.ItsHermiteData.size(); i++)
    {
        ItsHermiteData.push_back(HD.ItsHermiteData[i]);
    }
}

void HermiteData::Cout_HermiteData()
{
	cout<<"ItsHermiteData = ["<<ItsHermiteData[0]<<", "<<ItsHermiteData[1]<<", "<<ItsHermiteData[2]<<", "<<ItsHermiteData[3]<<"]"<<endl;
}
