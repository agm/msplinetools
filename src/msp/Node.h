#ifndef NODE_H
#define NODE_H

#include <vector>
using namespace std;

template <class T>
class Node
{
public:
    vector<T> ItsParents;
    vector<T> ItsChildren;

    Node()
    {
        ItsParents.clear();
        ItsChildren.clear();
    }
   Node(const Node & element)
    {
        ItsParents.clear();
        ItsChildren.clear();
        for(unsigned int i=0; i<element.ItsChildren.size(); i++)
        {
            ItsChildren.push_back(element.ItsChildren[i]);
        }
        for(unsigned int i=0; i<element.ItsParents.size(); i++)
        {
            ItsParents.push_back(element.ItsParents[i]);
        }
    }
   Node &operator=(const Node& element)
   {
       if(this==&element)
       {
           return *this;
       }
       else
       {
           ItsParents.clear();
           ItsChildren.clear();
           for(unsigned int i=0; i<element.ItsChildren.size(); i++)
           {
               ItsChildren.push_back(element.ItsChildren[i]);
           }
           for(unsigned int i=0; i<element.ItsParents.size(); i++)
           {
               ItsParents.push_back(element.ItsParents[i]);
           }
           return *this;
       }
   }


    void ClearItsParents()
    {
        ItsParents.clear();
    }
    void ClearItsChildren()
    {
        ItsChildren.clear();
    }

};

#endif // NODE_H
