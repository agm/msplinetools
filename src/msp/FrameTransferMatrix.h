#ifndef FRAMETRANSFERMATRIX_H
#define FRAMETRANSFERMATRIX_H

class FrameTransferMatrix
{
public:
    /**
     * @brief O11, O12, O21, O22 are used to describe the FrameTransferMatrix [Oij]
     */
	double O11, O12, O21, O22;
public:
    FrameTransferMatrix();
    ~FrameTransferMatrix(void);

    /**
     * @brief FrameTransferMatrix: generate a FrameTransferMatrix such that Oij=aij
     * @param a11
     * @param a12
     * @param a21
     * @param a22
     */
    FrameTransferMatrix(double a11, double a12, double a21, double a22);
};

#endif
