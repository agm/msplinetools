//#include "StdAfx.h"

#include <iostream>

#include "Cell.h"

Cell::Cell(void)
{
	ItsParent=-1;
}

Cell::~Cell(void)
{
}

Cell::Cell(const Cell &C)
{
// 	unsigned int Index_InitialCell;
	Index_InitialCell=C.Index_InitialCell;

//	double CellSize[4];
	CellSize[0]=C.CellSize[0];
	CellSize[1]=C.CellSize[1];
	CellSize[2]=C.CellSize[2];
	CellSize[3]=C.CellSize[3];

// 	vector<unsigned int> Index_ItsCornerVertices;
	Index_ItsCornerVertices.clear();
	for (unsigned int i=0; i<C.Index_ItsCornerVertices.size(); i++)
	{
		Index_ItsCornerVertices.push_back(C.Index_ItsCornerVertices[i]);
	}
// set<unsigned int> Index_ItsSubCells
	Index_ItsSubCells.clear();
	for (set<unsigned int>::const_iterator it=C.Index_ItsSubCells.begin(); it!=C.Index_ItsSubCells.end(); it++)
	{
		Index_ItsSubCells.insert(*it);
	}

// ItsParent
	ItsParent=C.ItsParent;

// Index_AdjacentCell_up;
    Index_AdjacentCell_up.clear();
    for(set<unsigned int>::const_iterator it=C.Index_AdjacentCell_up.begin(); it!=C.Index_AdjacentCell_up.end(); it++)
    {
        Index_AdjacentCell_up.insert(*it);
    }

// Index_AdjacentCell_down;
    Index_AdjacentCell_down.clear();
    for(set<unsigned int>::const_iterator it=C.Index_AdjacentCell_down.begin(); it!=C.Index_AdjacentCell_down.end(); it++)
    {
        Index_AdjacentCell_down.insert(*it);
    }

// Index_AdjacentCell_left;
    Index_AdjacentCell_left.clear();
    for(set<unsigned int>::const_iterator it=C.Index_AdjacentCell_left.begin(); it!=C.Index_AdjacentCell_left.end(); it++)
    {
        Index_AdjacentCell_left.insert(*it);
    }

// Index_AdjacentCell_right;
    Index_AdjacentCell_right.clear();
    for(set<unsigned int>::const_iterator it=C.Index_AdjacentCell_right.begin(); it!=C.Index_AdjacentCell_right.end(); it++)
    {
        Index_AdjacentCell_right.insert(*it);
    }

}

bool Cell::Is_CornerVertex(const unsigned int & IndexVertex, unsigned int &IndexInThisCell)
{
    bool Flag=false;
    IndexInThisCell=0;

    for(unsigned int i=0; i< this->Index_ItsCornerVertices.size(); i++)
    {
        if(this->Index_ItsCornerVertices[i]==IndexVertex)
        {
            Flag=true;
            IndexInThisCell=i;
            break;
        }
    }

    return Flag;
}

int Cell::TheLocalDirection(const unsigned int &IndexV1, const unsigned int &IndexV2)
{
    int DirectionFlag=0;
    //s-direction (1), -s -direction (-1); t-direction (2), -t -direction (-2)

    unsigned int IndexCell1, IndexCell2;
    bool Flag1=this->Is_CornerVertex(IndexV1, IndexCell1);
    bool Flag2=this->Is_CornerVertex(IndexV2, IndexCell2);

    if(Flag1&&Flag2)
    {
        if((IndexCell1==0&& IndexCell2==1)||(IndexCell1==3&&IndexCell2==2))
        {
            DirectionFlag=1;
        }
        if((IndexCell2==0&& IndexCell1==1)||(IndexCell2==3&&IndexCell1==2))
        {
            DirectionFlag=-1;
        }
        if((IndexCell1==0&& IndexCell2==3)||(IndexCell1==1&&IndexCell2==2))
        {
            DirectionFlag=2;
        }
        if((IndexCell2==0&& IndexCell1==3)||(IndexCell2==1&&IndexCell1==2))
        {
            DirectionFlag=-2;
        }

    }
    else
    {
        std::cout<<"Cell()::Warning::Check that the input vertices are the corner vertices of this cell or not."<<std::endl;
    }

    return DirectionFlag;
}

//========
bool Cell::CoordinateOfACornerVertex(const unsigned int &IrregularCenterIndex, vector<double> &ST)
{
    bool Key=false;

    ST.clear();

    unsigned int IndexInThisCell;

    Key=this->Is_CornerVertex(IrregularCenterIndex, IndexInThisCell);

    if(Key)
    {
        double x0= this->CellSize[0];
        double x1= this->CellSize[1];
        double y0= this->CellSize[2];
        double y1= this->CellSize[3];

        switch(IndexInThisCell)
        {
        case 0:
            ST.push_back(x0);
            ST.push_back(y0);
            break;
        case 1:
            ST.push_back(x1);
            ST.push_back(y0);
            break;
        case 2:
            ST.push_back(x1);
            ST.push_back(y1);
            break;
        case 3:
            ST.push_back(x0);
            ST.push_back(y1);
            break;
        default:
            std::cout<<"Cell::CoordinateOfACornerVertex():: Error:: the position of input vertex"<<std::endl;
            std::cin.get();
            break;
        }
    }

    return Key;
}
//=======
bool Cell::IndexInItsCornerVertices(const unsigned int &IndexV, unsigned int &Index)

{
    bool Key=false;
    for(Index=0; Index<this->Index_ItsCornerVertices.size(); Index++)
    {
        if(this->Index_ItsCornerVertices[Index]==IndexV)
        {
            Key=true;
            break;
        }
    }
    return Key;
}

//===========
