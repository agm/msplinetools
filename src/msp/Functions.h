#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <vector>
#include <fstream>
#include <set>
#include <algorithm>

#include <iostream>
#include <iomanip>

#include "FrameTransferMatrix.h"
#include "Mesh.h"
#include "Coordinate.h"
#include "MSplineFunction.h"




#include "petsc.h"

using namespace std;
//=====================================================================
static void Vec2Stdvector(Vec &PetscVec, vector<double>& StdVec)
{
    //cout<<"Functions: Vec2Stdvector()"<<endl;
    PetscInt size;
    //cout<<"Before VecGetSize()"<<endl;

    VecGetSize(PetscVec, &size);
    StdVec.resize(size, 0.0);
    for(PetscInt i=0; i<size; i++)
    {
        PetscScalar v;

        //cout<<"Before VecGetValues()  --i= "<< i<<endl;
        VecGetValues(PetscVec,1, &i, &v);

        StdVec[i]=v;
    }
}
//================ParametricMap Part===========================================================
//Reader Coeff
static void Reader_Coeff_ParametricMap(string file, unsigned int dimension, unsigned int VerNum , vector< vector<double> > &Coeff_Parametric_Map)
{
        unsigned int BasesNum= VerNum*4;
        ifstream infile(file.c_str());

        Coeff_Parametric_Map.clear();
        vector<double> Coeff_i;
        for (unsigned int i=0; i<dimension; i++)
        {
            Coeff_Parametric_Map.push_back(Coeff_i);
        }
        for (unsigned int i=0; i<BasesNum; i++)
        {
                for (unsigned int j=0; j<dimension; j++)
                {
                        double values;
                        infile>>values;
                        Coeff_Parametric_Map[j].push_back(values);
                }

        }

};
//========================================================================================================
static void Map2set_unsigned_int(set<unsigned int> &CellIndex0, const map<unsigned int, Coordinate> & Map)
{
	CellIndex0.clear();
	for (map<unsigned int, Coordinate>::const_iterator it=Map.begin(); it!=Map.end(); it++)
	{
		CellIndex0.insert((*it).first);
	}
};
//=======================================================================================================
static FrameTransferMatrix FrameTranMatrix_inverse(FrameTransferMatrix M)
{
	FrameTransferMatrix M1;//the inverse of M
	double Det=M.O11*M.O22-M.O12*M.O21;
	M1.O11=M.O22/Det;
	M1.O12=-M.O12/Det;
	M1.O21=-M.O21/Det;
	M1.O22=M.O11/Det;
	return M1;
};

//======================================================================================================
//Set the Hermite data at the index_vertex-th vertex based on the type of this vertex
static void Set_HermiteData_Vertex(unsigned int index_vertex, const Mesh & CurrentMesh, vector<vector<double> >& HermiteDatas)
{
	HermiteDatas.clear();
	vector<double> Hermitedata;
	//Is the index_vertex-th vertex an irregular vertex?
	map<unsigned int, unsigned int>::const_iterator it=CurrentMesh.IrregularVerticesMap.find(index_vertex);
	if (it==CurrentMesh.IrregularVerticesMap.end())//is not an irregular vertex
	{
		//Hermitedata=[1,0,0,0]
		Hermitedata.clear();
		Hermitedata.push_back(1);
		Hermitedata.push_back(0);
		Hermitedata.push_back(0);
		Hermitedata.push_back(0);
		HermiteDatas.push_back(Hermitedata);
		//Hermitedata=[0,1,0,0]
		Hermitedata.clear();
		Hermitedata.push_back(0);
		Hermitedata.push_back(1);
		Hermitedata.push_back(0);
		Hermitedata.push_back(0);
		HermiteDatas.push_back(Hermitedata);
		//Hermitedata=[0,0,1,0]
		Hermitedata.clear();
		Hermitedata.push_back(0);
		Hermitedata.push_back(0);
		Hermitedata.push_back(1);
		Hermitedata.push_back(0);
		HermiteDatas.push_back(Hermitedata);
		//Hermitedata=[0,0,0,1]
		Hermitedata.clear();
		Hermitedata.push_back(0);
		Hermitedata.push_back(0);
		Hermitedata.push_back(0);
		Hermitedata.push_back(1);
		HermiteDatas.push_back(Hermitedata);
	}
	else//is an irregular vertex
	{
		switch((*it).second % 4)//degV % 4
		{
		case 1:
			//Hermitedata=[1,0,0,0]
			Hermitedata.clear();
			Hermitedata.push_back(1);
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			HermiteDatas.push_back(Hermitedata);
			//Hermitedata=[0,0,0,0]//0 0 0 0 
			Hermitedata.clear();
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			HermiteDatas.push_back(Hermitedata);
			//Hermitedata=[0,0,0,0]//0 0 0 0 
			Hermitedata.clear();
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			HermiteDatas.push_back(Hermitedata);
			//Hermitedata=[0,0,0,0]//0 0 0 0 
			Hermitedata.clear();
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			HermiteDatas.push_back(Hermitedata);
			break;
		case 2:
			//Hermitedata=[1,0,0,0]
			Hermitedata.clear();
			Hermitedata.push_back(1);
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			HermiteDatas.push_back(Hermitedata);
			//Hermitedata=[0,0,0,0]//0 0 0 0 
			Hermitedata.clear();
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			HermiteDatas.push_back(Hermitedata);
			//Hermitedata=[0,0,0,0]//0 0 0 0 
			Hermitedata.clear();
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			HermiteDatas.push_back(Hermitedata);
			//Hermitedata=[0,0,0,1]
			Hermitedata.clear();
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			Hermitedata.push_back(1);
			HermiteDatas.push_back(Hermitedata);
			break;
		case 3:
			//Hermitedata=[1,0,0,0]
			Hermitedata.clear();
			Hermitedata.push_back(1);
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			HermiteDatas.push_back(Hermitedata);
			//Hermitedata=[0,0,0,0]//0 0 0 0 
			Hermitedata.clear();
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			HermiteDatas.push_back(Hermitedata);
			//Hermitedata=[0,0,0,0]//0 0 0 0 
			Hermitedata.clear();
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			HermiteDatas.push_back(Hermitedata);
			//Hermitedata=[0,0,0,0]//0 0 0 0 
			Hermitedata.clear();
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			Hermitedata.push_back(0);
			HermiteDatas.push_back(Hermitedata);
			break;

		}
	}
}
//Solver===================================================================================================
static void LUsolve(int matrixorder, int p, vector< vector<double> > matrix, vector<double> b,vector<double> &solution)
{
    int i,j;
    int h,s,pp;
    double c;
    double **M;
    M = new double*[matrixorder];
    for(i = 0; i < matrixorder; i++)
        M[i] = new double[p];

    for (i = 0; i < matrixorder; i++)
    {
        for (j = 0; j < p; j++)
        {
            M[i][j] = matrix[i][j];
        }
        solution[i] = b[i];
    }
    for(i = 2; i <= matrixorder; i++)
    {
        if(matrixorder >= i + p ) h = i + p - 1;
        else h = matrixorder;
        for(j = i; j <= h; j++)
        {
            c=0.0;
            if(j > p) s = j - p + 1;
            else s = 1;
            for(int pp = s; pp <= i - 1; pp++)
                    c += matrix[pp-1][i-pp] * matrix[pp-1][j-pp] / matrix[pp-1][0];

            matrix[i-1][j-i] -= c;
        }
    }

    for(i = 2; i <= matrixorder; i++)
    {
        c = 0.0;
        if(i > p) s = i - p + 1;
        else s = 1;
        for(pp = s; pp <= i - 1; pp++)
                c += matrix[pp-1][i-pp] * solution[pp-1] / matrix[pp-1][0];
        solution[i-1] = solution[i-1] - c;
    }

    for(i = matrixorder; i >= 1; i--)
    {
        c = 0.0;
        if(matrixorder >= i + p) s = i + p - 1;
        else s = matrixorder;
        for(pp = i + 1; pp <= s; pp++)
                c += matrix[i-1][pp-i] * solution[pp-1];

        solution[i-1] = (solution[i-1] - c) / matrix[i-1][0];
    }
    for(i = 0; i < matrixorder; i++)
        delete[] M[i];

    delete[] M;
}


//============================================================================
//Print
static void PrintParametricMap(const vector<vector<double> > &Coeff_Parametric_Map,const Mesh * P_ParametricMesh, char * filename)
{
        ofstream ofAxel;
        ofstream ofMesh;
        ofstream ofCoeff;

        ofAxel.open(filename);
        ofAxel<<"<axl>"<<endl;
        ofAxel<<"<mspline type=\"InitialMesh\">"<<endl;
        ofAxel<<"<vertexnumber> "<<P_ParametricMesh->P_Vertices->size()<<"</vertexnumber>"<<endl;

        //Mesh
        ofMesh.open("ParametricMesh.txt");
        ofMesh<<P_ParametricMesh->P_Vertices->size()<<" "<<P_ParametricMesh->P_Cells->size()<<" TransitionNUM"<<endl;
        //ofMesh<<endl;

        for (unsigned int i=0; i<P_ParametricMesh->P_Cells->size(); i++)
        {
                ofAxel<<"<cell>"<<endl;
                ofAxel<<P_ParametricMesh->P_Cells->at(i).CellSize[0]<<" "<<P_ParametricMesh->P_Cells->at(i).CellSize[1]<<" "<<P_ParametricMesh->P_Cells->at(i).CellSize[2]<<" "<<P_ParametricMesh->P_Cells->at(i).CellSize[3]<<endl;
                ofAxel<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[0]<<" "<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[1]<<" "<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[2]<<" "<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[3]<<endl;
                ofAxel<<"</cell>"<<endl;

                //Mesh
                ofMesh<<endl;
                ofMesh<<P_ParametricMesh->P_Cells->at(i).CellSize[0]<<" "<<P_ParametricMesh->P_Cells->at(i).CellSize[1]<<" "<<P_ParametricMesh->P_Cells->at(i).CellSize[2]<<" "<<P_ParametricMesh->P_Cells->at(i).CellSize[3]<<endl;
                ofMesh<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[0]<<" "<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[1]<<" "<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[2]<<" "<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[3]<<endl;
                ofMesh<<endl;
        }

        unsigned int TransitionNUM=0;

        for (unsigned int i=0; i<P_ParametricMesh->P_Cells->size(); i++)
        {
                for (unsigned int j=0; j<i; j++)
                {
                    unsigned int J=P_ParametricMesh->P_Cells->at(j).Index_InitialCell;
                    unsigned int I=P_ParametricMesh->P_Cells->at(i).Index_InitialCell;
                    double o11=(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O11;
                    double o12=(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O12;
                    double o21=(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O21;
                    double o22=(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O22;
                    double tol=(o11-1.0)*(o11-1.0)+o12*o12+o21*o21+(o22-1.0)*(o22-1.0);
                    if(tol>1.0e-2)
                    {
                        ofAxel<<"<transition cells=\""<<i<<" "<<j<<"\">"<<endl;
                        ofAxel<<(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O11<<" "<<(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O12<<" "<<(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O21<<" "<<(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O22<<endl;
                        ofAxel<<"</transition>"<<endl;

                        ofMesh<<endl;
                        ofMesh<<i<<" "<<j<<endl;
                        ofMesh<<(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O11<<" "<<(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O12<<" "<<(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O21<<" "<<(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O22<<endl;
                        ofMesh<<endl;

                        TransitionNUM=TransitionNUM+1;
                    }

                }
        }

        ofMesh<<"Transition Map Number = "<<TransitionNUM<<endl;
        ofMesh.close();

        ofAxel<<"<points>"<<endl;

        //Coeff
        ofCoeff.open("ParametricCoeff.txt");
        switch(Coeff_Parametric_Map.size())
        {
        case 0:
                cout<<"There is no point!!"<<endl;
                break;
        case 1:
                for (unsigned int i=0; i<Coeff_Parametric_Map[0].size(); i++)
                {
                        ofAxel<<Coeff_Parametric_Map[0][i]<<" "<<0.0<<" "<<0.0<<endl;
                        ofCoeff<<Coeff_Parametric_Map[0][i]<<" "<<0.0<<endl;
                }
                break;
        case 2:
                for (unsigned int i=0; i<Coeff_Parametric_Map[0].size(); i++)
                {
                        ofAxel<<Coeff_Parametric_Map[0][i]<<" "<<Coeff_Parametric_Map[1][i]<<" "<<0.0<<endl;

                        ofCoeff<<Coeff_Parametric_Map[0][i]<<" "<<Coeff_Parametric_Map[1][i]<<endl;
                }
                break;
        case 3:
                for (unsigned int i=0; i<Coeff_Parametric_Map[0].size(); i++)
                {
                        ofAxel<<Coeff_Parametric_Map[0][i]<<" "<<Coeff_Parametric_Map[1][i]<<" "<<Coeff_Parametric_Map[2][i]<<endl;

                        ofCoeff<<Coeff_Parametric_Map[0][i]<<" "<<Coeff_Parametric_Map[1][i]<<" "<<Coeff_Parametric_Map[2][i]<<endl;
                }
                break;
        default:
                for (unsigned int i=0; i<Coeff_Parametric_Map[0].size(); i++)
                {
                        ofAxel<<Coeff_Parametric_Map[0][i]<<" "<<Coeff_Parametric_Map[1][i]<<" "<<Coeff_Parametric_Map[2][i]<<endl;

                        ofCoeff<<Coeff_Parametric_Map[0][i]<<" "<<Coeff_Parametric_Map[1][i]<<" "<<Coeff_Parametric_Map[2][i]<<endl;
                }
                cout<<"the dimesion of parametricMap > 3"<<endl;
                break;

        }

        ofCoeff.close();

        ofAxel<<"</points>"<<endl;
        ofAxel<<"</mspline>"<<endl;
        ofAxel<<"</axl>"<<endl;
        ofAxel.close();
}

//Print
static void PrintParametricMeshCoeff(const vector<vector<double> > &Coeff_Parametric_Map,const Mesh * P_ParametricMesh)
{
       // ofstream ofAxel;
        ofstream ofMesh;
        ofstream ofCoeff;

        //ofAxel.open(filename);
        //ofAxel<<"<axl>"<<endl;
       // ofAxel<<"<mspline type=\"InitialMesh\">"<<endl;
        //ofAxel<<"<vertexnumber> "<<P_ParametricMesh->P_Vertices->size()<<"</vertexnumber>"<<endl;

        //Mesh
        ofMesh.open("ParametricMesh.txt");
        ofMesh<<P_ParametricMesh->P_Vertices->size()<<" "<<P_ParametricMesh->P_Cells->size()<<" TransitionNUM"<<endl;
        //ofMesh<<endl;

        for (unsigned int i=0; i<P_ParametricMesh->P_Cells->size(); i++)
        {
//                ofAxel<<"<cell>"<<endl;
//                ofAxel<<P_ParametricMesh->P_Cells->at(i).CellSize[0]<<" "<<P_ParametricMesh->P_Cells->at(i).CellSize[1]<<" "<<P_ParametricMesh->P_Cells->at(i).CellSize[2]<<" "<<P_ParametricMesh->P_Cells->at(i).CellSize[3]<<endl;
//                ofAxel<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[0]<<" "<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[1]<<" "<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[2]<<" "<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[3]<<endl;
//                ofAxel<<"</cell>"<<endl;

                //Mesh
                ofMesh<<endl;
                ofMesh<<P_ParametricMesh->P_Cells->at(i).CellSize[0]<<" "<<P_ParametricMesh->P_Cells->at(i).CellSize[1]<<" "<<P_ParametricMesh->P_Cells->at(i).CellSize[2]<<" "<<P_ParametricMesh->P_Cells->at(i).CellSize[3]<<endl;
                ofMesh<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[0]<<" "<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[1]<<" "<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[2]<<" "<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[3]<<endl;
                ofMesh<<endl;
        }

        unsigned int TransitionNUM=0;

        for (unsigned int i=0; i<P_ParametricMesh->P_Cells->size(); i++)
        {
                for (unsigned int j=0; j<i; j++)
                {
                    unsigned int J=P_ParametricMesh->P_Cells->at(j).Index_InitialCell;
                    unsigned int I=P_ParametricMesh->P_Cells->at(i).Index_InitialCell;
                    double o11=(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O11;
                    double o12=(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O12;
                    double o21=(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O21;
                    double o22=(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O22;
                    double tol=(o11-1.0)*(o11-1.0)+o12*o12+o21*o21+(o22-1.0)*(o22-1.0);
                    if(tol>1.0e-2)
                    {
//                        ofAxel<<"<transition cells=\""<<i<<" "<<j<<"\">"<<endl;
//                        ofAxel<<(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O11<<" "<<(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O12<<" "<<(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O21<<" "<<(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O22<<endl;
//                        ofAxel<<"</transition>"<<endl;

                        ofMesh<<endl;
                        ofMesh<<i<<" "<<j<<endl;
                        ofMesh<<(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O11<<" "<<(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O12<<" "<<(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O21<<" "<<(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O22<<endl;
                        ofMesh<<endl;

                        TransitionNUM=TransitionNUM+1;
                    }

                }
        }

        ofMesh<<"Transition Map Number = "<<TransitionNUM<<endl;
        ofMesh.close();

       // ofAxel<<"<points>"<<endl;

        //Coeff
        ofCoeff.open("ParametricCoeff.txt");
        switch(Coeff_Parametric_Map.size())
        {
        case 0:
                cout<<"There is no point!!"<<endl;
                break;
        case 1:
                for (unsigned int i=0; i<Coeff_Parametric_Map[0].size(); i++)
                {
                        //ofAxel<<Coeff_Parametric_Map[0][i]<<" "<<0.0<<" "<<0.0<<endl;
                        ofCoeff<<Coeff_Parametric_Map[0][i]<<" "<<0.0<<endl;
                }
                break;
        case 2:
                for (unsigned int i=0; i<Coeff_Parametric_Map[0].size(); i++)
                {
                        //ofAxel<<Coeff_Parametric_Map[0][i]<<" "<<Coeff_Parametric_Map[1][i]<<" "<<0.0<<endl;

                        ofCoeff<<Coeff_Parametric_Map[0][i]<<" "<<Coeff_Parametric_Map[1][i]<<endl;
                }
                break;
        case 3:
                for (unsigned int i=0; i<Coeff_Parametric_Map[0].size(); i++)
                {
                        //ofAxel<<Coeff_Parametric_Map[0][i]<<" "<<Coeff_Parametric_Map[1][i]<<" "<<Coeff_Parametric_Map[2][i]<<endl;

                        ofCoeff<<Coeff_Parametric_Map[0][i]<<" "<<Coeff_Parametric_Map[1][i]<<endl;
                }
                break;
        default:
                for (unsigned int i=0; i<Coeff_Parametric_Map[0].size(); i++)
                {
                        //ofAxel<<Coeff_Parametric_Map[0][i]<<" "<<Coeff_Parametric_Map[1][i]<<" "<<Coeff_Parametric_Map[2][i]<<endl;

                        ofCoeff<<Coeff_Parametric_Map[0][i]<<" "<<Coeff_Parametric_Map[1][i]<<endl;
                }
                cout<<"the dimesion of parametricMap > 3"<<endl;
                break;

        }

        ofCoeff.close();

//        ofAxel<<"</points>"<<endl;
//        ofAxel<<"</mspline>"<<endl;
//        ofAxel<<"</axl>"<<endl;
//        ofAxel.close();
}

//========================================================================
static void JacobianValue(MSplineFunction* X, MSplineFunction* Y, const vector<Coordinate *> & P_Coordinates,
                          vector<double> &Values, const unsigned int IndexCellX, const unsigned int IndexCellY,
                          const Mesh * P_CurrentMeshX, const Mesh * P_CurrentMeshY)//J(x,y)
{
    vector<double> VXs, VXt, VYs, VYt;
    X->Evaluation(1, 0, P_Coordinates, VXs, IndexCellX, *P_CurrentMeshX);
    X->Evaluation(0, 1, P_Coordinates, VXt, IndexCellX, *P_CurrentMeshX);
    Y->Evaluation(1, 0, P_Coordinates, VYs, IndexCellY, *P_CurrentMeshY);
    Y->Evaluation(0, 1, P_Coordinates, VYt, IndexCellY, *P_CurrentMeshY);

    Values.clear();
    for(unsigned int i=0; i<P_Coordinates.size(); i++)
    {
        double Jv=VXs[i]*VYt[i]-VXt[i]*VYs[i];//VXs*VYt-VXt*VYs

       // cout<<"Jv = "<<Jv<<endl;

//        if(Jv<1.0e-10&&Jv>-1.0e-10)
//        {
//            cout<<setprecision(14)<<Jv<<endl;
//            cout<<setprecision(14)<<VXs[i]<<endl;
//            cout<<setprecision(14)<<VYt[i]<<endl;
//            cout<<setprecision(14)<<VXt[i]<<endl;
//            cout<<setprecision(14)<<VYs[i]<<endl;
//            cin.get();
//        }

        Values.push_back(Jv);
    }

//    cout<<"In JacobianValue:"<<endl;
//    for(unsigned int i=0; i<Values.size(); i++)
//    {
//        cout<<Values[i]<<endl;
//    }
//    cin.get();
}
//=======
static void JacobianValue_surface(MSplineFunction* X, MSplineFunction* Y, MSplineFunction *Z, const vector<Coordinate *> & P_Coordinates,
                          vector<double> &Values, const unsigned int IndexCellX, const unsigned int IndexCellY,const unsigned int IndexCellZ,
                          const Mesh * P_CurrentMeshX, const Mesh * P_CurrentMeshY, const Mesh * P_CurrentMeshZ)
{
    vector<double> VXs, VXt, VYs, VYt, VZs, VZt;
    X->Evaluation(1, 0, P_Coordinates, VXs, IndexCellX, *P_CurrentMeshX);
    X->Evaluation(0, 1, P_Coordinates, VXt, IndexCellX, *P_CurrentMeshX);
    Y->Evaluation(1, 0, P_Coordinates, VYs, IndexCellY, *P_CurrentMeshY);
    Y->Evaluation(0, 1, P_Coordinates, VYt, IndexCellY, *P_CurrentMeshY);
    Z->Evaluation(1, 0, P_Coordinates, VZs, IndexCellZ, *P_CurrentMeshZ);
    Z->Evaluation(0, 1, P_Coordinates, VZt, IndexCellZ, *P_CurrentMeshZ);

    Values.clear();
    for(unsigned int i=0; i<P_Coordinates.size(); i++)
    { //[xs,ys,zs] [xt, yt, zt]
        double G11=VXs[i]*VXs[i]+VYs[i]*VYs[i]+VZs[i]*VZs[i];
        double G12=VXs[i]*VXt[i]+VYs[i]*VYt[i]+VZs[i]*VZt[i];
        double G21=G12;
        double G22=VXt[i]*VXt[i]+VYt[i]*VYt[i]+VZt[i]*VZt[i];

        Values.push_back(sqrt(G11*G22-G12*G21));
    }
}

//================
static void DsJacobianValue(MSplineFunction* X, MSplineFunction* Y, const vector<Coordinate *> & P_Coordinates,
                            vector<double> &Values, const unsigned int IndexCellX, const unsigned int IndexCellY,
                            const Mesh * P_CurrentMeshX, const Mesh * P_CurrentMeshY)//DsJ(x,y)
{
    vector<double> VXs, VXss, VXst, VXt;
    X->Evaluation(1, 0, P_Coordinates, VXs, IndexCellX, *P_CurrentMeshX);
    X->Evaluation(2, 0, P_Coordinates, VXss, IndexCellX, *P_CurrentMeshX);
    X->Evaluation(1, 1, P_Coordinates, VXst, IndexCellX, *P_CurrentMeshX);
    X->Evaluation(0, 1, P_Coordinates, VXt, IndexCellX, *P_CurrentMeshX);

    vector<double> VYt, VYst, VYs, VYss;
    Y->Evaluation(0, 1, P_Coordinates, VYt, IndexCellY, *P_CurrentMeshY);
    Y->Evaluation(1, 1, P_Coordinates, VYst, IndexCellY, *P_CurrentMeshY);
    Y->Evaluation(1, 0, P_Coordinates, VYs, IndexCellY, *P_CurrentMeshY);
    Y->Evaluation(2, 0, P_Coordinates, VYss, IndexCellY, *P_CurrentMeshY);

    Values.clear();
    for(unsigned int i=0; i<P_Coordinates.size(); i++)
    {
        double Jv=VXss[i]*VYt[i]+VXs[i]*VYst[i]-VXst[i]*VYs[i]-VXt[i]*VYss[i];
        //VXss*VYt+VXs*VYst-VXst*VYs-VXt*VYss
        Values.push_back(Jv);
    }
}
//=======
static void DtJacobianValue(MSplineFunction* X, MSplineFunction* Y, const vector<Coordinate *> & P_Coordinates,
                            vector<double> &Values, const unsigned int IndexCellX, const unsigned int IndexCellY,
                            const Mesh * P_CurrentMeshX, const Mesh * P_CurrentMeshY)//DtJ(x,y)
{
    vector<double> VXst, VXs, VXtt, VXt;
    X->Evaluation(1, 1, P_Coordinates, VXst, IndexCellX, *P_CurrentMeshX);
    X->Evaluation(1, 0, P_Coordinates, VXs, IndexCellX, *P_CurrentMeshX);
    X->Evaluation(0, 2, P_Coordinates, VXtt, IndexCellX, *P_CurrentMeshX);
    X->Evaluation(0, 1, P_Coordinates, VXt, IndexCellX, *P_CurrentMeshX);

    vector<double> VYt, VYtt, VYs, VYst;
    Y->Evaluation(0, 1, P_Coordinates, VYt, IndexCellY, *P_CurrentMeshY);
    Y->Evaluation(0, 2, P_Coordinates, VYtt, IndexCellY, *P_CurrentMeshY);
    Y->Evaluation(1, 0, P_Coordinates, VYs, IndexCellY, *P_CurrentMeshY);
    Y->Evaluation(1, 1, P_Coordinates, VYst, IndexCellY, *P_CurrentMeshY);

    Values.clear();
    for(unsigned int i = 0; i < P_Coordinates.size(); i ++)
    {
        double Jv=VXst[i]*VYt[i]+VXs[i]*VYtt[i]-VXtt[i]*VYs[i]-VXt[i]*VYst[i];
        //VXst*VYt+VXs*VYtt-VXtt*VYs-VXt*VYst
        Values.push_back(Jv);
    }

}
//=======
static void DUxxValue_PLUS_DUyyValue(MSplineFunction* X, MSplineFunction* Y, MSplineFunction* U,
                      const vector<Coordinate *> P_Coordinates,
                      vector<double> &Values, const unsigned int IndexCellU,
                      const Mesh* P_MeshU, const Mesh* P_MeshXY)
{
    unsigned int IndexCellXY=P_MeshU->P_Cells->at(IndexCellU).ItsParent;
    vector<double> Xs, Xt;
    X->Evaluation(1, 0, P_Coordinates, Xs, IndexCellXY, *P_MeshXY);
    X->Evaluation(0, 1, P_Coordinates, Xt, IndexCellXY, *P_MeshXY);

    vector<double> Yt, Ys;
    Y->Evaluation(1, 0, P_Coordinates, Ys, IndexCellXY, *P_MeshXY);
    Y->Evaluation(0, 1, P_Coordinates, Yt, IndexCellXY, *P_MeshXY);

    vector<double> Jxy, JUy, JxU, DsJUy, DsJxU, DsJxy, DtJUy, DtJxU,  DtJxy;
    JacobianValue(X, Y, P_Coordinates, Jxy, IndexCellXY, IndexCellXY, P_MeshXY, P_MeshXY);
    JacobianValue(U, Y, P_Coordinates, JUy, IndexCellU, IndexCellXY, P_MeshU, P_MeshXY);
    JacobianValue(X, U, P_Coordinates, JxU, IndexCellXY, IndexCellU, P_MeshXY, P_MeshU);

    DsJacobianValue(U, Y, P_Coordinates, DsJUy, IndexCellU, IndexCellXY, P_MeshU, P_MeshXY);
    DsJacobianValue(X, U, P_Coordinates, DsJxU, IndexCellXY, IndexCellU, P_MeshXY, P_MeshU);
    DsJacobianValue(X, Y, P_Coordinates, DsJxy, IndexCellXY, IndexCellXY, P_MeshXY, P_MeshXY);

    DtJacobianValue(U, Y, P_Coordinates, DtJUy, IndexCellU, IndexCellXY, P_MeshU, P_MeshXY);
    DtJacobianValue(X, U, P_Coordinates, DtJxU, IndexCellXY, IndexCellU, P_MeshXY, P_MeshU);
    DtJacobianValue(X, Y, P_Coordinates, DtJxy, IndexCellXY, IndexCellXY, P_MeshXY, P_MeshXY);

    //=============
    Values.clear();
    for(unsigned int i=0; i<P_Coordinates.size(); i++)
    {

        double Uxx=Yt[i]*DsJUy[i]/(Jxy[i]*Jxy[i]);
        Uxx=Uxx-JUy[i]*DsJxy[i]*Yt[i]/(Jxy[i]*Jxy[i]*Jxy[i]);
        Uxx=Uxx-Ys[i]*DtJUy[i]/(Jxy[i]*Jxy[i]);
        Uxx=Uxx+JUy[i]*DtJxy[i]*Ys[i]/(Jxy[i]*Jxy[i]*Jxy[i]);




        double Uyy=Xs[i]*DtJxU[i]/(Jxy[i]*Jxy[i]);
        Uyy=Uyy-Xs[i]*JxU[i]*DtJxy[i]/(Jxy[i]*Jxy[i]*Jxy[i]);
        Uyy=Uyy-Xt[i]*DsJxU[i]/(Jxy[i]*Jxy[i]);
        Uyy=Uyy+Xt[i]*JxU[i]*DsJxy[i]/(Jxy[i]*Jxy[i]*Jxy[i]);

        Values.push_back(Uxx+Uyy);
//        if((Uxx+Uyy)!=(Uxx+Uyy))
//        {
//            if(Uxx!=Uxx)
//            {
//                cout<<"Uxx:"<<endl;
//                cout<<setprecision(14)<<Yt[i]<<endl;
//                cout<<setprecision(14)<<DsJUy[i]<<endl;
//                cout<<setprecision(14)<<Jxy[i]<<endl;
//                cout<<setprecision(14)<<JUy[i]<<endl;
//                cout<<setprecision(14)<<DsJxy[i]<<endl;
//            }

//            if(Uyy!=Uyy)
//            {
//                cout<<"Uyy:"<<endl;
//                cout<<setprecision(14)<<DtJxU[i]<<" "<<Jxy[i]<<" "<<DsJxU[i]<<" "<<JxU[i]<<" "<<DsJxy[i]<<endl;
//            }


//        }
        //
    }

}

//====================
static void DUx_AND_DUy(MSplineFunction* X, MSplineFunction* Y, MSplineFunction* U,
               const vector<Coordinate *> P_Coordinates,
                 vector<double> &VUx, vector<double> &VUy,
                 const unsigned int IndexCellU,
               const Mesh* P_MeshU, const Mesh* P_MeshXY)
{
    unsigned int IndexCellXY=P_MeshU->P_Cells->at(IndexCellU).ItsParent;
    vector<double> JXY;
    JacobianValue(X, Y, P_Coordinates, JXY, IndexCellXY, IndexCellXY, P_MeshXY, P_MeshXY);
    vector<double> JUY;
    JacobianValue(U, Y, P_Coordinates, JUY, IndexCellU, IndexCellXY, P_MeshU, P_MeshXY);
    vector<double> JXU;
    JacobianValue(X, U, P_Coordinates, JXU, IndexCellXY, IndexCellU, P_MeshXY, P_MeshU);

    VUx.clear();
    VUy.clear();

    for(unsigned int i=0; i<P_Coordinates.size(); i++)
    {
        double ux, uy;
        ux=JUY[i]/JXY[i];
        VUx.push_back(ux);

        uy=JXU[i]/JXY[i];
        VUy.push_back(uy);
    }
}

//=======================================================================================
//Determine the s-direction and t-direction associated with the given vertex (the i_vertex-th vertex)
//vector<unsigned int> S1=[v0, v1,i], S1[i]=i_vertex, T1;//s-direction and t-direction in the first Cell (store the indices of vertices)
//Ssize-->size of s-direction, Tsize-->size of t-direction
static void STDirectionWithGivenVertex(vector<unsigned int> &S1, vector<unsigned int> &T1,
                                       double& Ssize, double& Tsize, const unsigned int i_vertex,
                                       const Cell& TheCell)
{
    S1.clear();
    T1.clear();
    Ssize=TheCell.CellSize[1]-TheCell.CellSize[0];
    Tsize=TheCell.CellSize[3]-TheCell.CellSize[2];

    bool Key=false;
    unsigned int iv;
    for(iv=0; iv<TheCell.Index_ItsCornerVertices.size(); iv++)
    {
        if(TheCell.Index_ItsCornerVertices[iv]==i_vertex)
        {
            Key=true;
            break;
        }
    }

    if(Key)
    {
        switch (iv)
        {
        case 0://S1=[v0, v1,i], S1[i]=i_vertex
            S1.push_back(TheCell.Index_ItsCornerVertices[0]);
            S1.push_back(TheCell.Index_ItsCornerVertices[1]);
            S1.push_back(0);
            //T1=[v0, v1, i], T1[i]=i_vertex
            T1.push_back(TheCell.Index_ItsCornerVertices[0]);
            T1.push_back(TheCell.Index_ItsCornerVertices[3]);
            T1.push_back(0);
            break;
        case 1:
            S1.push_back(TheCell.Index_ItsCornerVertices[0]);
            S1.push_back(TheCell.Index_ItsCornerVertices[1]);
            S1.push_back(1);
            //T1
            T1.push_back(TheCell.Index_ItsCornerVertices[1]);
            T1.push_back(TheCell.Index_ItsCornerVertices[2]);
            T1.push_back(0);
            break;
        case 2:
            S1.push_back(TheCell.Index_ItsCornerVertices[3]);
            S1.push_back(TheCell.Index_ItsCornerVertices[2]);
            S1.push_back(1);
            //T1
            T1.push_back(TheCell.Index_ItsCornerVertices[1]);
            T1.push_back(TheCell.Index_ItsCornerVertices[2]);
            T1.push_back(1);
            break;
        case 3:
            S1.push_back(TheCell.Index_ItsCornerVertices[3]);
            S1.push_back(TheCell.Index_ItsCornerVertices[2]);
            S1.push_back(0);
            //T1
            T1.push_back(TheCell.Index_ItsCornerVertices[0]);
            T1.push_back(TheCell.Index_ItsCornerVertices[3]);
            T1.push_back(1);
            break;
        default:
            cout<<"Function.h::STDirectionWithGivenVertex: iv>3"<<endl;
            cin.get();
            break;
        }
    }
    else
    {
        cout<<"Function.h::STDirectionWithGivenVertex::Do not find the i_vertex vertex in the given cell"<<endl;
        cin.get();
    }
}

/*
 * Based on the structure of the mesh (*P_ParametricMesh), return the common cells at the vertices with indices as S1[0], S1[1]
*/
static void CommonCells(const Mesh* P_ParametricMesh, set<unsigned int>& CommonCellsS1,
                        const vector<unsigned int> &S1)
{
    CommonCellsS1.clear();

    set<unsigned int> CellSet0, CellSet1;

//    cout<<"S1.size = "<<S1.size()<<endl;
    //==========
    map<unsigned int, Coordinate>::const_iterator it0=P_ParametricMesh->P_Vertices->at(S1[0]).ItsCellsToCoordinatesMap.begin();
    map<unsigned int, Coordinate>::const_iterator it1=P_ParametricMesh->P_Vertices->at(S1[1]).ItsCellsToCoordinatesMap.begin();
    //==========
//    cout<<"CellSet0:"<<endl;
    for(; it0!=P_ParametricMesh->P_Vertices->at(S1[0]).ItsCellsToCoordinatesMap.end(); it0++)
    {
        CellSet0.insert(it0->first);
        //cout<<it0->first<<" ";
    }
//    cout<<endl;

//    cout<<"CellSet1:"<<endl;
    for(; it1!=P_ParametricMesh->P_Vertices->at(S1[1]).ItsCellsToCoordinatesMap.end(); it1++)
    {
        CellSet1.insert(it1->first);
        //cout<<it1->first<<" ";
    }
//    cout<<endl;
//    //==========
    set_intersection(CellSet0.begin(), CellSet0.end(), CellSet1.begin(), CellSet1.end(), inserter(CommonCellsS1, CommonCellsS1.begin()) );

//    cout<<"the intersection of CellSet0 and CellSet1"<<endl;
//    for(set<unsigned int>::const_iterator itc=CommonCellsS1.begin(); itc!=CommonCellsS1.end(); itc++)
//    {
//        cout<<*itc<<" ";
//    }
//    cout<<endl;

}

/*
 * Return a bool key: if the ivertex-th vertex as one of cell's vertice in CommonCellsS1, then
 * key=turn, otherwise key=false
*/
static bool Is_InCommonCells(const unsigned int ivertex, const set<unsigned int>& CommonCellsS1, const Mesh* P_ParametricMesh)
{
    bool key=false;

    set<unsigned int> IV;
    IV.insert(ivertex);

    set<unsigned int> VerticesSet;// CommonCellsS1 should have two elements

    for(set<unsigned int>::iterator it = CommonCellsS1.begin(); it!=CommonCellsS1.end(); it++)
    {
        unsigned int icell=*it;

//        //====Cout====
//        cout<<"the vertex indices of the "<<icell<<"-th cell"<<endl;
//        for(vector<unsigned int>::const_iterator itc=P_ParametricMesh->P_Cells->at(icell).Index_ItsCornerVertices.begin();
//            itc!=P_ParametricMesh->P_Cells->at(icell).Index_ItsCornerVertices.end(); itc++)
//        {
//            cout<<*itc<<" ";
//        }
//        cout<<endl;
//        //===EndCout===

       VerticesSet.insert(P_ParametricMesh->P_Cells->at(icell).Index_ItsCornerVertices.begin(),
                          P_ParametricMesh->P_Cells->at(icell).Index_ItsCornerVertices.end());
    }

//    //===Cout====
//    cout<<"the union of the vertex set:"<<endl;
//    for(set<unsigned int>::const_iterator itc=VerticesSet.begin(); itc!=VerticesSet.end(); itc++)
//    {
//        cout<<*itc<<" ";
//    }
//    cout<<endl;
//    //===EndCout===


    set<unsigned int> InterSection;
    set_intersection(IV.begin(), IV.end(), VerticesSet.begin(), VerticesSet.end(), inserter(InterSection, InterSection.begin()) );

//    //===Cout====
//    cout<<"the intersection of IV and VerticesSet"<<endl;
//    for(set<unsigned int>::const_iterator itc=InterSection.begin(); itc!=InterSection.end(); itc++)
//    {
//        cout<<*itc<<"  should be ivertex";
//    }
//    cout<<endl;
//    //===EndCout===



    if(InterSection.size())
    {
        key=true;
    }
    else
    {
        key=false;
    }
    return key;
}

//========
/* Input Two sets: WhatWeHaveVertexIndex and Index_ItsVertices
 * Output: the first element in the set Index_ItsVertices and it is not in the set WhatWeHaveVertexIndex
*/
static unsigned int VertexDoNotInVerticesSet(const set<unsigned int> &WhatWeHaveVertexIndex, const set<unsigned int> & Index_ItsVertices)
{
    unsigned int T2Vindex;

    set<unsigned int> SetDifference;

    set_difference(Index_ItsVertices.begin(),Index_ItsVertices.end(),
                   WhatWeHaveVertexIndex.begin(), WhatWeHaveVertexIndex.end(),
                   inserter(SetDifference, SetDifference.begin() ));
//    //===Cout=====
//    cout<<"WhatWeHaveVertexIndex:"<<endl;
//    for(set<unsigned int>::const_iterator itc=WhatWeHaveVertexIndex.begin(); itc!=WhatWeHaveVertexIndex.end();
//        itc++)
//    {
//        cout<<*itc<<endl;
//    }
//    cout<<endl;

//    cout<<"Index_ItsVertices:"<<endl;
//    for(set<unsigned int>::const_iterator itc=Index_ItsVertices.begin(); itc!=Index_ItsVertices.end();
//        itc++)
//    {
//        cout<<*itc<<endl;
//    }
//    cout<<endl;
//    //
//    cout<<"the difference of these two sets:"<<endl;
//    for(set<unsigned int>::const_iterator itc=SetDifference.begin(); itc!=SetDifference.end(); itc++)
//    {
//        cout<<*itc<<" ";
//    }
//    cout<<endl;
//    //====CoutEnd===

    if(SetDifference.size())
    {
        T2Vindex=*SetDifference.begin();
    }
    else
    {
        cout<<"Error::Functions.h::VertexDoNotInVerticesSet::SetDifference.size()=0"<<endl;
        cin.get();
    }

    return T2Vindex;

}

//==============
/*Input: S1, S2, T1, T2, Ssize1, Tsize1 and VerticesPosition
 * Algrithm: Based on VerticesPosition and the vertices with their indices in S1 S2 T1 T2
 * 	     Compute the coeff in x-direction and y-direction (DsX, DtX), (DsY, DtY)
 * Output: CoeffX.push_back(DsX), Coeff.push_back(DtX);
 * 	    CoeffY.push_back(DsY), Coeff.push_back(DtY)
 *
 * Remark: Here, we allow the size of one of S1, S2, T1, T2 is zero since this algrithm will be used to
 * 	   compute the Hermite data at a boundary vertex as well
*/
static void ComputeTheCoeffdata(const vector<unsigned int> &S1, const vector<unsigned int>& S2, const vector<unsigned int>& T1,
                         const vector<unsigned int>& T2, const double Ssize1, const double Tsize1,
                         const vector<vector<double> > & VerticesPosition,
                         vector<double> &CoeffX, vector<double>& CoeffY)
{
    //vector<double> DsXY, DtXY;
    //DsXY=0.5*(VerticesPosition[S1[1]]-VerticesPosition[S1[0]]+VerticesPosition[S2[1]]-VerticesPosition[S2[0]])/Ssize1
    //DtXY=0.5*(VerticesPosition[T1[1]]-VerticesPosition[T1[0]]+VerticesPosition[T2[1]]-VerticesPosition[T2[0]])/Tsize1
    //


    double DsX, DsY;
    if(S1.size() && S2.size())
    {
        if(S1[1]>4*VerticesPosition.size()-1)
        {
            cout<<S1[1]<<endl;
            cin.get();
            cin.get();
            cin.get();
        }
        if(S1[0]>4*VerticesPosition.size()-1)
        {
            cout<<S1[0]<<endl;
            cin.get();
            cin.get();
            cin.get();
        }
        if(S2[1]>4*VerticesPosition.size()-1)
        {
            cout<<S2[1]<<endl;
            cin.get();
            cin.get();
            cin.get();
        }
        if(S2[0]>4*VerticesPosition.size()-1)
        {
            cout<<S2[0]<<endl;
            cin.get();
            cin.get();
            cin.get();
        }


        DsX=0.5*(VerticesPosition[S1[1]][0]-VerticesPosition[S1[0]][0]+VerticesPosition[S2[1]][0]-VerticesPosition[S2[0]][0])/Ssize1;
        DsY=0.5*(VerticesPosition[S1[1]][1]-VerticesPosition[S1[0]][1]+VerticesPosition[S2[1]][1]-VerticesPosition[S2[0]][1])/Ssize1;
    }
    else//S1.size()==0 or S2.size()==0
    {
        if(S1.size()==0)
        {
            if(S2[1]>4*VerticesPosition.size()-1)
            {
                cout<<S1[1]<<endl;
                cin.get();
                cin.get();
                cin.get();
            }
            if(S2[0]>4*VerticesPosition.size()-1)
            {
                cout<<S1[0]<<endl;
                cin.get();
                cin.get();
                cin.get();
            }

            DsX=(VerticesPosition[S2[1]][0]-VerticesPosition[S2[0]][0])/Ssize1;
            DsY=(VerticesPosition[S2[1]][1]-VerticesPosition[S2[0]][1])/Ssize1;
        }
        else//S2.size()=0
        {
            if(S1[1]>4*VerticesPosition.size()-1)
            {
                cout<<S1[1]<<endl;
                cin.get();
                cin.get();
                cin.get();
            }
            if(S1[0]>4*VerticesPosition.size()-1)
            {
                cout<<S1[0]<<endl;
                cin.get();
                cin.get();
                cin.get();
            }
            DsX=(VerticesPosition[S1[1]][0]-VerticesPosition[S1[0]][0])/Ssize1;
            DsY=(VerticesPosition[S1[1]][1]-VerticesPosition[S1[0]][1])/Ssize1;
        }
    }

    //DsXY.push_back(DsX);
    //DsXY.push_back(DsY);

    double DtX, DtY;
    if(T1.size() && T2.size())
    {
        DtX=0.5*(VerticesPosition[T1[1]][0]-VerticesPosition[T1[0]][0]+VerticesPosition[T2[1]][0]-VerticesPosition[T2[0]][0])/Tsize1;
        DtY=0.5*(VerticesPosition[T1[1]][1]-VerticesPosition[T1[0]][1]+VerticesPosition[T2[1]][1]-VerticesPosition[T2[0]][1])/Tsize1;
    }
    else
    {
        if(T1.size()==0)
        {
            DtX=(VerticesPosition[T2[1]][0]-VerticesPosition[T2[0]][0])/Tsize1;
            DtY=(VerticesPosition[T2[1]][1]-VerticesPosition[T2[0]][1])/Tsize1;
        }
        else//T2.size()=0
        {
            DtX=(VerticesPosition[T1[1]][0]-VerticesPosition[T1[0]][0])/Tsize1;
            DtY=(VerticesPosition[T1[1]][1]-VerticesPosition[T1[0]][1])/Tsize1;
        }
    }

    //DtXY.push_back(DtX);
    //DtXY.push_back(DtY);


    //CoeffX
    CoeffX.push_back(DsX);
    CoeffX.push_back(DtX);
    CoeffX.push_back(0.0);

    //CoeffY
    CoeffY.push_back(DsY);
    CoeffY.push_back(DtY);
    CoeffY.push_back(0.0);
}
//===================================================================================
/*
 * Just keep the position information of Coeff_Parametric_Map
*/
static void ThePositionsInfo(vector<vector<double> >& CoeffVerticesPosition, const vector<vector<double> > &Coeff_Parametric_Map)
{
    CoeffVerticesPosition.clear();
    vector<double> CoeffVerticesPosition_i;

    for(unsigned int i = 0; i < Coeff_Parametric_Map.size(); i ++)
    {
        CoeffVerticesPosition_i.clear();
        for(unsigned int j = 0; j < Coeff_Parametric_Map[i].size(); j ++)
        {
            if(j%4==0)
            {
                CoeffVerticesPosition_i.push_back(Coeff_Parametric_Map[i][j]);
            }
            else
            {
                CoeffVerticesPosition_i.push_back(0.0);
            }

        }
        CoeffVerticesPosition.push_back(CoeffVerticesPosition_i);

    }
}

//===============================================================================
/*
 * Print a Parametric Map's Position Information
 *  (just keep the positioninfo and other hermite data set as zeros)
*/
static void PrintParametricMapPositionInfo(const vector<vector<double> >& Coeff_Parametric_Map, const Mesh * P_ParametricMesh, char * filename)
{
    vector<vector<double> > CoeffVerticesPosition;
    ThePositionsInfo(CoeffVerticesPosition, Coeff_Parametric_Map);

    ofstream ofAxel;

    ofAxel.open(filename);
    ofAxel<<"<axl>"<<endl;
    ofAxel<<"<mspline type=\"InitialMesh\">"<<endl;
    ofAxel<<"<vertexnumber> "<<P_ParametricMesh->P_Vertices->size()<<"</vertexnumber>"<<endl;



    for (unsigned int i=0; i<P_ParametricMesh->P_Cells->size(); i++)
    {
            ofAxel<<"<cell>"<<endl;
            ofAxel<<P_ParametricMesh->P_Cells->at(i).CellSize[0]<<" "<<P_ParametricMesh->P_Cells->at(i).CellSize[1]<<" "<<P_ParametricMesh->P_Cells->at(i).CellSize[2]<<" "<<P_ParametricMesh->P_Cells->at(i).CellSize[3]<<endl;
            ofAxel<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[0]<<" "<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[1]<<" "<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[2]<<" "<<P_ParametricMesh->P_Cells->at(i).Index_ItsCornerVertices[3]<<endl;
            ofAxel<<"</cell>"<<endl;
    }

    for (unsigned int i=0; i<P_ParametricMesh->P_Cells->size(); i++)
    {
            for (unsigned int j=0; j<i; j++)
            {
                unsigned int J=P_ParametricMesh->P_Cells->at(j).Index_InitialCell;
                unsigned int I=P_ParametricMesh->P_Cells->at(i).Index_InitialCell;
                double o11=(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O11;
                double o12=(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O12;
                double o21=(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O21;
                double o22=(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O22;
                double tol=(o11-1.0)*(o11-1.0)+o12*o12+o21*o21+(o22-1.0)*(o22-1.0);
                if(tol>1.0e-2)
                {
                    ofAxel<<"<transition cells=\""<<i<<" "<<j<<"\">"<<endl;
                    ofAxel<<(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O11<<" "<<(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O12<<" "<<(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O21<<" "<<(P_ParametricMesh->P_FrameTranMatrix->at(I))[J].O22<<endl;
                    ofAxel<<"</transition>"<<endl;
                }

            }
    }

    ofAxel<<"<points>"<<endl;

    switch(CoeffVerticesPosition.size())
    {
    case 0:
            cout<<"There is no point!!"<<endl;
            break;
    case 1:
            for (unsigned int i=0; i<CoeffVerticesPosition[0].size(); i++)
            {
                ofAxel<<CoeffVerticesPosition[0][i]<<" "<<0.0<<" "<<0.0<<endl;
            }
            break;
    case 2:
            for (unsigned int i=0; i<CoeffVerticesPosition[0].size(); i++)
            {
                ofAxel<<CoeffVerticesPosition[0][i]<<" "<<CoeffVerticesPosition[1][i]<<" "<<0.0<<endl;
            }
            break;
    case 3:
            for (unsigned int i=0; i<CoeffVerticesPosition[0].size(); i++)
            {
                ofAxel<<CoeffVerticesPosition[0][i]<<" "<<CoeffVerticesPosition[1][i]<<" "<<CoeffVerticesPosition[2][i]<<endl;
            }
            break;
    default:
            for (unsigned int i=0; i<CoeffVerticesPosition[0].size(); i++)
            {
                ofAxel<<CoeffVerticesPosition[0][i]<<" "<<CoeffVerticesPosition[1][i]<<" "<<CoeffVerticesPosition[2][i]<<endl;
            }
            cout<<"the dimesion of parametricMap > 3"<<endl;
            break;

    }

    ofAxel<<"</points>"<<endl;
    ofAxel<<"</mspline>"<<endl;
    ofAxel<<"</axl>"<<endl;
    ofAxel.close();

}



///*
// * OutPut the condition number of the PETSC Matrix *PCoeffMatrix
//*/
//static void ConditionNumberofAMatrix(int argc,char **args, const Mat *PCoeffMatrix, ofstream ofProgress)
//{


//}

/**/
//static void ComputeEigvalues(Mat &coeff_matrix, ofstream & ofProgress)
//{
//    cout<<"ComputeEigvalues()::the size of matrix<=500, this function will present more exact eigvalues."<<endl;

//       cout<<"Here1:Functions.h"<<endl;
//    KSP ksp;
//    PC pc;
////    Vec m_solut;
////    VecCreate(PETSC_COMM_WORLD, &m_solut);
////    VecSetSizes(m_solut, PETSC_DECIDE, 4*P_FEMMesh->P_Vertices->size());
////    VecSetFromOptions(m_solut);



//    KSPCreate(PETSC_COMM_WORLD, &ksp);
//    KSPSetType(ksp, KSPGMRES);
//    KSPSetOperators(ksp, coeff_matrix, coeff_matrix, DIFFERENT_NONZERO_PATTERN);

//    KSPGetPC(ksp, &pc);
//    PCSetType(pc, PCGAMG);
//    KSPSetFromOptions(ksp);

//    cout<<"KSPSetComputeEigenvalues():Functions.h"<<endl;

//    KSPSetComputeEigenvalues(ksp, PETSC_TRUE);

//    KSPSetUp(ksp);

//   // // //====KSPSetComputeEigenvalues()
//   // // //=============================
////    KSPSolve(ksp, load_vect, m_solut);
////    KSPGetConvergedReason(ksp, &reason);

//    //======KSPComputeEigenvalues()
//     PetscInt numberEigen;
//     PetscInt numberEigen2;
//     MatGetSize(coeff_matrix,&numberEigen, &numberEigen2);

//     PetscReal r[numberEigen];
//     PetscReal c[numberEigen];

//    KSPComputeEigenvaluesExplicitly(ksp, numberEigen, r, c);
//cout<<"Functions.h: Here~ ~ ~"<<endl;

//    double maxNorm, minNorm;
//    maxNorm=0;
//    minNorm=0;
//    if(numberEigen>0)
//    {
//        minNorm=sqrt(r[0]*r[0]+c[0]*c[0]);
//    }


//    ofProgress<<"EigenValues of this stiff matrix:"<<endl;
//    for(unsigned int j=0; j<numberEigen; j++)
//    {
//        double l=sqrt(r[j]*r[j]+c[j]*c[j]);
//        if(l>maxNorm)
//        {
//            maxNorm=l;
//        }
//        if(l<minNorm)
//        {
//            minNorm=l;
//        }

//        ofProgress<<r[j]<<" + "<<c[j]<<" i"<<endl;
//    }

//    ofProgress<<endl;
//    ofProgress<<"MaxNorm = "<<maxNorm<<endl;
//    ofProgress<<"MinNorm = "<<minNorm<<endl;

//     //========================

//    //Destroy ksp coeff_matrix load_vec m_solut
//    KSPDestroy(&ksp);
//    MatDestroy(&coeff_matrix);
//    //VecDestroy(&load_vect);
//    //VecDestroy(&m_solut);
//    PCDestroy(&pc);
//}







#endif
