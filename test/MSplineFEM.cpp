#include <cmath>
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <time.h>
using namespace std;

#include "HermiteCell.h"
#include "HermiteVertex.h"
#include "bases.h"
#include "node.h"
#include "hermitetransfermatrix.h"
#include "BasesAxel.h"
#include "splinefunction.h"
#include "mesh.h"

#include "SearchBasisFEM.h"
#include "FunctionsFEM.h"
#include <stdio.h>
#include <stdlib.h>
#include <WTypes.h>
#include "BSpline.h"

#include "MatlabSolutionDisplay.h"
void ReadParametricMapCoeff(string file, vector<vector<double>>ParametricMapCoeff, int numbasis, int dimPara)
{
	ParametricMapCoeff.clear();
	ifstream infile(file.c_str());
	if(infile == NULL)//Do not find this file
	{
		cout<< "There is no elements in Initial Mesh File:" << file <<endl;
	}
	else
	{
		vector<double> Coeff_i;
		for (int i=0; i<dimPara; i++)
		{
			ParametricMapCoeff.push_back(Coeff_i);
		}
		for (int i=0; i<numbasis; i++)
		{
			for(int j=0; j<dimPara; j++)
			{
				double putin;
				infile>>putin;
				ParametricMapCoeff[j].push_back(putin);
			};
		}
		//-----------------------

	};
};

void main()
{
	//===========Jacobian non-trivial===============================
	//------MSpline Files----------
	cout<<"Read the Mesh:"<<endl;
	//-------Parametric Map--------
	Mesh CurrentParametricMesh;
	CurrentParametricMesh->ReadFile_withoutHangingVertices("ParametricMesh.txt");
	Bases* P_CurrentParametricBases;
	P_CurrentParametricBases->Bases(CurrentParametricMesh);
	vector<vector<double>> ParametricMapCoeff;//ParametricMapCoeff[0]: For X-Direction
												//ParametricMapCoeff[1]: For Y-Direction
	int numbasis=0;
	for (unsigned int i=0; i<P_CurrentParametricBases->BasesSet.size();i++)
	{
		numbasis=numbasis+P_CurrentParametricBases.BasesSet[i].Base_Vertex.size();
	}
	int dimPara=2;
	ReadParametricMapCoeff("ParametricCoeffs.txt", ParametricMapCoeff, numbasis, dimPara);
	SplineFunctions MSplineParametricMap;
	MSplineParametricMap.ResetSpFunctions(ParametricMapCoeff,*P_CurrentParametricBases, CurrentParametricMesh);
	//delete P_CurrentParametricMesh;
	delete P_CurrentParametricBases;
	//-------------------------------


	//FEM Mesh, Bases
	Mesh CurrentMesh;
	CurrentMesh.ReadFile_withoutHangingVertices("FEMMesh.txt");
	cout<<"Generate the Bases:"<<endl;
	Bases CurrentBases(CurrentMesh);
	vector<unsigned int> BasesType;
	vector<Coordinate> VerticeImageCoordinates;
	SetBasesType(CurrentBases, CurrentMesh, BasesType, VerticeImageCoordinates);
	vector< vector<vector<unsigned int> > > CellBasisIndex;
	search_basis(CurrentMesh.Cells.size(), CurrentBases, CellBasisIndex);

	//FEM Boundary Condition
	//Const C along the boundary 
	double C=0.0;//0.0--->Homogenous function

	//---FEM part----------------------------------------
	cout<<"Solving........:"<<endl;
	//GuassPoints/////
	vector<double> gausspt, m_loadw, m_Weight, m_IntePt;
	GaussIntegalPoint(u_gaussnum, m_Weight,m_IntePt);
	GaussIntegalPoint(5, m_loadw, gausspt);

	//Weights/////////
	double **w;
	w = new double *[u_gaussnum];
	for (int i=0; i < u_gaussnum; i++)
	{
		w[i] = new double[u_gaussnum];
	};
	for (unsigned int i=0; i<u_gaussnum; i++)
	{
		for (int j=0; j<u_gaussnum; j++)
		{
			w[i][j]=m_Weight[i]*m_Weight[j];
		};
	};


	////////////////////////
	int matrix_size;
	vector<double> load_vect, m_solut;
	vector<vector<double> > coeff_matrix;
	//----Solving---------------
	coeff_matrix.clear();
	load_vect.clear();
	m_solut.clear();

	//coeff_matrix
	cout<<"Generate Coeff_Matrix"<<endl;
	matrix_size=0;
	for (unsigned int i=0; i<CurrentBases.BasesSet.size();i++)
	{
		matrix_size=matrix_size+CurrentBases.BasesSet[i].Base_Vertex.size();
	};

	coeff_matrix.resize(matrix_size);
	for (unsigned int i=0; i<matrix_size; i++)
	{
		coeff_matrix[i].resize(matrix_size);
	};
	for (unsigned int k=0; k<matrix_size; k++)
	{
		for (unsigned int l=0; l<matrix_size; l++)
		{
			coeff_matrix[k][l]=0.0;
		};
	};
	generate_matrix_MSplineParametricMap(CurrentBases, coeff_matrix, m_IntePt, m_IntePt, w, CurrentMesh, CellBasisIndex, CurrentParametricMesh, MSplineParametricMap);
	//load_vect

	cout<<"Generate the load vector"<<endl;
	load_vect.resize(matrix_size);
	for(int i=0; i<matrix_size; i++)
	{
		load_vect[i]=0.0;
	};

	load_vector_MSplineParametricMap(load_vect, gausspt, m_loadw, CurrentMesh, CurrentBases, CellBasisIndex, CurrentParametricMesh);

//	cout<<"Original load_vect:"<<endl;
// 	for(unsigned int ii=0; ii<load_vect.size(); ii++)
// 	{
// 		cout<<ii<<"--->"<<load_vect[ii]<<endl;
// 	};

	for(int i=0; i<matrix_size; i++)
	{
		if(BasesType[i]!=0)
		{
			if (BasesType[i]==1)
			{
				load_vect[i]=C;
				//modify other load_vect's elements
				for(unsigned int ii=0; ii<load_vect.size(); ii++)
				{
					if (BasesType[ii]==0)
					{
						if (ii<i)
						{
							load_vect[ii]=load_vect[ii]-C*coeff_matrix[ii][i-ii];//M[ii][i-ii]
						}
						if (ii>i)
						{
							load_vect[ii]=load_vect[ii]-C*coeff_matrix[i][ii-i];
						}
					}

				};

			}
			else
			{
				if (BasesType[i])
				{
					load_vect[i]=0.0;
				}
				
			};
		};
	};

// 	cout<<"Modified load_vect:"<<endl;
// 	for(unsigned int ii=0; ii<load_vect.size(); ii++)
// 	{
// 		cout<<ii<<"--->"<<load_vect[ii]<<endl;
// 	};

	//Modified the elements according to the basis with non-trivial boundary value
	int m_boundbasenum=0;

	for (int i=0; i<matrix_size; i++)
	{
		if (BasesType[i])
		{
			m_boundbasenum++;
			for (int j=0; j<matrix_size; j++)
			{
				coeff_matrix[i][j] = 0.0;
				if (i-j>=0)
				{
					coeff_matrix[j][i-j]=0.0;
				}
			};
			coeff_matrix[i][0]=1.0;
		};
	};

// 	cout<<"Modified Coeff_Matrix:"<<endl;
// 	for (unsigned int ii=0; ii<coeff_matrix.size(); ii++)
// 	{
// 		for(unsigned int jj=0; jj<coeff_matrix[ii].size(); jj++)
// 		{
// 			cout<<coeff_matrix[ii][jj]<<" ";
// 		}
// 
// 		cout<<endl;
// 	}


	// 		ofstream ofload;
	// 		ofload.open("load.txt");
	// 		for (int i=0;i<matrix_size;i++)
	// 		{
	// 			ofload<<load_vect[i]<<"  "<<endl;
	// 		}
	// 		ofload<<endl;
	// 		ofload.close();




	
// 		ofstream ofmatrix;
// 		ofmatrix.open("matrix.txt");
// 		for (int i=0;i<matrix_size;i++)
// 		{
// 			for(int j=0;j<matrix_size;j++)
// 				ofmatrix<<coeff_matrix[i][j]<<"  ";
// 			ofmatrix<<endl;
// 		}
// 		ofmatrix.close();


	cout<<"solving Linear Equation:"<<endl;
	//===============================
	m_solut.resize(matrix_size);
	LUsolve(matrix_size,matrix_size,coeff_matrix,load_vect,m_solut);
// 	//m_solut---->coeff of basis functions
// 	//===============================
	//delete coeff_matrix;
	//delete load_vect;
	//===============================

		ofstream ofsolut;
		ofsolut.open("coeff.txt");
		for (int i=0; i<matrix_size; i++)
		{
			
			ofsolut<<m_solut[i]<<endl;
		}
		ofsolut.close();
	//================================


// 	//Error----
// 	vector<double> cell_errors;
// 	ComputError(gausspt,gausspt, w, m_solut, cell_errors, m_cell, Basis);


	//[u0, v0]X[u1, v1]
// 	//Example1:===========================
// 	double u0=0;
// 	double u1=2*3.1415926;
// 	double v0=1;
// 	double v1=2;
// 	//=====================================
		cout<<"Write to MatlabeMesh:"<<endl;
	//Example3:===========================
	double u0=0;
	double u1=2*3.1415926;
	double v0=0;
	double v1=1;
	//=====================================
	unsigned int PointDensity=40;
	 WriteXYZMatrix(u0, u1, v0, v1, PointDensity, m_solut, CurrentBases, CurrentMesh);
	system("PAUSE");
	//the Solution Writer
};


// 
// //Testing Program 
// void main()
// {
// 	//====Testing LUSolver======================================
// 	vector<vector<double> > Matrix_Test;
// 	vector<double> MTest;
// 	MTest.push_back(0.0); MTest.push_back(0.0);
// 	Matrix_Test.push_back(MTest); Matrix_Test.push_back(MTest);
// 	MTest[0]=1; MTest[1]=1;
// 	Matrix_Test[0][0]=1.0; Matrix_Test[0][1]=1.5;
// 	Matrix_Test[1][0]=1.0;
// 	vector<double> soluTest;
// 	soluTest.push_back(0.0);soluTest.push_back(0.0);
// 	LUsolve(2,2,Matrix_Test,MTest,soluTest);
// 	for (unsigned int i=0; i<soluTest.size(); i++)
// 	{
// 		cout<<soluTest[i]<<endl;
// 	}
// 	//==========================================================
// 
// 	double time_Start, time_Finish;
// 	//===========Jacobian trivial===============================
// 	//------MSpline Files----------
// 	cout<<"Reader The Mesh:"<<endl;
// 	time_Start=(double)clock();//Start time
// 	//----
// 	Mesh CurrentMesh;
// 	CurrentMesh.ReadFile_withoutHangingVertices("TensorProductMesh.txt");
// 	time_Finish=(double)clock();//Finish time
// 	cout<<"Finished and It costs "<<(time_Finish-time_Start)/1000.0<<" s"<<endl;
// 	cout<<endl;
// 	//-----
// 
// 	cout<<"Generating Bases:"<<endl;
// 	time_Start=(double)clock();//Start time
// 	//----
// 	Bases CurrentBases(CurrentMesh);
// 	//----
// 	time_Finish=(double)clock();//Finish time
// 	cout<<"Finished and It costs "<<(time_Finish-time_Start)/1000.0<<" s"<<endl;
// 	cout<<endl;
// 
// 	cout<<"Set Basis's Type"<<endl;
// 	time_Start=(double)clock();//Start time
// 	//------
// 	vector<unsigned int> BasesType;
// 	SetBasesType(CurrentBases, CurrentMesh, BasesType);
// 	//------
// 	time_Finish=(double)clock();//Finish time
// 	cout<<"Finished and It costs "<<(time_Finish-time_Start)/1000.0<<" s"<<endl;
// 	cout<<endl;
// 
// 	vector<vector<double> > coeff_matrix;
// 
// 	cout<<"Search_Basis:"<<endl;
// 	time_Start=(double)clock();//Start time
// 	//-------
// 	vector< vector<vector<unsigned int> > > CellBasisIndex;
// 	search_basis(CurrentMesh.Cells.size(), CurrentBases, CellBasisIndex);
// 	//------
// 	time_Finish=(double)clock();//Finish time
// 	cout<<"Finished and It costs "<<(time_Finish-time_Start)/1000.0<<" s"<<endl;
// 	cout<<endl;
// 
// 	//Const C along the boundary 
// 	double C=0.0;//0.0--->Homogenous function
// 
// 	//Example: F=1.0
// 	C=1.0;
// 
// 	//---FEM part----------------------------------------
// 	//GuassPoints/////
// 	vector<double> gausspt, m_loadw, m_Weight, m_IntePt;
// 	GaussIntegalPoint(u_gaussnum, m_Weight,m_IntePt);
// 	GaussIntegalPoint(5,m_loadw,gausspt);
// 
// 	//Weights/////////
// 	double **w;
// 	w = new double *[u_gaussnum];
// 	for (int i=0; i < u_gaussnum; i++)
// 	{
// 		w[i] = new double[u_gaussnum];
// 	};
// 	for (unsigned int i=0; i<u_gaussnum; i++)
// 	{
// 		for (int j=0; j<u_gaussnum; j++)
// 		{
// 			w[i][j]=m_Weight[i]*m_Weight[j];
// 		};
// 	};
// 
// 	int matrix_size;
// 	vector<double> load_vect, m_solut, error_Tk;
// 	////////////////////////
// 
// 	//----Solving---------------
// 	coeff_matrix.clear();
// 	load_vect.clear();
// 	m_solut.clear();
// 
// 	//coeff_matrix
// 	cout<<"Generate Matrix:"<<endl;
// 	time_Start=(double)clock();//Start time
// 	matrix_size=0;
// 	for (unsigned int i=0; i<CurrentBases.BasesSet.size();i++)
// 	{
// 		matrix_size=matrix_size+CurrentBases.BasesSet[i].Base_Vertex.size();
// 	};
// 
// 	coeff_matrix.resize(matrix_size);
// 	for (unsigned int i=0; i<matrix_size; i++)
// 	{
// 		coeff_matrix[i].resize(matrix_size);
// 	};
// 	for (unsigned int k=0; k<matrix_size; k++)
// 	{
// 		for (unsigned int l=0; l<matrix_size; l++)
// 		{
// 			coeff_matrix[k][l]=0.0;
// 		};
// 	};
// 
// 	generate_matrix_WithoutJacobian(CurrentBases, coeff_matrix, m_IntePt, m_IntePt, w, CurrentMesh, CellBasisIndex);
// 	//------
// 	time_Finish=(double)clock();//Finish time
// 	cout<<"Finished and It costs "<<(time_Finish-time_Start)/1000.0<<" s"<<endl;
// 	cout<<endl;
// 
// 
// 	//load_vect
// 	cout<<"Generate Load Vector:"<<endl;
// 	time_Start=(double)clock();//Start time
// 	//----
// 	load_vect.resize(matrix_size);
// 	for(int i=0; i<matrix_size; i++)
// 	{
// 		load_vect[i]=0.0;
// 	};
// 	load_vector_WithoutJacobian(load_vect, gausspt, m_loadw, CurrentMesh, CurrentBases, CellBasisIndex);
// 
// 	for(int i=0; i<matrix_size; i++)
// 	{
// 		if(BasesType[i]!=0)
// 		{
// 			//if Basis.Base[i].edge==1, load_vect[i]=C and modify other load_vect
// 			if (BasesType[i]==1)
// 			{
// 				load_vect[i]=C;
// 				//modify other load_vect's elements
// 				for(unsigned int ii=0; ii<load_vect.size(); ii++)
// 				{
// 					if (BasesType[ii]==0)
// 					{
// 						if (ii<i)
// 						{
// 							load_vect[ii]=load_vect[ii]-C*coeff_matrix[ii][i-ii];//M[ii][i-ii]
// 						}
// 						if (ii>i)
// 						{
// 							load_vect[ii]=load_vect[ii]-C*coeff_matrix[i][ii-i];
// 						}
// 					}
// 
// 				};
// 
// 			}
// 			else
// 			{
// 				if (BasesType[i])
// 				{
// 					load_vect[i]=0.0;
// 				}
// 
// 			}
// 			//if Basis.Base[i].edge==2 and 3, load_vect[i]=0.0;
// 
// 		}
// 	};
// 	//------
// 	time_Finish=(double)clock();//Finish time
// 	cout<<"Finished and It costs "<<(time_Finish-time_Start)/1000.0<<" s"<<endl;
// 	cout<<endl;
// 
// 
// 
// 	// 		//Modified the elements according to the basis with non-trivial boundary value
// 	int m_boundbasenum=0;
// 
// 	for (int i=0; i<matrix_size; i++)
// 	{
// 		if (BasesType[i])
// 		{
// 			m_boundbasenum++;
// 			for (int j=0; j<matrix_size; j++)
// 			{
// 				coeff_matrix[i][j] = 0.0;
// 				if (i-j>=0)
// 				{
// 					coeff_matrix[j][i-j]=0.0;
// 				}
// 			};
// 			coeff_matrix[i][0]=1.0;
// 		};
// 	};
// 
// 	//===============================
// 	cout<<"Solving FEM equation:"<<endl;
// 	time_Start=(double)clock();//Start time
// 	m_solut.resize(matrix_size);
// 	LUsolve(matrix_size,matrix_size,coeff_matrix,load_vect,m_solut);
// 
// 	//------
// 	time_Finish=(double)clock();//Finish time
// 	cout<<"Finished and It costs "<<(time_Finish-time_Start)/1000.0<<" s"<<endl;
// 	cout<<endl;
// 
// 	// 	//m_solut---->coeff of basis functions
// 	// 	//===============================
// 	//delete coeff_matrix;
// 	//delete load_vect;
// 	//===============================
// 
// 	ofstream ofsolut;
// 	ofsolut.open("coeff.txt");
// 	for (int i=0; i<matrix_size; i++)
// 	{
// 
// 		ofsolut<<m_solut[i]<<endl;
// 	}
// 	ofsolut.close();
// 	//================================
// 
// 
// 	// 	//Error----
// 	// 	vector<double> cell_errors;
// 	// 	ComputError(gausspt,gausspt, w, m_solut, cell_errors, m_cell, Basis);
// 
// 
// 	cout<<"WriterToMatlab:"<<endl;
// 	time_Start=(double)clock();//Start time
// 	//[u0, v0]X[u1, v1]
// 	//Example3:===========================
// 	double u0=0;
// 	double u1=1;
// 	double v0=0;
// 	double v1=1;
// 	//=====================================
// 	unsigned int PointDensity=40;
// 	WriteXYZMatrix_WithoutJacobian(u0, u1, v0, v1, PointDensity, m_solut, CurrentBases, CurrentMesh);
// 	//------
// 	time_Finish=(double)clock();//Finish time
// 	cout<<"Finished and It costs "<<(time_Finish-time_Start)/1000.0<<" s"<<endl;
// 	cout<<endl;
// 
// 	system("PAUSE");
// 	//the Solution Writer
// };
