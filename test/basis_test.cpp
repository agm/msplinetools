#include <cmath>
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <time.h>
using namespace std;

#include "msp/HermiteCell.h"
#include "msp/HermiteVertex.h"
#include "msp/bases.h"
#include "msp/node.h"
#include "msp/hermitetransfermatrix.h"
#include "msp/BasesAxel.h"
#include "msp/splinefunction.h"
#include "msp/mesh.h"
#include "msp/BSpline.h"


#include "mspFEM/SearchBasisFEM.h"
#include "mspFEM/FunctionsFEM.h"
#include <stdio.h>
#include <stdlib.h>
#include <WTypes.h>


//#include "MatlabSolutionDisplay.h"
void ReadParametricMapCoeff(string file, vector<vector<double>>ParametricMapCoeff, int numbasis, int dimPara)
{
    ParametricMapCoeff.clear();
    ifstream infile(file.c_str());
    if(infile == NULL)//Do not find this file
    {
        cout<< "There is no elements in Initial Mesh File:" << file <<endl;
    }
    else
    {
        vector<double> Coeff_i;
        for (int i=0; i<dimPara; i++)
        {
            ParametricMapCoeff.push_back(Coeff_i);
        }
        for (int i=0; i<numbasis; i++)
        {
            for(int j=0; j<dimPara; j++)
            {
                double putin;
                infile>>putin;
                ParametricMapCoeff[j].push_back(putin);
            };
        }
        //-----------------------

    };
};

void main()
{
    //===========Jacobian non-trivial===============================
    //------MSpline Files----------
    cout<<"Read the Mesh:"<<endl;
    //-------Parametric Map--------
    Mesh CurrentParametricMesh;
    CurrentParametricMesh->ReadFile_withoutHangingVertices("ParametricMesh.txt");
    Bases* P_CurrentParametricBases;
    P_CurrentParametricBases->Bases(CurrentParametricMesh);
    vector<vector<double>> ParametricMapCoeff;//ParametricMapCoeff[0]: For X-Direction
                                                //ParametricMapCoeff[1]: For Y-Direction
    int numbasis=0;
    for (unsigned int i=0; i<P_CurrentParametricBases->BasesSet.size();i++)
    {
        numbasis=numbasis+P_CurrentParametricBases.BasesSet[i].Base_Vertex.size();
    }
    int dimPara=2;
    ReadParametricMapCoeff("ParametricCoeffs.txt", ParametricMapCoeff, numbasis, dimPara);
    SplineFunctions MSplineParametricMap;
    MSplineParametricMap.ResetSpFunctions(ParametricMapCoeff,*P_CurrentParametricBases, CurrentParametricMesh);
    //delete P_CurrentParametricMesh;
    delete P_CurrentParametricBases;
    //-------------------------------


    //FEM Mesh, Bases
    Mesh CurrentMesh;
    CurrentMesh.ReadFile_withoutHangingVertices("FEMMesh.txt");
    cout<<"Generate the Bases:"<<endl;
    Bases CurrentBases(CurrentMesh);
    vector<unsigned int> BasesType;
    vector<Coordinate> VerticeImageCoordinates;
    SetBasesType(CurrentBases, CurrentMesh, BasesType, VerticeImageCoordinates);
    vector< vector<vector<unsigned int> > > CellBasisIndex;
    search_basis(CurrentMesh.Cells.size(), CurrentBases, CellBasisIndex);

    //FEM Boundary Condition
    //Const C along the boundary
    double C=0.0;//0.0--->Homogenous function

    //---FEM part----------------------------------------
    cout<<"Solving........:"<<endl;
    //GuassPoints/////
    vector<double> gausspt, m_loadw, m_Weight, m_IntePt;
    GaussIntegalPoint(u_gaussnum, m_Weight,m_IntePt);
    GaussIntegalPoint(5, m_loadw, gausspt);

    //Weights/////////
    double **w;
    w = new double *[u_gaussnum];
    for (int i=0; i < u_gaussnum; i++)
    {
        w[i] = new double[u_gaussnum];
    };
    for (unsigned int i=0; i<u_gaussnum; i++)
    {
        for (int j=0; j<u_gaussnum; j++)
        {
            w[i][j]=m_Weight[i]*m_Weight[j];
        };
    };


    ////////////////////////
    int matrix_size;
    vector<double> load_vect, m_solut;
    vector<vector<double> > coeff_matrix;
    //----Solving---------------
    coeff_matrix.clear();
    load_vect.clear();
    m_solut.clear();

    //coeff_matrix
    cout<<"Generate Coeff_Matrix"<<endl;
    matrix_size=0;
    for (unsigned int i=0; i<CurrentBases.BasesSet.size();i++)
    {
        matrix_size=matrix_size+CurrentBases.BasesSet[i].Base_Vertex.size();
    };

    coeff_matrix.resize(matrix_size);
    for (unsigned int i=0; i<matrix_size; i++)
    {
        coeff_matrix[i].resize(matrix_size);
    };
    for (unsigned int k=0; k<matrix_size; k++)
    {
        for (unsigned int l=0; l<matrix_size; l++)
        {
            coeff_matrix[k][l]=0.0;
        };
    };
    generate_matrix_MSplineParametricMap(CurrentBases, coeff_matrix, m_IntePt, m_IntePt, w, CurrentMesh, CellBasisIndex, CurrentParametricMesh, MSplineParametricMap);
    //load_vect

    cout<<"Generate the load vector"<<endl;
    load_vect.resize(matrix_size);
    for(int i=0; i<matrix_size; i++)
    {
        load_vect[i]=0.0;
    };

    load_vector_MSplineParametricMap(load_vect, gausspt, m_loadw, CurrentMesh, CurrentBases, CellBasisIndex, CurrentParametricMesh);

//	cout<<"Original load_vect:"<<endl;
// 	for(unsigned int ii=0; ii<load_vect.size(); ii++)
// 	{
// 		cout<<ii<<"--->"<<load_vect[ii]<<endl;
// 	};

    for(int i=0; i<matrix_size; i++)
    {
        if(BasesType[i]!=0)
        {
            if (BasesType[i]==1)
            {
                load_vect[i]=C;
                //modify other load_vect's elements
                for(unsigned int ii=0; ii<load_vect.size(); ii++)
                {
                    if (BasesType[ii]==0)
                    {
                        if (ii<i)
                        {
                            load_vect[ii]=load_vect[ii]-C*coeff_matrix[ii][i-ii];//M[ii][i-ii]
                        }
                        if (ii>i)
                        {
                            load_vect[ii]=load_vect[ii]-C*coeff_matrix[i][ii-i];
                        }
                    }

                };

            }
            else
            {
                if (BasesType[i])
                {
                    load_vect[i]=0.0;
                }

            };
        };
    };

// 	cout<<"Modified load_vect:"<<endl;
// 	for(unsigned int ii=0; ii<load_vect.size(); ii++)
// 	{
// 		cout<<ii<<"--->"<<load_vect[ii]<<endl;
// 	};

    //Modified the elements according to the basis with non-trivial boundary value
    int m_boundbasenum=0;

    for (int i=0; i<matrix_size; i++)
    {
        if (BasesType[i])
        {
            m_boundbasenum++;
            for (int j=0; j<matrix_size; j++)
            {
                coeff_matrix[i][j] = 0.0;
                if (i-j>=0)
                {
                    coeff_matrix[j][i-j]=0.0;
                }
            };
            coeff_matrix[i][0]=1.0;
        };
    };

// 	cout<<"Modified Coeff_Matrix:"<<endl;
// 	for (unsigned int ii=0; ii<coeff_matrix.size(); ii++)
// 	{
// 		for(unsigned int jj=0; jj<coeff_matrix[ii].size(); jj++)
// 		{
// 			cout<<coeff_matrix[ii][jj]<<" ";
// 		}
//
// 		cout<<endl;
// 	}


    // 		ofstream ofload;
    // 		ofload.open("load.txt");
    // 		for (int i=0;i<matrix_size;i++)
    // 		{
    // 			ofload<<load_vect[i]<<"  "<<endl;
    // 		}
    // 		ofload<<endl;
    // 		ofload.close();





// 		ofstream ofmatrix;
// 		ofmatrix.open("matrix.txt");
// 		for (int i=0;i<matrix_size;i++)
// 		{
// 			for(int j=0;j<matrix_size;j++)
// 				ofmatrix<<coeff_matrix[i][j]<<"  ";
// 			ofmatrix<<endl;
// 		}
// 		ofmatrix.close();


    cout<<"solving Linear Equation:"<<endl;
    //===============================
    m_solut.resize(matrix_size);
    LUsolve(matrix_size,matrix_size,coeff_matrix,load_vect,m_solut);
// 	//m_solut---->coeff of basis functions
// 	//===============================
    //delete coeff_matrix;
    //delete load_vect;
    //===============================

        ofstream ofsolut;
        ofsolut.open("coeff.txt");
        for (int i=0; i<matrix_size; i++)
        {

            ofsolut<<m_solut[i]<<endl;
        }
        ofsolut.close();
    //================================


// 	//Error----
// 	vector<double> cell_errors;
// 	ComputError(gausspt,gausspt, w, m_solut, cell_errors, m_cell, Basis);
};
