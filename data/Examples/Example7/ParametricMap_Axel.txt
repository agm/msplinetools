<axl>
<mspline type="InitialMesh">
<vertexnumber> 20</vertexnumber>

<cell>
0 1 0 1
1 8 7 0
</cell>

<cell>
0 1 0 1
2 3 8 1
</cell>

<cell>
0 1 0 1
8 11 10 7
</cell>

<cell>
0 1 0 1
19 12 11 8
</cell>

<cell>
0 1 0 1
11 17 16 10
</cell>

<cell>
0 1 0 1
12 18 17 11
</cell>

<cell>
0 1 0 1
17 14 15 16
</cell>

<cell>
0 1 0 1
18 13 14 17
</cell>

<cell>
0 1 0 1
14 8 9 15
</cell>

<cell>
0 1 0 1
13 19 8 14
</cell>

<cell>
0 1 0 1
8 5 6 9
</cell>

<cell>
0 1 0 1
3 4 5 8
</cell>

<cell>
0 1 0 1
13 18 12 19
</cell>


<transition cells="12 5">
0.0 1.0 -1.0 0.0
</transition>

<transition cells="9 12">
0.0 1.0 -1.0 0.0
</transition>

<transition cells="12 7">
-1.0 0.0 0.0 -1.0
</transition>


<points>
5 3 0
-1 -1 0
0 1 0
0 0 0

5 2 0
-1 -1 0
0 1 0
0 0 0

5 1 0
-1 0 0
0 1 0
0 0 0

4 0 0
0 -1 0
-1 0 0
0 0 0

5 -1 0
1 0 0
0 -1 0
0 0 0

5 -2 0
1 -1 0
0 -1 0
0 0 0

5 -3 0
1 -1 0
0 -1 0
0 0  0

3 2 0
-1 0 0
0 1 0
0 0 0

3 0 0
0 0 0
0 0 0
0 0 0

3 -2 0
1 0 0
0 -1 0
0 0 0

1 3 0
-3 0 0
0 1 0
0 0 0

1 2 0
-2 0 0
0 1 0
0 0 0

1 1 0
0 0 0
0 0 0
0 0 0

1 -1 0
0 0 0
0 0 0
0 0 0

1 -2 0
2 0 0
0 -1 0
0 0 0

1 -3 0
3 0 0
0 -1 0
0 0 0

-2 0 0
0 -3 0
-1 0 0
0 0 0

-1 0 0
0 -2 0
-1 0 0
0 0 0

0 0 0
0 0 0
0 0 0
0 0 0

2 0 0
0 0 0
0 0 0
0 0 0
</points>

</mspline>
</axl>