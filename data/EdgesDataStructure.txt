**************************************************************************
This file will introduce the data structure of Edges
**************************************************************************

-1: the general data structure:
_______________________________

NumEdges

#For each edge#################
Vindex1 Vindex2 IndexCommonCell        #describe an edge
#Repeat NumEdges times#########
_______________________________

-2: Examples

Let v0, v1, … , vn be all the vertices of a mesh,
where vi0, vi1, …, vik be the vertex sequence that describes the boundary of this mesh,
vijvi(j+1) is an edge of a cell (its index is Cellj) of this mesh.
Then Edges.txt is shown in the following:
=====================
k
vi0 vi1 Cell0
vi1 vi2 Cell1
vi2 vi3 Cell2
… … …
… … …
vi(k-1) vik Cell(k-1)
======================

