#ifndef DISTANCEBASEDHOOKCOEFFSALGORITHM_H
#define DISTANCEBASEDHOOKCOEFFSALGORITHM_H


//#include <utility>//pair

#include <vector>
#include <map>

#include <axlCore/axlAbstractData.h>
#include <axlCore/axlAbstractCurveBSpline.h>

#include "msp/Mesh.h"

using namespace std;

static double EdgeBased_HookCoeff(double mini_d, double max_d, unsigned indexV1,
                        unsigned indexV2, const vector<vector<double> >& VerticesPosition,
                                  const vector<axlPoint*>& ClosestPoints)
{
    double hc=0.0;
    //
    double x1=VerticesPosition[indexV1][0];
    double y1=VerticesPosition[indexV1][1];
    double x2=VerticesPosition[indexV2][0];
    double y2=VerticesPosition[indexV2][1];
    //
    double x3=ClosestPoints[indexV1]->x();
    double y3=ClosestPoints[indexV1]->y();

    //the normal direction (x1-x3,y1-y3)
    //the current direction (x2-x1,y2-y1)

    double sinA=abs_double((x1-x3)*(y2-y1)-(x2-x1)*(y1-y3));
    double d1=sqrt((x1-x3)*(x1-x3)+(y1-y3)*(y1-y3));
    double d2=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
    sinA=sinA/(d1*d2);
    if(sinA<1.0/sqrt(2.0))
    {
        double d=0.5*(d1+d2);
        if(d<max_d && d>mini_d)
        {
            hc=1.0/(d+mini_d);//Here can be chosen the best algorithm
            if(hc>2.0)
            {
                hc=2.0;
            }
        }
        else
        {
            hc=1.0;
        }

    }
    else
    {
        hc=1.0;
    }

    return hc;
}
//=========================================================================

//*************************************************************************************************
static void Algrithm1_ForChosingDifferentHookCoeffient(double mini_d,  double max_d,
                        const vector<double> &D_VToCurve, const vector<axlPoint*> &ClosestPoints,
                        const vector<vector<double> >& VerticesPosition, Mesh * P_ParametricMesh,
                        map<pair<unsigned, unsigned>,double>& Hc)
{
    //
    for(unsigned i_v=0; i_v<P_ParametricMesh->P_Vertices->size(); i_v++)
    {
        //For the i_v-th vertex of the ParametricMesh
        //The edge (pair<unsigned, unsigned>) connects to the i_v-th vertex
        for(set<unsigned>::iterator itV=P_ParametricMesh->P_Vertices->at(i_v).Index_ItsVertices.begin();
            itV!=P_ParametricMesh->P_Vertices->at(i_v).Index_ItsVertices.end(); itV++)
        {
            if(*itV>i_v)
            {
                pair<unsigned, unsigned> E(*itV,i_v);
                double hc=EdgeBased_HookCoeff(mini_d, max_d, *itV, i_v, VerticesPosition, ClosestPoints);

                Hc[E]=hc;

            }
        }
    }
}

//*************************************************************************************************

#endif
