#ifndef SPRINGFUNCTIONS_H
#define SPRINGFUNCTIONS_H


#include <utility>
#include <vector>
#include <map>

//*********************************************
#include <axlCore/axlAbstractData.h>
#include <axlCore/axlAbstractCurveBSpline.h>

#include <paramtools/distfield_curve.hpp>

//*********************************************

#include "msp/Mesh.h"

using namespace std;

static void DeltaPositionBasedOnSpringModelHcConst(double  Hc, double TimeSize, const vector<double>& Mass, const vector<vector<double> >& VerticesPosition,
                                     Mesh * P_ParametricMesh, vector<vector<double> > &DeltaPosition)
{
    /** InPut: Hook's coeff (Hc); TimeSize; the mass vector vector<double> Mass, the positions of vertices vector<vector<double> >& VerticesPosition
     *	  And the mesh P_ParmetricMesh
     * OutPut: the difference between the original vertices and the new vertices vector<vector<double> > &DeltaPosition
     */
    
    DeltaPosition.clear();
    
    //For each vertex, compute DeltaPosition[i_vertex]
    for(unsigned int i_vertex = 0; i_vertex < P_ParametricMesh->P_Vertices->size(); i_vertex++)
    {
        //===============================
        // For the i_vertex-th vertex, computer the force at this vertex (set<unsigned int> Index_ItsVertices;)
        //The force
        vector<double> Fi;
        Fi.push_back(0);
        Fi.push_back(0);//Fi=[0, 0]
        if(P_ParametricMesh->P_Vertices->at(i_vertex).Is_interiorVertex)
        {
            //The length:
            vector<double> L;
            double MeanL=0;
            
            for(set<unsigned int>::const_iterator itV=P_ParametricMesh->P_Vertices->at(i_vertex).Index_ItsVertices.begin();
                itV != P_ParametricMesh->P_Vertices->at(i_vertex).Index_ItsVertices.end(); itV++)
            {
                //L=[L0, L1, L2, ... ...]
                //the length from VerticesPosition[*itV] to VerticesPosition[i_vertex]
                double s=sqrt((VerticesPosition[*itV][0]-VerticesPosition[i_vertex][0])*(VerticesPosition[*itV][0]-VerticesPosition[i_vertex][0])+(VerticesPosition[*itV][1]-VerticesPosition[i_vertex][1])*(VerticesPosition[*itV][1]-VerticesPosition[i_vertex][1]));
                MeanL=MeanL+s;
                L.push_back(s);
            }
            MeanL=MeanL/(L.size()*1.0);// the average of the length
            
            unsigned int index=0;
            for(set<unsigned int>::const_iterator itV=P_ParametricMesh->P_Vertices->at(i_vertex).Index_ItsVertices.begin();
                itV != P_ParametricMesh->P_Vertices->at(i_vertex).Index_ItsVertices.end(); itV++)
            {
                //Fi=Fi+(VerticesPosition[*itV]-VerticesPosition[i_vertex])//the sum of vectors
                //directionCoeff=1.0-(MeanL/L[index])
                
                Fi[0]=Fi[0]+(VerticesPosition[*itV][0]-VerticesPosition[i_vertex][0]);
                Fi[1]=Fi[1]+(VerticesPosition[*itV][1]-VerticesPosition[i_vertex][1]);
                
                index=index+1;
                
            }
            //Fi=Hc*Fi
            Fi[0]=Hc*Fi[0];
            Fi[1]=Hc*Fi[1];
            //
            //
        }
        
        //============================================
        //The delta displacement
        vector<double> Si;
        Si.push_back(0.5*Fi[0]*TimeSize*TimeSize/Mass[i_vertex]);
        Si.push_back(0.5*Fi[1]*TimeSize*TimeSize/Mass[i_vertex]);
        
        //============================================
        //Push_back the i_vertex-th delta displacement
        DeltaPosition.push_back(Si);
        
    }
    
}

/*
 * Compute the delta positions owning to the coulomb Force from boundary edges
 */
static double CoulombForceIntegration(const double k, const double l, const double A, const double B, const double C)
{
    double F=0;
    //------------
    F=k*(-(2.0*B+4.0*C)/((4*A*C-B*B)*sqrt(A+B+C))+4.0*C/((4.0*A*C-B*B)*sqrt(C)));
    F=F+l*((4.0*A+2.0*B)/((4.0*A*C-B*B)*sqrt(A+B+C))-(2.0*B)/((4.0*A*C-B*B)*sqrt(C)));
    //------------
    return F;
}
static void CoulombForce(vector<double>& dF, const unsigned int i_vertex, set<pair<unsigned int, unsigned int> >::const_iterator et, const vector<vector<double> >& VerticesPosition)
{
    dF.clear();
    //---------------------
    unsigned int VEindex1=(*et).first;
    unsigned int VEindex2=(*et).second;
    double x0, y0, x1, y1, x2, y2;
    x0=VerticesPosition[i_vertex][0];
    y0=VerticesPosition[i_vertex][1];
    
    x1=VerticesPosition[VEindex1][0];
    y1=VerticesPosition[VEindex1][1];
    
    x2=VerticesPosition[VEindex2][0];
    y2=VerticesPosition[VEindex2][1];
    //-----------------------
    double A, B, C;
    A=(x2-x1)*(x2-x1)+(y2-y1)*(y2-y1);
    B=-2.0*((x0-x1)*(x2-x1)+(y0-y1)*(y2-y1));
    C=(x0-x1)*(x0-x1)+(y0-y1)*(y0-y1);
    
    double kx, lx;
    kx=x1-x2;
    lx=x0-x1;
    
    double ky, ly;
    ky=y1-y2;
    ly=y0-y1;
    //
    double Fx, Fy;
    Fx=CoulombForceIntegration(kx, lx, A, B, C);
    Fy=CoulombForceIntegration(ky, ly, A, B, C);
    //
    dF.push_back(Fx);//dF[0]
    
    dF.push_back(Fy);//dF[1]
    
    
}
static void DeltaPositionBasedOnBoundaryCoulombForce(const double &CoulombCoeff, const double &TimeSize, Mesh* P_ParametricMesh,
                                              const vector<vector<double> > &VerticesPosition,
                                              const vector<double> &Mass,
                                              vector<vector<double> > &DeltaPositionCoulomb)
{
    DeltaPositionCoulomb.clear();
    
    //Compute the boundary edges
    set<pair<unsigned int, unsigned int> > BoundaryEdges;
    P_ParametricMesh->GetBoundaryEdgesFromParametricMesh(BoundaryEdges);
    
    //Compute its delta position based on the force
    for(unsigned int i_vertex = 0; i_vertex < P_ParametricMesh->P_Vertices->size(); i_vertex++)
    {
        
        //----------------
        vector<double> dP;//the delta position for the i_vertex-th vertex
        //----------------
        
        vector<double> Fi;
        Fi.push_back(0.0);
        Fi.push_back(0.0);
        
        if(P_ParametricMesh->P_Vertices->at(i_vertex).Is_interiorVertex)//the i_vertex-th vertex is an interior vertex
        {
            //-----------For each boundary------
            for(set<pair<unsigned int, unsigned int> >::const_iterator et=BoundaryEdges.begin();
                et!=BoundaryEdges.end(); et++)
            {
                
                
                //-----
                vector<double> dF;
                //For the i_vertex (interior) vertex,
                //compute the force from the boundary edge *et
                CoulombForce(dF, i_vertex, et, VerticesPosition);//Do not implement
                //-----
                
                Fi[0]=Fi[0]+dF[0];
                Fi[1]=Fi[1]+dF[1];
                
            }
            
            
            //-----------------------------
        }
        
        Fi[0]=CoulombCoeff*Fi[0];
        Fi[1]=CoulombCoeff*Fi[1];
        
        //Compute the delta position for the i_vertex-th (interior) vertex
        dP.push_back(0.5*Fi[0]*TimeSize*TimeSize/Mass[i_vertex]);
        dP.push_back(0.5*Fi[1]*TimeSize*TimeSize/Mass[i_vertex]);
        
        //
        DeltaPositionCoulomb.push_back(dP);
    }
}

/*
 *VerticesPosition = VerticesPosition + DeltaPosition
 */
static void UpDataVerticesPosition(vector< vector<double> > &VerticesPosition,
                            const vector< vector<double> > &DeltaPosition)
{
    for(unsigned int i=0; i < VerticesPosition.size(); i++)
    {
        VerticesPosition[i][0]=VerticesPosition[i][0]+DeltaPosition[i][0];
        VerticesPosition[i][1]=VerticesPosition[i][1]+DeltaPosition[i][1];
    }
    //Return VerticesPosition
}


static double TotalDistance(const vector<vector<double> > &DeltaPositions)
{
    double epsilon=0.0;
    
    for(unsigned int i=0; i<DeltaPositions.size(); i++)
    {
        for(unsigned int j=0; j<DeltaPositions[i].size(); j++)
        {
            epsilon=epsilon+DeltaPositions[i][j]*DeltaPositions[i][j];
        }
    }
    
    epsilon=sqrt(epsilon)/(1.0*DeltaPositions.size());
    
    return epsilon;
}
//========================================================================

static void DeltaPositionSpringModelHcEdgeByEdge(const map<pair<unsigned, unsigned>, double> & Hc, double TimeSize,
                                                 const vector<double>& Mass, const vector<vector<double> >& VerticesPosition,
                                                 Mesh * P_ParametricMesh, vector<vector<double> > &DeltaPosition)
{
    /** InPut: Hook's coeff (Hc); TimeSize; the mass vector vector<double> Mass, the positions of vertices vector<vector<double> >& VerticesPosition
     *	  And the mesh P_ParmetricMesh
     * OutPut: the difference between the original vertices and the new vertices vector<vector<double> > &DeltaPosition
     */

    DeltaPosition.clear();

    //For each vertex, compute DeltaPosition[i_vertex]
    for(unsigned int i_vertex = 0; i_vertex < P_ParametricMesh->P_Vertices->size(); i_vertex++)
    {
        //===============================
        // For the i_vertex-th vertex, computer the force at this vertex (set<unsigned int> Index_ItsVertices;)
        //The force
        vector<double> Fi;
        Fi.push_back(0);
        Fi.push_back(0);//Fi=[0, 0]
        if(P_ParametricMesh->P_Vertices->at(i_vertex).Is_interiorVertex)
        {
            unsigned int index=0;
            for(set<unsigned int>::const_iterator itV=P_ParametricMesh->P_Vertices->at(i_vertex).Index_ItsVertices.begin();
                itV != P_ParametricMesh->P_Vertices->at(i_vertex).Index_ItsVertices.end(); itV++)
            {
                pair<unsigned, unsigned> edge(*itV, i_vertex);
                map<pair<unsigned,unsigned>, double>::const_iterator itmap=Hc.find(edge);
                double Hc_edge=itmap->second;

                Fi[0]=Fi[0]+Hc_edge*(VerticesPosition[*itV][0]-VerticesPosition[i_vertex][0]);
                Fi[1]=Fi[1]+Hc_edge*(VerticesPosition[*itV][1]-VerticesPosition[i_vertex][1]);
                //Fi=Fi+Hc_edge*(VerticesPosition[*itV]-VerticesPosition[i_vertex])//the sum of vectors

                index=index+1;
            }
            //
        }

        //============================================
        //The delta displacement
        vector<double> Si;
        Si.push_back(0.5*Fi[0]*TimeSize*TimeSize/Mass[i_vertex]);
        Si.push_back(0.5*Fi[1]*TimeSize*TimeSize/Mass[i_vertex]);

        //============================================
        //Push_back the i_vertex-th delta displacement
        DeltaPosition.push_back(Si);

    }

}
// //=========================================================================
/*Reture D_VToCurve, ClosestPoints*/
static void DistanceViToGeometry(const vector<vector<double> > &VerticesPosition, vector<double> &D_VToCurve,
                                 vector<axlPoint*> &ClosestPoints, axlAbstractCurveBSpline *axlcurve)
{
    //
    D_VToCurve.clear();
    ClosestPoints.clear();
    for(unsigned i_v=0; i_v<VerticesPosition.size(); i_v++)
    {
        double x=VerticesPosition[i_v][0];
        double y=VerticesPosition[i_v][1];
        double z=0.0;

        distfield_curve<axlAbstractCurveBSpline> dst(axlcurve, 100);
        //
        D_VToCurve.push_back(dst.distance(x,y,z));
        //Closest point
        int i;
        double u0 = dst.nearestCurveParameter(x,y,z, i);//i is the i-th curve returned
        axlPoint* P = new axlPoint(dst.curve(i)->eval(u0));

        ClosestPoints.push_back(P);

    }
}

//=========================================================================
#endif
