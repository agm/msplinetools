// /////////////////////////////////////////////////////////////////
// Generated by axel-plugin wizard
// /////////////////////////////////////////////////////////////////

/* (C) MyCompany */


/* Put a short description of your plugin here */

/* MyCompany-contact@mycompany.com-http://www.mycompany.com */

#ifndef MSPLINETOOLSPLUGIN_H
#define MSPLINETOOLSPLUGIN_H

#include <dtkCoreSupport/dtkPlugin.h>

#include "MSplineToolsExport.h"

class dtkAbstractDataFactory;
class dtkAbstractProcessFactory;

class MSplineToolsPluginPrivate;

class MSPLINETOOLSPLUGIN_EXPORT MSplineToolsPlugin : public dtkPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.MSplineToolsPlugin" FILE "MSplineToolsPlugin.json")

public:
    MSplineToolsPlugin(QObject *parent = 0);
    ~MSplineToolsPlugin(void);
    
    virtual bool initialize(void);
    virtual bool uninitialize(void);
    
    virtual QString name(void) const;
    virtual QString description(void) const;
    
    virtual QStringList tags(void) const;
    virtual QStringList types(void) const;
    
public:
    static dtkAbstractDataFactory *dataFactSingleton;
    static dtkAbstractProcessFactory *processFactSingleton;
    
    
private:
    MSplineToolsPluginPrivate *d;
};

#endif //MSPLINETOOLSPLUGIN_H

