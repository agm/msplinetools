// /////////////////////////////////////////////////////////////////
// Generated by axel-plugin wizard
// /////////////////////////////////////////////////////////////////

/*  */


/*  */

/* INRIA-Meng- */

#ifndef MSPLINETOOLSMSPLINESURFACEDATAREADER_H
#define MSPLINETOOLSMSPLINESURFACEDATAREADER_H

#include <axlCore/axlAbstractDataReader.h>

#include "MSplineToolsExport.h"

class dtkAbstractData;

//class MSplineToolsMSplineSurfaceData;

class MSPLINETOOLSPLUGIN_EXPORT MSplineToolsMSplineSurfaceDataReader : public axlAbstractDataReader
{
    Q_OBJECT

public :
    MSplineToolsMSplineSurfaceDataReader(void);
    ~MSplineToolsMSplineSurfaceDataReader(void);
    
public:
    QString identifier(void) const;
    QString description(void) const;
    QStringList handled(void) const;
    
    static bool registered(void);
    
public:
    bool accept(const QDomNode& node);
    bool reject(const QDomNode& node);
    
    axlAbstractData *read(const QDomNode& node);
    //MSplineToolsMSplineSurfaceData* read(const QDomNode& node);
};

dtkAbstractDataReader *createMSplineToolsMSplineSurfaceDataReader(void);

#endif //MSPLINETOOLSMSPLINESURFACEDATAREADER_H

